//
//  Rectangle.m
//  Layout
//
//  Created by beesightsoft on 7/5/14.
//  Copyright (c) 2014 Marc Matteo. All rights reserved.
//

#import "Rectangle.h"

@implementation Rectangle

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.clipsToBounds = YES;
        self.userInteractionEnabled = YES;
        
        if (UIDeviceOrientationIsPortrait([UIDevice currentDevice].orientation))
        {
            self.width = 150;
            self.height = 200;
        }
        else
        {
            self.width = 200;
            self.height = 150;
        }
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder {
    [coder encodeObject:[NSNumber numberWithFloat:self.scaleValue] forKey:@"ScaleValue"];
    [coder encodeObject:[NSNumber numberWithFloat:self.rotateValue] forKey:@"RotateValue"];
    
    //    self.width = self.width * self.scaleValue;
    //    self.height = self.height * self.scaleValue;
    
//    NSLog(@"Encode rotate value: %.0f", self.rotateValue);
//    NSLog(@"Encode scale value: %.2f", self.scaleValue);
    
    [coder encodeObject:[NSNumber numberWithFloat:self.width] forKey:@"Width"];
    [coder encodeObject:[NSNumber numberWithFloat:self.height] forKey:@"Height"];
    
    [coder encodeObject:[NSNumber numberWithInteger:self.radius] forKey:@"Radius"];
    
    [coder encodeObject:self.image forKey:@"Image"];
    [coder encodeObject:self.backgroundColor forKey:@"BackgroundColor"];
    [coder encodeObject:[NSValue valueWithCGPoint:self.center] forKey:@"CenterPoint"];
    //    [coder encodeObject:self.transform forKey:@"Transform"];
    
    //    CGAffineTransform rotate = CGAffineTransformMakeRotation(0);
    //    self.transform = rotate;
    
    //    NSNumber *frameWidth = [NSNumber numberWithFloat:self.frame.size.width];
    //    NSNumber *frameHeight = [NSNumber numberWithFloat:self.frame.size.height];
    //    NSNumber *frameX = [NSNumber numberWithFloat:self.frame.origin.x];
    //    NSNumber *frameY = [NSNumber numberWithFloat:self.frame.origin.y];
    
    //    [coder encodeObject:frameWidth forKey:@"FrameWidth"];
    //    [coder encodeObject:frameHeight forKey:@"FrameHeight"];
    //    [coder encodeObject:frameX forKey:@"FrameX"];
    //    [coder encodeObject:frameY forKey:@"FrameY"];
    
    //    rotate = CGAffineTransformMakeRotation(self.rotateValue);
    //    self.transform = rotate;
}

- (id)initWithCoder:(NSCoder *)coder {
    self = [super init];
    if (self) {
        self.scaleValue = [[coder decodeObjectForKey:@"ScaleValue"] floatValue];
        self.rotateValue = [[coder decodeObjectForKey:@"RotateValue"] floatValue];
        
        self.width = [[coder decodeObjectForKey:@"Width"] floatValue];
        self.height = [[coder decodeObjectForKey:@"Height"] floatValue];
        
        self.radius = [[coder decodeObjectForKey:@"Radius"] integerValue];
        
        [self.layer setCornerRadius:self.radius];
        self.image = [coder decodeObjectForKey:@"Image"];
        [self setBackgroundColor:[coder decodeObjectForKey:@"BackgroundColor"]];
        //        CGFloat frameWidth = [[coder decodeObjectForKey:@"FrameWidth"] floatValue];
        //        CGFloat frameHeight = [[coder decodeObjectForKey:@"FrameHeight"] floatValue];
        //        CGFloat frameX = [[coder decodeObjectForKey:@"FrameX"] floatValue];
        //        CGFloat frameY = [[coder decodeObjectForKey:@"FrameY"] floatValue];
        
        if (UIDeviceOrientationIsPortrait([UIDevice currentDevice].orientation))
        {
            CGRect rect = CGRectMake(0, 0, 150, 200);
            self.frame = rect;
        }
        else
        {
            CGRect rect = CGRectMake(0, 0, 200, 150);
            self.frame = rect;
        }
        
        self.center = [[coder decodeObjectForKey:@"CenterPoint"] CGPointValue];
        
        NSLog(@"Decode rotate value: %.0f", self.rotateValue);
        NSLog(@"Decode scale value: %.2f", self.scaleValue);
        
    }
    return self;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    /* Set UIView Border */
	// Get the contextRef
	CGContextRef contextRef = UIGraphicsGetCurrentContext();
	
	// Set the border width
	CGContextSetLineWidth(contextRef, 5.0);
	
	// Set the border color to RED
	CGContextSetRGBStrokeColor(contextRef, 255.0, 0.0, 0.0, 1.0);
	
	// Draw the border along the view edge
	CGContextStrokeRect(contextRef, rect);
}

- (BOOL)canBecomeFirstResponder {
    return YES;
}


@end
