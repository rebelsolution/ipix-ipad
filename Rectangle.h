//
//  Rectangle.h
//  Layout
//
//  Created by beesightsoft on 7/5/14.
//  Copyright (c) 2014 Marc Matteo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TSPAppDelegate.h"

@interface Rectangle : UIImageView

@property (nonatomic) CGFloat scaleValue;
@property (nonatomic) CGFloat rotateValue;
@property (nonatomic) CGFloat width;
@property (nonatomic) CGFloat height;
@property (nonatomic) NSInteger radius;

@end
