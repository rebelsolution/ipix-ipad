//
//  CustomScrollView.h
//  Layout
//
//  Created by beesightsoft on 7/14/14.
//  Copyright (c) 2014 Marc Matteo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Rectangle.h"

@interface CustomScrollView : UIScrollView

@end