//
//  AMUploadItem.m
//  PhotoManagerTest
//
//  Created by App Developer on 10/5/16.
//  Copyright © 2016 My Company. All rights reserved.
//

#import "AMUploadItem.h"

#define KEY_NAME  @"kImageName"
#define KEY_DICT  @"kDictionary"
#define KEY_PATH  @"kPath"


#define KEY_RESUME           @"kResume"
#define KEY_UPLOAD_COMPLETE  @"kUploadComplete"
#define KEY_IDENTIFIER       @"kIdentifier"
#define KEY_DID_ERROR        @"kDidError"


@implementation AMUploadItem
- (id) initWithItem:(NSString *)itemName andDictionary:(NSMutableDictionary*)dictionary {
    if (self = [super init]) {
        self.itemName = itemName;
        self.dictionary = dictionary;
        self.fileProgress = 0.0f;
        self.isUploading = NO;
        self.uploadComplete = NO;
        self.didError = NO;
        self.taskIdentifier = -1;
        self.uploadTask = nil;
    }
    
    return self;
    
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if(self){
        _imageData      = [aDecoder decodeObjectForKey:KEY_PATH];
        _itemName   = [aDecoder decodeObjectForKey:KEY_NAME];
        _dictionary = [aDecoder decodeObjectForKey:KEY_DICT];

        _didError = [aDecoder decodeBoolForKey:KEY_DID_ERROR];
        _taskResumeData   = [aDecoder decodeObjectForKey:KEY_RESUME];
        _uploadComplete = [aDecoder decodeBoolForKey:KEY_UPLOAD_COMPLETE];
        _taskIdentifier = [aDecoder decodeIntegerForKey:KEY_IDENTIFIER];

    }
    return self;
}

- (void) encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:_imageData  forKey:KEY_PATH];
    [aCoder encodeObject:_itemName   forKey:KEY_NAME];
    [aCoder encodeObject:_dictionary forKey:KEY_DICT];
    
    [aCoder encodeObject:_taskResumeData    forKey:KEY_RESUME];
    [aCoder encodeBool:_uploadComplete      forKey:KEY_UPLOAD_COMPLETE];
    [aCoder encodeInteger:_taskIdentifier   forKey:KEY_IDENTIFIER];
    [aCoder encodeBool:_didError            forKey:KEY_DID_ERROR];


}


@end
