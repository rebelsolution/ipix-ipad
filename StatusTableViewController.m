//
//  PhotoStatusTableViewController.m
//  PhotoManagerTest
//
//  Created by App Developer on 10/5/16.
//  Copyright © 2016 My Company. All rights reserved.
//

#import "StatusTableViewController.h"
#import "AMPhotoManager.h"
#import "AMUploadItem.h"
#import "ItemTableViewCell.h"


@interface StatusTableViewController () <AMPhotoManagerDelegate>
@property (strong, nonatomic) AMPhotoManager * manager;
@property (strong, nonatomic) IBOutlet UITableView *tblView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnPause;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnClear;

@end

@implementation StatusTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _manager = [AMPhotoManager sharedManager];
    _manager.delegate = self;
    
    self.title = [NSString stringWithFormat:@"Photos: %lu", (unsigned long)_manager.getQueueCount];
    
    [self updateGUI];
    
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self didUpdatePhotoCount];
    
    
}

- (void) viewWillAppear:(BOOL)animated {
    [self.navigationController setToolbarHidden:NO animated:YES];
    
}

- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[UINavigationBar appearance] setBarTintColor:[UIColor whiteColor]];
    [[UIToolbar appearance] setBarTintColor:[UIColor whiteColor]];
    
    
    [self.navigationController setToolbarHidden:YES animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneButton:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)pause:(id)sender {
    
    if ([_manager isTimerRunning]) {
        [_manager stop];
    } else {
        [_manager start];
    }
    
    [[NSUserDefaults standardUserDefaults] setBool:[_manager isTimerRunning] forKey:@"kPhotoMangerIsRunning"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    [self updateGUI];
    
    
}

- (IBAction)report:(id)sender {
    [self performSegueWithIdentifier:@"seg_showreport" sender:sender];
    
}

- (IBAction)clear:(id)sender {
    [self showClearQuestion:sender];
}

- (void) showClearQuestion:(id)sender {
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"Are you sure you want to clear out the queue?" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel action")
                                   style:UIAlertActionStyleCancel
                                   handler:nil];
    
    
    UIAlertAction *deleteAction  = [UIAlertAction actionWithTitle:NSLocalizedString(@"Yes", @"Yes")
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction *action)
                                    {
                                        [_manager deleteQueue];
                                        
                                    }];
    
    
    [alertController addAction:cancelAction];
    [alertController addAction:deleteAction];
    
    
    
    UIPopoverPresentationController *popover = alertController.popoverPresentationController;
    if (popover)
    {
        UIButton * btn = (UIButton*)sender;
        popover.sourceView = btn;
        popover.sourceRect = btn.bounds;
        popover.permittedArrowDirections = UIPopoverArrowDirectionAny;
    }
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void) updateGUI {
    [self.btnPause setTitle:[_manager isTimerRunning] ? @"PAUSE" : @"START"];
    
    //Completely optional but fun for the user
    [self.navigationController.navigationBar  setBarTintColor:[_manager isTimerRunning] ? [UIColor whiteColor] : [UIColor yellowColor]];
    [self.navigationController.toolbar setBarTintColor:[_manager isTimerRunning] ? [UIColor whiteColor] : [UIColor yellowColor]];
    
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_manager getQueueCount];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // Configure the cell...
    static NSString *simpleTableIdentifier = @"cellPhoto";
    
    ItemTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[ItemTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    [cell.btnRetry addTarget:self action:@selector(retryUpload:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnDelete addTarget:self action:@selector(deleteUpload:) forControlEvents:UIControlEventTouchUpInside];
    
    AMUploadItem * item = [_manager listImagesInQueue][indexPath.row];
    cell.lblTitle.text = item.itemName;
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    
    dispatch_async(queue, ^{
        dispatch_sync(dispatch_get_main_queue(), ^{
            cell.thumbnailImage.image = [UIImage imageWithData:item.imageData];
            
        });
    });
    
    cell.progressView.progress = item.fileProgress;
    cell.lblProgress.text = [NSString stringWithFormat:@"%.1f%%", (item.fileProgress * 100) ];
    
    cell.lblTitle.textColor = [UIColor darkGrayColor];
    
    if (item.didError == NO) {
        cell.btnDelete.hidden = YES;
        cell.btnRetry.hidden = YES;
    } else if (item.didError == YES) {
        cell.lblTitle.textColor = [UIColor redColor];
        cell.btnDelete.hidden = NO;
        cell.btnRetry.hidden = NO;
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 75;
}

#pragma mark DELETE RETRY ACTIONS
- (void) retryUpload:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    if (indexPath != nil) {
        [_manager retryItemWithIdentifier:[self getTaskIdentifierForIndexPath:indexPath]];
        [self.tblView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    }
}

- (void) deleteUpload:(id)sender {
    
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"Remove?" message:@"Are you sure you want to cancel this upload?" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel action")
                                   style:UIAlertActionStyleCancel
                                   handler:nil];
    
    
    UIAlertAction *ok  = [UIAlertAction actionWithTitle:NSLocalizedString(@"Yes", @"Yes")
                                                  style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction *action)
                          {
                              
                              if (indexPath != nil) {
                                  [_manager deleteItemWithIdentifier:[self getTaskIdentifierForIndexPath:indexPath]];
                              }
                              
                              
                          }];
    
    
    [alertController addAction:cancelAction];
    [alertController addAction:ok];
    
    
    
    UIPopoverPresentationController *popover = alertController.popoverPresentationController;
    if (popover)
    {
        UIButton * btn = (UIButton*)sender;
        popover.sourceView = btn;
        popover.sourceRect = btn.bounds;
        popover.permittedArrowDirections = UIPopoverArrowDirectionAny;
    }
    
    [self presentViewController:alertController animated:YES completion:nil];
    
    
}

#pragma mark AMPhotoManager Delgate

- (void) didUpdatePhotoCount {
    self.title = [NSString stringWithFormat:@"Photos: %lu", (unsigned long)_manager.getQueueCount];
    [_tblView reloadData];
}

- (void) updateProgressViewInIndex:(NSUInteger)identifier {
    ItemTableViewCell *cell = nil;
    for(AMUploadItem* item in [_manager listImagesInQueue]){
        if(item.taskIdentifier  == identifier){
            
            cell = [self.tblView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:[self getFileDownloadInfoIndexWithTaskIdentifier:identifier] inSection:0]];
            cell.progressView.progress = item.fileProgress;
            cell.lblProgress.text = [NSString stringWithFormat:@"%.1f%%", (item.fileProgress * 100) ];
            
            
            if (item.didError == NO) {
                cell.lblTitle.textColor = [UIColor darkGrayColor];
                
                cell.btnDelete.hidden = YES;
                cell.btnRetry.hidden = YES;
            } else if (item.didError == YES) {
                
                cell.lblTitle.textColor = [UIColor redColor];
                
                cell.btnDelete.hidden = NO;
                cell.btnRetry.hidden = NO;
            }
            
        }
    }
}

- (void) reloadCellContainingIdentifer:(NSUInteger)identifier {
    
    NSIndexPath* indexPath = [NSIndexPath indexPathForRow:[self getFileDownloadInfoIndexWithTaskIdentifier:identifier] inSection:0];
    NSArray* indexArray = [NSArray arrayWithObjects:indexPath, nil];
    [_tblView reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationNone];
    
}

- (NSUInteger) getTaskIdentifierForIndexPath:(NSIndexPath*) indexPath {
    AMUploadItem * item = [_manager listImagesInQueue][indexPath.row];
    return item.taskIdentifier;
}

- (int) getFileDownloadInfoIndexWithTaskIdentifier:(NSUInteger)taskIdentifier{
    int index = 0;
    
    for (int i=0; i< [_manager getQueueCount]; i++) {
        AMUploadItem *upload = [_manager listImagesInQueue][i];
        if (upload.taskIdentifier == taskIdentifier) {
            index = i;
            break;
        }
    }
    return index;
}

@end
