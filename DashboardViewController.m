//
//  ViewController.m
//  PhotoManagerTest
//
//  Created by App Developer on 10/5/16.
//  Copyright © 2016 My Company. All rights reserved.
//

#import "DashboardViewController.h"
#import "AMPhotoManager.h"

@interface DashboardViewController () <AMPhotoManagerDelegate, UIImagePickerControllerDelegate,UINavigationControllerDelegate, UIAlertViewDelegate>

@property (strong, nonatomic) AMPhotoManager * manager;
@property (strong, nonatomic) UIActivityIndicatorView * activityAdd;


@property (weak, nonatomic) IBOutlet UIButton *btnAddPhoto;
@property (weak, nonatomic) IBOutlet UIButton *btnViewStatus;
@property (weak, nonatomic) IBOutlet UIButton *btnClear;
@property (weak, nonatomic) IBOutlet UIButton *btnPauseUploads;
@property (weak, nonatomic) IBOutlet UILabel *count;
@property (weak, nonatomic) IBOutlet UILabel *state;
@property (weak, nonatomic) IBOutlet UILabel *lblMessage;

@end

@implementation DashboardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Here you want to iniitalize the manager
    //Usually in your first ViewController or whereever you add images view controller will live.
    _manager = [AMPhotoManager sharedManager];
    _manager.useCellularData = YES; //Set to no to upload on wifi connections only.
    _manager.timerInterval = 1.0f; // How often should you check the queue for photos to upload? 1 second is pretty good.
    _manager.logErrorReports = YES; // do you want to use the logging functionality
    _manager.delegate = self;
    
    
    
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"kPhotoMangerIsRunning"]) {
        // NSLog(@"NO PHOTOMANGER KEY EXISTS. CREATING ONE");
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"kPhotoMangerIsRunning"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"kPhotoMangerIsRunning"]) {
        [_manager start]; //Starts checking the queue for images.
    }
    
    
    
    //This is just added for convience to the user
    //You can implement your own activity indicator.
    //This shows after the image picker dissmises, to show the user that it is working to add it to the photo queue
    self.activityAdd = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    self.activityAdd.color = [UIColor blueColor];
    [self.activityAdd hidesWhenStopped];
    self.activityAdd.frame = CGRectMake(self.view.frame.size.width / 2 , self.view.frame.size.height / 2, self.activityAdd.frame.size.height, self.activityAdd.frame.size.height);
    [self.activityAdd stopAnimating];
    [self.view addSubview:self.activityAdd];
    
    
    
    [self updateGUI];
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self didUpdatePhotoCount];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) updateGUI {
    
    [_btnPauseUploads setTitle:[_manager isTimerRunning] ? @"PAUSE QUEUE" : @"START UPLOADS" forState:UIControlStateNormal];
    
    _count.text = [NSString stringWithFormat:@"%lu", (long)(unsigned)[_manager getQueueCount]];
    
    _state.textColor = [_manager isTimerRunning] ? [UIColor greenColor] : [UIColor redColor];
    _state.text = [NSString stringWithFormat:@"%@", [_manager isTimerRunning] ? @"RUNNING" : @"PAUSED"];
}

#pragma mark - PHOTO MANAGER DELEGATE METHODS
- (void) didUpdatePhotoCount {    
    //This is called so you can update any onscreen counts
    _count.text = [NSString stringWithFormat:@"%lu", [_manager getQueueCount]];
}

- (void) photoWasAddedToQueue:(NSString *)name {
    //here is where you would optionally notify the user that a photo was added
    //this is just here for a demo
    _lblMessage.text = [NSString stringWithFormat:@"Photo %@ was added.", name];
}

#pragma mark - IBACTIONS
- (IBAction)actionPause:(id)sender {
    
    if ([_manager isTimerRunning]) {
        [_manager stop]; //stops checking the queue for photos to upload, but allows the in progress images to finish.
    } else {
        [_manager start];
    }
    
    [[NSUserDefaults standardUserDefaults] setBool:[_manager isTimerRunning] forKey:@"kPhotoMangerIsRunning"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    
    [self updateGUI];
}


- (IBAction)actionViewStatus:(id)sender {
    
}

- (IBAction)clear:(id)sender {
    //DELETES ALL REMAINING IMAGES FROM QUEUE
    [_manager deleteQueue];
    [self didUpdatePhotoCount];
}

- (IBAction)addPhotos:(id)sender {
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"Choose Source" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel action")
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction *action)
                                   {
                                       NSLog(@"Cancel action");
                                   }];
    
    UIAlertAction *cameraAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Camera", @"Camera")
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction *action)
                                   {
                                       [self showImagePickerForSourceType:UIImagePickerControllerSourceTypeCamera];
                                   }];
    
    UIAlertAction *albumAction  = [UIAlertAction actionWithTitle:NSLocalizedString(@"Album", @"Album")
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction *action)
                                   {
                                       [self showImagePickerForSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
                                   }];
    
    
    //CHECK TO SEE IF THE DEVICE HAS A CAMERA BEFORE WE ADD THE BUTTON OR THE APP WILL CRASH
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        [alertController addAction:cameraAction];
    }
    
    [alertController addAction:cancelAction];
    [alertController addAction:albumAction];
    
    
    //IF IPAD SHOW THE ACTION AS A POP UP OR THE APP WILL CRASH
    //PER APPLE UI GUIDLINES
    UIPopoverPresentationController *popover = alertController.popoverPresentationController;
    if (popover) {
        UIButton * btn = (UIButton*)sender;
        popover.sourceView = btn;
        popover.sourceRect = btn.bounds;
        popover.permittedArrowDirections = UIPopoverArrowDirectionAny;
    }
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}


#pragma mark - UIImagePickerController METHODS
- (void)showImagePickerForSourceType:(UIImagePickerControllerSourceType)sourceType {
    
    UIImagePickerController * ipc = [[UIImagePickerController alloc] init];
    ipc.modalPresentationStyle = UIModalPresentationCurrentContext;
    ipc.sourceType = sourceType;
    ipc.allowsEditing = YES; // <- can be set to NO if you dont want the user editing the photo
    ipc.delegate = self;
    
    if (sourceType == UIImagePickerControllerSourceTypeCamera) {
        ipc.showsCameraControls = YES;
    }
    
    [self presentViewController:ipc animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    [self.activityAdd startAnimating];
    
    [picker dismissViewControllerAnimated:YES completion:^{
        
        __block NSData * imageData = nil;
        __block NSMutableDictionary * companion = nil;
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            //LETS BUILD THE IMAGE AND DICTIONARY ON THE BACKGROUND THREAD TO NOT FREEZE THE GUI
            
            //HERE YOU CAN ADD SOME EXTRA STUFF SO YOU CAN PULL IT OUT LATER IN YOUR POST.
            companion = [[NSMutableDictionary alloc]init];
            [companion setValue:[self randomStringWithLength:8] forKey:@"EasyName"];  //YOU CAN NAME IT WHAT YOU WANT THIS IS JUST FOR AN EXAMPLE.
            [companion setValue:[self buildJSONForImageUpload] forKey:@"POST_DATA"];  //GET the authentication stuff

            
            imageData = UIImagePNGRepresentation(info[UIImagePickerControllerOriginalImage]);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if (imageData != nil) {
                    [_manager addImageToQueue:imageData withDictionary:companion];
                    //Show the user that the image was added           
                } else {
                    NSLog(@"Image was null");
                    //Show the user the image was null.
                }
                [self.activityAdd stopAnimating];
                
            }); //distpach get main queue
        });//background queueu
    }];//picker completion
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:^{
        //just in case it is running lets stop the activity indicator
        [self.activityAdd stopAnimating];
    }];
}



#pragma mark - Photo upload methods
/*
 
 THIS IS JUST DEMO / DUMMY JSON DATA TO HELP YOU BUILD AND TEST YOUR OWN SYSTEM.
 
 THIS IS PROVIDED AS A CONVIENCE FOR YOU AND IS NOT SUPPORTED
 
 */

- (NSString*) buildJSONForImageUpload {
    NSString * product  = @"MyProduct";
    NSString * noun     = @"Images";
    NSString * verb     = @"UploadImage";
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"EN"]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    //Header
    NSMutableDictionary * dicOperation = [[NSMutableDictionary alloc]init];
    [dicOperation setValue:product forKey:@"Product"];
    [dicOperation setValue:noun    forKey:@"Noun"];
    [dicOperation setValue:verb    forKey:@"Verb"];
    
    //Content
    NSMutableDictionary * dicAuthentication = [[NSMutableDictionary alloc] init];
    [dicAuthentication setValue:@"145654665"   forKey:@"SessionID"];
    [dicAuthentication setValue:@"MyUsername"    forKey:@"UserName"];
    
    //Extra Data
    
    NSMutableDictionary * dicData = [[NSMutableDictionary alloc] init];
    [dicData setValue:@"A Value"    forKey:@"SomeExtraData"];
    [dicData setValue:@"userkey"     forKey:@"MoreDataYouCanUse"];
    [dicData setValue:@"CallKey"     forKey:@"TheFileToAttachTo"];

    
    //Package all the dictionarys together
    NSMutableDictionary *dictPackage = [[NSMutableDictionary alloc] init];
    [dictPackage setValue:dicAuthentication forKey:@"Authentication"];
    [dictPackage setValue:dicOperation      forKey:@"Operation"];
    [dictPackage setValue:dicData           forKey:@"Data"];
    
    
    //build the json data from the dictionaries
    NSError *error;
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:dictPackage options:NSJSONWritingPrettyPrinted error:&error];

    return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
}

#pragma mark - DEMO METHODS

- (NSString *) randomStringWithLength: (int) len {
    NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    
    NSMutableString *randomString = [NSMutableString stringWithCapacity: len];
    
    for (int i=0; i<len; i++) {
        [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random_uniform((int)[letters length])]];
    }
    
    return randomString;
}


@end
