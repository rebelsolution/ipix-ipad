//
//  LCFlashControl.h
//  LumeCubeSDK
//
//  Created by Daniel Hoggar on 11/10/15.
//  Copyright © 2015 Lume Cube, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LCBluetoothManager.h"

/*******************************************************************************/
/* Common LumeCube commands                                                    */
/*******************************************************************************/
extern NSString *kMaxBrightnessOneSecondFlash;
extern NSString *kMediumBrightnessOneSecondFlash;
extern NSString *kLowBrightnessOneSecondFlash;
extern NSString *kVideoLightOff;
extern NSString *kOptoTriggerOn;
extern NSString *kOptoTriggerOff;

/*******************************************************************************/
/* LumeCube supported flash brightness                                         */
/*******************************************************************************/
#ifndef FLASHBRIGHTNESS
typedef enum : NSUInteger {
    FullBrightness,
    ThreeQuarterBrightness,
    HalfBrightness,
    QuarterBrightness,
    EighthBrightness,
    SixteenthBrightness,
    ThirtySecondBrightness,
    SixtyFourthBrightness,
    OneTwentyEighthBrightness
} FlashBrightness ;
#define FLASHBRIGHTNESS
#endif

/*******************************************************************************/
/* LumeCube supported flash durations                                          */
/*******************************************************************************/
#ifndef FLASHDURATION
typedef enum : NSUInteger {
    OneSecond,
    HalfSecond,
    QuarterSecond,
    EighthSecond,
    FifteenthSecond,
    ThirtiethSecond,
    SixtiethSecond,
    OneTwenthFifthSecond,
    TwoFiftiethSecond,
    FiveHundredthSecond,
    ThousandthSecond,
    TwentyFiveHundredthSecond,
    FiveThousandthSecond,
    EightThousandthSecond
} FlashDuration ;
#define FLASHDURATION
#endif

/*******************************************************************************/
/* LumeCube supported video durations                                          */
/*******************************************************************************/
#ifndef VIDEODURATION
typedef enum : NSUInteger {
    AlwaysOn,
    ThirtyMinutes,
    TwentyMinutes,
    FifteenMinutes,
    TenMinutes,
    FiveMinutes,
    TwoMinutes,
    OneMinute
} VideoDuration ;
#define VIDEODURATION
#endif

/*******************************************************************************/
/* LumeCube supported strobe frequencies                                       */
/*******************************************************************************/
#ifndef STROBEFREQUENCY
typedef enum : NSUInteger {
    HalfHertz,
    OneHertz,
    OneAndHalfHertz,
    TwoHertz,
    ThreeHertz,
    FiveHertz,
    TenHertz
} StrobeFrequency ;
#define STROBEFREQUENCY
#endif

/*******************************************************************************/
/* LCFlashControl Class                                                        */
/*******************************************************************************/
@interface LCFlashControl : NSObject

/*******************************************************************************/
/* Flash Control                                                               */
/*******************************************************************************/

// Peform a one second flash at max brightness on all cubes
+ (void)fireDefaultFlashOnAllLumeCubes;

// Perform one of the standard flashes on all cubes
+ (void)fireFlashOnAllLumeCubesWithType:(NSString *)flashType;

// Perform one of the standard flashes on the specified cube
+ (void)fireFlashOnLumeCube:(CBPeripheral *)peripheral
                   withType:(NSString *)flashType;

// Perform a custom flash on the specified cube
+ (void)fireFlashOnLumeCube:(CBPeripheral *)peripheral
             withBrightness:(FlashBrightness)brightness
                andDuration:(FlashDuration)duration;

// Turn on the opto trigger for the specified cube
+ (void)enableOptoTriggerOnLumeCube:(CBPeripheral *)peripheral;

// Turn off the opto trigger for the specified cube
+ (void)disableOptoTriggerOnLumeCube:(CBPeripheral *)peripheral;

/*******************************************************************************/
/* Video / Strobe Light Control                                                */
/*******************************************************************************/

// Turn on video light on specified cube with percentage brightness from 1-100
// and supported duration.  Percentage brightness of 0 turns off the light on
// specified cube.
+ (void)turnOnVideoLightOnLumeCube:(CBPeripheral *)peripheral
          withPercentageBrightness:(NSUInteger)brightness
                       andDuration:(VideoDuration)duration;

// Turn on strobe light on specified cube with percentage brightness from 1-100
// and supported frequency.  Percentage brightness of 0 turns off the light on
// specified cube.
+ (void)turnOnStrobeLightOnLumeCube:(CBPeripheral *)peripheral
           withPercentageBrightness:(NSUInteger)brightness
                       andFrequency:(StrobeFrequency)frequency;

// Turn off the light on specifed cube
+ (void)turnOffVideoLightOnLumeCube:(CBPeripheral *)peripheral;

@end
