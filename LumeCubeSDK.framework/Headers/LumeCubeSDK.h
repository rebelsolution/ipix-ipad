//
//  LumeCubeSDK.h
//  LumeCubeSDK
//
//  Created by Daniel Hoggar on 11/10/15.
//  Copyright © 2015 Lume Cube, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for LumeCubeSDK.
FOUNDATION_EXPORT double LumeCubeSDKVersionNumber;

//! Project version string for LumeCubeSDK.
FOUNDATION_EXPORT const unsigned char LumeCubeSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <LumeCubeSDK/PublicHeader.h>
#import <LumeCubeSDK/LCBluetoothManager.h>
#import <LumeCubeSDK/LCFlashControl.h>


