//
//  LCBluetoothManager.h
//  LumeCubeSDK
//
//  Created by Daniel Hoggar on 11/10/15.
//  Copyright © 2015 Lume Cube, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>

/*******************************************************************************/
/* Protocols                                                                   */
/*******************************************************************************/
@protocol LCBluetoothManagerDelegate <NSObject>
- (void)bluetoothPoweredOff;
- (void)lumeCubeDiscovered:(CBPeripheral *)lcPeripheral;
@optional
- (void)lumeCubeConnected:(CBPeripheral *)lcPeripheral;
- (void)lumeCubeDisconnected:(CBPeripheral *)lcPeripheral;
- (void)batteryLevelUpdated:(NSUInteger)level
              forPeripheral:(CBPeripheral *)lcPeripheral;
- (void)signalStrengthUpdated:(NSNumber *)RSSI
                forPeripheral:(CBPeripheral *)lcPeripheral;
@end

/*******************************************************************************/
/* LCBluetoothManager Class                                                    */
/*******************************************************************************/
@interface LCBluetoothManager : NSObject

/*******************************************************************************/
/* Properties                                                                  */
/*******************************************************************************/
@property (nonatomic, assign) id<LCBluetoothManagerDelegate> lcbmDelegate;
@property (nonatomic, strong) NSMutableArray *connectedPeripherals;
@property (nonatomic, strong) NSMutableArray *discoveredPeripherals;

/*******************************************************************************/
/* Singleton Access                                                            */
/*******************************************************************************/
+ (id)sharedInstance;

/*******************************************************************************/
/* Public methods typically used by the user                                   */
/*******************************************************************************/
- (void)scanForLumeCubes;
- (void)stopScan;
- (void)connectPeripheral:(CBPeripheral *)peripheral;
- (void)disconnectPeripheral:(CBPeripheral *)peripheral;

@end
