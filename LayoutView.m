//
//  LayoutView.m
//  iSIT
//
//  Created by beesightsoft on 7/19/14.
//  Copyright (c) 2014 Mountain. All rights reserved.
//

#import "LayoutView.h"

@implementation LayoutView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder {
    
    NSNumber *frameWidth = [NSNumber numberWithFloat:self.frame.size.width];
    NSNumber *frameHeight = [NSNumber numberWithFloat:self.frame.size.height];
    NSNumber *frameX = [NSNumber numberWithFloat:self.frame.origin.x];
    NSNumber *frameY = [NSNumber numberWithFloat:self.frame.origin.y];
    
    [coder encodeObject:frameWidth forKey:@"FrameWidth"];
    [coder encodeObject:frameHeight forKey:@"FrameHeight"];
    [coder encodeObject:frameX forKey:@"FrameX"];
    [coder encodeObject:frameY forKey:@"FrameY"];
}

- (id)initWithCoder:(NSCoder *)coder {
    self = [super init];
    if (self) {
        
        CGFloat frameWidth = [[coder decodeObjectForKey:@"FrameWidth"] floatValue];
        CGFloat frameHeight = [[coder decodeObjectForKey:@"FrameHeight"] floatValue];
        CGFloat frameX = [[coder decodeObjectForKey:@"FrameX"] floatValue];
        CGFloat frameY = [[coder decodeObjectForKey:@"FrameY"] floatValue];
        
        CGRect rect = CGRectMake(frameX, frameY, frameWidth, frameHeight);
        self.frame = rect;
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
