//
//  MWGridViewController.h
//  MWPhotoBrowser
//
//  Created by Michael Waterfall on 08/10/2013.
//
//

#import <UIKit/UIKit.h>
#import "MWPhotoBrowser.h"
#import "FCAlertView.h"
@class MWGridViewController;

@protocol MWGridViewControllerDelegate <NSObject>

- (void)sharePhotoActionFromGrid;
- (void)reloadGalleryFromGrid:(MWGridViewController*)grid;

@end

@interface MWGridViewController : UICollectionViewController <FCAlertViewDelegate>{}

@property (nonatomic) id <MWGridViewControllerDelegate> delegate;

@property (nonatomic, assign) MWPhotoBrowser *browser;
@property (nonatomic) BOOL selectionMode;
@property (nonatomic) CGPoint initialContentOffset;

- (void)adjustOffsetsAsRequired;

@end
