//
//  CustomScrollView.m
//  Layout
//
//  Created by beesightsoft on 7/14/14.
//  Copyright (c) 2014 Marc Matteo. All rights reserved.
//

#import "CustomScrollView.h"
#import "Rectangle.h"


@implementation CustomScrollView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

//- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
//{
//    NSLog(@"touch began");
//    [self setScrollEnabled:YES];
//}
//
//-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
//    CGPoint locationPoint = [[touches anyObject] locationInView:self];
//    
//    if ([[self hitTest:locationPoint withEvent:event] isKindOfClass:[UIView class]])
//    {
//        NSLog(@"Touch scrollview");
//        return;
//    }
//    else
//    {
//        NSLog(@"TEST");
//    }
//}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
