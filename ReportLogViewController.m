//
//  ReportLogViewController.m
//  PhotoManagerTest
//
//  Created by App Developer on 10/14/16.
//  Copyright © 2016 My Company. All rights reserved.
//

#import "ReportLogViewController.h"
#import "AMPhotoManager.h"

#define EMAIL_ADDRESS @"me@mydomain.com"

@interface ReportLogViewController () <UIAlertViewDelegate, MFMailComposeViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UITextView *textView;

@end

@implementation ReportLogViewController

- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.textView.text = [[AMPhotoManager sharedManager] getReportContents];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void) showClearAlert {
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"CLEAR LOG?" message:@"Are you sure want to clear the log?" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction       = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel action")  style:UIAlertActionStyleDefault handler:nil];
    UIAlertAction *clearAction  = [UIAlertAction actionWithTitle:NSLocalizedString(@"Clear", @"Clear action")  style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action){
        [[AMPhotoManager sharedManager] clearReportLog];
        self.textView.text = [[AMPhotoManager sharedManager] getReportContents];
        
    }];
    [alertController addAction:cancelAction];
    [alertController addAction:clearAction];
    [self presentViewController:alertController animated:YES completion:nil];
    
}

- (void) sendEmail {
    
    if ([MFMailComposeViewController canSendMail]) {
        
        NSString *emailTitle = @"My Upload Log Report";
        
        NSMutableString *messageBody = [[NSMutableString alloc] init];
        [messageBody appendString:@"Here is my upload report:\n"];
        
        [messageBody appendString:[[AMPhotoManager sharedManager] getReportContents]];
        
        NSArray *toRecipents = [NSArray arrayWithObject:EMAIL_ADDRESS];
        
        MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
        mc.mailComposeDelegate = self;
        [mc setSubject:emailTitle];
        [mc setMessageBody:messageBody isHTML:NO];
        [mc setToRecipients:toRecipents];
        
        // Present mail view controller on screen
        [self presentViewController:mc animated:YES completion:NULL];
        
    } else {
        
        [self showAlertWithTitle:@"Error!" Message:@"No email accounts found. Please add an account in settings."];
        
    }
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    
    NSString * message = @"";
    
    switch (result)
    {
        case MFMailComposeResultCancelled:
            message = @"Send mail cancelled.";
            break;
        case MFMailComposeResultSaved:
            message = @"Saved to drafts.";
            break;
        case MFMailComposeResultSent:
            message = @"Mail sent!";
            break;
        case MFMailComposeResultFailed:
            message = [NSString stringWithFormat:@"Mail sent failure: %@", [error localizedDescription]];
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:^{
        [self showAlertWithTitle:@"Alert!" Message:message];
    }];
}

- (IBAction)btnPressed:(id)sender {
    UIButton * btn = (UIButton*) sender;
    switch (btn.tag) {
        case 1:
            [self sendEmail];
            break;
        case 2:
            [self showClearAlert];
            break;
        case 3:
            [self dismissViewControllerAnimated:YES completion:nil];
            break;
            
        default:
            break;
    }
}

- (void) showAlertWithTitle:(NSString*)title Message:(NSString*)message {
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:NSLocalizedString(@"Ok", @"Cancel action")  style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:action];
    [self presentViewController:alertController animated:YES completion:nil];
}





@end
