//
//  TSPPhotoRepository.h
//  iPIX Social
//
//  Created by Luong Chau Tuan on 1/28/17.
//  Copyright © 2017 Tuts+. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TSPPhotoRepository : NSObject

@property (nonatomic) UIImage* image;
@property (nonatomic) NSInteger index;

- (id)initWithPhoto:(UIImage*)image andIndex:(NSInteger)index;

@end
