//
//  AMPopTipDefaults.h
//  AMPopTip
//
//  Created by Andrea Mazzini on 10/06/15.
//  Copyright (c) 2015 Fancy Pixel. All rights reserved.
//

/** @constant AMPopTip default values */
#define kDefaultFont [UIFont systemFontOfSize:[UIFont systemFontSize]]
#define kDefaultTextColor [UIColor whiteColor]
#define kDefaultBackgroundColor [UIColor redColor]
#define kDefaultBorderColor [UIColor colorWithWhite:0.182 alpha:1.000]
#define kDefaultBorderWidth 2
#define kDefaultRadius 18
#define kDefaultPadding 20
#define kDefaultArrowSize CGSizeMake(35, 35)
#define kDefaultAnimationIn 0.4
#define kDefaultAnimationOut 0.2
#define kDefaultBounceAnimationIn 1.2
#define kDefaultBounceAnimationOut 1.0
#define kDefaultEdgeInsets UIEdgeInsetsZero
#define kDefaultEdgeMargin 0
#define kDefaultOffset 0
#define kDefaultBubbleOffset 0
#define kDefaultBounceOffset 8
#define kDefaultFloatOffset 8
#define kDefaultPulseOffset 1.1
