//
//  TSPPhotoRepository.m
//  iPIX Social
//
//  Created by Luong Chau Tuan on 1/28/17.
//  Copyright © 2017 Tuts+. All rights reserved.
//

#import "TSPPhotoRepository.h"

@implementation TSPPhotoRepository

- (id)initWithPhoto:(UIImage *)image andIndex:(NSInteger)index
{
    self = [super init];
    
    if (self)
    {
        _image = image;
        _index = index;
    }
    
    return self;
}
@end
