//
//  AMPhotoManager.h
//  PhotoManagerTest
//
//  Created by App Developer on 10/5/16.
//  Copyright © 2016 My Company. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>



@class AMPhotoManager;
@protocol AMPhotoManagerDelegate <NSObject>

/**
 *  Delegate method that gets called to update any labels or count monitors.
 */
@optional
- (void) didUpdatePhotoCount;

/**
 *  Delegate method that gets called to on the table cells to show progress.
 */
@optional
- (void) updateProgressViewInIndex:(NSUInteger)identifier;

/**
 *  Delegate method that request a cell reload its data.
 */
@optional
- (void) reloadCellContainingIdentifer:(NSUInteger)identifer;

/**
 *  A method that is called to notify the user a photo was added
 */
@optional
- (void) photoWasAddedToQueue : (NSString*) name;

@end


@interface AMPhotoManager : NSObject

/**
 *  Property for the wait time between checking.
 */
@property (nonatomic,assign) float timerInterval;

/**
 *  Property to detemine if photos should be uploaded on celluar data.
 */
@property (nonatomic,assign) BOOL useCellularData;

/**
 *  Property to detemine if errors should be logged in a text file.
 */
@property (nonatomic,assign) BOOL logErrorReports;

/**
 *  Delegate property accessor
 */
@property (weak, nonatomic) id <AMPhotoManagerDelegate> delegate;

/**
 *  Singleton instance through which all photos are managed.
 *
 *  @return AMPhotoManager instance (singleton).
 */
+ (AMPhotoManager *) sharedManager;

/**
 *  Starts the timer to check for photos.
 */
- (void) start;

/**
 *  Stops the timer to check for photos.
 */
- (void) stop;


/**
 *  Returns a count of images in queue.
 *
 *  @return Convience method to return a NSInterger of images in queue.
 */
- (NSUInteger) getQueueCount;


/**
 *  Returns a list of images in queue along with additional infomation.
 *
 *  @return Dictionary of images in queue.
 */
- (NSMutableArray *) listImagesInQueue;

/**
 *  Checks if the timer is valid and running.
 *
 *  @return bool if timer is valid.
 */
- (BOOL) isTimerRunning;



- (void) addImageToQueue:(NSData *)image;

/**
 *  Retry's uploading an image with a given identfier.
 *
 *  @param identfier          Task identifier.
 *
 */
- (void) retryItemWithIdentifier:(NSUInteger)identfier;

/**
 *  Delete an upload an image with a given identfier.
 *
 *  @param identfier          Task identifier.
 *
 */
- (void) deleteItemWithIdentifier:(NSUInteger)identfier;

/**
 *  Get the contents of the PhotoManger error log.
 *
 *  @return String with conents of log.
 */
- (NSString*) getReportContents;

/**
 *  Clears the contents of the log.
 */
- (void) clearReportLog;

/**
 *  Clears the contents of the queue.
 */
- (void) deleteQueue;



@end
