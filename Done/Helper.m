//
//  Helper.m
//  Kitchen
//
//  Created by Luong Chau Tuan on 12/31/15.
//  Copyright © 2015 Luong Chau Tuan. All rights reserved.
//

#import "Helper.h"

static Helper *sharedInstance = nil;

NSInteger const ANIMATION_GIF = 0;
NSInteger const PHOTO_BOOTH = 1;
NSInteger const SHARE_STATION = 2;
NSString *FORMAT_VIDEO = @"VIDEO";
NSString *FORMAT_GIF = @"GIF";

@implementation Helper

+(Helper*)getShareInstance
{
    if (!sharedInstance)
    {
        sharedInstance = [[super allocWithZone:NULL] init];
    }
    
    return sharedInstance;
}

+ (NSString *)uuid
{
    CFUUIDRef uuidRef = CFUUIDCreate(NULL);
    CFStringRef uuidStringRef = CFUUIDCreateString(NULL, uuidRef);
    CFRelease(uuidRef);
    return (__bridge_transfer NSString *)uuidStringRef;
}

@end
