//
//  TSPToDoCell.m
//  Done
//
//  Created by Bart Jacobs on 24/07/14.
//  Copyright (c) 2014 Tuts+. All rights reserved.
//

#import "TSPToDoCell.h"

@implementation TSPToDoCell

#pragma mark -
#pragma mark Initialization
- (void)awakeFromNib {
    [super awakeFromNib];
    
    // Setup View
    [self setupView];
}

#pragma mark -
#pragma mark View Methods
- (void)setupView {
    UIImage *imageNormal = [UIImage imageNamed:@"delete"];
    UIImage *imageSelected = [UIImage imageNamed:@"delete"];

    
    
    [self.doneButton setImage:imageNormal forState:UIControlStateNormal];
    [self.doneButton setImage:imageNormal forState:UIControlStateDisabled];
    [self.doneButton setImage:imageSelected forState:UIControlStateSelected];
    [self.doneButton setImage:imageSelected forState:UIControlStateHighlighted];
    [self.doneButton addTarget:self action:@selector(didTapButton:) forControlEvents:UIControlEventTouchUpInside];
    
    
 
    [self.doneButton1 addTarget:self action:@selector(didTapButton1:) forControlEvents:UIControlEventTouchUpInside];
    
      [self.doneButton2 addTarget:self action:@selector(didTapButton2:) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark -
#pragma mark Actions
- (void)didTapButton:(UIButton *)button {
    if (self.didTapButtonBlock) {
        self.didTapButtonBlock();
    }
}


- (void)didTapButton1:(UIButton *)button {
    if (self.didTapButtonBlock1) {
        self.didTapButtonBlock1();
    }
}

- (void)didTapButton2:(UIButton *)button {
    if (self.didTapButtonBlock2) {
        self.didTapButtonBlock2();
    }
}


@end

