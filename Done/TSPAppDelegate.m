//
//  TSPAppDelegate.m
//  Done
//
//  Created by Bart Jacobs on 13/07/14.
//  Copyright (c) 2014 Tuts+. All rights reserved.
//

#import "TSPAppDelegate.h"
#import "Rectangle.h"
#import <CoreData/CoreData.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "TSPViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "TSPEventRepository.h"

#define RADIANS_TO_DEGREES(radians) ((radians) * (180.0 / M_PI))
#define DEGREES_TO_RADIANS(degrees) ((degrees) * (M_PI / 180.0))

@interface TSPAppDelegate ()

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@end

@implementation TSPAppDelegate

+ (TSPAppDelegate*) sharedAppDelegate
{
    return (TSPAppDelegate *) [UIApplication sharedApplication].delegate;
}

#pragma mark -
#pragma mark Application Did Finish Launching
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Fetch Main Storyboard
    [application setStatusBarHidden:YES];
    
    _mcManager = [[MCManager alloc] init];
    
    _selectedPhotoCount = 0;
    
    _arrConnectedDevices = [[NSMutableArray alloc]init];
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    
    // Instantiate Root Navigation Controller
    UINavigationController *rootNavigationController = (UINavigationController *)[mainStoryboard instantiateViewControllerWithIdentifier:@"rootNavigationController"];
    
    DBSession* dbSession =[[DBSession alloc]initWithAppKey:@"2wk0p3pfw9by3x0" appSecret:@"8y9vlovehfe921p" root:kDBRootDropbox]; // Root either kDBRootAppFolder or kDBRootDropbox
    [DBSession setSharedSession:dbSession];
    [FBSDKLikeControl class];
    [FBSDKLoginButton class];
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    // Configure View Controller
    TSPViewController *viewController = (TSPViewController *)[rootNavigationController topViewController];
    
    if ([viewController isKindOfClass:[TSPViewController class]]) {
        [viewController setManagedObjectContext:self.managedObjectContext];
    }
    
    [self getCustomLayoutFromDefaults];
    
    // Configure Window
    [self.window setRootViewController:rootNavigationController];
    
    
    return YES;
}

#pragma mark -
#pragma mark Application Life Cycle

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation


{
    
    
    if ([[DBSession sharedSession] handleOpenURL:url])
    {
        if ([[DBSession sharedSession] isLinked])
        {
            // At this point you can start making API calls
            
            NSLog(@"App linked successfully!");
            NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
            [center postNotificationName:@"Login Success" object: self];
        }
        // Add whatever other url handling code your app requires here
    }
    
    
    
    
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
    
    
    
    return NO;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Save Managed Object Context
    [self saveManagedObjectContext];
    
    /*
     NSError *error = nil;
     
     if (![self.managedObjectContext save:&error]) {
     if (error) {
     NSLog(@"Unable to save changes.");
     NSLog(@"%@, %@", error, error.localizedDescription);
     }
     }
     */
}



- (void)applicationWillEnterForeground:(UIApplication *)application {
    
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Save Managed Object Context
    [self saveManagedObjectContext];
    
    /*
     NSError *error = nil;
     
     if (![self.managedObjectContext save:&error]) {
     if (error) {
     NSLog(@"Unable to save changes.");
     NSLog(@"%@, %@", error, error.localizedDescription);
     }
     }
     */
}

#pragma mark -
#pragma mark Core Data Stack
- (NSManagedObjectContext *)managedObjectContext {
    if (_managedObjectContext) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    
    if (coordinator) {
        _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    
    return _managedObjectContext;
}

- (NSManagedObjectModel *)managedObjectModel {
    if (_managedObjectModel) {
        return _managedObjectModel;
    }
    
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Done" withExtension:@"momd"];
    
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    if (_persistentStoreCoordinator) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *applicationDocumentsDirectory = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
    NSURL *storeURL = [applicationDocumentsDirectory URLByAppendingPathComponent:@"Done.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        //        abort();
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark -
#pragma mark Helper Methods
- (void)saveManagedObjectContext {
    NSError *error = nil;
    
    if (![self.managedObjectContext save:&error]) {
        if (error) {
            NSLog(@"Unable to save changes.");
            NSLog(@"%@, %@", error, error.localizedDescription);
        }
    }
}
- (BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark - Save And Get Custom Layout

- (void)saveCustomLayoutToDefaults
{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
    NSMutableArray *arrNew = [[NSMutableArray alloc] init];
    for (int i = 0; i < self.customEvents.count; i++)
    {
        TSPEventRepository* eventItem = [self.customEvents objectAtIndex:i];
        
        [arrNew addObject:eventItem.getEventData];
    }
    
    [standardUserDefaults setObject:arrNew forKey:@"PhotoboothEvents"];
    
    [standardUserDefaults synchronize];
    
}

- (void)getCustomLayoutFromDefaults
{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
    NSMutableArray *arrNew = [standardUserDefaults objectForKey:@"PhotoboothEvents"];
    
    self.customEvents = [[NSMutableArray alloc] init];
    
    for (int i = 0; i< arrNew.count; i++)
    {
        TSPEventRepository* eventItem = [[TSPEventRepository alloc] initWithDict:[arrNew objectAtIndex:i]];
        [self.customEvents addObject:eventItem];
    }
}

- (void)saveCustomLayout
{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
    NSMutableArray *archiveArray = [NSMutableArray arrayWithCapacity:self.customLayoutArray.count];
    
    for (Rectangle *rectangle in self.customLayoutArray)
    {
        NSData *rectangleData = [NSKeyedArchiver archivedDataWithRootObject:rectangle];
        [archiveArray addObject:rectangleData];
    }
    
    [standardUserDefaults setObject:archiveArray forKey:@"CustomLayoutArray"];
    
    NSLog(@"IsSmallCustomLayout: %@", [NSNumber numberWithBool:self.isSmallCustomLayout]);
    NSLog(@"IsSmallCustomLayouth: %@", [NSNumber numberWithBool:self.isSmallCustomLayouth]);
    NSLog(@"IsLargeCustomLayout: %@", [NSNumber numberWithBool:self.isLargeCustomLayout]);
    NSLog(@"IsLargeCustomLayouth: %@", [NSNumber numberWithBool:self.isLargeCustomLayouth]);
    
    [standardUserDefaults setObject:[NSNumber numberWithInteger:self.customLayoutCount] forKey:@"CustomLayoutCount"];
    [standardUserDefaults setBool:self.isSmallCustomLayout forKey:@"IsSmallCustomLayout"];
    [standardUserDefaults setBool:self.isSplipLayout forKey:@"IsSplitLayOut"];
    [standardUserDefaults setBool:self.isSmallCustomLayouth forKey:@"IsSmallCustomLayouth"];
    [standardUserDefaults setBool:self.isLargeCustomLayout forKey:@"IsLargeCustomLayout"];
    [standardUserDefaults setBool:self.isLargeCustomLayouth forKey:@"IsLargeCustomLayouth"];
    [standardUserDefaults synchronize];
}

- (void)getCustomLayout
{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
    self.customLayoutArray = [NSMutableArray array];
    
    NSMutableArray *rectanglesArray = [standardUserDefaults objectForKey:@"CustomLayoutArray"];
    
    for (int i = 0; i< rectanglesArray.count; i++)
    {
        NSData *rectangleData = [rectanglesArray objectAtIndex:i];
        Rectangle *rectangle = [NSKeyedUnarchiver unarchiveObjectWithData:rectangleData];
        
        [self.customLayoutArray addObject:rectangle];
    }
    
    self.customLayoutCount = [[standardUserDefaults objectForKey:@"CustomLayoutCount"] integerValue];
    
    self.isSmallCustomLayout = [[standardUserDefaults objectForKey:@"IsSmallCustomLayout"] boolValue];
    
    //Redraw custom layout view
    if (self.customLayoutArray.count > 0)
    {
        if (self.isSmallCustomLayout)
        {
            self.customLayoutView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 476, 715)];
            
            //Add background imageView
            UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 476, 715)];
            [backgroundImageView setTag:1];
            [self.customLayoutView addSubview:backgroundImageView];
            
            for (int i = 0; i < self.customLayoutArray.count; i++)
            {
                Rectangle *rectangle = [self.customLayoutArray objectAtIndex:i];
                rectangle.frame = rectangle.frame;
                
                CGPoint centerPoint = rectangle.center;
                
                CGAffineTransform rotate = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(rectangle.rotateValue));
                CGAffineTransform scale = CGAffineTransformMakeScale(rectangle.scaleValue, rectangle.scaleValue);
                CGAffineTransform scaleAndRotate = CGAffineTransformConcat(rotate, scale);
                rectangle.transform = scaleAndRotate;
                
                rectangle.center = centerPoint;
                
                [self.customLayoutView addSubview:rectangle];
            }
        }
        
        if (self.isLargeCustomLayout)
        {
            self.customLayoutView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 510, 715)];
            
            //Add background imageView
            UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 510, 715)];
            [backgroundImageView setTag:1];
            [self.customLayoutView addSubview:backgroundImageView];
            
            for (int i = 0; i < self.customLayoutArray.count; i++)
            {
                Rectangle *rectangle = [self.customLayoutArray objectAtIndex:i];
                rectangle.frame = rectangle.frame;
                
                CGPoint centerPoint = rectangle.center;
                
                CGAffineTransform rotate = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(rectangle.rotateValue));
                CGAffineTransform scale = CGAffineTransformMakeScale(rectangle.scaleValue, rectangle.scaleValue);
                CGAffineTransform scaleAndRotate = CGAffineTransformConcat(rotate, scale);
                rectangle.transform = scaleAndRotate;
                
                rectangle.center = centerPoint;
                
                [self.customLayoutView addSubview:rectangle];
            }
        }
        
        if (self.isSmallCustomLayouth)
        {
            self.customLayoutView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 715, 476)];
            
            //Add background imageView
            UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 715, 476)];
            [backgroundImageView setTag:1];
            [self.customLayoutView addSubview:backgroundImageView];
            
            for (int i = 0; i < self.customLayoutArray.count; i++)
            {
                Rectangle *rectangle = [self.customLayoutArray objectAtIndex:i];
                rectangle.frame = rectangle.frame;
                
                CGPoint centerPoint = rectangle.center;
                
                CGAffineTransform rotate = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(rectangle.rotateValue));
                CGAffineTransform scale = CGAffineTransformMakeScale(rectangle.scaleValue, rectangle.scaleValue);
                CGAffineTransform scaleAndRotate = CGAffineTransformConcat(rotate, scale);
                rectangle.transform = scaleAndRotate;
                
                rectangle.center = centerPoint;
                
                [self.customLayoutView addSubview:rectangle];
            }
        }
        
        if (self.isLargeCustomLayouth)
        {
            self.customLayoutView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 715, 510)];
            
            //Add background imageView
            UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0,715, 510)];
            [backgroundImageView setTag:1];
            [self.customLayoutView addSubview:backgroundImageView];
            
            for (int i = 0; i < self.customLayoutArray.count; i++)
            {
                Rectangle *rectangle = [self.customLayoutArray objectAtIndex:i];
                rectangle.frame = rectangle.frame;
                
                CGPoint centerPoint = rectangle.center;
                
                CGAffineTransform rotate = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(rectangle.rotateValue));
                CGAffineTransform scale = CGAffineTransformMakeScale(rectangle.scaleValue, rectangle.scaleValue);
                CGAffineTransform scaleAndRotate = CGAffineTransformConcat(rotate, scale);
                rectangle.transform = scaleAndRotate;
                
                rectangle.center = centerPoint;
                
                [self.customLayoutView addSubview:rectangle];
            }
        }
    }
}



@end
