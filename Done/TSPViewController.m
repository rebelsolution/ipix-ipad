//
//  TSPViewController.m
//  Done
//
//  Created by Bart Jacobs on 13/07/14.
//  Copyright (c) 2014 Tuts+. All rights reserved.
//

#import "TSPViewController.h"
#import "AnimatedNormal.h"
#import <CoreData/CoreData.h>
#import "PhotoBoothNormal.h"
#import "SharingNormal.h"
#import "TSPToDoCell.h"
#import "TSPAddToDoViewController.h"
#import "TSPUpdateToDoViewController.h"
#import <Photos/Photos.h>
#import "JSONModel+networking.h"
#import "SBJson.h"
#import "CNPPopupController.h"
#import <MessageUI/MessageUI.h>
#import "Helper.h"

@interface TSPViewController () <MFMailComposeViewControllerDelegate,  CNPPopupControllerDelegate,NSFetchedResultsControllerDelegate>
{
    IBOutlet UITextField *IDNUMBER;
    IBOutlet UITextField *ipixemailaddress;
    IBOutlet UIButton *emptysurvey;
}
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

@property (strong, nonatomic) NSIndexPath *selection;
@property (nonatomic, strong) CNPPopupController *popupController;
@end

@implementation TSPViewController
@synthesize buttonborders;
@synthesize resultView;
#pragma mark -
#pragma mark View Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self deviceUUID];
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                                                  stringForKey:[[NSBundle mainBundle] bundleIdentifier]];
                        
                            NSLog(@"UUID IS %@", savedValue);
    IDNUMBER.text = savedValue;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString* strValue =[defaults objectForKey:@"ipixemail"];
       ipixemailaddress.text = strValue;
    ipixemailaddress.returnKeyType = UIReturnKeyGo;

    
    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
        switch (status) {
            case PHAuthorizationStatusAuthorized:
                NSLog(@"YES");
                break;
            case PHAuthorizationStatusRestricted:
                NSLog(@"Restricted");
                break;
            case PHAuthorizationStatusDenied:
                NSLog(@"Denied");
                break;
            default:
                break;
        }
    }];
 
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if(authStatus == AVAuthorizationStatusAuthorized)
    {
        NSLog(@"%@", @"You have camera access");
    }
    else if(authStatus == AVAuthorizationStatusDenied)
    {
        NSLog(@"%@", @"Denied camera access");
        
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            if(granted){
                NSLog(@"Granted access to %@", AVMediaTypeVideo);
            } else {
                NSLog(@"Not granted access to %@", AVMediaTypeVideo);
            }
        }];
    }
    else if(authStatus == AVAuthorizationStatusRestricted)
    {
        NSLog(@"%@", @"Restricted, normally won't happen");
    }
    else if(authStatus == AVAuthorizationStatusNotDetermined)
    {
        NSLog(@"%@", @"Camera access not determined. Ask for permission.");
        
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            if(granted){
                NSLog(@"Granted access to %@", AVMediaTypeVideo);
            } else {
                NSLog(@"Not granted access to %@", AVMediaTypeVideo);
            }
        }];
    }
    else
    {
        NSLog(@"%@", @"Camera access unknown error.");
    }

    AVAuthorizationStatus authStatus1 = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeAudio];
    if(authStatus1 == AVAuthorizationStatusAuthorized)
    {
        NSLog(@"%@", @"You have camera access");
    }
    else if(authStatus1 == AVAuthorizationStatusDenied)
    {
        NSLog(@"%@", @"Denied camera access");
        
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeAudio completionHandler:^(BOOL granted) {
            if(granted){
                NSLog(@"Granted access to %@", AVMediaTypeAudio);
            } else {
                NSLog(@"Not granted access to %@", AVMediaTypeAudio);
            }
        }];
    }
    else if(authStatus1 == AVAuthorizationStatusRestricted)
    {
        NSLog(@"%@", @"Restricted, normally won't happen");
    }
    else if(authStatus1 == AVAuthorizationStatusNotDetermined)
    {
        NSLog(@"%@", @"Camera access not determined. Ask for permission.");
        
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeAudio completionHandler:^(BOOL granted) {
            if(granted){
                NSLog(@"Granted access to %@", AVMediaTypeAudio);
            } else {
                NSLog(@"Not granted access to %@", AVMediaTypeAudio);
            }
        }];
    }
    else
    {
        NSLog(@"%@", @"Camera access unknown error.");
    }
    
    
    
    // Initialize Fetch Request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"TSPItem"];
    
    // Add Sort Descriptors
    [fetchRequest setSortDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"createdAt" ascending:YES]]];
    
    // Initialize Fetched Results Controller
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    
    // Configure Fetched Results Controller
    [self.fetchedResultsController setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsController performFetch:&error];
    
    if (error) {
        NSLog(@"Unable to perform fetch.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    }
    
    for (UIButton *button in buttonborders) {
        [[button layer] setBorderWidth:1.0f];
        [[button layer] setBorderColor:[UIColor darkGrayColor].CGColor];
    }
    
}






-(IBAction)RegisterDevice:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    TSPViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"RegisterAdmin"];
    [vc setManagedObjectContext:self.managedObjectContext];
    
    [vc setModalPresentationStyle:UIModalPresentationFullScreen];
    [self presentViewController:vc animated:NO completion:nil];
}


-(IBAction)Move:(id)sender
{
    
   NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString* strValue =[defaults objectForKey:@"ipixemail"];
    NSLog(@"Email subject %@", strValue);
    
    
        NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                                stringForKey:[[NSBundle mainBundle] bundleIdentifier]];
    
        NSLog(@"UUID IS %@", savedValue);
        @try {
           if([strValue isEqualToString:@""] || strValue == nil ) {
               
               
               [self showhaveyou:CNPPopupStyleCentered];
               
               
    }
            
            
           
            
           else {
               NSString *post =[[NSString alloc] initWithFormat:@"email=%@", strValue];
               NSLog(@"PostData: %@",post);
               NSURL *url1=[NSURL URLWithString:@"http://www.ipixsocial.com/membership/view/front/cache/test/getresult.php"];
               
               NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
               
               NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
               
               NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
               [request setURL:url1];
               [request setHTTPMethod:@"POST"];
               [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
               [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
               [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
               [request setHTTPBody:postData];
               
               NSError *error = [[NSError alloc] init];
               NSHTTPURLResponse *response = nil;
               NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
               
               NSLog(@"Response code: %ld", (long)[response statusCode]);
               if ([response statusCode] >=200 && [response statusCode] <300)
               {
                   NSString *responseData = [[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
                   NSLog(@"JSON Response is %@", responseData);
                   
                   
                   SBJsonParser *jsonParser = [SBJsonParser new];
                   NSDictionary *jsonData = (NSDictionary *) [jsonParser objectWithString:responseData error:nil];
                   //   NSString *username = [(NSString *) [jsonData objectForKey:@"email"]init];
                   NSInteger success = [(NSNumber *) [jsonData objectForKey:@"uid"] integerValue];
                   NSString* ipixid = [jsonData objectForKey:@"ipixid"];
                   NSString* ipixid1 = [jsonData objectForKey:@"ipixid1"];
                   NSString* ipixid2 = [jsonData objectForKey:@"ipixid2"];
                   NSString* ipixid3 = [jsonData objectForKey:@"ipixid3"];
                   NSString* ipixid4 = [jsonData objectForKey:@"ipixid4"];
                   NSString* ipixid5 = [jsonData objectForKey:@"ipixid5"];
                   
                   NSString* twiliosid = [jsonData objectForKey:@"twiliosid"];
                   NSString* twilioauth = [jsonData objectForKey:@"twilioauth"];
                   NSString* twiliophone = [jsonData objectForKey:@"twiliophone"];
                   NSString* sendgridlogin = [jsonData objectForKey:@"sendgridlogin"];
                   NSString* sendgridpass = [jsonData objectForKey:@"sendgridpass"];
                   NSString* twitterkey = [jsonData objectForKey:@"twitterkey"];
                   NSString* twittersecret = [jsonData objectForKey:@"twittersecret"];
                   
                   
                   NSLog(@"Sendgrid pass %@", sendgridpass);
                   NSLog(@"Sendgrid log %@", sendgridlogin);
                   
                   [[NSUserDefaults standardUserDefaults] setObject:twiliosid forKey:@"twiliosid"];
                   [[NSUserDefaults standardUserDefaults] setObject:twilioauth forKey:@"twilioauthkey"];
                    [[NSUserDefaults standardUserDefaults] setObject:twiliophone forKey:@"twiliofromnumber"];
                   [[NSUserDefaults standardUserDefaults] setObject:sendgridlogin forKey:@"username"];
                   [[NSUserDefaults standardUserDefaults] setObject:sendgridpass forKey:@"password"];
                    [[NSUserDefaults standardUserDefaults] setObject:twitterkey forKey:@"consumerkey"];
                   [[NSUserDefaults standardUserDefaults] setObject:twittersecret forKey:@"consumersecret"];
                   [[NSUserDefaults standardUserDefaults] synchronize];
                   
              
                   
                   NSLog(@"My success %ld",(long)success);
                   
                   
                   if ([urlData length] == 0)
                   {
                       
                       [self showhaveyou2:CNPPopupStyleCentered];
                       
                       
                      
                   }
                   
                   else if(success == 0)
                   {
                       
                       
                       
                       //        NSInteger x = [[NSUserDefaults standardUserDefaults] integerForKey:@"registered"];
                       
                       NSString *isgood = @"YESWATERMARK";
                       [[NSUserDefaults standardUserDefaults] setObject:isgood forKey:@"watermark"];
                       [[NSUserDefaults standardUserDefaults] synchronize];
                       
                       
                       
                       UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                       TSPAddToDoViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"PhotoBoothAdmin"];
                       [vc setManagedObjectContext:self.managedObjectContext];
                       
                       [vc setModalPresentationStyle:UIModalPresentationFullScreen];
                       [self presentViewController:vc animated:YES completion:nil];
                       
                       
                       
                       NSLog (@"YES IT WORKS");
                       
                   } else {
                       
                       
                       
                       if ([ipixid isEqualToString:savedValue] || [ipixid1 isEqualToString:savedValue] || [ipixid2 isEqualToString:savedValue] || [ipixid3 isEqualToString:savedValue] || [ipixid4 isEqualToString:savedValue] || [ipixid5 isEqualToString:savedValue])
                           
                       {
                           NSString *isgood = @"NOWATERMARK";
                           [[NSUserDefaults standardUserDefaults] setObject:isgood forKey:@"watermark"];
                           [[NSUserDefaults standardUserDefaults] synchronize];
                           
                           
                           
                           
                           UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                           TSPAddToDoViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"PhotoBoothAdmin"];
                           [vc setManagedObjectContext:self.managedObjectContext];
                           
                           [vc setModalPresentationStyle:UIModalPresentationFullScreen];
                           [self presentViewController:vc animated:YES completion:nil];
                           
                           NSLog(@"YES MY IPIX ID IS VERIFIED");
                       }
                       
                       else
                       {
                           
                           [self showhaveyou1:CNPPopupStyleCentered];
                          
                       }
                       
                       
                       
                       
                       
                   }
               }
               else
               {
                   
                   
                   
               }
           }
        }
    @catch (NSException * e) {
        
    }
    
    
    // [txtUsername resignFirstResponder];
    
    
    [[UIApplication sharedApplication] resignFirstResponder];
    
    
}

    
    
    
    

-(IBAction)Move1:(id)sender
{
   
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString* strValue =[defaults objectForKey:@"ipixemail"];
    NSLog(@"Email subject %@", strValue);
    
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:[[NSBundle mainBundle] bundleIdentifier]];
    
    NSLog(@"UUID IS %@", savedValue);
    @try {
        if([strValue isEqualToString:@""] || strValue == nil ) {
            
            
            [self showhaveyouanim:CNPPopupStyleCentered];
            
            
        }
        
        
        
        
        else {
            NSString *post =[[NSString alloc] initWithFormat:@"email=%@", strValue];
            NSLog(@"PostData: %@",post);
            NSURL *url1=[NSURL URLWithString:@"http://www.ipixsocial.com/membership/view/front/cache/test/getresult.php"];
            
            NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
            
            NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
            
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
            [request setURL:url1];
            [request setHTTPMethod:@"POST"];
            [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
            [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            [request setHTTPBody:postData];
            
            NSError *error = [[NSError alloc] init];
            NSHTTPURLResponse *response = nil;
            NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            
            NSLog(@"Response code: %ld", (long)[response statusCode]);
            if ([response statusCode] >=200 && [response statusCode] <300)
            {
                NSString *responseData = [[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
                NSLog(@"JSON Response is %@", responseData);
                
                
                SBJsonParser *jsonParser = [SBJsonParser new];
                NSDictionary *jsonData = (NSDictionary *) [jsonParser objectWithString:responseData error:nil];
                //   NSString *username = [(NSString *) [jsonData objectForKey:@"email"]init];
                NSInteger success = [(NSNumber *) [jsonData objectForKey:@"uid"] integerValue];
                NSString* ipixid = [jsonData objectForKey:@"ipixid"];
                NSString* ipixid1 = [jsonData objectForKey:@"ipixid1"];
                NSString* ipixid2 = [jsonData objectForKey:@"ipixid2"];
                NSString* ipixid3 = [jsonData objectForKey:@"ipixid3"];
                NSString* ipixid4 = [jsonData objectForKey:@"ipixid4"];
                NSString* ipixid5 = [jsonData objectForKey:@"ipixid5"];
                
                NSString* twiliosid = [jsonData objectForKey:@"twiliosid"];
                NSString* twilioauth = [jsonData objectForKey:@"twilioauth"];
                NSString* twiliophone = [jsonData objectForKey:@"twiliophone"];
                NSString* sendgridlogin = [jsonData objectForKey:@"sendgridlogin"];
                NSString* sendgridpass = [jsonData objectForKey:@"sendgridpass"];
                NSString* twitterkey = [jsonData objectForKey:@"twitterkey"];
                NSString* twittersecret = [jsonData objectForKey:@"twittersecret"];
                
                [[NSUserDefaults standardUserDefaults] setObject:twiliosid forKey:@"twiliosid"];
                [[NSUserDefaults standardUserDefaults] setObject:twilioauth forKey:@"twilioauthkey"];
                [[NSUserDefaults standardUserDefaults] setObject:twiliophone forKey:@"twiliofromnumber"];
                [[NSUserDefaults standardUserDefaults] setObject:sendgridlogin forKey:@"username"];
                [[NSUserDefaults standardUserDefaults] setObject:sendgridpass forKey:@"password"];
                [[NSUserDefaults standardUserDefaults] setObject:twitterkey forKey:@"consumerkey"];
                [[NSUserDefaults standardUserDefaults] setObject:twittersecret forKey:@"consumersecret"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                
                
                NSLog(@"My success %ld",(long)success);
                
                
                if ([urlData length] == 0)
                {
                    
                    [self showhaveyouanim2:CNPPopupStyleCentered];
                    
                    
                    
                }
                
                else if(success == 0)
                {
                    
                    
                    
                    //        NSInteger x = [[NSUserDefaults standardUserDefaults] integerForKey:@"registered"];
                    
                    NSString *isgood = @"YESWATERMARK";
                    [[NSUserDefaults standardUserDefaults] setObject:isgood forKey:@"watermark"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    
                    
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    TSPAddToDoViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"AnimAdmin"];
                    [vc setManagedObjectContext:self.managedObjectContext];
                    
                    [vc setModalPresentationStyle:UIModalPresentationFullScreen];
                    [self presentViewController:vc animated:YES completion:nil];
                    
                    
                    
                    NSLog (@"YES IT WORKS");
                    
                } else {
                    
                    
                    
                    if ([ipixid isEqualToString:savedValue] || [ipixid1 isEqualToString:savedValue] || [ipixid2 isEqualToString:savedValue] || [ipixid3 isEqualToString:savedValue] || [ipixid4 isEqualToString:savedValue] || [ipixid5 isEqualToString:savedValue])
                        
                    {
                        NSString *isgood = @"NOWATERMARK";
                        [[NSUserDefaults standardUserDefaults] setObject:isgood forKey:@"watermark"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                        
                        
                        
                        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        TSPAddToDoViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"AnimAdmin"];
                        [vc setManagedObjectContext:self.managedObjectContext];
                        
                        [vc setModalPresentationStyle:UIModalPresentationFullScreen];
                        [self presentViewController:vc animated:YES completion:nil];
                        
                        NSLog(@"YES MY IPIX ID IS VERIFIED");
                    }
                    
                    else
                    {
                        
                        [self showhaveyouanim1:CNPPopupStyleCentered];
                        
                    }
                    
                    
                    
                    
                    
                }
            }
            else
            {
                
                
                
            }
        }
    }
    @catch (NSException * e) {
        
    }
    
    
    // [txtUsername resignFirstResponder];
    
    
    [[UIApplication sharedApplication] resignFirstResponder];
    
    
}


-(IBAction)sharing:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    TSPAddToDoViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"SharingAdmin"];
    [vc setManagedObjectContext:self.managedObjectContext];
    
    [vc setModalPresentationStyle:UIModalPresentationFullScreen];
    [self presentViewController:vc animated:YES completion:nil];
}


- (void)showhaveyou:(CNPPopupStyle)popupStyle {
    
   

    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"isInfoIcon@2x.png"]];
    imageView.frame = CGRectMake(0, 0, 50, 50);
    imageView.image = [imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [imageView setTintColor:[UIColor darkGrayColor]];
    
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSAttributedString *title = [[NSAttributedString alloc] initWithString:@"HAVE YOU REGISTERED?" attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:25], NSForegroundColorAttributeName : [UIColor darkGrayColor], NSParagraphStyleAttributeName : paragraphStyle}];

    
    NSAttributedString *lineOne = [[NSAttributedString alloc] initWithString:@"We can't seem to validate your account. You may create an event by pressing on Sample Event or Register to setup your account. Internet access is needed to create new events so please check to make sure you are connected." attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:18], NSParagraphStyleAttributeName : paragraphStyle}];
  
     UIView *customView4 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 360, 70)];
    CNPPopupButton *button = [[CNPPopupButton alloc] initWithFrame:CGRectMake(0, 20, 170, 50)];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button setTitle:@"SAMPLE EVENT" forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.backgroundColor = [UIColor lightGrayColor];
    button.layer.cornerRadius = 0;
    button.selectionHandler = ^(CNPPopupButton *button){
        [self.popupController dismissPopupControllerAnimated:YES];
        NSLog(@"Block for button: %@", button.titleLabel.text);
        NSString *isgood = @"YESWATERMARK";
        [[NSUserDefaults standardUserDefaults] setObject:isgood forKey:@"watermark"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        NSLog(@"IS GOOD %@", isgood);
        [[UIApplication sharedApplication] resignFirstResponder];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        TSPAddToDoViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"PhotoBoothAdmin"];
        [vc setManagedObjectContext:self.managedObjectContext];
        
        [vc setModalPresentationStyle:UIModalPresentationFullScreen];
        [self presentViewController:vc animated:NO completion:nil];

    };
    
    
    CNPPopupButton *button1 = [[CNPPopupButton alloc] initWithFrame:CGRectMake(180, 20, 170, 50)];
    [button1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button1.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button1 setTitle:@"REGISTER" forState:UIControlStateNormal];
    [button1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
     button1.backgroundColor = [UIColor colorWithRed:206.0/255.0 green:188.0/255.0 blue:178.0/255.0 alpha:1.0];
    button1.layer.cornerRadius = 0;
    button1.selectionHandler = ^(CNPPopupButton *button1){
        [self.popupController dismissPopupControllerAnimated:YES];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        TSPViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"RegisterAdmin"];
        [vc setManagedObjectContext:self.managedObjectContext];
        
        [vc setModalPresentationStyle:UIModalPresentationFullScreen];
        [self presentViewController:vc animated:NO completion:nil];
        
    };

    [customView4 addSubview:button];
    [customView4 addSubview:button1];
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.numberOfLines = 0;
    titleLabel.attributedText = title;
    
    UILabel *lineOneLabel = [[UILabel alloc] init];
    lineOneLabel.numberOfLines = 0;
    lineOneLabel.attributedText = lineOne;
    

    
    self.popupController = [[CNPPopupController alloc] initWithContents:@[imageView, titleLabel, lineOneLabel, customView4]];
    self.popupController.theme = [CNPPopupTheme defaultTheme];
    self.popupController.theme.popupStyle = popupStyle;
    self.popupController.delegate = self;
    [self.popupController presentPopupControllerAnimated:YES];
}



- (void)showhaveyou1:(CNPPopupStyle)popupStyle {
    


    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"isInfoIcon@2x.png"]];
    imageView.frame = CGRectMake(0, 0, 50, 50);
    imageView.image = [imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [imageView setTintColor:[UIColor darkGrayColor]];
    
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSAttributedString *title = [[NSAttributedString alloc] initWithString:@"YOUR IPIX ID IS INVALID" attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:25], NSForegroundColorAttributeName : [UIColor darkGrayColor], NSParagraphStyleAttributeName : paragraphStyle}];
    
    
    NSAttributedString *lineOne = [[NSAttributedString alloc] initWithString:@"The iPIX ID for this device does not appear to be valid. Please press on Register and verify this is the same ID as you have entered in your online account or you may continue by pressing on Sample Event." attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:18], NSParagraphStyleAttributeName : paragraphStyle}];
    
    UIView *customView4 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 360, 70)];
    CNPPopupButton *button = [[CNPPopupButton alloc] initWithFrame:CGRectMake(0, 20, 170, 50)];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button setTitle:@"SAMPLE EVENT" forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.backgroundColor = [UIColor lightGrayColor];
    button.layer.cornerRadius = 0;
    button.selectionHandler = ^(CNPPopupButton *button){
        [self.popupController dismissPopupControllerAnimated:YES];
        NSLog(@"Block for button: %@", button.titleLabel.text);
        NSString *isgood = @"YESWATERMARK";
        [[NSUserDefaults standardUserDefaults] setObject:isgood forKey:@"watermark"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        NSLog(@"IS GOOD %@", isgood);
        [[UIApplication sharedApplication] resignFirstResponder];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        TSPAddToDoViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"PhotoBoothAdmin"];
        [vc setManagedObjectContext:self.managedObjectContext];
        
        [vc setModalPresentationStyle:UIModalPresentationFullScreen];
        [self presentViewController:vc animated:NO completion:nil];
        
    };
    
    
    CNPPopupButton *button1 = [[CNPPopupButton alloc] initWithFrame:CGRectMake(180, 20, 170, 50)];
    [button1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button1.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button1 setTitle:@"REGISTER" forState:UIControlStateNormal];
    [button1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button1.backgroundColor = [UIColor colorWithRed:206.0/255.0 green:188.0/255.0 blue:178.0/255.0 alpha:1.0];
    button1.layer.cornerRadius = 0;
    button1.selectionHandler = ^(CNPPopupButton *button1){
        [self.popupController dismissPopupControllerAnimated:YES];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        TSPViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"RegisterAdmin"];
        [vc setManagedObjectContext:self.managedObjectContext];
        
        [vc setModalPresentationStyle:UIModalPresentationFullScreen];
        [self presentViewController:vc animated:NO completion:nil];
        
    };
    
    [customView4 addSubview:button];
    [customView4 addSubview:button1];
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.numberOfLines = 0;
    titleLabel.attributedText = title;
    
    UILabel *lineOneLabel = [[UILabel alloc] init];
    lineOneLabel.numberOfLines = 0;
    lineOneLabel.attributedText = lineOne;
    
    
    
    self.popupController = [[CNPPopupController alloc] initWithContents:@[imageView, titleLabel, lineOneLabel, customView4]];
    self.popupController.theme = [CNPPopupTheme defaultTheme];
    self.popupController.theme.popupStyle = popupStyle;
    self.popupController.delegate = self;
    [self.popupController presentPopupControllerAnimated:YES];
}



- (void)showhaveyou2:(CNPPopupStyle)popupStyle {
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"isInfoIcon@2x.png"]];
    imageView.frame = CGRectMake(0, 0, 50, 50);
    imageView.image = [imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [imageView setTintColor:[UIColor darkGrayColor]];
    
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSAttributedString *title = [[NSAttributedString alloc] initWithString:@"YOUR EMAIL SEEMS WRONG" attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:25], NSForegroundColorAttributeName : [UIColor darkGrayColor], NSParagraphStyleAttributeName : paragraphStyle}];
    
    
    NSAttributedString *lineOne = [[NSAttributedString alloc] initWithString:@"It appears that your registered email is not valid. Go to Register to make sure your email is correct. Also, make sure you have internet. Or you may continue by pressing on Sample Event" attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:18], NSParagraphStyleAttributeName : paragraphStyle}];
    
    UIView *customView4 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 360, 70)];
    CNPPopupButton *button = [[CNPPopupButton alloc] initWithFrame:CGRectMake(0, 20, 170, 50)];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button setTitle:@"SAMPLE EVENT" forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.backgroundColor = [UIColor lightGrayColor];
    button.layer.cornerRadius = 0;
    button.selectionHandler = ^(CNPPopupButton *button){
        [self.popupController dismissPopupControllerAnimated:YES];
        NSLog(@"Block for button: %@", button.titleLabel.text);
        NSString *isgood = @"YESWATERMARK";
        [[NSUserDefaults standardUserDefaults] setObject:isgood forKey:@"watermark"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        NSLog(@"IS GOOD %@", isgood);
        [[UIApplication sharedApplication] resignFirstResponder];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        TSPAddToDoViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"PhotoBoothAdmin"];
        [vc setManagedObjectContext:self.managedObjectContext];
        
        [vc setModalPresentationStyle:UIModalPresentationFullScreen];
        [self presentViewController:vc animated:NO completion:nil];
        
    };
    
    
    CNPPopupButton *button1 = [[CNPPopupButton alloc] initWithFrame:CGRectMake(180, 20, 170, 50)];
    [button1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button1.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button1 setTitle:@"REGISTER" forState:UIControlStateNormal];
    [button1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button1.backgroundColor = [UIColor colorWithRed:206.0/255.0 green:188.0/255.0 blue:178.0/255.0 alpha:1.0];
    button1.layer.cornerRadius = 0;
    button1.selectionHandler = ^(CNPPopupButton *button1){
        [self.popupController dismissPopupControllerAnimated:YES];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        TSPViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"RegisterAdmin"];
        [vc setManagedObjectContext:self.managedObjectContext];
        
        [vc setModalPresentationStyle:UIModalPresentationFullScreen];
        [self presentViewController:vc animated:NO completion:nil];
        
    };
    
    [customView4 addSubview:button];
    [customView4 addSubview:button1];
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.numberOfLines = 0;
    titleLabel.attributedText = title;
    
    UILabel *lineOneLabel = [[UILabel alloc] init];
    lineOneLabel.numberOfLines = 0;
    lineOneLabel.attributedText = lineOne;
    
    
    
    self.popupController = [[CNPPopupController alloc] initWithContents:@[imageView, titleLabel, lineOneLabel, customView4]];
    self.popupController.theme = [CNPPopupTheme defaultTheme];
    self.popupController.theme.popupStyle = popupStyle;
    self.popupController.delegate = self;
    [self.popupController presentPopupControllerAnimated:YES];
}



- (void)showhaveyouanim:(CNPPopupStyle)popupStyle {
    
    
    
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"isInfoIcon@2x.png"]];
    imageView.frame = CGRectMake(0, 0, 50, 50);
    imageView.image = [imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [imageView setTintColor:[UIColor darkGrayColor]];
    
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSAttributedString *title = [[NSAttributedString alloc] initWithString:@"HAVE YOU REGISTERED?" attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:25], NSForegroundColorAttributeName : [UIColor darkGrayColor], NSParagraphStyleAttributeName : paragraphStyle}];
    
    
    NSAttributedString *lineOne = [[NSAttributedString alloc] initWithString:@"We can't seem to validate your account. You may create an event by pressing on Sample Event or Register to setup your account. Internet access is needed to create new events so please check to make sure you are connected." attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:18], NSParagraphStyleAttributeName : paragraphStyle}];
    
    UIView *customView4 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 360, 70)];
    CNPPopupButton *button = [[CNPPopupButton alloc] initWithFrame:CGRectMake(0, 20, 170, 50)];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button setTitle:@"SAMPLE EVENT" forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.backgroundColor = [UIColor lightGrayColor];
    button.layer.cornerRadius = 0;
    button.selectionHandler = ^(CNPPopupButton *button){
        [self.popupController dismissPopupControllerAnimated:YES];
        NSLog(@"Block for button: %@", button.titleLabel.text);
        NSString *isgood = @"YESWATERMARK";
        [[NSUserDefaults standardUserDefaults] setObject:isgood forKey:@"watermark"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        NSLog(@"IS GOOD %@", isgood);
        [[UIApplication sharedApplication] resignFirstResponder];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        TSPAddToDoViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"AnimAdmin"];
        [vc setManagedObjectContext:self.managedObjectContext];
        [vc setModeWorking:ANIMATION_GIF];
        
        [vc setModalPresentationStyle:UIModalPresentationFullScreen];
        [self presentViewController:vc animated:NO completion:nil];
        
    };
    
    
    CNPPopupButton *button1 = [[CNPPopupButton alloc] initWithFrame:CGRectMake(180, 20, 170, 50)];
    [button1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button1.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button1 setTitle:@"REGISTER" forState:UIControlStateNormal];
    [button1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button1.backgroundColor = [UIColor colorWithRed:206.0/255.0 green:188.0/255.0 blue:178.0/255.0 alpha:1.0];
    button1.layer.cornerRadius = 0;
    button1.selectionHandler = ^(CNPPopupButton *button1){
        [self.popupController dismissPopupControllerAnimated:YES];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        TSPViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"RegisterAdmin"];
        [vc setManagedObjectContext:self.managedObjectContext];
        
        [vc setModalPresentationStyle:UIModalPresentationFullScreen];
        [self presentViewController:vc animated:NO completion:nil];
        
    };
    
    [customView4 addSubview:button];
    [customView4 addSubview:button1];
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.numberOfLines = 0;
    titleLabel.attributedText = title;
    
    UILabel *lineOneLabel = [[UILabel alloc] init];
    lineOneLabel.numberOfLines = 0;
    lineOneLabel.attributedText = lineOne;
    
    
    
    self.popupController = [[CNPPopupController alloc] initWithContents:@[imageView, titleLabel, lineOneLabel, customView4]];
    self.popupController.theme = [CNPPopupTheme defaultTheme];
    self.popupController.theme.popupStyle = popupStyle;
    self.popupController.delegate = self;
    [self.popupController presentPopupControllerAnimated:YES];
}



- (void)showhaveyouanim1:(CNPPopupStyle)popupStyle {
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"isInfoIcon@2x.png"]];
    imageView.frame = CGRectMake(0, 0, 50, 50);
    imageView.image = [imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [imageView setTintColor:[UIColor darkGrayColor]];
    
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSAttributedString *title = [[NSAttributedString alloc] initWithString:@"YOUR IPIX ID IS INVALID" attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:25], NSForegroundColorAttributeName : [UIColor darkGrayColor], NSParagraphStyleAttributeName : paragraphStyle}];
    
    
    NSAttributedString *lineOne = [[NSAttributedString alloc] initWithString:@"The iPIX ID for this device does not appear to be valid. Please press on Register and verify this is the same ID as you have entered in your online account or you may continue by pressing on Sample Event." attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:18], NSParagraphStyleAttributeName : paragraphStyle}];
    
    UIView *customView4 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 360, 70)];
    CNPPopupButton *button = [[CNPPopupButton alloc] initWithFrame:CGRectMake(0, 20, 170, 50)];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button setTitle:@"SAMPLE EVENT" forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.backgroundColor = [UIColor lightGrayColor];
    button.layer.cornerRadius = 0;
    button.selectionHandler = ^(CNPPopupButton *button){
        [self.popupController dismissPopupControllerAnimated:YES];
        NSLog(@"Block for button: %@", button.titleLabel.text);
        NSString *isgood = @"YESWATERMARK";
        [[NSUserDefaults standardUserDefaults] setObject:isgood forKey:@"watermark"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        NSLog(@"IS GOOD %@", isgood);
        [[UIApplication sharedApplication] resignFirstResponder];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        TSPAddToDoViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"AnimAdmin"];
        [vc setManagedObjectContext:self.managedObjectContext];
        
        [vc setModalPresentationStyle:UIModalPresentationFullScreen];
        [self presentViewController:vc animated:NO completion:nil];
        
    };
    
    
    CNPPopupButton *button1 = [[CNPPopupButton alloc] initWithFrame:CGRectMake(180, 20, 170, 50)];
    [button1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button1.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button1 setTitle:@"REGISTER" forState:UIControlStateNormal];
    [button1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button1.backgroundColor = [UIColor colorWithRed:206.0/255.0 green:188.0/255.0 blue:178.0/255.0 alpha:1.0];
    button1.layer.cornerRadius = 0;
    button1.selectionHandler = ^(CNPPopupButton *button1){
        [self.popupController dismissPopupControllerAnimated:YES];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        TSPViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"RegisterAdmin"];
        [vc setManagedObjectContext:self.managedObjectContext];
        
        [vc setModalPresentationStyle:UIModalPresentationFullScreen];
        [self presentViewController:vc animated:NO completion:nil];
        
    };
    
    [customView4 addSubview:button];
    [customView4 addSubview:button1];
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.numberOfLines = 0;
    titleLabel.attributedText = title;
    
    UILabel *lineOneLabel = [[UILabel alloc] init];
    lineOneLabel.numberOfLines = 0;
    lineOneLabel.attributedText = lineOne;
    
    
    
    self.popupController = [[CNPPopupController alloc] initWithContents:@[imageView, titleLabel, lineOneLabel, customView4]];
    self.popupController.theme = [CNPPopupTheme defaultTheme];
    self.popupController.theme.popupStyle = popupStyle;
    self.popupController.delegate = self;
    [self.popupController presentPopupControllerAnimated:YES];

}



- (void)showhaveyouanim2:(CNPPopupStyle)popupStyle {
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"isInfoIcon@2x.png"]];
    imageView.frame = CGRectMake(0, 0, 50, 50);
    imageView.image = [imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [imageView setTintColor:[UIColor darkGrayColor]];
    
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSAttributedString *title = [[NSAttributedString alloc] initWithString:@"YOUR EMAIL SEEMS WRONG" attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:25], NSForegroundColorAttributeName : [UIColor darkGrayColor], NSParagraphStyleAttributeName : paragraphStyle}];
    
    
    NSAttributedString *lineOne = [[NSAttributedString alloc] initWithString:@"It appears that your registered email is not valid. Go to Register to make sure your email is correct. Also, make sure you have internet. Or you may continue by pressing on Sample Event" attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:18], NSParagraphStyleAttributeName : paragraphStyle}];
    
    UIView *customView4 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 360, 70)];
    CNPPopupButton *button = [[CNPPopupButton alloc] initWithFrame:CGRectMake(0, 20, 170, 50)];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button setTitle:@"SAMPLE EVENT" forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.backgroundColor = [UIColor lightGrayColor];
    button.layer.cornerRadius = 0;
    button.selectionHandler = ^(CNPPopupButton *button){
        [self.popupController dismissPopupControllerAnimated:YES];
        NSLog(@"Block for button: %@", button.titleLabel.text);
        NSString *isgood = @"YESWATERMARK";
        [[NSUserDefaults standardUserDefaults] setObject:isgood forKey:@"watermark"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        NSLog(@"IS GOOD %@", isgood);
        [[UIApplication sharedApplication] resignFirstResponder];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        TSPAddToDoViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"AnimAdmin"];
        [vc setManagedObjectContext:self.managedObjectContext];
        
        [vc setModalPresentationStyle:UIModalPresentationFullScreen];
        [self presentViewController:vc animated:NO completion:nil];
        
    };
    
    
    CNPPopupButton *button1 = [[CNPPopupButton alloc] initWithFrame:CGRectMake(180, 20, 170, 50)];
    [button1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button1.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button1 setTitle:@"REGISTER" forState:UIControlStateNormal];
    [button1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button1.backgroundColor = [UIColor colorWithRed:206.0/255.0 green:188.0/255.0 blue:178.0/255.0 alpha:1.0];
    button1.layer.cornerRadius = 0;
    button1.selectionHandler = ^(CNPPopupButton *button1){
        [self.popupController dismissPopupControllerAnimated:YES];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        TSPViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"RegisterAdmin"];
        [vc setManagedObjectContext:self.managedObjectContext];
        
        [vc setModalPresentationStyle:UIModalPresentationFullScreen];
        [self presentViewController:vc animated:NO completion:nil];
        
    };
    
    [customView4 addSubview:button];
    [customView4 addSubview:button1];
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.numberOfLines = 0;
    titleLabel.attributedText = title;
    
    UILabel *lineOneLabel = [[UILabel alloc] init];
    lineOneLabel.numberOfLines = 0;
    lineOneLabel.attributedText = lineOne;
    
    
    
    self.popupController = [[CNPPopupController alloc] initWithContents:@[imageView, titleLabel, lineOneLabel, customView4]];
    self.popupController.theme = [CNPPopupTheme defaultTheme];
    self.popupController.theme.popupStyle = popupStyle;
    self.popupController.delegate = self;
    [self.popupController presentPopupControllerAnimated:YES];
}






#pragma mark - CNPPopupController Delegate


#pragma mark - event response

-(IBAction)cancel:(id)sender
{
UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
TSPViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"Home"];
[vc setManagedObjectContext:self.managedObjectContext];
    
    [vc setModalPresentationStyle:UIModalPresentationFullScreen];
    [self presentViewController:vc animated:YES completion:nil];

}
-(IBAction)sendemail:(id)sender
{
    MFMailComposeViewController* composeVC = [[MFMailComposeViewController alloc] init];
    composeVC.mailComposeDelegate = self;
    
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:[[NSBundle mainBundle] bundleIdentifier]];
    
    NSString *valueToSave = ipixemailaddress.text;
    
        NSArray *usersTo = [NSArray arrayWithObject: [NSString stringWithFormat:@"%@", valueToSave]];
    
     [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"ipixemail"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
  
   [composeVC setToRecipients:usersTo];
   [composeVC setSubject:@"Your iPIX ID Number"];
 [composeVC setMessageBody: [NSString stringWithFormat:@"Please log in to your iPIX account and then copy and paste this ID into the iPIX ID field under your profile tab. %@", savedValue] isHTML:NO];
  
  
    [self presentViewController:composeVC animated:YES completion:nil];

}



-(void)mailComposeController:(MFMailComposeViewController *)composeVC didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    
    if (result == MFMailComposeResultSent) {
      
            [composeVC dismissViewControllerAnimated:YES completion:nil];
            [ipixemailaddress resignFirstResponder];
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        TSPViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"Home"];
        [vc setManagedObjectContext:self.managedObjectContext];
        
        [vc setModalPresentationStyle:UIModalPresentationFullScreen];
        [self presentViewController:vc animated:NO completion:nil];
            
    }
    
    else if (result == MFMailComposeResultCancelled) {
        NSString *valueToSave = ipixemailaddress.text;

        [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"ipixemail"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [ipixemailaddress resignFirstResponder];
        [composeVC dismissViewControllerAnimated:YES completion:nil];
        
    }
  
    
    
    
    
}

- (void)viewWillAppear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}


- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}


- (void)keyboardWillShow:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = -keyboardSize.height;
        self.view.frame = f;
    }];
}

-(void)keyboardWillHide:(NSNotification *)notification
{
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = 0.0f;
        self.view.frame = f;
    }];
}


-(IBAction)goback:(id)sender

{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    TSPViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"Home"];
    [vc setManagedObjectContext:self.managedObjectContext];
    
    [vc setModalPresentationStyle:UIModalPresentationFullScreen];
    [self presentViewController:vc animated:NO completion:nil];
}


#pragma mark -
#pragma mark Fetched Results Controller Delegate Methods
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath
{
    switch (type)
    {
        case NSFetchedResultsChangeInsert:
        {
            [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        }
        case NSFetchedResultsChangeDelete:
        {
            [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        }
        case NSFetchedResultsChangeUpdate:
        {
//            [self configureCell:(TSPToDoCell *)[self.tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
        }
        case NSFetchedResultsChangeMove:
        {
            [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        }
    }
}

#pragma mark -
#pragma mark Table View Data Source Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSLog(@"SECTION: %u", [[self.fetchedResultsController sections] count]);
    
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *sections = [self.fetchedResultsController sections];
    id<NSFetchedResultsSectionInfo> sectionInfo = [sections objectAtIndex:section];
    
    NSLog(@"ROWS: %lu", (unsigned long)[sectionInfo numberOfObjects]);
    
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TSPToDoCell *cell = (TSPToDoCell *)[tableView dequeueReusableCellWithIdentifier:@"ToDoCell" forIndexPath:indexPath];
    
    // Configure Table View Cell
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(TSPToDoCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    // Fetch Record
    NSManagedObject *record = [self.fetchedResultsController objectAtIndexPath:indexPath];
    [self setSelection:indexPath];
    NSLog(@"INDEXPATH: %ld", (long)indexPath.row);
    
    // Update Cell
    [cell.eventnameLabel setText:[record valueForKey:@"eventname"]];
    [cell.doneButton setSelected:[[record valueForKey:@"done"] boolValue]];
    [cell.doneButton2 setTitle: @"EXPORT DATA" forState:UIControlStateNormal];
   
    
    NSString *docPath =[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0];
    //resultView.text = docPath;
    NSString *eventname = [record valueForKey:@"eventname"];
    NSString *surveys=[docPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_emails.csv", eventname]];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath: surveys])
    {
        NSFileHandle *fileHandle = [NSFileHandle fileHandleForReadingAtPath:surveys];
        NSString *surveyResults=[[NSString alloc]initWithData:[fileHandle availableData] encoding:NSUTF8StringEncoding];
        [fileHandle closeFile];
        self.resultView.text=surveyResults;
    }
    
    else
    {
        cell.doneButton2.hidden = YES;
    }
    
    
    [cell setDidTapButtonBlock2:^{
        
   
        
        
        MFMailComposeViewController* composeVC1 = [[MFMailComposeViewController alloc] init];
        composeVC1.mailComposeDelegate = self;
        
        
        [composeVC1 setSubject:@"CSV File"];
        [composeVC1 addAttachmentData:[NSData dataWithContentsOfFile:surveys]
                             mimeType:@"text/csv"
                             fileName:[NSString stringWithFormat:@"%@_emails.csv", eventname]];
        
        
        [self presentViewController:composeVC1 animated:YES completion:nil];

        
    }];
    
    [cell setDidTapButtonBlock:^{
        
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"isInfoIcon@2x.png"]];
        imageView.frame = CGRectMake(0, 0, 50, 50);
        imageView.image = [imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [imageView setTintColor:[UIColor darkGrayColor]];
        
        NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
        paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
        paragraphStyle.alignment = NSTextAlignmentCenter;
        
        NSAttributedString *title = [[NSAttributedString alloc] initWithString:@"ARE YOU SURE" attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:25], NSForegroundColorAttributeName : [UIColor darkGrayColor], NSParagraphStyleAttributeName : paragraphStyle}];
        NSAttributedString *lineOne = [[NSAttributedString alloc] initWithString:@"Are you sure? Once your event is deleted it cannot be recovered." attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:18], NSParagraphStyleAttributeName : paragraphStyle}];
        
        
        CNPPopupButton *button = [[CNPPopupButton alloc] initWithFrame:CGRectMake(0, 0, 300, 50)];
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        button.titleLabel.font = [UIFont boldSystemFontOfSize:18];
        [button setTitle:@"CANCEL" forState:UIControlStateNormal];
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        button.backgroundColor = [UIColor lightGrayColor];
        button.layer.cornerRadius = 10;
        button.selectionHandler = ^(CNPPopupButton *button){
            [self.popupController dismissPopupControllerAnimated:YES];
       
            
        };
        
        
        CNPPopupButton *button1 = [[CNPPopupButton alloc] initWithFrame:CGRectMake(0, 0, 300, 50)];
        [button1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        button1.titleLabel.font = [UIFont boldSystemFontOfSize:18];
        [button1 setTitle:@"DELETE" forState:UIControlStateNormal];
        [button1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        button1.backgroundColor = [UIColor redColor];
        button1.layer.cornerRadius = 10;
        button1.selectionHandler = ^(CNPPopupButton *button1){
          NSString *docPath =[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0];
            
            NSString *eventname = [record valueForKey:@"eventname"];
            NSString *surveys=[docPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_emails.csv", eventname]];
             if ([[NSFileManager defaultManager] fileExistsAtPath: surveys])
            {
                NSFileManager *fileManager = [NSFileManager defaultManager];
                [fileManager removeItemAtPath:surveys error:NULL];
            }
            
            BOOL isDone = [[record valueForKey:@"done"] boolValue];
            
            // Update Record
            [record setValue:@(!isDone) forKey:@"done"];
            
            NSManagedObject *record = [self.fetchedResultsController objectAtIndexPath:indexPath];
            
            if (record)
            {
                [self.fetchedResultsController.managedObjectContext deleteObject:record];
                //  [self.fetchedResultsController.managedObjectContext reset];
            
                NSError *error = nil;
                if (![self.fetchedResultsController.managedObjectContext save:&error])
                {
                    // handle error
                }
//                [self.tableView reloadData];
                 [self.popupController dismissPopupControllerAnimated:YES];
            }

            
        };
        
        
        
        UILabel *titleLabel = [[UILabel alloc] init];
        titleLabel.numberOfLines = 0;
        titleLabel.attributedText = title;
        
        UILabel *lineOneLabel = [[UILabel alloc] init];
        lineOneLabel.numberOfLines = 0;
        lineOneLabel.attributedText = lineOne;
        
        
        self.popupController = [[CNPPopupController alloc] initWithContents:@[imageView, titleLabel, lineOneLabel, button,  button1]];
        self.popupController.theme.popupStyle = CNPPopupStyleCentered;
        self.popupController.delegate = self;

        [self.popupController presentPopupControllerAnimated:YES];
        
        
        
        
   }];
    
    
    [cell setDidTapButtonBlock1:^{
       
        
        NSString * string1 = [record valueForKey:@"share"];
        
        
        // Update Record
        [record setValue:string1 forKey:@"share"];
        
        
        NSString * string = [record valueForKey:@"booth"];
        
       
        // Update Record
        [record setValue:string forKey:@"booth"];
        NSManagedObject *record = [self.fetchedResultsController objectAtIndexPath:indexPath];
        
        if (record) {
//            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//            TSPAddToDoViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"PhotoBoothAdmin"];
//            [vc setModalPresentationStyle:UIModalPresentationFullScreen];
//            [self presentViewController:vc animated:YES completion:nil];
            if (self.selection) {
                // Fetch Record
                NSManagedObject *record = [self.fetchedResultsController objectAtIndexPath:self.selection];
                
                if (record) {
                    if ([string isEqual: @"YES"])
                    {
                        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        TSPAddToDoViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"PhotoBoothAdmin"];
                        [vc setModalPresentationStyle:UIModalPresentationFullScreen];
                        [self presentViewController:vc animated:YES completion:nil];
                        [vc setRecord:record];
                    }
                    
                    else
                    {
                        NSLog (@"SWITCH IS OFF");
                        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        TSPAddToDoViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"AnimAdmin"];
                        [vc setModalPresentationStyle:UIModalPresentationFullScreen];
                        [self presentViewController:vc animated:YES completion:nil];
                        [vc setRecord:record];
                    }
                    
                    
                    if ([string1 isEqualToString:@"YES"])
                    {
                        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        TSPAddToDoViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"SharingAdmin"];
                        [vc setModalPresentationStyle:UIModalPresentationFullScreen];
                        [self presentViewController:vc animated:YES completion:nil];
                        [vc setRecord:record];
                    }
                    
                    
                }
                
                // Reset Selection
                [self setSelection:nil];
            }
        }

        
        
    
        
    }];

    
    
    
}


- (NSString *)deviceUUID
{
    if([[NSUserDefaults standardUserDefaults] objectForKey:[[NSBundle mainBundle] bundleIdentifier]])
        return [[NSUserDefaults standardUserDefaults] objectForKey:[[NSBundle mainBundle] bundleIdentifier]];
    
    @autoreleasepool {
        
        CFUUIDRef uuidReference = CFUUIDCreate(nil);
        CFStringRef stringReference = CFUUIDCreateString(nil, uuidReference);
        NSString *uuidString = (__bridge NSString *)(stringReference);
        [[NSUserDefaults standardUserDefaults] setObject:uuidString forKey:[[NSBundle mainBundle] bundleIdentifier]];
        [[NSUserDefaults standardUserDefaults] synchronize];
        CFRelease(uuidReference);
        CFRelease(stringReference);
        
        return uuidString;
    }
}




- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if (editingStyle == UITableViewCellEditingStyleInsert) {
        NSManagedObject *record = [self.fetchedResultsController objectAtIndexPath:indexPath];
        
        if (record) {
         //   [self.fetchedResultsController.managedObjectContext deleteObject:record];
        }
    }
    
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSManagedObject *record = [self.fetchedResultsController objectAtIndexPath:indexPath];
        
        if (record) {
            [self.fetchedResultsController.managedObjectContext deleteObject:record];
        }
    }
    
    
    
}

#pragma mark -
#pragma mark Table View Delegate Methods
- (void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath*)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    NSManagedObject *record = [self.fetchedResultsController objectAtIndexPath:indexPath];
    NSString * string = [record valueForKey:@"booth"];
    NSString * string1 = [record valueForKey:@"share"];
    NSString * string2 = [record valueForKey:@"switchRear"];
    NSLog(@"SHARING IS WHAT? %@", string1);
    if (([string  isEqual: @"YES"]) && ([string2  isEqual: @"NO"]))
    {
        // Store Selection
        [self setSelection:indexPath];
        
        // Perform Segue
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        PhotoBoothNormal *vc = [storyboard instantiateViewControllerWithIdentifier:@"PhotoBoothNormal"];
        [vc setManagedObjectContext:self.managedObjectContext];
        
        if (self.selection)
        {
            // Fetch Record
            NSManagedObject *record = [self.fetchedResultsController objectAtIndexPath:self.selection];
            
            if (record)
            {
                [vc setRecord:record];
            }
            
            // Reset Selection
            [self setSelection:nil];
        }
        
        [vc setModalPresentationStyle:UIModalPresentationFullScreen];
        [self presentViewController:vc animated:NO completion:nil];
    }
    if (([string  isEqual: @"NO"]) && ([string2  isEqual: @"NO"]))
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        AnimatedNormal *vc = [storyboard instantiateViewControllerWithIdentifier:@"AnimatedNormal"];
        [vc setManagedObjectContext:self.managedObjectContext];
        
        if (self.selection)
        {
            // Fetch Record
            NSManagedObject *record = [self.fetchedResultsController objectAtIndexPath:self.selection];
            
            if (record)
            {
                [vc setRecord:record];
            }
            
            // Reset Selection
            [self setSelection:nil];
        }
        
        [vc setModalPresentationStyle:UIModalPresentationFullScreen];
        [self presentViewController:vc animated:NO completion:nil];
    }
    
    if ([string1  isEqual: @"YES"])
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        SharingNormal *vc = [storyboard instantiateViewControllerWithIdentifier:@"SharingNormal"];
        [vc setManagedObjectContext:self.managedObjectContext];
        
        if (self.selection)
        {
            // Fetch Record
            NSManagedObject *record = [self.fetchedResultsController objectAtIndexPath:self.selection];
            
            if (record)
            {
                [vc setRecord:record];
            }
            
            // Reset Selection
            [self setSelection:nil];
        }
        
        [vc setModalPresentationStyle:UIModalPresentationFullScreen];
        [self presentViewController:vc animated:NO completion:nil];
        
    }
    
    if (([string  isEqual: @"YES"]) && ([string2  isEqual: @"YES"]))
    {
        NSLog (@"REAR PHOTO BOOTH COMING SOON");
    }
    
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    // Tell the keyboard where to go on next / go button.
    if(textField == ipixemailaddress)
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString* strValue =[defaults objectForKey:@"ipixemail"];
        ipixemailaddress.text = strValue;
    }
    
    return YES;
}





-(void)mailComposeController:(MFMailComposeViewController *)composeVC1 didFinishWithResult2:(MFMailComposeResult)result2 error:(NSError *)error
{
    
    if (result2 == MFMailComposeResultSent) {
        @autoreleasepool {
            [composeVC1 dismissViewControllerAnimated:YES completion:nil];
            
            
        }
        
        
        
    }
    
    else if (result2 == MFMailComposeResultCancelled) {
        [composeVC1 dismissViewControllerAnimated:YES completion:nil];
        
    }
    
    
    
    
    
    
}



- (BOOL)prefersStatusBarHidden
{
    return YES;
}

@end
