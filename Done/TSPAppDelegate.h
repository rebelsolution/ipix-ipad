#import <UIKit/UIKit.h>
#import "MCManager.h"
#import <DropboxSDK/DropboxSDK.h>
@class DROPBOX;

@interface TSPAppDelegate : UIResponder <UIApplicationDelegate,DBSessionDelegate>
{
    DROPBOX *box;
}
@property (strong, nonatomic) UIWindow *window;

@property (nonatomic, strong) UIView *customLayoutView;
@property (nonatomic, strong) NSMutableArray *customLayoutArray;
@property (nonatomic) NSInteger customLayoutCount;
@property (nonatomic, assign) BOOL isSmallCustomLayout;
@property (nonatomic, assign) BOOL isSmallCustomLayouth;
@property (nonatomic, assign) BOOL isLargeCustomLayout;
@property (nonatomic, assign) BOOL isLargeCustomLayouth;
@property (nonatomic, assign) BOOL isSplipLayout;

@property (nonatomic, assign) BOOL isSwitchOverlay;

@property (nonatomic, strong) NSData *customLayoutBackground;
@property (nonatomic, strong) UIImage *photoboothlayoutimage;
@property (nonatomic, strong) UIImage *imgCustomBackground;
@property (nonatomic, strong) MCManager *mcManager;
@property (nonatomic, strong) NSMutableArray *arrConnectedDevices;
@property (nonatomic, strong) NSMutableArray *customEvents;

@property (nonatomic) NSInteger selectedPhotoCount;
@property (nonatomic) NSInteger photoAllowed;
@property (nonatomic) BOOL isSendingEmail;

+ (TSPAppDelegate*) sharedAppDelegate;
- (void)saveCustomLayout;
- (void)getCustomLayout;
- (void)saveCustomLayoutToDefaults;

@end
