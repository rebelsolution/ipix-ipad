//
//  TSPToDoCell.h
//  Done
//
//  Created by Bart Jacobs on 24/07/14.
//  Copyright (c) 2014 Tuts+. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^TSPToDoCellDidTapButtonBlock)();

@interface TSPToDoCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *eventnameLabel;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;
@property (weak, nonatomic) IBOutlet UIButton *doneButton1;
@property (weak, nonatomic) IBOutlet UIButton *doneButton2;
@property (copy, nonatomic) TSPToDoCellDidTapButtonBlock didTapButtonBlock;
@property (copy, nonatomic) TSPToDoCellDidTapButtonBlock didTapButtonBlock1;
@property (copy, nonatomic) TSPToDoCellDidTapButtonBlock didTapButtonBlock2;
@end
