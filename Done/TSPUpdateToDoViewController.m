//
//  TSPUpdateToDoViewController.m
//  Done
//
//  Created by Bart Jacobs on 27/07/14.
//  Copyright (c) 2014 Tuts+. All rights reserved.
//

#import "TSPUpdateToDoViewController.h"

@implementation TSPUpdateToDoViewController

#pragma mark -
#pragma mark View Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.record) {
        // Update Text Field
        [self.textField setText:[self.record valueForKey:@"eventname"]];
         [self.textField1 setText:[self.record valueForKey:@"eventdate"]];
        NSString * string = [self.record valueForKey:@"switchGreen"];
        NSString * string1 = [self.record valueForKey:@"maxSelection1"];
        if ([string  isEqual: @"YES"])
        {
            NSLog (@"SWITCH IS ON");
        }
        
        else
        {
            NSLog (@"SWITCH IS OFF");
        }

        
        NSLog(@"MAX NUMBER IS %@", string1);
    }
}

#pragma mark -
#pragma mark Actions


@end
