
//  PhotoBoothNormal.m
//  iSIT
//
//  Created by Marc Matteo on 10/22/16.
//  Copyright © 2016 Marc Matteo. All rights reserved.
//
@import AVFoundation;

#import <Photos/Photos.h>
#import "TSPViewController.h"
#import "CNPPopupController.h"
#import "PhotoBoothNormal.h"
#import "AVCamManualPreviewView.h"
#import "PHPhotoLibrary+CustomPhotoAlbum.h"
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <CoreMedia/CoreMedia.h>
#import <CoreVideo/CoreVideo.h>
#import <QuartzCore/QuartzCore.h>
#import <CoreImage/CoreImage.h>
#import <ImageIO/ImageIO.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "CustomAlbum.h"
#import "ACSDefaultsUtil.h"
#import "ACSPinController.h"
#import <QuartzCore/QuartzCore.h>
#import <GLKit/GLKit.h>
#import <Social/Social.h>
#import "SendGrid.h"
#import "SendGridEmail.h"
#import "SendGridEmailAttachment.h"
#import "AMPopTip.h"
#import <TwitterKit/TwitterKit.h>
#import "FHSTwitterEngine.h"
#import "WebViewVC.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "AFHTTPRequestOperationManager.h"
#import "LayOutOne.h"
#import "LayOutTwo.h"
#import "LayOutThree.h"
#import "LayOutFour.h"
#import "LayOutFive.h"
#import "LayOutSix.h"
#import "LayOutSeven.h"
#import "LayOutEight.h"
#import "LayOutOne1.h"
#import "LayOutTwo1.h"
#import "LayOutThree1.h"
#import "LayOutFour1.h"
#import "LayOutFive1.h"
#import "LayOutSix1.h"
#import "LayOutSeven1.h"
#import "LayOutEight1.h"
#import "VerLayOutOne.h"
#import "VerLayOutTwo.h"
#import "VerLayOutThree.h"
#import "VerLayOutFour.h"
#import "VerLayOutFive.h"
#import "VerLayOutSix.h"
#import "VerLayOutSeven.h"
#import "VerLayOutEight.h"
#import "VerLayOutOne1.h"
#import "VerLayOutTwo1.h"
#import "VerLayOutThree1.h"
#import "VerLayOutFour1.h"
#import "VerLayOutFive1.h"
#import "VerLayOutSix1.h"
#import "VerLayOutSeven1.h"
#import "VerLayOutEight1.h"
#import "MBProgressHUD.h"
#import "TSPAppDelegate.h"
#import "TSPEventRepository.h"
#import "Rectangle.h"


int arrayValue;
int value;
@import AVKit;

#define DEGREES_TO_RADIANS(degrees) ((degrees) * (M_PI / 180.0))
static void *CapturingStillImageContext = &CapturingStillImageContext;
static void *RecordingContext = &RecordingContext;
static void *SessionRunningAndDeviceAuthorizedContext = &SessionRunningAndDeviceAuthorizedContext;

static void *FocusModeContext = &FocusModeContext;
static void *ExposureModeContext = &ExposureModeContext;
static void *WhiteBalanceModeContext = &WhiteBalanceModeContext;
static void *LensPositionContext = &LensPositionContext;
static void *ExposureDurationContext = &ExposureDurationContext;
static void *ISOContext = &ISOContext;
static void *ExposureTargetOffsetContext = &ExposureTargetOffsetContext;
static void *DeviceWhiteBalanceGainsContext = &DeviceWhiteBalanceGainsContext;

@interface PhotoBoothNormal ()< FHSTwitterEngineAccessTokenDelegate, CNPPopupControllerDelegate, ACSPinControllerDelegate, AVCaptureFileOutputRecordingDelegate, UITextFieldDelegate, MFMailComposeViewControllerDelegate, AVAudioRecorderDelegate>

{
    UIImage *_photo;
    NSString *recentImg;
    NSString *emailmessage1;
    NSString *emailsubject1;
    
    
    
    
    
    //LAYOUTSEND
    IBOutlet UIColor *fontcolor;
    IBOutlet UIColor *backgroundcolor1;
    
    IBOutlet NSString *TextIndividual;
    IBOutlet NSString *AlbumName;
    IBOutlet UIImageView *imgView;
    IBOutlet UIImage *welcomeimage;
    IBOutlet UIImage *getreadyimage;
    IBOutlet UIImage* animatedlayoutimage;
    IBOutlet UIImage* closedimage;
    IBOutlet UIImage* countdownimage;
    IBOutlet UIImage* customimage;
    IBOutlet UIImage* effectsimage;
    IBOutlet UIImage* emailimage;
    IBOutlet UIImage* finalimage;
    IBOutlet UIImage* greenscreenimage;
    IBOutlet UIImage* photoboothlayoutimage;
    IBOutlet UIImage* remainingimage;
    IBOutlet UIImage*  videoimage;
    AVAudioPlayer *_audioPlayer;
    AVAudioPlayer *_audioPlayer1;
    IBOutlet UISwitch *testshot;
    
    IBOutlet UISwitch *islumecube;
    
    IBOutlet UISwitch *pass;
    IBOutlet UIButton *testbutton;
    IBOutlet UIButton *recordyes;
    IBOutlet UIButton *recordno;
    IBOutlet UIButton *recordbutton;
    IBOutlet UIButton *stopbutton;
    IBOutlet UIView *pinclosed;
    IBOutlet UIView *social;
    IBOutlet UIView *socialview;
    
    IBOutlet UIButton *facebook;
    IBOutlet UIButton *twitter;
    IBOutlet UIButton *mail;
    IBOutlet UIButton *texting;
    IBOutlet UIButton *touchtostart;
    IBOutlet UIButton *backout;
    IBOutlet UIButton *soc;
    IBOutlet UIImageView *closed;
    IBOutlet UIImageView *welcome;
    IBOutlet UIImageView *effects;
    IBOutlet UIImageView *video;
    IBOutlet UIImageView *email;
    IBOutlet UIImageView *final;
    IBOutlet UIImageView *countdown;
    IBOutlet UIImageView *remaining;
    IBOutlet UIImageView *getready;
    IBOutlet UIImageView *printlayoutbackground;
    IBOutlet UIImageView *customlayoutbackground;
    IBOutlet UIImageView *green;
    IBOutlet UIView *remainbord;
    IBOutlet UIView *welcomescreen1;
    IBOutlet UIView *mainadmin;
    IBOutlet UIView *mainwelcome;
    IBOutlet UIView *mainready;
    IBOutlet UIView * mainclosed;
    IBOutlet UIView * maincountdown;
    IBOutlet UIView * maincountdown1;
    IBOutlet UIView * maincountdownbg;
    IBOutlet UIView * maincountdownbg1;
    IBOutlet UIView * maineffects;
    IBOutlet UIView *testing;
    IBOutlet UIView * maineffectsbg;
    IBOutlet UIView * maineffectsbg1;
    IBOutlet UIView *mainemail;
    IBOutlet UIView *mainfinal;
    IBOutlet UIView * mainremaining;
    IBOutlet UIView * mainvideo;
    IBOutlet UIView * countdownlook;
    IBOutlet UIView * videotimerview;
    IBOutlet UIView * welcomevideohide;
    IBOutlet UIView * welcomevideowshow;
    IBOutlet UIView *lookhere;
    IBOutlet UIView *lookhere1;
    IBOutlet UIView *press;
    IBOutlet UIView *viewtest;
    IBOutlet UIView *captured;
    IBOutlet UIView *alleffects;
    IBOutlet UIView *bwvin;
    IBOutlet UIView*bwsep;
    IBOutlet UIView *bw;
    IBOutlet UIView*vinsep;
    IBOutlet UIView *vin;
    IBOutlet UIView *sep;
    IBOutlet UIView *col;
    
      IBOutlet UIView *facetest;
    IBOutlet UITextField *textnumber;
    IBOutlet UITextField *emailaddress1;
    IBOutlet UITextField *printip;
    IBOutlet UITextField *printport;
    
    IBOutlet UIButton *SEPBG;
    IBOutlet UIButton *VINBG;
    IBOutlet UIButton *COLORBG;
    IBOutlet UIButton *BWBG;
    IBOutlet UIButton *skippingsocial;
    
    IBOutlet UIButton *sharestation;
    IBOutlet UIButton *photoButtoncolor;
    
    
    IBOutlet UIStackView *effectsstack;
    IBOutlet UIView *welcomescreen;
    IBOutlet UIView *closedscreen;
    IBOutlet UIView *effectsscreen;
    IBOutlet UIView *effectsscreen1;
    IBOutlet UIView *remainingscreen;
    IBOutlet UIView *videoscreen;
    IBOutlet UIView *hidevideoscreen;
    IBOutlet UIView *emailscreen;
    IBOutlet UIView *finalscreen;
    IBOutlet UIView *finalscreen1;
    IBOutlet UIView *finalscreen2;
    IBOutlet UIView *countdownscreen;
    IBOutlet UIView *countdownscreen1;
    IBOutlet UIView *getreadyscreen;
    IBOutlet UIView *homescreen;
    IBOutlet UIView *colorbuttons;
    IBOutlet UIView *video5;
    IBOutlet UIView *video2;
    IBOutlet UIView *social5;
    IBOutlet UILabel *videolab1;
    IBOutlet UILabel *sociallab1;
    IBOutlet UILabel *lblRemainPhoto1;
    IBOutlet UILabel *lblRemainPhoto2;
    IBOutlet UILabel *timerview;
    IBOutlet UILabel *videotimerlabel;
    __weak IBOutlet UIView *camerascreenview;
    
    IBOutlet UITextField *txtFldEmail;
    
    //BOOLS//
    BOOL isRecording;
    BOOL isTaking;
    BOOL captureImage;
    BOOL captureImage3;
    
    //RANDOM//
    CIContext *ciContext;
    CALayer *videoLayer;
    NSString *selectedFilterName;
    
    NSString *watermark;
}

@property (nonatomic, weak) IBOutlet UIButton *recordButton1;
@property (nonatomic, readwrite) AVCapturePhotoSettings *requestedPhotoSettings;
@property (nonatomic) void (^willCapturePhotoAnimation)();
@property (nonatomic) void (^completed)(PhotoBoothNormal *photoCaptureDelegate);
@property (nonatomic, strong) AMPopTip *popTip;
@property (nonatomic) NSData *jpegPhotoData;
@property (nonatomic) NSData *dngPhotoData;
@property (nonatomic, weak) IBOutlet NSString *emailstring;
@property (nonatomic, strong) CNPPopupController *popupController;

@property (strong, nonatomic) UIActivityIndicatorView * activityAdd;
@property (weak, nonatomic) IBOutlet UIButton *btnAddPhoto;
@property (weak, nonatomic) IBOutlet UIButton *btnViewStatus;
@property (weak, nonatomic) IBOutlet UIButton *btnClear;
@property (weak, nonatomic) IBOutlet UIButton *btnPauseUploads;
@property (weak, nonatomic) IBOutlet UILabel *count;
@property (weak, nonatomic) IBOutlet UILabel *state;
@property (weak, nonatomic) IBOutlet UILabel *lblMessage;

//CAMERA SETTINGS//
@property (nonatomic, strong) PHPhotoLibrary* library;
@property (nonatomic,strong) UILongPressGestureRecognizer *lpgr;
@property (nonatomic, weak) IBOutlet AVCamManualPreviewView *previewView;
@property (nonatomic, weak) IBOutlet UISegmentedControl *captureModeControl;
@property (nonatomic, weak) IBOutlet UISegmentedControl *printcam;
@property (nonatomic, weak) IBOutlet UILabel *cameraUnavailableLabel;
@property (nonatomic, weak) IBOutlet UIButton *resumeButton;
@property (nonatomic, weak) IBOutlet UIButton *recordButton;
@property (nonatomic, weak) IBOutlet UIButton *cameraButton;
@property (nonatomic, weak) IBOutlet UIButton *HUDButton;
@property (nonatomic, weak) IBOutlet UIView *manualHUD;
@property (nonatomic, weak) IBOutlet UIView *printsettings;
@property (nonatomic, weak) IBOutlet UIView *printserver;
@property (nonatomic, weak) IBOutlet UIView *airprint;
@property (nonatomic) NSArray *focusModes;
@property (nonatomic, weak) IBOutlet UIView *manualHUDFocusView;
@property (nonatomic, weak) IBOutlet UISegmentedControl *focusModeControl;
@property (nonatomic, weak) IBOutlet UISlider *lensPositionSlider;
@property (nonatomic, weak) IBOutlet UILabel *lensPositionNameLabel;
@property (nonatomic, weak) IBOutlet UILabel *lensPositionValueLabel;
@property (nonatomic) NSArray *exposureModes;
@property (nonatomic, weak) IBOutlet UIView *manualHUDExposureView;
@property (nonatomic, weak) IBOutlet UISegmentedControl *exposureModeControl;
@property (nonatomic, weak) IBOutlet UISlider *exposureDurationSlider;
@property (nonatomic, weak) IBOutlet UILabel *exposureDurationNameLabel;
@property (nonatomic, weak) IBOutlet UILabel *exposureDurationValueLabel;
@property (nonatomic, weak) IBOutlet UISlider *ISOSlider;
@property (nonatomic, weak) IBOutlet UILabel *ISONameLabel;
@property (nonatomic, weak) IBOutlet UILabel *ISOValueLabel;
@property (nonatomic, weak) IBOutlet UISlider *exposureTargetBiasSlider;
@property (nonatomic, weak) IBOutlet UILabel *exposureTargetBiasNameLabel;
@property (nonatomic, weak) IBOutlet UILabel *exposureTargetBiasValueLabel;
@property (nonatomic, weak) IBOutlet UISlider *exposureTargetOffsetSlider;
@property (nonatomic, weak) IBOutlet UILabel *exposureTargetOffsetNameLabel;
@property (nonatomic, weak) IBOutlet UILabel *exposureTargetOffsetValueLabel;
@property (nonatomic) NSArray *whiteBalanceModes;
@property (nonatomic, weak) IBOutlet UIView *manualHUDWhiteBalanceView;
@property (nonatomic, weak) IBOutlet UISegmentedControl *whiteBalanceModeControl;
@property (nonatomic, weak) IBOutlet UISlider *temperatureSlider;
@property (nonatomic, weak) IBOutlet UILabel *temperatureNameLabel;
@property (nonatomic, weak) IBOutlet UILabel *temperatureValueLabel;
@property (nonatomic, weak) IBOutlet UISlider *tintSlider;
@property (nonatomic, weak) IBOutlet UILabel *tintNameLabel;
@property (nonatomic, weak) IBOutlet UILabel *tintValueLabel;
@property (nonatomic, weak) IBOutlet UIView *manualHUDLensStabilizationView;
@property (nonatomic, weak) IBOutlet UISegmentedControl *lensStabilizationControl;
@property (nonatomic, weak) IBOutlet UIView *manualHUDPhotoView;
@property (nonatomic, weak) IBOutlet UISegmentedControl *rawControl;

// Session management
@property (nonatomic, strong) TSPAppDelegate *appDelegate;
@property (nonatomic, strong) ACSPinController *pinController;

@property (nonatomic) dispatch_queue_t sessionQueue; // Communicate with the session and other session objects on this queue.
@property (nonatomic) AVCaptureSession *session;
@property (nonatomic) AVCaptureDeviceInput *videoDeviceInput;
@property (nonatomic) AVCaptureDevice *videoDevice;
@property (nonatomic) AVCaptureMovieFileOutput *movieFileOutput;
@property (nonatomic) AVCaptureVideoDataOutput *videoDataOutput;
@property (nonatomic) AVCaptureStillImageOutput *stillImageOutput;
@property (nonatomic) UIBackgroundTaskIdentifier backgroundRecordingID;
@property (nonatomic, getter = isDeviceAuthorized) BOOL deviceAuthorized;
@property (nonatomic, readonly, getter = isSessionRunningAndDeviceAuthorized) BOOL sessionRunningAndDeviceAuthorized;
@property (nonatomic) BOOL lockInterfaceRotation;
@property (nonatomic) id runtimeErrorHandlingObserver;
@property(nonatomic) BOOL isTesting;
@property (nonatomic) AVCapturePhotoOutput *photoOutput;
@property (nonatomic) ACSDefaultsUtil *defaultsUtil;
@property (nonatomic) NSMutableDictionary<NSNumber *, PhotoBoothNormal *> *inProgressPhotoCaptureDelegates;
@property (nonatomic) AVPlayer *player;
@property (nonatomic) AVPlayer *finalplayer;
@property (strong) AVPlayerLayer *avPlayerLayer;
@property (strong) AVPlayerItem *currentItem;
@property (nonatomic) AVPlayerViewController *controller;
//CAMERA SETTINGS END//

-(void)peerDidChangeStateWithNotification:(NSNotification *)notification;

-(void)didStartReceivingResourceWithNotification:(NSNotification *)notification;
-(void)updateReceivingProgressWithNotification:(NSNotification *)notification;
-(void)didFinishReceivingResourceWithNotification:(NSNotification *)notification;

@end

@implementation PhotoBoothNormal

NSProgress *_send_progress;
NSProgress *_receive_progress;
MBProgressHUD *send_hud;
MBProgressHUD *receive_hud;
NSURL *refURL;
//BOOL isVideo=NO;

NSMutableArray *convertedFileName;
NSMutableArray *assetsSelected;
NSData *imagetrans;

@synthesize restClient1;
@synthesize restClient;
@synthesize Uppaths;
@synthesize documentsDirectory;
@synthesize imagePath;
@synthesize data;
@synthesize buttoneffects1;
@synthesize lineborder;
@synthesize effectsborder;
@synthesize timerLabel;
@synthesize videoLabel;
@synthesize LayoutBackgroundColor;
@synthesize LayoutFontBorderColor;
@synthesize PrintBackgroundColor;
@synthesize PrintFontBorderColor;
@synthesize buttonborders;
@synthesize buttonborders1;
@synthesize buttoneffects;
@synthesize screenbackgroundcolor;
@synthesize look;
@synthesize viewborders;
@synthesize lineseparator;
@synthesize labelcolors;
@synthesize labelcolors1;
@synthesize textfields;
@synthesize switches;
@synthesize stepper;
@synthesize sliders;
@synthesize starteffects;
@synthesize segmented;
@synthesize captureImage1, individualphoto, imgLayout;

static UIColor* CONTROL_NORMAL_COLOR = nil;
static UIColor* CONTROL_HIGHLIGHT_COLOR = nil;
static float EXPOSURE_DURATION_POWER = 8; // Higher numbers will give the slider more sensitivity at shorter durations
static float EXPOSURE_MINIMUM_DURATION = 1.0/6000; // Limit exposure duration to a useful range
//CAMERA SETTINGS END//



+ (void)initialize
{
    CONTROL_NORMAL_COLOR = [UIColor yellowColor];
    CONTROL_HIGHLIGHT_COLOR = [UIColor colorWithRed:0.0 green:122.0/255.0 blue:1.0 alpha:1.0]; // A nice blue
}

+ (NSSet *)keyPathsForValuesAffectingSessionRunningAndDeviceAuthorized
{
    return [NSSet setWithObjects:@"session.running", @"deviceAuthorized", nil];
}

- (BOOL)isSessionRunningAndDeviceAuthorized
{
    return [[self session] isRunning] && [self isDeviceAuthorized];
}



- (void)viewDidLoad {
    [super viewDidLoad];

    press.hidden = YES;
    _appDelegate = (TSPAppDelegate *)[[UIApplication sharedApplication] delegate];
    if (![[_appDelegate mcManager]peerID]) {
        [[_appDelegate mcManager] setupPeerAndSessionWithDisplayName:[UIDevice currentDevice].name];
        [[_appDelegate mcManager] advertiseSelf:YES];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(peerDidChangeStateWithNotification:)
                                                 name:@"MCDidChangeStateNotification"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didStartReceivingResourceWithNotification:)
                                                 name:@"MCDidStartReceivingResourceNotification"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateReceivingProgressWithNotification:)
                                                 name:@"MCReceivingProgressNotification"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didFinishReceivingResourceWithNotification:)
                                                 name:@"didFinishReceivingResourceNotification"
                                               object:nil];
    [_nameTxt setDelegate:self];
    
    _assetSelected.delegate=self;
    _assetSelected.dataSource=self;
    
    _connections.delegate=self;
    _connections.dataSource=self;
    [_connections reloadData];
    
    assetsSelected = [[NSMutableArray alloc]init];
    convertedFileName = [[NSMutableArray alloc]init];
    
    
    
    
    
    NSUserDefaults *defaults33 = [NSUserDefaults standardUserDefaults];
    
    watermark =[defaults33 objectForKey:@"watermark"];
    NSLog(@"WATERMARK IS %@", watermark);
    
    // Construct URL to sound file
    NSString *path = [NSString stringWithFormat:@"%@/beep.mp3", [[NSBundle mainBundle] resourcePath]];
    NSURL *soundUrl = [NSURL fileURLWithPath:path];
    
    // Create audio player object and initialize with URL to sound
    _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl error:nil];
    
    
    NSString *path1 = [NSString stringWithFormat:@"%@/camClick.wav", [[NSBundle mainBundle] resourcePath]];
    NSURL *soundUrl1 = [NSURL fileURLWithPath:path1];
    
    // Create audio player object and initialize with URL to sound
    _audioPlayer1 = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl1 error:nil];
    
    
    [FBSDKSettings setAppID: @"1175602815854519"];
    
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver: self selector: @selector(uploadFinish) name:@"UploadFinish" object: nil];
    
   
    
    self.printsettings.hidden=YES;
    
    
    
    
    
   
    
    NSUserDefaults *defaults2 = [NSUserDefaults standardUserDefaults];
    NSString* key =[defaults2 objectForKey:@"consumerkey"];
    NSString* secret =[defaults2 objectForKey:@"consumersecret"];
    if ([key isEqualToString:@""] || key == nil)
    {
        [[FHSTwitterEngine sharedEngine]permanentlySetConsumerKey:@"2QQs5ySOWwAxHnVV2i9ZnTEEM" andSecret:@"kOIcTSTepk1GTKysW1bK61BLhJHLHqApq7Ktkb68OtEA4VZPgd"];
        [[FHSTwitterEngine sharedEngine]setDelegate:self];
        [[FHSTwitterEngine sharedEngine]loadAccessToken];
    }
    
    else
    {
        [[FHSTwitterEngine sharedEngine]permanentlySetConsumerKey:[NSString stringWithFormat:@"%@",key] andSecret:[NSString stringWithFormat:@"%@",secret]];
        [[FHSTwitterEngine sharedEngine]setDelegate:self];
        [[FHSTwitterEngine sharedEngine]loadAccessToken];
        
    }
    
    // NSString* hash =[defaults2 objectForKey:@"facebookhashtag"];
    NSString* appid =[defaults2 objectForKey:@"appid"];
    
    if ([appid isEqualToString:@""])
    {
        [FBSDKSettings setAppID: @"1175602815854519"];
    }
    
    else{
        [FBSDKSettings setAppID: appid];
    }
    
     pinclosed.backgroundColor = [UIColor whiteColor];
    
    self.button=NO;
    
    
    //POPTIP//
    self.popTip = [AMPopTip popTip];
    self.popTip.font = [UIFont fontWithName:@"Avenir-Medium" size:40];
    self.popTip.edgeMargin = 5;
    self.popTip.offset = 2;
    self.popTip.edgeInsets = UIEdgeInsetsMake(0, 10, 0, 10);
    self.popTip.shouldDismissOnTap = NO;
    self.popTip.actionAnimation = AMPopTipActionAnimationNone;
    
    self.popTip.tapHandler = ^{
        NSLog(@"Tap!");
    };
    
    //TIMERS//
    [self timerSlider];
    [self videotimerSlider];
    
    
    
    NSString * string61 = [self.record valueForKey:@"switchSendgrid"];
    NSLog(@"SENDGRID IS = %@", string61);
    
    
    NSString * string62 = [self.record valueForKey:@"switchLayout"];
    NSLog(@"LAYOUT IS = %@", string62);
    
    NSString * string63 = [self.record valueForKey:@"switchIndividual"];
    NSLog(@"LAYOUT IS = %@", string63);
    
    
    TextIndividual = [[NSString alloc] init];
    texttaken = [[NSMutableArray alloc] init];
    self.arrSlidshowImg = [[NSMutableArray alloc] init];
    photostaken = [[NSMutableArray alloc] init];
    if([photostaken count]){
        [photostaken removeAllObjects];
        [self.arrSlidshowImg removeAllObjects];
    }
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    selectedFilterName = @"CIPhotoEffectNone";
    pass.hidden=YES;
    testshot.hidden=YES;
    //PASSCODE//
    self.defaultsUtil = [[ACSDefaultsUtil alloc] init];
    self.pinController = [[ACSPinController alloc] initWithPinServiceName:@"testservice" pinUserName:@"testuser" accessGroup:@"accesstest" delegate:self];
    self.pinController.retriesMax = 5;
    
    // Validation block for pin controller to check if entered pin is valid.
    __weak PhotoBoothNormal *weakSelf = self;
    self.pinController.validationBlock = ^(NSString *pin) {
        return [pin isEqualToString:weakSelf.defaultsUtil.savedPin];
    };
    
    
    
    //LONG PRESS//
    self.lpgr = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPressGestures:)];
    self.lpgr.minimumPressDuration = 3.5f;
    self.lpgr.allowableMovement = 100.0f;
    
    [press addGestureRecognizer:self.lpgr];
    
    remaining.hidden=YES;
    welcome.hidden=YES;
    mainwelcome.hidden=YES;
    mainready.hidden=YES;
    getready.hidden=YES;
    mainready.hidden=YES;
    mainclosed.hidden=YES;
    closed.hidden=YES;
    effects.hidden=YES;
    maineffects.hidden=YES;
    mainremaining.hidden=YES;
    mainvideo.hidden=YES;
    video.hidden=YES;
    mainemail.hidden=YES;
    email.hidden=YES;
    mainfinal.hidden=YES;
    final.hidden=YES;
    maincountdownbg1.hidden=YES;
    maincountdownbg.hidden=YES;
    touchtostart.hidden = YES;
    alleffects.hidden=YES;
    bwvin.hidden=YES;
    bwsep.hidden=YES;
    bw.hidden=YES;
    vinsep.hidden=YES;
    vin.hidden=YES;
    sep.hidden=YES;
    col.hidden=YES;
    maincountdown.hidden=YES;
    videotimerview.hidden = YES;
    social.hidden = YES;
    email.hidden=YES;
    
    self.printcam.hidden = NO;
    
    maineffectsbg1.hidden=YES;
    
    NSString *backgroundcolor = [self.record valueForKey:@"layoutbackgroundcolor"];
    NSString *fontbordercolor = [self.record valueForKey:@"layoutfontbordercolor"];
    
    //PROGRESS INDICATOR//
    KVNProgressConfiguration *configuration = [[KVNProgressConfiguration alloc] init];
    configuration.statusColor = [UIColor colorWithString:fontbordercolor];
    configuration.statusFont = [UIFont fontWithName:@"HelveticaNeue-Thin" size:25.0f];
    configuration.circleStrokeForegroundColor = [UIColor colorWithString:fontbordercolor];
    configuration.circleStrokeBackgroundColor = [[UIColor colorWithString:fontbordercolor]colorWithAlphaComponent:0.3f];
    configuration.circleFillBackgroundColor = [[UIColor colorWithString:fontbordercolor]colorWithAlphaComponent:0.1f];
    configuration.backgroundFillColor = [[UIColor colorWithString:backgroundcolor]colorWithAlphaComponent:0.9f];
    configuration.backgroundTintColor = [[UIColor colorWithString:backgroundcolor]colorWithAlphaComponent:1.0f];
    configuration.successColor = [UIColor colorWithString:backgroundcolor];
    configuration.errorColor = [UIColor colorWithString:backgroundcolor];
    configuration.stopColor = [UIColor colorWithString:backgroundcolor];
    configuration.circleSize = 50.0f;
    configuration.lineWidth = 1.0f;
    configuration.fullScreen = YES;
    configuration.showStop = YES;
    configuration.stopRelativeHeight = 0.4f;
    configuration.tapBlock = ^(KVNProgress *progressView) {
    };
    configuration.allowUserInteraction = NO;
    [KVNProgress setConfiguration:configuration];
    
    
    
    for (UITextField *view in textfields) {
        
        if ([fontbordercolor length] > 0) {
            view.layer.borderWidth = 1;
            view.layer.borderColor = [UIColor colorWithString:fontbordercolor].CGColor;
            view.textColor = [UIColor colorWithString:fontbordercolor];
            [view setValue:[UIColor colorWithString:fontbordercolor] forKeyPath:@"_placeholderLabel.textColor"];
        } else {
            view.layer.borderWidth = 1;
            view.layer.borderColor = [UIColor whiteColor].CGColor;
            view.textColor = [UIColor whiteColor];
            [view setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
        }
        
        
    }
    
    
    
    NSString *valueToSave = [self.record valueForKey:@"eventname"];
    [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"AlbumName"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSString *numberofphotos = [self.record valueForKey:@"numberofphotos"];
    NSLog(@"Layout number of photos is %@", numberofphotos);
    photocount = [numberofphotos intValue];
    
    
    NSString *duration = [self.record valueForKey:@"photopreview"];
    previewcount = [duration intValue];
    
    
    if ([backgroundcolor length] > 0) {
        self.previewView.backgroundColor = [UIColor colorWithString:backgroundcolor];
    } else {
        self.previewView.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:0.85];
    }
    
    
    
    
    
    
    NSData *videobackground = [self.record valueForKey:@"videobackground"];
    videoimage = [UIImage imageWithData:videobackground];
    NSLog(@"video IS %@", videoimage);
    if(videoimage == nil){
        if ([backgroundcolor length] > 0) {
            videoscreen.backgroundColor = [UIColor colorWithString:backgroundcolor];
        } else {
            videoscreen.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:0.85];
        }
    }
    else {
        video.image = videoimage;
    }
    
    
    
    
    
    
    
    
    NSData *remainingbackground = [self.record valueForKey:@"remainingbackground"];
    remainingimage = [UIImage imageWithData:remainingbackground];
    
    if(remainingimage == nil){
        if ([backgroundcolor length] > 0) {
            remainbord.layer.borderWidth = 1;
            remainbord.layer.borderColor = [UIColor colorWithString:fontbordercolor].CGColor;
            remainingscreen.backgroundColor = [UIColor colorWithString:backgroundcolor];
        } else {
            remainbord.layer.borderColor = [UIColor whiteColor].CGColor;
            remainingscreen.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:0.85];
        }
    }
    else {
        remaining.image = remainingimage;
        remainingscreen.hidden=YES;
        remainbord.layer.borderWidth = 0;
        
    }
    
    
    
    
    
    if ([backgroundcolor length] > 0) {
        closedscreen.backgroundColor = [UIColor colorWithString:backgroundcolor];
    } else {
        closedscreen.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:0.85];
    }
    
    
    
    NSData *final2 = [self.record valueForKey:@"finalbackground"];
    finalimage = [UIImage imageWithData:final2];
    NSLog(@"final IS %@", finalimage);
    
    
    if(finalimage == nil){
        if ([backgroundcolor length] > 0) {
            finalscreen.backgroundColor = [UIColor colorWithString:backgroundcolor];
        } else {
            finalscreen.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:0.85];
        }
    }
    else {
        final.image = finalimage;
    }
    
    
    
    
    NSData *closedbackground = [self.record valueForKey:@"closedbackground"];
    closedimage = [UIImage imageWithData:closedbackground];
    NSLog(@"closed IS %@", closedimage);
    
    if(closedimage == nil){
        if ([backgroundcolor length] > 0) {
            closedscreen.backgroundColor = [UIColor colorWithString:backgroundcolor];
        } else {
            closedscreen.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:1.0];
        }
    }
    else {
        closed.image = closedimage;
    }
    
    
    
    NSData *welcomebackground = [self.record valueForKey:@"welcomebackground"];
    welcomeimage = [UIImage imageWithData:welcomebackground];
    NSLog(@"welcome IS %@", welcomeimage);
    
    if(welcomeimage == nil){
        if ([backgroundcolor length] > 0) {
            welcomescreen.backgroundColor = [UIColor colorWithString:backgroundcolor ];
        } else {
            welcomescreen.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:1.0];
        }
    }
    else {
        welcome.image = welcomeimage;
    }
    
    NSData *readybackground = [self.record valueForKey:@"readybackground"];
    getreadyimage = [UIImage imageWithData:readybackground];
    NSLog(@"getready IS %@", getreadyimage);
    
    if(getreadyimage == nil){
        if ([backgroundcolor length] > 0) {
            getreadyscreen.backgroundColor = [UIColor colorWithString:backgroundcolor];
        } else {
            getreadyscreen.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:1.0];
        }
    }
    else {
        getready.image = getreadyimage;
    }
    
    
    
    
    NSString *layoutpicked = [self.record valueForKey:@"shrTheme"];
    NSLog(@"LAYOUT PICKED IS %@", layoutpicked);
    
    
    
    
    NSString * string6 = [self.record valueForKey:@"switchPhotoBoothBackground"];
    
    if ([string6  isEqual: @"YES"])
    {
        NSLog (@"Layout background image SWITCH IS ON");
    }
    
    else
    {
        NSLog (@"Layout background image SWITCH IS OFF");
    }
    
    NSString * string7 = [self.record valueForKey:@"switchOverlay"];
    
    if ([string7  isEqual: @"YES"])
    {
        NSLog (@"Layout background overlay SWITCH IS ON");
    }
    
    else
    {
        NSLog (@"Layout background overlay SWITCH IS OFF");
    }
    
    NSString * email1 = [self.record valueForKey:@"switchIndividual"];
    
    if ([email1 isEqual: @"YES"])
    {
        NSLog (@"Email Individual SWITCH IS ON");
    }
    
    else
    {
        NSLog (@"Email Individual SWITCH IS OFF");
    }
    
    NSString * email2 = [self.record valueForKey:@"switchLayout"];
    
    if ([email2 isEqual: @"YES"])
    {
        NSLog (@"Email Layout SWITCH IS ON");
    }
    
    else
    {
        NSLog (@"Email Layout SWITCH IS OFF");
    }
    
    
    
    
    //COLOR EFFECTS SWITCHES//
    
    
    if ([backgroundcolor length] > 0) {
        welcomevideowshow.layer.cornerRadius = 7;
        welcomevideowshow.layer.masksToBounds = YES;
        welcomevideowshow.backgroundColor = [UIColor colorWithString:backgroundcolor];
        [[welcomevideowshow layer] setBorderWidth:1.0f];
        [[welcomevideowshow layer] setBorderColor:[UIColor colorWithString:fontbordercolor].CGColor];
    } else {
        welcomevideowshow.layer.cornerRadius = 7;
        welcomevideowshow.layer.masksToBounds = YES;
        welcomevideowshow.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:0.85];
        [[welcomevideowshow layer] setBorderWidth:1.0f];
        [[welcomevideowshow layer] setBorderColor:[UIColor whiteColor].CGColor];
    }
    
    
    
    if ([backgroundcolor length] > 0) {
        videotimerview.layer.cornerRadius = 7;
        videotimerview.layer.masksToBounds = YES;
        videotimerview.backgroundColor = [UIColor colorWithString:backgroundcolor];
    } else {
        videotimerview.layer.cornerRadius = 7;
        videotimerview.layer.masksToBounds = YES;
        videotimerview.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:0.85];
    }
    
    NSString * string9 = [self.record valueForKey:@"switchBlackWhite"];
    NSString * string4 = [self.record valueForKey:@"switchSepia"];
    NSString * string5 = [self.record valueForKey:@"switchVintage"];
    
    if ([string9  isEqual: @"YES"])
    {
        BWBG.hidden=NO;
        photoButtoncolor.hidden=NO;
        
        COLORBG.hidden=NO;
    }
    
    else{
        BWBG.hidden=YES;
        photoButtoncolor.hidden=NO;
        
        COLORBG.hidden=NO;
    }
    
    if ([string4 isEqual: @"YES"] )
    {
        SEPBG.hidden=NO;
        photoButtoncolor.hidden=NO;
        
        COLORBG.hidden=NO;
    }
    
    else{
        SEPBG.hidden=YES;
        photoButtoncolor.hidden=NO;
        
        COLORBG.hidden=NO;
    }
    
    if ([string5  isEqual: @"YES"])
    {
        VINBG.hidden=NO;
        photoButtoncolor.hidden=NO;
        
        COLORBG.hidden=NO;
    }
    
    else{
        VINBG.hidden=YES;
        photoButtoncolor.hidden=NO;
        
        COLORBG.hidden=NO;
    }
    
    if ([string9  isEqual: @"NO"] && [string4  isEqual: @"NO"] &&[string5  isEqual: @"NO"])
    {
        
        NSLog(@"COLOR ONLY");
    }
    
    
    
    
    NSString * share = [self.record valueForKey:@"switchSharing"];
    
    if ([share  isEqual: @"YES"])
    {
        sharestation.hidden=NO;
        loginBtn.hidden=YES;
        outBtn.hidden=YES;
        NSLog (@"Sharing SWITCH IS ON");
        
    }
    
    else
    {
        
        outBtn.hidden=YES;
        //    if ([FBSDKAccessToken currentAccessToken]) {
        //        // User is logged in, do work such as go to next view controller.
        //    }
        //
        if (![[DBSession sharedSession] isLinked])
        {
            sharestation.hidden=YES;
            loginBtn.hidden=NO;
            facebook.hidden=YES;
        }else{
            sharestation.hidden=YES;
            facebook.hidden=YES;
            outBtn.hidden=NO;
        }
        
      
        NSLog (@"Sharing SWITCH IS OFF");
    }
    
    
    
    NSString * share3 = [self.record valueForKey:@"switchSharingIndividual"];
    
    if ([share3  isEqual: @"YES"])
    {
       
        NSLog (@"Sharing Individual IS ON");
        
    }
    
    else
    {
      
        
        NSLog (@"Sharing Individual IS OFF");
    }
    
    NSString * share4 = [self.record valueForKey:@"switchSharingLayout"];
    
    if ([share4  isEqual: @"YES"])
    {
        
        NSLog (@"Sharing Layout IS ON");
        
    }
    
    else
    {
        
        
        NSLog (@"Sharing Layout IS OFF");
    }
    
    
    //SOCIAL SWITCHES//
    
    NSString * face = [self.record valueForKey:@"switchFacebook"];
    
    if ([face  isEqual: @"YES"])
    {
        NSLog (@"Facebook SWITCH IS ON");
        
    }
    
    else
    {
        facebook.hidden=YES;
        NSLog (@"Facebook SWITCH IS OFF");
    }
    
    NSString *twit= [self.record valueForKey:@"switchTwitter"];
    
    if ([twit  isEqual: @"YES"])
    {
        NSLog (@"Twitter SWITCH IS ON");
        
    }
    
    else
    {
        twitter.hidden=YES;
        NSLog (@"Twitter SWITCH IS OFF");
    }
    
    
    NSString * email5 = [self.record valueForKey:@"switchEmail"];
    
    if ([email5  isEqual: @"YES"])
    {
        
        NSLog (@"Email SWITCH IS ON");
    }
    
    else
    {
        mail.hidden = YES;
        NSLog (@"Email SWITCH IS OFF");
    }
    
    NSString * text= [self.record valueForKey:@"switchText"];
    
    if ([text  isEqual: @"YES"])
    {
        
        NSLog (@"Email SWITCH IS ON");
    }
    
    else
    {
        texting.hidden = YES;
        NSLog (@"Email SWITCH IS OFF");
    }
    
    //WELCOME SCREEN SWITCH//
    NSString * string8 = [self.record valueForKey:@"switchMessage"];
    
    if ([string8  isEqual: @"YES"])
    {
        NSLog (@"Welcome SWITCH IS ON");
    }
    
    else
    {
        NSLog (@"Welcome SWITCH IS OFF");
    }
    
    NSString * string1 = [self.record valueForKey:@"maxSelection1"];
    NSLog(@"PHOTO COUNTDOWN IS %@", string1);
    NSString * video1 = [self.record valueForKey:@"maxSelection2"];
    NSLog(@"VIDEO COUNTDOWN IS %@", video1);
    
    for (UIView *view in lineseparator) {
        if ([backgroundcolor length] > 0) {
            view.backgroundColor = [UIColor colorWithString:backgroundcolor];
        } else {
            view.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:1.0];
        }
        
    }
    for (UIImageView *view in screenbackgroundcolor) {
        if ([backgroundcolor length] > 0) {
            view.backgroundColor = [UIColor colorWithString:backgroundcolor];
        } else {
            view.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:1.0];
        }
        
    }
    for (UIView *view in viewborders) {
        if ([fontbordercolor length] > 0) {
            view.backgroundColor = [UIColor colorWithString:fontbordercolor];
        } else {
            view.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0];
        }
    }
    
    for (UILabel *view in labelcolors) {
        if ([fontbordercolor length] > 0) {
            view.textColor = [UIColor colorWithString:fontbordercolor];
        } else {
            view.textColor = [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0];
        }
    }
    
    for (UILabel *view in labelcolors1) {
        if ([backgroundcolor length] > 0) {
            view.textColor = [UIColor colorWithString:backgroundcolor];
        } else {
            view.textColor = [UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:1.0];
        }
    }
    
    
    for (UISlider *view in sliders) {
        if ([fontbordercolor length] > 0) {
            view.minimumTrackTintColor = [UIColor colorWithString:fontbordercolor];
            view.maximumTrackTintColor = [UIColor colorWithString:fontbordercolor];
            view.thumbTintColor = [UIColor colorWithString:fontbordercolor];
        } else {
            view.minimumTrackTintColor = [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0];
            view.maximumTrackTintColor = [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0];
            view.thumbTintColor = [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0];
        }
        
    }
    
    for (UISegmentedControl *view in segmented) {
        
        if ([fontbordercolor length] > 0) {
            
            view.layer.cornerRadius = 0.0;
            view.layer.borderColor = [UIColor colorWithString:fontbordercolor].CGColor;
            view.tintColor =[UIColor colorWithString:fontbordercolor];
            
            view.backgroundColor = [UIColor clearColor];
            view.layer.borderWidth = 1.0f;
            view.layer.masksToBounds = YES;
            
            
        } else {
            
            view.layer.cornerRadius = 0.0;
            view.layer.borderColor = [UIColor whiteColor].CGColor;
            view.tintColor =[UIColor whiteColor];
            
            view.backgroundColor = [UIColor clearColor];
            view.layer.borderWidth = 1.0f;
            view.layer.masksToBounds = YES;
            
            
            
        }
        
        
    }
    
    
    
    
    for (UIButton *button in buttonborders) {
        
        
        if ([fontbordercolor length] > 0  && [backgroundcolor length] > 0) {
            [[button layer] setBorderWidth:1.0f];
            [[button layer] setBorderColor:[UIColor colorWithString:fontbordercolor].CGColor];
            [[button layer] setBackgroundColor:[UIColor colorWithString:backgroundcolor].CGColor];
            [button setTitleColor:[UIColor colorWithString:fontbordercolor] forState:UIControlStateNormal];
            
        }
        
        else {
            [[button layer] setBorderWidth:1.0f];
            [[button layer] setBorderColor:[UIColor whiteColor].CGColor];
            [[button layer] setBackgroundColor:[UIColor blackColor].CGColor];
            [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        }
        
        
        
    }
    
    
    for (UIView *button in lineborder) {
        
        
        if ([fontbordercolor length] > 0) {
            [[button layer] setBorderWidth:1.0f];
            [[button layer] setBorderColor:[UIColor colorWithString:fontbordercolor].CGColor];
            [[button layer] setBackgroundColor:[UIColor clearColor].CGColor];
            
            
        } else {
            [[button layer] setBorderWidth:1.0f];
            [[button layer] setBorderColor:[UIColor whiteColor].CGColor];
            [[button layer] setBackgroundColor:[UIColor clearColor].CGColor];
            
        }
        
        
        
    }
    
    
    //EFFECTS BUTTONS//
    
    
    for (UIButton *button in buttoneffects) {
        //EFFECTS BACKGROUND//
        NSData *effectsbackground = [self.record valueForKey:@"effectsbackground"];
        effectsimage = [UIImage imageWithData:effectsbackground];
        NSLog(@"effects IS %@", effectsimage);
        
        if(effectsimage == nil){
            
            if ([fontbordercolor length] > 0  && [backgroundcolor length] > 0) {
                [[button layer] setCornerRadius:45];
                [[button layer] setBorderWidth:3.0f];
                
                [[button layer] setBorderColor:[UIColor colorWithString:fontbordercolor].CGColor];
                [[button layer] setBackgroundColor:[UIColor colorWithString:backgroundcolor].CGColor];
                [button setTitleColor:[UIColor colorWithString:fontbordercolor] forState:UIControlStateNormal];
                
            } else {
                [[button layer] setCornerRadius:45];
                [[button layer] setBorderWidth:3.0f];
                [[button layer] setBorderColor:[UIColor whiteColor].CGColor];
                [[button layer] setBackgroundColor:[UIColor blackColor].CGColor];
                [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                
                
            }
            
        }
        else {
            effects.image = effectsimage;
            
            //            [skippingsocial.layer setBackgroundColor:[UIColor colorWithWhite:0.0 alpha:0.0].CGColor];
            //            [skippingsocial setTitle:@"" forState:UIControlStateNormal];
            //            [skippingsocial setTitle:@"" forState:UIControlStateSelected];
            //
            [[button layer] setBackgroundColor:[UIColor colorWithWhite:0.0 alpha:0.0].CGColor];
            [button setTitle:@"" forState:UIControlStateNormal];
            [button setTitle:@"" forState:UIControlStateSelected];
            
        }
        
        
        
    }
    
    
    
    
    
    //VIDEO BACKGROUND//
    if(videoimage == nil){
        
        
        
        
    }
    else {
        
        video.image = videoimage;
        videoscreen.hidden=YES;
        videolab1.hidden=YES;
        video2.layer.borderWidth = 0;
        video5.hidden = YES;
        [recordyes setBackgroundImage:nil forState:UIControlStateNormal];
        [recordno setBackgroundImage:nil forState:UIControlStateNormal];
        
        
    }
    
    
    //SOCIAL BACKGROUND//
    NSData *emailbackground = [self.record valueForKey:@"emailbackground"];
    emailimage = [UIImage imageWithData:emailbackground];
    NSLog(@"effects IS %@", emailimage);
    
    
    if(emailimage == nil){
        
        if ([backgroundcolor length] > 0) {
            emailscreen.backgroundColor = [UIColor colorWithString:backgroundcolor];
            [soc setTitleColor:[UIColor colorWithString:backgroundcolor] forState:UIControlStateNormal];
            [skippingsocial setTitleColor:[UIColor colorWithString:backgroundcolor] forState:UIControlStateNormal];
            
        } else {
            emailscreen.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:1.0];
            [skippingsocial setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [soc setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        }
        
        
        if ([fontbordercolor length] > 0) {
            [[soc layer] setBackgroundColor:[UIColor colorWithString:fontbordercolor].CGColor];
            
            [[skippingsocial layer] setBackgroundColor:[UIColor colorWithString:fontbordercolor].CGColor];
            
        } else {
            [[soc layer] setBackgroundColor:[UIColor whiteColor].CGColor];
            [[skippingsocial layer] setBackgroundColor:[UIColor whiteColor].CGColor];
        }
        
        
        
        
        
    }
    else {
        
        email.image = emailimage;
        emailscreen.hidden=YES;
        sociallab1.hidden=YES;
        socialview.layer.borderWidth = 0;
        social5.hidden = YES;
        [mail setBackgroundImage:nil forState:UIControlStateNormal];
        [texting setBackgroundImage:nil forState:UIControlStateNormal];
        [facebook setBackgroundImage:nil forState:UIControlStateNormal];
        [twitter setBackgroundImage:nil forState:UIControlStateNormal];
        
        for (UIButton *button in buttoneffects1) {
            [[button layer] setBackgroundColor:[UIColor colorWithWhite:0.0 alpha:0.0].CGColor];
            [button setTitle:@"" forState:UIControlStateNormal];
            [button setTitle:@"" forState:UIControlStateSelected];
        }
    }
    
    
    
    
    
    
    for (UIButton *button in starteffects) {
        NSData *effectsbackground = [self.record valueForKey:@"effectsbackground"];
        effectsimage = [UIImage imageWithData:effectsbackground];
        NSLog(@"effects IS %@", effectsimage);
        
        if(effectsimage == nil){
            
            
            if ([fontbordercolor length] > 0  && [backgroundcolor length] > 0) {
                [[button layer] setCornerRadius:45];
                [[button layer] setBorderWidth:3.0f];
                
                [[button layer] setBorderColor:[UIColor colorWithString:backgroundcolor].CGColor];
                [[button layer] setBackgroundColor:[UIColor colorWithString:fontbordercolor].CGColor];
                [button setTitleColor:[UIColor colorWithString:backgroundcolor] forState:UIControlStateNormal];
                
            } else {
                [[button layer] setCornerRadius:45];
                [[button layer] setBorderWidth:3.0f];
                [[button layer] setBorderColor:[UIColor blackColor].CGColor];
                [[button layer] setBackgroundColor:[UIColor whiteColor].CGColor];
                [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                
                
            }
            
        }
        else {
            effects.image = effectsimage;
            
            [[button layer] setBackgroundColor:[UIColor colorWithWhite:0.0 alpha:0.0].CGColor];
            [button setTitle:@"" forState:UIControlStateNormal];
            [button setTitle:@"" forState:UIControlStateSelected];
            
        }
    }
    for (UIButton *button in buttonborders1) {
        
        if ([backgroundcolor length] > 0) {
            [button setTitleColor:[UIColor colorWithString:backgroundcolor] forState:UIControlStateNormal];
            
        } else {
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        }
        
        
        if ([fontbordercolor length] > 0) {
            [[button layer] setBackgroundColor:[UIColor colorWithString:fontbordercolor].CGColor];
            
        } else {
            [[button layer] setBackgroundColor:[UIColor whiteColor].CGColor];
        }
    }
    
    for (UIView *button in look) {
        if ([backgroundcolor length] > 0) {
            
            [[button layer] setBackgroundColor:[UIColor colorWithString:backgroundcolor].CGColor];
            
        } else {
            [[button layer] setBackgroundColor:[UIColor whiteColor].CGColor];
            
        }
        
        
        if ([fontbordercolor length] > 0) {
            button.backgroundColor = [UIColor colorWithString:fontbordercolor];
            
        } else {
            button.backgroundColor = [UIColor whiteColor];
        }
        
        
    }
    
    
    if ([backgroundcolor length] > 0) {
        [self.recordButton setTitleColor:[UIColor colorWithString:backgroundcolor] forState:UIControlStateNormal];
        
    } else {
        [self.recordButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    }
    
    
    if ([fontbordercolor length] > 0) {
        [[self.recordButton layer] setBackgroundColor:[UIColor colorWithString:fontbordercolor].CGColor];
        
    } else {
        [[self.recordButton layer] setBackgroundColor:[UIColor whiteColor].CGColor];
    }
    
    
    if ([backgroundcolor length] > 0) {
        countdownscreen.layer.cornerRadius = 7;
        countdownscreen.layer.masksToBounds = YES;
        countdownscreen.backgroundColor = [UIColor colorWithString:backgroundcolor];
    } else {
        countdownscreen.layer.cornerRadius = 7;
        countdownscreen.layer.masksToBounds = YES;
        countdownscreen.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:0.85];
    }
    
    
    
    
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    
    if (orientation == UIInterfaceOrientationPortrait)
    {   maincountdownbg.hidden=YES;
        videotimerview.hidden=YES;
        
        [maincountdown removeFromSuperview];
        [maincountdown setTranslatesAutoresizingMaskIntoConstraints:YES];
        [maincountdown setFrame:CGRectMake(300, 10, 315, 148)];
        [countdownscreen setFrame:CGRectMake(0, 0, 315, 148)];
        [countdownlook setFrame:CGRectMake(8, 8, 155, 132)];
        [timerview setFrame:CGRectMake(171, 0, 136, 140)];
        [self.view addSubview:maincountdown];
        
        [videotimerview removeFromSuperview];
        [videotimerview setTranslatesAutoresizingMaskIntoConstraints:YES];
        [videotimerview setFrame:CGRectMake(300, 10, 315, 148)];
        [recordbutton setFrame:CGRectMake(170, 8, 136, 132)];
        [stopbutton setFrame:CGRectMake(170, 8, 136, 132)];
        [videotimerlabel setFrame:CGRectMake(8, 8, 155, 132)];
        [self.view addSubview:videotimerview];
        
        
    }
    
    if (orientation == UIInterfaceOrientationLandscapeLeft)
    {   maincountdownbg.hidden=YES;
        videotimerview.hidden=YES;
        
        [maincountdown removeFromSuperview];
        [maincountdown setTranslatesAutoresizingMaskIntoConstraints:YES];
        [maincountdown setFrame:CGRectMake(843, 306, 171, 315)];
        [countdownscreen setFrame:CGRectMake(0, 0, 171, 315)];
        [countdownlook setFrame:CGRectMake(8, 8, 155, 132)];
        [timerview setFrame:CGRectMake(15, 140, 141, 162)];
        [self.view addSubview:maincountdown];
        
        [videotimerview removeFromSuperview];
        [videotimerview setTranslatesAutoresizingMaskIntoConstraints:YES];
        [videotimerview setFrame:CGRectMake(843, 306, 171, 315)];
        [recordbutton setFrame:CGRectMake(8, 166, 155, 141)];
        [stopbutton setFrame:CGRectMake(8, 166, 155, 141)];
        [videotimerlabel setFrame:CGRectMake(8, 8, 155, 150)];
        [self.view addSubview:videotimerview];
        
        
    }
    
    if (orientation == UIInterfaceOrientationLandscapeRight)
    {    maincountdownbg.hidden=YES;
        videotimerview.hidden=YES;
        
        [maincountdown removeFromSuperview];
        [maincountdown setTranslatesAutoresizingMaskIntoConstraints:YES];
        [maincountdown setFrame:CGRectMake(10, 306, 171, 315)];
        [countdownscreen setFrame:CGRectMake(0, 0, 171, 315)];
        [countdownlook setFrame:CGRectMake(8, 8, 155, 132)];
        [timerview setFrame:CGRectMake(15, 140, 141, 162)];
        [self.view addSubview:maincountdown];
        
        [videotimerview removeFromSuperview];
        [videotimerview setTranslatesAutoresizingMaskIntoConstraints:YES];
        [videotimerview setFrame:CGRectMake(10, 306, 171, 315)];
        [recordbutton setFrame:CGRectMake(8, 166, 155, 141)];
        [stopbutton setFrame:CGRectMake(8, 166, 155, 141)];
        [videotimerlabel setFrame:CGRectMake(8, 8, 155, 150)];
        [videotimerlabel setFrame:CGRectMake(8, 8, 155, 150)];
        [self.view addSubview:videotimerview];
        
    }
    
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"printip"];
    
    NSString *savedValue1 = [[NSUserDefaults standardUserDefaults]
                             stringForKey:@"printport"];
    printip.text = [NSString stringWithFormat:@"%@", savedValue];
    printport.text = [NSString stringWithFormat:@"%@", savedValue1];
    [self camera];
    
}

//PRINT SETTINGS//
-(IBAction)connectServer:(id)sender
{
    
    
    [self tapPing];
    NSLog(@"Connecting server...");
    
    
}


- (void)tapPing {
    
    [SimplePingHelper ping: printip.text target:self sel:@selector(pingResult:)];
    // [SimplePingHelper ping: printport.text target:self sel:@selector(pingResult:)];
}


- (void)pingResult:(NSNumber*)success {
    if (success.boolValue) {
        
        NSString *valueToSave = printip.text;
        NSString *valueToSave1 = printport.text;
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *url = [NSString stringWithFormat:@"%@:%@", valueToSave, valueToSave1];
        [defaults setObject:url forKey:@"print"];
        [defaults setObject:valueToSave forKey:@"printip"];
        [defaults setObject:valueToSave1 forKey:@"printport"];
        
        
        NSURLRequest *theRequest=[NSURLRequest requestWithURL:[NSURL URLWithString:url]
                                  
                                                  cachePolicy:NSURLRequestUseProtocolCachePolicy
                                  
                                              timeoutInterval:5.0];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            FCAlertView *alert = [[FCAlertView alloc] init];
            alert.darkTheme = YES;
            [alert makeAlertTypeSuccess];
            alert.detachButtons = YES;
            alert.blurBackground = YES;
            [alert showAlertWithTitle:@"SUCCESSFULLY CONNECTED"
                         withSubtitle:@"You have been successfully connected. "
                      withCustomImage:nil
                  withDoneButtonTitle:nil
                           andButtons:nil];
            
        });
        
        
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            FCAlertView *alert = [[FCAlertView alloc] init];
            alert.darkTheme = YES;
            [alert makeAlertTypeSuccess];
            alert.detachButtons = YES;
            alert.blurBackground = YES;
            [alert showAlertWithTitle:@"YOU ARE NOT CONNECTED"
                         withSubtitle:@"Please try again."
                      withCustomImage:nil
                  withDoneButtonTitle:nil
                           andButtons:nil];
            
        });
        
    }
}




//LUMECUBE

- (void)lumeCubeDisconnected:(CBPeripheral *)lcPeripheral {
    LCBluetoothManager *bt = [LCBluetoothManager sharedInstance];
    if (bt.connectedPeripherals.count == 0) {
        [self.videoDevice setExposureMode:AVCaptureExposureModeAutoExpose];
        [self.videoDevice setWhiteBalanceMode:AVCaptureWhiteBalanceModeContinuousAutoWhiteBalance];
    }

}

- (void)bluetoothPoweredOff {
    NSLog(@"HEY BITCH. NO BT");
}
// Connect Lume Cubes as they are discovered
- (void)lumeCubeDiscovered:(CBPeripheral *)lcPeripheral {
    LCBluetoothManager *bt = [LCBluetoothManager sharedInstance];
    
    [bt connectPeripheral:lcPeripheral];

}


- (void)lumeCubeConnected:(CBPeripheral *)lcPeripheral {
    LCBluetoothManager *bt = [LCBluetoothManager sharedInstance];
    if (bt.connectedPeripherals.count >= 5) {
        [bt stopScan]; }
}

- (void)disconnectPeripheral:(CBPeripheral *)lcPeripheral {
    LCBluetoothManager *bt = [LCBluetoothManager sharedInstance];
    [bt disconnectPeripheral:lcPeripheral];
    islumecube.on = NO;
     NSLog(@"LUMECUBE DISCONNECTED BY ME");
    
    
}

- (IBAction)didSelectLume:(id)sender {
    
    LCBluetoothManager *bt = [LCBluetoothManager sharedInstance];
    bt.lcbmDelegate = self;
    [bt scanForLumeCubes];
    [self selectlume:CNPPopupStyleCentered];
    
    islumecube.on = YES;
    if ([islumecube isOn]) {
        NSLog(@"its on!");
    } else {
        NSLog(@"its off!");
    }
}

- (void)selectlume:(CNPPopupStyle)popupStyle
{
    
    fontcolor = [UIColor whiteColor];
    backgroundcolor1  = [UIColor darkGrayColor];
    
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 85, 85)];
    imageView.image = [UIImage imageNamed:@"flashicon.png"];
    imageView.image = [imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [imageView setTintColor:[UIColor darkGrayColor]];
    
    
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    UIView *customView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 355)];
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 95)];
    
    titleLabel.numberOfLines = 4;
    titleLabel.text = @"Lume Cubes are bluetooth flashes. If you have not purchased any you can just hit cancel. If you are using them please select a power level then hit connect.";
    titleLabel.textColor = [UIColor darkGrayColor];
    
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0,115, 320, 180)];
    
    _tableView.rowHeight = 45;
    _tableView.sectionFooterHeight = 22;
    _tableView.sectionHeaderHeight = 22;
    _tableView.scrollEnabled = NO;
    _tableView.showsVerticalScrollIndicator = YES;
    _tableView.userInteractionEnabled = YES;
    _tableView.bounces = YES;
    
    _tableView.delegate = self;
    _tableView.dataSource = self;
    
    
    
    CNPPopupButton *buttonconnect = [[CNPPopupButton alloc] initWithFrame:CGRectMake(0, 325,320, 45)];
    [buttonconnect setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    buttonconnect.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [buttonconnect setTitle:@"CONNECT" forState:UIControlStateNormal];
    [buttonconnect setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    buttonconnect.backgroundColor = [UIColor whiteColor];
    [[buttonconnect layer] setBorderWidth:1.0f];
    [[buttonconnect layer] setBorderColor:[UIColor darkGrayColor].CGColor];
    buttonconnect.layer.cornerRadius = 15;
    buttonconnect.selectionHandler = ^(CNPPopupButton *button){
        
        
        //        FullBrightness,
        //        ThreeQuarterBrightness,
        //        HalfBrightness,
        //        QuarterBrightness,
        //        EighthBrightness,
        //        SixteenthBrightness,
        //
        if ([_power.text isEqualToString: @"FULL"])
        {
            NSLog(@"YEP FULL");
            LCBluetoothManager *bt = [LCBluetoothManager sharedInstance]; for (CBPeripheral *lumeCube in bt.connectedPeripherals) {
                [LCFlashControl fireFlashOnLumeCube:lumeCube withBrightness:FullBrightness andDuration:QuarterSecond];
            }
        }
        
        if ([_power.text isEqualToString: @"3/4"])
        {
            NSLog(@"YEP 3/4");
            LCBluetoothManager *bt = [LCBluetoothManager sharedInstance]; for (CBPeripheral *lumeCube in bt.connectedPeripherals) {
                [LCFlashControl fireFlashOnLumeCube:lumeCube withBrightness:ThreeQuarterBrightness andDuration:QuarterSecond];
            }
        }
        
        if ([_power.text isEqualToString: @"1/2"])
        {
            NSLog(@"YEP 1/2");
            LCBluetoothManager *bt = [LCBluetoothManager sharedInstance]; for (CBPeripheral *lumeCube in bt.connectedPeripherals) {
                [LCFlashControl fireFlashOnLumeCube:lumeCube withBrightness:HalfBrightness andDuration:QuarterSecond];
                
            }
        }
        
        if ([_power.text isEqualToString: @"1/4"])
        {
            NSLog(@"YEP 1/4");
            LCBluetoothManager *bt = [LCBluetoothManager sharedInstance]; for (CBPeripheral *lumeCube in bt.connectedPeripherals) {
                [LCFlashControl fireFlashOnLumeCube:lumeCube withBrightness:QuarterBrightness andDuration:QuarterSecond];
            }
            
            
        }
        
        
        NSLog(@"CONNECT BUTTON PRESSED");
    };
    
    
    
    self.data2 = [[NSArray alloc]initWithObjects:@"FULL", @"3/4",@"1/2",@"1/4",nil];
    
    _power = [[UILabel alloc] init];
    [_power setFont:[UIFont fontWithName:@"Helvetica Neue" size:22]];
    _power.textColor= [UIColor blackColor];
    _power.userInteractionEnabled=NO;
    _power.numberOfLines = 1;
    _power.text= @"Select a power level";
    
    
    
    
    
    
    
    [customView addSubview:titleLabel];
    [customView addSubview:buttonconnect];
    [customView addSubview:_power];
    [customView addSubview:_tableView];
    
    
    
    UIView *customView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 65)];
    
    CNPPopupButton *button1 = [[CNPPopupButton alloc] initWithFrame:CGRectMake(0, 10, 150, 45)];
    [button1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button1.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button1 setTitle:@"CANCEL" forState:UIControlStateNormal];
    [button1 setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    
    [[button1 layer] setBorderWidth:1.0f];
    [[button1 layer] setBorderColor:[UIColor darkGrayColor].CGColor];
    button1.layer.cornerRadius = 15;
    button1.selectionHandler = ^(CNPPopupButton *button1){
        [self.popupController dismissPopupControllerAnimated:YES];
        islumecube.on = NO;
        if ([islumecube isOn]) {
            NSLog(@"its on!");
        } else {
            NSLog(@"its off!");
        }

        
    };
    CNPPopupButton *button = [[CNPPopupButton alloc] initWithFrame:CGRectMake(170, 10, 150, 45)];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button setTitle:@"SAVE" forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.backgroundColor = [UIColor darkGrayColor];
    button.layer.cornerRadius = 15;
    button.selectionHandler = ^(CNPPopupButton *button){
        [self.popupController dismissPopupControllerAnimated:YES];
        LCBluetoothManager *bt = [LCBluetoothManager sharedInstance];
            if (bt.connectedPeripherals.count >= 1) {
                if ([_power.text isEqualToString: @"FULL"])
                {
                    NSLog(@"YEP FULL");
                    NSString *valueToSave = @"FullBrightness";
                    [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"lumepower"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    NSLog(@"TextField Saved: %@", valueToSave);
                    
                }
                
                if ([_power.text isEqualToString: @"3/4"])
                {
                    NSLog(@"YEP 3/4");
                    NSString *valueToSave = @"ThreeQuarterBrightness";
                    [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"lumepower"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    NSLog(@"TextField Saved: %@", valueToSave);
                    
                    
                }
                
                if ([_power.text isEqualToString: @"1/2"])
                {
                    NSLog(@"YEP 1/2");
                    NSString *valueToSave = @"HalfBrightness";
                    [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"lumepower"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    NSLog(@"TextField Saved: %@", valueToSave);
                    
                    
                }
                
                if ([_power.text isEqualToString: @"1/4"])
                {
                    NSLog(@"YEP 1/4");
                    NSString *valueToSave = @"QuarterBrightness";
                    [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"lumepower"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    NSLog(@"TextField Saved: %@", valueToSave);
                    
                    
                    
                    
                }

            }
            
            else
                
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    FCAlertView *alert = [[FCAlertView alloc] init];
                    alert.darkTheme = YES;
                    [alert makeAlertTypeWarning];
                    alert.detachButtons = YES;
                    alert.blurBackground = YES;
                    [alert showAlertWithTitle:@"NO LUME CUBES DETECTED"
                                 withSubtitle:@"Make sure your Lume Cubes are on."
                              withCustomImage:nil
                          withDoneButtonTitle:nil
                                   andButtons:nil];
                    
                });
            }
        
        
        
        
        
        
        
        
        
        //        if ([self validateEmail:textFied.text] == 1) {
        //
        //            NSString *valueToSave = textFied.text;
        //            [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"lumecubepower"];
        //            [[NSUserDefaults standardUserDefaults] synchronize];
        //            NSLog(@"TextField Saved: %@", valueToSave);
        //            [self nointernetmailcomposer];
        //            email.hidden=YES;
        //            emailscreen.hidden=YES;
        //            social.hidden = YES;
        //            socialview.hidden=YES;
        //            social.hidden=YES;
        
        //    }
        
        //
        //        else {
        //            NSLog(@"VALIDATE EMAIL DONE");
        //            [self tryagain:CNPPopupStyleCentered];
        //        }
        
    };
    
    
    
    [customView1 addSubview:button1];
    [customView1 addSubview:button];
    self.popupController = [[CNPPopupController alloc] initWithContents:@[imageView,  customView, customView1]];
    self.popupController.theme = [CNPPopupTheme defaultTheme];
    self.popupController.theme.popupStyle = popupStyle;
    self.popupController.delegate = self;
    [self.popupController presentPopupControllerAnimated:NO];
    
    
    
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [self.data2 count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    
    
    if (cell == nil) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        
    }
    
    
    cell.textLabel.text = [self.data2 objectAtIndex:indexPath.row] ;
    
    //cell.textLabel.font = [UIFont systemFontOfSize:11.0];
    
    
    return cell;
    
    
    
}





- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    _power.text = cell.textLabel.text;
    //  self.tableView.hidden = YES;
    
    NSLog(@"Power level is %@", _power.text);
    
}




//DROPBOX//

-(IBAction)droboxlogin:(id)sender
{
    box=[[PhotoBoothNormal alloc]init];
    [box loginWithViewController:self];
    NSNotificationCenter *centerNew = [NSNotificationCenter defaultCenter];
    [centerNew addObserver: self selector: @selector(afterLogin) name:@"Login Success" object: nil];
    
}


-(IBAction)droboxlogout:(id)sender
{
    
    box=[[PhotoBoothNormal alloc]init];
    [box logOut];
    outBtn.hidden=YES;
    loginBtn.hidden=NO;
}


-(void)login:(id)sender
{
    box=[[PhotoBoothNormal alloc]init];
    [box loginWithViewController:self];
}



-(void)afterLogin
{
    
    loginBtn.hidden=YES;
    outBtn.hidden=NO;
}


-(void)uploadFinish
{
    [MBProgressHUD hideHUDForView:self.view animated:NO];
}

-(void)logout:(id)sender
{
    box=[[PhotoBoothNormal alloc]init];
    [box logOut];
    outBtn.hidden=YES;
    loginBtn.hidden=NO;
}




//PASSCODE//

- (void)updateTouchIDState
{
    if (![self.pinController touchIDAvailable:NULL] || self.defaultsUtil.savedPin == nil) {
        
        [self.defaultsUtil setTouchIDActive:NO];
    }
}

- (void)updateVerifyAndChangeState {
    if (self.defaultsUtil.savedPin) {
    }
    else {
    }
}

- (void)updatePin:(NSString *)pin {
    [self.defaultsUtil savePin:pin];
    [self updateTouchIDState];
    [self updateVerifyAndChangeState];
    
}


- (void)resetPin {
    [self.pinController resetPIN];
    [self.defaultsUtil removePin];
    [self updateTouchIDState];
    [self updateVerifyAndChangeState];
}
#pragma mark - Button actions


- (IBAction)didSelectVerify:(id)sender {
    
    [self.pinController presentVerifyControllerFromViewController1:self];
}

- (IBAction)didSelectCreate:(id)sender {
    
    [self.pinController presentCreateControllerFromViewController:self];
}

- (IBAction)didSelectChange:(id)sender {
    
    [self.pinController presentChangeControllerFromViewController:self];
}

- (IBAction)didSelectRemovePIN:(id)sender {
    [self resetPin];
}

- (IBAction)didSelectTouchIDSwitch:(UISwitch *)sender {
    
    [self.defaultsUtil setTouchIDActive:sender.on];
    if (sender.on) {
        [self.pinController storePin:self.defaultsUtil.savedPin];
    }
    else {
        [self.pinController resetPIN];
    }
}

- (IBAction)didSelectShowPinString:(id)sender {
    
    NSLog(@"In pin controller keychain: %@", [self.pinController storedPin]);
    NSLog(@"In user defaults (custom): %@", self.defaultsUtil.savedPin);
}

#pragma mark - Pin controller delegate

- (void)pinChangeController:(UIViewController *)pinChangeController didChangePin:(NSString *)pin
{
    NSLog(@"Did change pin: %@", pin);
    [self updatePin:pin];
}

- (void)pinCreateController:(UIViewController *)pinCreateController didCreatePin:(NSString *)pin
{
    NSLog(@"Did create pin: %@", pin);
    [self updatePin:pin];
}

- (void)pinController:(UIViewController *)pinController1 didVerifyPin1:(NSString *)pin
{
    mainclosed.hidden=YES;
    NSLog(@"Did verify pin1: %@", pin);
}


- (void)pinController:(UIViewController *)pinController didVerifyPin:(NSString *)pin
{
    pinclosed.hidden=NO;
    
}


- (void)pinControllerDidEnterWrongPin:(UIViewController *)pinController lastRetry:(BOOL)lastRetry
{
    NSLog(@"Did enter wrong pin - last retry? -> %@", lastRetry ? @"YES" : @"NO");
    if (lastRetry) {
        // Maybe show an alert view controller that indicates that the user has just one retry left?
    }
}

- (void)pinControllerCouldNotVerifyPin:(UIViewController *)pinController
{
    NSLog(@"Could not verify pin - no more retries!");
    
}

- (void)pinControllerDidSelectCancel:(UIViewController *)pinController
{
    NSLog(@"Did select cancel!");
    [pinController dismissViewControllerAnimated:YES completion:nil];
}

- (void)handleLongPressGestures:(UILongPressGestureRecognizer *)sender2
{
    if ([sender2 isEqual:self.lpgr]) {
        [self.popTip hide];
        if (sender2.state == UIGestureRecognizerStateBegan)
        {
            [self.pinController presentVerifyControllerFromViewController:self];
            
        }
    }
}


- (IBAction)closebooth:(id)sender {
    
    pinclosed.hidden=YES;
    mainclosed.hidden=NO;
    closed.hidden=NO;
  
    
}

- (IBAction)openbooth:(id)sender {
    
    pinclosed.hidden=YES;
    mainclosed.hidden=YES;
    closed.hidden=YES;
    touchtostart.hidden=NO;
    
}

- (IBAction)searchDevices:(id)sender {
    [[_appDelegate mcManager] setupMCBrowser];
    [[[_appDelegate mcManager] browser] setDelegate:self];
    [self presentViewController:[[_appDelegate mcManager] browser] animated:YES completion:nil];
    
}

#pragma mark - MCBrowserViewControllerDelegate method implementation
-(void)browserViewControllerDidFinish:(MCBrowserViewController *)browserViewController{
    [_appDelegate.mcManager.browser dismissViewControllerAnimated:YES completion:nil];
}


-(void)browserViewControllerWasCancelled:(MCBrowserViewController *)browserViewController{
    [_appDelegate.mcManager.browser dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UITextField Delegate method implementation
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [_nameTxt resignFirstResponder];
    
    _appDelegate.mcManager.peerID = nil;
    _appDelegate.mcManager.session = nil;
    _appDelegate.mcManager.browser = nil;
    [_appDelegate.mcManager.advertiser stop];
    _appDelegate.mcManager.advertiser = nil;
    
    NSString *name = _nameTxt.text;
    if ([name isEqualToString:@""]) {
        name = [UIDevice currentDevice].name;
    }
    
    [_appDelegate.mcManager setupPeerAndSessionWithDisplayName:name];
    [_appDelegate.mcManager setupMCBrowser];
    [_appDelegate.mcManager advertiseSelf:YES];
    
    return YES;
}


#pragma mark - Connection state
-(void)peerDidChangeStateWithNotification:(NSNotification *)notification{
    MCPeerID *peerID = [[notification userInfo] objectForKey:@"peerID"];
    NSString *peerDisplayName = peerID.displayName;
    MCSessionState state = [[[notification userInfo] objectForKey:@"state"] intValue];
    //NSLog(@"state %d",state);
    
    if (state != MCSessionStateConnecting) {
        if (state == MCSessionStateConnected) {
            [_appDelegate.arrConnectedDevices addObject:peerDisplayName];
            
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                //                FCAlertView *alert = [[FCAlertView alloc] init];
                //                alert.darkTheme = YES;
                //                [alert makeAlertTypeSuccess];
                //                alert.detachButtons = YES;
                //                alert.blurBackground = YES;
                //                [alert showAlertWithTitle:@"SUCCESSFULLY CONNECTED"
                //                             withSubtitle:@"You have been successfully connected. "
                //                          withCustomImage:nil
                //                      withDoneButtonTitle:nil
                //                               andButtons:nil];
                //
                
                pinclosed.hidden=YES;
            });
            
        }
        else if (state == MCSessionStateNotConnected){
            if ([_appDelegate.arrConnectedDevices count] > 0) {
                int indexOfPeer = (int)[_appDelegate.arrConnectedDevices indexOfObject:peerDisplayName];
                [_appDelegate.arrConnectedDevices removeObjectAtIndex:indexOfPeer];
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                FCAlertView *alert = [[FCAlertView alloc] init];
                alert.darkTheme = YES;
                [alert makeAlertTypeWarning];
                alert.detachButtons = YES;
                alert.blurBackground = YES;
                [alert showAlertWithTitle:@"DISCONNECTED"
                             withSubtitle:@"This device has been disconnected. Please let the attendant know."
                          withCustomImage:nil
                      withDoneButtonTitle:nil
                               andButtons:nil];
                
            });
            
            
        }
        
        
        [_connections reloadData];
        
        BOOL peersExist = ([[_appDelegate.mcManager.session connectedPeers] count] == 0);
        [_btnDisconnect setEnabled:!peersExist];
        [_nameTxt setEnabled:peersExist];
    }
}

- (void) viewDidLayoutSubviews {
    [super viewDidLayoutSubviews]; //if you want superclass's behaviour...
    // resize your layers based on the view's new frame
    videoLayer.frame = self.previewView.bounds;
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (UIInterfaceOrientationIsPortrait(interfaceOrientation) || UIInterfaceOrientationIsLandscape(interfaceOrientation));
    
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    if(UIInterfaceOrientationIsPortrait(toInterfaceOrientation)){
        
        
        
    }
    else if(UIInterfaceOrientationIsLandscape(toInterfaceOrientation)){
    }
    
    
    
    
}



-(void)camera{
    
    self.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    if( ciContext == nil )
        ciContext = [CIContext contextWithOptions:nil];
    AVCaptureSession *session = [[AVCaptureSession alloc] init];
    session.sessionPreset = AVCaptureSessionPresetPhoto;
    
    [self setSession:session];
    [[self previewView] setSession:session];
    [self checkDeviceAuthorizationStatus];
    dispatch_queue_t sessionQueue = dispatch_queue_create("session queue", DISPATCH_QUEUE_SERIAL);
    [self setSessionQueue:sessionQueue];
    
    dispatch_async(sessionQueue, ^{
        
        [self setBackgroundRecordingID:UIBackgroundTaskInvalid];
        
        NSError *error = nil;
        
        AVCaptureDevice *videoDevice = [PhotoBoothNormal deviceWithMediaType:AVMediaTypeVideo preferringPosition:AVCaptureDevicePositionFront];
        AVCaptureDeviceInput *videoDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:videoDevice error:&error];
        
        if (error)
        {
            NSLog(@"%@", error);
        }
        
        [[self session] beginConfiguration];
        
        if ([session canAddInput:videoDeviceInput])
        {
            [session addInput:videoDeviceInput];
            [self setVideoDeviceInput:videoDeviceInput];
            [self setVideoDevice:videoDeviceInput.device];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [[(AVCaptureVideoPreviewLayer *)[[self previewView] layer] connection] setVideoOrientation:(AVCaptureVideoOrientation)[self interfaceOrientation]];
            });
        }
        
        AVCaptureDevice *audioDevice = [[AVCaptureDevice devicesWithMediaType:AVMediaTypeAudio] firstObject];
        AVCaptureDeviceInput *audioDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:audioDevice error:&error];
        
        if (error)
        {
            NSLog(@"%@", error);
        }
        
        if ([session canAddInput:audioDeviceInput])
        {
            [session addInput:audioDeviceInput];
        }
        
        
        AVCaptureMovieFileOutput *movieFileOutput = [[AVCaptureMovieFileOutput alloc] init];
        AVCaptureConnection *connection = [movieFileOutput connectionWithMediaType:AVMediaTypeVideo];
        
        if ([connection isVideoOrientationSupported])
        {
            AVCaptureVideoOrientation newOrientation;
            switch ([[UIDevice currentDevice] orientation]) {
                case UIDeviceOrientationPortrait:
                    newOrientation = AVCaptureVideoOrientationPortrait;
                    break;
                case UIDeviceOrientationPortraitUpsideDown:
                    newOrientation = AVCaptureVideoOrientationPortraitUpsideDown;
                    break;
                case UIDeviceOrientationLandscapeLeft:
                    newOrientation = AVCaptureVideoOrientationLandscapeRight;
                    break;
                case UIDeviceOrientationLandscapeRight:
                    newOrientation = AVCaptureVideoOrientationLandscapeLeft;
                    break;
                default:
                    newOrientation = AVCaptureVideoOrientationLandscapeLeft;
            }
            [connection setVideoOrientation:newOrientation];
        }
        [self setMovieFileOutput:movieFileOutput];
        //        }
        
        self.videoDataOutput = [[AVCaptureVideoDataOutput alloc] init];
        [self.videoDataOutput setSampleBufferDelegate:self queue:self.sessionQueue];
        self.videoDataOutput.alwaysDiscardsLateVideoFrames = YES;
        if ([session canAddOutput:self.videoDataOutput])
        {
            [session addOutput:self.videoDataOutput];
            AVCaptureConnection *connection = [self.videoDataOutput connectionWithMediaType:AVMediaTypeVideo];
            
            if ([connection isVideoOrientationSupported])
            {
                AVCaptureVideoOrientation newOrientation;
                switch ([[UIDevice currentDevice] orientation]) {
                    case UIDeviceOrientationPortrait:
                        newOrientation = AVCaptureVideoOrientationPortrait;
                        videoLayer = [CALayer layer];
                        videoLayer.frame = self.previewView.bounds;
                        [self.previewView.layer addSublayer:videoLayer];
                        break;
                        
                        
                        
                    case UIDeviceOrientationLandscapeLeft:
                        newOrientation = AVCaptureVideoOrientationLandscapeRight;
                        videoLayer = [CALayer layer];
                        videoLayer.frame = self.previewView.bounds;
                        [self.previewView.layer addSublayer:videoLayer];
                        
                        break;
                    case UIDeviceOrientationLandscapeRight:
                        newOrientation = AVCaptureVideoOrientationLandscapeLeft;
                        videoLayer = [CALayer layer];
                        videoLayer.frame = self.previewView.bounds;
                        [self.previewView.layer addSublayer:videoLayer];
                        break;
                    default:
                        newOrientation = AVCaptureVideoOrientationLandscapeLeft;
                }
                [connection setVideoOrientation:newOrientation];
            }
            [connection setVideoMirrored:YES];
            
        }
        self.videoDataOutput.videoSettings = [NSDictionary dictionaryWithObject:[NSNumber numberWithInt:kCVPixelFormatType_32BGRA]
                                                                         forKey:(id)kCVPixelBufferPixelFormatTypeKey];
        AVCapturePhotoOutput *photoOutput = [[AVCapturePhotoOutput alloc] init];
        if ( [self.session canAddOutput:photoOutput] ) {
            [self.session addOutput:photoOutput];
            self.photoOutput = photoOutput;
            self.photoOutput.highResolutionCaptureEnabled = YES;
            
            
            self.inProgressPhotoCaptureDelegates = [NSMutableDictionary dictionary];
        }
        
        [[self session] commitConfiguration];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self configureManualHUD];
        });
    });
    
    self.manualHUD.hidden = YES;
    self.manualHUDExposureView.hidden = YES;
    self.manualHUDWhiteBalanceView.hidden = YES;
    
    self.printsettings.hidden = YES;
    self.airprint.hidden = YES;
    self.printserver.hidden = YES;
    
}


//TEST PHOTO//
-(IBAction)captureTestPhoto:(id)sender
{
  
    
    
    double durationSecond = [[NSUserDefaults standardUserDefaults] doubleForKey:@"DURATION_SECOND"];
    if (durationSecond == 0)
    {
        FCAlertView *alert = [[FCAlertView alloc] init];
        alert.darkTheme = YES;
        [alert makeAlertTypeCaution];
        alert.detachButtons = YES;
        [alert setAlertSoundWithFileName:@"error.mp3"];
        alert.blurBackground = YES;
        [alert showAlertWithTitle:@"SET YOUR CAMERA SETTINGS"
                     withSubtitle:@"You need to set your camera settings first."
                  withCustomImage:nil
              withDoneButtonTitle:nil
                       andButtons:nil];
        
        return;
    }
 
   else {
        testshot.on = YES;
        mainadmin.hidden=YES;
       countdownTimer = [NSTimer scheduledTimerWithTimeInterval:1.8 target:self selector:@selector(startCountdown1) userInfo:nil repeats:false];
  
    
    }
    
    
    
}

-(void)startCountdown1
{
    NSString *lumepower1 = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"lumepower"];
    
    UIView *flashView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1024 , 1024)];
    [flashView setBackgroundColor:[UIColor whiteColor]];
    [[[self view] window] addSubview:flashView];
    
    [UIView animateWithDuration:.25f
                     animations:^{
                         [_audioPlayer1 play];
                         
                         
                         
                         LCBluetoothManager *bt = [LCBluetoothManager sharedInstance];
                         
                         for (CBPeripheral *lumeCube in bt.connectedPeripherals) {
                             [LCFlashControl fireFlashOnLumeCube:lumeCube withBrightness:lumepower1 andDuration:QuarterSecond];
                             double durationSecond = [[NSUserDefaults standardUserDefaults] doubleForKey:@"DURATION_SECOND"];
                             
                             [self.videoDevice setExposureModeCustomWithDuration:CMTimeMakeWithSeconds(durationSecond, 1000*1000*1000)  ISO:AVCaptureISOCurrent completionHandler:nil];
                             NSLog(@"FLASH POWER IS WHAT %@", lumepower1);
                             
                             float temperatureValue = [[NSUserDefaults standardUserDefaults] floatForKey:@"TEMPERATURE"];
                             float tintValue = [[NSUserDefaults standardUserDefaults] floatForKey:@"TINT"];
                             
                             AVCaptureWhiteBalanceTemperatureAndTintValues temperatureAndTint = {
                                 
                                 .temperature = temperatureValue,
                                 .tint = tintValue,
                                 
                                 
                             };
                             [self setWhiteBalanceGains:[self.videoDevice deviceWhiteBalanceGainsForTemperatureAndTintValues:temperatureAndTint]];
                         }
                         
                         [flashView setAlpha:0.f];
                         
                         
                         
                     }
                     completion:^(BOOL finished) {
                         [flashView removeFromSuperview];
                     }
     ];
    [NSTimer scheduledTimerWithTimeInterval:0.25 target:self selector:@selector(flash1) userInfo:nil repeats:false];
    
}



#pragma mark Capturing Photos


//STEP 1: STARTING THE BOOTH. The welcome screen is the first screen to appear unless the 'welcome slider' was off in the admin when creating the event.//
- (IBAction)StartBooth {
    testshot.on = NO;
    photoButtoncolor.hidden=YES;
    [self startboothing];
    

  }

-(void)startboothing
{
    
    if ([islumecube isOn]) {
        NSLog(@"its on!");
        LCBluetoothManager *bt = [LCBluetoothManager sharedInstance];
          [self.videoDevice setExposureMode:AVCaptureExposureModeAutoExpose];
            [self.videoDevice setWhiteBalanceMode:AVCaptureWhiteBalanceModeContinuousAutoWhiteBalance];
     
    } else {
        NSLog(@"its off!");
        
            double durationSecond = [[NSUserDefaults standardUserDefaults] doubleForKey:@"DURATION_SECOND"];
        if (durationSecond == 0)
        {
            FCAlertView *alert = [[FCAlertView alloc] init];
            alert.darkTheme = YES;
            [alert makeAlertTypeCaution];
            alert.detachButtons = YES;
            [alert setAlertSoundWithFileName:@"error.mp3"];
            alert.blurBackground = YES;
            [alert showAlertWithTitle:@"SET YOUR CAMERA SETTINGS"
                         withSubtitle:@"You need to set your camera settings first."
                      withCustomImage:nil
                  withDoneButtonTitle:nil
                           andButtons:nil];
            
            return;
        }
            [self.videoDevice setExposureModeCustomWithDuration:CMTimeMakeWithSeconds(durationSecond, 1000*1000*1000)  ISO:AVCaptureISOCurrent completionHandler:nil];
        
        float temperatureValue = [[NSUserDefaults standardUserDefaults] floatForKey:@"TEMPERATURE"];
        float tintValue = [[NSUserDefaults standardUserDefaults] floatForKey:@"TINT"];
        
        AVCaptureWhiteBalanceTemperatureAndTintValues temperatureAndTint = {
            
            .temperature = temperatureValue,
            .tint = tintValue,
            
            
        };
        [self setWhiteBalanceGains:[self.videoDevice deviceWhiteBalanceGainsForTemperatureAndTintValues:temperatureAndTint]];
    }

    
   
   

    
    
    NSString * string8 = [self.record valueForKey:@"switchMessage"];
    NSLog(@"STRING 8 IS %@", string8);
     press.hidden = NO;

    if ([string8  isEqual: @"NO"])
    {
         mainadmin.hidden=YES;
    
        NSString * string9 = [self.record valueForKey:@"switchBlackWhite"];
        NSString * string4 = [self.record valueForKey:@"switchSepia"];
        NSString * string5 = [self.record valueForKey:@"switchVintage"];
        if ([string9  isEqual: @"NO"] && [string4  isEqual: @"NO"] &&[string5  isEqual: @"NO"])
        {
            mainwelcome.hidden=NO;
            welcomescreen.hidden=YES;
            welcomevideohide.hidden=YES;
            welcomevideowshow.hidden=NO;
            welcome.hidden=NO;
            touchtostart.hidden=NO;
            selectedFilterName = @"CIPhotoEffectNone";
   
            NSLog(@"COLOR ONLY");
        }
        
        else
        {
            countdown.hidden = NO;
            [self starting];
            self.popTip = [AMPopTip popTip];
            self.popTip.font = [UIFont fontWithName:@"Avenir-Medium" size:40];
            self.popTip.edgeMargin = 5;
            self.popTip.offset = 2;
            self.popTip.edgeInsets = UIEdgeInsetsMake(0, 10, 0, 10);
            self.popTip.shouldDismissOnTap = NO;
            self.popTip.actionAnimation = AMPopTipActionAnimationNone;
    
            
            UIView *customView = [[UIView alloc] initWithFrame:CGRectMake(0, self.previewView.frame.size.height- 160, self.previewView.frame.size.width, self.previewView.frame.size.height)];
            self.popTip.popoverColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.75];
            [self.popTip showText:@"SELECT A FILTER" direction:AMPopTipDirectionUp maxWidth:600 inView:self.view fromFrame:customView.frame];
        }

        
    }
    
    else
    {
        NSString *videowelcome = [self.record valueForKey:@"welcomeimagevideo"];
        if (videowelcome == nil)
        {
            touchtostart.hidden=NO;
            mainwelcome.hidden=NO;
            welcomescreen.hidden=NO;
            mainadmin.hidden=YES;
            welcome.hidden=NO;
            welcomevideowshow.hidden=YES;
            welcomevideohide.hidden=NO;
            welcomescreen1.hidden=YES;
            welcomescreen.hidden=NO;
            
        }
        else
        {
            [self addvideowelcome ];
            touchtostart.hidden=NO;
            mainwelcome.hidden=NO;
            welcomevideowshow.hidden=NO;
            welcomevideohide.hidden=YES;
            welcomescreen1.hidden=NO;
            welcomescreen.hidden=YES;
            mainadmin.hidden=YES;
            welcome.hidden=NO;
            [_player play];
            
        }
        
        
    }
    
}








//STEP 2: SELECTING AN EFFECT. This step is for selecting a color effect. The 'TouchtoStart' button hides the welcome screen and allows for selecting an effect. If the event is color only, go to step 3. //

- (IBAction)TouchToStart {
    NSLog(@"TOUCH TO START PRESSED");
    [_player pause];
    NSString * string9 = [self.record valueForKey:@"switchBlackWhite"];
    NSString * string4 = [self.record valueForKey:@"switchSepia"];
    NSString * string5 = [self.record valueForKey:@"switchVintage"];
    if ([string9  isEqual: @"NO"] && [string4  isEqual: @"NO"] &&[string5  isEqual: @"NO"])
    {
        mainwelcome.hidden=NO;
        welcomescreen.hidden=NO;
        welcomevideohide.hidden=NO;
        welcome.hidden=NO;
        selectedFilterName = @"CIPhotoEffectNone";
        [self capturephoto];
        NSLog(@"COLOR ONLY");
    }
    
    else
    {
       
        countdown.hidden = NO;
        [self starting];
        self.popTip = [AMPopTip popTip];
        self.popTip.font = [UIFont fontWithName:@"Avenir-Medium" size:40];
        self.popTip.edgeMargin = 5;
        self.popTip.offset = 2;
        self.popTip.edgeInsets = UIEdgeInsetsMake(0, 10, 0, 10);
        self.popTip.shouldDismissOnTap = NO;
        self.popTip.actionAnimation = AMPopTipActionAnimationNone;
        
        UIView *customView = [[UIView alloc] initWithFrame:CGRectMake(0, self.previewView.frame.size.height- 160, self.previewView.frame.size.width, self.previewView.frame.size.height)];
        self.popTip.popoverColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.75];
        [self.popTip showText:@"SELECT A FILTER" direction:AMPopTipDirectionUp maxWidth:600 inView:self.view fromFrame:customView.frame];
    }
    
}


-(IBAction)BWStart:(id)sender
{
    
    UIView *customView = [[UIView alloc] initWithFrame:CGRectMake(photoButtoncolor.frame.origin.x+effectsstack.frame.origin.x-35 , self.previewView.frame.size.height- 160, 200, 200)];
    self.popTip.popoverColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.75];
    [self.popTip showText:@"PRESS BEGIN" direction:AMPopTipDirectionUp maxWidth:600 inView:self.view fromFrame:customView.frame];
    selectedFilterName = @"CIPhotoEffectMono";
    photoButtoncolor.enabled=YES;
    photoButtoncolor.hidden=NO;
    self.button= YES;
    NSLog(@"EFFECT SELECTED = %@", selectedFilterName);
}

-(IBAction)ColorStart:(id)sender
{
    UIView *customView = [[UIView alloc] initWithFrame:CGRectMake(photoButtoncolor.frame.origin.x+effectsstack.frame.origin.x-35 , self.previewView.frame.size.height- 160, 200, 200)];
    self.popTip.popoverColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.75];
    [self.popTip showText:@"PRESS BEGIN" direction:AMPopTipDirectionUp maxWidth:600 inView:self.view fromFrame:customView.frame];
    
    selectedFilterName = @"CIPhotoEffectNone";
    photoButtoncolor.enabled=YES;
    photoButtoncolor.hidden=NO;
    self.button= YES;
    
    NSLog(@"EFFECT SELECTED = %@", selectedFilterName);
    
}


-(IBAction)VintageStart:(id)sender
{
    UIView *customView = [[UIView alloc] initWithFrame:CGRectMake(photoButtoncolor.frame.origin.x+effectsstack.frame.origin.x-35 , self.previewView.frame.size.height- 160, 200, 200)];
    self.popTip.popoverColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.75];
    [self.popTip showText:@"PRESS BEGIN" direction:AMPopTipDirectionUp maxWidth:600 inView:self.view fromFrame:customView.frame];
    photoButtoncolor.enabled=YES;
    photoButtoncolor.hidden=NO;
    self.button= YES;
    selectedFilterName = @"CIPhotoEffectProcess";
    NSLog(@"EFFECT SELECTED = %@", selectedFilterName);
}

-(IBAction)SepiaStart:(id)sender
{
    
    UIView *customView = [[UIView alloc] initWithFrame:CGRectMake(photoButtoncolor.frame.origin.x+effectsstack.frame.origin.x-35 , self.previewView.frame.size.height- 160, 200, 200)];
    self.popTip.popoverColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.75];
    [self.popTip showText:@"PRESS BEGIN" direction:AMPopTipDirectionUp maxWidth:600 inView:self.view fromFrame:customView.frame];
    photoButtoncolor.enabled=YES;
    photoButtoncolor.hidden=NO;
    self.button= YES;
    selectedFilterName = @"CISepiaTone";
    NSLog(@"EFFECT SELECTED = %@", selectedFilterName);
}

-(void)starting
{
    
    NSData *effectsbackground = [self.record valueForKey:@"effectsbackground"];
    effectsimage = [UIImage imageWithData:effectsbackground];
    
    if(effectsimage == nil){
        
        
        touchtostart.hidden=YES;
        welcome.hidden=YES;
        welcomescreen.hidden=YES;
        mainwelcome.hidden=YES;
        maineffectsbg1.hidden=NO;
        photoButtoncolor.hidden=NO;
        photoButtoncolor.enabled=YES;
        self.button= NO;
        
        
    }
    else {
        touchtostart.hidden=YES;
        welcome.hidden=YES;
        welcomescreen.hidden=YES;
        mainwelcome.hidden=YES;
        maineffectsbg1.hidden=NO;
        photoButtoncolor.hidden=NO;
        photoButtoncolor.enabled=YES;
        self.button= NO;
        effects.image = effectsimage;
        effects.hidden=NO;
        
    }
    
}









//STEP 3: BEGIN COUNTDOWN. The IBAction 'capturephoto' begins the countdown.//

-(IBAction)capturePhoto:(id)sender
{
    [self capturephoto];
}


-(void)capturephoto
{
    NSString * string9 = [self.record valueForKey:@"switchBlackWhite"];
    NSString * string4 = [self.record valueForKey:@"switchSepia"];
    NSString * string5 = [self.record valueForKey:@"switchVintage"];
    if ([string9  isEqual: @"NO"] && [string4  isEqual: @"NO"] &&[string5  isEqual: @"NO"])
    {
        effectsscreen1.hidden=YES;
        touchtostart.hidden=YES;
        mainadmin.hidden=YES;
        effects.hidden=YES;
        mainwelcome.hidden=YES;
        mainready.hidden=NO;
        welcome.hidden=YES;
        [NSTimer scheduledTimerWithTimeInterval:2.0
                                         target:self
                                       selector:@selector(begincount:)
                                       userInfo:nil
                                        repeats:NO];
    }
    
    else
    {
        if (self.button == 1)
        {
            [self.popTip hide];
          
            
            photoButtoncolor.hidden=YES;
            effects.hidden=YES;
            maineffectsbg1.hidden=YES;
            col.hidden=YES;
            mainready.hidden=NO;
            getready.hidden=NO;
            NSString *numberofphotos = [self.record valueForKey:@"numberofphotos"];
            NSLog(@"Layout number of photos is %@", numberofphotos);
            photocount = [numberofphotos intValue];
            NSString * string8 = [self.record valueForKey:@"switchMessage"];
            
            if ([string8  isEqual: @"YES"])
            {
                effectsscreen1.hidden=YES;
                touchtostart.hidden=YES;
                mainadmin.hidden=YES;
                welcome.hidden=YES;
                
            }
            
            else
            {
                touchtostart.hidden=YES;
                mainadmin.hidden=YES;
                UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
                if (orientation == UIInterfaceOrientationPortrait)  {
                    maincountdown1.hidden=YES;
                    maincountdown.hidden=YES;
                }
                
                else
                {
                    maincountdown1.hidden=YES;
                    maincountdown.hidden=YES;
                }
                
                NSLog (@"Welcome SWITCH IS OFF");
            }
            
            
            [NSTimer scheduledTimerWithTimeInterval:2.0
                                             target:self
                                           selector:@selector(begincount:)
                                           userInfo:nil
                                            repeats:NO];
        }
        if (self.button == 0)
        {
            NSLog(@"ISNT ENABLED");
            UIView *customView = [[UIView alloc] initWithFrame:CGRectMake(0, self.previewView.frame.size.height- 160, self.previewView.frame.size.width, self.previewView.frame.size.height)];
            self.popTip.popoverColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.75];
            [self.popTip showText:@"SELECT A FILTER FIRST" direction:AMPopTipDirectionUp maxWidth:600 inView:self.view fromFrame:customView.frame ];
            photoButtoncolor.enabled=YES;
            
        }
        
    }
    
    
    
}


-(void)begincount:(id)sender{
    for (UILabel *view in timerLabel) {
        view.hidden = false;
    }
    mainready.hidden=YES;
    countdownTimer = [NSTimer scheduledTimerWithTimeInterval:1.1 target:self selector:@selector(startCountdown) userInfo:nil repeats:true];
    countdown.hidden = NO;
}


-(void)startCountdown {
    mainready.hidden=YES;
    getready.hidden=YES;
    maincountdown.hidden=NO;
    captureImage1.hidden = YES;
    
    if (timerInt > 0) {
        [self showLookHereArrow];
        
        timerInt--;
        for (UILabel *view in timerLabel) {
            view.text =[NSString stringWithFormat:@"%ld", (long)timerInt];
        }
        [_audioPlayer play];
    }
    
    
   
    
    
    if (timerInt <= 0) {
        maincountdown.hidden=YES;
        NSString *lumepower1 = [[NSUserDefaults standardUserDefaults]
                                stringForKey:@"lumepower"];
        
        
        NSString *a = [self.record valueForKey:@"maxSelection1"];
        NSInteger b = [a integerValue];
        timerInt = b +1;
        
        UIView *flashView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1024 , 1024)];
        [flashView setBackgroundColor:[UIColor whiteColor]];
        [[[self view] window] addSubview:flashView];
        
        [UIView animateWithDuration:.25f
                         animations:^{
                             [_audioPlayer1 play];
                             
                           
                             
                             LCBluetoothManager *bt = [LCBluetoothManager sharedInstance];
                             
                               for (CBPeripheral *lumeCube in bt.connectedPeripherals) {
                                   if (bt.connectedPeripherals.count == 0) {
                                       [self.videoDevice setExposureMode:AVCaptureExposureModeAutoExpose];
                                       [self.videoDevice setWhiteBalanceMode:AVCaptureWhiteBalanceModeContinuousAutoWhiteBalance];
                                   }
                                   else{
                                     [LCFlashControl fireFlashOnLumeCube:lumeCube withBrightness:lumepower1 andDuration:QuarterSecond];
                                     double durationSecond = [[NSUserDefaults standardUserDefaults] doubleForKey:@"DURATION_SECOND"];
                                     
                                     [self.videoDevice setExposureModeCustomWithDuration:CMTimeMakeWithSeconds(durationSecond, 1000*1000*1000)  ISO:AVCaptureISOCurrent completionHandler:nil];
                                     NSLog(@"FLASH POWER IS WHAT %@", lumepower1);
                                       
                                       float temperatureValue = [[NSUserDefaults standardUserDefaults] floatForKey:@"TEMPERATURE"];
                                       float tintValue = [[NSUserDefaults standardUserDefaults] floatForKey:@"TINT"];
                                       
                                       AVCaptureWhiteBalanceTemperatureAndTintValues temperatureAndTint = {
                                           
                                           .temperature = temperatureValue,
                                           .tint = tintValue,
                                           
                                           
                                       };
                                       [self setWhiteBalanceGains:[self.videoDevice deviceWhiteBalanceGainsForTemperatureAndTintValues:temperatureAndTint]];
                                   }
                                 }
                                 
                                 [flashView setAlpha:0.f];
                           
                             
                             
                         }
                         completion:^(BOOL finished) {
                             [flashView removeFromSuperview];
                         }
         ];
        
        
        
        [NSTimer scheduledTimerWithTimeInterval:0.25 target:self selector:@selector(flash) userInfo:nil repeats:false];
        
        
    }
}

-(void)flash
{
    [self shotPhoto];
    if ([islumecube isOn]) {
        NSLog(@"its on!");
          [self.videoDevice setExposureMode:AVCaptureExposureModeAutoExpose];
            [self.videoDevice setWhiteBalanceMode:AVCaptureWhiteBalanceModeContinuousAutoWhiteBalance];
       
    } else {
        NSLog(@"its off!");
        
        double durationSecond = [[NSUserDefaults standardUserDefaults] doubleForKey:@"DURATION_SECOND"];
        [self.videoDevice setExposureModeCustomWithDuration:CMTimeMakeWithSeconds(durationSecond, 1000*1000*1000)  ISO:AVCaptureISOCurrent completionHandler:nil];
        
        float temperatureValue = [[NSUserDefaults standardUserDefaults] floatForKey:@"TEMPERATURE"];
        float tintValue = [[NSUserDefaults standardUserDefaults] floatForKey:@"TINT"];
        
        AVCaptureWhiteBalanceTemperatureAndTintValues temperatureAndTint = {
            
            .temperature = temperatureValue,
            .tint = tintValue,
            
            
        };
        [self setWhiteBalanceGains:[self.videoDevice deviceWhiteBalanceGainsForTemperatureAndTintValues:temperatureAndTint]];
        
    }
    [countdownTimer invalidate];
}


-(void)flash1
{
    [self shotPhoto1];
    
}

//THIS CODE IS FOR THE COUNTDOWN TIMES BETWEEN EACH PHOTO//
- (void)timerSlider{
    NSString *a = [self.record valueForKey:@"maxSelection1"];
    NSInteger b = [a integerValue];
    timerInt = b + 1;
    for (UILabel *view in timerLabel) {
        view.text =[NSString stringWithFormat:@"%ld", (long)timerInt];
    }
    
}

- (void)showLookHereArrow {
    
    
    lookhere.frame = CGRectMake(lookhere.frame.origin.x , lookhere.frame.origin.y , lookhere.frame.size.width, lookhere.frame.size.height);
    
    // scale the button down before the animation...
    lookhere.transform = CGAffineTransformMakeScale(0.1, 0.1);
    
    // now animate the view...
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         lookhere.transform = CGAffineTransformIdentity;
                         lookhere.frame = CGRectMake(lookhere.frame.origin.x, lookhere.frame.origin.y, lookhere.frame.size.width, lookhere.frame.size.height);
                     }
                     completion:nil];
    
    
    
}

//THIS CAPTURES THE PHOTO AND PROCESS THE CODE - (void)captureOutput:(AVCaptureOutput *)captureOutput//
- (void)shotPhoto {
    NSLog (@"shotPhoto");
    captureImage = YES;
    ;
    photocount--;
    
    
    
}

- (void)shotPhoto1 {
    NSLog (@"shotPhotoTes");
    captureImage = YES;
    ;

    
    
    
}



//THIS CODE IS FOR WHEN THE LAST PHOTO IS TAKEN//
-(void) final
{
    
    
    UIDeviceOrientation deviceOrientation = [UIDevice currentDevice].orientation;
    
    if ( UIDeviceOrientationIsPortrait( deviceOrientation ) ) {
        UIImage *img = [self makeLayoutImageVertical];
    }
    
    
    
    if ( UIDeviceOrientationIsLandscape( deviceOrientation ) ) {
        UIImage *img = [self makeLayoutImage];
    }
    
    
    NSLog (@"final");
    captured.hidden=NO;
    captureImage1.hidden=YES;
    
    
    NSString * string = [self.record valueForKey:@"switchVideo"];
    
    if ([string  isEqual: @"YES"])
    {
        video.hidden=NO;
        mainvideo.hidden=NO;
        maineffectsbg1.hidden=YES;
        maincountdown.hidden = YES;
    }
    
    else
    {
        
     
        NSString * sharing = [self.record valueForKey:@"switchSharing"];
        
        if ([sharing isEqual: @"YES"])
        {
            
            
            NSString *videofinal = [self.record valueForKey:@"finalimagevideo"];
            if (videofinal == nil)
            {
                mainfinal.hidden = NO;
                final.hidden = NO;
                finalscreen2.hidden=NO;
                finalscreen.hidden=NO;
                [NSTimer scheduledTimerWithTimeInterval:3.5
                                                 target:self
                                               selector:@selector(newsession)
                                               userInfo:nil
                                                repeats:NO];
            }
            else{
                mainfinal.hidden = NO;
                finalscreen2.hidden=YES;
                finalscreen.hidden=YES;
                [self addvideofinal];
            }

            
            NSLog(@"Need code to send photos as an array with custom name");
//            for (int i = 0; i < photostaken.count; i++)
//            {
//                [self getPath];
//                NSString *prefixString = @"Image";
//                
//                NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%d", prefixString, i];
//                imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
//                UIImage *image= [UIImage imageWithData:[_arrSlidshowImg objectAtIndex:i]];
//                data = UIImageJPEGRepresentation(image,0.5);
//                [data writeToFile:imagePath atomically:YES];
//                
//                refURL = [NSURL fileURLWithPath:imagePath];
//                if ([[_appDelegate.mcManager.session connectedPeers] count]) {
//                    [self sendResource];
//                }
//                
//            }
        }
        else
        {
            NSString * face = [self.record valueForKey:@"switchFacebook"];
            NSString * twit = [self.record valueForKey:@"switchTwitter"];
            NSString * emails = [self.record valueForKey:@"switchEmail"];
            NSString * text = [self.record valueForKey:@"switchText"];
            if ([face  isEqual: @"NO"] && [twit  isEqual: @"NO"] &&[emails  isEqual: @"NO"]&&[text  isEqual: @"NO"])
            {
                
                NSString *videofinal = [self.record valueForKey:@"finalimagevideo"];
                if (videofinal == nil)
                {
                    mainfinal.hidden = NO;
                    final.hidden = NO;
                    finalscreen2.hidden=NO;
                    finalscreen.hidden=NO;
                    [NSTimer scheduledTimerWithTimeInterval:3.5
                                                     target:self
                                                   selector:@selector(newsession)
                                                   userInfo:nil
                                                    repeats:NO];
                }
                else{
                    mainfinal.hidden = NO;
                    finalscreen2.hidden=YES;
                    finalscreen.hidden=YES;
                    [self addvideofinal];
                }
                
                
                
                
                
            }
            else
            {
                [self emailinternetcheck];
                email.hidden=NO;
                emailscreen.hidden=NO;
                social.hidden = NO;
                socialview.hidden=NO;
                social.hidden=NO;
                
                
            }
            
        }
        
        
        
        
        
    }
    
    
    //  UIImage *img = [self makeLayoutImage];
    
    //    if (!self.isBoothStartViewPrint) {
    //        [NSTimer scheduledTimerWithTimeInterval:1.2 target:self selector:@selector(loadfiles) userInfo:nil repeats:NO];
    //    }
    //    else{
    //        [NSTimer scheduledTimerWithTimeInterval:2.2 target:self selector:@selector(loadfiles) userInfo:nil repeats:NO];
    //    }
    //
}


//THIS CODE IS CALLED TO BEGIN A NEW SESSION//
-(void)newsession
{
    
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"textnumber"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    if([photostaken count]){
        [texttaken removeAllObjects];
        [photostaken removeAllObjects];
        [self.arrSlidshowImg removeAllObjects];
    }
    
    NSString *numberofphotos = [self.record valueForKey:@"numberofphotos"];
    NSLog(@"Layout number of photos is %@", numberofphotos);
    photocount = [numberofphotos intValue];
    
    
    NSString *duration = [self.record valueForKey:@"photopreview"];
    previewcount = [duration intValue];
    
    
    email.hidden=YES;
    emailscreen.hidden=YES;
    social.hidden = YES;
    socialview.hidden=YES;
    social.hidden=YES;
    
    mainfinal.hidden=YES;
    final.hidden=YES;
    finalscreen.hidden=YES;
    [self timerSlider];
    [self videotimerSlider];
    [_finalplayer seekToTime:kCMTimeZero];
    [_player seekToTime:kCMTimeZero];
    [self startboothing];
    
}










//STEP 4: IF VIDEO WAS SELECTED AS AN OPTION THIS IS THE CODE FOR RECORDING VIDEO. IF VIDEO WAS NOT SELECTED GO TO STEP 5.

-(IBAction)videoyes:(id)sender
{
    welcome.hidden=YES;
    mainwelcome.hidden=YES;
    mainvideo.hidden=YES;
    video.hidden=YES;
    maineffectsbg1.hidden=YES;
    videotimerview.hidden=NO;
    [self checkDeviceAuthorizationStatus];
    NSLog(@"videorecord is Recording");
    dispatch_async(dispatch_get_main_queue(), ^{
        [videoLayer removeFromSuperlayer];
    });
    [self.session beginConfiguration];
    [self.session removeOutput:self.videoDataOutput];
    [self.session addOutput:self.movieFileOutput];
    
    [self.session commitConfiguration];
    
    self.session.sessionPreset = AVCaptureSessionPresetHigh;
    
    [self setLockInterfaceRotation:YES];
    
    
}

//THIS GOES TO STEP 5//
-(IBAction)videono:(id)sender
{
    welcome.hidden=YES;
    mainwelcome.hidden=YES;
    mainvideo.hidden=YES;
    video.hidden=YES;
    maineffectsbg1.hidden=YES;
    videotimerview.hidden=YES;
    
    NSString * face = [self.record valueForKey:@"switchFacebook"];
    NSString * twit = [self.record valueForKey:@"switchTwitter"];
    NSString * emails = [self.record valueForKey:@"switchEmail"];
    NSString * text = [self.record valueForKey:@"switchText"];
    if ([face  isEqual: @"NO"] && [twit  isEqual: @"NO"] &&[emails  isEqual: @"NO"]&&[text  isEqual: @"NO"])
    {
        
        NSString *videofinal = [self.record valueForKey:@"finalimagevideo"];
        if (videofinal == nil)
        {
            mainfinal.hidden = NO;
            final.hidden = NO;
            [NSTimer scheduledTimerWithTimeInterval:3.5
                                             target:self
                                           selector:@selector(newsession)
                                           userInfo:nil
                                            repeats:NO];
        }
        else{
            mainfinal.hidden = NO;
            finalscreen2.hidden=YES;
            finalscreen.hidden=YES;
            [self addvideofinal];
        }
        
    }
    else
    {
        [self emailinternetcheck];
        email.hidden=NO;
        emailscreen.hidden=NO;
        social.hidden = NO;
        socialview.hidden=NO;
        social.hidden=NO;
        
        
    }
    
    
}


- (IBAction)videorecord:(id)sender
{
    
    NSLog(@"Record Button Pressed");
    stopbutton.hidden=NO;
    recordbutton.hidden = YES;
    isRecording = !isRecording;
    [self timer];
    dispatch_async([self sessionQueue], ^{
        if ([[UIDevice currentDevice] isMultitaskingSupported])
        {
            [self setBackgroundRecordingID:[[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:nil]];
        }
        
        [[[self movieFileOutput] connectionWithMediaType:AVMediaTypeVideo] setVideoOrientation:[[(AVCaptureVideoPreviewLayer *)[[self previewView] layer] connection] videoOrientation]];
        
        [PhotoBoothNormal setFlashMode:AVCaptureFlashModeOff forDevice:[self videoDevice]];
        
        NSString *outputFilePath = [NSTemporaryDirectory() stringByAppendingPathComponent:[@"movie" stringByAppendingPathExtension:@"mov"]];
        [[self movieFileOutput] startRecordingToOutputFileURL:[NSURL fileURLWithPath:outputFilePath] recordingDelegate:self];
        
        
        
    });
}


- (IBAction)videostop:(id)sender
{
    NSLog(@"Record Button Pressed");
    stopbutton.hidden=YES;
    recordbutton.hidden = NO;
    welcome.hidden=YES;
    mainwelcome.hidden=YES;
    mainvideo.hidden=YES;
    video.hidden=YES;
    maineffectsbg1.hidden=YES;
    videotimerview.hidden=YES;
    [[self movieFileOutput] stopRecording];
    [videoTimer invalidate];
    
    
    
    
    
    dispatch_async([self sessionQueue], ^{
        
        
        // Check for device authorization
        [self checkDeviceAuthorizationStatus];
        
        NSLog(@"videorecord stopped Recording");
        [[self movieFileOutput] stopRecording];
        
        [self.session beginConfiguration];
        [self.session removeOutput:self.movieFileOutput];
        [self.session addOutput:self.videoDataOutput];
        
        AVCaptureConnection *connection = [self.videoDataOutput connectionWithMediaType:AVMediaTypeVideo];
        if ([connection isVideoOrientationSupported])
        {
            AVCaptureVideoOrientation newOrientation;
            switch ([[UIDevice currentDevice] orientation]) {
                case UIDeviceOrientationPortrait:
                    newOrientation = AVCaptureVideoOrientationPortrait;
                    break;
                case UIDeviceOrientationPortraitUpsideDown:
                    newOrientation = AVCaptureVideoOrientationPortraitUpsideDown;
                    break;
                case UIDeviceOrientationLandscapeLeft:
                    newOrientation = AVCaptureVideoOrientationLandscapeRight;
                    break;
                case UIDeviceOrientationLandscapeRight:
                    newOrientation = AVCaptureVideoOrientationLandscapeLeft;
                    break;
                default:
                    newOrientation = AVCaptureVideoOrientationLandscapeLeft;
            }
            [connection setVideoOrientation:newOrientation];
        }
        if ([connection isVideoMirroringSupported])
        {
            [connection setVideoMirrored:YES];
        }
        
        if ([connection isVideoOrientationSupported])
        {
            AVCaptureVideoOrientation newOrientation;
            switch ([[UIDevice currentDevice] orientation]) {
                case UIDeviceOrientationPortrait:
                    newOrientation = AVCaptureVideoOrientationPortrait;
                    videoLayer = [CALayer layer];
                    videoLayer.frame = self.previewView.bounds;
                    [self.previewView.layer addSublayer:videoLayer];
                    break;
                    
                    
                    
                case UIDeviceOrientationLandscapeLeft:
                    newOrientation = AVCaptureVideoOrientationLandscapeRight;
                    videoLayer = [CALayer layer];
                    videoLayer.frame = self.previewView.bounds;
                    [self.previewView.layer addSublayer:videoLayer];
                    
                    break;
                case UIDeviceOrientationLandscapeRight:
                    newOrientation = AVCaptureVideoOrientationLandscapeLeft;
                    videoLayer = [CALayer layer];
                    videoLayer.frame = self.previewView.bounds;
                    [self.previewView.layer addSublayer:videoLayer];
                    break;
                default:
                    newOrientation = AVCaptureVideoOrientationLandscapeLeft;
            }
            [connection setVideoOrientation:newOrientation];
        }
        
        
        [self.session commitConfiguration];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            self.session.sessionPreset = AVCaptureSessionPresetPhoto;
            [self.previewView.layer addSublayer:videoLayer];
            NSString * face = [self.record valueForKey:@"switchFacebook"];
            NSString * twit = [self.record valueForKey:@"switchTwitter"];
            NSString * emails = [self.record valueForKey:@"switchEmail"];
            NSString * text = [self.record valueForKey:@"switchText"];
            if ([face  isEqual: @"NO"] && [twit  isEqual: @"NO"] &&[emails  isEqual: @"NO"]&&[text  isEqual: @"NO"])
            {
                
                NSString *videofinal = [self.record valueForKey:@"finalimagevideo"];
                if (videofinal == nil)
                {
                    mainfinal.hidden = NO;
                    final.hidden = NO;
                    [NSTimer scheduledTimerWithTimeInterval:3.5
                                                     target:self
                                                   selector:@selector(newsession)
                                                   userInfo:nil
                                                    repeats:NO];
                }
                else{
                    mainfinal.hidden = NO;
                    finalscreen2.hidden=YES;
                    finalscreen.hidden=YES;
                    [self addvideofinal];
                }
            }
            
            else
            {
                [self emailinternetcheck];
                email.hidden=NO;
                emailscreen.hidden=NO;
                social.hidden = NO;
                socialview.hidden=NO;
                social.hidden=NO;
                
                
            }
            
            
        });
    });
    
}



//TIMER COUNTDOWN FOR VIDEO//

-(void)timer
{
    videoTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(startvideoCountdown) userInfo:nil repeats:true];
}

-(void)startvideoCountdown {
    maincountdown.hidden=YES;
    videotimerview.hidden=NO;
    captureImage1.hidden = YES;
    NSLog(@"IS IT RECORDING");
    dispatch_async( dispatch_get_main_queue(), ^{
        self.recordButton.enabled = YES;
        [[self.recordButton layer] setBackgroundColor:[UIColor redColor].CGColor];
        [self.recordButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        self.recordButton.titleLabel.font = [UIFont systemFontOfSize:19.0];
        [self.recordButton setTitle:NSLocalizedString( @"STOP", @"Recording button stop title" ) forState:UIControlStateNormal];
    });
    if (videotimerInt > 0) {
        videotimerInt--;
        for (UILabel *view in videoLabel) {
            view.text =[NSString stringWithFormat:@"%ld", (long)videotimerInt];
        }
        
    }
    
    if (videotimerInt <= 0) {
        NSString *a = [self.record valueForKey:@"maxSelection2"];
        NSInteger b = [a integerValue];
        videotimerInt = b;
        maincountdown.hidden=YES;
        NSLog(@"Record Button Pressed");
        NSLog(@"Record Button Pressed");
        stopbutton.hidden=NO;
        recordbutton.hidden = YES;
        welcome.hidden=YES;
        mainwelcome.hidden=YES;
        mainvideo.hidden=YES;
        video.hidden=YES;
        maineffectsbg1.hidden=YES;
        videotimerview.hidden=YES;
        [[self movieFileOutput] stopRecording];
        [videoTimer invalidate];
        dispatch_async([self sessionQueue], ^{
            
            
            // Check for device authorization
            [self checkDeviceAuthorizationStatus];
            
            NSLog(@"videorecord stopped Recording");
            [[self movieFileOutput] stopRecording];
            
            [self.session beginConfiguration];
            [self.session removeOutput:self.movieFileOutput];
            [self.session addOutput:self.videoDataOutput];
            
            AVCaptureConnection *connection = [self.videoDataOutput connectionWithMediaType:AVMediaTypeVideo];
            if ([connection isVideoOrientationSupported])
            {
                AVCaptureVideoOrientation newOrientation;
                switch ([[UIDevice currentDevice] orientation]) {
                    case UIDeviceOrientationPortrait:
                        newOrientation = AVCaptureVideoOrientationPortrait;
                        break;
                    case UIDeviceOrientationPortraitUpsideDown:
                        newOrientation = AVCaptureVideoOrientationPortraitUpsideDown;
                        break;
                    case UIDeviceOrientationLandscapeLeft:
                        newOrientation = AVCaptureVideoOrientationLandscapeRight;
                        break;
                    case UIDeviceOrientationLandscapeRight:
                        newOrientation = AVCaptureVideoOrientationLandscapeLeft;
                        break;
                    default:
                        newOrientation = AVCaptureVideoOrientationLandscapeLeft;
                }
                [connection setVideoOrientation:newOrientation];
            }
            if ([connection isVideoMirroringSupported])
            {
                [connection setVideoMirrored:YES];
            }
            
            if ([connection isVideoOrientationSupported])
            {
                AVCaptureVideoOrientation newOrientation;
                switch ([[UIDevice currentDevice] orientation]) {
                    case UIDeviceOrientationPortrait:
                        newOrientation = AVCaptureVideoOrientationPortrait;
                        videoLayer = [CALayer layer];
                        videoLayer.frame = self.previewView.bounds;
                        [self.previewView.layer addSublayer:videoLayer];
                        break;
                        
                        
                        
                    case UIDeviceOrientationLandscapeLeft:
                        newOrientation = AVCaptureVideoOrientationLandscapeRight;
                        videoLayer = [CALayer layer];
                        videoLayer.frame = self.previewView.bounds;
                        [self.previewView.layer addSublayer:videoLayer];
                        
                        break;
                    case UIDeviceOrientationLandscapeRight:
                        newOrientation = AVCaptureVideoOrientationLandscapeLeft;
                        videoLayer = [CALayer layer];
                        videoLayer.frame = self.previewView.bounds;
                        [self.previewView.layer addSublayer:videoLayer];
                        break;
                    default:
                        newOrientation = AVCaptureVideoOrientationLandscapeLeft;
                }
                [connection setVideoOrientation:newOrientation];
            }
            
            
            [self.session commitConfiguration];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                self.session.sessionPreset = AVCaptureSessionPresetPhoto;
                [self.previewView.layer addSublayer:videoLayer];
                NSString * face = [self.record valueForKey:@"switchFacebook"];
                NSString * twit = [self.record valueForKey:@"switchTwitter"];
                NSString * emails = [self.record valueForKey:@"switchEmail"];
                NSString * text = [self.record valueForKey:@"switchText"];
                if ([face  isEqual: @"NO"] && [twit  isEqual: @"NO"] &&[emails  isEqual: @"NO"]&&[text  isEqual: @"NO"])
                {
                    
                    NSString *videofinal = [self.record valueForKey:@"finalimagevideo"];
                    if (videofinal == nil)
                    {
                        mainfinal.hidden = NO;
                        final.hidden = NO;
                        [NSTimer scheduledTimerWithTimeInterval:3.5
                                                         target:self
                                                       selector:@selector(newsession)
                                                       userInfo:nil
                                                        repeats:NO];
                    }
                    else{
                        mainfinal.hidden = NO;
                        finalscreen2.hidden=YES;
                        finalscreen.hidden=YES;
                        [self addvideofinal];
                    }
                }
                
                else
                {
                    [self emailinternetcheck];
                    email.hidden=NO;
                    emailscreen.hidden=NO;
                    social.hidden = NO;
                    socialview.hidden=NO;
                    social.hidden=NO;
                    
                    
                }
                
                
            });
        });
        
    }
}


- (void)videotimerSlider{
    NSString *a = [self.record valueForKey:@"maxSelection2"];
    NSInteger b = [a integerValue];
    videotimerInt = b;
    for (UILabel *view in videoLabel) {
        view.text =[NSString stringWithFormat:@"%ld", (long)videotimerInt];
    }
    
}


//VIDEO OUTPUT//
- (void)captureOutput:(AVCaptureFileOutput *)captureOutput didFinishRecordingToOutputFileAtURL:(NSURL *)outputFileURL fromConnections:(NSArray *)connections error:(NSError *)error
{
    //NSLog(@"captureOutput");
    if (error)
    {
        NSLog(@"%@", error);
    }
    
    [self setLockInterfaceRotation:NO];
    UIBackgroundTaskIdentifier backgroundRecordingID = [self backgroundRecordingID];
    [self setBackgroundRecordingID:UIBackgroundTaskInvalid];
    
    [[[ALAssetsLibrary alloc] init] writeVideoAtPathToSavedPhotosAlbum:outputFileURL completionBlock:^(NSURL *assetURL, NSError *error) {
        if (error)
        {
            NSLog(@"%@", error);
        }
        
        [[NSFileManager defaultManager] removeItemAtURL:outputFileURL error:nil];
        
        if (backgroundRecordingID != UIBackgroundTaskInvalid)
        {
            [[UIApplication sharedApplication] endBackgroundTask:backgroundRecordingID];
        }
    }];
}







//STEP 5: THIS STEP IS FOR SOCIAL SHARING. IF THE USER SKIPS THIS STEP IT WILL GO TO THE FINAL SCREEN//


//EMAIL//

-(IBAction)sendemail:(id)sender
{
    if ([self hasInternetConnection])
    {
        //IF SENDGRID IS ACTIVE//
        NSString * string6 = [self.record valueForKey:@"switchSendgrid"];
        
        
        if ([string6  isEqualToString: @"YES"])
        {
            
            [self sendgridemail:CNPPopupStyleCentered];
            NSLog(@"SENDGRID IS ON");
            
        }
        
        else
        {
            [self nointernetmail:CNPPopupStyleCentered];
        }
    }
    
    else{
        [self nointernetmail:CNPPopupStyleCentered];
    }
}





- (void)emailinternetcheck {
    if ([self hasInternetConnection]) {
        
        NSLog(@"HAS INTERNET");
        NSString * face = [self.record valueForKey:@"switchFacebook"];
        
        if ([face  isEqual: @"YES"])
        {
            facebook.hidden=NO;
            NSLog (@"Facebook SWITCH IS ON");
            
        }
        
        else
        {
            facebook.hidden=YES;
            NSLog (@"Facebook SWITCH IS OFF");
        }
        
        NSString *twit= [self.record valueForKey:@"switchTwitter"];
        
        if ([twit  isEqual: @"YES"])
        {
            twitter.hidden=NO;
            NSLog (@"Twitter SWITCH IS ON");
            
        }
        
        else
        {
            twitter.hidden=YES;
            NSLog (@"Twitter SWITCH IS OFF");
        }
        
        
        NSString * email2 = [self.record valueForKey:@"switchEmail"];
        
        if ([email2  isEqual: @"YES"])
        {
            mail.hidden = NO;
            NSLog (@"Email SWITCH IS ON");
        }
        
        else
        {
            mail.hidden = YES;
            NSLog (@"Email SWITCH IS OFF");
        }
        
        
        if (![[DBSession sharedSession] isLinked])
        {
            texting.hidden=YES;
        }else{
            NSString * text= [self.record valueForKey:@"switchText"];
            
            if ([text  isEqual: @"YES"])
            {
                
                NSString *twilioSID = [[NSUserDefaults standardUserDefaults]
                                       stringForKey:@"twiliosid"];
                
                NSString *twilioAuthKey = [[NSUserDefaults standardUserDefaults]
                                           stringForKey:@"twilioauthkey"];
                
                NSString *fromNumber = [[NSUserDefaults standardUserDefaults]
                                        stringForKey:@"twiliofromnumber"];
                
                
                
                
                if ([twilioSID isEqualToString:@""]||[twilioAuthKey isEqualToString:@""]||[fromNumber isEqualToString:@""])
                {
                    texting.hidden = YES;
                }
                else
                {
                    texting.hidden = NO;
                    
                    NSLog (@"Email SWITCH IS ON");
                }
            }
            
            else
            {
                texting.hidden = YES;
                NSLog (@"Email SWITCH IS OFF");
            }
            
        }
        
        
        
        
        
    }
    
    else{
        
        NSLog(@"NO INTERNET");
        texting.hidden=YES;
        facebook.hidden=YES;
        twitter.hidden=YES;
        mail.hidden=NO;
        
        
    }
    
}

- (void)nointernetmail:(CNPPopupStyle)popupStyle
{
    
    NSLog(@"EMAIL BUTTON PRESSED");
    fontcolor = [UIColor whiteColor];
    backgroundcolor1  = [UIColor darkGrayColor];
    
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 85, 85)];
    imageView.image = [UIImage imageNamed:@"Email.png"];
      imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    
    UIView *customView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 45)];
    customView.backgroundColor = backgroundcolor1;
    UIView *spacerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    UILabel *lineTwoLabel = [[UILabel alloc] init];
    UITextField *textFied = [[UITextField alloc] initWithFrame:CGRectMake(1,1, 318, 43)];
    [textFied setBackgroundColor:fontcolor];
    [textFied setValue: [UIColor colorWithRed:79.0/255.0 green:127.0/255.0 blue:226.0/255.0 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    textFied.placeholder = @"Enter your email address";
    textFied.text = emailaddress1.text;
    [textFied setFont:[UIFont fontWithName:@"Helvetica Neue" size:22]];
    textFied.delegate = self;
    [textFied setLeftViewMode:UITextFieldViewModeAlways];
    [textFied setLeftView:spacerView];
    
    [customView addSubview:lineTwoLabel];
    [customView addSubview:textFied];
    
    UIView *customView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 340, 65)];
    
    CNPPopupButton *button1 = [[CNPPopupButton alloc] initWithFrame:CGRectMake(20, 10, 140, 45)];
    [button1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button1.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button1 setTitle:@"CANCEL" forState:UIControlStateNormal];
    [button1 setTitleColor:[UIColor colorWithRed:79.0/255.0 green:127.0/255.0 blue:226.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    button1.backgroundColor = fontcolor;
    [[button1 layer] setBorderWidth:1.0f];
    [[button1 layer] setBorderColor:[UIColor colorWithRed:79.0/255.0 green:127.0/255.0 blue:226.0/255.0 alpha:1.0].CGColor];
    button1.layer.cornerRadius = 15;
    button1.selectionHandler = ^(CNPPopupButton *button1){
        [self.popupController dismissPopupControllerAnimated:YES];
        
    };
    CNPPopupButton *button = [[CNPPopupButton alloc] initWithFrame:CGRectMake(180, 10, 140, 45)];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button setTitle:@"SEND EMAIL" forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.backgroundColor = [UIColor colorWithRed:79.0/255.0 green:127.0/255.0 blue:226.0/255.0 alpha:1.0];
    button.layer.cornerRadius = 15;
    button.selectionHandler = ^(CNPPopupButton *button){
        [self.popupController dismissPopupControllerAnimated:YES];
        if ([self validateEmail:textFied.text] == 1) {
            
            NSString *valueToSave = textFied.text;
            [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"emailaddress1"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            NSLog(@"TextField Saved: %@", valueToSave);
            [self nointernetmailcomposer];
            email.hidden=NO;
            emailscreen.hidden=NO;
            social.hidden = NO;
            socialview.hidden=NO;
            social.hidden=NO;
            
            NSString *resultLine=[NSString stringWithFormat:@" %@\n",
                                  
                                  
                                  textFied.text
                                 ];
            
            NSString *eventname = [self.record valueForKey:@"eventname"];
            NSString *docPath =[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0];
            NSString *surveys=[docPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_emails.csv", eventname]];
            
            if (![[NSFileManager defaultManager] fileExistsAtPath:surveys]) {
                [[NSFileManager defaultManager]
                 createFileAtPath:surveys contents:nil attributes:nil];
            }
            
            NSFileHandle *fileHandle = [NSFileHandle fileHandleForUpdatingAtPath:surveys];
            [fileHandle seekToEndOfFile];
            [fileHandle writeData:[resultLine dataUsingEncoding:NSUTF8StringEncoding]];
            [fileHandle closeFile];
            
            
            
            
        }
        
        
        else {
            NSLog(@"VALIDATE EMAIL DONE");
            [self tryagain:CNPPopupStyleCentered];
        }
        
    };
    
    
    
    [customView1 addSubview:button1];
    [customView1 addSubview:button];
    self.popupController = [[CNPPopupController alloc] initWithContents:@[imageView, customView, customView1]];
    self.popupController.theme = [CNPPopupTheme defaultTheme];
    self.popupController.theme.popupStyle = popupStyle;
    self.popupController.delegate = self;
    [self.popupController presentPopupControllerAnimated:NO];
    
    
    
}

-(void)nointernetmailcomposer
{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString* strValue =[defaults objectForKey:@"emailsubject"];
    NSLog(@"Email subject %@", strValue);
    
    NSUserDefaults *defaults1 = [NSUserDefaults standardUserDefaults];
    NSString* strValue1 = [defaults1 objectForKey:@"emailmessage"];
    NSLog(@"Email message %@", strValue1);
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"emailaddress1"];
    
    MFMailComposeViewController* composeVC = [[MFMailComposeViewController alloc] init];
    composeVC.mailComposeDelegate = self;
    
    // Configure the fields of the interface.
    
    NSArray *usersTo = [NSArray arrayWithObject: [NSString stringWithFormat:@"%@", savedValue]];
    NSString *subject = [NSString stringWithFormat:@"%@", strValue];
    NSString *message = [NSString stringWithFormat:@"%@", strValue1];
    
    [composeVC setToRecipients:usersTo];
    
    if ([strValue isEqualToString:@""] || strValue == nil)
        [composeVC setSubject:@"Your photobooth pictures"];
    else
        [composeVC setSubject:subject];
    
    if ([strValue1 isEqualToString:@""] || strValue1 == nil)
        [composeVC setMessageBody: @"We hope you enjoyed yourself in the photo booth. Please feel free to use these photos as you wish." isHTML:NO];
    
    else
        [composeVC setMessageBody:message isHTML:YES];
    
    
    NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
    NSData *data6 = [currentDefaults objectForKey:@"layoutphoto"];
    UIImage * myImage = [NSKeyedUnarchiver unarchiveObjectWithData:data6];
    
    NSString * string61 = [self.record valueForKey:@"switchIndividual"];
    NSString * string62 = [self.record valueForKey:@"switchLayout"];
    
    if ([string61 isEqualToString:@"YES"] && [string62 isEqualToString:@"YES"])
    {
        for (int i = 0; i < photostaken.count; i++)
        {
            [composeVC addAttachmentData:[photostaken objectAtIndex:i] mimeType:@"image/jpeg" fileName:[NSString stringWithFormat:@"Individual_%d.jpeg", i]];
        }
        [composeVC addAttachmentData:UIImageJPEGRepresentation(myImage, 0.5) mimeType:@"image/jpeg" fileName:[NSString stringWithFormat:@"Layout_%@.jpeg", myImage]];
    }
    else if ([string61 isEqualToString:@"YES"] && [string62 isEqualToString:@"NO"])
    {
        for (int i = 0; i < photostaken.count; i++)
        {
            [composeVC addAttachmentData:[photostaken objectAtIndex:i] mimeType:@"image/jpeg" fileName:[NSString stringWithFormat:@"Individual_%d.jpeg", i]];
        }
    }
    
    else if ([string61 isEqualToString:@"NO"] && [string62 isEqualToString:@"YES"])
    {
        [composeVC addAttachmentData:UIImageJPEGRepresentation(myImage, 0.5) mimeType:@"image/jpeg" fileName:[NSString stringWithFormat:@"Layout_%@.jpeg", myImage]];
        
    }
    
    
    
    [self presentViewController:composeVC animated:YES completion:nil];
    
    
    //     NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
    //    [currentDefaults removeObjectForKey:@"yourKeyName"];
    
}

-(void)tryagain:(CNPPopupStyle)popupStyle
{
    fontcolor = [UIColor whiteColor];
    backgroundcolor1  = [UIColor darkGrayColor];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 85, 85)];
    imageView.image = [UIImage imageNamed:@"Email.png"];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    
    UILabel *titleLabel = [[UILabel alloc] init];
    NSAttributedString *lineTwo = [[NSAttributedString alloc] initWithString:@"Looks like you entered an incorrect email address. Would you like to try again?" attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:18], NSForegroundColorAttributeName : [UIColor darkGrayColor], NSParagraphStyleAttributeName : paragraphStyle}];
    titleLabel.numberOfLines = 2;
    titleLabel.attributedText = lineTwo;
    
    UIView *customView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 340, 65)];
    
    CNPPopupButton *button1 = [[CNPPopupButton alloc] initWithFrame:CGRectMake(20, 10, 140, 45)];
    [button1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button1.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button1 setTitle:@"CANCEL" forState:UIControlStateNormal];
    [button1 setTitleColor:[UIColor colorWithRed:79.0/255.0 green:127.0/255.0 blue:226.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    button1.backgroundColor = fontcolor;
    [[button1 layer] setBorderWidth:1.0f];
    [[button1 layer] setBorderColor:[UIColor colorWithRed:79.0/255.0 green:127.0/255.0 blue:226.0/255.0 alpha:1.0].CGColor];
    button1.layer.cornerRadius = 15;
    button1.selectionHandler = ^(CNPPopupButton *button1){
        [self.popupController dismissPopupControllerAnimated:YES];
        
    };
    CNPPopupButton *button = [[CNPPopupButton alloc] initWithFrame:CGRectMake(180, 10, 140, 45)];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button setTitle:@"TRY AGAIN" forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.backgroundColor = [UIColor colorWithRed:79.0/255.0 green:127.0/255.0 blue:226.0/255.0 alpha:1.0];
    button.layer.cornerRadius = 15;
    button.selectionHandler = ^(CNPPopupButton *button){
        [self.popupController dismissPopupControllerAnimated:YES];
        [self nointernetmail:CNPPopupStyleCentered];
    };
    
    
    
    [customView1 addSubview:button1];
    [customView1 addSubview:button];
    self.popupController = [[CNPPopupController alloc] initWithContents:@[imageView, titleLabel, customView1]];
    self.popupController.theme = [CNPPopupTheme defaultTheme];
    self.popupController.theme.popupStyle = popupStyle;
    self.popupController.delegate = self;
    [self.popupController presentPopupControllerAnimated:NO];
    
    
    
}



- (void)sendgridemail:(CNPPopupStyle)popupStyle
{
    
    NSLog(@"EMAIL BUTTON PRESSED");
    fontcolor = [UIColor whiteColor];
    backgroundcolor1  = [UIColor darkGrayColor];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 85, 85)];
    imageView.image = [UIImage imageNamed:@"mailicon.png"];
      imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    UIView *customView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 45)];
    customView.backgroundColor = [UIColor colorWithRed:79.0/255.0 green:127.0/255.0 blue:226.0/255.0 alpha:1.0];
    UIView *spacerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    UILabel *lineTwoLabel = [[UILabel alloc] init];
    UITextField *textFied = [[UITextField alloc] initWithFrame:CGRectMake(1,1, 318, 43)];
    [textFied setBackgroundColor:fontcolor];
    [textFied setValue: [UIColor colorWithRed:79.0/255.0 green:127.0/255.0 blue:226.0/255.0 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    textFied.placeholder = @"Enter your email address";
    textFied.text = emailaddress1.text;
    textFied.delegate = self;
    [textFied setLeftViewMode:UITextFieldViewModeAlways];
    [textFied setLeftView:spacerView];
    
    [customView addSubview:lineTwoLabel];
    [customView addSubview:textFied];
    
    UIView *customView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 340, 65)];
    
    CNPPopupButton *button1 = [[CNPPopupButton alloc] initWithFrame:CGRectMake(20, 10, 140, 45)];
    [button1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button1.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button1 setTitle:@"CANCEL" forState:UIControlStateNormal];
    [button1 setTitleColor:[UIColor colorWithRed:79.0/255.0 green:127.0/255.0 blue:226.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    button1.backgroundColor = fontcolor;
    [[button1 layer] setBorderWidth:1.0f];
    [[button1 layer] setBorderColor:[UIColor colorWithRed:79.0/255.0 green:127.0/255.0 blue:226.0/255.0 alpha:1.0].CGColor];
    button1.layer.cornerRadius = 15;
    button1.selectionHandler = ^(CNPPopupButton *button1){
        [self.popupController dismissPopupControllerAnimated:YES];
        
    };
    CNPPopupButton *button = [[CNPPopupButton alloc] initWithFrame:CGRectMake(180, 10, 140, 45)];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button setTitle:@"SEND EMAIL" forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.backgroundColor = [UIColor colorWithRed:79.0/255.0 green:127.0/255.0 blue:226.0/255.0 alpha:1.0];
    button.layer.cornerRadius = 15;
    button.selectionHandler = ^(CNPPopupButton *button){
        [self.popupController dismissPopupControllerAnimated:YES];
        if ([self validateEmail:textFied.text] == 1) {
            
            NSString *valueToSave = textFied.text;
            [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"emailaddress1"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            NSLog(@"TextField Saved: %@", valueToSave);
            [self sendgridmailcomposer];
            email.hidden=NO;
            emailscreen.hidden=NO;
            social.hidden = NO;
            socialview.hidden=NO;
       
            NSString *resultLine=[NSString stringWithFormat:@" %@\n",
                                  
                                  
                                  textFied.text
                                  ];
            
            
            
            
            
            NSString *eventname = [self.record valueForKey:@"eventname"];
            NSString *docPath =[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0];
            NSString *surveys=[docPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_emails.csv", eventname]];
            
            if (![[NSFileManager defaultManager] fileExistsAtPath:surveys]) {
                [[NSFileManager defaultManager]
                 createFileAtPath:surveys contents:nil attributes:nil];
            }
            
            NSFileHandle *fileHandle = [NSFileHandle fileHandleForUpdatingAtPath:surveys];
            [fileHandle seekToEndOfFile];
            [fileHandle writeData:[resultLine dataUsingEncoding:NSUTF8StringEncoding]];
            [fileHandle closeFile];

            
            
            
        }
        
        
        else {
            NSLog(@"VALIDATE EMAIL DONE");
            [self tryagain:CNPPopupStyleCentered];
        }
        
    };
    
    
    
    [customView1 addSubview:button1];
    [customView1 addSubview:button];
    self.popupController = [[CNPPopupController alloc] initWithContents:@[imageView, customView, customView1]];
    self.popupController.theme = [CNPPopupTheme defaultTheme];
    self.popupController.theme.popupStyle = popupStyle;
    self.popupController.delegate = self;
    [self.popupController presentPopupControllerAnimated:NO];
    
    
    
}



-(void)sendgridmailcomposer
{
    [KVNProgress showWithStatus:@"Please wait while your photos are being sent."];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString* strValue =[defaults objectForKey:@"emailsubject"];
    NSLog(@"Email subject %@", strValue);
    
    NSUserDefaults *defaults1 = [NSUserDefaults standardUserDefaults];
    NSString* strValue1 = [defaults1 objectForKey:@"emailmessage"];
    NSLog(@"Email message %@", strValue1);
    
    
    NSUserDefaults *defaults2 = [NSUserDefaults standardUserDefaults];
    NSString* strValue2 =[defaults2 objectForKey:@"username"];
    NSLog(@"Username %@", strValue2);
    
    NSUserDefaults *defaults3 = [NSUserDefaults standardUserDefaults];
    NSString* strValue3 = [defaults3 objectForKey:@"password"];
    NSLog(@"Password %@", strValue3);
    
    NSUserDefaults *defaults4 = [NSUserDefaults standardUserDefaults];
    NSString* strValue4 =[defaults4 objectForKey:@"ipixemail"];
    NSLog(@"Email %@", strValue4);
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"emailaddress1"];
    
    SendGrid *sendgrid = [SendGrid apiUser:[NSString stringWithFormat:@"%@", strValue2] apiKey:[NSString stringWithFormat:@"%@", strValue3]];
    SendGridEmail *email4 = [[SendGridEmail alloc] init];
    
    
    
    if ([strValue isEqualToString:@""] || strValue == nil)
        email4.subject = @"Your photobooth pictures";
    else
        email4.subject = [NSString stringWithFormat:@"%@", strValue];
    
    if ([strValue1 isEqualToString:@""] || strValue1 == nil)
        email4.html = @"We hope you enjoyed yourself in the photo booth. Please feel free to use these photos as you wish.";
    else
        email4.html = [NSString stringWithFormat:@"%@", strValue1];
    
    if ([strValue1 isEqualToString:@""] || strValue1 == nil)
        email4.text = @"We hope you enjoyed yourself in the photo booth. Please feel free to use these photos as you wish.";
    else
        email4.text = [NSString stringWithFormat:@"%@", strValue1];
    
    
    email4.to = [NSString stringWithFormat:@"%@", savedValue];
    email4.from = [NSString stringWithFormat:@"%@", strValue4];
    
    NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
    NSData *data6 = [currentDefaults objectForKey:@"layoutphoto"];
    UIImage * myImage = [NSKeyedUnarchiver unarchiveObjectWithData:data6];
    
    NSString * string61 = [self.record valueForKey:@"switchIndividual"];
    NSString * string62 = [self.record valueForKey:@"switchLayout"];
    
    if ([string61 isEqualToString:@"YES"] && [string62 isEqualToString:@"YES"])
    {
        for (int i = 0; i < photostaken.count; i++)
        {
            SendGridEmailAttachment* someImageAttachment = [[SendGridEmailAttachment alloc] init];
            someImageAttachment.attachmentData = [photostaken objectAtIndex:i];
            someImageAttachment.mimeType = @"image/jpeg";
            someImageAttachment.fileName = [NSString stringWithFormat:@"Individual_%d.jpeg", i];
            someImageAttachment.extension = @"jpeg";
            [email4 attachFile:someImageAttachment];
        }
        
        SendGridEmailAttachment* someImageAttachment = [[SendGridEmailAttachment alloc] init];
        someImageAttachment.attachmentData = UIImageJPEGRepresentation(myImage, 0.5);
        someImageAttachment.mimeType = @"image/jpeg";
        someImageAttachment.fileName = [NSString stringWithFormat:@"Layout.jpeg"];
        someImageAttachment.extension = @"jpeg";
        [email4 attachFile:someImageAttachment];
        
    }
    else if ([string61 isEqualToString:@"YES"] && [string62 isEqualToString:@"NO"])
    {
        for (int i = 0; i < photostaken.count; i++)
        {
            SendGridEmailAttachment* someImageAttachment = [[SendGridEmailAttachment alloc] init];
            someImageAttachment.attachmentData = [photostaken objectAtIndex:i];
            someImageAttachment.mimeType = @"image/jpeg";
            someImageAttachment.fileName = [NSString stringWithFormat:@"Individual_%d.jpeg", i];
            someImageAttachment.extension = @"jpeg";
            [email4 attachFile:someImageAttachment];
        }
        
        
    }
    
    else if ([string61 isEqualToString:@"NO"] && [string62 isEqualToString:@"YES"])
    {
        SendGridEmailAttachment* someImageAttachment = [[SendGridEmailAttachment alloc] init];
        someImageAttachment.attachmentData = UIImageJPEGRepresentation(myImage, 0.5);
        someImageAttachment.mimeType = @"image/jpeg";
        someImageAttachment.fileName = [NSString stringWithFormat:@"Layout.jpeg"];
        someImageAttachment.extension = @"jpeg";
        [email4 attachFile:someImageAttachment];
        
    }
    
    
    
    [sendgrid sendAttachmentWithWeb:email4
                       successBlock:^(id responseObject)
     {
         NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
         [currentDefaults removeObjectForKey:@"layoutphoto"];
         [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"emailaddress1"];
         [[NSUserDefaults standardUserDefaults] synchronize];
         [KVNProgress dismiss];
         NSLog(@"YES IT WORKED");
         NSString *videofinal = [self.record valueForKey:@"finalimagevideo"];
         if (videofinal == nil)
         {
             
             email.hidden=YES;
             emailscreen.hidden=YES;
             social.hidden = YES;
             socialview.hidden=YES;
             social.hidden=YES;
             
             
             mainfinal.hidden = NO;
             final.hidden = NO;
             finalscreen2.hidden=NO;
             finalscreen.hidden=NO;
             [NSTimer scheduledTimerWithTimeInterval:3.5
                                              target:self
                                            selector:@selector(newsession)
                                            userInfo:nil
                                             repeats:NO];
         }
         else{
             email.hidden=YES;
             emailscreen.hidden=YES;
             social.hidden = YES;
             socialview.hidden=YES;
             social.hidden=YES;
             
             
             mainfinal.hidden = NO;
             finalscreen2.hidden=YES;
             finalscreen.hidden=YES;
             [self addvideofinal];
         }
         
         
         
         
     }
                       failureBlock:^(NSError *error)
     {
         MFMailComposeViewController* composeVC = [[MFMailComposeViewController alloc] init];
         composeVC.mailComposeDelegate = self;
         
         // Configure the fields of the interface.
         NSArray *usersTo = [NSArray arrayWithObject: [NSString stringWithFormat:@"%@", savedValue]];
         NSString *subject = [NSString stringWithFormat:@"%@", strValue];
         NSString *message = [NSString stringWithFormat:@"%@", strValue1];
         
         [composeVC setToRecipients:usersTo];
         
         if ([strValue isEqualToString:@""] || strValue == nil)
             [composeVC setSubject:@"Your photobooth pictures"];
         else
             [composeVC setSubject:subject];
         
         if ([strValue1 isEqualToString:@""] || strValue1 == nil)
             [composeVC setMessageBody: @"We hope you enjoyed yourself in the photo booth. Please feel free to use these photos as you wish." isHTML:NO];
         
         else
             [composeVC setMessageBody:message isHTML:YES];
         
         NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
         NSData *data6 = [currentDefaults objectForKey:@"layoutphoto"];
         UIImage * myImage = [NSKeyedUnarchiver unarchiveObjectWithData:data6];
         
         NSString * string61 = [self.record valueForKey:@"switchIndividual"];
         NSString * string62 = [self.record valueForKey:@"switchLayout"];
         
         if ([string61 isEqualToString:@"YES"] && [string62 isEqualToString:@"YES"])
         {
             for (int i = 0; i < photostaken.count; i++)
             {
                 [composeVC addAttachmentData:[photostaken objectAtIndex:i] mimeType:@"image/jpeg" fileName:[NSString stringWithFormat:@"Individual_%d.jpeg", i]];
             }
             [composeVC addAttachmentData:UIImageJPEGRepresentation(myImage, 0.5) mimeType:@"image/jpeg" fileName:[NSString stringWithFormat:@"Layout_%@.jpeg", myImage]];
         }
         else if ([string61 isEqualToString:@"YES"] && [string62 isEqualToString:@"NO"])
         {
             for (int i = 0; i < photostaken.count; i++)
             {
                 [composeVC addAttachmentData:[photostaken objectAtIndex:i] mimeType:@"image/jpeg" fileName:[NSString stringWithFormat:@"Individual_%d.jpeg", i]];
             }
         }
         
         else if ([string61 isEqualToString:@"NO"] && [string62 isEqualToString:@"YES"])
         {
             [composeVC addAttachmentData:UIImageJPEGRepresentation(myImage, 0.5) mimeType:@"image/jpeg" fileName:[NSString stringWithFormat:@"Layout_%@.jpeg", myImage]];
             
         }
         
         [self presentViewController:composeVC animated:YES completion:nil];
         
     }];
    
    
    
}

-(IBAction)skipsocial:(id)sender
{
    NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
    [currentDefaults removeObjectForKey:@"layoutphoto"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"emailaddress1"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSString *videofinal = [self.record valueForKey:@"finalimagevideo"];
    if (videofinal == nil)
    {
        email.hidden=YES;
        emailscreen.hidden=YES;
        social.hidden = YES;
        socialview.hidden=YES;
        social.hidden=YES;
        
        mainfinal.hidden = NO;
        final.hidden = NO;
        finalscreen2.hidden=NO;
        finalscreen.hidden=NO;
        [NSTimer scheduledTimerWithTimeInterval:3.5
                                         target:self
                                       selector:@selector(newsession)
                                       userInfo:nil
                                        repeats:NO];
    }
    else{
        email.hidden=YES;
        emailscreen.hidden=YES;
        social.hidden = YES;
        socialview.hidden=YES;
        social.hidden=YES;
        
        mainfinal.hidden = NO;
        finalscreen2.hidden=YES;
        finalscreen.hidden=YES;
        [self addvideofinal];
    }
}

- (BOOL) hasInternetConnection {
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        return false;
    } else {
        return true;
    }
    
    
}

- (BOOL) isWifiConnected {
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == ReachableViaWiFi) {
        return true;
    } else {
        return false;
    }
    
}


-(BOOL)validateEmail:(NSString *)candidate {
    
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    //	return 0;
    return [emailTest evaluateWithObject:candidate];
}


-(void)mailComposeController:(MFMailComposeViewController *)composeVC didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    
    if (result == MFMailComposeResultSent) {
        @autoreleasepool {
            [composeVC dismissViewControllerAnimated:YES completion:nil];
            
            NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
            [currentDefaults removeObjectForKey:@"layoutphoto"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"emailaddress1"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            NSString *videofinal = [self.record valueForKey:@"finalimagevideo"];
            if (videofinal == nil)
            {
                email.hidden=YES;
                emailscreen.hidden=YES;
                social.hidden = YES;
                socialview.hidden=YES;
                social.hidden=YES;
                mainfinal.hidden = NO;
                final.hidden = NO;
                finalscreen2.hidden=NO;
                finalscreen.hidden=NO;
                [NSTimer scheduledTimerWithTimeInterval:3.5
                                                 target:self
                                               selector:@selector(newsession)
                                               userInfo:nil
                                                repeats:NO];
            }
            
            
            else{
                email.hidden=YES;
                emailscreen.hidden=YES;
                social.hidden = YES;
                socialview.hidden=YES;
                social.hidden=YES;
                
                mainfinal.hidden = NO;
                finalscreen2.hidden=YES;
                finalscreen.hidden=YES;
                [self addvideofinal];
            }
            
        }
        
    }
    
    else if (result == MFMailComposeResultCancelled) {
        email.hidden=NO;
        emailscreen.hidden=NO;
        social.hidden = NO;
        socialview.hidden=NO;
        
        [txtFldEmail resignFirstResponder];
        [composeVC dismissViewControllerAnimated:YES completion:nil];
        
    }
    
    
    
    
    
    
}



//TWITTER//


-(IBAction)sendTwitter:(id)sender
{
    UIViewController *loginController = [[FHSTwitterEngine sharedEngine]loginControllerWithCompletionHandler:^(BOOL success) {
        NSLog(success?@"L0L success":@"O noes!!! Loggen faylur!!!");
        [self postTweet];
        
        
    }];
    [self presentViewController:loginController animated:YES completion:nil];
}



- (void)postTweet {
    
    //    for (int i = 0; i < [photostaken count]; i++)
    //    {
    //    photostaken = [NSMutableArray arrayWithObjects:
    //                           @"india.jpg",
    //                           @"australia.jpg",
    //                           @"singapore.jpg",
    //                           @"america.jpg",
    //                           nil];
    //
    //    UIImage *image = [UIImage imageNamed:[photostaken objectAtIndex:0]];
    //    NSData *imageData = UIImageJPEGRepresentation(image, 1.0);
    //    NSLog (@"IMAGE DATA %@", imageData);
    //    [[FHSTwitterEngine sharedEngine]postTweet:@"Check out my photo from the!" withImageData:imageData];
    //
    
    NSUserDefaults *defaults2 = [NSUserDefaults standardUserDefaults];
    NSString* string64 =[defaults2 objectForKey:@"twitterhashtag"];
    NSData *data6 = [defaults2 objectForKey:@"layoutphoto"];
    UIImage * myImage = [NSKeyedUnarchiver unarchiveObjectWithData:data6];
    NSString * string61 = [self.record valueForKey:@"switchTwitterIndividual"];
    NSString * string62 = [self.record valueForKey:@"switchTwitterLayout"];
    
    if ([string61 isEqualToString:@"YES"] && [string62 isEqualToString:@"YES"])
    {
        
        
        for (int i = 0; i < [photostaken count]; i++)
        {
            
            
            UIImage *img = [UIImage imageWithData:[_arrSlidshowImg objectAtIndex:i]];
            
            NSData *data12 = UIImageJPEGRepresentation(img, 0.4);
            
            if ([string64 isEqualToString:@""] || string64 == nil)
            {
                [[FHSTwitterEngine sharedEngine]postTweet:[NSString stringWithFormat:@"We had a blast. Here's one of our photos."] withImageData:data12];
            }
            
            else
            {
                [[FHSTwitterEngine sharedEngine]postTweet:[NSString stringWithFormat:@"%@", string64] withImageData:data12];
            }
            
            
            
        }
        
        if ([string64 isEqualToString:@""] || string64 == nil)
        {
            NSData *data13 = UIImageJPEGRepresentation(myImage, 0.4);
            [[FHSTwitterEngine sharedEngine]postTweet:[NSString stringWithFormat:@"We had a blast. Here's one of our photos."] withImageData:data13];
        }
        
        else
        {
            NSData *data13 = UIImageJPEGRepresentation(myImage, 0.4);
            [[FHSTwitterEngine sharedEngine]postTweet:[NSString stringWithFormat:@"%@", string64] withImageData:data13];
        }
        
        
        
        
    }
    else if ([string61 isEqualToString:@"YES"] && [string62 isEqualToString:@"NO"])
    {
        for (int i = 0; i < photostaken.count; i++)
        {
            
            UIImage *img = [UIImage imageWithData:[_arrSlidshowImg objectAtIndex:i]];
            
            NSData *data12 = UIImageJPEGRepresentation(img, 0.4);
            
            if ([string64 isEqualToString:@""] || string64 == nil)
            {
                [[FHSTwitterEngine sharedEngine]postTweet:[NSString stringWithFormat:@"We had a blast. Here's one of our photos."] withImageData:data12];
            }
            
            else
            {
                [[FHSTwitterEngine sharedEngine]postTweet:[NSString stringWithFormat:@"%@", string64] withImageData:data12];
            }
            
        }
    }
    
    else if ([string61 isEqualToString:@"NO"] && [string62 isEqualToString:@"YES"])
    {
        if ([string64 isEqualToString:@""] || string64 == nil)
        {
            NSData *data13 = UIImageJPEGRepresentation(myImage, 0.4);
            [[FHSTwitterEngine sharedEngine]postTweet:[NSString stringWithFormat:@"We had a blast. Here's one of our photos."] withImageData:data13];
        }
        
        else
        {
            NSData *data13 = UIImageJPEGRepresentation(myImage, 0.4);
            [[FHSTwitterEngine sharedEngine]postTweet:[NSString stringWithFormat:@"%@", string64] withImageData:data13];
        }
        
    }
    
    
    
    
    [self logout];
    
    //}
    
}

- (void)logout {
    [[FHSTwitterEngine sharedEngine] clearAccessToken];
    NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
    [currentDefaults removeObjectForKey:@"layoutphoto"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSString *videofinal = [self.record valueForKey:@"finalimagevideo"];
    if (videofinal == nil)
    {
        email.hidden=YES;
        emailscreen.hidden=YES;
        social.hidden = YES;
        socialview.hidden=YES;
        social.hidden=YES;
        mainfinal.hidden = NO;
        final.hidden = NO;
        finalscreen2.hidden=NO;
        finalscreen.hidden=NO;
        [NSTimer scheduledTimerWithTimeInterval:3.5
                                         target:self
                                       selector:@selector(newsession)
                                       userInfo:nil
                                        repeats:NO];
    }
    
    
    else{
        email.hidden=YES;
        emailscreen.hidden=YES;
        social.hidden = YES;
        socialview.hidden=YES;
        social.hidden=YES;
        
        mainfinal.hidden = NO;
        finalscreen2.hidden=YES;
        finalscreen.hidden=YES;
        [self addvideofinal];
    }
    
    
}


//FACEBOOK//

-(IBAction)sendFacebook:(id)sender
{
    
    
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    
    login.loginBehavior = FBSDKLoginBehaviorWeb;
    
    [login logInWithPublishPermissions:@[@"publish_actions"]
     
                    fromViewController:self
     
                               handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                                   
                                   if (error) {
                                       
                                       NSLog(@"Process error");
                                       
                                   } else if (result.isCancelled) {
                                       
                                       NSLog(@"Cancelled");
                                       
                                   } else {
                                       
                                       NSLog(@"Logged in");
                                       
                                       
                                       [self shareSegmentWithFacebookComposer];
                                       
                                       
                                       
                                       
                                   }
                                   
                               }];
    
}


-(void)shareSegmentWithFacebookComposer{
    if ([[FBSDKAccessToken currentAccessToken] hasGranted:@"publish_actions"]) {
        NSLog(@"has publish permissions");
        
        
       [self facebookoption:CNPPopupStyleCentered];; //publish
        
    } else {
        NSLog(@"no publish permissions"); // no publish permissions so get them, then post
        FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
        [loginManager logInWithPublishPermissions:@[@"publish_actions"]
                               fromViewController:self
                                          handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                                              [self facebookoption:CNPPopupStyleCentered];
                                          }];
        
        
        
    }
}

-(void) publishFBPost1{
    
  
    
    NSUserDefaults *defaults2 = [NSUserDefaults standardUserDefaults];
    NSData *data6 = [defaults2 objectForKey:@"layoutphoto"];
    UIImage * myImage = [NSKeyedUnarchiver unarchiveObjectWithData:data6];
    NSString * string61 = [self.record valueForKey:@"switchFacebookIndividual"];
    NSString * string62 = [self.record valueForKey:@"switchFacebookLayout"];
    NSString* strValue =[defaults2 objectForKey:@"fbwebsite"];
    NSString* strValue1 =[defaults2 objectForKey:@"fbtitle"];
    NSString* strValue2 =[defaults2 objectForKey:@"fbdescription"];
    NSString* strValue3 =[defaults2 objectForKey:@"fblogo"];
    NSMutableArray* photos = [[NSMutableArray alloc] init];
    
//    if ([FBSDKAccessToken currentAccessToken]){
//        NSDictionary *params = @{
//                                 @"message": [NSString stringWithFormat:@"%@", hashtag],
//                                 
//                                 };
//        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"/me/feed" parameters:params HTTPMethod:@"POST"]startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
//            if (error)
//                NSLog(@"error:%@", error);
//            else{
//                //                UIAlertView *av = [[UIAlertView alloc]initWithTitle:@"FBSDKGraphRequest" message:@"Text Shared successfully..!" delegate:self cancelButtonTitle:@"Done" otherButtonTitles:nil, nil];
//                //                [av show];
//            }
//        }];
//    }
    
  
    
    
    if ([string61 isEqualToString:@"YES"] && [string62 isEqualToString:@"YES"])
    {
        
        
        
        for (int i = 0; i < [photostaken count]; i++)
        {
            
            UIImage *image = [UIImage imageWithData:[_arrSlidshowImg objectAtIndex:i]];
            FBSDKSharePhoto *photo = [[FBSDKSharePhoto alloc] init];
            photo.image = image;
            photo.userGenerated = YES;
            [photos addObject:photo];
            
            
            
        }
        
        FBSDKSharePhoto *photo = [[FBSDKSharePhoto alloc] init];
        photo.image = myImage;
        photo.userGenerated = YES;
        [photos addObject:photo];
        
        
        
        
    }
    else if ([string61 isEqualToString:@"YES"] && [string62 isEqualToString:@"NO"])
    {
        for (int i = 0; i < photostaken.count; i++)
        {
            
            UIImage *image = [UIImage imageWithData:[_arrSlidshowImg objectAtIndex:i]];
            FBSDKSharePhoto *photo = [[FBSDKSharePhoto alloc] init];
            photo.image = image;
            photo.userGenerated = YES;
            [photos addObject:photo];
            
        }
    }
    
    else if ([string61 isEqualToString:@"NO"] && [string62 isEqualToString:@"YES"])
    {
        FBSDKSharePhoto *photo = [[FBSDKSharePhoto alloc] init];
        photo.image = myImage;
        photo.userGenerated = YES;
        [photos addObject:photo];
    }
    
    

    
    //   //THIS IS THE PHOTO ONLY CODE
    FBSDKSharePhotoContent *content = [[FBSDKSharePhotoContent alloc] init];
    content.photos = [photos copy];
    [FBSDKShareAPI shareWithContent:content delegate:nil];
    
      FBSDKShareLinkContent *linkContent = [[FBSDKShareLinkContent alloc] init];
         linkContent.contentURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@", strValue]];
         linkContent.contentTitle=[NSString stringWithFormat:@"%@", strValue1];
         linkContent.contentDescription=[NSString stringWithFormat:@"%@", strValue2];
         linkContent.imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@", strValue3]];
         [FBSDKShareAPI shareWithContent:linkContent delegate:nil];
 
    
    NSLog (@"FB VALUES %@ %@ %@ %@", strValue, strValue1, strValue2, strValue3);
  
    
    NSString *videofinal = [self.record valueForKey:@"finalimagevideo"];
    if (videofinal == nil)
    {
        NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
        [currentDefaults removeObjectForKey:@"layoutphoto"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        email.hidden=YES;
        emailscreen.hidden=YES;
        social.hidden = YES;
        socialview.hidden=YES;
        social.hidden=YES;
        
        mainfinal.hidden = NO;
        final.hidden = NO;
        finalscreen2.hidden=NO;
        finalscreen.hidden=NO;
        [NSTimer scheduledTimerWithTimeInterval:3.5
                                         target:self
                                       selector:@selector(newsession)
                                       userInfo:nil
                                        repeats:NO];
    }
    
    
    else{
        
        NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
        [currentDefaults removeObjectForKey:@"layoutphoto"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        email.hidden=YES;
        emailscreen.hidden=YES;
        social.hidden = YES;
        socialview.hidden=YES;
        social.hidden=YES;
        
        mainfinal.hidden = NO;
        finalscreen2.hidden=YES;
        finalscreen.hidden=YES;
        [self addvideofinal];
    }
    
    
    //    FBSDKShareDialog *shareDialog = [FBSDKShareDialog new];
    //
    //    [shareDialog setMode:FBSDKShareDialogModeAutomatic];
    //    //    [FBSDKShareDialog showFromViewController:self.messageTableViewController withContent:content delegate:self];
    //    [shareDialog setShareContent:content];
    //    [shareDialog show];
    
    [[FBSDKLoginManager new] logOut];
}



-(void) publishFBPost{
    
    NSLog(@"PUBLISH 1");
    
    NSUserDefaults *defaults2 = [NSUserDefaults standardUserDefaults];
    NSData *data6 = [defaults2 objectForKey:@"layoutphoto"];
    UIImage * myImage = [NSKeyedUnarchiver unarchiveObjectWithData:data6];
    NSString * string61 = [self.record valueForKey:@"switchFacebookIndividual"];
    NSString * string62 = [self.record valueForKey:@"switchFacebookLayout"];
    
    NSMutableArray* photos = [[NSMutableArray alloc] init];
    
    //    if ([FBSDKAccessToken currentAccessToken]){
    //        NSDictionary *params = @{
    //                                 @"message": [NSString stringWithFormat:@"%@", hashtag],
    //
    //                                 };
    //        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"/me/feed" parameters:params HTTPMethod:@"POST"]startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
    //            if (error)
    //                NSLog(@"error:%@", error);
    //            else{
    //                //                UIAlertView *av = [[UIAlertView alloc]initWithTitle:@"FBSDKGraphRequest" message:@"Text Shared successfully..!" delegate:self cancelButtonTitle:@"Done" otherButtonTitles:nil, nil];
    //                //                [av show];
    //            }
    //        }];
    //    }
    
    
    
    
    if ([string61 isEqualToString:@"YES"] && [string62 isEqualToString:@"YES"])
    {
        
        
        
        for (int i = 0; i < [photostaken count]; i++)
        {
            
            UIImage *image = [UIImage imageWithData:[_arrSlidshowImg objectAtIndex:i]];
            FBSDKSharePhoto *photo = [[FBSDKSharePhoto alloc] init];
            photo.image = image;
            photo.userGenerated = YES;
            [photos addObject:photo];
            
            
            
        }
        
        FBSDKSharePhoto *photo = [[FBSDKSharePhoto alloc] init];
        photo.image = myImage;
        photo.userGenerated = YES;
        [photos addObject:photo];
        
        
        
        
    }
    else if ([string61 isEqualToString:@"YES"] && [string62 isEqualToString:@"NO"])
    {
        for (int i = 0; i < photostaken.count; i++)
        {
            
            UIImage *image = [UIImage imageWithData:[_arrSlidshowImg objectAtIndex:i]];
            FBSDKSharePhoto *photo = [[FBSDKSharePhoto alloc] init];
            photo.image = image;
            photo.userGenerated = YES;
            [photos addObject:photo];
            
        }
    }
    
    else if ([string61 isEqualToString:@"NO"] && [string62 isEqualToString:@"YES"])
    {
        FBSDKSharePhoto *photo = [[FBSDKSharePhoto alloc] init];
        photo.image = myImage;
        photo.userGenerated = YES;
        [photos addObject:photo];
    }
    FBSDKSharePhotoContent *content = [[FBSDKSharePhotoContent alloc] init];
    content.photos = [photos copy];
    [FBSDKShareAPI shareWithContent:content delegate:nil];
    
    
    //   //THIS IS THE PHOTO ONLY CODE

    
    
    NSString *videofinal = [self.record valueForKey:@"finalimagevideo"];
    if (videofinal == nil)
    {
        NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
        [currentDefaults removeObjectForKey:@"layoutphoto"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        email.hidden=YES;
        emailscreen.hidden=YES;
        social.hidden = YES;
        socialview.hidden=YES;
        social.hidden=YES;
        
        mainfinal.hidden = NO;
        final.hidden = NO;
        finalscreen2.hidden=NO;
        finalscreen.hidden=NO;
        [NSTimer scheduledTimerWithTimeInterval:3.5
                                         target:self
                                       selector:@selector(newsession)
                                       userInfo:nil
                                        repeats:NO];
    }
    
    
    else{
        
        NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
        [currentDefaults removeObjectForKey:@"layoutphoto"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        email.hidden=YES;
        emailscreen.hidden=YES;
        social.hidden = YES;
        socialview.hidden=YES;
        social.hidden=YES;
        
        mainfinal.hidden = NO;
        finalscreen2.hidden=YES;
        finalscreen.hidden=YES;
        [self addvideofinal];
    }
    
    
    //    FBSDKShareDialog *shareDialog = [FBSDKShareDialog new];
    //
    //    [shareDialog setMode:FBSDKShareDialogModeAutomatic];
    //    //    [FBSDKShareDialog showFromViewController:self.messageTableViewController withContent:content delegate:self];
    //    [shareDialog setShareContent:content];
    //    [shareDialog show];
    
    [[FBSDKLoginManager new] logOut];
}




- (void)facebookoption:(CNPPopupStyle)popupStyle
{
    
   
    
    NSLog(@"EMAIL BUTTON PRESSED");
    fontcolor = [UIColor whiteColor];
    backgroundcolor1  = [UIColor darkGrayColor];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 85, 85)];
    imageView.image = [UIImage imageNamed:@"FB.png"];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    
    UILabel *titleLabel = [[UILabel alloc] init];
    NSAttributedString *lineTwo = [[NSAttributedString alloc] initWithString:@"Is this your first time sharing to Facebook from this event?" attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:18], NSForegroundColorAttributeName : [UIColor darkGrayColor], NSParagraphStyleAttributeName : paragraphStyle}];
    titleLabel.numberOfLines = 2;
    titleLabel.attributedText = lineTwo;
    
    UIView *customView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 340, 65)];
    
    CNPPopupButton *button1 = [[CNPPopupButton alloc] initWithFrame:CGRectMake(20, 10, 140, 45)];
    [button1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button1.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button1 setTitle:@"YES" forState:UIControlStateNormal];
    [button1 setTitleColor:[UIColor colorWithRed:58.0/255.0 green:89.0/255.0 blue:153.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    button1.backgroundColor = fontcolor;
    [[button1 layer] setBorderWidth:2.0f];
    [[button1 layer] setBorderColor:[UIColor colorWithRed:58.0/255.0 green:89.0/255.0 blue:153.0/255.0 alpha:1.0].CGColor];
    button1.layer.cornerRadius = 15;
    button1.selectionHandler = ^(CNPPopupButton *button1){
        [self.popupController dismissPopupControllerAnimated:YES];
        [self publishFBPost1];
        
    };
    CNPPopupButton *button = [[CNPPopupButton alloc] initWithFrame:CGRectMake(180, 10, 140, 45)];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button setTitle:@"NO" forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.backgroundColor = [UIColor colorWithRed:58.0/255.0 green:89.0/255.0 blue:153.0/255.0 alpha:1.0];
    button.layer.cornerRadius = 15;
    button.selectionHandler = ^(CNPPopupButton *button){
        [self.popupController dismissPopupControllerAnimated:YES];
        
        [self publishFBPost];
          };
    
    
    
    [customView1 addSubview:button1];
    [customView1 addSubview:button];
    self.popupController = [[CNPPopupController alloc] initWithContents:@[imageView, titleLabel, customView1]];
    self.popupController.theme = [CNPPopupTheme defaultTheme];
    self.popupController.theme.popupStyle = popupStyle;
    self.popupController.delegate = self;
    [self.popupController presentPopupControllerAnimated:NO];
    
    
    
}





//TEXTING//

-(IBAction)sendText:(id)sender
{
    
    [self sendtexting:CNPPopupStyleCentered];
    
    
    
}


-(void)sendtexting:(CNPPopupStyle)popupStyle
{
    
    fontcolor = [UIColor whiteColor];
    backgroundcolor1  = [UIColor darkGrayColor];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];
    imageView.image = [UIImage imageNamed:@"Message.png"];
     imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    UIView *customView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 45)];
    customView.backgroundColor = [UIColor colorWithRed:168.0/255.0 green:217.0/255.0 blue:69.0/255.0 alpha:1.0];
    UIView *spacerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    UILabel *lineTwoLabel = [[UILabel alloc] init];
    UITextField *textFied = [[UITextField alloc] initWithFrame:CGRectMake(1,1, 318, 43)];
    [textFied setBackgroundColor:fontcolor];
    [textFied setValue: [UIColor colorWithRed:168.0/255.0 green:217.0/255.0 blue:69.0/255.0 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    textFied.placeholder = @"Enter your phone number";
    textFied.text = textnumber.text;
    textFied.delegate = self;
    [textFied setLeftViewMode:UITextFieldViewModeAlways];
    [textFied setLeftView:spacerView];
    
    [customView addSubview:lineTwoLabel];
    [customView addSubview:textFied];
    textFied.keyboardType = UIKeyboardTypeNumberPad;
    
    UIView *customView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 340, 65)];
    
    CNPPopupButton *button1 = [[CNPPopupButton alloc] initWithFrame:CGRectMake(20, 10, 140, 45)];
    [button1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button1.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button1 setTitle:@"CANCEL" forState:UIControlStateNormal];
    [button1 setTitleColor:[UIColor colorWithRed:168.0/255.0 green:217.0/255.0 blue:69.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    button1.backgroundColor = fontcolor;
    [[button1 layer] setBorderWidth:1.0f];
    [[button1 layer] setBorderColor:[UIColor colorWithRed:168.0/255.0 green:217.0/255.0 blue:69.0/255.0 alpha:1.0].CGColor];
    button1.layer.cornerRadius = 15;
    button1.selectionHandler = ^(CNPPopupButton *button1){
        [self.popupController dismissPopupControllerAnimated:YES];
        
    };
    CNPPopupButton *button = [[CNPPopupButton alloc] initWithFrame:CGRectMake(180, 10, 140, 45)];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button setTitle:@"SEND TEXT" forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.backgroundColor = [UIColor colorWithRed:168.0/255.0 green:217.0/255.0 blue:69.0/255.0 alpha:1.0];
    button.layer.cornerRadius = 15;
    button.selectionHandler = ^(CNPPopupButton *button){
        [self.popupController dismissPopupControllerAnimated:YES];
        //        MBProgressHUD *nud=[MBProgressHUD showHUDAddedTo:self.view animated:YES];
        //        nud.labelText=@"Uploading..";
        //
        
        
        NSString *valueToSave = textFied.text;
        [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"textnumber"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        NSLog(@"TextField Saved: %@", valueToSave);
        [self uploadphotos];
        
        
        
        
    };
    
    
    
    [customView1 addSubview:button1];
    [customView1 addSubview:button];
    self.popupController = [[CNPPopupController alloc] initWithContents:@[imageView, customView, customView1]];
    self.popupController.theme = [CNPPopupTheme defaultTheme];
    self.popupController.theme.popupStyle = popupStyle;
    self.popupController.delegate = self;
    [self.popupController presentPopupControllerAnimated:NO];
    
}

-(void)uploadphotos
{
    
    
    
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"textnumber"];
    
    
    NSString *twilioSID = [[NSUserDefaults standardUserDefaults]
                           stringForKey:@"twiliosid"];
    
    NSString *twilioAuthKey = [[NSUserDefaults standardUserDefaults]
                               stringForKey:@"twilioauthkey"];
    
    NSString *bodyMessage = [[NSUserDefaults standardUserDefaults]
                             stringForKey:@"twiliomessage"];
    
    NSString *fromNumber = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"twiliofromnumber"];
    
    //   NSString *savedValue1 = [[NSUserDefaults standardUserDefaults]
    //                    stringForKey:@"textphotos"];
    
    
    
    
    
    NSUserDefaults *defaults2 = [NSUserDefaults standardUserDefaults];
    NSString *layphoto = [defaults2 objectForKey:@"textphotos"];
    NSString * string61 = [self.record valueForKey:@"switchTextIndividual"];
    NSString * string62 = [self.record valueForKey:@"switchTextLayout"];
    
    if ([string61 isEqualToString:@"YES"] && [string62 isEqualToString:@"YES"])
    {
        
        
        for (int i = 0; i < [texttaken count]; i++)
        {
            
            NSString *text = [texttaken objectAtIndex:i];
            
            if ([bodyMessage isEqualToString:@""] || bodyMessage == nil)
            {
                NSString *urlString = [NSString stringWithFormat:@"https://%@:%@@api.twilio.com/2010-04-01/Accounts/%@/Messages", twilioSID, twilioAuthKey, twilioSID];
                
                NSURL *url = [NSURL URLWithString:urlString];
                
                
                
                
                NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
                [request setURL:url];
                [request setHTTPMethod:@"POST"];
                
                NSString *bodyString = [NSString stringWithFormat:@"From=%@&To=%@&Body=My Photo&MediaUrl=%@", fromNumber,savedValue,text];
                
                NSData *data3 =[bodyString dataUsingEncoding:NSUTF8StringEncoding];
                
                
                
                
                
                [request setHTTPBody:data3];
                NSError *error;
                
                NSURLResponse *response;
                
                NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
                
                //Handle the received data
                
                if(error){
                    NSLog(@"Error:%@", error);
                }else{
                    NSString *receivedString = [[NSString alloc]initWithData:receivedData encoding:NSUTF8StringEncoding];
                    NSLog(@"Request sent.%@",receivedString);
                }
                
                
            }
            
            else
            {
                NSString *urlString = [NSString stringWithFormat:@"https://%@:%@@api.twilio.com/2010-04-01/Accounts/%@/Messages", twilioSID, twilioAuthKey, twilioSID];
                
                NSURL *url = [NSURL URLWithString:urlString];
                
                
                
                
                NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
                [request setURL:url];
                [request setHTTPMethod:@"POST"];
                
                NSString *bodyString = [NSString stringWithFormat:@"From=%@&To=%@&Body=%@&MediaUrl=%@", fromNumber,savedValue,bodyMessage,text];
                
                NSData *data3 =[bodyString dataUsingEncoding:NSUTF8StringEncoding];
                
                
                
                
                [request setHTTPBody:data3];
                
                NSError *error;
                
                NSURLResponse *response;
                
                NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
                
                //Handle the received data
                
                if(error){
                    NSLog(@"Error:%@", error);
                }else{
                    NSString *receivedString = [[NSString alloc]initWithData:receivedData encoding:NSUTF8StringEncoding];
                    NSLog(@"Request sent.%@",receivedString);
                }
                
                
                
            }
            
        }
        
        if ([bodyMessage isEqualToString:@""] || bodyMessage == nil)
        {
            NSString *urlString = [NSString stringWithFormat:@"https://%@:%@@api.twilio.com/2010-04-01/Accounts/%@/Messages", twilioSID, twilioAuthKey, twilioSID];
            
            NSURL *url = [NSURL URLWithString:urlString];
            
            
            
            
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
            [request setURL:url];
            [request setHTTPMethod:@"POST"];
            
            NSString *bodyString = [NSString stringWithFormat:@"From=%@&To=%@&Body=My Photo&MediaUrl=%@", fromNumber,savedValue,layphoto];
            
            NSData *data3 =[bodyString dataUsingEncoding:NSUTF8StringEncoding];
            
            [request setHTTPBody:data3];
            
            NSError *error;
            
            NSURLResponse *response;
            
            NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            
            //Handle the received data
            
            if(error){
                NSLog(@"Error:%@", error);
            }else{
                NSString *receivedString = [[NSString alloc]initWithData:receivedData encoding:NSUTF8StringEncoding];
                NSLog(@"Request sent.%@",receivedString);
            }
            
            
            
        }
        
        else
        {
            NSString *urlString = [NSString stringWithFormat:@"https://%@:%@@api.twilio.com/2010-04-01/Accounts/%@/Messages", twilioSID, twilioAuthKey, twilioSID];
            
            NSURL *url = [NSURL URLWithString:urlString];
            
            
            
            
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
            [request setURL:url];
            [request setHTTPMethod:@"POST"];
            
            NSString *bodyString = [NSString stringWithFormat:@"From=%@&To=%@&Body=%@&MediaUrl=%@", fromNumber,savedValue,bodyMessage,layphoto];
            
            NSData *data3 =[bodyString dataUsingEncoding:NSUTF8StringEncoding];
            [request setHTTPBody:data3];
            
            NSError *error;
            
            NSURLResponse *response;
            
            NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            
            //Handle the received data
            
            if(error){
                NSLog(@"Error:%@", error);
            }else{
                NSString *receivedString = [[NSString alloc]initWithData:receivedData encoding:NSUTF8StringEncoding];
                NSLog(@"Request sent.%@",receivedString);
            }
            
            
            
            
        }
        
        
        
        
    }
    else if ([string61 isEqualToString:@"YES"] && [string62 isEqualToString:@"NO"])
    {
        for (int i = 0; i < [texttaken count]; i++)
        {
            
            NSString *text = [texttaken objectAtIndex:i];
            
            if ([bodyMessage isEqualToString:@""] || bodyMessage == nil)
            {
                NSString *urlString = [NSString stringWithFormat:@"https://%@:%@@api.twilio.com/2010-04-01/Accounts/%@/Messages", twilioSID, twilioAuthKey, twilioSID];
                
                NSURL *url = [NSURL URLWithString:urlString];
                
                
                
                
                NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
                [request setURL:url];
                [request setHTTPMethod:@"POST"];
                
                NSString *bodyString = [NSString stringWithFormat:@"From=%@&To=%@&Body=My Photo&MediaUrl=%@", fromNumber,savedValue,text];
                
                NSData *data3 =[bodyString dataUsingEncoding:NSUTF8StringEncoding];
                
                
                
                
                [request setHTTPBody:data3];
                
                NSError *error;
                
                NSURLResponse *response;
                
                NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
                
                //Handle the received data
                
                if(error){
                    NSLog(@"Error:%@", error);
                }else{
                    NSString *receivedString = [[NSString alloc]initWithData:receivedData encoding:NSUTF8StringEncoding];
                    NSLog(@"Request sent.%@",receivedString);
                }
                
                
                
            }
            
            else
            {
                NSString *urlString = [NSString stringWithFormat:@"https://%@:%@@api.twilio.com/2010-04-01/Accounts/%@/Messages", twilioSID, twilioAuthKey, twilioSID];
                
                NSURL *url = [NSURL URLWithString:urlString];
                
                
                
                
                NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
                [request setURL:url];
                [request setHTTPMethod:@"POST"];
                
                NSString *bodyString = [NSString stringWithFormat:@"From=%@&To=%@&Body=%@&MediaUrl=%@", fromNumber,savedValue,bodyMessage,text];
                
                NSData *data3 =[bodyString dataUsingEncoding:NSUTF8StringEncoding];
                
                [request setHTTPBody:data3];
                
                NSError *error;
                
                NSURLResponse *response;
                
                NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
                
                //Handle the received data
                
                if(error){
                    NSLog(@"Error:%@", error);
                }else{
                    NSString *receivedString = [[NSString alloc]initWithData:receivedData encoding:NSUTF8StringEncoding];
                    NSLog(@"Request sent.%@",receivedString);
                }
                
                
                
                
            }
            
        }
        
        
    }
    
    else if ([string61 isEqualToString:@"NO"] && [string62 isEqualToString:@"YES"])
    {
        if ([bodyMessage isEqualToString:@""] || bodyMessage == nil)
        {
            NSString *urlString = [NSString stringWithFormat:@"https://%@:%@@api.twilio.com/2010-04-01/Accounts/%@/Messages", twilioSID, twilioAuthKey, twilioSID];
            
            NSURL *url = [NSURL URLWithString:urlString];
            
            
            
            
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
            [request setURL:url];
            [request setHTTPMethod:@"POST"];
            
            NSString *bodyString = [NSString stringWithFormat:@"From=%@&To=%@&Body=My Photo&MediaUrl=%@", fromNumber,savedValue,layphoto];
            
            NSData *data3 =[bodyString dataUsingEncoding:NSUTF8StringEncoding];
            [request setHTTPBody:data3];
            
            NSError *error;
            
            NSURLResponse *response;
            
            NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            
            //Handle the received data
            
            if(error){
                NSLog(@"Error:%@", error);
            }else{
                NSString *receivedString = [[NSString alloc]initWithData:receivedData encoding:NSUTF8StringEncoding];
                NSLog(@"Request sent.%@",receivedString);
            }
            
            
            
        }
        
        else
        {
            NSString *urlString = [NSString stringWithFormat:@"https://%@:%@@api.twilio.com/2010-04-01/Accounts/%@/Messages", twilioSID, twilioAuthKey, twilioSID];
            
            NSURL *url = [NSURL URLWithString:urlString];
            
            
            
            
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
            [request setURL:url];
            [request setHTTPMethod:@"POST"];
            
            NSString *bodyString = [NSString stringWithFormat:@"From=%@&To=%@&Body=%@&MediaUrl=%@", fromNumber,savedValue,bodyMessage,layphoto];
            
            NSData *data3 =[bodyString dataUsingEncoding:NSUTF8StringEncoding];
            
            [request setHTTPBody:data3];
            
            NSError *error;
            
            NSURLResponse *response;
            
            NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            
            //Handle the received data
            
            if(error){
                NSLog(@"Error:%@", error);
            }else{
                NSString *receivedString = [[NSString alloc]initWithData:receivedData encoding:NSUTF8StringEncoding];
                NSLog(@"Request sent.%@",receivedString);
            }
            
            
            
        }
        
        
    }
    
    
    
    
    
    
    
}




//DROPBOX//



- (DBRestClient *)restClient
{
    if (restClient == nil) {
        if ( [[DBSession sharedSession].userIds count] ) {
            restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
            restClient.delegate = self;
        }
    }
    
    return restClient;
}

-(void)getPath
{
    Uppaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    documentsDirectory = [Uppaths objectAtIndex:0];
}

-(void)loginWithViewController:(UIViewController *)selfViewCtrl
{
    [[DBSession sharedSession]linkFromController:selfViewCtrl];
}


-(void)logOut
{
    [[DBSession sharedSession] unlinkAll];
}


//-(void)DropboxuploadWithMultiImage:(NSArray *)ImageArray
//{
//    arrayValue=[ImageArray count];
//    [self getPath];
//    for (int i=0; i<[ImageArray count]; i++)
//    {
//        imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"Image%d.png",i]];
//        UIImage *image=[ImageArray objectAtIndex:i];
//        data = UIImageJPEGRepresentation(image,0.5);
//        [data writeToFile:imagePath atomically:YES];
//        NSString *destDir = @"/Uploaded Images/";
//        restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
//        restClient.delegate = self;
//        [restClient uploadFile:[NSString stringWithFormat:@"Image%d.png",i] toPath:destDir withParentRev:nil fromPath:imagePath];
//    }
//}
//
//-(void)DropboxuploadWithMultiVideo:(NSArray *)multiVideoURLArray
//{
//    arrayValue=[multiVideoURLArray count];
//    [self getPath];
//    for (int i=0; i<[multiVideoURLArray count]; i++)
//    {
//        imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"Video%d.mp4",i]];
//        NSString *singleURL=[multiVideoURLArray objectAtIndex:i];
//        data =  [NSData dataWithContentsOfFile:singleURL];
//        [data writeToFile:imagePath atomically:YES];
//        NSString *destDir = @"/Uploaded Videos/";
//        restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
//        restClient.delegate = self;
//        [restClient uploadFile:[NSString stringWithFormat:@"Video%d.mp4",i] toPath:destDir withParentRev:nil fromPath:imagePath];
//    }
//}

#pragma mark - Dropbox uploading Response

- (void)restClient:(DBRestClient*)client uploadedFile:(NSString*)destPath
              from:(NSString*)srcPath metadata:(DBMetadata*)metadata
{
    
    NSLog(@"File uploaded successfully to path: %@", metadata.path);
    
    [[self restClient] loadSharableLinkForFile:metadata.path shortUrl:NO] ;
    
    
    
    value++;
    if ( value==arrayValue)
    {
        
        NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
        [center postNotificationName:@"UploadFinish" object: self];
        value=0;
    }
    
}

- (void)restClient:(DBRestClient*)client uploadFileFailedWithError:(NSError*)error
{
    NSLog(@"File upload failed with error - %@", error);
    value++;
    if ( value==arrayValue)
    {
        
        NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
        [center postNotificationName:@"UploadFinish" object: self];
        value=0;
    }
    
}

#pragma mark -Get Dropbox Uploaded File's Link

- (void)restClient:(DBRestClient*)restClient loadedSharableLink:(NSString*)link
           forFile:(NSString*)path
{
    
    NSLog(@"Old link %@",link);
    NSString *newString = [link substringToIndex:[link length]-4];
    NSString *addString = @"dl=1";
    NSString *complete = [NSString stringWithFormat:@"%@%@", newString,
                          addString];
    
    
    
    NSLog(@"New link %@",newString);
    NSLog(@"Complete Path %@ ",complete);
    //    [[NSUserDefaults standardUserDefaults] setObject:complete forKey:@"textphotos"];
    //    [[NSUserDefaults standardUserDefaults] synchronize];
    //
    
    
    
    [texttaken addObject:complete];
    
    
    
    
    
    
}





- (void)restClient:(DBRestClient*)restClient loadSharableLinkFailedWithError:(NSError*)error
{
    NSLog(@"Error %@",error);
}






//THESE ARE THE VIDEOS THAT ARE DISPLAYED IF THE USER SELECTED A VIDEO MESSAGE OPTION FOR THE WELCOME SCREEN AND THE FINAL SCREEN//

-(void)addvideowelcome
{
    
    NSString *videowelcome = [self.record valueForKey:@"welcomeimagevideo"];
    NSLog(@"WELCOME VIDEO IS %@", videowelcome);
    NSURL *videoURL = [NSURL URLWithString:[videowelcome stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    _player = [AVPlayer playerWithURL:videoURL];
    
    // create a player view controller
    _controller = [[AVPlayerViewController alloc]init];
    [_controller setShowsPlaybackControls:NO];
    _controller.videoGravity = AVLayerVideoGravityResizeAspectFill;
    _controller.player = _player;
    [_player play];
    _player.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:[_player currentItem]];
    // [self addChildViewController:controller];
    [welcomescreen1 addSubview:_controller.view];
    _controller.view.frame = welcomescreen1.frame;
}


-(void)addvideofinal
{
    
    NSString *videofinal = [self.record valueForKey:@"finalimagevideo"];
    NSURL *videoURL = [NSURL URLWithString:[videofinal stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    _finalplayer = [AVPlayer playerWithURL:videoURL];
    
    // create a player view controller
    _controller = [[AVPlayerViewController alloc]init];
    [_controller setShowsPlaybackControls:NO];
    _controller.videoGravity = AVLayerVideoGravityResizeAspectFill;
    _controller.player = _finalplayer;
    [_finalplayer play];
    _finalplayer.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd1:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:[_finalplayer currentItem]];
    // [self addChildViewController:controller];
    [finalscreen1 addSubview:_controller.view];
    _controller.view.frame = finalscreen1.frame;
}




- (void)playerItemDidReachEnd:(NSNotification *)notification {
    AVPlayerItem *p = [notification object];
    [p seekToTime:kCMTimeZero];
}

- (void)playerItemDidReachEnd1:(NSNotification *)notification {
    [_finalplayer pause];
    [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(newsession) userInfo:nil repeats:NO];
    
}




//THIS BUTTON RETURNS TO THE ADMIN
- (IBAction)GoBack {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    TSPViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"Home"];
    [vc setManagedObjectContext:self.managedObjectContext];
    
    [vc setModalPresentationStyle:UIModalPresentationFullScreen];
    [self presentViewController:vc animated:NO completion:nil];
}













//THIS IS THE CODE TO PROCESS THE IMAGE AND THE EFFECTS//
#pragma mark UI


- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection {
    
//    NSLog(@"CAPTURE OUT PUT");
    
    if ([watermark isEqualToString:@"YESWATERMARK"])
    {
        @autoreleasepool {
            
            // Get a CMSampleBuffer's Core Video image buffer for the media data
            CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
            
            // turn buffer into an image we can manipulate
            CIImage *result = [CIImage imageWithCVPixelBuffer:imageBuffer];
            
            // store final usable image
            CGImageRef finishedImage;
            
            //        if( useFilters ) {
            // hue
            
            if([selectedFilterName  isEqual: @"CIPhotoEffectNone"])
            {
                
            }
            
            else
                
            {
                CIFilter *hueAdjust = [CIFilter filterWithName:selectedFilterName];
                
                [hueAdjust setDefaults];
                [hueAdjust setValue:result forKey:@"inputImage"];
                //            [hueAdjust setValue:[NSNumber numberWithFloat:8.094] forKey: @"inputAngle"];
                result = hueAdjust.outputImage;
                
            }
            
            
            finishedImage = [ciContext createCGImage:result fromRect:[result extent]];
            
            
            if (testshot.on == NO)
            {
                if (captureImage == YES)
                {
                    
                    captureImage = NO;
                    
                    UIImage *originalcapturedImage = [UIImage imageWithCGImage:finishedImage];
                    UIImage *capturedImage =  [self combineImage:originalcapturedImage];
                    
                    
                    
                    UIImage* flippedImage = [UIImage imageWithCGImage:capturedImage.CGImage
                                                                scale:capturedImage.scale
                                                          orientation:UIImageOrientationUpMirrored];
                    
                    
                    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                                            stringForKey:@"AlbumName"];
                    
                    
                    
                    
                    
                    [CustomAlbum addNewAssetWithImage:flippedImage toAlbum:[CustomAlbum getMyAlbumWithName:savedValue] onSuccess:^(NSString *ImageId) {
                        recentImg = ImageId;
                        
                        
                    } onError:^(NSError *error) {
                        NSLog(@"probelm in saving image");
                    }];
                    
                    
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.arrSlidshowImg addObject:UIImageJPEGRepresentation(flippedImage, 1.0)];
                            [photostaken addObject:UIImageJPEGRepresentation(flippedImage, 0.5)];
                            
                            
                            
                            
                            
                            
                            
                            
                            NSString * sharing = [self.record valueForKey:@"switchSharing"];
                            
                            if ([sharing isEqual: @"YES"])
                            {
                                NSString * share4 = [self.record valueForKey:@"switchSharingIndividual"];
                                
                                if ([share4  isEqual: @"YES"])
                                {
                                    [self getPath];
                                    NSString *prefixString = @"Image";
                                    
                                    NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
                                    NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
                                    imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
                                    
                                    UIImage *image= capturedImage;
                                    UIImage* flippedImage2 = [UIImage imageWithCGImage:image.CGImage
                                                                                scale:image.scale
                                                                          orientation:UIImageOrientationUpMirrored];
                                    data = UIImageJPEGRepresentation(flippedImage2, 1.0);
                                    [data writeToFile:imagePath atomically:YES];
                                    
                                    NSLog(@"filepath=%@",imagePath);
                                    refURL = [NSURL fileURLWithPath:imagePath];
                                    if ([[_appDelegate.mcManager.session connectedPeers] count]) {
                                        [self sendResource];
                                    }

                                    NSLog (@"Sharing Layout IS ON");
                                    
                                }
                                
                                else
                                {
                                    
                                    
                                    NSLog (@"Sharing Layout IS OFF");
                                }
                                
                            }
                            else
                            {
                                if (![[DBSession sharedSession] isLinked])
                                {
                                    
                                    
                                    
                                }
                                
                                else
                                {
                                    
                                    [self getPath];
                                    NSString *prefixString = @"Image";
                                    
                                    NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
                                    NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
                                    imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
                                    UIImage *image= capturedImage;
                                    UIImage* flippedImage2 = [UIImage imageWithCGImage:image.CGImage
                                                                                 scale:image.scale
                                                                           orientation:UIImageOrientationUpMirrored];
                                    data = UIImageJPEGRepresentation(flippedImage2, 0.7);
                                    [data writeToFile:imagePath atomically:YES];
                                    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                                                            stringForKey:@"AlbumName"];
                                    NSArray* words = [savedValue componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                                    NSString* nospacestring = [words componentsJoinedByString:@""];
                                    
                                    NSString *destDir = [NSString stringWithFormat: @"/%@/", nospacestring];
                                    restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
                                    restClient.delegate = self;
                                    [restClient uploadFile:[NSString stringWithFormat:@"%@.jpg",uniqueFileName] toPath:destDir withParentRev:nil fromPath:imagePath];
                                    
                                    
                                    
                                    
                                }
                                

                            }
                            
                            
                            
                            
                            
                            
                            //[[self restClient] loadSharableLinkForFile:destDir shortUrl:NO] ;
                            
                            if (photocount == 0) {
                                NSLog (@"count==mPhotoCount");
                                captureImage1.hidden = NO;
                                captured.hidden=NO;
                                [self repaintImageWithImage:capturedImage];
                                NSLog(@"Photo effect none");
                                selectedFilterName = @"CIPhotoEffectNone";
                                NSString *duration = [self.record valueForKey:@"photopreview"];
                                previewcount = [duration intValue];
                                [NSTimer scheduledTimerWithTimeInterval:previewcount target:self selector:@selector(final) userInfo:nil repeats:NO];
                            }
                            
                            else {
                                NSLog(@"Start Timer");
                                captured.hidden=NO;
                                captureImage1.hidden = NO;
                                [self repaintImageWithImage:capturedImage];
                                NSString *duration = [self.record valueForKey:@"photopreview"];
                                previewcount = [duration intValue];
                                [NSTimer scheduledTimerWithTimeInterval:previewcount target:self selector:@selector(hidecapture1) userInfo:nil repeats:NO];
                            }
                            
                        });
                    });
                }
                
                
            }
            else if (testshot.on == YES)
            {
                if (captureImage == YES)
                {
                    
                    captureImage = NO;
                    
                    UIImage *capturedImage = [UIImage imageWithCGImage:finishedImage];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        captureImage1.hidden = NO;
                        captured.hidden=NO;
                        [self repaintImageWithImage:capturedImage];
                        NSLog(@"Photo effect none");
                        selectedFilterName = @"CIPhotoEffectNone";
                      
                        [NSTimer scheduledTimerWithTimeInterval:2.5 target:self selector:@selector(testend) userInfo:nil repeats:NO];
                        
                    });
                    
                }
                
                
            }
            
            
            
            
            
            
            
            
            
            [videoLayer performSelectorOnMainThread:@selector(setContents:) withObject:(__bridge id)finishedImage waitUntilDone:YES];
            
            CGImageRelease(finishedImage);
        }
        
    }
    
    
    
    if ([watermark isEqualToString:@"NOWATERMARK"])
    {
        @autoreleasepool {
            
            // Get a CMSampleBuffer's Core Video image buffer for the media data
            CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
            
            // turn buffer into an image we can manipulate
            CIImage *result = [CIImage imageWithCVPixelBuffer:imageBuffer];
            
            // store final usable image
            CGImageRef finishedImage;
            
            //        if( useFilters ) {
            // hue
            
            if([selectedFilterName  isEqual: @"CIPhotoEffectNone"])
            {
                
            }
            
            else
                
            {
                CIFilter *hueAdjust = [CIFilter filterWithName:selectedFilterName];
                
                [hueAdjust setDefaults];
                [hueAdjust setValue:result forKey:@"inputImage"];
                //            [hueAdjust setValue:[NSNumber numberWithFloat:8.094] forKey: @"inputAngle"];
                result = hueAdjust.outputImage;
                
            }
            
            
            finishedImage = [ciContext createCGImage:result fromRect:[result extent]];
            
            
            if (testshot.on == NO)
            {
                if (captureImage == YES)
                {
                    
                    captureImage = NO;
                    
                    UIImage *capturedImage = [UIImage imageWithCGImage:finishedImage];
                    
                    
                    
                    UIImage* flippedImage = [UIImage imageWithCGImage:capturedImage.CGImage
                                                                scale:capturedImage.scale
                                                          orientation:UIImageOrientationUpMirrored];
                    
                    
                    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                                            stringForKey:@"AlbumName"];
                    
                    
                    
                    
                    
                    [CustomAlbum addNewAssetWithImage:flippedImage toAlbum:[CustomAlbum getMyAlbumWithName:savedValue] onSuccess:^(NSString *ImageId) {
                        recentImg = ImageId;
                        
                        
                    } onError:^(NSError *error) {
                        NSLog(@"probelm in saving image");
                    }];
                    
                    
                    
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                        
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.arrSlidshowImg addObject:UIImageJPEGRepresentation(flippedImage, 1.0)];
                            [photostaken addObject:UIImageJPEGRepresentation(flippedImage, 0.5)];
                            
                            
                            NSString * sharing = [self.record valueForKey:@"switchSharing"];
                            
                            if ([sharing isEqual: @"YES"])
                            {
                                [self getPath];
                                NSString *prefixString = @"Image";
                                
                                NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
                                NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
                                imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
                                UIImage *image= capturedImage;
                                UIImage* flippedImage2 = [UIImage imageWithCGImage:image.CGImage
                                                                             scale:image.scale
                                                                       orientation:UIImageOrientationUpMirrored];
                                data = UIImageJPEGRepresentation(flippedImage2, 1.0);
                                [data writeToFile:imagePath atomically:YES];
                                
                                NSLog(@"filepath=%@",imagePath);
                                refURL = [NSURL fileURLWithPath:imagePath];
                                if ([[_appDelegate.mcManager.session connectedPeers] count]) {
                                    [self sendResource];
                                }
                                
                            }
                            else
                            {
                                if (![[DBSession sharedSession] isLinked])
                                {
                                    
                                }
                                else
                                {
                                    
                                    [self getPath];
                                    NSString *prefixString = @"Image";
                                    
                                    NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
                                    NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
                                    
                                    
                                    
                                    imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
                                    UIImage *image= capturedImage;
                                    UIImage* flippedImage2 = [UIImage imageWithCGImage:image.CGImage
                                                                                 scale:image.scale
                                                                           orientation:UIImageOrientationUpMirrored];
                                    data = UIImageJPEGRepresentation(flippedImage2,0.7);
                                    [data writeToFile:imagePath atomically:YES];
                                    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                                                            stringForKey:@"AlbumName"];
                                    NSArray* words = [savedValue componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                                    NSString* nospacestring = [words componentsJoinedByString:@""];
                                    
                                    NSString *destDir = [NSString stringWithFormat: @"/%@/", nospacestring];
                                    restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
                                    restClient.delegate = self;
                                    [restClient uploadFile:[NSString stringWithFormat:@"%@.jpg",uniqueFileName] toPath:destDir withParentRev:nil fromPath:imagePath];
                                    
                                }

                            }
                            
                            //[[self restClient] loadSharableLinkForFile:destDir shortUrl:NO] ;
                            
                            if (photocount == 0) {
                                NSLog (@"count==mPhotoCount");
                                captureImage1.hidden = NO;
                                captured.hidden=NO;
                                [self repaintImageWithImage:capturedImage];
                                NSLog(@"Photo effect none");
                                selectedFilterName = @"CIPhotoEffectNone";
                                NSString *duration = [self.record valueForKey:@"photopreview"];
                                previewcount = [duration intValue];
                                [NSTimer scheduledTimerWithTimeInterval:previewcount target:self selector:@selector(final) userInfo:nil repeats:NO];
                            }
                            
                            else {
                                NSLog(@"Start Timer");
                                captured.hidden=NO;
                                captureImage1.hidden = NO;
                                [self repaintImageWithImage:capturedImage];
                                NSString *duration = [self.record valueForKey:@"photopreview"];
                                previewcount = [duration intValue];
                                [NSTimer scheduledTimerWithTimeInterval:previewcount target:self selector:@selector(hidecapture1) userInfo:nil repeats:NO];
                            }
                            
                        });
                    });
                }
                
                
            }
            else if (testshot.on == YES)
            {
                if (captureImage == YES)
                {
                    
                    captureImage = NO;
                    
                    UIImage *capturedImage = [UIImage imageWithCGImage:finishedImage];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        captureImage1.hidden = NO;
                        captured.hidden=NO;
                        [self repaintImageWithImage:capturedImage];
                        NSLog(@"Photo effect none");
                        selectedFilterName = @"CIPhotoEffectNone";
                     
                        [NSTimer scheduledTimerWithTimeInterval:2.5 target:self selector:@selector(testend) userInfo:nil repeats:NO];
                        
                    });
                    
                }
                
                
            }
            
            
            
            
            
            
            
            
            
            [videoLayer performSelectorOnMainThread:@selector(setContents:) withObject:(__bridge id)finishedImage waitUntilDone:YES];
            
            CGImageRelease(finishedImage);
        }
        
    }
    
    
    
    
    
}

-(void)testend
{
    captureImage1.hidden = YES;
    captured.hidden=YES;
    mainadmin.hidden=NO;
}
- (void)repaintImageWithImage:(UIImage *)anImage {
    NSLog (@"repaintImageWithImage");
    
    if (UIDeviceOrientationIsPortrait([[UIDevice currentDevice] orientation]))
    {
        UIGraphicsBeginImageContext(CGSizeMake(768, 1024));
        [anImage drawInRect:CGRectMake(0, 0, 768, 1024)];
        UIImage *smallImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        CGRect cropRect = CGRectMake(0, 0, 768, 1024);
        CGImageRef imageRef = CGImageCreateWithImageInRect([smallImage CGImage], cropRect);
        [captureImage1 setImage:[UIImage imageWithCGImage:imageRef]];
        CGImageRelease(imageRef);
    }
    
    else
    {
        UIGraphicsBeginImageContext(CGSizeMake(1024, 768));
        [anImage drawInRect:CGRectMake(0, 0, 1024, 768)];
        UIImage *smallImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        CGRect cropRect = CGRectMake(0, 0, 1024, 768);
        CGImageRef imageRef = CGImageCreateWithImageInRect([smallImage CGImage], cropRect);
        [captureImage1 setImage:[UIImage imageWithCGImage:imageRef]];
        CGImageRelease(imageRef);
    }
    
}

- (UIImage *)captureView:(UIView *)view
{
    NSLog (@"captureView");
    NSArray *arraySubView = view.subviews;
    if (arraySubView.count == 0)
        return nil;
    
    CGRect screenRect = view.frame;
    UIGraphicsBeginImageContextWithOptions(screenRect.size, view.opaque, 0.0);

    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextFillRect(ctx, screenRect);
    [view.layer renderInContext:ctx];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize
{
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

- (UIImage *) imageFromSampleBuffer:(CMSampleBufferRef) sampleBuffer
{
    NSLog(@"imageFromSampleBuffer: called");
    CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    CVPixelBufferLockBaseAddress(imageBuffer, 0);
    void *baseAddress = CVPixelBufferGetBaseAddress(imageBuffer);
    
    size_t bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer);
    size_t width = CVPixelBufferGetWidth(imageBuffer);
    size_t height = CVPixelBufferGetHeight(imageBuffer);
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    CGContextRef context = CGBitmapContextCreate(baseAddress, width, height, 8,
                                                 bytesPerRow, colorSpace, kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst);
    CGImageRef quartzImage = CGBitmapContextCreateImage(context);
    
    CVPixelBufferUnlockBaseAddress(imageBuffer,0);
    
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    
    UIImage *image = [UIImage imageWithCGImage:quartzImage];
    
    CGImageRelease(quartzImage);
    
    return (image);
}



- (void)hidecapture1 {
    NSLog(@"SHOW REMAINING");
    
    if (photocount > 1)
    {
        lblRemainPhoto1.text = ([NSString stringWithFormat:@"%ld", photocount]);
        NSLog(@"%ld PHOTOS LEFT", (long)photocount);
        lblRemainPhoto2.text = @"PHOTOS LEFT";
    }
    
    if (photocount == 1)
    {
        lblRemainPhoto1.text = @"1";
        lblRemainPhoto2.text = @"PHOTO LEFT";
        
    }
    
    if (photocount == 0)
    {
        NSLog(@"DONE");
        
    }
    captureImage1.hidden=YES;
    remaining.hidden=NO;
    mainremaining.hidden=NO;
    [NSTimer scheduledTimerWithTimeInterval:2.5 target:self selector:@selector(hidecapture) userInfo:nil repeats:false];
}


- (void)hidecapture {
    NSLog(@"CAPTURE PHOTO HIDDEN");
    captureImage1.hidden=YES;
    remaining.hidden=YES;
    mainremaining.hidden=YES;
    countdownTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(startCountdown) userInfo:nil repeats:true];
}

- (void)removephoto {
    NSLog(@"PHOTO REMOVED");
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"image"];
    imgView.hidden=YES;
}



//THESE ARE THE SETTINGS FOR THE MANUAL CAMERA OPTIONS AND PRINTER OPTIONS//




- (IBAction)toggleHUD:(id)sender
{
    
    
    UISegmentedControl *control = sender;
    self.printsettings.hidden = (control.selectedSegmentIndex == 0) ? NO : YES;
    self.manualHUD.hidden = (control.selectedSegmentIndex == 1) ? NO : YES;
    
}

- (IBAction)changePrint:(id)sender
{
    UISegmentedControl *control = sender;
    
    self.printserver.hidden = (control.selectedSegmentIndex == 0) ? NO : YES;
    self.airprint.hidden = (control.selectedSegmentIndex == 1) ? NO : YES;
}


- (IBAction)changeManualHUD:(id)sender
{
    UISegmentedControl *control = sender;
    
    self.manualHUDExposureView.hidden = (control.selectedSegmentIndex == 0) ? NO : YES;
    self.manualHUDWhiteBalanceView.hidden = (control.selectedSegmentIndex == 1) ? NO : YES;
}
- (IBAction)changeFocusMode:(id)sender
{
    UISegmentedControl *control = sender;
    AVCaptureFocusMode mode = (AVCaptureFocusMode)[self.focusModes[control.selectedSegmentIndex] intValue];
    NSError *error = nil;
    
    if ([self.videoDevice lockForConfiguration:&error])
    {
        if ([self.videoDevice isFocusModeSupported:mode])
        {
            NSLog(@"Check 1");
            self.videoDevice.focusMode = mode;
        }
        else
        {
            NSLog(@"Focus mode %@ is not supported. Focus mode is %@.", [self stringFromFocusMode:mode], [self stringFromFocusMode:self.videoDevice.focusMode]);
            self.focusModeControl.selectedSegmentIndex = [self.focusModes indexOfObject:@(self.videoDevice.focusMode)];
        }
    }
    else
    {
        NSLog(@"Error: %@", error);
    }
}
- (IBAction)changeExposureMode:(id)sender
{
    UISegmentedControl *control = sender;
    NSError *error = nil;
    AVCaptureExposureMode mode = (AVCaptureExposureMode)[self.exposureModes[control.selectedSegmentIndex] intValue];
    
    if ([self.videoDevice lockForConfiguration:&error])
    {
        if ([self.videoDevice isExposureModeSupported:mode])
        {
            self.videoDevice.exposureMode = mode;
        }
        else
        {
            NSLog(@"Exposure mode %@ is not supported. Exposure mode is %@.", [self stringFromExposureMode:mode], [self stringFromExposureMode:self.videoDevice.exposureMode]);
        }
    }
    else
    {
        NSLog(@"%@", error);
    }
}

- (IBAction)changeWhiteBalanceMode:(id)sender
{
    UISegmentedControl *control = sender;
    AVCaptureWhiteBalanceMode mode = (AVCaptureWhiteBalanceMode)[self.whiteBalanceModes[control.selectedSegmentIndex] intValue];
    NSError *error = nil;
    
    if ([self.videoDevice lockForConfiguration:&error])
    {
        if ([self.videoDevice isWhiteBalanceModeSupported:mode])
        {
            self.videoDevice.whiteBalanceMode = mode;
        }
        else
        {
            NSLog(@"White balance mode %@ is not supported. White balance mode is %@.", [self stringFromWhiteBalanceMode:mode], [self stringFromWhiteBalanceMode:self.videoDevice.whiteBalanceMode]);
        }
    }
    else
    {
        NSLog(@"%@", error);
    }
}

- (IBAction)changeLensPosition:(id)sender
{
    UISlider *control = sender;
    AVCaptureFocusMode mode = (AVCaptureFocusMode)[self.focusModes[0] intValue];
    NSError *error = nil;
    
    if ([self.videoDevice lockForConfiguration:&error])
    {
        if ([self.videoDevice isFocusModeSupported:mode])
        {
            [self.videoDevice setFocusModeLockedWithLensPosition:control.value completionHandler:nil];
        }
        else
        {
            NSLog(@"Focus mode not support");
        }
    }
    else
    {
        NSLog(@"%@", error);
    }
}




- (IBAction)changeExposureDuration:(id)sender
{
    UISlider *control = sender;
    NSError *error = nil;
    
    double p = pow( control.value, EXPOSURE_DURATION_POWER );
    double minDurationSeconds = MAX(CMTimeGetSeconds(self.videoDevice.activeFormat.minExposureDuration), EXPOSURE_MINIMUM_DURATION);
    double maxDurationSeconds = CMTimeGetSeconds(self.videoDevice.activeFormat.maxExposureDuration);
    double newDurationSeconds = p * ( maxDurationSeconds - minDurationSeconds ) + minDurationSeconds;
    
    if (self.videoDevice.exposureMode == AVCaptureExposureModeCustom)
    {
        if ( newDurationSeconds < 1 )
        {
            int digits = MAX( 0, 2 + floor( log10( newDurationSeconds ) ) );
            self.exposureDurationValueLabel.text = [NSString stringWithFormat:@"1/%.*f", digits, 1/newDurationSeconds];
        }
        else
        {
            self.exposureDurationValueLabel.text = [NSString stringWithFormat:@"%.2f", newDurationSeconds];
        }
    }
    
    if ([self.videoDevice lockForConfiguration:&error])
    {
        NSLog(@"NEW DURATION SECOND: %f", newDurationSeconds);
        NSUserDefaults* userDefault = [NSUserDefaults standardUserDefaults];
        [userDefault setDouble:newDurationSeconds forKey:@"DURATION_SECOND"];
        [userDefault synchronize];
        
        [self.videoDevice setExposureModeCustomWithDuration:CMTimeMakeWithSeconds(newDurationSeconds, 1000*1000*1000)  ISO:AVCaptureISOCurrent completionHandler:nil];
        
        
    }
    else
    {
        NSLog(@"%@", error);
    }
}

- (IBAction)changeISO:(id)sender
{
    UISlider *control = sender;
    NSError *error = nil;
    
    if ([self.videoDevice lockForConfiguration:&error])
    {
        [self.videoDevice setExposureModeCustomWithDuration:AVCaptureExposureDurationCurrent ISO:control.value completionHandler:nil];
    }
    else
    {
        NSLog(@"%@", error);
    }
}



- (IBAction)changeTemperature:(id)sender
{
    
    
    AVCaptureWhiteBalanceTemperatureAndTintValues temperatureAndTint = {
        
        .temperature = self.temperatureSlider.value,
        .tint = self.tintSlider.value,
        
        
    };
    
    NSUserDefaults* userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setFloat:self.temperatureSlider.value forKey:@"TEMPERATURE"];
    [userDefault setFloat:self.tintSlider.value forKey:@"TINT"];
    [userDefault synchronize];
    
    [self setWhiteBalanceGains:[self.videoDevice deviceWhiteBalanceGainsForTemperatureAndTintValues:temperatureAndTint]];
    
    
}

- (IBAction)changeTint:(id)sender
{
    NSUserDefaults* userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setFloat:self.temperatureSlider.value forKey:@"TEMPERATURE"];
    [userDefault setFloat:self.tintSlider.value forKey:@"TINT"];
    [userDefault synchronize];
    
    AVCaptureWhiteBalanceTemperatureAndTintValues temperatureAndTint = {
        
        .temperature = self.temperatureSlider.value,
        .tint = self.tintSlider.value,
        
        
    };
    
    [self setWhiteBalanceGains:[self.videoDevice deviceWhiteBalanceGainsForTemperatureAndTintValues:temperatureAndTint]];
}



- (IBAction)sliderTouchBegan:(id)sender
{
    
    NSString *fontbordercolor = [self.record valueForKey:@"layoutfontbordercolor"];
    
    
    UISlider *slider = (UISlider*)sender;
    [self setSlider:slider highlightColor:[UIColor colorWithString:fontbordercolor]];
}

- (IBAction)sliderTouchEnded:(id)sender
{
    NSString *fontbordercolor = [self.record valueForKey:@"layoutfontbordercolor"];
    UISlider *slider = (UISlider*)sender;
    [self setSlider:slider highlightColor:[UIColor colorWithString:fontbordercolor]];
}

- (void)runStillImageCaptureAnimation
{
    NSLog(@"runStillImageCaptureAnimation");
    dispatch_async(dispatch_get_main_queue(), ^{
        [[[self previewView] layer] setOpacity:0.0];
        [UIView animateWithDuration:.25 animations:^{
            [[[self previewView] layer] setOpacity:1.0];
        }];
    });
}

- (void)configureManualHUD
{
    NSLog(@"configureManualHUD");
    self.focusModes = @[@(AVCaptureFocusModeContinuousAutoFocus), @(AVCaptureFocusModeLocked)];
    
    self.focusModeControl.selectedSegmentIndex = [self.focusModes indexOfObject:@(self.videoDevice.focusMode)];
    for (NSNumber *mode in self.focusModes) {
        [self.focusModeControl setEnabled:([self.videoDevice isFocusModeSupported:[mode intValue]]) forSegmentAtIndex:[self.focusModes indexOfObject:mode]];
        [self.lensPositionSlider setEnabled:([self.videoDevice isFocusModeSupported:[mode intValue]])];
    }
    
    
    self.lensPositionSlider.hidden = NO;
    self.lensPositionValueLabel.hidden=NO;
    self.lensPositionSlider.minimumValue = 0.0;
    self.lensPositionSlider.maximumValue = 1.0;
    //        self.lensPositionSlider.enabled = (self.videoDevice.focusMode == AVCaptureFocusModeLocked);
    
    self.exposureModes = @[@(AVCaptureExposureModeContinuousAutoExposure), @(AVCaptureExposureModeCustom)];
    
    self.exposureModeControl.selectedSegmentIndex = [self.exposureModes indexOfObject:@(self.videoDevice.exposureMode)];
    for (NSNumber *mode in self.exposureModes) {
        [self.exposureModeControl setEnabled:([self.videoDevice isExposureModeSupported:[mode intValue]]) forSegmentAtIndex:[self.exposureModes indexOfObject:mode]];
    }
    
    self.exposureDurationSlider.minimumValue = 0;
    self.exposureDurationSlider.maximumValue = 1;
    self.exposureDurationSlider.enabled = (self.videoDevice.exposureMode == AVCaptureExposureModeCustom);
    
    self.ISOSlider.minimumValue = self.videoDevice.activeFormat.minISO;
    self.ISOSlider.maximumValue = self.videoDevice.activeFormat.maxISO;
    self.ISOSlider.enabled = (self.videoDevice.exposureMode == AVCaptureExposureModeCustom);
    
    self.exposureTargetBiasSlider.minimumValue = self.videoDevice.minExposureTargetBias;
    self.exposureTargetBiasSlider.maximumValue = self.videoDevice.maxExposureTargetBias;
    self.exposureTargetBiasSlider.enabled = YES;
    
    self.exposureTargetOffsetSlider.minimumValue = self.videoDevice.minExposureTargetBias;
    self.exposureTargetOffsetSlider.maximumValue = self.videoDevice.maxExposureTargetBias;
    self.exposureTargetOffsetSlider.enabled = NO;
    self.whiteBalanceModes = @[@(AVCaptureWhiteBalanceModeContinuousAutoWhiteBalance), @(AVCaptureWhiteBalanceModeLocked)];
    
    self.whiteBalanceModeControl.selectedSegmentIndex = [self.whiteBalanceModes indexOfObject:@(self.videoDevice.whiteBalanceMode)];
    for (NSNumber *mode in self.whiteBalanceModes) {
        [self.whiteBalanceModeControl setEnabled:([self.videoDevice isWhiteBalanceModeSupported:[mode intValue]]) forSegmentAtIndex:[self.whiteBalanceModes indexOfObject:mode]];
    }
    
    self.temperatureSlider.minimumValue = 1000;
    self.temperatureSlider.maximumValue = 8000;
    self.temperatureSlider.enabled = (self.videoDevice.whiteBalanceMode == AVCaptureWhiteBalanceModeLocked);
    
    self.tintSlider.minimumValue = -150;
    self.tintSlider.maximumValue = 150;
    self.tintSlider.enabled = (self.videoDevice.whiteBalanceMode == AVCaptureWhiteBalanceModeLocked);
}

- (void)positionManualHUD
{
    NSLog(@"positionManualHUD");
    self.manualHUDExposureView.frame = CGRectMake(self.manualHUDFocusView.frame.origin.x, self.manualHUDFocusView.frame.origin.y, self.manualHUDExposureView.frame.size.width, self.manualHUDExposureView.frame.size.height);
    self.manualHUDWhiteBalanceView.frame = CGRectMake(self.manualHUDFocusView.frame.origin.x, self.manualHUDFocusView.frame.origin.y, self.manualHUDWhiteBalanceView.frame.size.width, self.manualHUDWhiteBalanceView.frame.size.height);
}

- (void)setSlider:(UISlider*)slider highlightColor:(UIColor*)color
{
    NSLog(@"setSlider");
    slider.tintColor = color;
    
    if (slider == self.lensPositionSlider)
    {
        self.lensPositionNameLabel.textColor = self.lensPositionValueLabel.textColor = slider.tintColor;
    }
    else if (slider == self.exposureDurationSlider)
    {
        self.exposureDurationNameLabel.textColor = self.exposureDurationValueLabel.textColor = slider.tintColor;
    }
    else if (slider == self.ISOSlider)
    {
        self.ISONameLabel.textColor = self.ISOValueLabel.textColor = slider.tintColor;
    }
    else if (slider == self.exposureTargetBiasSlider)
    {
        self.exposureTargetBiasNameLabel.textColor = self.exposureTargetBiasValueLabel.textColor = slider.tintColor;
    }
    else if (slider == self.temperatureSlider)
    {
        self.temperatureNameLabel.textColor = self.temperatureValueLabel.textColor = slider.tintColor;
    }
    else if (slider == self.tintSlider)
    {
        self.tintNameLabel.textColor = self.tintValueLabel.textColor = slider.tintColor;
    }
}
- (void)focusWithMode:(AVCaptureFocusMode)focusMode exposeWithMode:(AVCaptureExposureMode)exposureMode atDevicePoint:(CGPoint)point monitorSubjectAreaChange:(BOOL)monitorSubjectAreaChange
{
    dispatch_async([self sessionQueue], ^{
        AVCaptureDevice *device = [self videoDevice];
        NSError *error = nil;
        if ([device lockForConfiguration:&error])
        {
            if ([device isFocusPointOfInterestSupported] && [device isFocusModeSupported:focusMode])
            {
                [device setFocusMode:focusMode];
                [device setFocusPointOfInterest:point];
            }
            if ([device isExposurePointOfInterestSupported] && [device isExposureModeSupported:exposureMode])
            {
                [device setExposureMode:exposureMode];
                [device setExposurePointOfInterest:point];
            }
            [device setSubjectAreaChangeMonitoringEnabled:monitorSubjectAreaChange];
            [device unlockForConfiguration];
        }
        else
        {
            NSLog(@"%@", error);
        }
    });
}

+ (void)setFlashMode:(AVCaptureFlashMode)flashMode forDevice:(AVCaptureDevice *)device
{
    if ([device hasFlash] && [device isFlashModeSupported:flashMode])
    {
        NSError *error = nil;
        if ([device lockForConfiguration:&error])
        {
            [device setFlashMode:flashMode];
            [device unlockForConfiguration];
        }
        else
        {
            NSLog(@"%@", error);
        }
    }
}

- (void)setWhiteBalanceGains:(AVCaptureWhiteBalanceGains)gains
{
    NSError *error = nil;
    
    if ([self.videoDevice lockForConfiguration:&error])
    {
        AVCaptureWhiteBalanceGains normalizedGains = [self normalizedGains:gains]; // Conversion can yield out-of-bound values, cap to limits
        [self.videoDevice setWhiteBalanceModeLockedWithDeviceWhiteBalanceGains:normalizedGains completionHandler:nil];
    }
    else
    {
        NSLog(@"%@", error);
    }
}

#pragma mark KVO

- (void)addObservers
{
    NSLog(@"addObservers");
    [self addObserver:self forKeyPath:@"sessionRunningAndDeviceAuthorized" options:(NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew) context:SessionRunningAndDeviceAuthorizedContext];
    [self addObserver:self forKeyPath:@"stillImageOutput.capturingStillImage" options:(NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew) context:CapturingStillImageContext];
    [self addObserver:self forKeyPath:@"movieFileOutput.recording" options:(NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew) context:RecordingContext];
    
    [self addObserver:self forKeyPath:@"videoDeviceInput.device.focusMode" options:(NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew) context:FocusModeContext];
    [self addObserver:self forKeyPath:@"videoDeviceInput.device.lensPosition" options:(NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew) context:LensPositionContext];
    
    [self addObserver:self forKeyPath:@"videoDeviceInput.device.exposureMode" options:(NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew) context:ExposureModeContext];
    [self addObserver:self forKeyPath:@"videoDeviceInput.device.exposureDuration" options:(NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew) context:ExposureDurationContext];
    [self addObserver:self forKeyPath:@"videoDeviceInput.device.ISO" options:(NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew) context:ISOContext];
    [self addObserver:self forKeyPath:@"videoDeviceInput.device.exposureTargetOffset" options:(NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew) context:ExposureTargetOffsetContext];
    
    [self addObserver:self forKeyPath:@"videoDeviceInput.device.whiteBalanceMode" options:(NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew) context:WhiteBalanceModeContext];
    [self addObserver:self forKeyPath:@"videoDeviceInput.device.deviceWhiteBalanceGains" options:(NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew) context:DeviceWhiteBalanceGainsContext];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(subjectAreaDidChange:) name:AVCaptureDeviceSubjectAreaDidChangeNotification object:[self videoDevice]];
    
    
    __weak PhotoBoothNormal *weakSelf = self;
    [self setRuntimeErrorHandlingObserver:[[NSNotificationCenter defaultCenter] addObserverForName:AVCaptureSessionRuntimeErrorNotification object:[self session] queue:nil usingBlock:^(NSNotification *note) {
        PhotoBoothNormal *strongSelf = weakSelf;
        dispatch_async([strongSelf sessionQueue], ^{
            [[strongSelf session] startRunning];
            if (AVCaptureDevicePositionBack)
            {
                NSLog(@"addObservers AVCaptureDevicePositionBack");
            }
            else
            {
                NSLog(@"addObservers AVCaptureDevicePositionFront");
                [[strongSelf recordButton1] setTitle:NSLocalizedString(@"Start", @"Recording button record title") forState:UIControlStateNormal];
            }
        });
    }]];
}




- (void)removeObservers
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVCaptureDeviceSubjectAreaDidChangeNotification object:[self videoDevice]];
    [[NSNotificationCenter defaultCenter] removeObserver:[self runtimeErrorHandlingObserver]];
    
    @try{
        [self removeObserver:self forKeyPath:@"sessionRunningAndDeviceAuthorized" context:SessionRunningAndDeviceAuthorizedContext];
        [self removeObserver:self forKeyPath:@"stillImageOutput.capturingStillImage" context:CapturingStillImageContext];
        [self removeObserver:self forKeyPath:@"movieFileOutput.recording" context:RecordingContext];
        [self removeObserver:self forKeyPath:@"videoDeviceInput.device.focusMode" context:FocusModeContext];
        [self removeObserver:self forKeyPath:@"videoDeviceInput.device.exposureMode" context:ExposureModeContext];
        [self removeObserver:self forKeyPath:@"videoDeviceInput.device.lensPosition" context:LensPositionContext];
        
        // [self removeObserver:self forKeyPath:@"videoDeviceInput.device.exposureMode" context:ExposureModeContext];
        [self removeObserver:self forKeyPath:@"videoDeviceInput.device.exposureDuration" context:ExposureDurationContext];
        [self removeObserver:self forKeyPath:@"videoDeviceInput.device.ISO" context:ISOContext];
        [self removeObserver:self forKeyPath:@"videoDeviceInput.device.exposureTargetOffset" context:ExposureTargetOffsetContext];
        
        [self removeObserver:self forKeyPath:@"videoDeviceInput.device.whiteBalanceMode" context:WhiteBalanceModeContext];
        [self removeObserver:self forKeyPath:@"videoDeviceInput.device.deviceWhiteBalanceGains" context:DeviceWhiteBalanceGainsContext];
    }@catch(id anException){
        //do nothing, obviously it wasn't attached because an exception was thrown
    }
}


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    
    
    {
        if (object == _send_progress) {
            // Handle new fractionCompleted value
            send_hud.progress=_send_progress.fractionCompleted;
            send_hud.labelText=[NSString stringWithFormat:@"Sending... %.f%%",[(NSProgress *)object fractionCompleted] * 100];
            return;
        }else if (object == _receive_progress){
            receive_hud.progress=_receive_progress.fractionCompleted;
            receive_hud.labelText=[NSString stringWithFormat:@"Receiving... %.f%%",[(NSProgress *)object fractionCompleted] * 100];
            return;
        }
        
    }
    
    // NSLog(@"observeValueForKeyPath");
    if (context == FocusModeContext)
    {
        AVCaptureFocusMode oldMode = [change[NSKeyValueChangeOldKey] intValue];
        AVCaptureFocusMode newMode = [change[NSKeyValueChangeNewKey] intValue];
        NSLog(@"focus mode: %@ -> %@", [self stringFromFocusMode:oldMode], [self stringFromFocusMode:newMode]);
        
        self.focusModeControl.selectedSegmentIndex = [self.focusModes indexOfObject:@(newMode)];
        self.lensPositionSlider.enabled = (newMode == AVCaptureFocusModeLocked);
    }
    else if (context == LensPositionContext)
    {
        float newLensPosition = [change[NSKeyValueChangeNewKey] floatValue];
        
        if (self.videoDevice.focusMode != AVCaptureFocusModeLocked)
        {
            self.lensPositionSlider.value = newLensPosition;
        }
        self.lensPositionValueLabel.text = [NSString stringWithFormat:@"%.1f", newLensPosition];
    }
    else if (context == ExposureModeContext)
    {
        AVCaptureExposureMode oldMode = [change[NSKeyValueChangeOldKey] intValue];
        AVCaptureExposureMode newMode = [change[NSKeyValueChangeNewKey] intValue];
        NSLog(@"exposure mode: %@ -> %@", [self stringFromExposureMode:oldMode], [self stringFromExposureMode:newMode]);
        
        self.exposureModeControl.selectedSegmentIndex = [self.exposureModes indexOfObject:@(newMode)];
        self.exposureDurationSlider.enabled = (newMode == AVCaptureExposureModeCustom);
        self.ISOSlider.enabled = (newMode == AVCaptureExposureModeCustom);
    }
    else if (context == ExposureDurationContext)
    {
        double newDurationSeconds = CMTimeGetSeconds([change[NSKeyValueChangeNewKey] CMTimeValue]);
        if (self.videoDevice.exposureMode != AVCaptureExposureModeCustom)
        {
            NSLog(@"OBSERVE VALUE");
            
            double minDurationSeconds = MAX(CMTimeGetSeconds(self.videoDevice.activeFormat.minExposureDuration), EXPOSURE_MINIMUM_DURATION);
            double maxDurationSeconds = CMTimeGetSeconds(self.videoDevice.activeFormat.maxExposureDuration);
            // Map from duration to non-linear UI range 0-1
            double p = ( newDurationSeconds - minDurationSeconds ) / ( maxDurationSeconds - minDurationSeconds ); // Scale to 0-1
            self.exposureDurationSlider.value = pow( p, 1 / EXPOSURE_DURATION_POWER ); // Apply inverse power
            
            if ( newDurationSeconds < 1 )
            {
                int digits = MAX( 0, 2 + floor( log10( newDurationSeconds ) ) );
                self.exposureDurationValueLabel.text = [NSString stringWithFormat:@"1/%.*f", digits, 1/newDurationSeconds];
            }
            else
            {
                self.exposureDurationValueLabel.text = [NSString stringWithFormat:@"%.2f", newDurationSeconds];
            }
        }
    }
    else if (context == ISOContext)
    {
        float newISO = [change[NSKeyValueChangeNewKey] floatValue];
        
        if (self.videoDevice.exposureMode != AVCaptureExposureModeCustom)
        {
            self.ISOSlider.value = newISO;
        }
        self.ISOValueLabel.text = [NSString stringWithFormat:@"%i", (int)newISO];
    }
    else if (context == ExposureTargetOffsetContext)
    {
        float newExposureTargetOffset = [change[NSKeyValueChangeNewKey] floatValue];
        
        self.exposureTargetOffsetSlider.value = newExposureTargetOffset;
        self.exposureTargetOffsetValueLabel.text = [NSString stringWithFormat:@"%.1f", newExposureTargetOffset];
    }
    else if (context == WhiteBalanceModeContext)
    {
        AVCaptureWhiteBalanceMode oldMode = [change[NSKeyValueChangeOldKey] intValue];
        AVCaptureWhiteBalanceMode newMode = [change[NSKeyValueChangeNewKey] intValue];
        NSLog(@"white balance mode: %@ -> %@", [self stringFromWhiteBalanceMode:oldMode], [self stringFromWhiteBalanceMode:newMode]);
        
        self.whiteBalanceModeControl.selectedSegmentIndex = [self.whiteBalanceModes indexOfObject:@(newMode)];
        self.temperatureSlider.enabled = (newMode == AVCaptureWhiteBalanceModeLocked);
        self.tintSlider.enabled = (newMode == AVCaptureWhiteBalanceModeLocked);
    }
    else if (context == DeviceWhiteBalanceGainsContext)
    {
        AVCaptureWhiteBalanceGains newGains;
        [change[NSKeyValueChangeNewKey] getValue:&newGains];
        AVCaptureWhiteBalanceTemperatureAndTintValues newTemperatureAndTint = [self.videoDevice temperatureAndTintValuesForDeviceWhiteBalanceGains:newGains];
        
        if (self.videoDevice.whiteBalanceMode != AVCaptureExposureModeLocked)
        {
            self.temperatureSlider.value = newTemperatureAndTint.temperature;
            self.tintSlider.value = newTemperatureAndTint.tint;
        }
        self.temperatureValueLabel.text = [NSString stringWithFormat:@"%i", (int)newTemperatureAndTint.temperature];
        self.tintValueLabel.text = [NSString stringWithFormat:@"%i", (int)newTemperatureAndTint.tint];
    }
    else if (context == CapturingStillImageContext)
    {
        BOOL isCapturingStillImage = [change[NSKeyValueChangeNewKey] boolValue];
        
        if (isCapturingStillImage)
        {
            [self runStillImageCaptureAnimation];
        }
    }
    else if (context == RecordingContext)
    {
        BOOL isRecording = [change[NSKeyValueChangeNewKey] boolValue];
        
    }
    else if (context == SessionRunningAndDeviceAuthorizedContext)
    {
        BOOL isRunning = [change[NSKeyValueChangeNewKey] boolValue];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            NSLog (@" AVCaptureDevicePositionBack Stop Button Pressed");
            if (isRunning)
            {
                NSLog (@"Record Button Pressed 1");
                
                //        [[self recordButton1] setEnabled:YES];
                
            }
            else
            {
                NSLog (@"Stop Button Pressed 1");
                
                //         [[self recordButton1] setEnabled:NO];
                
            }
            
        });
    }
    else
    {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

- (void)subjectAreaDidChange:(NSNotification *)notification
{
    CGPoint devicePoint = CGPointMake(.5, .5);
    [self focusWithMode:AVCaptureFocusModeContinuousAutoFocus exposeWithMode:AVCaptureExposureModeContinuousAutoExposure atDevicePoint:devicePoint monitorSubjectAreaChange:NO];
}

#pragma mark Utilities

+ (AVCaptureDevice *)deviceWithMediaType:(NSString *)mediaType preferringPosition:(AVCaptureDevicePosition)position
{
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:mediaType];
    AVCaptureDevice *captureDevice = [devices firstObject];
    
    for (AVCaptureDevice *device in devices)
    {
        if ([device position] == position)
        {
            captureDevice = device;
            break;
        }
    }
    
    return captureDevice;
}

- (void)checkDeviceAuthorizationStatus
{
    NSString *mediaType = AVMediaTypeVideo;
    
    [AVCaptureDevice requestAccessForMediaType:mediaType completionHandler:^(BOOL granted) {
        if (granted)
        {
            [self setDeviceAuthorized:YES];
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[[UIAlertView alloc] initWithTitle:@"AVCamManual"
                                            message:@"AVCamManual doesn't have permission to use the Camera"
                                           delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil] show];
                [self setDeviceAuthorized:NO];
            });
        }
    }];
}

- (NSString *)stringFromFocusMode:(AVCaptureFocusMode) focusMode
{
    NSString *string = @"INVALID FOCUS MODE";
    
    if (focusMode == AVCaptureFocusModeLocked)
    {
        string = @"Locked";
    }
    else if (focusMode == AVCaptureFocusModeAutoFocus)
    {
        string = @"Auto";
    }
    else if (focusMode == AVCaptureFocusModeContinuousAutoFocus)
    {
        string = @"ContinuousAuto";
    }
    
    return string;
}

- (NSString *)stringFromExposureMode:(AVCaptureExposureMode) exposureMode
{
    NSString *string = @"INVALID EXPOSURE MODE";
    
    if (exposureMode == AVCaptureExposureModeLocked)
    {
        string = @"Locked";
    }
    else if (exposureMode == AVCaptureExposureModeAutoExpose)
    {
        string = @"Auto";
    }
    else if (exposureMode == AVCaptureExposureModeContinuousAutoExposure)
    {
        string = @"ContinuousAuto";
    }
    else if (exposureMode == AVCaptureExposureModeCustom)
    {
        string = @"Custom";
    }
    
    return string;
}

- (NSString *)stringFromWhiteBalanceMode:(AVCaptureWhiteBalanceMode) whiteBalanceMode
{
    NSString *string = @"INVALID WHITE BALANCE MODE";
    
    if (whiteBalanceMode == AVCaptureWhiteBalanceModeLocked)
    {
        string = @"Locked";
    }
    else if (whiteBalanceMode == AVCaptureWhiteBalanceModeAutoWhiteBalance)
    {
        string = @"Auto";
    }
    else if (whiteBalanceMode == AVCaptureWhiteBalanceModeContinuousAutoWhiteBalance)
    {
        string = @"ContinuousAuto";
    }
    
    return string;
}

- (AVCaptureWhiteBalanceGains)normalizedGains:(AVCaptureWhiteBalanceGains) gains
{
    AVCaptureWhiteBalanceGains g = gains;
    
    g.redGain = MAX(1.0, g.redGain);
    g.greenGain = MAX(1.0, g.greenGain);
    g.blueGain = MAX(1.0, g.blueGain);
    
    g.redGain = MIN(self.videoDevice.maxWhiteBalanceGain, g.redGain);
    g.greenGain = MIN(self.videoDevice.maxWhiteBalanceGain, g.greenGain);
    g.blueGain = MIN(self.videoDevice.maxWhiteBalanceGain, g.blueGain);
    
    return g;
}


//DEALLOC//
-(void) dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver: self];
    [[UIDevice currentDevice] endGeneratingDeviceOrientationNotifications];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    dispatch_async([self sessionQueue],
                   ^{
                       [self addObservers];
                       
                       [[self session] startRunning];
                       
                   });
    
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    NSLog(@"YEP. Disappeared");
    dispatch_async([self sessionQueue], ^{
        [[self session] stopRunning];
        
        //        [self removeObservers];
    });
    
    
    LCBluetoothManager *bt = [LCBluetoothManager sharedInstance];
    for (CBPeripheral *lumeCube in bt.connectedPeripherals) {
        [bt disconnectPeripheral:lumeCube];
       NSLog(@"LUME BYE BYE");
    }
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"DURATION_SECOND"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"TEMPERATURE"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"TINT"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}





-(void)disable {
    printport.alpha = 0.5;
    printport.userInteractionEnabled = FALSE;
}

-(void)enable {
    printport.alpha = 1.0;
    printport.userInteractionEnabled = TRUE;
}

-(void)disable1 {
    printip.alpha = 0.5;
    printip.userInteractionEnabled = FALSE;
}

-(void)enable1 {
    printip.alpha = 1.0;
    printip.userInteractionEnabled = TRUE;
}



- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    
    
    [coordinator animateAlongsideTransition:nil completion:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        
    }];
    
    UIDeviceOrientation deviceOrientation = [UIDevice currentDevice].orientation;
    
    if ( UIDeviceOrientationIsPortrait( deviceOrientation ) || UIDeviceOrientationIsLandscape( deviceOrientation ) ) {
        AVCaptureVideoPreviewLayer *previewLayer = (AVCaptureVideoPreviewLayer *)self.previewView.layer;
        previewLayer.connection.videoOrientation = (AVCaptureVideoOrientation)deviceOrientation;
    }
    
    
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}

- (BOOL)shouldAutorotate
{
    // Disable autorotation of the interface when recording is in progress
    return ! self.movieFileOutput.isRecording;
}




- (BOOL)prefersStatusBarHidden
{
    return YES;
}




//DROPBOX UPLOAD//


//KEYBOARD//


//UNSURE CALLED ACTIONS//

//- (void)updatePreviewLayer
//{
//    CGAffineTransform transform = CGAffineTransformIdentity;
//    switch ( UIDevice.currentDevice.orientation )
//    {
//        case UIDeviceOrientationLandscapeLeft:
//            transform = CGAffineTransformRotate(transform, M_PI);
//            break;
//        case UIDeviceOrientationLandscapeRight:
//            transform = CGAffineTransformRotate(transform, 0);
//            break;
//        case UIDeviceOrientationPortrait:
//            transform = CGAffineTransformRotate(transform,M_PI_2);
//            break;
//        default:
//            break;
//    }
//    videoLayer.affineTransform = transform;
//    [self.previewView.layer addSublayer:videoLayer];
//}




////PHOTO BOOTH LAYOUTS////



- (UIImage *)makeLayoutImage
{
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"AlbumName"];
    
    NSString *background = [self.record valueForKey:@"printbackgroundcolor"];
    NSString *font = [self.record valueForKey:@"printfontbordercolor"];
    NSString *eventname = [self.record valueForKey:@"eventname"];
    NSString *eventdate = [self.record valueForKey:@"eventdate"];
    
    NSString * string61 = [self.record valueForKey:@"shrTheme"];
    NSLog(@"SENDGRID IS = %@", string61);
    NSString * overlay = [self.record valueForKey:@"switchOverlay"];
    
    NSData *bgimage = [self.record valueForKey:@"photoboothlayoutbackground"];
    printlayoutbackground.image = [UIImage imageWithData:bgimage];
    
    NSString *strAlbumName = [NSString stringWithFormat:@"%@_Layout", savedValue];
    NSString *numberofphotos = [self.record valueForKey:@"numberofphotos"];
    
    [self customLayoutWithType:string61 andOverlay:overlay andNumberOfPhoto:numberofphotos andAlbumName:strAlbumName];
    
    //4x6 Layouts
    
    if ([string61 isEqualToString:@"0"])
    {
        LayOutOne* layoutController = [[LayOutOne alloc] initWithNibName:@"LayOutOne" bundle:nil];
        
        
        if (bgimage == nil)
        {
            [layoutController setEventDate:eventdate];
            [layoutController setEventName:eventname];
            layoutController.view.backgroundColor = [UIColor colorWithString:background];
            layoutController.lblEventNameRight.textColor = [UIColor colorWithString:font];
            layoutController.lblEventNameLeft.textColor = [UIColor colorWithString:font];
            layoutController.lblEventDateRight.textColor = [UIColor colorWithString:font];
            layoutController.lblEventDateLeft.textColor = [UIColor colorWithString:font];
            
            
            layoutController.lblEventNameRight.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventNameLeft.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventDateRight.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventDateLeft.adjustsFontSizeToFitWidth = YES;
        }
        
        else
        {
            layoutController.lblEventNameRight.hidden = YES;
            layoutController.lblEventNameLeft.hidden = YES;
            layoutController.lblEventDateRight.hidden = YES;
            layoutController.lblEventDateLeft.hidden = YES;
            [layoutController setIsOverlay:overlay];
            [layoutController setImage:[UIImage imageWithData:bgimage]];
        }
        
        
        
        
        int tagView = 1;
        for (int i = 1; i <= [numberofphotos intValue]; i++)
        {
            NSLog(@"mPhotoCount: %ld", (long)[numberofphotos intValue]);
            
            UIImageView *imageLeft = (UIImageView *) [layoutController.view viewWithTag:tagView];
            UIImageView *imageRight = (UIImageView *) [layoutController.view viewWithTag:(tagView + 1)];
            
            UIImage *img = [UIImage imageWithData:[_arrSlidshowImg objectAtIndex:i-1]];
            [imageLeft setImage:img];
            [imageRight setImage:img];
            
            tagView += 2;
        }
        
         NSString * share4 = [self.record valueForKey:@"switchSharingLayout"];
            
            if ([share4  isEqual: @"YES"])
            {
                [self getPath];
                NSString *prefixString = @"Image";
                
                NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
                NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
                imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
                UIImage *image= [self captureView:layoutController.view];
                data = UIImageJPEGRepresentation(image,0.5);
                [data writeToFile:imagePath atomically:YES];
                
                NSLog(@"filepath Make Layout =%@",imagePath);
                refURL = [NSURL fileURLWithPath:imagePath];
                if ([[_appDelegate.mcManager.session connectedPeers] count]) {
                    NSLog(@"SEND LAYOUT");
                    [self sendResource];
                }
                
                
          
            
            else
            {
                
                
                NSLog (@"Sharing Layout IS OFF");
            }
            
            
        }
        
        else
        {
            if (![[DBSession sharedSession] isLinked])
            {
                
                
                
            }
            
            else
            {
                NSString *prefixString = @"Image";
                
                NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
                NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
                
                
                [self getPath];
                imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
                UIImage *image= [self captureView:layoutController.view];
                data = UIImageJPEGRepresentation(image,0.5);
                [data writeToFile:imagePath atomically:YES];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                                        stringForKey:@"AlbumName"];
                NSArray* words = [savedValue componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                NSString* nospacestring = [words componentsJoinedByString:@""];
                
                NSString *destDir = [NSString stringWithFormat: @"/%@_layout/", nospacestring];
                restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
                restClient.delegate = self;
                [restClient uploadFile:[NSString stringWithFormat:@"%@.jpg",uniqueFileName] toPath:destDir withParentRev:nil fromPath:imagePath];
                
                
            }
            
        }
        

        NSString *valueToSave = [[NSUserDefaults standardUserDefaults]
                                 stringForKey:@"printport"];
        NSString *valueToSave1 = [[NSUserDefaults standardUserDefaults]
                                  stringForKey:@"printip"];
        
      
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            dispatch_async(dispatch_get_main_queue(), ^{
               
                NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
                NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
                
                NSURL * url = [NSURL URLWithString:[NSString stringWithFormat: @"http://%@:%@", valueToSave1,valueToSave]];
                NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
                NSData *imageData = UIImageJPEGRepresentation([self captureView:layoutController.view], 0.5);
                
                [urlRequest setHTTPMethod:@"POST"];
                [urlRequest setHTTPBody:imageData];
                
                NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest
                                                                   completionHandler:^(NSData *data5, NSURLResponse *response, NSError *error) {
                                                                       NSLog(@"Response:%@ %@\n", response, error);
                                                                       if(error == nil)
                                                                       {
                                                                           NSString * text = [[NSString alloc] initWithData: data5 encoding: NSUTF8StringEncoding];
                                                                           NSLog(@"Data = %@",text);
                                                                       }
                                                                       
                                                                   }];
                [dataTask resume];
            });
            
        });
        
        [CustomAlbum addNewAssetWithImage:[self captureView:layoutController.view] toAlbum:[CustomAlbum getMyAlbumWithName:strAlbumName] onSuccess:^(NSString *ImageId) {
            recentImg = ImageId;
            NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
            NSData *data3 = [NSKeyedArchiver archivedDataWithRootObject:[self captureView:layoutController.view]];
            [currentDefaults setObject:data3 forKey:@"layoutphoto"];
            
            
            
        } onError:^(NSError *error) {
            NSLog(@"probelm in saving image");
        }];
        
        
    }
    
    
    
    if ([string61 isEqualToString:@"1"])
    {
        LayOutTwo* layoutController = [[LayOutTwo alloc] initWithNibName:@"LayOutTwo" bundle:nil];
        
        
        if (bgimage == nil)
        {
            [layoutController setEventDate:eventdate];
            [layoutController setEventName:eventname];
            layoutController.view.backgroundColor = [UIColor colorWithString:background];
            layoutController.lblEventNameRight.textColor = [UIColor colorWithString:font];
            layoutController.lblEventNameLeft.textColor = [UIColor colorWithString:font];
            //            layoutController.lblEventDateRight.textColor = [UIColor colorWithString:font];
            //            layoutController.lblEventDateLeft.textColor = [UIColor colorWithString:font];
            
            
            layoutController.lblEventNameRight.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventNameLeft.adjustsFontSizeToFitWidth = YES;
            //            layoutController.lblEventDateRight.adjustsFontSizeToFitWidth = YES;
            //            layoutController.lblEventDateLeft.adjustsFontSizeToFitWidth = YES;
        }
        
        else
        {
            layoutController.lblEventNameRight.hidden = YES;
            layoutController.lblEventNameLeft.hidden = YES;
            //            layoutController.lblEventDateRight.hidden = YES;
            //            layoutController.lblEventDateLeft.hidden = YES;
            [layoutController setIsOverlay:overlay];
            [layoutController setImage:[UIImage imageWithData:bgimage]];
        }
        
        
        
        
        int tagView = 1;
        for (int i = 1; i <= [numberofphotos intValue]; i++)
        {
            NSLog(@"mPhotoCount: %ld", (long)[numberofphotos intValue]);
            
            UIImageView *imageLeft = (UIImageView *) [layoutController.view viewWithTag:tagView];
            UIImageView *imageRight = (UIImageView *) [layoutController.view viewWithTag:(tagView + 1)];
            
            UIImage *img = [UIImage imageWithData:[_arrSlidshowImg objectAtIndex:i-1]];
            [imageLeft setImage:img];
            [imageRight setImage:img];
            
            tagView += 2;
        }
        
            NSString * share4 = [self.record valueForKey:@"switchSharingLayout"];
            
            if ([share4  isEqual: @"YES"])
            {
                [self getPath];
                NSString *prefixString = @"Image";
                
                NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
                NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
                imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
                UIImage *image= [self captureView:layoutController.view];
                data = UIImageJPEGRepresentation(image,0.5);
                [data writeToFile:imagePath atomically:YES];
                
                NSLog(@"filepath Make Layout =%@",imagePath);
                refURL = [NSURL fileURLWithPath:imagePath];
                if ([[_appDelegate.mcManager.session connectedPeers] count]) {
                    NSLog(@"SEND LAYOUT");
                    [self sendResource];
                }
                
                
          
            
            else
            {
                
                
                NSLog (@"Sharing Layout IS OFF");
            }
            
            
        }
        
        else
        {
            if (![[DBSession sharedSession] isLinked])
            {
                
                
                
            }
            
            else
            {
                NSString *prefixString = @"Image";
                
                NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
                NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
                
                
                [self getPath];
                imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
                UIImage *image= [self captureView:layoutController.view];
                data = UIImageJPEGRepresentation(image,0.5);
                [data writeToFile:imagePath atomically:YES];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                                        stringForKey:@"AlbumName"];
                NSArray* words = [savedValue componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                NSString* nospacestring = [words componentsJoinedByString:@""];
                
                NSString *destDir = [NSString stringWithFormat: @"/%@_layout/", nospacestring];
                restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
                restClient.delegate = self;
                [restClient uploadFile:[NSString stringWithFormat:@"%@.jpg",uniqueFileName] toPath:destDir withParentRev:nil fromPath:imagePath];
                
                
            }
            
        }
        
        NSString *valueToSave = [[NSUserDefaults standardUserDefaults]
                                 stringForKey:@"printport"];
        NSString *valueToSave1 = [[NSUserDefaults standardUserDefaults]
                                  stringForKey:@"printip"];
        
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
                NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
                
                NSURL * url = [NSURL URLWithString:[NSString stringWithFormat: @"http://%@:%@", valueToSave1,valueToSave]];
                NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
                NSData *imageData = UIImageJPEGRepresentation([self captureView:layoutController.view], 0.5);
                
                [urlRequest setHTTPMethod:@"POST"];
                [urlRequest setHTTPBody:imageData];
                
                NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest
                                                                   completionHandler:^(NSData *data5, NSURLResponse *response, NSError *error) {
                                                                       NSLog(@"Response:%@ %@\n", response, error);
                                                                       if(error == nil)
                                                                       {
                                                                           NSString * text = [[NSString alloc] initWithData: data5 encoding: NSUTF8StringEncoding];
                                                                           NSLog(@"Data = %@",text);
                                                                       }
                                                                       
                                                                   }];
                [dataTask resume];
            });
            
        });
        [CustomAlbum addNewAssetWithImage:[self captureView:layoutController.view] toAlbum:[CustomAlbum getMyAlbumWithName:strAlbumName] onSuccess:^(NSString *ImageId) {
            recentImg = ImageId;
            NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
            NSData *data3 = [NSKeyedArchiver archivedDataWithRootObject:[self captureView:layoutController.view]];
            [currentDefaults setObject:data3 forKey:@"layoutphoto"];
            
            
            
        } onError:^(NSError *error) {
            NSLog(@"probelm in saving image");
        }];
        
        
    }
    
    
    if ([string61 isEqualToString:@"2"])
    {
        LayOutThree* layoutController = [[LayOutThree alloc] initWithNibName:@"LayOutThree" bundle:nil];
        
        
        if (bgimage == nil)
        {
            [layoutController setEventDate:eventdate];
            [layoutController setEventName:eventname];
            layoutController.view.backgroundColor = [UIColor colorWithString:background];
            // layoutController.lblEventNameRight.textColor = [UIColor colorWithString:font];
            layoutController.lblEventNameLeft.textColor = [UIColor colorWithString:font];
            //            layoutController.lblEventDateRight.textColor = [UIColor colorWithString:font];
            layoutController.lblEventDateLeft.textColor = [UIColor colorWithString:font];
            
            
            //   layoutController.lblEventNameRight.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventNameLeft.adjustsFontSizeToFitWidth = YES;
            //            layoutController.lblEventDateRight.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventDateLeft.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventNameLeft.transform = CGAffineTransformMakeRotation(3.14 / 2);
            layoutController.lblEventDateLeft.transform = CGAffineTransformMakeRotation(3.14 / 2);
        }
        
        else
        {
            //   layoutController.lblEventNameRight.hidden = YES;
            layoutController.lblEventNameLeft.hidden = YES;
            //            layoutController.lblEventDateRight.hidden = YES;
            layoutController.lblEventDateLeft.hidden = YES;
            [layoutController setIsOverlay:overlay];
            [layoutController setImage:[UIImage imageWithData:bgimage]];
        }
        
        
        
        for (int i = 0; i <= [numberofphotos intValue]; i++) {
            UIImage *img = [UIImage imageWithData:[_arrSlidshowImg objectAtIndex:i % 4]];
            UIImageView *imgView1 = (UIImageView *) [layoutController.view viewWithTag:i + 1];
            [imgView1 setImage:img];
            
            if ([overlay isEqualToString:@"NO"])
            {
                imgView1.layer.borderWidth = 4;
                imgView1.layer.borderColor = [UIColor colorWithString:font].CGColor;
            }
            
            
        }
        
        
        
        NSString * share4 = [self.record valueForKey:@"switchSharingLayout"];
        
       
            if ([share4  isEqual: @"YES"])
            {
                [self getPath];
                NSString *prefixString = @"Image";
                
                NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
                NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
                imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
                UIImage *image= [self captureView:layoutController.view];
                data = UIImageJPEGRepresentation(image,0.5);
                [data writeToFile:imagePath atomically:YES];
                
                NSLog(@"filepath Make Layout =%@",imagePath);
                refURL = [NSURL fileURLWithPath:imagePath];
                if ([[_appDelegate.mcManager.session connectedPeers] count]) {
                    NSLog(@"SEND LAYOUT");
                    [self sendResource];
                }
                
              
            else
            {
                
                
                NSLog (@"Sharing Layout IS OFF");
            }
            
            
        }
        
        else
        {
            if (![[DBSession sharedSession] isLinked])
            {
                
                
                
            }
            
            else
            {
                NSString *prefixString = @"Image";
                
                NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
                NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
                
                
                [self getPath];
                imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
                UIImage *image= [self captureView:layoutController.view];
                data = UIImageJPEGRepresentation(image,0.5);
                [data writeToFile:imagePath atomically:YES];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                                        stringForKey:@"AlbumName"];
                NSArray* words = [savedValue componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                NSString* nospacestring = [words componentsJoinedByString:@""];
                
                NSString *destDir = [NSString stringWithFormat: @"/%@_layout/", nospacestring];
                restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
                restClient.delegate = self;
                [restClient uploadFile:[NSString stringWithFormat:@"%@.jpg",uniqueFileName] toPath:destDir withParentRev:nil fromPath:imagePath];
                
                
            }
            
        }

        NSString *valueToSave = [[NSUserDefaults standardUserDefaults]
                                 stringForKey:@"printport"];
        NSString *valueToSave1 = [[NSUserDefaults standardUserDefaults]
                                  stringForKey:@"printip"];
        
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
                NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
                
                NSURL * url = [NSURL URLWithString:[NSString stringWithFormat: @"http://%@:%@", valueToSave1,valueToSave]];
                NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
                NSData *imageData = UIImageJPEGRepresentation([self captureView:layoutController.view], 0.5);
                
                [urlRequest setHTTPMethod:@"POST"];
                [urlRequest setHTTPBody:imageData];
                
                NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest
                                                                   completionHandler:^(NSData *data5, NSURLResponse *response, NSError *error) {
                                                                       NSLog(@"Response:%@ %@\n", response, error);
                                                                       if(error == nil)
                                                                       {
                                                                           NSString * text = [[NSString alloc] initWithData: data5 encoding: NSUTF8StringEncoding];
                                                                           NSLog(@"Data = %@",text);
                                                                       }
                                                                       
                                                                   }];
                [dataTask resume];
            });
            
        });
        [CustomAlbum addNewAssetWithImage:[self captureView:layoutController.view] toAlbum:[CustomAlbum getMyAlbumWithName:strAlbumName] onSuccess:^(NSString *ImageId) {
            recentImg = ImageId;
            NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
            NSData *data3 = [NSKeyedArchiver archivedDataWithRootObject:[self captureView:layoutController.view]];
            [currentDefaults setObject:data3 forKey:@"layoutphoto"];
            
            
            
        } onError:^(NSError *error) {
            NSLog(@"probelm in saving image");
        }];
        
        
    }
    
    
    
    if ([string61 isEqualToString:@"3"])
    {
        LayOutFour* layoutController = [[LayOutFour alloc] initWithNibName:@"LayOutFour" bundle:nil];
        
        
        if (bgimage == nil)
        {
            [layoutController setEventDate:eventdate];
            [layoutController setEventName:eventname];
            layoutController.view.backgroundColor = [UIColor colorWithString:background];
            // layoutController.lblEventNameRight.textColor = [UIColor colorWithString:font];
            layoutController.lblEventNameLeft.textColor = [UIColor colorWithString:font];
            //            layoutController.lblEventDateRight.textColor = [UIColor colorWithString:font];
            layoutController.lblEventDateLeft.textColor = [UIColor colorWithString:font];
            
            
            //   layoutController.lblEventNameRight.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventNameLeft.adjustsFontSizeToFitWidth = YES;
            //            layoutController.lblEventDateRight.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventDateLeft.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventNameLeft.transform = CGAffineTransformMakeRotation(3.14 / 2);
            layoutController.lblEventDateLeft.transform = CGAffineTransformMakeRotation(3.14 / 2);
        }
        
        else
        {
            //   layoutController.lblEventNameRight.hidden = YES;
            layoutController.lblEventNameLeft.hidden = YES;
            //            layoutController.lblEventDateRight.hidden = YES;
            layoutController.lblEventDateLeft.hidden = YES;
            [layoutController setIsOverlay:overlay];
            [layoutController setImage:[UIImage imageWithData:bgimage]];
        }
        
        
        
        for (int i = 0; i <= [numberofphotos intValue]; i++) {
            UIImage *img = [UIImage imageWithData:[_arrSlidshowImg objectAtIndex:i % [numberofphotos intValue]]];
            UIImageView *imgView1 = (UIImageView *) [layoutController.view viewWithTag:i + 1];
            [imgView1 setImage:img];
            
            if ([overlay isEqualToString:@"NO"])
            {
                imgView1.layer.borderWidth = 4;
                imgView1.layer.borderColor = [UIColor colorWithString:font].CGColor;
            }
            
            
        }
        
        
        
         NSString * share4 = [self.record valueForKey:@"switchSharingLayout"];
            
            if ([share4  isEqual: @"YES"])
            {
                [self getPath];
                NSString *prefixString = @"Image";
                
                NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
                NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
                imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
                UIImage *image= [self captureView:layoutController.view];
                data = UIImageJPEGRepresentation(image,0.5);
                [data writeToFile:imagePath atomically:YES];
                
                NSLog(@"filepath Make Layout =%@",imagePath);
                refURL = [NSURL fileURLWithPath:imagePath];
                if ([[_appDelegate.mcManager.session connectedPeers] count]) {
                    NSLog(@"SEND LAYOUT");
                    [self sendResource];
                }
                
                
          
            
            else
            {
                
                
                NSLog (@"Sharing Layout IS OFF");
            }
            
            
        }


        else
        {
            if (![[DBSession sharedSession] isLinked])
            {
                
                
                
            }
            
            else
            {
                NSString *prefixString = @"Image";
                
                NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
                NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
                
                
                [self getPath];
                imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
                UIImage *image= [self captureView:layoutController.view];
                data = UIImageJPEGRepresentation(image,0.5);
                [data writeToFile:imagePath atomically:YES];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                                        stringForKey:@"AlbumName"];
                NSArray* words = [savedValue componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                NSString* nospacestring = [words componentsJoinedByString:@""];
                
                NSString *destDir = [NSString stringWithFormat: @"/%@_layout/", nospacestring];
                restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
                restClient.delegate = self;
                [restClient uploadFile:[NSString stringWithFormat:@"%@.jpg",uniqueFileName] toPath:destDir withParentRev:nil fromPath:imagePath];
                
                
            }
            
        }
        
        NSString *valueToSave = [[NSUserDefaults standardUserDefaults]
                                 stringForKey:@"printport"];
        NSString *valueToSave1 = [[NSUserDefaults standardUserDefaults]
                                  stringForKey:@"printip"];
        
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
                NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
                
                NSURL * url = [NSURL URLWithString:[NSString stringWithFormat: @"http://%@:%@", valueToSave1,valueToSave]];
                NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
                NSData *imageData = UIImageJPEGRepresentation([self captureView:layoutController.view], 0.5);
                
                [urlRequest setHTTPMethod:@"POST"];
                [urlRequest setHTTPBody:imageData];
                
                NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest
                                                                   completionHandler:^(NSData *data5, NSURLResponse *response, NSError *error) {
                                                                       NSLog(@"Response:%@ %@\n", response, error);
                                                                       if(error == nil)
                                                                       {
                                                                           NSString * text = [[NSString alloc] initWithData: data5 encoding: NSUTF8StringEncoding];
                                                                           NSLog(@"Data = %@",text);
                                                                       }
                                                                       
                                                                   }];
                [dataTask resume];
            });
            
        });
        [CustomAlbum addNewAssetWithImage:[self captureView:layoutController.view] toAlbum:[CustomAlbum getMyAlbumWithName:strAlbumName] onSuccess:^(NSString *ImageId) {
            recentImg = ImageId;
            NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
            NSData *data3 = [NSKeyedArchiver archivedDataWithRootObject:[self captureView:layoutController.view]];
            [currentDefaults setObject:data3 forKey:@"layoutphoto"];
            
            
            
        } onError:^(NSError *error) {
            NSLog(@"probelm in saving image");
        }];
        
        
    }
    
    
    
    
    
    if ([string61 isEqualToString:@"4"])
    {
        LayOutFive* layoutController = [[LayOutFive alloc] initWithNibName:@"LayOutFive" bundle:nil];
        
        
        if (bgimage == nil)
        {
            [layoutController setEventDate:eventdate];
            [layoutController setEventName:eventname];
            layoutController.view.backgroundColor = [UIColor colorWithString:background];
            // layoutController.lblEventNameRight.textColor = [UIColor colorWithString:font];
            layoutController.lblEventNameLeft.textColor = [UIColor colorWithString:font];
            //            layoutController.lblEventDateRight.textColor = [UIColor colorWithString:font];
            layoutController.lblEventDateLeft.textColor = [UIColor colorWithString:font];
            
            
            //   layoutController.lblEventNameRight.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventNameLeft.adjustsFontSizeToFitWidth = YES;
            //            layoutController.lblEventDateRight.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventDateLeft.adjustsFontSizeToFitWidth = YES;
            
        }
        
        else
        {
            //   layoutController.lblEventNameRight.hidden = YES;
            layoutController.lblEventNameLeft.hidden = YES;
            //            layoutController.lblEventDateRight.hidden = YES;
            layoutController.lblEventDateLeft.hidden = YES;
            [layoutController setIsOverlay:overlay];
            [layoutController setImage:[UIImage imageWithData:bgimage]];
        }
        
        
        
        for (int i = 0; i <= [numberofphotos intValue]; i++) {
            UIImage *img = [UIImage imageWithData:[_arrSlidshowImg objectAtIndex:i % [numberofphotos intValue]]];
            UIImageView *imgView1 = (UIImageView *) [layoutController.view viewWithTag:i + 1];
            [imgView1 setImage:img];
            
            if ([overlay isEqualToString:@"NO"])
            {
                imgView1.layer.borderWidth = 4;
                imgView1.layer.borderColor = [UIColor colorWithString:font].CGColor;
            }
            
            
        }
        
        
        
        
      
            
            NSString * share4 = [self.record valueForKey:@"switchSharingLayout"];
            
            if ([share4  isEqual: @"YES"])
            {
                [self getPath];
                NSString *prefixString = @"Image";
                
                NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
                NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
                imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
                UIImage *image= [self captureView:layoutController.view];
                data = UIImageJPEGRepresentation(image,0.5);
                [data writeToFile:imagePath atomically:YES];
                
                NSLog(@"filepath Make Layout =%@",imagePath);
                refURL = [NSURL fileURLWithPath:imagePath];
                if ([[_appDelegate.mcManager.session connectedPeers] count]) {
                    NSLog(@"SEND LAYOUT");
                    [self sendResource];
                }
                
                
            
            
            else
            {
                
                
                NSLog (@"Sharing Layout IS OFF");
            }
            
            
        }
        
        else
        {
            if (![[DBSession sharedSession] isLinked])
            {
                
                
                
            }
            
            else
            {
                NSString *prefixString = @"Image";
                
                NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
                NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
                
                
                [self getPath];
                imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
                UIImage *image= [self captureView:layoutController.view];
                data = UIImageJPEGRepresentation(image,0.5);
                [data writeToFile:imagePath atomically:YES];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                                        stringForKey:@"AlbumName"];
                NSArray* words = [savedValue componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                NSString* nospacestring = [words componentsJoinedByString:@""];
                
                NSString *destDir = [NSString stringWithFormat: @"/%@_layout/", nospacestring];
                restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
                restClient.delegate = self;
                [restClient uploadFile:[NSString stringWithFormat:@"%@.jpg",uniqueFileName] toPath:destDir withParentRev:nil fromPath:imagePath];
                
                
            }
            
        }

        NSString *valueToSave = [[NSUserDefaults standardUserDefaults]
                                 stringForKey:@"printport"];
        NSString *valueToSave1 = [[NSUserDefaults standardUserDefaults]
                                  stringForKey:@"printip"];
        
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
                NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
                
                NSURL * url = [NSURL URLWithString:[NSString stringWithFormat: @"http://%@:%@", valueToSave1,valueToSave]];
                NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
                NSData *imageData = UIImageJPEGRepresentation([self captureView:layoutController.view], 0.5);
                
                [urlRequest setHTTPMethod:@"POST"];
                [urlRequest setHTTPBody:imageData];
                
                NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest
                                                                   completionHandler:^(NSData *data5, NSURLResponse *response, NSError *error) {
                                                                       NSLog(@"Response:%@ %@\n", response, error);
                                                                       if(error == nil)
                                                                       {
                                                                           NSString * text = [[NSString alloc] initWithData: data5 encoding: NSUTF8StringEncoding];
                                                                           NSLog(@"Data = %@",text);
                                                                       }
                                                                       
                                                                   }];
                [dataTask resume];
            });
            
        });
        [CustomAlbum addNewAssetWithImage:[self captureView:layoutController.view] toAlbum:[CustomAlbum getMyAlbumWithName:strAlbumName] onSuccess:^(NSString *ImageId) {
            recentImg = ImageId;
            NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
            NSData *data3 = [NSKeyedArchiver archivedDataWithRootObject:[self captureView:layoutController.view]];
            [currentDefaults setObject:data3 forKey:@"layoutphoto"];
            
            
            
        } onError:^(NSError *error) {
            NSLog(@"probelm in saving image");
        }];
        
        
    }
    
    
    
    
    
    
    
    if ([string61 isEqualToString:@"5"])
    {
        LayOutSix* layoutController = [[LayOutSix alloc] initWithNibName:@"LayOutSix" bundle:nil];
        
        
        if (bgimage == nil)
        {
            [layoutController setEventDate:eventdate];
            [layoutController setEventName:eventname];
            layoutController.view.backgroundColor = [UIColor colorWithString:background];
            // layoutController.lblEventNameRight.textColor = [UIColor colorWithString:font];
            layoutController.lblEventNameLeft.textColor = [UIColor colorWithString:font];
            //            layoutController.lblEventDateRight.textColor = [UIColor colorWithString:font];
            layoutController.lblEventDateLeft.textColor = [UIColor colorWithString:font];
            
            
            //   layoutController.lblEventNameRight.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventNameLeft.adjustsFontSizeToFitWidth = YES;
            //            layoutController.lblEventDateRight.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventDateLeft.adjustsFontSizeToFitWidth = YES;
            
        }
        
        else
        {
            //   layoutController.lblEventNameRight.hidden = YES;
            layoutController.lblEventNameLeft.hidden = YES;
            //            layoutController.lblEventDateRight.hidden = YES;
            layoutController.lblEventDateLeft.hidden = YES;
            [layoutController setIsOverlay:overlay];
            [layoutController setImage:[UIImage imageWithData:bgimage]];
        }
        
        
        
        for (int i = 0; i <= [numberofphotos intValue]; i++) {
            UIImage *img = [UIImage imageWithData:[_arrSlidshowImg objectAtIndex:i % [numberofphotos intValue]]];
            UIImageView *imgView1 = (UIImageView *) [layoutController.view viewWithTag:i + 1];
            [imgView1 setImage:img];
            
            if ([overlay isEqualToString:@"NO"])
            {
                imgView1.layer.borderWidth = 4;
                imgView1.layer.borderColor = [UIColor colorWithString:font].CGColor;
            }
            
            
        }
        
        
        
         NSString * share4 = [self.record valueForKey:@"switchSharingLayout"];
            
            if ([share4  isEqual: @"YES"])
            {
                [self getPath];
                NSString *prefixString = @"Image";
                
                NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
                NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
                imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
                UIImage *image= [self captureView:layoutController.view];
                data = UIImageJPEGRepresentation(image,0.5);
                [data writeToFile:imagePath atomically:YES];
                
                NSLog(@"filepath Make Layout =%@",imagePath);
                refURL = [NSURL fileURLWithPath:imagePath];
                if ([[_appDelegate.mcManager.session connectedPeers] count]) {
                    NSLog(@"SEND LAYOUT");
                    [self sendResource];
                }
                
                
          
            else
            {
                
                
                NSLog (@"Sharing Layout IS OFF");
            }
            
            
        }
        
        else
        {
            if (![[DBSession sharedSession] isLinked])
            {
                
                
                
            }
            
            else
            {
                NSString *prefixString = @"Image";
                
                NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
                NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
                
                
                [self getPath];
                imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
                UIImage *image= [self captureView:layoutController.view];
                data = UIImageJPEGRepresentation(image,0.5);
                [data writeToFile:imagePath atomically:YES];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                                        stringForKey:@"AlbumName"];
                NSArray* words = [savedValue componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                NSString* nospacestring = [words componentsJoinedByString:@""];
                
                NSString *destDir = [NSString stringWithFormat: @"/%@_layout/", nospacestring];
                restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
                restClient.delegate = self;
                [restClient uploadFile:[NSString stringWithFormat:@"%@.jpg",uniqueFileName] toPath:destDir withParentRev:nil fromPath:imagePath];
                
                
            }
            
        }

        
        NSString *valueToSave = [[NSUserDefaults standardUserDefaults]
                                 stringForKey:@"printport"];
        NSString *valueToSave1 = [[NSUserDefaults standardUserDefaults]
                                  stringForKey:@"printip"];
        
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
                NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
                
                NSURL * url = [NSURL URLWithString:[NSString stringWithFormat: @"http://%@:%@", valueToSave1,valueToSave]];
                NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
                NSData *imageData = UIImageJPEGRepresentation([self captureView:layoutController.view], 0.5);
                
                [urlRequest setHTTPMethod:@"POST"];
                [urlRequest setHTTPBody:imageData];
                
                NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest
                                                                   completionHandler:^(NSData *data5, NSURLResponse *response, NSError *error) {
                                                                       NSLog(@"Response:%@ %@\n", response, error);
                                                                       if(error == nil)
                                                                       {
                                                                           NSString * text = [[NSString alloc] initWithData: data5 encoding: NSUTF8StringEncoding];
                                                                           NSLog(@"Data = %@",text);
                                                                       }
                                                                       
                                                                   }];
                [dataTask resume];
            });
            
        });
        [CustomAlbum addNewAssetWithImage:[self captureView:layoutController.view] toAlbum:[CustomAlbum getMyAlbumWithName:strAlbumName] onSuccess:^(NSString *ImageId) {
            recentImg = ImageId;
            NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
            NSData *data3 = [NSKeyedArchiver archivedDataWithRootObject:[self captureView:layoutController.view]];
            [currentDefaults setObject:data3 forKey:@"layoutphoto"];
            
            
            
        } onError:^(NSError *error) {
            NSLog(@"probelm in saving image");
        }];
        
        
    }
    
    
    
    
    if ([string61 isEqualToString:@"6"])
    {
        LayOutSeven* layoutController = [[LayOutSeven alloc] initWithNibName:@"LayOutSeven" bundle:nil];
        
        
        if (bgimage == nil)
        {
            [layoutController setEventDate:eventdate];
            [layoutController setEventName:eventname];
            layoutController.view.backgroundColor = [UIColor colorWithString:background];
            // layoutController.lblEventNameRight.textColor = [UIColor colorWithString:font];
            layoutController.lblEventNameLeft.textColor = [UIColor colorWithString:font];
            //            layoutController.lblEventDateRight.textColor = [UIColor colorWithString:font];
            layoutController.lblEventDateLeft.textColor = [UIColor colorWithString:font];
            
            
            //   layoutController.lblEventNameRight.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventNameLeft.adjustsFontSizeToFitWidth = YES;
            //            layoutController.lblEventDateRight.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventDateLeft.adjustsFontSizeToFitWidth = YES;
            
            layoutController.lblEventDateLeft.transform = CGAffineTransformMakeRotation(3.14 / 2);
            layoutController.lblEventNameLeft.transform = CGAffineTransformMakeRotation(3.14 / 2);
            
        }
        
        else
        {
            //   layoutController.lblEventNameRight.hidden = YES;
            layoutController.lblEventNameLeft.hidden = YES;
            //            layoutController.lblEventDateRight.hidden = YES;
            layoutController.lblEventDateLeft.hidden = YES;
            [layoutController setIsOverlay:overlay];
            [layoutController setImage:[UIImage imageWithData:bgimage]];
        }
        
        
        
        for (int i = 0; i <= [numberofphotos intValue]; i++) {
            UIImage *img = [UIImage imageWithData:[_arrSlidshowImg objectAtIndex:i % [numberofphotos intValue]]];
            UIImageView *imgView1 = (UIImageView *) [layoutController.view viewWithTag:i + 1];
            [imgView1 setImage:img];
            
            if ([overlay isEqualToString:@"NO"])
            {
                imgView1.layer.borderWidth = 4;
                imgView1.layer.borderColor = [UIColor colorWithString:font].CGColor;
                imgView1.transform = CGAffineTransformMakeRotation(3.14 / 2);
            }
            
            
        }
        
        
        
      
            NSString * share4 = [self.record valueForKey:@"switchSharingLayout"];
            
            if ([share4  isEqual: @"YES"])
            {
                [self getPath];
                NSString *prefixString = @"Image";
                
                NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
                NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
                imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
                UIImage *image= [self captureView:layoutController.view];
                data = UIImageJPEGRepresentation(image,0.5);
                [data writeToFile:imagePath atomically:YES];
                
                NSLog(@"filepath Make Layout =%@",imagePath);
                refURL = [NSURL fileURLWithPath:imagePath];
                if ([[_appDelegate.mcManager.session connectedPeers] count]) {
                    NSLog(@"SEND LAYOUT");
                    [self sendResource];
                }
                
                
            
            
            else
            {
                
                
                NSLog (@"Sharing Layout IS OFF");
            }
            
            
        }
        
        else
        {
            if (![[DBSession sharedSession] isLinked])
            {
                
                
                
            }
            
            else
            {
                NSString *prefixString = @"Image";
                
                NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
                NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
                
                
                [self getPath];
                imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
                UIImage *image= [self captureView:layoutController.view];
                data = UIImageJPEGRepresentation(image,0.5);
                [data writeToFile:imagePath atomically:YES];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                                        stringForKey:@"AlbumName"];
                NSArray* words = [savedValue componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                NSString* nospacestring = [words componentsJoinedByString:@""];
                
                NSString *destDir = [NSString stringWithFormat: @"/%@_layout/", nospacestring];
                restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
                restClient.delegate = self;
                [restClient uploadFile:[NSString stringWithFormat:@"%@.jpg",uniqueFileName] toPath:destDir withParentRev:nil fromPath:imagePath];
                
                
            }
            
        }

        NSString *valueToSave = [[NSUserDefaults standardUserDefaults]
                                 stringForKey:@"printport"];
        NSString *valueToSave1 = [[NSUserDefaults standardUserDefaults]
                                  stringForKey:@"printip"];
        
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
                NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
                
                NSURL * url = [NSURL URLWithString:[NSString stringWithFormat: @"http://%@:%@", valueToSave1,valueToSave]];
                NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
                NSData *imageData = UIImageJPEGRepresentation([self captureView:layoutController.view], 0.5);
                
                [urlRequest setHTTPMethod:@"POST"];
                [urlRequest setHTTPBody:imageData];
                
                NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest
                                                                   completionHandler:^(NSData *data5, NSURLResponse *response, NSError *error) {
                                                                       NSLog(@"Response:%@ %@\n", response, error);
                                                                       if(error == nil)
                                                                       {
                                                                           NSString * text = [[NSString alloc] initWithData: data5 encoding: NSUTF8StringEncoding];
                                                                           NSLog(@"Data = %@",text);
                                                                       }
                                                                       
                                                                   }];
                [dataTask resume];
            });
            
        });
        [CustomAlbum addNewAssetWithImage:[self captureView:layoutController.view] toAlbum:[CustomAlbum getMyAlbumWithName:strAlbumName] onSuccess:^(NSString *ImageId) {
            recentImg = ImageId;
            NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
            NSData *data3 = [NSKeyedArchiver archivedDataWithRootObject:[self captureView:layoutController.view]];
            [currentDefaults setObject:data3 forKey:@"layoutphoto"];
            
            
            
        } onError:^(NSError *error) {
            NSLog(@"probelm in saving image");
        }];
        
        
    }
    
    
    
    
    
    if ([string61 isEqualToString:@"7"])
    {
        LayOutEight* layoutController = [[LayOutEight alloc] initWithNibName:@"LayOutEight" bundle:nil];
        
        
        if (bgimage == nil)
        {
            [layoutController setEventDate:eventdate];
            [layoutController setEventName:eventname];
            layoutController.view.backgroundColor = [UIColor colorWithString:background];
            // layoutController.lblEventNameRight.textColor = [UIColor colorWithString:font];
            layoutController.lblEventNameLeft.textColor = [UIColor colorWithString:font];
            //            layoutController.lblEventDateRight.textColor = [UIColor colorWithString:font];
            layoutController.lblEventDateLeft.textColor = [UIColor colorWithString:font];
            
            
            //   layoutController.lblEventNameRight.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventNameLeft.adjustsFontSizeToFitWidth = YES;
            //            layoutController.lblEventDateRight.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventDateLeft.adjustsFontSizeToFitWidth = YES;
            
            layoutController.lblEventDateLeft.transform = CGAffineTransformMakeRotation(3.14);
            layoutController.lblEventNameLeft.transform = CGAffineTransformMakeRotation(3.14);
            
        }
        
        else
        {
            //   layoutController.lblEventNameRight.hidden = YES;
            layoutController.lblEventNameLeft.hidden = YES;
            //            layoutController.lblEventDateRight.hidden = YES;
            layoutController.lblEventDateLeft.hidden = YES;
            [layoutController setIsOverlay:overlay];
            [layoutController setImage:[UIImage imageWithData:bgimage]];
        }
        
        
        
        for (int i = 0; i <= [numberofphotos intValue]; i++) {
            UIImage *img = [UIImage imageWithData:[_arrSlidshowImg objectAtIndex:i % [numberofphotos intValue]]];
            UIImageView *imgView1 = (UIImageView *) [layoutController.view viewWithTag:i + 1];
            [imgView1 setImage:img];
            
            if ([overlay isEqualToString:@"NO"])
            {
                imgView1.layer.borderWidth = 4;
                imgView1.layer.borderColor = [UIColor colorWithString:font].CGColor;
                imgView1.transform = CGAffineTransformMakeRotation(3.14 / 2);
            }
            
            
        }
        
        
        
        
       
            
            NSString * share4 = [self.record valueForKey:@"switchSharingLayout"];
            
            if ([share4  isEqual: @"YES"])
            {
                [self getPath];
                NSString *prefixString = @"Image";
                
                NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
                NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
                imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
                UIImage *image= [self captureView:layoutController.view];
                data = UIImageJPEGRepresentation(image,0.5);
                [data writeToFile:imagePath atomically:YES];
                
                NSLog(@"filepath Make Layout =%@",imagePath);
                refURL = [NSURL fileURLWithPath:imagePath];
                if ([[_appDelegate.mcManager.session connectedPeers] count]) {
                    NSLog(@"SEND LAYOUT");
                    [self sendResource];
                }
                
                
           
            
            else
            {
                
                
                NSLog (@"Sharing Layout IS OFF");
            }
            
            
        }

        
        else
        {
            if (![[DBSession sharedSession] isLinked])
            {
                
                
                
            }
            
            else
            {
                NSString *prefixString = @"Image";
                
                NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
                NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
                
                
                [self getPath];
                imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
                UIImage *image= [self captureView:layoutController.view];
                data = UIImageJPEGRepresentation(image,0.5);
                [data writeToFile:imagePath atomically:YES];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                                        stringForKey:@"AlbumName"];
                NSArray* words = [savedValue componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                NSString* nospacestring = [words componentsJoinedByString:@""];
                
                NSString *destDir = [NSString stringWithFormat: @"/%@_layout/", nospacestring];
                restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
                restClient.delegate = self;
                [restClient uploadFile:[NSString stringWithFormat:@"%@.jpg",uniqueFileName] toPath:destDir withParentRev:nil fromPath:imagePath];
                
                
            }
            
        }

        NSString *valueToSave = [[NSUserDefaults standardUserDefaults]
                                 stringForKey:@"printport"];
        NSString *valueToSave1 = [[NSUserDefaults standardUserDefaults]
                                  stringForKey:@"printip"];
        
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
                NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
                
                NSURL * url = [NSURL URLWithString:[NSString stringWithFormat: @"http://%@:%@", valueToSave1,valueToSave]];
                NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
                NSData *imageData = UIImageJPEGRepresentation([self captureView:layoutController.view], 0.5);
                
                [urlRequest setHTTPMethod:@"POST"];
                [urlRequest setHTTPBody:imageData];
                
                NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest
                                                                   completionHandler:^(NSData *data5, NSURLResponse *response, NSError *error) {
                                                                       NSLog(@"Response:%@ %@\n", response, error);
                                                                       if(error == nil)
                                                                       {
                                                                           NSString * text = [[NSString alloc] initWithData: data5 encoding: NSUTF8StringEncoding];
                                                                           NSLog(@"Data = %@",text);
                                                                       }
                                                                       
                                                                   }];
                [dataTask resume];
            });
            
        });
        
        [CustomAlbum addNewAssetWithImage:[self captureView:layoutController.view] toAlbum:[CustomAlbum getMyAlbumWithName:strAlbumName] onSuccess:^(NSString *ImageId) {
            recentImg = ImageId;
            NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
            NSData *data3 = [NSKeyedArchiver archivedDataWithRootObject:[self captureView:layoutController.view]];
            [currentDefaults setObject:data3 forKey:@"layoutphoto"];
            
            
            
        } onError:^(NSError *error) {
            NSLog(@"probelm in saving image");
        }];
        
        
    }
    
    
    
    
    //5x7 Layouts//
    
    
    
    
    
    if ([string61 isEqualToString:@"8"])
    {
        LayOutOne1* layoutController = [[LayOutOne1 alloc] initWithNibName:@"LayOutOne1" bundle:nil];
        
        
        if (bgimage == nil)
        {
            [layoutController setEventDate:eventdate];
            [layoutController setEventName:eventname];
            layoutController.view.backgroundColor = [UIColor colorWithString:background];
            layoutController.lblEventNameRight.textColor = [UIColor colorWithString:font];
            layoutController.lblEventNameLeft.textColor = [UIColor colorWithString:font];
            layoutController.lblEventDateRight.textColor = [UIColor colorWithString:font];
            layoutController.lblEventDateLeft.textColor = [UIColor colorWithString:font];
            
            
            layoutController.lblEventNameRight.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventNameLeft.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventDateRight.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventDateLeft.adjustsFontSizeToFitWidth = YES;
        }
        
        else
        {
            layoutController.lblEventNameRight.hidden = YES;
            layoutController.lblEventNameLeft.hidden = YES;
            layoutController.lblEventDateRight.hidden = YES;
            layoutController.lblEventDateLeft.hidden = YES;
            [layoutController setIsOverlay:overlay];
            [layoutController setImage:[UIImage imageWithData:bgimage]];
        }
        
        
        
        int tagView = 1;
        for (int i = 1; i <= [numberofphotos intValue]; i++)
        {
            NSLog(@"mPhotoCount: %ld", (long)[numberofphotos intValue]);
            
            UIImageView *imageLeft = (UIImageView *) [layoutController.view viewWithTag:tagView];
            UIImageView *imageRight = (UIImageView *) [layoutController.view viewWithTag:(tagView + 1)];
            
            UIImage *img = [UIImage imageWithData:[_arrSlidshowImg objectAtIndex:i-1]];
            [imageLeft setImage:img];
            [imageRight setImage:img];
            
            tagView += 2;
        }
        
    
            
            NSString * share4 = [self.record valueForKey:@"switchSharingLayout"];
            
            if ([share4  isEqual: @"YES"])
            {
                [self getPath];
                NSString *prefixString = @"Image";
                
                NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
                NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
                imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
                UIImage *image= [self captureView:layoutController.view];
                data = UIImageJPEGRepresentation(image,0.5);
                [data writeToFile:imagePath atomically:YES];
                
                NSLog(@"filepath Make Layout =%@",imagePath);
                refURL = [NSURL fileURLWithPath:imagePath];
                if ([[_appDelegate.mcManager.session connectedPeers] count]) {
                    NSLog(@"SEND LAYOUT");
                    [self sendResource];
                }
                
                
           
            else
            {
                
                
                NSLog (@"Sharing Layout IS OFF");
            }
            
            
        }
        
        else
        {
            if (![[DBSession sharedSession] isLinked])
            {
                
                
                
            }
            
            else
            {
                NSString *prefixString = @"Image";
                
                NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
                NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
                
                
                [self getPath];
                imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
                UIImage *image= [self captureView:layoutController.view];
                data = UIImageJPEGRepresentation(image,0.5);
                [data writeToFile:imagePath atomically:YES];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                                        stringForKey:@"AlbumName"];
                NSArray* words = [savedValue componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                NSString* nospacestring = [words componentsJoinedByString:@""];
                
                NSString *destDir = [NSString stringWithFormat: @"/%@_layout/", nospacestring];
                restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
                restClient.delegate = self;
                [restClient uploadFile:[NSString stringWithFormat:@"%@.jpg",uniqueFileName] toPath:destDir withParentRev:nil fromPath:imagePath];
                
                
            }
            
        }
        NSString *valueToSave = [[NSUserDefaults standardUserDefaults]
                                 stringForKey:@"printport"];
        NSString *valueToSave1 = [[NSUserDefaults standardUserDefaults]
                                  stringForKey:@"printip"];
        
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
                NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
                
                NSURL * url = [NSURL URLWithString:[NSString stringWithFormat: @"http://%@:%@", valueToSave1,valueToSave]];
                NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
                NSData *imageData = UIImageJPEGRepresentation([self captureView:layoutController.view], 0.5);
                
                [urlRequest setHTTPMethod:@"POST"];
                [urlRequest setHTTPBody:imageData];
                
                NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest
                                                                   completionHandler:^(NSData *data5, NSURLResponse *response, NSError *error) {
                                                                       NSLog(@"Response:%@ %@\n", response, error);
                                                                       if(error == nil)
                                                                       {
                                                                           NSString * text = [[NSString alloc] initWithData: data5 encoding: NSUTF8StringEncoding];
                                                                           NSLog(@"Data = %@",text);
                                                                       }
                                                                       
                                                                   }];
                [dataTask resume];
            });
            
        });
        [CustomAlbum addNewAssetWithImage:[self captureView:layoutController.view] toAlbum:[CustomAlbum getMyAlbumWithName:strAlbumName] onSuccess:^(NSString *ImageId) {
            recentImg = ImageId;
            NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
            NSData *data3 = [NSKeyedArchiver archivedDataWithRootObject:[self captureView:layoutController.view]];
            [currentDefaults setObject:data3 forKey:@"layoutphoto"];
            
            
            
        } onError:^(NSError *error) {
            NSLog(@"probelm in saving image");
        }];
        
        
    }
    
    
    
    if ([string61 isEqualToString:@"9"])
    {
        LayOutTwo1* layoutController = [[LayOutTwo1 alloc] initWithNibName:@"LayOutTwo1" bundle:nil];
        
        
        if (bgimage == nil)
        {
            [layoutController setEventDate:eventdate];
            [layoutController setEventName:eventname];
            layoutController.view.backgroundColor = [UIColor colorWithString:background];
            layoutController.lblEventNameRight.textColor = [UIColor colorWithString:font];
            layoutController.lblEventNameLeft.textColor = [UIColor colorWithString:font];
            //            layoutController.lblEventDateRight.textColor = [UIColor colorWithString:font];
            //            layoutController.lblEventDateLeft.textColor = [UIColor colorWithString:font];
            
            
            layoutController.lblEventNameRight.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventNameLeft.adjustsFontSizeToFitWidth = YES;
            //            layoutController.lblEventDateRight.adjustsFontSizeToFitWidth = YES;
            //            layoutController.lblEventDateLeft.adjustsFontSizeToFitWidth = YES;
        }
        
        else
        {
            layoutController.lblEventNameRight.hidden = YES;
            layoutController.lblEventNameLeft.hidden = YES;
            //            layoutController.lblEventDateRight.hidden = YES;
            //            layoutController.lblEventDateLeft.hidden = YES;
            [layoutController setIsOverlay:overlay];
            [layoutController setImage:[UIImage imageWithData:bgimage]];
        }
        
        
        
        
        int tagView = 1;
        for (int i = 1; i <= [numberofphotos intValue]; i++)
        {
            NSLog(@"mPhotoCount: %ld", (long)[numberofphotos intValue]);
            
            UIImageView *imageLeft = (UIImageView *) [layoutController.view viewWithTag:tagView];
            UIImageView *imageRight = (UIImageView *) [layoutController.view viewWithTag:(tagView + 1)];
            
            UIImage *img = [UIImage imageWithData:[_arrSlidshowImg objectAtIndex:i-1]];
            [imageLeft setImage:img];
            [imageRight setImage:img];
            
            tagView += 2;
        }
        
       
            NSString * share4 = [self.record valueForKey:@"switchSharingLayout"];
            
            if ([share4  isEqual: @"YES"])
            {
                [self getPath];
                NSString *prefixString = @"Image";
                
                NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
                NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
                imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
                UIImage *image= [self captureView:layoutController.view];
                data = UIImageJPEGRepresentation(image,0.5);
                [data writeToFile:imagePath atomically:YES];
                
                NSLog(@"filepath Make Layout =%@",imagePath);
                refURL = [NSURL fileURLWithPath:imagePath];
                if ([[_appDelegate.mcManager.session connectedPeers] count]) {
                    NSLog(@"SEND LAYOUT");
                    [self sendResource];
                }
                
                
         
            
            else
            {
                
                
                NSLog (@"Sharing Layout IS OFF");
            }
            
            
        }

        
        else
        {
            if (![[DBSession sharedSession] isLinked])
            {
                
                
                
            }
            
            else
            {
                NSString *prefixString = @"Image";
                
                NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
                NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
                
                
                [self getPath];
                imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
                UIImage *image= [self captureView:layoutController.view];
                data = UIImageJPEGRepresentation(image,0.5);
                [data writeToFile:imagePath atomically:YES];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                                        stringForKey:@"AlbumName"];
                NSArray* words = [savedValue componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                NSString* nospacestring = [words componentsJoinedByString:@""];
                
                NSString *destDir = [NSString stringWithFormat: @"/%@_layout/", nospacestring];
                restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
                restClient.delegate = self;
                [restClient uploadFile:[NSString stringWithFormat:@"%@.jpg",uniqueFileName] toPath:destDir withParentRev:nil fromPath:imagePath];
                
                
            }
            
        }
        NSString *valueToSave = [[NSUserDefaults standardUserDefaults]
                                 stringForKey:@"printport"];
        NSString *valueToSave1 = [[NSUserDefaults standardUserDefaults]
                                  stringForKey:@"printip"];
        
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
                NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
                
                NSURL * url = [NSURL URLWithString:[NSString stringWithFormat: @"http://%@:%@", valueToSave1,valueToSave]];
                NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
                NSData *imageData = UIImageJPEGRepresentation([self captureView:layoutController.view], 0.5);
                
                [urlRequest setHTTPMethod:@"POST"];
                [urlRequest setHTTPBody:imageData];
                
                NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest
                                                                   completionHandler:^(NSData *data5, NSURLResponse *response, NSError *error) {
                                                                       NSLog(@"Response:%@ %@\n", response, error);
                                                                       if(error == nil)
                                                                       {
                                                                           NSString * text = [[NSString alloc] initWithData: data5 encoding: NSUTF8StringEncoding];
                                                                           NSLog(@"Data = %@",text);
                                                                       }
                                                                       
                                                                   }];
                [dataTask resume];
            });
            
        });
        [CustomAlbum addNewAssetWithImage:[self captureView:layoutController.view] toAlbum:[CustomAlbum getMyAlbumWithName:strAlbumName] onSuccess:^(NSString *ImageId) {
            recentImg = ImageId;
            NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
            NSData *data3 = [NSKeyedArchiver archivedDataWithRootObject:[self captureView:layoutController.view]];
            [currentDefaults setObject:data3 forKey:@"layoutphoto"];
            
            
            
        } onError:^(NSError *error) {
            NSLog(@"probelm in saving image");
        }];
        
        
    }
    
    
    if ([string61 isEqualToString:@"10"])
    {
        LayOutThree1* layoutController = [[LayOutThree1 alloc] initWithNibName:@"LayOutThree1" bundle:nil];
        
        
        if (bgimage == nil)
        {
            [layoutController setEventDate:eventdate];
            [layoutController setEventName:eventname];
            layoutController.view.backgroundColor = [UIColor colorWithString:background];
            // layoutController.lblEventNameRight.textColor = [UIColor colorWithString:font];
            layoutController.lblEventNameLeft.textColor = [UIColor colorWithString:font];
            //            layoutController.lblEventDateRight.textColor = [UIColor colorWithString:font];
            layoutController.lblEventDateLeft.textColor = [UIColor colorWithString:font];
            
            
            //   layoutController.lblEventNameRight.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventNameLeft.adjustsFontSizeToFitWidth = YES;
            //            layoutController.lblEventDateRight.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventDateLeft.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventNameLeft.transform = CGAffineTransformMakeRotation(3.14 / 2);
            layoutController.lblEventDateLeft.transform = CGAffineTransformMakeRotation(3.14 / 2);
        }
        
        else
        {
            //   layoutController.lblEventNameRight.hidden = YES;
            layoutController.lblEventNameLeft.hidden = YES;
            //            layoutController.lblEventDateRight.hidden = YES;
            layoutController.lblEventDateLeft.hidden = YES;
            [layoutController setIsOverlay:overlay];
            [layoutController setImage:[UIImage imageWithData:bgimage]];
        }
        
        
        
        for (int i = 0; i <= [numberofphotos intValue]; i++) {
            UIImage *img = [UIImage imageWithData:[_arrSlidshowImg objectAtIndex:i % 4]];
            UIImageView *imgView1 = (UIImageView *) [layoutController.view viewWithTag:i + 1];
            [imgView1 setImage:img];
            
            if ([overlay isEqualToString:@"NO"])
            {
                imgView1.layer.borderWidth = 4;
                imgView1.layer.borderColor = [UIColor colorWithString:font].CGColor;
            }
            
            
        }
        
        
        
     
            
            NSString * share4 = [self.record valueForKey:@"switchSharingLayout"];
            
            if ([share4  isEqual: @"YES"])
            {
                [self getPath];
                NSString *prefixString = @"Image";
                
                NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
                NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
                imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
                UIImage *image= [self captureView:layoutController.view];
                data = UIImageJPEGRepresentation(image,0.5);
                [data writeToFile:imagePath atomically:YES];
                
                NSLog(@"filepath Make Layout =%@",imagePath);
                refURL = [NSURL fileURLWithPath:imagePath];
                if ([[_appDelegate.mcManager.session connectedPeers] count]) {
                    NSLog(@"SEND LAYOUT");
                    [self sendResource];
                }
                
                
     
            
            else
            {
                
                
                NSLog (@"Sharing Layout IS OFF");
            }
            
            
        }

        else
        {
            if (![[DBSession sharedSession] isLinked])
            {
                
                
                
            }
            
            else
            {
                NSString *prefixString = @"Image";
                
                NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
                NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
                
                
                [self getPath];
                imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
                UIImage *image= [self captureView:layoutController.view];
                data = UIImageJPEGRepresentation(image,0.5);
                [data writeToFile:imagePath atomically:YES];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                                        stringForKey:@"AlbumName"];
                NSArray* words = [savedValue componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                NSString* nospacestring = [words componentsJoinedByString:@""];
                
                NSString *destDir = [NSString stringWithFormat: @"/%@_layout/", nospacestring];
                restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
                restClient.delegate = self;
                [restClient uploadFile:[NSString stringWithFormat:@"%@.jpg",uniqueFileName] toPath:destDir withParentRev:nil fromPath:imagePath];
                
                
            }
            
        }
        
        NSString *valueToSave = [[NSUserDefaults standardUserDefaults]
                                 stringForKey:@"printport"];
        NSString *valueToSave1 = [[NSUserDefaults standardUserDefaults]
                                  stringForKey:@"printip"];
        
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
                NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
                
                NSURL * url = [NSURL URLWithString:[NSString stringWithFormat: @"http://%@:%@", valueToSave1,valueToSave]];
                NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
                NSData *imageData = UIImageJPEGRepresentation([self captureView:layoutController.view], 0.5);
                
                [urlRequest setHTTPMethod:@"POST"];
                [urlRequest setHTTPBody:imageData];
                
                NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest
                                                                   completionHandler:^(NSData *data5, NSURLResponse *response, NSError *error) {
                                                                       NSLog(@"Response:%@ %@\n", response, error);
                                                                       if(error == nil)
                                                                       {
                                                                           NSString * text = [[NSString alloc] initWithData: data5 encoding: NSUTF8StringEncoding];
                                                                           NSLog(@"Data = %@",text);
                                                                       }
                                                                       
                                                                   }];
                [dataTask resume];
            });
            
        });
        [CustomAlbum addNewAssetWithImage:[self captureView:layoutController.view] toAlbum:[CustomAlbum getMyAlbumWithName:strAlbumName] onSuccess:^(NSString *ImageId) {
            recentImg = ImageId;
            NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
            NSData *data3 = [NSKeyedArchiver archivedDataWithRootObject:[self captureView:layoutController.view]];
            [currentDefaults setObject:data3 forKey:@"layoutphoto"];
            
            
            
        } onError:^(NSError *error) {
            NSLog(@"probelm in saving image");
        }];
        
        
    }
    
    
    
    if ([string61 isEqualToString:@"11"])
    {
        LayOutFour1* layoutController = [[LayOutFour1 alloc] initWithNibName:@"LayOutFour1" bundle:nil];
        
        
        if (bgimage == nil)
        {
            [layoutController setEventDate:eventdate];
            [layoutController setEventName:eventname];
            layoutController.view.backgroundColor = [UIColor colorWithString:background];
            // layoutController.lblEventNameRight.textColor = [UIColor colorWithString:font];
            layoutController.lblEventNameLeft.textColor = [UIColor colorWithString:font];
            //            layoutController.lblEventDateRight.textColor = [UIColor colorWithString:font];
            layoutController.lblEventDateLeft.textColor = [UIColor colorWithString:font];
            
            
            //   layoutController.lblEventNameRight.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventNameLeft.adjustsFontSizeToFitWidth = YES;
            //            layoutController.lblEventDateRight.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventDateLeft.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventNameLeft.transform = CGAffineTransformMakeRotation(3.14 / 2);
            layoutController.lblEventDateLeft.transform = CGAffineTransformMakeRotation(3.14 / 2);
        }
        
        else
        {
            //   layoutController.lblEventNameRight.hidden = YES;
            layoutController.lblEventNameLeft.hidden = YES;
            //            layoutController.lblEventDateRight.hidden = YES;
            layoutController.lblEventDateLeft.hidden = YES;
            [layoutController setIsOverlay:overlay];
            [layoutController setImage:[UIImage imageWithData:bgimage]];
        }
        
        
        
        for (int i = 0; i <= [numberofphotos intValue]; i++) {
            UIImage *img = [UIImage imageWithData:[_arrSlidshowImg objectAtIndex:i % [numberofphotos intValue]]];
            UIImageView *imgView1 = (UIImageView *) [layoutController.view viewWithTag:i + 1];
            [imgView1 setImage:img];
            
            if ([overlay isEqualToString:@"NO"])
            {
                imgView1.layer.borderWidth = 4;
                imgView1.layer.borderColor = [UIColor colorWithString:font].CGColor;
            }
            
            
        }
        
        
        
        
            NSString * share4 = [self.record valueForKey:@"switchSharingLayout"];
            
            if ([share4  isEqual: @"YES"])
            {
                [self getPath];
                NSString *prefixString = @"Image";
                
                NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
                NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
                imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
                UIImage *image= [self captureView:layoutController.view];
                data = UIImageJPEGRepresentation(image,0.5);
                [data writeToFile:imagePath atomically:YES];
                
                NSLog(@"filepath Make Layout =%@",imagePath);
                refURL = [NSURL fileURLWithPath:imagePath];
                if ([[_appDelegate.mcManager.session connectedPeers] count]) {
                    NSLog(@"SEND LAYOUT");
                    [self sendResource];
                }
                
          
            
            else
            {
                
                
                NSLog (@"Sharing Layout IS OFF");
            }
            
            
        }
        
        else
        {
            if (![[DBSession sharedSession] isLinked])
            {
                
                
                
            }
            
            else
            {
                NSString *prefixString = @"Image";
                
                NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
                NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
                
                
                [self getPath];
                imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
                UIImage *image= [self captureView:layoutController.view];
                data = UIImageJPEGRepresentation(image,0.5);
                [data writeToFile:imagePath atomically:YES];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                                        stringForKey:@"AlbumName"];
                NSArray* words = [savedValue componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                NSString* nospacestring = [words componentsJoinedByString:@""];
                
                NSString *destDir = [NSString stringWithFormat: @"/%@_layout/", nospacestring];
                restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
                restClient.delegate = self;
                [restClient uploadFile:[NSString stringWithFormat:@"%@.jpg",uniqueFileName] toPath:destDir withParentRev:nil fromPath:imagePath];
                
                
            }
            
        }
        NSString *valueToSave = [[NSUserDefaults standardUserDefaults]
                                 stringForKey:@"printport"];
        NSString *valueToSave1 = [[NSUserDefaults standardUserDefaults]
                                  stringForKey:@"printip"];
        
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
                NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
                
                NSURL * url = [NSURL URLWithString:[NSString stringWithFormat: @"http://%@:%@", valueToSave1,valueToSave]];
                NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
                NSData *imageData = UIImageJPEGRepresentation([self captureView:layoutController.view], 0.5);
                
                [urlRequest setHTTPMethod:@"POST"];
                [urlRequest setHTTPBody:imageData];
                
                NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest
                                                                   completionHandler:^(NSData *data5, NSURLResponse *response, NSError *error) {
                                                                       NSLog(@"Response:%@ %@\n", response, error);
                                                                       if(error == nil)
                                                                       {
                                                                           NSString * text = [[NSString alloc] initWithData: data5 encoding: NSUTF8StringEncoding];
                                                                           NSLog(@"Data = %@",text);
                                                                       }
                                                                       
                                                                   }];
                [dataTask resume];
            });
            
        });
        [CustomAlbum addNewAssetWithImage:[self captureView:layoutController.view] toAlbum:[CustomAlbum getMyAlbumWithName:strAlbumName] onSuccess:^(NSString *ImageId) {
            recentImg = ImageId;
            NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
            NSData *data3 = [NSKeyedArchiver archivedDataWithRootObject:[self captureView:layoutController.view]];
            [currentDefaults setObject:data3 forKey:@"layoutphoto"];
            
            
            
        } onError:^(NSError *error) {
            NSLog(@"probelm in saving image");
        }];
        
        
    }
    
    
    
    
    
    if ([string61 isEqualToString:@"12"])
    {
        LayOutFive1* layoutController = [[LayOutFive1 alloc] initWithNibName:@"LayOutFive1" bundle:nil];
        
        
        if (bgimage == nil)
        {
            [layoutController setEventDate:eventdate];
            [layoutController setEventName:eventname];
            layoutController.view.backgroundColor = [UIColor colorWithString:background];
            // layoutController.lblEventNameRight.textColor = [UIColor colorWithString:font];
            layoutController.lblEventNameLeft.textColor = [UIColor colorWithString:font];
            //            layoutController.lblEventDateRight.textColor = [UIColor colorWithString:font];
            layoutController.lblEventDateLeft.textColor = [UIColor colorWithString:font];
            
            
            //   layoutController.lblEventNameRight.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventNameLeft.adjustsFontSizeToFitWidth = YES;
            //            layoutController.lblEventDateRight.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventDateLeft.adjustsFontSizeToFitWidth = YES;
            
        }
        
        else
        {
            //   layoutController.lblEventNameRight.hidden = YES;
            layoutController.lblEventNameLeft.hidden = YES;
            //            layoutController.lblEventDateRight.hidden = YES;
            layoutController.lblEventDateLeft.hidden = YES;
            [layoutController setIsOverlay:overlay];
            [layoutController setImage:[UIImage imageWithData:bgimage]];
        }
        
        
        
        for (int i = 0; i <= [numberofphotos intValue]; i++) {
            UIImage *img = [UIImage imageWithData:[_arrSlidshowImg objectAtIndex:i % [numberofphotos intValue]]];
            UIImageView *imgView1 = (UIImageView *) [layoutController.view viewWithTag:i + 1];
            [imgView1 setImage:img];
            
            if ([overlay isEqualToString:@"NO"])
            {
                imgView1.layer.borderWidth = 4;
                imgView1.layer.borderColor = [UIColor colorWithString:font].CGColor;
            }
            
            
        }
        
        
        
        
     
            
            NSString * share4 = [self.record valueForKey:@"switchSharingLayout"];
            
            if ([share4  isEqual: @"YES"])
            {
                [self getPath];
                NSString *prefixString = @"Image";
                
                NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
                NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
                imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
                UIImage *image= [self captureView:layoutController.view];
                data = UIImageJPEGRepresentation(image,0.5);
                [data writeToFile:imagePath atomically:YES];
                
                NSLog(@"filepath Make Layout =%@",imagePath);
                refURL = [NSURL fileURLWithPath:imagePath];
                if ([[_appDelegate.mcManager.session connectedPeers] count]) {
                    NSLog(@"SEND LAYOUT");
                    [self sendResource];
                }
                
                
        
            
            else
            {
                
                
                NSLog (@"Sharing Layout IS OFF");
            }
            
            
        }

        
        else
        {
            if (![[DBSession sharedSession] isLinked])
            {
                
                
                
            }
            
            else
            {
                NSString *prefixString = @"Image";
                
                NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
                NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
                
                
                [self getPath];
                imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
                UIImage *image= [self captureView:layoutController.view];
                data = UIImageJPEGRepresentation(image,0.5);
                [data writeToFile:imagePath atomically:YES];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                                        stringForKey:@"AlbumName"];
                NSArray* words = [savedValue componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                NSString* nospacestring = [words componentsJoinedByString:@""];
                
                NSString *destDir = [NSString stringWithFormat: @"/%@_layout/", nospacestring];
                restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
                restClient.delegate = self;
                [restClient uploadFile:[NSString stringWithFormat:@"%@.jpg",uniqueFileName] toPath:destDir withParentRev:nil fromPath:imagePath];
                
                
            }
            
        }
        
        NSString *valueToSave = [[NSUserDefaults standardUserDefaults]
                                 stringForKey:@"printport"];
        NSString *valueToSave1 = [[NSUserDefaults standardUserDefaults]
                                  stringForKey:@"printip"];
        
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
                NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
                
                NSURL * url = [NSURL URLWithString:[NSString stringWithFormat: @"http://%@:%@", valueToSave1,valueToSave]];
                NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
                NSData *imageData = UIImageJPEGRepresentation([self captureView:layoutController.view], 0.5);
                
                [urlRequest setHTTPMethod:@"POST"];
                [urlRequest setHTTPBody:imageData];
                
                NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest
                                                                   completionHandler:^(NSData *data5, NSURLResponse *response, NSError *error) {
                                                                       NSLog(@"Response:%@ %@\n", response, error);
                                                                       if(error == nil)
                                                                       {
                                                                           NSString * text = [[NSString alloc] initWithData: data5 encoding: NSUTF8StringEncoding];
                                                                           NSLog(@"Data = %@",text);
                                                                       }
                                                                       
                                                                   }];
                [dataTask resume];
            });
            
        });
        [CustomAlbum addNewAssetWithImage:[self captureView:layoutController.view] toAlbum:[CustomAlbum getMyAlbumWithName:strAlbumName] onSuccess:^(NSString *ImageId) {
            recentImg = ImageId;
            NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
            NSData *data3 = [NSKeyedArchiver archivedDataWithRootObject:[self captureView:layoutController.view]];
            [currentDefaults setObject:data3 forKey:@"layoutphoto"];
            
            
            
        } onError:^(NSError *error) {
            NSLog(@"probelm in saving image");
        }];
        
        
    }
    
    
    
    
    
    
    
    if ([string61 isEqualToString:@"13"])
    {
        LayOutSix1* layoutController = [[LayOutSix1 alloc] initWithNibName:@"LayOutSix1" bundle:nil];
        
        
        if (bgimage == nil)
        {
            [layoutController setEventDate:eventdate];
            [layoutController setEventName:eventname];
            layoutController.view.backgroundColor = [UIColor colorWithString:background];
            // layoutController.lblEventNameRight.textColor = [UIColor colorWithString:font];
            layoutController.lblEventNameLeft.textColor = [UIColor colorWithString:font];
            //            layoutController.lblEventDateRight.textColor = [UIColor colorWithString:font];
            layoutController.lblEventDateLeft.textColor = [UIColor colorWithString:font];
            
            
            //   layoutController.lblEventNameRight.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventNameLeft.adjustsFontSizeToFitWidth = YES;
            //            layoutController.lblEventDateRight.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventDateLeft.adjustsFontSizeToFitWidth = YES;
            
        }
        
        else
        {
            //   layoutController.lblEventNameRight.hidden = YES;
            layoutController.lblEventNameLeft.hidden = YES;
            //            layoutController.lblEventDateRight.hidden = YES;
            layoutController.lblEventDateLeft.hidden = YES;
            [layoutController setIsOverlay:overlay];
            [layoutController setImage:[UIImage imageWithData:bgimage]];
        }
        
        
        
        for (int i = 0; i <= [numberofphotos intValue]; i++) {
            UIImage *img = [UIImage imageWithData:[_arrSlidshowImg objectAtIndex:i % [numberofphotos intValue]]];
            UIImageView *imgView1 = (UIImageView *) [layoutController.view viewWithTag:i + 1];
            [imgView1 setImage:img];
            
            if ([overlay isEqualToString:@"NO"])
            {
                imgView1.layer.borderWidth = 4;
                imgView1.layer.borderColor = [UIColor colorWithString:font].CGColor;
            }
            
            
        }
        
        
        
            NSString * share4 = [self.record valueForKey:@"switchSharingLayout"];
            
            if ([share4  isEqual: @"YES"])
            {
                [self getPath];
                NSString *prefixString = @"Image";
                
                NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
                NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
                imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
                UIImage *image= [self captureView:layoutController.view];
                data = UIImageJPEGRepresentation(image,0.5);
                [data writeToFile:imagePath atomically:YES];
                
                NSLog(@"filepath Make Layout =%@",imagePath);
                refURL = [NSURL fileURLWithPath:imagePath];
                if ([[_appDelegate.mcManager.session connectedPeers] count]) {
                    NSLog(@"SEND LAYOUT");
                    [self sendResource];
                }
                
          
            else
            {
                
                
                NSLog (@"Sharing Layout IS OFF");
            }
            
            
        }
        
        else
        {
            if (![[DBSession sharedSession] isLinked])
            {
                
                
                
            }
            
            else
            {
                NSString *prefixString = @"Image";
                
                NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
                NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
                
                
                [self getPath];
                imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
                UIImage *image= [self captureView:layoutController.view];
                data = UIImageJPEGRepresentation(image,0.5);
                [data writeToFile:imagePath atomically:YES];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                                        stringForKey:@"AlbumName"];
                NSArray* words = [savedValue componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                NSString* nospacestring = [words componentsJoinedByString:@""];
                
                NSString *destDir = [NSString stringWithFormat: @"/%@_layout/", nospacestring];
                restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
                restClient.delegate = self;
                [restClient uploadFile:[NSString stringWithFormat:@"%@.jpg",uniqueFileName] toPath:destDir withParentRev:nil fromPath:imagePath];
                
                
            }
            
        }
        NSString *valueToSave = [[NSUserDefaults standardUserDefaults]
                                 stringForKey:@"printport"];
        NSString *valueToSave1 = [[NSUserDefaults standardUserDefaults]
                                  stringForKey:@"printip"];
        
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
                NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
                
                NSURL * url = [NSURL URLWithString:[NSString stringWithFormat: @"http://%@:%@", valueToSave1,valueToSave]];
                NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
                NSData *imageData = UIImageJPEGRepresentation([self captureView:layoutController.view], 0.5);
                
                [urlRequest setHTTPMethod:@"POST"];
                [urlRequest setHTTPBody:imageData];
                
                NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest
                                                                   completionHandler:^(NSData *data5, NSURLResponse *response, NSError *error) {
                                                                       NSLog(@"Response:%@ %@\n", response, error);
                                                                       if(error == nil)
                                                                       {
                                                                           NSString * text = [[NSString alloc] initWithData: data5 encoding: NSUTF8StringEncoding];
                                                                           NSLog(@"Data = %@",text);
                                                                       }
                                                                       
                                                                   }];
                [dataTask resume];
            });
            
        });
        
        [CustomAlbum addNewAssetWithImage:[self captureView:layoutController.view] toAlbum:[CustomAlbum getMyAlbumWithName:strAlbumName] onSuccess:^(NSString *ImageId) {
            recentImg = ImageId;
            NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
            NSData *data3 = [NSKeyedArchiver archivedDataWithRootObject:[self captureView:layoutController.view]];
            [currentDefaults setObject:data3 forKey:@"layoutphoto"];
            
            
            
        } onError:^(NSError *error) {
            NSLog(@"probelm in saving image");
        }];
        
        
    }
    
    
    
    
    if ([string61 isEqualToString:@"14"])
    {
        LayOutSeven1* layoutController = [[LayOutSeven1 alloc] initWithNibName:@"LayOutSeven1" bundle:nil];
        
        
        if (bgimage == nil)
        {
            [layoutController setEventDate:eventdate];
            [layoutController setEventName:eventname];
            layoutController.view.backgroundColor = [UIColor colorWithString:background];
            // layoutController.lblEventNameRight.textColor = [UIColor colorWithString:font];
            layoutController.lblEventNameLeft.textColor = [UIColor colorWithString:font];
            //            layoutController.lblEventDateRight.textColor = [UIColor colorWithString:font];
            layoutController.lblEventDateLeft.textColor = [UIColor colorWithString:font];
            
            
            //   layoutController.lblEventNameRight.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventNameLeft.adjustsFontSizeToFitWidth = YES;
            //            layoutController.lblEventDateRight.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventDateLeft.adjustsFontSizeToFitWidth = YES;
            
            layoutController.lblEventDateLeft.transform = CGAffineTransformMakeRotation(3.14 / 2);
            layoutController.lblEventNameLeft.transform = CGAffineTransformMakeRotation(3.14 / 2);
            
        }
        
        else
        {
            //   layoutController.lblEventNameRight.hidden = YES;
            layoutController.lblEventNameLeft.hidden = YES;
            //            layoutController.lblEventDateRight.hidden = YES;
            layoutController.lblEventDateLeft.hidden = YES;
            [layoutController setIsOverlay:overlay];
            [layoutController setImage:[UIImage imageWithData:bgimage]];
        }
        
        
        
        for (int i = 0; i <= [numberofphotos intValue]; i++) {
            UIImage *img = [UIImage imageWithData:[_arrSlidshowImg objectAtIndex:i % [numberofphotos intValue]]];
            UIImageView *imgView1 = (UIImageView *) [layoutController.view viewWithTag:i + 1];
            [imgView1 setImage:img];
            
            if ([overlay isEqualToString:@"NO"])
            {
                imgView1.layer.borderWidth = 4;
                imgView1.layer.borderColor = [UIColor colorWithString:font].CGColor;
                imgView1.transform = CGAffineTransformMakeRotation(3.14 / 2);
            }
            
            
        }
        
        
        
        
            NSString * share4 = [self.record valueForKey:@"switchSharingLayout"];
            
            if ([share4  isEqual: @"YES"])
            {
                [self getPath];
                NSString *prefixString = @"Image";
                
                NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
                NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
                imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
                UIImage *image= [self captureView:layoutController.view];
                data = UIImageJPEGRepresentation(image,0.5);
                [data writeToFile:imagePath atomically:YES];
                
                NSLog(@"filepath Make Layout =%@",imagePath);
                refURL = [NSURL fileURLWithPath:imagePath];
                if ([[_appDelegate.mcManager.session connectedPeers] count]) {
                    NSLog(@"SEND LAYOUT");
                    [self sendResource];
                }
                
                
        
            
            else
            {
                
                
                NSLog (@"Sharing Layout IS OFF");
            }
            
            
        }
        
        else
        {
            if (![[DBSession sharedSession] isLinked])
            {
                
                
                
            }
            
            else
            {
                NSString *prefixString = @"Image";
                
                NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
                NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
                
                
                [self getPath];
                imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
                UIImage *image= [self captureView:layoutController.view];
                data = UIImageJPEGRepresentation(image,0.5);
                [data writeToFile:imagePath atomically:YES];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                                        stringForKey:@"AlbumName"];
                NSArray* words = [savedValue componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                NSString* nospacestring = [words componentsJoinedByString:@""];
                
                NSString *destDir = [NSString stringWithFormat: @"/%@_layout/", nospacestring];
                restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
                restClient.delegate = self;
                [restClient uploadFile:[NSString stringWithFormat:@"%@.jpg",uniqueFileName] toPath:destDir withParentRev:nil fromPath:imagePath];
                
                
            }
            
        }
        NSString *valueToSave = [[NSUserDefaults standardUserDefaults]
                                 stringForKey:@"printport"];
        NSString *valueToSave1 = [[NSUserDefaults standardUserDefaults]
                                  stringForKey:@"printip"];
        
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
                NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
                
                NSURL * url = [NSURL URLWithString:[NSString stringWithFormat: @"http://%@:%@", valueToSave1,valueToSave]];
                NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
                NSData *imageData = UIImageJPEGRepresentation([self captureView:layoutController.view], 0.5);
                
                [urlRequest setHTTPMethod:@"POST"];
                [urlRequest setHTTPBody:imageData];
                
                NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest
                                                                   completionHandler:^(NSData *data5, NSURLResponse *response, NSError *error) {
                                                                       NSLog(@"Response:%@ %@\n", response, error);
                                                                       if(error == nil)
                                                                       {
                                                                           NSString * text = [[NSString alloc] initWithData: data5 encoding: NSUTF8StringEncoding];
                                                                           NSLog(@"Data = %@",text);
                                                                       }
                                                                       
                                                                   }];
                [dataTask resume];
            });
            
        });
        [CustomAlbum addNewAssetWithImage:[self captureView:layoutController.view] toAlbum:[CustomAlbum getMyAlbumWithName:strAlbumName] onSuccess:^(NSString *ImageId) {
            recentImg = ImageId;
            NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
            NSData *data3 = [NSKeyedArchiver archivedDataWithRootObject:[self captureView:layoutController.view]];
            [currentDefaults setObject:data3 forKey:@"layoutphoto"];
            
            
            
        } onError:^(NSError *error) {
            NSLog(@"probelm in saving image");
        }];
        
        
    }
    
    
    
    
    
    if ([string61 isEqualToString:@"15"])
    {
        LayOutEight1* layoutController = [[LayOutEight1 alloc] initWithNibName:@"LayOutEight1" bundle:nil];
        
        
        if (bgimage == nil)
        {
            [layoutController setEventDate:eventdate];
            [layoutController setEventName:eventname];
            layoutController.view.backgroundColor = [UIColor colorWithString:background];
            // layoutController.lblEventNameRight.textColor = [UIColor colorWithString:font];
            layoutController.lblEventNameLeft.textColor = [UIColor colorWithString:font];
            //            layoutController.lblEventDateRight.textColor = [UIColor colorWithString:font];
            layoutController.lblEventDateLeft.textColor = [UIColor colorWithString:font];
            
            
            //   layoutController.lblEventNameRight.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventNameLeft.adjustsFontSizeToFitWidth = YES;
            //            layoutController.lblEventDateRight.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventDateLeft.adjustsFontSizeToFitWidth = YES;
            
            layoutController.lblEventDateLeft.transform = CGAffineTransformMakeRotation(3.14);
            layoutController.lblEventNameLeft.transform = CGAffineTransformMakeRotation(3.14);
            
        }
        
        else
        {
            //   layoutController.lblEventNameRight.hidden = YES;
            layoutController.lblEventNameLeft.hidden = YES;
            //            layoutController.lblEventDateRight.hidden = YES;
            layoutController.lblEventDateLeft.hidden = YES;
            [layoutController setIsOverlay:overlay];
            [layoutController setImage:[UIImage imageWithData:bgimage]];
        }
        
        
        
        for (int i = 0; i <= [numberofphotos intValue]; i++) {
            UIImage *img = [UIImage imageWithData:[_arrSlidshowImg objectAtIndex:i % [numberofphotos intValue]]];
            UIImageView *imgView1 = (UIImageView *) [layoutController.view viewWithTag:i + 1];
            [imgView1 setImage:img];
            
            if ([overlay isEqualToString:@"NO"])
            {
                imgView1.layer.borderWidth = 4;
                imgView1.layer.borderColor = [UIColor colorWithString:font].CGColor;
                imgView1.transform = CGAffineTransformMakeRotation(3.14 / 2);
            }
            
            
        }
        
        
        
            NSString * share4 = [self.record valueForKey:@"switchSharingLayout"];
            
            if ([share4  isEqual: @"YES"])
            {
                [self getPath];
                NSString *prefixString = @"Image";
                
                NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
                NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
                imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
                UIImage *image= [self captureView:layoutController.view];
                data = UIImageJPEGRepresentation(image,0.5);
                [data writeToFile:imagePath atomically:YES];
                
                NSLog(@"filepath Make Layout =%@",imagePath);
                refURL = [NSURL fileURLWithPath:imagePath];
                if ([[_appDelegate.mcManager.session connectedPeers] count]) {
                    NSLog(@"SEND LAYOUT");
                    [self sendResource];
                }
                
                
          
            else
            {
                
                
                NSLog (@"Sharing Layout IS OFF");
            }
            
            
        }
        
        else
        {
            if (![[DBSession sharedSession] isLinked])
            {
                
                
                
            }
            
            else
            {
                NSString *prefixString = @"Image";
                
                NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
                NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
                
                
                [self getPath];
                imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
                UIImage *image= [self captureView:layoutController.view];
                data = UIImageJPEGRepresentation(image,0.5);
                [data writeToFile:imagePath atomically:YES];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                                        stringForKey:@"AlbumName"];
                NSArray* words = [savedValue componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                NSString* nospacestring = [words componentsJoinedByString:@""];
                
                NSString *destDir = [NSString stringWithFormat: @"/%@_layout/", nospacestring];
                restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
                restClient.delegate = self;
                [restClient uploadFile:[NSString stringWithFormat:@"%@.jpg",uniqueFileName] toPath:destDir withParentRev:nil fromPath:imagePath];
                
                
            }
            
        }
        
        NSString *valueToSave = [[NSUserDefaults standardUserDefaults]
                                 stringForKey:@"printport"];
        NSString *valueToSave1 = [[NSUserDefaults standardUserDefaults]
                                  stringForKey:@"printip"];
        
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
                NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
                
                NSURL * url = [NSURL URLWithString:[NSString stringWithFormat: @"http://%@:%@", valueToSave1,valueToSave]];
                NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
                NSData *imageData = UIImageJPEGRepresentation([self captureView:layoutController.view], 0.5);
                
                [urlRequest setHTTPMethod:@"POST"];
                [urlRequest setHTTPBody:imageData];
                
                NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest
                                                                   completionHandler:^(NSData *data5, NSURLResponse *response, NSError *error) {
                                                                       NSLog(@"Response:%@ %@\n", response, error);
                                                                       if(error == nil)
                                                                       {
                                                                           NSString * text = [[NSString alloc] initWithData: data5 encoding: NSUTF8StringEncoding];
                                                                           NSLog(@"Data = %@",text);
                                                                       }
                                                                       
                                                                   }];
                [dataTask resume];
            });
            
        });
        [CustomAlbum addNewAssetWithImage:[self captureView:layoutController.view] toAlbum:[CustomAlbum getMyAlbumWithName:strAlbumName] onSuccess:^(NSString *ImageId) {
            recentImg = ImageId;
            NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
            NSData *data3 = [NSKeyedArchiver archivedDataWithRootObject:[self captureView:layoutController.view]];
            [currentDefaults setObject:data3 forKey:@"layoutphoto"];
            
            
            
        } onError:^(NSError *error) {
            NSLog(@"probelm in saving image");
        }];
        
        
    }
    
    
    
    
    
    return imgLayout;
}

- (void)customLayoutWithType:(NSString*)typeTheme andOverlay:(NSString*)overlay andNumberOfPhoto:(NSString*)numberOfPhoto andAlbumName:(NSString*)albumName
{
    //Get Custom Layout
    [TSPAppDelegate sharedAppDelegate].customLayoutArray = [NSMutableArray array];
    for (TSPEventRepository* eventItem in [TSPAppDelegate sharedAppDelegate].customEvents)
    {
        if ([eventItem.eventID isEqualToString:[self.record valueForKey:@"eventid"]])
        {
            NSMutableArray *rectanglesArray = eventItem.customLayoutArray;
            
            for (int i = 0; i< rectanglesArray.count; i++)
            {
                NSData *rectangleData = [rectanglesArray objectAtIndex:i];
                Rectangle *rectangle = [NSKeyedUnarchiver unarchiveObjectWithData:rectangleData];
                [[TSPAppDelegate sharedAppDelegate].customLayoutArray addObject:rectangle];
            }
            
            [TSPAppDelegate sharedAppDelegate].customLayoutCount = eventItem.customLayoutCount;
            [TSPAppDelegate sharedAppDelegate].isSmallCustomLayout = eventItem.isSmallCustomLayout;
            [TSPAppDelegate sharedAppDelegate].isSmallCustomLayouth = eventItem.isSmallCustomLayoutH;
            [TSPAppDelegate sharedAppDelegate].isLargeCustomLayout = eventItem.isLargeCustomLayout;
            [TSPAppDelegate sharedAppDelegate].isLargeCustomLayouth = eventItem.isLargeCustomLayoutH;
            
            break;
        }
    }
    
    //Redraw custom layout view
    if ([TSPAppDelegate sharedAppDelegate].customLayoutArray.count > 0)
    {
        if ([TSPAppDelegate sharedAppDelegate].isSmallCustomLayout)
        {
            [TSPAppDelegate sharedAppDelegate].customLayoutView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 476, 715)];
            
            //Add background imageView
            UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 476, 715)];
            [backgroundImageView setTag:1];
            [[TSPAppDelegate sharedAppDelegate].customLayoutView addSubview:backgroundImageView];
            
            for (int i = 0; i < [TSPAppDelegate sharedAppDelegate].customLayoutArray.count; i++)
            {
                Rectangle *rectangle = [[TSPAppDelegate sharedAppDelegate].customLayoutArray objectAtIndex:i];
                rectangle.frame = rectangle.frame;
                
                CGPoint centerPoint = rectangle.center;
                
                CGAffineTransform rotate = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(rectangle.rotateValue));
                CGAffineTransform scale = CGAffineTransformMakeScale(rectangle.scaleValue, rectangle.scaleValue);
                CGAffineTransform scaleAndRotate = CGAffineTransformConcat(rotate, scale);
                rectangle.transform = scaleAndRotate;
                
                rectangle.center = centerPoint;
                
                [[TSPAppDelegate sharedAppDelegate].customLayoutView addSubview:rectangle];
            }
            
            //Add foreground imageView
            UIImageView *foregroundImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 476, 715)];
            [foregroundImageView setTag:2];
            [[TSPAppDelegate sharedAppDelegate].customLayoutView addSubview:foregroundImageView];
        }
        else if ([TSPAppDelegate sharedAppDelegate].isLargeCustomLayout)
        {
            [TSPAppDelegate sharedAppDelegate].customLayoutView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 510, 715)];
            
            //Add background imageView
            UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 510, 715)];
            [backgroundImageView setTag:1];
            [[TSPAppDelegate sharedAppDelegate].customLayoutView addSubview:backgroundImageView];
            
            for (int i = 0; i < [TSPAppDelegate sharedAppDelegate].customLayoutArray.count; i++)
            {
                Rectangle *rectangle = [[TSPAppDelegate sharedAppDelegate].customLayoutArray objectAtIndex:i];
                rectangle.frame = rectangle.frame;
                
                CGPoint centerPoint = rectangle.center;
                
                CGAffineTransform rotate = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(rectangle.rotateValue));
                CGAffineTransform scale = CGAffineTransformMakeScale(rectangle.scaleValue, rectangle.scaleValue);
                CGAffineTransform scaleAndRotate = CGAffineTransformConcat(rotate, scale);
                rectangle.transform = scaleAndRotate;
                
                rectangle.center = centerPoint;
                
                [[TSPAppDelegate sharedAppDelegate].customLayoutView addSubview:rectangle];
            }
            
            //Add foreground imageView
            UIImageView *foregroundImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 510, 715)];
            [foregroundImageView setTag:2];
            [[TSPAppDelegate sharedAppDelegate].customLayoutView addSubview:foregroundImageView];
        }
        
        else if ([TSPAppDelegate sharedAppDelegate].isSmallCustomLayouth)
        {
            [TSPAppDelegate sharedAppDelegate].customLayoutView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 715, 476)];
            
            //Add background imageView
            UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 715, 476)];
            [backgroundImageView setTag:1];
            [[TSPAppDelegate sharedAppDelegate].customLayoutView addSubview:backgroundImageView];
            
            for (int i = 0; i < [TSPAppDelegate sharedAppDelegate].customLayoutArray.count; i++)
            {
                Rectangle *rectangle = [[TSPAppDelegate sharedAppDelegate].customLayoutArray objectAtIndex:i];
                rectangle.frame = rectangle.frame;
                
                CGPoint centerPoint = rectangle.center;
                
                CGAffineTransform rotate = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(rectangle.rotateValue));
                CGAffineTransform scale = CGAffineTransformMakeScale(rectangle.scaleValue, rectangle.scaleValue);
                CGAffineTransform scaleAndRotate = CGAffineTransformConcat(rotate, scale);
                rectangle.transform = scaleAndRotate;
                
                rectangle.center = centerPoint;
                
                [[TSPAppDelegate sharedAppDelegate].customLayoutView addSubview:rectangle];
            }
            
            //Add foreground imageView
            UIImageView *foregroundImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 715, 476)];
            [foregroundImageView setTag:2];
            [[TSPAppDelegate sharedAppDelegate].customLayoutView addSubview:foregroundImageView];
        }
        else
        {
            [TSPAppDelegate sharedAppDelegate].customLayoutView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 715, 510)];
            
            //Add background imageView
            UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 715, 510)];
            [backgroundImageView setTag:1];
            [[TSPAppDelegate sharedAppDelegate].customLayoutView addSubview:backgroundImageView];
            
            for (int i = 0; i < [TSPAppDelegate sharedAppDelegate].customLayoutArray.count; i++)
            {
                Rectangle *rectangle = [[TSPAppDelegate sharedAppDelegate].customLayoutArray objectAtIndex:i];
                rectangle.frame = rectangle.frame;
                
                CGPoint centerPoint = rectangle.center;
                
                CGAffineTransform rotate = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(rectangle.rotateValue));
                CGAffineTransform scale = CGAffineTransformMakeScale(rectangle.scaleValue, rectangle.scaleValue);
                CGAffineTransform scaleAndRotate = CGAffineTransformConcat(rotate, scale);
                rectangle.transform = scaleAndRotate;
                
                rectangle.center = centerPoint;
                
                [[TSPAppDelegate sharedAppDelegate].customLayoutView addSubview:rectangle];
            }
            
            //Add foreground imageView
            UIImageView *foregroundImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 715, 510)];
            [foregroundImageView setTag:2];
            [[TSPAppDelegate sharedAppDelegate].customLayoutView addSubview:foregroundImageView];
        }
        
    }
    
    if ([typeTheme isEqualToString:@"CustomTheme"])
    {
        NSData *bgimage = [self.record valueForKey:@"custombackground"];
        
        if ([overlay isEqualToString:@"YES"])
        {
            UIImageView *backgroundImageView = (UIImageView *)[[TSPAppDelegate sharedAppDelegate].customLayoutView viewWithTag:2];
            [backgroundImageView setBackgroundColor:[UIColor clearColor]];
            backgroundImageView.image = [UIImage imageWithData:bgimage];
        }
        else
        {
            UIImageView *backgroundImageView = (UIImageView *)[[TSPAppDelegate sharedAppDelegate].customLayoutView viewWithTag:1];
            
            backgroundImageView.image = [UIImage imageWithData:bgimage];
        }
        
        for (int i = 0; i < [numberOfPhoto intValue]; i++)
        {
            NSLog(@"mPhotoCount: %ld", (long)photocount);
            
            if ([numberOfPhoto intValue] < [TSPAppDelegate sharedAppDelegate].customLayoutArray.count)
            {
                UIImage *img = [UIImage imageWithData:[_arrSlidshowImg objectAtIndex:i]];
                
                Rectangle *rectangle1 = [[TSPAppDelegate sharedAppDelegate].customLayoutArray objectAtIndex:i*2];
                [rectangle1 setImage:img];
                
                Rectangle *rectangle2 = [[TSPAppDelegate sharedAppDelegate].customLayoutArray objectAtIndex:i*2+1];
                [rectangle2 setImage:img];
            }
            else
            {
                UIImage *img = [UIImage imageWithData:[_arrSlidshowImg objectAtIndex:i]];
                
                Rectangle *rectangle = (Rectangle*)[[TSPAppDelegate sharedAppDelegate].customLayoutArray objectAtIndex:i];
                [rectangle setImage:img];
            }
        }
        
        UIImage *captureLayout = [self captureView:[TSPAppDelegate sharedAppDelegate].customLayoutView];
        if ([TSPAppDelegate sharedAppDelegate].isSmallCustomLayout)
        {
            captureLayout = [self imageWithImage:captureLayout scaledToSize:CGSizeMake(1200, 1800)];
        }
        if ([TSPAppDelegate sharedAppDelegate].isLargeCustomLayout)
        {
            captureLayout = [self imageWithImage:captureLayout scaledToSize:CGSizeMake(1500, 2100)];
        }
        if ([TSPAppDelegate sharedAppDelegate].isSmallCustomLayouth)
        {
            captureLayout = [self imageWithImage:captureLayout scaledToSize:CGSizeMake(1800, 1200)];
        }
        if ([TSPAppDelegate sharedAppDelegate].isLargeCustomLayouth)
        {
            captureLayout = [self imageWithImage:captureLayout scaledToSize:CGSizeMake(2100, 1500)];
        }
        
        
        
        NSString * sharing = [self.record valueForKey:@"switchSharingLayout"];
        
        if ([sharing isEqual: @"YES"])
        {
            [self getPath];
            NSString *prefixString = @"Image";
            
            NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
            NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
            imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
            UIImage *image= [self captureView:[TSPAppDelegate sharedAppDelegate].customLayoutView];
            data = UIImagePNGRepresentation(image);
            [data writeToFile:imagePath atomically:YES];
            
            NSLog(@"filepath=%@",imagePath);
            refURL = [NSURL fileURLWithPath:imagePath];
            if ([[_appDelegate.mcManager.session connectedPeers] count]) {
                [self sendResource];
            }
        }
        
        else
        {
            if (![[DBSession sharedSession] isLinked])
            {
                
                
                
            }
            
            else
            {
                NSString *prefixString = @"Image";
                
                NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
                NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
                
                
                [self getPath];
                imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
                UIImage *image= [self captureView:[TSPAppDelegate sharedAppDelegate].customLayoutView];
                data = UIImagePNGRepresentation(image);
                [data writeToFile:imagePath atomically:YES];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                                        stringForKey:@"AlbumName"];
                NSArray* words = [savedValue componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                NSString* nospacestring = [words componentsJoinedByString:@""];
                
                NSString *destDir = [NSString stringWithFormat: @"/%@_layout/", nospacestring];
                restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
                restClient.delegate = self;
                [restClient uploadFile:[NSString stringWithFormat:@"%@.jpg",uniqueFileName] toPath:destDir withParentRev:nil fromPath:imagePath];
                
                
            }
            
        }
        
        NSString *valueToSave = [[NSUserDefaults standardUserDefaults]
                                 stringForKey:@"printport"];
        NSString *valueToSave1 = [[NSUserDefaults standardUserDefaults]
                                  stringForKey:@"printip"];
        
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
                NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
                
                NSURL * url = [NSURL URLWithString:[NSString stringWithFormat: @"http://%@:%@", valueToSave1,valueToSave]];
                NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
                NSData *imageData = UIImageJPEGRepresentation([self captureView:[TSPAppDelegate sharedAppDelegate].customLayoutView] , 0.5);
                
                [urlRequest setHTTPMethod:@"POST"];
                [urlRequest setHTTPBody:imageData];
                
                NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest
                                                                   completionHandler:^(NSData *data5, NSURLResponse *response, NSError *error) {
                                                                       NSLog(@"Response:%@ %@\n", response, error);
                                                                       if(error == nil)
                                                                       {
                                                                           NSString * text = [[NSString alloc] initWithData: data5 encoding: NSUTF8StringEncoding];
                                                                           NSLog(@"Data = %@",text);
                                                                       }
                                                                       
                                                                   }];
                [dataTask resume];
            });
            
        });
        [CustomAlbum addNewAssetWithImage:[self captureView:[TSPAppDelegate sharedAppDelegate].customLayoutView] toAlbum:[CustomAlbum getMyAlbumWithName:albumName] onSuccess:^(NSString *ImageId) {
            recentImg = ImageId;
            NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
            NSData *data3 = [NSKeyedArchiver archivedDataWithRootObject:[self captureView:[TSPAppDelegate sharedAppDelegate].customLayoutView]];
            [currentDefaults setObject:data3 forKey:@"layoutphoto"];
            
            
            
        } onError:^(NSError *error) {
            NSLog(@"probelm in saving image");
        }];
    }

}

//VERTICAL LAYOUTS//

- (UIImage *)makeLayoutImageVertical
{
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"AlbumName"];
    
    NSString *background = [self.record valueForKey:@"printbackgroundcolor"];
    NSString *font = [self.record valueForKey:@"printfontbordercolor"];
    NSString *eventname = [self.record valueForKey:@"eventname"];
    NSString *eventdate = [self.record valueForKey:@"eventdate"];
    
    NSString * string61 = [self.record valueForKey:@"shrTheme"];
    NSLog(@"SENDGRID IS = %@", string61);
    NSString * overlay = [self.record valueForKey:@"switchOverlay"];
    
    NSData *bgimage = [self.record valueForKey:@"photoboothlayoutbackground"];
    printlayoutbackground.image = [UIImage imageWithData:bgimage];
    
    NSString *strAlbumName = [NSString stringWithFormat:@"%@_Layout", savedValue];
    NSString *numberofphotos = [self.record valueForKey:@"numberofphotos"];
    
    
    [self customLayoutWithType:string61 andOverlay:overlay andNumberOfPhoto:numberofphotos andAlbumName:strAlbumName];
    
    //4x6 Layouts
    
    
    if ([string61 isEqualToString:@"0"])
    {
        VerLayOutOne* layoutController = [[VerLayOutOne alloc] initWithNibName:@"VerLayOutOne" bundle:nil];
        
        
        if (bgimage == nil)
        {
            [layoutController setEventDate:eventdate];
            [layoutController setEventName:eventname];
            layoutController.view.backgroundColor = [UIColor colorWithString:background];
            layoutController.lblEventNameRight.textColor = [UIColor colorWithString:font];
            layoutController.lblEventNameLeft.textColor = [UIColor colorWithString:font];
            layoutController.lblEventDateRight.textColor = [UIColor colorWithString:font];
            layoutController.lblEventDateLeft.textColor = [UIColor colorWithString:font];
            
            
            layoutController.lblEventNameRight.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventNameLeft.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventDateRight.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventDateLeft.adjustsFontSizeToFitWidth = YES;
        }
        
        else
        {
            layoutController.lblEventNameRight.hidden = YES;
            layoutController.lblEventNameLeft.hidden = YES;
            layoutController.lblEventDateRight.hidden = YES;
            layoutController.lblEventDateLeft.hidden = YES;
            [layoutController setIsOverlay:overlay];
            [layoutController setImage:[UIImage imageWithData:bgimage]];
        }
        
        
        
        
        int tagView = 1;
        for (int i = 1; i <= [numberofphotos intValue]; i++)
        {
            NSLog(@"mPhotoCount: %ld", (long)[numberofphotos intValue]);
            
            UIImageView *imageLeft = (UIImageView *) [layoutController.view viewWithTag:tagView];
            UIImageView *imageRight = (UIImageView *) [layoutController.view viewWithTag:(tagView + 1)];
            
            UIImage *img = [UIImage imageWithData:[_arrSlidshowImg objectAtIndex:i-1]];
            [imageLeft setImage:img];
            [imageRight setImage:img];
            
            tagView += 2;
        }
        
        NSString * sharing = [self.record valueForKey:@"switchSharingLayout"];
        
        if ([sharing isEqual: @"YES"])
        {
            [self getPath];
            NSString *prefixString = @"Image";
            
            NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
            NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
            imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
            UIImage *image= [self captureView:layoutController.view];
            data = UIImageJPEGRepresentation(image,0.5);
            [data writeToFile:imagePath atomically:YES];
            
            NSLog(@"filepath=%@",imagePath);
            refURL = [NSURL fileURLWithPath:imagePath];
            if ([[_appDelegate.mcManager.session connectedPeers] count]) {
                [self sendResource];
            }
        }
        
        else
        {
            if (![[DBSession sharedSession] isLinked])
            {
                
                
                
            }
            
            else
            {
                NSString *prefixString = @"Image";
                
                NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
                NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
                
                
                [self getPath];
                imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
                UIImage *image= [self captureView:layoutController.view];
                data = UIImageJPEGRepresentation(image,0.5);
                [data writeToFile:imagePath atomically:YES];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                                        stringForKey:@"AlbumName"];
                NSArray* words = [savedValue componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                NSString* nospacestring = [words componentsJoinedByString:@""];
                
                NSString *destDir = [NSString stringWithFormat: @"/%@_layout/", nospacestring];
                restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
                restClient.delegate = self;
                [restClient uploadFile:[NSString stringWithFormat:@"%@.jpg",uniqueFileName] toPath:destDir withParentRev:nil fromPath:imagePath];
                
                
            }
            
        }
        NSString *valueToSave = [[NSUserDefaults standardUserDefaults]
                                 stringForKey:@"printport"];
        NSString *valueToSave1 = [[NSUserDefaults standardUserDefaults]
                                  stringForKey:@"printip"];
        
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
                NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
                
                NSURL * url = [NSURL URLWithString:[NSString stringWithFormat: @"http://%@:%@", valueToSave1,valueToSave]];
                NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
                NSData *imageData = UIImageJPEGRepresentation([self captureView:layoutController.view], 0.5);
                
                [urlRequest setHTTPMethod:@"POST"];
                [urlRequest setHTTPBody:imageData];
                
                NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest
                                                                   completionHandler:^(NSData *data5, NSURLResponse *response, NSError *error) {
                                                                       NSLog(@"Response:%@ %@\n", response, error);
                                                                       if(error == nil)
                                                                       {
                                                                           NSString * text = [[NSString alloc] initWithData: data5 encoding: NSUTF8StringEncoding];
                                                                           NSLog(@"Data = %@",text);
                                                                       }
                                                                       
                                                                   }];
                [dataTask resume];
            });
            
        });
        
        [CustomAlbum addNewAssetWithImage:[self captureView:layoutController.view] toAlbum:[CustomAlbum getMyAlbumWithName:strAlbumName] onSuccess:^(NSString *ImageId) {
            recentImg = ImageId;
            NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
            NSData *data3 = [NSKeyedArchiver archivedDataWithRootObject:[self captureView:layoutController.view]];
            [currentDefaults setObject:data3 forKey:@"layoutphoto"];
            
            
            
        } onError:^(NSError *error) {
            NSLog(@"probelm in saving image");
        }];
        
        
    }
    
    
    
    if ([string61 isEqualToString:@"1"])
    {
        VerLayOutTwo* layoutController = [[VerLayOutTwo alloc] initWithNibName:@"VerLayOutTwo" bundle:nil];
        
        
        if (bgimage == nil)
        {
            [layoutController setEventDate:eventdate];
            [layoutController setEventName:eventname];
            layoutController.view.backgroundColor = [UIColor colorWithString:background];
            layoutController.lblEventNameRight.textColor = [UIColor colorWithString:font];
            layoutController.lblEventNameLeft.textColor = [UIColor colorWithString:font];
            //            layoutController.lblEventDateRight.textColor = [UIColor colorWithString:font];
            //            layoutController.lblEventDateLeft.textColor = [UIColor colorWithString:font];
            
            
            layoutController.lblEventNameRight.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventNameLeft.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventNameLeft.transform = CGAffineTransformMakeRotation(3.14 / 2);
            layoutController.lblEventNameRight.transform = CGAffineTransformMakeRotation(3.14 / 2);
            //            layoutController.lblEventDateRight.adjustsFontSizeToFitWidth = YES;
            //            layoutController.lblEventDateLeft.adjustsFontSizeToFitWidth = YES;
        }
        
        else
        {
            layoutController.lblEventNameRight.hidden = YES;
            layoutController.lblEventNameLeft.hidden = YES;
            //            layoutController.lblEventDateRight.hidden = YES;
            //            layoutController.lblEventDateLeft.hidden = YES;
            [layoutController setIsOverlay:overlay];
            [layoutController setImage:[UIImage imageWithData:bgimage]];
        }
        
        
        
        
        int tagView = 1;
        for (int i = 1; i <= [numberofphotos intValue]; i++)
        {
            NSLog(@"mPhotoCount: %ld", (long)[numberofphotos intValue]);
            
            UIImageView *imageLeft = (UIImageView *) [layoutController.view viewWithTag:tagView];
            UIImageView *imageRight = (UIImageView *) [layoutController.view viewWithTag:(tagView + 1)];
            
            UIImage *img = [UIImage imageWithData:[_arrSlidshowImg objectAtIndex:i-1]];
            [imageLeft setImage:img];
            [imageRight setImage:img];
            
            tagView += 2;
        }
        
        NSString * sharing = [self.record valueForKey:@"switchSharingLayout"];
        
        if ([sharing isEqual: @"YES"])
        {
            [self getPath];
            NSString *prefixString = @"Image";
            
            NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
            NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
            imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
            UIImage *image= [self captureView:layoutController.view];
            data = UIImageJPEGRepresentation(image,0.5);
            [data writeToFile:imagePath atomically:YES];
            
            NSLog(@"filepath=%@",imagePath);
            refURL = [NSURL fileURLWithPath:imagePath];
            if ([[_appDelegate.mcManager.session connectedPeers] count]) {
                [self sendResource];
            }
        }
        
        else
        {
            if (![[DBSession sharedSession] isLinked])
            {
                
                
                
            }
            
            else
            {
                NSString *prefixString = @"Image";
                
                NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
                NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
                
                
                [self getPath];
                imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
                UIImage *image= [self captureView:layoutController.view];
                data = UIImageJPEGRepresentation(image,0.5);
                [data writeToFile:imagePath atomically:YES];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                                        stringForKey:@"AlbumName"];
                NSArray* words = [savedValue componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                NSString* nospacestring = [words componentsJoinedByString:@""];
                
                NSString *destDir = [NSString stringWithFormat: @"/%@_layout/", nospacestring];
                restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
                restClient.delegate = self;
                [restClient uploadFile:[NSString stringWithFormat:@"%@.jpg",uniqueFileName] toPath:destDir withParentRev:nil fromPath:imagePath];
                
                
            }
            
        }
        
        NSString *valueToSave = [[NSUserDefaults standardUserDefaults]
                                 stringForKey:@"printport"];
        NSString *valueToSave1 = [[NSUserDefaults standardUserDefaults]
                                  stringForKey:@"printip"];
        
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
                NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
                
                NSURL * url = [NSURL URLWithString:[NSString stringWithFormat: @"http://%@:%@", valueToSave1,valueToSave]];
                NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
                NSData *imageData = UIImageJPEGRepresentation([self captureView:layoutController.view], 0.5);
                
                [urlRequest setHTTPMethod:@"POST"];
                [urlRequest setHTTPBody:imageData];
                
                NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest
                                                                   completionHandler:^(NSData *data5, NSURLResponse *response, NSError *error) {
                                                                       NSLog(@"Response:%@ %@\n", response, error);
                                                                       if(error == nil)
                                                                       {
                                                                           NSString * text = [[NSString alloc] initWithData: data5 encoding: NSUTF8StringEncoding];
                                                                           NSLog(@"Data = %@",text);
                                                                       }
                                                                       
                                                                   }];
                [dataTask resume];
            });
            
        });
        [CustomAlbum addNewAssetWithImage:[self captureView:layoutController.view] toAlbum:[CustomAlbum getMyAlbumWithName:strAlbumName] onSuccess:^(NSString *ImageId) {
            recentImg = ImageId;
            NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
            NSData *data3 = [NSKeyedArchiver archivedDataWithRootObject:[self captureView:layoutController.view]];
            [currentDefaults setObject:data3 forKey:@"layoutphoto"];
            
            
            
        } onError:^(NSError *error) {
            NSLog(@"probelm in saving image");
        }];
        
        
    }
    
    
    if ([string61 isEqualToString:@"2"])
    {
        VerLayOutThree* layoutController = [[VerLayOutThree alloc] initWithNibName:@"VerLayOutThree" bundle:nil];
        
        
        if (bgimage == nil)
        {
            [layoutController setEventDate:eventdate];
            [layoutController setEventName:eventname];
            layoutController.view.backgroundColor = [UIColor colorWithString:background];
            // layoutController.lblEventNameRight.textColor = [UIColor colorWithString:font];
            layoutController.lblEventNameLeft.textColor = [UIColor colorWithString:font];
            //            layoutController.lblEventDateRight.textColor = [UIColor colorWithString:font];
            layoutController.lblEventDateLeft.textColor = [UIColor colorWithString:font];
            
            
            //   layoutController.lblEventNameRight.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventNameLeft.adjustsFontSizeToFitWidth = YES;
            //            layoutController.lblEventDateRight.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventDateLeft.adjustsFontSizeToFitWidth = YES;
            
        }
        
        else
        {
            //   layoutController.lblEventNameRight.hidden = YES;
            layoutController.lblEventNameLeft.hidden = YES;
            //            layoutController.lblEventDateRight.hidden = YES;
            layoutController.lblEventDateLeft.hidden = YES;
            [layoutController setIsOverlay:overlay];
            [layoutController setImage:[UIImage imageWithData:bgimage]];
        }
        
        
        
        for (int i = 0; i <= [numberofphotos intValue]; i++) {
            UIImage *img = [UIImage imageWithData:[_arrSlidshowImg objectAtIndex:i % 4]];
            UIImageView *imgView1 = (UIImageView *) [layoutController.view viewWithTag:i + 1];
            [imgView1 setImage:img];
            
            if ([overlay isEqualToString:@"NO"])
            {
                imgView1.layer.borderWidth = 4;
                imgView1.layer.borderColor = [UIColor colorWithString:font].CGColor;
            }
            
            
        }
        
        
        
        NSString * sharing = [self.record valueForKey:@"switchSharingLayout"];
        
        if ([sharing isEqual: @"YES"])
        {
            [self getPath];
            NSString *prefixString = @"Image";
            
            NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
            NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
            imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
            UIImage *image= [self captureView:layoutController.view];
            data = UIImageJPEGRepresentation(image,0.5);
            [data writeToFile:imagePath atomically:YES];
            
            NSLog(@"filepath=%@",imagePath);
            refURL = [NSURL fileURLWithPath:imagePath];
            if ([[_appDelegate.mcManager.session connectedPeers] count]) {
                [self sendResource];
            }
        }
        
        else
        {
            if (![[DBSession sharedSession] isLinked])
            {
                
                
                
            }
            
            else
            {
                NSString *prefixString = @"Image";
                
                NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
                NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
                
                
                [self getPath];
                imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
                UIImage *image= [self captureView:layoutController.view];
                data = UIImageJPEGRepresentation(image,0.5);
                [data writeToFile:imagePath atomically:YES];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                                        stringForKey:@"AlbumName"];
                NSArray* words = [savedValue componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                NSString* nospacestring = [words componentsJoinedByString:@""];
                
                NSString *destDir = [NSString stringWithFormat: @"/%@_layout/", nospacestring];
                restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
                restClient.delegate = self;
                [restClient uploadFile:[NSString stringWithFormat:@"%@.jpg",uniqueFileName] toPath:destDir withParentRev:nil fromPath:imagePath];
                
                
            }
            
        }
        
        NSString *valueToSave = [[NSUserDefaults standardUserDefaults]
                                 stringForKey:@"printport"];
        NSString *valueToSave1 = [[NSUserDefaults standardUserDefaults]
                                  stringForKey:@"printip"];
        
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
                NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
                
                NSURL * url = [NSURL URLWithString:[NSString stringWithFormat: @"http://%@:%@", valueToSave1,valueToSave]];
                NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
                NSData *imageData = UIImageJPEGRepresentation([self captureView:layoutController.view], 0.5);
                
                [urlRequest setHTTPMethod:@"POST"];
                [urlRequest setHTTPBody:imageData];
                
                NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest
                                                                   completionHandler:^(NSData *data5, NSURLResponse *response, NSError *error) {
                                                                       NSLog(@"Response:%@ %@\n", response, error);
                                                                       if(error == nil)
                                                                       {
                                                                           NSString * text = [[NSString alloc] initWithData: data5 encoding: NSUTF8StringEncoding];
                                                                           NSLog(@"Data = %@",text);
                                                                       }
                                                                       
                                                                   }];
                [dataTask resume];
            });
            
        });
        [CustomAlbum addNewAssetWithImage:[self captureView:layoutController.view] toAlbum:[CustomAlbum getMyAlbumWithName:strAlbumName] onSuccess:^(NSString *ImageId) {
            recentImg = ImageId;
            NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
            NSData *data3 = [NSKeyedArchiver archivedDataWithRootObject:[self captureView:layoutController.view]];
            [currentDefaults setObject:data3 forKey:@"layoutphoto"];
            
            
            
        } onError:^(NSError *error) {
            NSLog(@"probelm in saving image");
        }];
        
        
    }
    
    
    
    if ([string61 isEqualToString:@"3"])
    {
        VerLayOutFour* layoutController = [[VerLayOutFour alloc] initWithNibName:@"VerLayOutFour" bundle:nil];
        
        
        if (bgimage == nil)
        {
            [layoutController setEventDate:eventdate];
            [layoutController setEventName:eventname];
            layoutController.view.backgroundColor = [UIColor colorWithString:background];
            // layoutController.lblEventNameRight.textColor = [UIColor colorWithString:font];
            layoutController.lblEventNameLeft.textColor = [UIColor colorWithString:font];
            //            layoutController.lblEventDateRight.textColor = [UIColor colorWithString:font];
            layoutController.lblEventDateLeft.textColor = [UIColor colorWithString:font];
            
            
            //   layoutController.lblEventNameRight.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventNameLeft.adjustsFontSizeToFitWidth = YES;
            //            layoutController.lblEventDateRight.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventDateLeft.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventNameLeft.transform = CGAffineTransformMakeRotation(3.14 / 2);
            layoutController.lblEventDateLeft.transform = CGAffineTransformMakeRotation(3.14 / 2);
        }
        
        else
        {
            //   layoutController.lblEventNameRight.hidden = YES;
            layoutController.lblEventNameLeft.hidden = YES;
            //            layoutController.lblEventDateRight.hidden = YES;
            layoutController.lblEventDateLeft.hidden = YES;
            [layoutController setIsOverlay:overlay];
            [layoutController setImage:[UIImage imageWithData:bgimage]];
        }
        
        
        
        for (int i = 0; i <= [numberofphotos intValue]; i++) {
            UIImage *img = [UIImage imageWithData:[_arrSlidshowImg objectAtIndex:i % [numberofphotos intValue]]];
            UIImageView *imgView1 = (UIImageView *) [layoutController.view viewWithTag:i + 1];
            [imgView1 setImage:img];
            
            if ([overlay isEqualToString:@"NO"])
            {
                imgView1.layer.borderWidth = 4;
                imgView1.layer.borderColor = [UIColor colorWithString:font].CGColor;
            }
            
            
        }
        
        
        
        
        NSString * sharing = [self.record valueForKey:@"switchSharingLayout"];
        
        if ([sharing isEqual: @"YES"])
        {
            [self getPath];
            NSString *prefixString = @"Image";
            
            NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
            NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
            imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
            UIImage *image= [self captureView:layoutController.view];
            data = UIImageJPEGRepresentation(image,0.5);
            [data writeToFile:imagePath atomically:YES];
            
            NSLog(@"filepath=%@",imagePath);
            refURL = [NSURL fileURLWithPath:imagePath];
            if ([[_appDelegate.mcManager.session connectedPeers] count]) {
                [self sendResource];
            }
        }
        
        else
        {
            if (![[DBSession sharedSession] isLinked])
            {
                
                
                
            }
            
            else
            {
                NSString *prefixString = @"Image";
                
                NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
                NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
                
                
                [self getPath];
                imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
                UIImage *image= [self captureView:layoutController.view];
                data = UIImageJPEGRepresentation(image,0.5);
                [data writeToFile:imagePath atomically:YES];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                                        stringForKey:@"AlbumName"];
                NSArray* words = [savedValue componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                NSString* nospacestring = [words componentsJoinedByString:@""];
                
                NSString *destDir = [NSString stringWithFormat: @"/%@_layout/", nospacestring];
                restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
                restClient.delegate = self;
                [restClient uploadFile:[NSString stringWithFormat:@"%@.jpg",uniqueFileName] toPath:destDir withParentRev:nil fromPath:imagePath];
                
                
            }
            
        }
        NSString *valueToSave = [[NSUserDefaults standardUserDefaults]
                                 stringForKey:@"printport"];
        NSString *valueToSave1 = [[NSUserDefaults standardUserDefaults]
                                  stringForKey:@"printip"];
        
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
                NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
                
                NSURL * url = [NSURL URLWithString:[NSString stringWithFormat: @"http://%@:%@", valueToSave1,valueToSave]];
                NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
                NSData *imageData = UIImageJPEGRepresentation([self captureView:layoutController.view], 0.5);
                
                [urlRequest setHTTPMethod:@"POST"];
                [urlRequest setHTTPBody:imageData];
                
                NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest
                                                                   completionHandler:^(NSData *data5, NSURLResponse *response, NSError *error) {
                                                                       NSLog(@"Response:%@ %@\n", response, error);
                                                                       if(error == nil)
                                                                       {
                                                                           NSString * text = [[NSString alloc] initWithData: data5 encoding: NSUTF8StringEncoding];
                                                                           NSLog(@"Data = %@",text);
                                                                       }
                                                                       
                                                                   }];
                [dataTask resume];
            });
            
        });
        [CustomAlbum addNewAssetWithImage:[self captureView:layoutController.view] toAlbum:[CustomAlbum getMyAlbumWithName:strAlbumName] onSuccess:^(NSString *ImageId) {
            recentImg = ImageId;
            NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
            NSData *data3 = [NSKeyedArchiver archivedDataWithRootObject:[self captureView:layoutController.view]];
            [currentDefaults setObject:data3 forKey:@"layoutphoto"];
            
            
            
        } onError:^(NSError *error) {
            NSLog(@"probelm in saving image");
        }];
        
        
    }
    
    if ([string61 isEqualToString:@"4"])
    {
        VerLayOutFive* layoutController = [[VerLayOutFive alloc] initWithNibName:@"VerLayOutFive" bundle:nil];
        
        
        if (bgimage == nil)
        {
            [layoutController setEventDate:eventdate];
            [layoutController setEventName:eventname];
            layoutController.view.backgroundColor = [UIColor colorWithString:background];
            // layoutController.lblEventNameRight.textColor = [UIColor colorWithString:font];
            layoutController.lblEventNameLeft.textColor = [UIColor colorWithString:font];
            //            layoutController.lblEventDateRight.textColor = [UIColor colorWithString:font];
            layoutController.lblEventDateLeft.textColor = [UIColor colorWithString:font];
            
            
            //   layoutController.lblEventNameRight.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventNameLeft.adjustsFontSizeToFitWidth = YES;
            //            layoutController.lblEventDateRight.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventDateLeft.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventDateLeft.transform = CGAffineTransformMakeRotation(3.14 / 2);
            layoutController.lblEventNameLeft.transform = CGAffineTransformMakeRotation(3.14 / 2);
            
        }
        
        else
        {
            //   layoutController.lblEventNameRight.hidden = YES;
            layoutController.lblEventNameLeft.hidden = YES;
            //            layoutController.lblEventDateRight.hidden = YES;
            layoutController.lblEventDateLeft.hidden = YES;
            [layoutController setIsOverlay:overlay];
            [layoutController setImage:[UIImage imageWithData:bgimage]];
        }
        
        
        
        for (int i = 0; i <= [numberofphotos intValue]; i++) {
            UIImage *img = [UIImage imageWithData:[_arrSlidshowImg objectAtIndex:i % [numberofphotos intValue]]];
            UIImageView *imgView1 = (UIImageView *) [layoutController.view viewWithTag:i + 1];
            [imgView1 setImage:img];
            
            if ([overlay isEqualToString:@"NO"])
            {
                imgView1.layer.borderWidth = 4;
                imgView1.layer.borderColor = [UIColor colorWithString:font].CGColor;
            }
            
            
        }
        
        
        
        
        NSString * sharing = [self.record valueForKey:@"switchSharingLayout"];
        
        if ([sharing isEqual: @"YES"])
        {
            [self getPath];
            NSString *prefixString = @"Image";
            
            NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
            NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
            imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
            UIImage *image= [self captureView:layoutController.view];
            data = UIImageJPEGRepresentation(image,0.5);
            [data writeToFile:imagePath atomically:YES];
            
            NSLog(@"filepath=%@",imagePath);
            refURL = [NSURL fileURLWithPath:imagePath];
            if ([[_appDelegate.mcManager.session connectedPeers] count]) {
                [self sendResource];
            }
        }
        
        else
        {
            if (![[DBSession sharedSession] isLinked])
            {
                
                
                
            }
            
            else
            {
                NSString *prefixString = @"Image";
                
                NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
                NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
                
                
                [self getPath];
                imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
                UIImage *image= [self captureView:layoutController.view];
                data = UIImageJPEGRepresentation(image,0.5);
                [data writeToFile:imagePath atomically:YES];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                                        stringForKey:@"AlbumName"];
                NSArray* words = [savedValue componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                NSString* nospacestring = [words componentsJoinedByString:@""];
                
                NSString *destDir = [NSString stringWithFormat: @"/%@_layout/", nospacestring];
                restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
                restClient.delegate = self;
                [restClient uploadFile:[NSString stringWithFormat:@"%@.jpg",uniqueFileName] toPath:destDir withParentRev:nil fromPath:imagePath];
                
                
            }
            
        }
        NSString *valueToSave = [[NSUserDefaults standardUserDefaults]
                                 stringForKey:@"printport"];
        NSString *valueToSave1 = [[NSUserDefaults standardUserDefaults]
                                  stringForKey:@"printip"];
        
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
                NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
                
                NSURL * url = [NSURL URLWithString:[NSString stringWithFormat: @"http://%@:%@", valueToSave1,valueToSave]];
                NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
                NSData *imageData = UIImageJPEGRepresentation([self captureView:layoutController.view], 0.5);
                
                [urlRequest setHTTPMethod:@"POST"];
                [urlRequest setHTTPBody:imageData];
                
                NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest
                                                                   completionHandler:^(NSData *data5, NSURLResponse *response, NSError *error) {
                                                                       NSLog(@"Response:%@ %@\n", response, error);
                                                                       if(error == nil)
                                                                       {
                                                                           NSString * text = [[NSString alloc] initWithData: data5 encoding: NSUTF8StringEncoding];
                                                                           NSLog(@"Data = %@",text);
                                                                       }
                                                                       
                                                                   }];
                [dataTask resume];
            });
            
        });
        [CustomAlbum addNewAssetWithImage:[self captureView:layoutController.view] toAlbum:[CustomAlbum getMyAlbumWithName:strAlbumName] onSuccess:^(NSString *ImageId) {
            recentImg = ImageId;
            NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
            NSData *data3 = [NSKeyedArchiver archivedDataWithRootObject:[self captureView:layoutController.view]];
            [currentDefaults setObject:data3 forKey:@"layoutphoto"];
            
            
            
        } onError:^(NSError *error) {
            NSLog(@"probelm in saving image");
        }];
        
        
    }
    
    
    
    
    
    
    
    if ([string61 isEqualToString:@"5"])
    {
        VerLayOutSix* layoutController = [[VerLayOutSix alloc] initWithNibName:@"VerLayOutSix" bundle:nil];
        
        
        if (bgimage == nil)
        {
            [layoutController setEventDate:eventdate];
            [layoutController setEventName:eventname];
            layoutController.view.backgroundColor = [UIColor colorWithString:background];
            // layoutController.lblEventNameRight.textColor = [UIColor colorWithString:font];
            layoutController.lblEventNameLeft.textColor = [UIColor colorWithString:font];
            //            layoutController.lblEventDateRight.textColor = [UIColor colorWithString:font];
            layoutController.lblEventDateLeft.textColor = [UIColor colorWithString:font];
            
            
            //   layoutController.lblEventNameRight.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventNameLeft.adjustsFontSizeToFitWidth = YES;
            //            layoutController.lblEventDateRight.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventDateLeft.adjustsFontSizeToFitWidth = YES;
            
        }
        
        else
        {
            //   layoutController.lblEventNameRight.hidden = YES;
            layoutController.lblEventNameLeft.hidden = YES;
            //            layoutController.lblEventDateRight.hidden = YES;
            layoutController.lblEventDateLeft.hidden = YES;
            [layoutController setIsOverlay:overlay];
            [layoutController setImage:[UIImage imageWithData:bgimage]];
        }
        
        
        
        for (int i = 0; i <= [numberofphotos intValue]; i++) {
            UIImage *img = [UIImage imageWithData:[_arrSlidshowImg objectAtIndex:i % [numberofphotos intValue]]];
            UIImageView *imgView1 = (UIImageView *) [layoutController.view viewWithTag:i + 1];
            [imgView1 setImage:img];
            
            if ([overlay isEqualToString:@"NO"])
            {
                imgView1.layer.borderWidth = 4;
                imgView1.layer.borderColor = [UIColor colorWithString:font].CGColor;
            }
            
            
        }
        
        
        
        
        NSString * sharing = [self.record valueForKey:@"switchSharingLayout"];
        
        if ([sharing isEqual: @"YES"])
        {
            [self getPath];
            NSString *prefixString = @"Image";
            
            NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
            NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
            imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
            UIImage *image= [self captureView:layoutController.view];
            data = UIImageJPEGRepresentation(image,0.5);
            [data writeToFile:imagePath atomically:YES];
            
            NSLog(@"filepath=%@",imagePath);
            refURL = [NSURL fileURLWithPath:imagePath];
            if ([[_appDelegate.mcManager.session connectedPeers] count]) {
                [self sendResource];
            }
        }
        
        else
        {
            if (![[DBSession sharedSession] isLinked])
            {
                
                
                
            }
            
            else
            {
                NSString *prefixString = @"Image";
                
                NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
                NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
                
                
                [self getPath];
                imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
                UIImage *image= [self captureView:layoutController.view];
                data = UIImageJPEGRepresentation(image,0.5);
                [data writeToFile:imagePath atomically:YES];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                                        stringForKey:@"AlbumName"];
                NSArray* words = [savedValue componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                NSString* nospacestring = [words componentsJoinedByString:@""];
                
                NSString *destDir = [NSString stringWithFormat: @"/%@_layout/", nospacestring];
                restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
                restClient.delegate = self;
                [restClient uploadFile:[NSString stringWithFormat:@"%@.jpg",uniqueFileName] toPath:destDir withParentRev:nil fromPath:imagePath];
                
                
            }
            
        }
        
        NSString *valueToSave = [[NSUserDefaults standardUserDefaults]
                                 stringForKey:@"printport"];
        NSString *valueToSave1 = [[NSUserDefaults standardUserDefaults]
                                  stringForKey:@"printip"];
        
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
                NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
                
                NSURL * url = [NSURL URLWithString:[NSString stringWithFormat: @"http://%@:%@", valueToSave1,valueToSave]];
                NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
                NSData *imageData = UIImageJPEGRepresentation([self captureView:layoutController.view], 0.5);
                
                [urlRequest setHTTPMethod:@"POST"];
                [urlRequest setHTTPBody:imageData];
                
                NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest
                                                                   completionHandler:^(NSData *data5, NSURLResponse *response, NSError *error) {
                                                                       NSLog(@"Response:%@ %@\n", response, error);
                                                                       if(error == nil)
                                                                       {
                                                                           NSString * text = [[NSString alloc] initWithData: data5 encoding: NSUTF8StringEncoding];
                                                                           NSLog(@"Data = %@",text);
                                                                       }
                                                                       
                                                                   }];
                [dataTask resume];
            });
            
        });
        [CustomAlbum addNewAssetWithImage:[self captureView:layoutController.view] toAlbum:[CustomAlbum getMyAlbumWithName:strAlbumName] onSuccess:^(NSString *ImageId) {
            recentImg = ImageId;
            NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
            NSData *data3 = [NSKeyedArchiver archivedDataWithRootObject:[self captureView:layoutController.view]];
            [currentDefaults setObject:data3 forKey:@"layoutphoto"];
            
            
            
        } onError:^(NSError *error) {
            NSLog(@"probelm in saving image");
        }];
        
        
    }
    
    
    
    
    if ([string61 isEqualToString:@"6"])
    {
        VerLayOutSeven* layoutController = [[VerLayOutSeven alloc] initWithNibName:@"VerLayOutSeven" bundle:nil];
        
        
        if (bgimage == nil)
        {
            [layoutController setEventDate:eventdate];
            [layoutController setEventName:eventname];
            layoutController.view.backgroundColor = [UIColor colorWithString:background];
            // layoutController.lblEventNameRight.textColor = [UIColor colorWithString:font];
            layoutController.lblEventNameLeft.textColor = [UIColor colorWithString:font];
            //            layoutController.lblEventDateRight.textColor = [UIColor colorWithString:font];
            layoutController.lblEventDateLeft.textColor = [UIColor colorWithString:font];
            
            
            //   layoutController.lblEventNameRight.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventNameLeft.adjustsFontSizeToFitWidth = YES;
            //            layoutController.lblEventDateRight.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventDateLeft.adjustsFontSizeToFitWidth = YES;
            
            layoutController.lblEventDateLeft.transform = CGAffineTransformMakeRotation(3.14 / 2);
            layoutController.lblEventNameLeft.transform = CGAffineTransformMakeRotation(3.14 / 2);
            
        }
        
        else
        {
            //   layoutController.lblEventNameRight.hidden = YES;
            layoutController.lblEventNameLeft.hidden = YES;
            //            layoutController.lblEventDateRight.hidden = YES;
            layoutController.lblEventDateLeft.hidden = YES;
            [layoutController setIsOverlay:overlay];
            [layoutController setImage:[UIImage imageWithData:bgimage]];
        }
        
        
        
        for (int i = 0; i <= [numberofphotos intValue]; i++) {
            UIImage *img = [UIImage imageWithData:[_arrSlidshowImg objectAtIndex:i % [numberofphotos intValue]]];
            UIImageView *imgView1 = (UIImageView *) [layoutController.view viewWithTag:i + 1];
            [imgView1 setImage:img];
            
            if ([overlay isEqualToString:@"NO"])
            {
                imgView1.layer.borderWidth = 4;
                imgView1.layer.borderColor = [UIColor colorWithString:font].CGColor;
                imgView1.transform = CGAffineTransformMakeRotation(3.14 / 2);
            }
            
            
        }
        
        
        
        NSString * sharing = [self.record valueForKey:@"switchSharingLayout"];
        
        if ([sharing isEqual: @"YES"])
        {
            [self getPath];
            NSString *prefixString = @"Image";
            
            NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
            NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
            imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
            UIImage *image= [self captureView:layoutController.view];
            data = UIImageJPEGRepresentation(image,0.5);
            [data writeToFile:imagePath atomically:YES];
            
            NSLog(@"filepath=%@",imagePath);
            refURL = [NSURL fileURLWithPath:imagePath];
            if ([[_appDelegate.mcManager.session connectedPeers] count]) {
                [self sendResource];
            }
        }
        
        else
        {
            if (![[DBSession sharedSession] isLinked])
            {
                
                
                
            }
            
            else
            {
                NSString *prefixString = @"Image";
                
                NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
                NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
                
                
                [self getPath];
                imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
                UIImage *image= [self captureView:layoutController.view];
                data = UIImageJPEGRepresentation(image,0.5);
                [data writeToFile:imagePath atomically:YES];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                                        stringForKey:@"AlbumName"];
                NSArray* words = [savedValue componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                NSString* nospacestring = [words componentsJoinedByString:@""];
                
                NSString *destDir = [NSString stringWithFormat: @"/%@_layout/", nospacestring];
                restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
                restClient.delegate = self;
                [restClient uploadFile:[NSString stringWithFormat:@"%@.jpg",uniqueFileName] toPath:destDir withParentRev:nil fromPath:imagePath];
                
                
            }
            
        }

        NSString *valueToSave = [[NSUserDefaults standardUserDefaults]
                                 stringForKey:@"printport"];
        NSString *valueToSave1 = [[NSUserDefaults standardUserDefaults]
                                  stringForKey:@"printip"];
        
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
                NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
                
                NSURL * url = [NSURL URLWithString:[NSString stringWithFormat: @"http://%@:%@", valueToSave1,valueToSave]];
                NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
                NSData *imageData = UIImageJPEGRepresentation([self captureView:layoutController.view], 0.5);
                
                [urlRequest setHTTPMethod:@"POST"];
                [urlRequest setHTTPBody:imageData];
                
                NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest
                                                                   completionHandler:^(NSData *data5, NSURLResponse *response, NSError *error) {
                                                                       NSLog(@"Response:%@ %@\n", response, error);
                                                                       if(error == nil)
                                                                       {
                                                                           NSString * text = [[NSString alloc] initWithData: data5 encoding: NSUTF8StringEncoding];
                                                                           NSLog(@"Data = %@",text);
                                                                       }
                                                                       
                                                                   }];
                [dataTask resume];
            });
            
        });
        
        [CustomAlbum addNewAssetWithImage:[self captureView:layoutController.view] toAlbum:[CustomAlbum getMyAlbumWithName:strAlbumName] onSuccess:^(NSString *ImageId) {
            recentImg = ImageId;
            NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
            NSData *data3 = [NSKeyedArchiver archivedDataWithRootObject:[self captureView:layoutController.view]];
            [currentDefaults setObject:data3 forKey:@"layoutphoto"];
            
            
            
        } onError:^(NSError *error) {
            NSLog(@"probelm in saving image");
        }];
        
        
    }
    
    
    
    
    
    if ([string61 isEqualToString:@"7"])
    {
        VerLayOutEight* layoutController = [[VerLayOutEight alloc] initWithNibName:@"VerLayOutEight" bundle:nil];
        
        
        if (bgimage == nil)
        {
            [layoutController setEventDate:eventdate];
            [layoutController setEventName:eventname];
            layoutController.view.backgroundColor = [UIColor colorWithString:background];
            // layoutController.lblEventNameRight.textColor = [UIColor colorWithString:font];
            layoutController.lblEventNameLeft.textColor = [UIColor colorWithString:font];
            //            layoutController.lblEventDateRight.textColor = [UIColor colorWithString:font];
            layoutController.lblEventDateLeft.textColor = [UIColor colorWithString:font];
            
            
            //   layoutController.lblEventNameRight.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventNameLeft.adjustsFontSizeToFitWidth = YES;
            //            layoutController.lblEventDateRight.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventDateLeft.adjustsFontSizeToFitWidth = YES;
            
            layoutController.lblEventDateLeft.transform = CGAffineTransformMakeRotation(3.14);
            layoutController.lblEventNameLeft.transform = CGAffineTransformMakeRotation(3.14);
            
        }
        
        else
        {
            //   layoutController.lblEventNameRight.hidden = YES;
            layoutController.lblEventNameLeft.hidden = YES;
            //            layoutController.lblEventDateRight.hidden = YES;
            layoutController.lblEventDateLeft.hidden = YES;
            [layoutController setIsOverlay:overlay];
            [layoutController setImage:[UIImage imageWithData:bgimage]];
        }
        
        
        
        for (int i = 0; i <= [numberofphotos intValue]; i++) {
            UIImage *img = [UIImage imageWithData:[_arrSlidshowImg objectAtIndex:i % [numberofphotos intValue]]];
            UIImageView *imgView1 = (UIImageView *) [layoutController.view viewWithTag:i + 1];
            [imgView1 setImage:img];
            
            if ([overlay isEqualToString:@"NO"])
            {
                imgView1.layer.borderWidth = 4;
                imgView1.layer.borderColor = [UIColor colorWithString:font].CGColor;
                imgView1.transform = CGAffineTransformMakeRotation(3.14 / 2);
            }
            
            
        }
        
        
        
        
        NSString * sharing = [self.record valueForKey:@"switchSharingLayout"];
        
        if ([sharing isEqual: @"YES"])
        {
            [self getPath];
            NSString *prefixString = @"Image";
            
            NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
            NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
            imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
            UIImage *image= [self captureView:layoutController.view];
            data = UIImageJPEGRepresentation(image,0.5);
            [data writeToFile:imagePath atomically:YES];
            
            NSLog(@"filepath=%@",imagePath);
            refURL = [NSURL fileURLWithPath:imagePath];
            if ([[_appDelegate.mcManager.session connectedPeers] count]) {
                [self sendResource];
            }
        }
        
        else
        {
            if (![[DBSession sharedSession] isLinked])
            {
                
                
                
            }
            
            else
            {
                NSString *prefixString = @"Image";
                
                NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
                NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
                
                
                [self getPath];
                imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
                UIImage *image= [self captureView:layoutController.view];
                data = UIImageJPEGRepresentation(image,0.5);
                [data writeToFile:imagePath atomically:YES];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                                        stringForKey:@"AlbumName"];
                NSArray* words = [savedValue componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                NSString* nospacestring = [words componentsJoinedByString:@""];
                
                NSString *destDir = [NSString stringWithFormat: @"/%@_layout/", nospacestring];
                restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
                restClient.delegate = self;
                [restClient uploadFile:[NSString stringWithFormat:@"%@.jpg",uniqueFileName] toPath:destDir withParentRev:nil fromPath:imagePath];
                
                
            }
            
        }
        
        NSString *valueToSave = [[NSUserDefaults standardUserDefaults]
                                 stringForKey:@"printport"];
        NSString *valueToSave1 = [[NSUserDefaults standardUserDefaults]
                                  stringForKey:@"printip"];
        
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
                NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
                
                NSURL * url = [NSURL URLWithString:[NSString stringWithFormat: @"http://%@:%@", valueToSave1,valueToSave]];
                NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
                NSData *imageData = UIImageJPEGRepresentation([self captureView:layoutController.view], 0.5);
                
                [urlRequest setHTTPMethod:@"POST"];
                [urlRequest setHTTPBody:imageData];
                
                NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest
                                                                   completionHandler:^(NSData *data5, NSURLResponse *response, NSError *error) {
                                                                       NSLog(@"Response:%@ %@\n", response, error);
                                                                       if(error == nil)
                                                                       {
                                                                           NSString * text = [[NSString alloc] initWithData: data5 encoding: NSUTF8StringEncoding];
                                                                           NSLog(@"Data = %@",text);
                                                                       }
                                                                       
                                                                   }];
                [dataTask resume];
            });
            
        });
        [CustomAlbum addNewAssetWithImage:[self captureView:layoutController.view] toAlbum:[CustomAlbum getMyAlbumWithName:strAlbumName] onSuccess:^(NSString *ImageId) {
            recentImg = ImageId;
            NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
            NSData *data3 = [NSKeyedArchiver archivedDataWithRootObject:[self captureView:layoutController.view]];
            [currentDefaults setObject:data3 forKey:@"layoutphoto"];
            
            
            
        } onError:^(NSError *error) {
            NSLog(@"probelm in saving image");
        }];
        
        
    }
    
    
    
    
    //5x7 Layouts//
    
    
    
    
    
    if ([string61 isEqualToString:@"8"])
    {
        VerLayOutOne1* layoutController = [[VerLayOutOne1 alloc] initWithNibName:@"VerLayOutOne1" bundle:nil];
        
        
        if (bgimage == nil)
        {
            [layoutController setEventDate:eventdate];
            [layoutController setEventName:eventname];
            layoutController.view.backgroundColor = [UIColor colorWithString:background];
            layoutController.lblEventNameRight.textColor = [UIColor colorWithString:font];
            layoutController.lblEventNameLeft.textColor = [UIColor colorWithString:font];
            layoutController.lblEventDateRight.textColor = [UIColor colorWithString:font];
            layoutController.lblEventDateLeft.textColor = [UIColor colorWithString:font];
            
            
            layoutController.lblEventNameRight.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventNameLeft.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventDateRight.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventDateLeft.adjustsFontSizeToFitWidth = YES;
        }
        
        else
        {
            layoutController.lblEventNameRight.hidden = YES;
            layoutController.lblEventNameLeft.hidden = YES;
            layoutController.lblEventDateRight.hidden = YES;
            layoutController.lblEventDateLeft.hidden = YES;
            [layoutController setIsOverlay:overlay];
            [layoutController setImage:[UIImage imageWithData:bgimage]];
        }
        
        
        
        int tagView = 1;
        for (int i = 1; i <= [numberofphotos intValue]; i++)
        {
            NSLog(@"mPhotoCount: %ld", (long)[numberofphotos intValue]);
            
            UIImageView *imageLeft = (UIImageView *) [layoutController.view viewWithTag:tagView];
            UIImageView *imageRight = (UIImageView *) [layoutController.view viewWithTag:(tagView + 1)];
            
            UIImage *img = [UIImage imageWithData:[_arrSlidshowImg objectAtIndex:i-1]];
            [imageLeft setImage:img];
            [imageRight setImage:img];
            
            tagView += 2;
        }
        
        NSString * sharing = [self.record valueForKey:@"switchSharingLayout"];
        
        if ([sharing isEqual: @"YES"])
        {
            [self getPath];
            NSString *prefixString = @"Image";
            
            NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
            NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
            imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
            UIImage *image= [self captureView:layoutController.view];
            data = UIImageJPEGRepresentation(image,0.5);
            [data writeToFile:imagePath atomically:YES];
            
            NSLog(@"filepath=%@",imagePath);
            refURL = [NSURL fileURLWithPath:imagePath];
            if ([[_appDelegate.mcManager.session connectedPeers] count]) {
                [self sendResource];
            }
        }
        
        else
        {
            if (![[DBSession sharedSession] isLinked])
            {
                
                
                
            }
            
            else
            {
                NSString *prefixString = @"Image";
                
                NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
                NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
                
                
                [self getPath];
                imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
                UIImage *image= [self captureView:layoutController.view];
                data = UIImageJPEGRepresentation(image,0.5);
                [data writeToFile:imagePath atomically:YES];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                                        stringForKey:@"AlbumName"];
                NSArray* words = [savedValue componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                NSString* nospacestring = [words componentsJoinedByString:@""];
                
                NSString *destDir = [NSString stringWithFormat: @"/%@_layout/", nospacestring];
                restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
                restClient.delegate = self;
                [restClient uploadFile:[NSString stringWithFormat:@"%@.jpg",uniqueFileName] toPath:destDir withParentRev:nil fromPath:imagePath];
                
                
            }
            
        }
        NSString *valueToSave = [[NSUserDefaults standardUserDefaults]
                                 stringForKey:@"printport"];
        NSString *valueToSave1 = [[NSUserDefaults standardUserDefaults]
                                  stringForKey:@"printip"];
        
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
                NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
                
                NSURL * url = [NSURL URLWithString:[NSString stringWithFormat: @"http://%@:%@", valueToSave1,valueToSave]];
                NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
                NSData *imageData = UIImageJPEGRepresentation([self captureView:layoutController.view], 0.5);
                
                [urlRequest setHTTPMethod:@"POST"];
                [urlRequest setHTTPBody:imageData];
                
                NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest
                                                                   completionHandler:^(NSData *data5, NSURLResponse *response, NSError *error) {
                                                                       NSLog(@"Response:%@ %@\n", response, error);
                                                                       if(error == nil)
                                                                       {
                                                                           NSString * text = [[NSString alloc] initWithData: data5 encoding: NSUTF8StringEncoding];
                                                                           NSLog(@"Data = %@",text);
                                                                       }
                                                                       
                                                                   }];
                [dataTask resume];
            });
            
        });
        [CustomAlbum addNewAssetWithImage:[self captureView:layoutController.view] toAlbum:[CustomAlbum getMyAlbumWithName:strAlbumName] onSuccess:^(NSString *ImageId) {
            recentImg = ImageId;
            NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
            NSData *data3 = [NSKeyedArchiver archivedDataWithRootObject:[self captureView:layoutController.view]];
            [currentDefaults setObject:data3 forKey:@"layoutphoto"];
            
            
            
        } onError:^(NSError *error) {
            NSLog(@"probelm in saving image");
        }];
        
        
    }
    
    
    
    if ([string61 isEqualToString:@"9"])
    {
        VerLayOutTwo1* layoutController = [[VerLayOutTwo1 alloc] initWithNibName:@"VerLayOutTwo1" bundle:nil];
        
        
        if (bgimage == nil)
        {
            [layoutController setEventDate:eventdate];
            [layoutController setEventName:eventname];
            layoutController.view.backgroundColor = [UIColor colorWithString:background];
            layoutController.lblEventNameRight.textColor = [UIColor colorWithString:font];
            layoutController.lblEventNameLeft.textColor = [UIColor colorWithString:font];
            //            layoutController.lblEventDateRight.textColor = [UIColor colorWithString:font];
            //            layoutController.lblEventDateLeft.textColor = [UIColor colorWithString:font];
            
            
            layoutController.lblEventNameRight.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventNameLeft.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventNameRight.transform = CGAffineTransformMakeRotation(3.14/2);
            layoutController.lblEventNameLeft.transform = CGAffineTransformMakeRotation(3.14/2);
            //            layoutController.lblEventDateRight.adjustsFontSizeToFitWidth = YES;
            //            layoutController.lblEventDateLeft.adjustsFontSizeToFitWidth = YES;
        }
        
        else
        {
            layoutController.lblEventNameRight.hidden = YES;
            layoutController.lblEventNameLeft.hidden = YES;
            //            layoutController.lblEventDateRight.hidden = YES;
            //            layoutController.lblEventDateLeft.hidden = YES;
            [layoutController setIsOverlay:overlay];
            [layoutController setImage:[UIImage imageWithData:bgimage]];
        }
        
        
        
        
        int tagView = 1;
        for (int i = 1; i <= [numberofphotos intValue]; i++)
        {
            NSLog(@"mPhotoCount: %ld", (long)[numberofphotos intValue]);
            
            UIImageView *imageLeft = (UIImageView *) [layoutController.view viewWithTag:tagView];
            UIImageView *imageRight = (UIImageView *) [layoutController.view viewWithTag:(tagView + 1)];
            
            UIImage *img = [UIImage imageWithData:[_arrSlidshowImg objectAtIndex:i-1]];
            [imageLeft setImage:img];
            [imageRight setImage:img];
            
            tagView += 2;
        }
        
        NSString * sharing = [self.record valueForKey:@"switchSharingLayout"];
        
        if ([sharing isEqual: @"YES"])
        {
            [self getPath];
            NSString *prefixString = @"Image";
            
            NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
            NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
            imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
            UIImage *image= [self captureView:layoutController.view];
            data = UIImageJPEGRepresentation(image,0.5);
            [data writeToFile:imagePath atomically:YES];
            
            NSLog(@"filepath=%@",imagePath);
            refURL = [NSURL fileURLWithPath:imagePath];
            if ([[_appDelegate.mcManager.session connectedPeers] count]) {
                [self sendResource];
            }
        }
        
        else
        {
            if (![[DBSession sharedSession] isLinked])
            {
                
                
                
            }
            
            else
            {
                NSString *prefixString = @"Image";
                
                NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
                NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
                
                
                [self getPath];
                imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
                UIImage *image= [self captureView:layoutController.view];
                data = UIImageJPEGRepresentation(image,0.5);
                [data writeToFile:imagePath atomically:YES];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                                        stringForKey:@"AlbumName"];
                NSArray* words = [savedValue componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                NSString* nospacestring = [words componentsJoinedByString:@""];
                
                NSString *destDir = [NSString stringWithFormat: @"/%@_layout/", nospacestring];
                restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
                restClient.delegate = self;
                [restClient uploadFile:[NSString stringWithFormat:@"%@.jpg",uniqueFileName] toPath:destDir withParentRev:nil fromPath:imagePath];
                
                
            }
            
        }
        NSString *valueToSave = [[NSUserDefaults standardUserDefaults]
                                 stringForKey:@"printport"];
        NSString *valueToSave1 = [[NSUserDefaults standardUserDefaults]
                                  stringForKey:@"printip"];
        
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
                NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
                
                NSURL * url = [NSURL URLWithString:[NSString stringWithFormat: @"http://%@:%@", valueToSave1,valueToSave]];
                NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
                NSData *imageData = UIImageJPEGRepresentation([self captureView:layoutController.view], 0.5);
                
                [urlRequest setHTTPMethod:@"POST"];
                [urlRequest setHTTPBody:imageData];
                
                NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest
                                                                   completionHandler:^(NSData *data5, NSURLResponse *response, NSError *error) {
                                                                       NSLog(@"Response:%@ %@\n", response, error);
                                                                       if(error == nil)
                                                                       {
                                                                           NSString * text = [[NSString alloc] initWithData: data5 encoding: NSUTF8StringEncoding];
                                                                           NSLog(@"Data = %@",text);
                                                                       }
                                                                       
                                                                   }];
                [dataTask resume];
            });
            
        });
        
        [CustomAlbum addNewAssetWithImage:[self captureView:layoutController.view] toAlbum:[CustomAlbum getMyAlbumWithName:strAlbumName] onSuccess:^(NSString *ImageId) {
            recentImg = ImageId;
            NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
            NSData *data3 = [NSKeyedArchiver archivedDataWithRootObject:[self captureView:layoutController.view]];
            [currentDefaults setObject:data3 forKey:@"layoutphoto"];
            
            
            
        } onError:^(NSError *error) {
            NSLog(@"probelm in saving image");
        }];
        
        
    }
    
    
    if ([string61 isEqualToString:@"10"])
    {
        VerLayOutThree1* layoutController = [[VerLayOutThree1 alloc] initWithNibName:@"VerLayOutThree1" bundle:nil];
        
        
        if (bgimage == nil)
        {
            [layoutController setEventDate:eventdate];
            [layoutController setEventName:eventname];
            layoutController.view.backgroundColor = [UIColor colorWithString:background];
            // layoutController.lblEventNameRight.textColor = [UIColor colorWithString:font];
            layoutController.lblEventNameLeft.textColor = [UIColor colorWithString:font];
            //            layoutController.lblEventDateRight.textColor = [UIColor colorWithString:font];
            layoutController.lblEventDateLeft.textColor = [UIColor colorWithString:font];
            
            
            //   layoutController.lblEventNameRight.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventNameLeft.adjustsFontSizeToFitWidth = YES;
            //            layoutController.lblEventDateRight.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventDateLeft.adjustsFontSizeToFitWidth = YES;
            
        }
        
        else
        {
            //   layoutController.lblEventNameRight.hidden = YES;
            layoutController.lblEventNameLeft.hidden = YES;
            //            layoutController.lblEventDateRight.hidden = YES;
            layoutController.lblEventDateLeft.hidden = YES;
            [layoutController setIsOverlay:overlay];
            [layoutController setImage:[UIImage imageWithData:bgimage]];
        }
        
        
        
        for (int i = 0; i <= [numberofphotos intValue]; i++) {
            UIImage *img = [UIImage imageWithData:[_arrSlidshowImg objectAtIndex:i % 4]];
            UIImageView *imgView1 = (UIImageView *) [layoutController.view viewWithTag:i + 1];
            [imgView1 setImage:img];
            
            if ([overlay isEqualToString:@"NO"])
            {
                imgView1.layer.borderWidth = 4;
                imgView1.layer.borderColor = [UIColor colorWithString:font].CGColor;
            }
            
            
        }
        
        
        
        
        NSString * sharing = [self.record valueForKey:@"switchSharingLayout"];
        
        if ([sharing isEqual: @"YES"])
        {
            [self getPath];
            NSString *prefixString = @"Image";
            
            NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
            NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
            imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
            UIImage *image= [self captureView:layoutController.view];
            data = UIImageJPEGRepresentation(image,0.5);
            [data writeToFile:imagePath atomically:YES];
            
            NSLog(@"filepath=%@",imagePath);
            refURL = [NSURL fileURLWithPath:imagePath];
            if ([[_appDelegate.mcManager.session connectedPeers] count]) {
                [self sendResource];
            }
        }
        
        else
        {
            if (![[DBSession sharedSession] isLinked])
            {
                
                
                
            }
            
            else
            {
                NSString *prefixString = @"Image";
                
                NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
                NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
                
                
                [self getPath];
                imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
                UIImage *image= [self captureView:layoutController.view];
                data = UIImageJPEGRepresentation(image,0.5);
                [data writeToFile:imagePath atomically:YES];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                                        stringForKey:@"AlbumName"];
                NSArray* words = [savedValue componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                NSString* nospacestring = [words componentsJoinedByString:@""];
                
                NSString *destDir = [NSString stringWithFormat: @"/%@_layout/", nospacestring];
                restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
                restClient.delegate = self;
                [restClient uploadFile:[NSString stringWithFormat:@"%@.jpg",uniqueFileName] toPath:destDir withParentRev:nil fromPath:imagePath];
                
                
            }
            
        }
        NSString *valueToSave = [[NSUserDefaults standardUserDefaults]
                                 stringForKey:@"printport"];
        NSString *valueToSave1 = [[NSUserDefaults standardUserDefaults]
                                  stringForKey:@"printip"];
        
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
                NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
                
                NSURL * url = [NSURL URLWithString:[NSString stringWithFormat: @"http://%@:%@", valueToSave1,valueToSave]];
                NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
                NSData *imageData = UIImageJPEGRepresentation([self captureView:layoutController.view], 0.5);
                
                [urlRequest setHTTPMethod:@"POST"];
                [urlRequest setHTTPBody:imageData];
                
                NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest
                                                                   completionHandler:^(NSData *data5, NSURLResponse *response, NSError *error) {
                                                                       NSLog(@"Response:%@ %@\n", response, error);
                                                                       if(error == nil)
                                                                       {
                                                                           NSString * text = [[NSString alloc] initWithData: data5 encoding: NSUTF8StringEncoding];
                                                                           NSLog(@"Data = %@",text);
                                                                       }
                                                                       
                                                                   }];
                [dataTask resume];
            });
            
        });
        [CustomAlbum addNewAssetWithImage:[self captureView:layoutController.view] toAlbum:[CustomAlbum getMyAlbumWithName:strAlbumName] onSuccess:^(NSString *ImageId) {
            recentImg = ImageId;
            NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
            NSData *data3 = [NSKeyedArchiver archivedDataWithRootObject:[self captureView:layoutController.view]];
            [currentDefaults setObject:data3 forKey:@"layoutphoto"];
            
            
            
        } onError:^(NSError *error) {
            NSLog(@"probelm in saving image");
        }];
        
        
    }
    
    
    
    if ([string61 isEqualToString:@"11"])
    {
        VerLayOutFour1* layoutController = [[VerLayOutFour1 alloc] initWithNibName:@"VerLayOutFour1" bundle:nil];
        
        
        if (bgimage == nil)
        {
            [layoutController setEventDate:eventdate];
            [layoutController setEventName:eventname];
            layoutController.view.backgroundColor = [UIColor colorWithString:background];
            // layoutController.lblEventNameRight.textColor = [UIColor colorWithString:font];
            layoutController.lblEventNameLeft.textColor = [UIColor colorWithString:font];
            //            layoutController.lblEventDateRight.textColor = [UIColor colorWithString:font];
            layoutController.lblEventDateLeft.textColor = [UIColor colorWithString:font];
            
            
            //   layoutController.lblEventNameRight.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventNameLeft.adjustsFontSizeToFitWidth = YES;
            //            layoutController.lblEventDateRight.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventDateLeft.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventNameLeft.transform = CGAffineTransformMakeRotation(3.14 / 2);
            layoutController.lblEventDateLeft.transform = CGAffineTransformMakeRotation(3.14 / 2);
        }
        
        else
        {
            //   layoutController.lblEventNameRight.hidden = YES;
            layoutController.lblEventNameLeft.hidden = YES;
            //            layoutController.lblEventDateRight.hidden = YES;
            layoutController.lblEventDateLeft.hidden = YES;
            [layoutController setIsOverlay:overlay];
            [layoutController setImage:[UIImage imageWithData:bgimage]];
        }
        
        
        
        for (int i = 0; i <= [numberofphotos intValue]; i++) {
            UIImage *img = [UIImage imageWithData:[_arrSlidshowImg objectAtIndex:i % [numberofphotos intValue]]];
            UIImageView *imgView1 = (UIImageView *) [layoutController.view viewWithTag:i + 1];
            [imgView1 setImage:img];
            
            if ([overlay isEqualToString:@"NO"])
            {
                imgView1.layer.borderWidth = 4;
                imgView1.layer.borderColor = [UIColor colorWithString:font].CGColor;
            }
            
            
        }
        
        
        
        
        NSString * sharing = [self.record valueForKey:@"switchSharingLayout"];
        
        if ([sharing isEqual: @"YES"])
        {
            [self getPath];
            NSString *prefixString = @"Image";
            
            NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
            NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
            imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
            UIImage *image= [self captureView:layoutController.view];
            data = UIImageJPEGRepresentation(image,0.5);
            [data writeToFile:imagePath atomically:YES];
            
            NSLog(@"filepath=%@",imagePath);
            refURL = [NSURL fileURLWithPath:imagePath];
            if ([[_appDelegate.mcManager.session connectedPeers] count]) {
                [self sendResource];
            }
        }
        
        else
        {
            if (![[DBSession sharedSession] isLinked])
            {
                
                
                
            }
            
            else
            {
                NSString *prefixString = @"Image";
                
                NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
                NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
                
                
                [self getPath];
                imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
                UIImage *image= [self captureView:layoutController.view];
                data = UIImageJPEGRepresentation(image,0.5);
                [data writeToFile:imagePath atomically:YES];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                                        stringForKey:@"AlbumName"];
                NSArray* words = [savedValue componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                NSString* nospacestring = [words componentsJoinedByString:@""];
                
                NSString *destDir = [NSString stringWithFormat: @"/%@_layout/", nospacestring];
                restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
                restClient.delegate = self;
                [restClient uploadFile:[NSString stringWithFormat:@"%@.jpg",uniqueFileName] toPath:destDir withParentRev:nil fromPath:imagePath];
                
                
            }
            
        }
        NSString *valueToSave = [[NSUserDefaults standardUserDefaults]
                                 stringForKey:@"printport"];
        NSString *valueToSave1 = [[NSUserDefaults standardUserDefaults]
                                  stringForKey:@"printip"];
        
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
                NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
                
                NSURL * url = [NSURL URLWithString:[NSString stringWithFormat: @"http://%@:%@", valueToSave1,valueToSave]];
                NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
                NSData *imageData = UIImageJPEGRepresentation([self captureView:layoutController.view], 0.5);
                
                [urlRequest setHTTPMethod:@"POST"];
                [urlRequest setHTTPBody:imageData];
                
                NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest
                                                                   completionHandler:^(NSData *data5, NSURLResponse *response, NSError *error) {
                                                                       NSLog(@"Response:%@ %@\n", response, error);
                                                                       if(error == nil)
                                                                       {
                                                                           NSString * text = [[NSString alloc] initWithData: data5 encoding: NSUTF8StringEncoding];
                                                                           NSLog(@"Data = %@",text);
                                                                       }
                                                                       
                                                                   }];
                [dataTask resume];
            });
            
        });
        [CustomAlbum addNewAssetWithImage:[self captureView:layoutController.view] toAlbum:[CustomAlbum getMyAlbumWithName:strAlbumName] onSuccess:^(NSString *ImageId) {
            recentImg = ImageId;
            NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
            NSData *data3 = [NSKeyedArchiver archivedDataWithRootObject:[self captureView:layoutController.view]];
            [currentDefaults setObject:data3 forKey:@"layoutphoto"];
            
            
            
        } onError:^(NSError *error) {
            NSLog(@"probelm in saving image");
        }];
        
        
    }
    
    
    
    
    
    if ([string61 isEqualToString:@"12"])
    {
        VerLayOutFive1* layoutController = [[VerLayOutFive1 alloc] initWithNibName:@"VerLayOutFive1" bundle:nil];
        
        
        if (bgimage == nil)
        {
            [layoutController setEventDate:eventdate];
            [layoutController setEventName:eventname];
            layoutController.view.backgroundColor = [UIColor colorWithString:background];
            // layoutController.lblEventNameRight.textColor = [UIColor colorWithString:font];
            layoutController.lblEventNameLeft.textColor = [UIColor colorWithString:font];
            //            layoutController.lblEventDateRight.textColor = [UIColor colorWithString:font];
            layoutController.lblEventDateLeft.textColor = [UIColor colorWithString:font];
            
            
            //   layoutController.lblEventNameRight.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventNameLeft.adjustsFontSizeToFitWidth = YES;
            //            layoutController.lblEventDateRight.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventDateLeft.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventNameLeft.transform = CGAffineTransformMakeRotation(3.14/2);
            layoutController.lblEventDateLeft.transform = CGAffineTransformMakeRotation(3.14/2);
        }
        
        else
        {
            //   layoutController.lblEventNameRight.hidden = YES;
            layoutController.lblEventNameLeft.hidden = YES;
            //            layoutController.lblEventDateRight.hidden = YES;
            layoutController.lblEventDateLeft.hidden = YES;
            [layoutController setIsOverlay:overlay];
            [layoutController setImage:[UIImage imageWithData:bgimage]];
        }
        
        
        
        for (int i = 0; i <= [numberofphotos intValue]; i++) {
            UIImage *img = [UIImage imageWithData:[_arrSlidshowImg objectAtIndex:i % [numberofphotos intValue]]];
            UIImageView *imgView1 = (UIImageView *) [layoutController.view viewWithTag:i + 1];
            [imgView1 setImage:img];
            
            if ([overlay isEqualToString:@"NO"])
            {
                imgView1.layer.borderWidth = 4;
                imgView1.layer.borderColor = [UIColor colorWithString:font].CGColor;
            }
            
            
        }
        
        
        
        
        NSString * sharing = [self.record valueForKey:@"switchSharingLayout"];
        
        if ([sharing isEqual: @"YES"])
        {
            [self getPath];
            NSString *prefixString = @"Image";
            
            NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
            NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
            imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
            UIImage *image= [self captureView:layoutController.view];
            data = UIImageJPEGRepresentation(image,0.5);
            [data writeToFile:imagePath atomically:YES];
            
            NSLog(@"filepath=%@",imagePath);
            refURL = [NSURL fileURLWithPath:imagePath];
            if ([[_appDelegate.mcManager.session connectedPeers] count]) {
                [self sendResource];
            }
        }
        
        else
        {
            if (![[DBSession sharedSession] isLinked])
            {
                
                
                
            }
            
            else
            {
                NSString *prefixString = @"Image";
                
                NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
                NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
                
                
                [self getPath];
                imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
                UIImage *image= [self captureView:layoutController.view];
                data = UIImageJPEGRepresentation(image,0.5);
                [data writeToFile:imagePath atomically:YES];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                                        stringForKey:@"AlbumName"];
                NSArray* words = [savedValue componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                NSString* nospacestring = [words componentsJoinedByString:@""];
                
                NSString *destDir = [NSString stringWithFormat: @"/%@_layout/", nospacestring];
                restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
                restClient.delegate = self;
                [restClient uploadFile:[NSString stringWithFormat:@"%@.jpg",uniqueFileName] toPath:destDir withParentRev:nil fromPath:imagePath];
                
                
            }
            
        }
        NSString *valueToSave = [[NSUserDefaults standardUserDefaults]
                                 stringForKey:@"printport"];
        NSString *valueToSave1 = [[NSUserDefaults standardUserDefaults]
                                  stringForKey:@"printip"];
        
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
                NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
                
                NSURL * url = [NSURL URLWithString:[NSString stringWithFormat: @"http://%@:%@", valueToSave1,valueToSave]];
                NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
                NSData *imageData = UIImageJPEGRepresentation([self captureView:layoutController.view], 0.5);
                
                [urlRequest setHTTPMethod:@"POST"];
                [urlRequest setHTTPBody:imageData];
                
                NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest
                                                                   completionHandler:^(NSData *data5, NSURLResponse *response, NSError *error) {
                                                                       NSLog(@"Response:%@ %@\n", response, error);
                                                                       if(error == nil)
                                                                       {
                                                                           NSString * text = [[NSString alloc] initWithData: data5 encoding: NSUTF8StringEncoding];
                                                                           NSLog(@"Data = %@",text);
                                                                       }
                                                                       
                                                                   }];
                [dataTask resume];
            });
            
        });
        
        [CustomAlbum addNewAssetWithImage:[self captureView:layoutController.view] toAlbum:[CustomAlbum getMyAlbumWithName:strAlbumName] onSuccess:^(NSString *ImageId) {
            recentImg = ImageId;
            NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
            NSData *data3 = [NSKeyedArchiver archivedDataWithRootObject:[self captureView:layoutController.view]];
            [currentDefaults setObject:data3 forKey:@"layoutphoto"];
            
            
            
        } onError:^(NSError *error) {
            NSLog(@"probelm in saving image");
        }];
        
        
    }
    
    
    
    
    
    
    
    if ([string61 isEqualToString:@"13"])
    {
        VerLayOutSix1* layoutController = [[VerLayOutSix1 alloc] initWithNibName:@"VerLayOutSix1" bundle:nil];
        
        
        if (bgimage == nil)
        {
            [layoutController setEventDate:eventdate];
            [layoutController setEventName:eventname];
            layoutController.view.backgroundColor = [UIColor colorWithString:background];
            // layoutController.lblEventNameRight.textColor = [UIColor colorWithString:font];
            layoutController.lblEventNameLeft.textColor = [UIColor colorWithString:font];
            //            layoutController.lblEventDateRight.textColor = [UIColor colorWithString:font];
            layoutController.lblEventDateLeft.textColor = [UIColor colorWithString:font];
            
            
            //   layoutController.lblEventNameRight.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventNameLeft.adjustsFontSizeToFitWidth = YES;
            //            layoutController.lblEventDateRight.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventDateLeft.adjustsFontSizeToFitWidth = YES;
            
        }
        
        else
        {
            //   layoutController.lblEventNameRight.hidden = YES;
            layoutController.lblEventNameLeft.hidden = YES;
            //            layoutController.lblEventDateRight.hidden = YES;
            layoutController.lblEventDateLeft.hidden = YES;
            [layoutController setIsOverlay:overlay];
            [layoutController setImage:[UIImage imageWithData:bgimage]];
        }
        
        
        
        for (int i = 0; i <= [numberofphotos intValue]; i++) {
            UIImage *img = [UIImage imageWithData:[_arrSlidshowImg objectAtIndex:i % [numberofphotos intValue]]];
            UIImageView *imgView1 = (UIImageView *) [layoutController.view viewWithTag:i + 1];
            [imgView1 setImage:img];
            
            if ([overlay isEqualToString:@"NO"])
            {
                imgView1.layer.borderWidth = 4;
                imgView1.layer.borderColor = [UIColor colorWithString:font].CGColor;
            }
            
            
        }
        
        
        
        
        NSString * sharing = [self.record valueForKey:@"switchSharingLayout"];
        
        if ([sharing isEqual: @"YES"])
        {
            [self getPath];
            NSString *prefixString = @"Image";
            
            NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
            NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
            imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
            UIImage *image= [self captureView:layoutController.view];
            data = UIImageJPEGRepresentation(image,0.5);
            [data writeToFile:imagePath atomically:YES];
            
            NSLog(@"filepath=%@",imagePath);
            refURL = [NSURL fileURLWithPath:imagePath];
            if ([[_appDelegate.mcManager.session connectedPeers] count]) {
                [self sendResource];
            }
        }
        
        else
        {
            if (![[DBSession sharedSession] isLinked])
            {
                
                
                
            }
            
            else
            {
                NSString *prefixString = @"Image";
                
                NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
                NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
                
                
                [self getPath];
                imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
                UIImage *image= [self captureView:layoutController.view];
                data = UIImageJPEGRepresentation(image,0.5);
                [data writeToFile:imagePath atomically:YES];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                                        stringForKey:@"AlbumName"];
                NSArray* words = [savedValue componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                NSString* nospacestring = [words componentsJoinedByString:@""];
                
                NSString *destDir = [NSString stringWithFormat: @"/%@_layout/", nospacestring];
                restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
                restClient.delegate = self;
                [restClient uploadFile:[NSString stringWithFormat:@"%@.jpg",uniqueFileName] toPath:destDir withParentRev:nil fromPath:imagePath];
                
                
            }
            
        }
        NSString *valueToSave = [[NSUserDefaults standardUserDefaults]
                                 stringForKey:@"printport"];
        NSString *valueToSave1 = [[NSUserDefaults standardUserDefaults]
                                  stringForKey:@"printip"];
        
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
                NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
                
                NSURL * url = [NSURL URLWithString:[NSString stringWithFormat: @"http://%@:%@", valueToSave1,valueToSave]];
                NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
                NSData *imageData = UIImageJPEGRepresentation([self captureView:layoutController.view], 0.5);
                
                [urlRequest setHTTPMethod:@"POST"];
                [urlRequest setHTTPBody:imageData];
                
                NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest
                                                                   completionHandler:^(NSData *data5, NSURLResponse *response, NSError *error) {
                                                                       NSLog(@"Response:%@ %@\n", response, error);
                                                                       if(error == nil)
                                                                       {
                                                                           NSString * text = [[NSString alloc] initWithData: data5 encoding: NSUTF8StringEncoding];
                                                                           NSLog(@"Data = %@",text);
                                                                       }
                                                                       
                                                                   }];
                [dataTask resume];
            });
            
        });
        [CustomAlbum addNewAssetWithImage:[self captureView:layoutController.view] toAlbum:[CustomAlbum getMyAlbumWithName:strAlbumName] onSuccess:^(NSString *ImageId) {
            recentImg = ImageId;
            NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
            NSData *data3 = [NSKeyedArchiver archivedDataWithRootObject:[self captureView:layoutController.view]];
            [currentDefaults setObject:data3 forKey:@"layoutphoto"];
            
            
            
        } onError:^(NSError *error) {
            NSLog(@"probelm in saving image");
        }];
        
        
    }
    
    
    
    
    if ([string61 isEqualToString:@"14"])
    {
        VerLayOutSeven1* layoutController = [[VerLayOutSeven1 alloc] initWithNibName:@"VerLayOutSeven1" bundle:nil];
        
        
        if (bgimage == nil)
        {
            [layoutController setEventDate:eventdate];
            [layoutController setEventName:eventname];
            layoutController.view.backgroundColor = [UIColor colorWithString:background];
            // layoutController.lblEventNameRight.textColor = [UIColor colorWithString:font];
            layoutController.lblEventNameLeft.textColor = [UIColor colorWithString:font];
            //            layoutController.lblEventDateRight.textColor = [UIColor colorWithString:font];
            layoutController.lblEventDateLeft.textColor = [UIColor colorWithString:font];
            
            
            //   layoutController.lblEventNameRight.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventNameLeft.adjustsFontSizeToFitWidth = YES;
            //            layoutController.lblEventDateRight.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventDateLeft.adjustsFontSizeToFitWidth = YES;
            
            layoutController.lblEventDateLeft.transform = CGAffineTransformMakeRotation(3.14 / 2);
            layoutController.lblEventNameLeft.transform = CGAffineTransformMakeRotation(3.14 / 2);
            
        }
        
        else
        {
            //   layoutController.lblEventNameRight.hidden = YES;
            layoutController.lblEventNameLeft.hidden = YES;
            //            layoutController.lblEventDateRight.hidden = YES;
            layoutController.lblEventDateLeft.hidden = YES;
            [layoutController setIsOverlay:overlay];
            [layoutController setImage:[UIImage imageWithData:bgimage]];
        }
        
        
        
        for (int i = 0; i <= [numberofphotos intValue]; i++) {
            UIImage *img = [UIImage imageWithData:[_arrSlidshowImg objectAtIndex:i % [numberofphotos intValue]]];
            UIImageView *imgView1 = (UIImageView *) [layoutController.view viewWithTag:i + 1];
            [imgView1 setImage:img];
            
            if ([overlay isEqualToString:@"NO"])
            {
                imgView1.layer.borderWidth = 4;
                imgView1.layer.borderColor = [UIColor colorWithString:font].CGColor;
                imgView1.transform = CGAffineTransformMakeRotation(3.14 / 2);
            }
            
            
        }
        
        
        NSString * sharing = [self.record valueForKey:@"switchSharingLayout"];
        
        if ([sharing isEqual: @"YES"])
        {
            [self getPath];
            NSString *prefixString = @"Image";
            
            NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
            NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
            imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
            UIImage *image= [self captureView:layoutController.view];
            data = UIImageJPEGRepresentation(image,0.5);
            [data writeToFile:imagePath atomically:YES];
            
            NSLog(@"filepath=%@",imagePath);
            refURL = [NSURL fileURLWithPath:imagePath];
            if ([[_appDelegate.mcManager.session connectedPeers] count]) {
                [self sendResource];
            }
        }
        
        else
        {
            if (![[DBSession sharedSession] isLinked])
            {
                
                
                
            }
            
            else
            {
                NSString *prefixString = @"Image";
                
                NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
                NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
                
                
                [self getPath];
                imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
                UIImage *image= [self captureView:layoutController.view];
                data = UIImageJPEGRepresentation(image,0.5);
                [data writeToFile:imagePath atomically:YES];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                                        stringForKey:@"AlbumName"];
                NSArray* words = [savedValue componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                NSString* nospacestring = [words componentsJoinedByString:@""];
                
                NSString *destDir = [NSString stringWithFormat: @"/%@_layout/", nospacestring];
                restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
                restClient.delegate = self;
                [restClient uploadFile:[NSString stringWithFormat:@"%@.jpg",uniqueFileName] toPath:destDir withParentRev:nil fromPath:imagePath];
                
                
            }
            
        }
        NSString *valueToSave = [[NSUserDefaults standardUserDefaults]
                                 stringForKey:@"printport"];
        NSString *valueToSave1 = [[NSUserDefaults standardUserDefaults]
                                  stringForKey:@"printip"];
        
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
                NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
                
                NSURL * url = [NSURL URLWithString:[NSString stringWithFormat: @"http://%@:%@", valueToSave1,valueToSave]];
                NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
                NSData *imageData = UIImageJPEGRepresentation([self captureView:layoutController.view], 0.5);
                
                [urlRequest setHTTPMethod:@"POST"];
                [urlRequest setHTTPBody:imageData];
                
                NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest
                                                                   completionHandler:^(NSData *data5, NSURLResponse *response, NSError *error) {
                                                                       NSLog(@"Response:%@ %@\n", response, error);
                                                                       if(error == nil)
                                                                       {
                                                                           NSString * text = [[NSString alloc] initWithData: data5 encoding: NSUTF8StringEncoding];
                                                                           NSLog(@"Data = %@",text);
                                                                       }
                                                                       
                                                                   }];
                [dataTask resume];
            });
            
        });
        [CustomAlbum addNewAssetWithImage:[self captureView:layoutController.view] toAlbum:[CustomAlbum getMyAlbumWithName:strAlbumName] onSuccess:^(NSString *ImageId) {
            recentImg = ImageId;
            NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
            NSData *data3 = [NSKeyedArchiver archivedDataWithRootObject:[self captureView:layoutController.view]];
            [currentDefaults setObject:data3 forKey:@"layoutphoto"];
            
            
            
        } onError:^(NSError *error) {
            NSLog(@"probelm in saving image");
        }];
        
        
    }
    
    
    
    
    
    if ([string61 isEqualToString:@"15"])
    {
        VerLayOutEight1* layoutController = [[VerLayOutEight1 alloc] initWithNibName:@"VerLayOutEight1" bundle:nil];
        
        
        if (bgimage == nil)
        {
            [layoutController setEventDate:eventdate];
            [layoutController setEventName:eventname];
            layoutController.view.backgroundColor = [UIColor colorWithString:background];
            // layoutController.lblEventNameRight.textColor = [UIColor colorWithString:font];
            layoutController.lblEventNameLeft.textColor = [UIColor colorWithString:font];
            //            layoutController.lblEventDateRight.textColor = [UIColor colorWithString:font];
            layoutController.lblEventDateLeft.textColor = [UIColor colorWithString:font];
            
            
            //   layoutController.lblEventNameRight.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventNameLeft.adjustsFontSizeToFitWidth = YES;
            //            layoutController.lblEventDateRight.adjustsFontSizeToFitWidth = YES;
            layoutController.lblEventDateLeft.adjustsFontSizeToFitWidth = YES;
            
            layoutController.lblEventDateLeft.transform = CGAffineTransformMakeRotation(3.14);
            layoutController.lblEventNameLeft.transform = CGAffineTransformMakeRotation(3.14);
            
        }
        
        else
        {
            //   layoutController.lblEventNameRight.hidden = YES;
            layoutController.lblEventNameLeft.hidden = YES;
            //            layoutController.lblEventDateRight.hidden = YES;
            layoutController.lblEventDateLeft.hidden = YES;
            [layoutController setIsOverlay:overlay];
            [layoutController setImage:[UIImage imageWithData:bgimage]];
        }
        
        
        
        for (int i = 0; i <= [numberofphotos intValue]; i++) {
            UIImage *img = [UIImage imageWithData:[_arrSlidshowImg objectAtIndex:i % [numberofphotos intValue]]];
            UIImageView *imgView1 = (UIImageView *) [layoutController.view viewWithTag:i + 1];
            [imgView1 setImage:img];
            
            if ([overlay isEqualToString:@"NO"])
            {
                imgView1.layer.borderWidth = 4;
                imgView1.layer.borderColor = [UIColor colorWithString:font].CGColor;
                imgView1.transform = CGAffineTransformMakeRotation(3.14 / 2);
            }
            
            
        }
        
        
        
        
        NSString * sharing = [self.record valueForKey:@"switchSharingLayout"];
        
        if ([sharing isEqual: @"YES"])
        {
            [self getPath];
            NSString *prefixString = @"Image";
            
            NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
            NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
            imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
            UIImage *image= [self captureView:layoutController.view];
            data = UIImageJPEGRepresentation(image,0.5);
            [data writeToFile:imagePath atomically:YES];
            
            NSLog(@"filepath=%@",imagePath);
            refURL = [NSURL fileURLWithPath:imagePath];
            if ([[_appDelegate.mcManager.session connectedPeers] count]) {
                [self sendResource];
            }
        }
        
        else
        {
            if (![[DBSession sharedSession] isLinked])
            {
                
                
                
            }
            
            else
            {
                NSString *prefixString = @"Image";
                
                NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
                NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
                
                
                [self getPath];
                imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
                UIImage *image= [self captureView:layoutController.view];
                data = UIImageJPEGRepresentation(image,0.5);
                [data writeToFile:imagePath atomically:YES];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                                        stringForKey:@"AlbumName"];
                NSArray* words = [savedValue componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                NSString* nospacestring = [words componentsJoinedByString:@""];
                
                NSString *destDir = [NSString stringWithFormat: @"/%@_layout/", nospacestring];
                restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
                restClient.delegate = self;
                [restClient uploadFile:[NSString stringWithFormat:@"%@.jpg",uniqueFileName] toPath:destDir withParentRev:nil fromPath:imagePath];
                
                
            }
            
        }
        
        NSString *valueToSave = [[NSUserDefaults standardUserDefaults]
                                 stringForKey:@"printport"];
        NSString *valueToSave1 = [[NSUserDefaults standardUserDefaults]
                                  stringForKey:@"printip"];
        
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
                NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
                
                NSURL * url = [NSURL URLWithString:[NSString stringWithFormat: @"http://%@:%@", valueToSave1,valueToSave]];
                NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
                NSData *imageData = UIImageJPEGRepresentation([self captureView:layoutController.view], 0.5);
                
                [urlRequest setHTTPMethod:@"POST"];
                [urlRequest setHTTPBody:imageData];
                
                NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest
                                                                   completionHandler:^(NSData *data5, NSURLResponse *response, NSError *error) {
                                                                       NSLog(@"Response:%@ %@\n", response, error);
                                                                       if(error == nil)
                                                                       {
                                                                           NSString * text = [[NSString alloc] initWithData: data5 encoding: NSUTF8StringEncoding];
                                                                           NSLog(@"Data = %@",text);
                                                                       }
                                                                       
                                                                   }];
                [dataTask resume];
            });
            
        });
        [CustomAlbum addNewAssetWithImage:[self captureView:layoutController.view] toAlbum:[CustomAlbum getMyAlbumWithName:strAlbumName] onSuccess:^(NSString *ImageId) {
            recentImg = ImageId;
            NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
            NSData *data3 = [NSKeyedArchiver archivedDataWithRootObject:[self captureView:layoutController.view]];
            [currentDefaults setObject:data3 forKey:@"layoutphoto"];
            
            
            
        } onError:^(NSError *error) {
            NSLog(@"probelm in saving image");
        }];
        
        
    }
    
    
    
    
    
    return imgLayout;
}



//WATERMARK

- (UIImage*) combineImage:(UIImage*)img {
    
    UIImage *imgWM = [UIImage imageNamed:@"watermark.png"];
    
    CGSize newSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height);
    UIGraphicsBeginImageContext( newSize );
    
    // Use existing opacity as is
    [img drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    
    // Apply supplied opacity if applicable
    [imgWM drawInRect:CGRectMake(0,0,newSize.width,newSize.height) blendMode:kCGBlendModeNormal alpha:0.8];
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}



//SHARING STATION//
- (IBAction)disconnect:(id)sender {
    
    [_appDelegate.mcManager.session disconnect];
    _nameTxt.enabled = YES;
    
    [_appDelegate.arrConnectedDevices removeAllObjects];
    [_connections reloadData];
    
}


#pragma mark - UIImageView
-(void)setRoundedView:(UIImageView *)roundedView toDiameter:(float)newSize;
{
    CGPoint saveCenter = roundedView.center;
    CGRect newFrame = CGRectMake(roundedView.frame.origin.x, roundedView.frame.origin.y, newSize, newSize);
    roundedView.frame = newFrame;
    roundedView.layer.cornerRadius = newSize / 2.0;
    roundedView.center = saveCenter;
    
    roundedView.clipsToBounds = YES;
}


#pragma mark - Send/Receive
-(void)didStartReceivingResourceWithNotification:(NSNotification *)notification{
//    NSProgress *progress = [[notification userInfo] objectForKey:@"progress"];
//    _receive_progress=progress;
//    dispatch_async(dispatch_get_main_queue(), ^{
//        NSLog(@"receiving...");
//        [_receive_progress addObserver:self
//                            forKeyPath:@"fractionCompleted"
//                               options:NSKeyValueObservingOptionNew
//                               context:NULL];
//        receive_hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//        //  receive_hud.mode = MBProgressHUDModeAnnularDeterminate;
//        receive_hud.labelText=@"Receiving...";
//        
//    });
    
}


-(void)updateReceivingProgressWithNotification:(NSNotification *)notification{
    
}


-(void)didFinishReceivingResourceWithNotification:(NSNotification *)notification
{
    NSLog(@"DID FINISH RECEIVING");
    
    NSDictionary *dict = [notification userInfo];
    
    NSURL *localURL = [dict objectForKey:@"localURL"];
    //NSString *resourceName = [dict objectForKey:@"resourceName"];
    
    @try{
        [_receive_progress removeObserver:self
                               forKeyPath:@"fractionCompleted"
                                  context:NULL];
    }
    @catch (NSException *e) {
        NSLog(@"error %@",e);
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    });
    
    [self performSelectorOnMainThread:@selector(saveData:) withObject:localURL waitUntilDone:NO];
}


-(void)sendResource{
    
    [_assetSelected reloadData];
    _send_progress = [_appDelegate.mcManager.session
                      sendResourceAtURL:refURL
                      withName:@"data"
                      toPeer:[[_appDelegate.mcManager.session connectedPeers] objectAtIndex:0]
                      withCompletionHandler:^(NSError *error) {
                      }];
    
    
}



-(void)saveData:(NSURL *)url
{
    
    NSData *data4 = [NSData dataWithContentsOfURL:url];
    UIImage *test4 = [UIImage imageWithData:data4];
    
    if (test4)
    {
        NSLog(@"It's an image");
        UIImage *img= [UIImage imageWithData:data4];
        UIImageWriteToSavedPhotosAlbum(img, self, nil, nil);
    }
    else
    {
        NSLog(@"It's a movie");
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsPath = [paths objectAtIndex:0]; //Get the docs directory
        NSString *filePath = [documentsPath stringByAppendingPathComponent:@"video.mov"]; //Add the file name
    
        NSLog(@"video is %@",filePath);
        
        [data writeToFile:filePath atomically:YES]; //Write the file
        
        if (UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(filePath))
        {
            UISaveVideoAtPathToSavedPhotosAlbum(filePath, self, @selector(video:didFinishSavingWithError:contextInfo:), nil);
        }
    }
    
}

-(void)video:(NSString *)videoPath didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    if (error)
    {
        NSLog(@"Finished with error: %@", error);
 
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: @"Error saving video"
                              message: [NSString stringWithFormat:@"%@",error]
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
}


-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if ([identifier isEqualToString:@"chat"] || [identifier isEqualToString:@"send_files"])
    {
        if (![[NSUserDefaults standardUserDefaults]boolForKey:@"HyperConnectProf"])
        {
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle: @"You have to unlock this content."
                                  message: @"Go to Settings to unlock all content."
                                  delegate: nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
            [alert show];
            
            return NO;
        }
    }
    
    return YES;
}






@end


