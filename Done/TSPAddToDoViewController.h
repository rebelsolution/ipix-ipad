//
//  TSPAddToDoViewController.h
//  Done
//
//  Created by Bart Jacobs on 24/07/14.
//  Copyright (c) 2014 Tuts+. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "RzColorPickerView.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "Rectangle.h"
#import <MessageUI/MessageUI.h>
#import "CustomScrollView.h"
#import "LayoutView.h"
#import <CoreData/CoreData.h>
#import "FCAlertView.h"


@interface TSPAddToDoViewController : UIViewController <FCAlertViewDelegate, UIAlertViewDelegate, UIGestureRecognizerDelegate,MFMailComposeViewControllerDelegate,UITextFieldDelegate, UITextViewDelegate,UIActionSheetDelegate>
{

}


@property (nonatomic) NSInteger modeWorking;

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSManagedObject *record;

@property (strong, nonatomic) UIScrollView* scrollviewlayout;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *buttonborders;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *buttonborders1;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *instbut;
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *viewborders;
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *lineseparator;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *labelcolors;
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *lineseparator1;
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *labelcolors1;
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *lineseparator2;
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *labelcolors2;
@property (strong, nonatomic) IBOutletCollection(UITextField) NSArray *textfields;
@property (strong, nonatomic) IBOutletCollection(UISwitch) NSArray *switches;
@property (strong, nonatomic) IBOutletCollection(UIStepper) NSArray *stepper;
@property (nonatomic) BOOL isPhotoboothViewPrint;
@property (strong, nonatomic)UIColor *LayoutBackgroundColor;
@property (strong, nonatomic)UIColor *LayoutFontBorderColor;
@property (strong, nonatomic)UIColor *PrintBackgroundColor;
@property (strong, nonatomic)UIColor *PrintFontBorderColor;
@property (strong, nonatomic)NSString *emailmes;
@property (strong, nonatomic)NSString *emailsubj;
@property (nonatomic)NSString *saveFormat;


@property (nonatomic, strong)NSString  *numberOfPhotosLayout;
@property (nonatomic) NSInteger numberAddPhotoAnimateGif;

@end

