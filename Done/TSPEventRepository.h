//
//  TSPEventRepository.h
//  iPIX Social
//
//  Created by Mac Mini on 12/30/16.
//  Copyright © 2016 Tuts+. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TSPEventRepository : NSObject

@property (nonatomic) BOOL isSmallCustomLayout;
@property (nonatomic) BOOL isSmallCustomLayoutH;
@property (nonatomic) BOOL isLargeCustomLayoutH;
@property (nonatomic) BOOL isLargeCustomLayout;
@property (nonatomic, strong) NSString* eventID;
@property (nonatomic, strong) NSMutableArray *customLayoutArray;
@property (nonatomic) NSInteger customLayoutCount;
@property (nonatomic, assign) BOOL isSplipLayout;
@property (nonatomic, strong) NSData *customLayoutBackground;

- (NSDictionary*)getEventData;
- (id)initWithDict:(NSDictionary*)dict;

@end
