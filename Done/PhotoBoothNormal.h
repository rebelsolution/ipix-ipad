//
//  PhotoBoothNormal.h
//  iSIT
//
//  Created by Marc Matteo on 10/22/16.
//  Copyright © 2016 Marc Matteo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "RzColorPickerView.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <MessageUI/MessageUI.h>
#import <AudioToolbox/AudioToolbox.h>
#import <CoreData/CoreData.h>
#import <CFNetwork/CFNetwork.h>
#import "KVNProgress.h"
#import "Reachability.h"
#import "SimplePingHelper.h"
#import <DropboxSDK/DropboxSDK.h>
#import <LumeCubeSDK/LumeCubeSDK.h>
#import <MultipeerConnectivity/MultipeerConnectivity.h>
@import AVFoundation;
@class PhotoBoothNormal;

NSInteger timerInt;
NSTimer *countdownTimer;
NSInteger photocount;
NSInteger previewcount;
NSInteger videotimerInt;
NSTimer *videoTimer;
NSInteger videocount;
UIImage *vectorImage;
CAShapeLayer *circleLayer;
NSInteger remainingphoto;
NSInteger remainingphoto1;
NSMutableArray * photostaken;
NSMutableArray * texttaken;
BOOL rotated;

@interface PhotoBoothNormal : UIViewController <MCBrowserViewControllerDelegate, UITableViewDataSource,UITableViewDelegate,DBRestClientDelegate,DBSessionDelegate, AVCaptureVideoDataOutputSampleBufferDelegate, AVCaptureAudioDataOutputSampleBufferDelegate, AVCaptureFileOutputRecordingDelegate, UIGestureRecognizerDelegate,MFMailComposeViewControllerDelegate,UITextFieldDelegate,UIActionSheetDelegate,NSURLConnectionDelegate, NSURLConnectionDataDelegate>

{
    PhotoBoothNormal *box;
    IBOutlet UIButton *imgBtn;
    IBOutlet UIButton *vidoBtn;
    IBOutlet UIButton *loginBtn;
    IBOutlet UIButton *outBtn;
    UIView *alertViewContents;
}

-(IBAction)image:(id)sender;
-(IBAction)video:(id)sender;
-(IBAction)login:(id)sender;
-(IBAction)logout:(id)sender;

@property(retain, nonatomic) NSArray *array;
@property (retain, nonatomic) IBOutlet UILabel *power;
@property (retain, nonatomic) IBOutlet UITableView *tableView;
@property(strong , nonatomic) NSArray *data2;
@property (retain, nonatomic) UIImage *alertImage;
@property (strong, nonatomic)UIColor *LayoutBackgroundColor;
@property (strong, nonatomic)UIColor *LayoutFontBorderColor;
@property (strong, nonatomic)UIColor *PrintBackgroundColor;
@property (strong, nonatomic)UIColor *PrintFontBorderColor;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *buttonborders;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *buttoneffects;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *buttoneffects1;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *starteffects;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *buttonborders1;
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *look;
@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *screenbackgroundcolor;
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *viewborders;
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *lineborder;
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *effectsborder;
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *lineseparator;
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *videotimerbg;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *labelcolors;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *labelcolors1;
@property (strong, nonatomic) IBOutletCollection(UITextField) NSArray *textfields;
@property (strong, nonatomic) IBOutletCollection(UISwitch) NSArray *switches;
@property (strong, nonatomic) IBOutletCollection(UIStepper) NSArray *stepper;
@property (strong, nonatomic) IBOutletCollection(UISlider) NSArray *sliders;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *timerLabel;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *videoLabel;
@property (nonatomic, strong) NSMutableArray *arrSlidshowImg;
@property (nonatomic) BOOL button;

@property (strong, nonatomic) IBOutletCollection(UISegmentedControl) NSArray *segmented;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSManagedObject *record;

- (UIImage *)makeLayoutImage;
@property(strong, nonatomic) IBOutlet UIImageView *captureImage1;
@property(nonatomic, strong) NSURL *urlGif;
@property(nonatomic, strong) UIImage *individualphoto;
@property(nonatomic, strong) UIImage *imgLayout;
@property(strong, nonatomic) IBOutlet UIButton *displayphoto;
@property (retain, nonatomic) DBRestClient *restClient;
@property (retain, nonatomic) DBRestClient *restClient1;

@property (retain, nonatomic) NSArray *Uppaths;
@property (retain, nonatomic) NSString *documentsDirectory,*imagePath ;
@property (retain, nonatomic) NSData *data;
@property (retain, nonatomic) UIViewController *currentViewCtrl;

-(void)DropboxuploadWithMultiImage:(NSArray *)ImageArray;

-(void)DropboxuploadWithMultiVideo:(NSArray *)multiVideoURLArray;

-(void)loginWithViewController:(UIViewController *)selfViewCtrl;

-(void)logOut;
- (instancetype)initWithRequestedPhotoSettings:(AVCapturePhotoSettings *)requestedPhotoSettings willCapturePhotoAnimation:(void (^)())willCapturePhotoAnimation completed:(void (^)( PhotoBoothNormal *photoCaptureDelegate ))completed;

@property (nonatomic, readonly) AVCapturePhotoSettings *requestedPhotoSettings;



//SHARING

- (IBAction)browse:(id)sender;
- (IBAction)disconnect:(id)sender;
- (IBAction)searchDevices:(id)sender;

@property (weak, nonatomic) IBOutlet UIImageView *connectionStatus;
@property (weak, nonatomic) IBOutlet UITableView *assetSelected;
@property (strong, nonatomic) IBOutlet UITableView *connections;
@property (strong, nonatomic) IBOutlet UITextField *nameTxt;
@property (strong, nonatomic) IBOutlet UIButton *btnDisconnect;
@property (strong, nonatomic) IBOutlet UILabel *statusLabel;


@end
