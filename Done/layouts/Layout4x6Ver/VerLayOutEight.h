

#import <UIKit/UIKit.h>

@interface VerLayOutEight : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *lblEventNameLeft;
@property (weak, nonatomic) IBOutlet UILabel *lblEventNameRight;
@property (weak, nonatomic) IBOutlet UILabel *lblEventDateLeft;
@property (weak, nonatomic) IBOutlet UILabel *lblEventDateRight;
@property (weak, nonatomic) IBOutlet UIImageView *imgBackground;
@property (weak, nonatomic) IBOutlet UIImageView *imgForeground;
@property (weak, nonatomic) IBOutlet UIView *viewLayout;

@property (nonatomic, strong) NSString* eventName;
@property (nonatomic, strong) NSString* eventDate;
@property (nonatomic) NSString* isOverlay;
@property (nonatomic) UIImage* image;

@end
