
#import "VerLayoutTwo.h"

@interface VerLayOutTwo ()

@end

@implementation VerLayOutTwo

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _lblEventDateLeft.text = _eventDate;
    _lblEventNameLeft.text = _eventName;
 
    _lblEventDateRight.text = _eventDate;
    _lblEventNameRight.text = _eventName;
    
    if ([_isOverlay isEqualToString:@"YES"])
    {
        _imgForeground.image =  _image;
    }
    else if ([_isOverlay isEqualToString:@"NO"])
    {
        _imgBackground.image =  _image;
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
