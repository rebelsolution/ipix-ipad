//
//  TSPEventRepository.m
//  iPIX Social
//
//  Created by Mac Mini on 12/30/16.
//  Copyright © 2016 Tuts+. All rights reserved.
//

#import "TSPEventRepository.h"

@implementation TSPEventRepository

- (id)init
{
    _customLayoutArray = [[NSMutableArray alloc] init];

    return self;
}

- (NSDictionary*)getEventData
{
    NSMutableDictionary* resultDict = [[NSMutableDictionary alloc] init];
    
    [resultDict setObject:[NSString stringWithFormat:@"%@", _isSmallCustomLayout ? @"YES" : @"NO"]forKey:@"isSmallCustomLayout"];
    [resultDict setObject:[NSString stringWithFormat:@"%@", _isSmallCustomLayoutH ? @"YES" : @"NO"]forKey:@"isSmallCustomLayoutH"];
    [resultDict setObject:[NSString stringWithFormat:@"%@", _isLargeCustomLayoutH ? @"YES" : @"NO"] forKey:@"isLargeCustomLayoutH"];
    [resultDict setObject:[NSString stringWithFormat:@"%@", _isLargeCustomLayout ? @"YES" : @"NO"] forKey:@"isLargeCustomLayout"];
    [resultDict setObject:_eventID forKey:@"eventID"];
    [resultDict setObject:[NSString stringWithFormat:@"%ld", (long)_customLayoutCount] forKey:@"customLayoutCount"];
    [resultDict setObject:_customLayoutArray forKey:@"customLayoutArray"];
    
    return resultDict;
}

- (void)encodeWithCoder:(NSCoder *)coder
{
    [coder encodeObject:_customLayoutArray forKey:@"customLayoutArray"];
    [coder encodeObject:[NSString stringWithFormat:@"%ld", (long)_customLayoutCount] forKey:@"CustomLayoutCount"];
    [coder encodeObject:[NSNumber numberWithBool:_isSmallCustomLayout] forKey:@"IsSmallLayout"];
    [coder encodeObject:[NSNumber numberWithBool:_isSmallCustomLayoutH] forKey:@"isSmallCustomLayoutH"];
    [coder encodeObject:[NSNumber numberWithBool:_isLargeCustomLayoutH] forKey:@"isLargeCustomLayoutH"];
    [coder encodeObject:[NSNumber numberWithBool:_isLargeCustomLayout] forKey:@"isLargeCustomLayout"];
    [coder encodeObject:_eventID forKey:@"eventID"];
}

- (id)initWithCoder:(NSCoder *)coder
{
    self = [super init];

    if (self)
    {
        _customLayoutArray = [coder decodeObjectForKey:@"customLayoutArray"];
        _eventID = [coder decodeObjectForKey:@"eventID"];
        _customLayoutCount = [[coder decodeObjectForKey:@"CustomLayoutCount"] integerValue];
        _isSmallCustomLayout = [[coder decodeObjectForKey:@"IsSmallLayout"] boolValue];
        _isSmallCustomLayoutH = [[coder decodeObjectForKey:@"isSmallCustomLayoutH"] boolValue];
        _isLargeCustomLayoutH = [[coder decodeObjectForKey:@"isLargeCustomLayoutH"] boolValue];
        _isLargeCustomLayout = [[coder decodeObjectForKey:@"isLargeCustomLayout"] boolValue];
    }
    
    return self;
}

- (id)initWithDict:(NSDictionary *)dict
{
    self = [super init];
    
    if (self)
    {
        _customLayoutArray = [dict valueForKey:@"customLayoutArray"];
        _eventID = [dict valueForKey:@"eventID"];
        _customLayoutCount = [[dict valueForKey:@"CustomLayoutCount"] integerValue];
        NSLog(@"IS SMAILL: %@", [dict valueForKey:@"isSmallCustomLayout"]);
        
        _isSmallCustomLayout = [[dict valueForKey:@"isSmallCustomLayout"] boolValue];
        _isSmallCustomLayoutH = [[dict valueForKey:@"isSmallCustomLayoutH"] boolValue];
        _isLargeCustomLayoutH = [[dict valueForKey:@"isLargeCustomLayoutH"] boolValue];

    }
    
    return self;
}

@end
