
@import AVFoundation;

#import <Photos/Photos.h>
#import "TSPViewController.h"
#import "CNPPopupController.h"
#import "SharingNormal.h"
#import "AVCamManualPreviewView.h"
#import "PHPhotoLibrary+CustomPhotoAlbum.h"
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <CoreMedia/CoreMedia.h>
#import <CoreVideo/CoreVideo.h>
#import <QuartzCore/QuartzCore.h>
#import <CoreImage/CoreImage.h>
#import <ImageIO/ImageIO.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "CustomAlbum.h"
#import "ACSDefaultsUtil.h"
#import "ACSPinController.h"
#import <QuartzCore/QuartzCore.h>
#import <GLKit/GLKit.h>
#import <Social/Social.h>
#import "SendGrid.h"
#import "SendGridEmail.h"
#import "SendGridEmailAttachment.h"
#import "AMPopTip.h"
#import <TwitterKit/TwitterKit.h>
#import "FHSTwitterEngine.h"
#import "WebViewVC.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "AFHTTPRequestOperationManager.h"
#import "TSPPhotoRepository.h"
#import "MWCommon.h"
#import "SDImageCache.h"
#import "MBProgressHUD.h"
#import "TSPAppDelegate.h"



int arrayValue;
int value;
@import AVKit;

#define DEGREES_TO_RADIANS(degrees) ((degrees) * (M_PI / 180.0))
static void *CapturingStillImageContext = &CapturingStillImageContext;
static void *RecordingContext = &RecordingContext;
static void *SessionRunningAndDeviceAuthorizedContext = &SessionRunningAndDeviceAuthorizedContext;

static void *FocusModeContext = &FocusModeContext;
static void *ExposureModeContext = &ExposureModeContext;
static void *WhiteBalanceModeContext = &WhiteBalanceModeContext;
static void *LensPositionContext = &LensPositionContext;
static void *ExposureDurationContext = &ExposureDurationContext;
static void *ISOContext = &ISOContext;
static void *ExposureTargetOffsetContext = &ExposureTargetOffsetContext;
static void *DeviceWhiteBalanceGainsContext = &DeviceWhiteBalanceGainsContext;

@interface SharingNormal ()< FHSTwitterEngineAccessTokenDelegate, CNPPopupControllerDelegate, ACSPinControllerDelegate, AVCaptureFileOutputRecordingDelegate, UITextFieldDelegate, MFMailComposeViewControllerDelegate, AVAudioRecorderDelegate>

{
    UIImage *_photo;
    NSString *recentImg;
    NSString *emailmessage1;
    NSString *emailsubject1;
    
    
    
    
    
    //LAYOUTSEND
    IBOutlet UIColor *fontcolor;
    IBOutlet UIColor *backgroundcolor1;
    
    IBOutlet NSString *TextIndividual;
    IBOutlet NSString *AlbumName;
    IBOutlet UIImageView *imgView;
    IBOutlet UIImage *welcomeimage;
    IBOutlet UIImage *getreadyimage;
    IBOutlet UIImage* animatedlayoutimage;
    IBOutlet UIImage* closedimage;
    IBOutlet UIImage* countdownimage;
    IBOutlet UIImage* customimage;
    IBOutlet UIImage* effectsimage;
    IBOutlet UIImage* emailimage;
    IBOutlet UIImage* finalimage;
    IBOutlet UIImage* greenscreenimage;
    IBOutlet UIImage* photoboothlayoutimage;
    IBOutlet UIImage* remainingimage;
    IBOutlet UIImage*  videoimage;
    AVAudioPlayer *_audioPlayer;
    AVAudioPlayer *_audioPlayer1;
    IBOutlet UISwitch *testshot;
    IBOutlet UISwitch *pass;
    IBOutlet UIButton *testbutton;
    IBOutlet UIButton *recordyes;
    IBOutlet UIButton *recordno;
    IBOutlet UIButton *recordbutton;
    IBOutlet UIButton *stopbutton;
    
    IBOutlet UIButton *connecttobooth;
    IBOutlet UIButton *closebooth;
    
    
     IBOutlet UIView *press1;
    IBOutlet UIView *social;
    IBOutlet UIView *socialview;
    
    IBOutlet UIButton *facebook;
    IBOutlet UIButton *twitter;
    IBOutlet UIButton *mail;
    IBOutlet UIButton *texting;
    IBOutlet UIButton *touchtostart;
    IBOutlet UIButton *backout;
    IBOutlet UIButton *soc;
    IBOutlet UIImageView *closed;
    IBOutlet UIImageView *welcome;
    IBOutlet UIImageView *effects;
    IBOutlet UIImageView *video;
    IBOutlet UIImageView *email;
    IBOutlet UIImageView *final;
    IBOutlet UIImageView *countdown;
    IBOutlet UIImageView *remaining;
    IBOutlet UIImageView *getready;
    IBOutlet UIImageView *printlayoutbackground;
    IBOutlet UIImageView *customlayoutbackground;
    IBOutlet UIImageView *green;
    IBOutlet UIView *remainbord;
    IBOutlet UIView *welcomescreen1;
    IBOutlet UIView *mainadmin;
    IBOutlet UIView *mainwelcome;
    IBOutlet UIView *mainready;
    IBOutlet UIView * mainclosed;
    IBOutlet UIView * maincountdown;
    IBOutlet UIView * maincountdown1;
    IBOutlet UIView * maincountdownbg;
    IBOutlet UIView * maincountdownbg1;
    IBOutlet UIView * maineffects;
    IBOutlet UIView *testing;
    IBOutlet UIView * maineffectsbg;
    IBOutlet UIView * maineffectsbg1;
    IBOutlet UIView *mainemail;
    IBOutlet UIView *mainfinal;
    IBOutlet UIView * mainremaining;
    IBOutlet UIView * mainvideo;
    IBOutlet UIView * countdownlook;
    IBOutlet UIView * videotimerview;
    IBOutlet UIView * welcomevideohide;
    IBOutlet UIView * welcomevideowshow;
    IBOutlet UIView *lookhere;
    IBOutlet UIView *lookhere1;
    IBOutlet UIView *press;
    IBOutlet UIView *viewtest;
    IBOutlet UIView *captured;
    IBOutlet UIView *alleffects;
    IBOutlet UIView *bwvin;
    IBOutlet UIView*bwsep;
    IBOutlet UIView *bw;
    IBOutlet UIView*vinsep;
    IBOutlet UIView *vin;
    IBOutlet UIView *sep;
    IBOutlet UIView *col;
    
    
    
    IBOutlet UISwitch *connect;
    IBOutlet UIView *pinclosed;
    
    IBOutlet UITextField *textnumber;
    IBOutlet UITextField *emailaddress1;
    IBOutlet UITextField *printip;
    IBOutlet UITextField *printport;
    
    IBOutlet UIButton *SEPBG;
    IBOutlet UIButton *VINBG;
    IBOutlet UIButton *COLORBG;
    IBOutlet UIButton *BWBG;
    IBOutlet UIButton *skippingsocial;
    
    IBOutlet UIButton *sharestation;
    IBOutlet UIButton *photoButtoncolor;
    
    IBOutlet UIStackView *effectsstack;
    IBOutlet UIView *welcomescreen;
    IBOutlet UIView *closedscreen;
    IBOutlet UIView *effectsscreen;
    IBOutlet UIView *effectsscreen1;
    IBOutlet UIView *remainingscreen;
    IBOutlet UIView *videoscreen;
    IBOutlet UIView *hidevideoscreen;
    IBOutlet UIView *emailscreen;
    IBOutlet UIView *finalscreen;
    IBOutlet UIView *finalscreen1;
    IBOutlet UIView *finalscreen2;
    IBOutlet UIView *countdownscreen;
    IBOutlet UIView *countdownscreen1;
    IBOutlet UIView *getreadyscreen;
    IBOutlet UIView *homescreen;
    IBOutlet UIView *colorbuttons;
    IBOutlet UIView *video5;
    IBOutlet UIView *video2;
    IBOutlet UIView *social5;
    IBOutlet UILabel *videolab1;
    IBOutlet UILabel *sociallab1;
    IBOutlet UILabel *lblRemainPhoto1;
    IBOutlet UILabel *lblRemainPhoto2;
    IBOutlet UILabel *timerview;
    IBOutlet UILabel *videotimerlabel;
    __weak IBOutlet UIView *camerascreenview;
    
    IBOutlet UITextField *txtFldEmail;
    
    //BOOLS//
    BOOL isRecording;
    BOOL isTaking;
    BOOL captureImage;
    BOOL captureImage3;
    
    //RANDOM//
    CIContext *ciContext;
    CALayer *videoLayer;
    NSString *selectedFilterName;
    
    NSString *watermark;
}

@property (nonatomic, weak) IBOutlet UIButton *recordButton1;
@property (nonatomic, readwrite) AVCapturePhotoSettings *requestedPhotoSettings;
@property (nonatomic) void (^willCapturePhotoAnimation)();
@property (nonatomic) void (^completed)(SharingNormal *photoCaptureDelegate);
@property (nonatomic, strong) AMPopTip *popTip;
@property (nonatomic) NSData *jpegPhotoData;
@property (nonatomic) NSData *dngPhotoData;
@property (nonatomic, weak) IBOutlet NSString *emailstring;
@property (nonatomic, strong) CNPPopupController *popupController;

@property (strong, nonatomic) UIActivityIndicatorView * activityAdd;
@property (weak, nonatomic) IBOutlet UIButton *btnAddPhoto;
@property (weak, nonatomic) IBOutlet UIButton *btnViewStatus;
@property (weak, nonatomic) IBOutlet UIButton *btnClear;
@property (weak, nonatomic) IBOutlet UIButton *btnPauseUploads;
@property (weak, nonatomic) IBOutlet UILabel *count;
@property (weak, nonatomic) IBOutlet UILabel *state;
@property (weak, nonatomic) IBOutlet UILabel *lblMessage;

//CAMERA SETTINGS//
@property (nonatomic, strong) PHPhotoLibrary* library;
@property (nonatomic,strong) UILongPressGestureRecognizer *lpgr;

@property (nonatomic, weak) IBOutlet UIView *printsettings;
@property (nonatomic, weak) IBOutlet UIView *printserver;
@property (nonatomic, weak) IBOutlet UIView *airprint;
@property (nonatomic, weak) IBOutlet UISegmentedControl *rawControl;

// Session management
@property (nonatomic, strong) TSPAppDelegate *appDelegate;
@property (nonatomic, strong) ACSPinController *pinController;

@property (nonatomic) dispatch_queue_t sessionQueue; // Communicate with the session and other session objects on this queue.
@property (nonatomic) AVCaptureSession *session;
@property (nonatomic) AVCaptureDeviceInput *videoDeviceInput;
@property (nonatomic) AVCaptureDevice *videoDevice;
@property (nonatomic) AVCaptureMovieFileOutput *movieFileOutput;
@property (nonatomic) AVCaptureVideoDataOutput *videoDataOutput;
@property (nonatomic) AVCaptureStillImageOutput *stillImageOutput;
@property (nonatomic) UIBackgroundTaskIdentifier backgroundRecordingID;
@property (nonatomic, getter = isDeviceAuthorized) BOOL deviceAuthorized;
@property (nonatomic, readonly, getter = isSessionRunningAndDeviceAuthorized) BOOL sessionRunningAndDeviceAuthorized;
@property (nonatomic) BOOL lockInterfaceRotation;
@property (nonatomic) id runtimeErrorHandlingObserver;
@property(nonatomic) BOOL isTesting;
@property (nonatomic) AVCapturePhotoOutput *photoOutput;
@property (nonatomic) ACSDefaultsUtil *defaultsUtil;
@property (nonatomic) NSMutableDictionary<NSNumber *, SharingNormal *> *inProgressPhotoCaptureDelegates;
@property (nonatomic) AVPlayer *player;
@property (nonatomic) AVPlayer *finalplayer;
@property (strong) AVPlayerLayer *avPlayerLayer;
@property (strong) AVPlayerItem *currentItem;
@property (nonatomic) AVPlayerViewController *controller;
//CAMERA SETTINGS END//

-(void)peerDidChangeStateWithNotification:(NSNotification *)notification;

-(void)didStartReceivingResourceWithNotification:(NSNotification *)notification;
-(void)updateReceivingProgressWithNotification:(NSNotification *)notification;
-(void)didFinishReceivingResourceWithNotification:(NSNotification *)notification;

@end

@implementation SharingNormal

NSProgress *_send_progress;
NSProgress *_receive_progress;
MBProgressHUD *send_hud;
MBProgressHUD *receive_hud;
NSURL *refURL;
//BOOL isVideo1=NO;

NSMutableArray *convertedFileName;
NSMutableArray *assetsSelected;
NSData *imagetrans;

@synthesize restClient1;
@synthesize restClient;
@synthesize Uppaths;
@synthesize documentsDirectory;
@synthesize imagePath;
@synthesize data;
@synthesize buttoneffects1;
@synthesize lineborder;
@synthesize effectsborder;
@synthesize timerLabel;
@synthesize videoLabel;
@synthesize LayoutBackgroundColor;
@synthesize LayoutFontBorderColor;
@synthesize PrintBackgroundColor;
@synthesize PrintFontBorderColor;
@synthesize buttonborders;
@synthesize buttonborders1;
@synthesize buttoneffects;
@synthesize screenbackgroundcolor;
@synthesize look;
@synthesize viewborders;
@synthesize lineseparator;
@synthesize labelcolors;
@synthesize labelcolors1;
@synthesize textfields;
@synthesize switches;
@synthesize stepper;
@synthesize sliders;
@synthesize starteffects;
@synthesize segmented;
@synthesize captureImage1, individualphoto, imgLayout;

static UIColor* CONTROL_NORMAL_COLOR = nil;
static UIColor* CONTROL_HIGHLIGHT_COLOR = nil;
static float EXPOSURE_DURATION_POWER = 5; // Higher numbers will give the slider more sensitivity at shorter durations
static float EXPOSURE_MINIMUM_DURATION = 1.0/4000; // Limit exposure duration to a useful range
//CAMERA SETTINGS END//



+ (void)initialize
{
    CONTROL_NORMAL_COLOR = [UIColor yellowColor];
    CONTROL_HIGHLIGHT_COLOR = [UIColor colorWithRed:0.0 green:122.0/255.0 blue:1.0 alpha:1.0]; // A nice blue
}

+ (NSSet *)keyPathsForValuesAffectingSessionRunningAndDeviceAuthorized
{
    return [NSSet setWithObjects:@"session.running", @"deviceAuthorized", nil];
}

- (BOOL)isSessionRunningAndDeviceAuthorized
{
    return [[self session] isRunning] && [self isDeviceAuthorized];
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    press1.hidden=YES;
    outBtn.hidden = YES;
    loginBtn.hidden = NO;
    _appDelegate = (TSPAppDelegate *)[[UIApplication sharedApplication] delegate];
    if (![[_appDelegate mcManager]peerID])
    {
        [[_appDelegate mcManager] setupPeerAndSessionWithDisplayName:[UIDevice currentDevice].name];
        [[_appDelegate mcManager] advertiseSelf:YES];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(peerDidChangeStateWithNotification:)
                                                 name:@"MCDidChangeStateNotification"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didStartReceivingResourceWithNotification:)
                                                 name:@"MCDidStartReceivingResourceNotification"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateReceivingProgressWithNotification:)
                                                 name:@"MCReceivingProgressNotification"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didFinishReceivingResourceWithNotification:)
                                                 name:@"didFinishReceivingResourceNotification"
                                               object:nil];
    [_nameTxt setDelegate:self];
    
    _assetSelected.delegate=self;
    _assetSelected.dataSource=self;
    
    _connections.delegate=self;
    _connections.dataSource=self;
    [_connections reloadData];
    
    assetsSelected = [[NSMutableArray alloc]init];
    convertedFileName = [[NSMutableArray alloc]init];
    
    
    
    [FBSDKSettings setAppID: @"1175602815854519"];
    
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver: self selector: @selector(uploadFinish) name:@"UploadFinish" object: nil];
 
    
    self.printsettings.hidden=YES;
    
    NSUserDefaults *defaults2 = [NSUserDefaults standardUserDefaults];
    NSString* key =[defaults2 objectForKey:@"consumerkey"];
    NSString* secret =[defaults2 objectForKey:@"consumersecret"];
    if ([key isEqualToString:@""] || key == nil)
    {
        [[FHSTwitterEngine sharedEngine]permanentlySetConsumerKey:@"2QQs5ySOWwAxHnVV2i9ZnTEEM" andSecret:@"kOIcTSTepk1GTKysW1bK61BLhJHLHqApq7Ktkb68OtEA4VZPgd"];
        [[FHSTwitterEngine sharedEngine]setDelegate:self];
        [[FHSTwitterEngine sharedEngine]loadAccessToken];
    }
    
    else
    {
        [[FHSTwitterEngine sharedEngine]permanentlySetConsumerKey:[NSString stringWithFormat:@"%@",key] andSecret:[NSString stringWithFormat:@"%@",secret]];
        [[FHSTwitterEngine sharedEngine]setDelegate:self];
        [[FHSTwitterEngine sharedEngine]loadAccessToken];
        
    }
    
    // NSString* hash =[defaults2 objectForKey:@"facebookhashtag"];
    NSString* appid =[defaults2 objectForKey:@"appid"];
    
    if ([appid isEqualToString:@""])
    {
        [FBSDKSettings setAppID: @"1175602815854519"];
    }
    
    else{
        [FBSDKSettings setAppID: appid];
    }
    
    
    
    self.button=NO;
    
    
    //POPTIP//
    self.popTip = [AMPopTip popTip];
    self.popTip.font = [UIFont fontWithName:@"Avenir-Medium" size:40];
    self.popTip.edgeMargin = 5;
    self.popTip.offset = 2;
    self.popTip.edgeInsets = UIEdgeInsetsMake(0, 10, 0, 10);
    self.popTip.shouldDismissOnTap = NO;
    self.popTip.actionAnimation = AMPopTipActionAnimationNone;
    
    self.popTip.tapHandler = ^{
        NSLog(@"Tap!");
    };
    
    
    NSString * string61 = [self.record valueForKey:@"switchSendgrid"];
    NSLog(@"SENDGRID IS = %@", string61);
    
    
    NSString * string62 = [self.record valueForKey:@"switchLayout"];
    NSLog(@"LAYOUT IS = %@", string62);
    
    NSString * string63 = [self.record valueForKey:@"switchIndividual"];
    NSLog(@"LAYOUT IS = %@", string63);
    
    NSString *allowed = [self.record valueForKey:@"maxSelectionSharing"];
    photosallowed = [allowed intValue];
    NSLog(@"MAX ALLOWED %ld", (long)photosallowed);
    
    [TSPAppDelegate sharedAppDelegate].photoAllowed = photosallowed;
    
    TextIndividual = [[NSString alloc] init];
    texttaken = [[NSMutableArray alloc] init];
    // self.arrSlidshowImg = [[NSMutableArray alloc] init];
    self.photoTakens = [[NSMutableArray alloc] init];
    if([self.photoTakens count]){
        [self.photoTakens removeAllObjects];
        //    [self.arrSlidshowImg removeAllObjects];
    }
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    selectedFilterName = @"CIPhotoEffectNone";
    pass.hidden=YES;
    testshot.hidden=YES;
    //PASSCODE//
    self.defaultsUtil = [[ACSDefaultsUtil alloc] init];
    self.pinController = [[ACSPinController alloc] initWithPinServiceName:@"testservice" pinUserName:@"testuser" accessGroup:@"accesstest" delegate:self];
    self.pinController.retriesMax = 5;
    
    // Validation block for pin controller to check if entered pin is valid.
    __weak SharingNormal *weakSelf = self;
    self.pinController.validationBlock = ^(NSString *pin) {
        return [pin isEqualToString:weakSelf.defaultsUtil.savedPin];
    };
    
    
    
    
    
    //LONG PRESS//
    self.lpgr = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPressGestures:)];
    self.lpgr.minimumPressDuration = 3.5f;
    self.lpgr.allowableMovement = 100.0f;
    
    [press1 addGestureRecognizer:self.lpgr];
    
    welcome.hidden=YES;
    mainwelcome.hidden=YES;
    mainready.hidden=YES;
    getready.hidden=YES;
    mainready.hidden=YES;
    mainclosed.hidden=YES;
    closed.hidden=YES;
    
    mainvideo.hidden=YES;
    video.hidden=YES;
    mainemail.hidden=YES;
    email.hidden=YES;
    mainfinal.hidden=YES;
    final.hidden=YES;
    
    maincountdown.hidden=YES;
    videotimerview.hidden = YES;
    //    social.hidden = YES;
    email.hidden=YES;
    
    NSString *backgroundcolor = [self.record valueForKey:@"layoutbackgroundcolor"];
    NSString *fontbordercolor = [self.record valueForKey:@"layoutfontbordercolor"];
    
    //PROGRESS INDICATOR//
    KVNProgressConfiguration *configuration = [[KVNProgressConfiguration alloc] init];
    configuration.statusColor = [UIColor colorWithString:fontbordercolor];
    configuration.statusFont = [UIFont fontWithName:@"HelveticaNeue-Thin" size:25.0f];
    configuration.circleStrokeForegroundColor = [UIColor colorWithString:fontbordercolor];
    configuration.circleStrokeBackgroundColor = [[UIColor colorWithString:fontbordercolor]colorWithAlphaComponent:0.3f];
    configuration.circleFillBackgroundColor = [[UIColor colorWithString:fontbordercolor]colorWithAlphaComponent:0.1f];
    configuration.backgroundFillColor = [[UIColor colorWithString:backgroundcolor]colorWithAlphaComponent:0.9f];
    configuration.backgroundTintColor = [[UIColor colorWithString:backgroundcolor]colorWithAlphaComponent:1.0f];
    configuration.successColor = [UIColor colorWithString:backgroundcolor];
    configuration.errorColor = [UIColor colorWithString:backgroundcolor];
    configuration.stopColor = [UIColor colorWithString:backgroundcolor];
    configuration.circleSize = 50.0f;
    configuration.lineWidth = 1.0f;
    configuration.fullScreen = YES;
    configuration.showStop = YES;
    configuration.stopRelativeHeight = 0.4f;
    configuration.tapBlock = ^(KVNProgress *progressView) {
    };
    configuration.allowUserInteraction = NO;
    [KVNProgress setConfiguration:configuration];
    
    
    
    for (UITextField *view in textfields) {
        
        if ([fontbordercolor length] > 0) {
            view.layer.borderWidth = 1;
            view.layer.borderColor = [UIColor colorWithString:fontbordercolor].CGColor;
            view.textColor = [UIColor colorWithString:fontbordercolor];
            [view setValue:[UIColor colorWithString:fontbordercolor] forKeyPath:@"_placeholderLabel.textColor"];
        } else {
            view.layer.borderWidth = 1;
            view.layer.borderColor = [UIColor whiteColor].CGColor;
            view.textColor = [UIColor whiteColor];
            [view setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
        }
        
    }
    
    NSString *valueToSave = [self.record valueForKey:@"eventname"];
    [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"AlbumName"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    
    
    
    if ([backgroundcolor length] > 0) {
        closedscreen.backgroundColor = [UIColor colorWithString:backgroundcolor];
    } else {
        closedscreen.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:0.85];
    }
    
    pinclosed.backgroundColor = [UIColor whiteColor];
    
    NSData *final2 = [self.record valueForKey:@"finalbackground"];
    finalimage = [UIImage imageWithData:final2];
    NSLog(@"final IS %@", finalimage);
    
    
    if(finalimage == nil){
        if ([backgroundcolor length] > 0) {
            finalscreen.backgroundColor = [UIColor colorWithString:backgroundcolor];
        } else {
            finalscreen.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:0.85];
        }
    }
    else {
        final.image = finalimage;
    }
    
    
    
    
    NSData *closedbackground = [self.record valueForKey:@"closedbackground"];
    closedimage = [UIImage imageWithData:closedbackground];
    NSLog(@"closed IS %@", closedimage);
    
    if(closedimage == nil){
        if ([backgroundcolor length] > 0) {
            closedscreen.backgroundColor = [UIColor colorWithString:backgroundcolor];
        } else {
            closedscreen.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:1.0];
        }
    }
    else {
        closed.image = closedimage;
    }
    
    
    
    NSData *welcomebackground = [self.record valueForKey:@"welcomebackground"];
    welcomeimage = [UIImage imageWithData:welcomebackground];
    NSLog(@"welcome IS %@", welcomeimage);
    
    if(welcomeimage == nil){
        if ([backgroundcolor length] > 0) {
            welcomescreen.backgroundColor = [UIColor colorWithString:backgroundcolor ];
        } else {
            welcomescreen.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:1.0];
        }
    }
    else {
        welcome.image = welcomeimage;
    }
    
    
    
    
    //COLOR EFFECTS SWITCHES//
    
    
    if ([backgroundcolor length] > 0) {
        welcomevideowshow.layer.cornerRadius = 7;
        welcomevideowshow.layer.masksToBounds = YES;
        welcomevideowshow.backgroundColor = [UIColor colorWithString:backgroundcolor];
        [[welcomevideowshow layer] setBorderWidth:1.0f];
        [[welcomevideowshow layer] setBorderColor:[UIColor colorWithString:fontbordercolor].CGColor];
    } else {
        welcomevideowshow.layer.cornerRadius = 7;
        welcomevideowshow.layer.masksToBounds = YES;
        welcomevideowshow.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:0.85];
        [[welcomevideowshow layer] setBorderWidth:1.0f];
        [[welcomevideowshow layer] setBorderColor:[UIColor whiteColor].CGColor];
    }
    
    
    
    
    
    
    
    //SOCIAL SWITCHES//
    
    NSString * face = [self.record valueForKey:@"switchFacebook"];
    
    if ([face  isEqual: @"YES"])
    {
        NSLog (@"Facebook SWITCH IS ON");
        [facebook setHidden:NO];
    }
    
    else
    {
        facebook.hidden = YES;
        NSLog (@"Facebook SWITCH IS OFF");
    }
    
    NSString *twit= [self.record valueForKey:@"switchTwitter"];
    
    if ([twit  isEqual: @"YES"])
    {
        NSLog (@"Twitter SWITCH IS ON");
        [twitter setHidden:NO];
    }
    
    else
    {
        twitter.hidden = YES;
        NSLog (@"Twitter SWITCH IS OFF");
    }
    
    
    NSString * email5 = [self.record valueForKey:@"switchEmail"];
    
    if ([email5  isEqual: @"YES"])
    {
        [mail setHidden:NO];
        NSLog (@"Email SWITCH IS ON");
    }
    
    else
    {
        mail.hidden = YES;
        NSLog (@"Email SWITCH IS OFF");
    }
    
    NSString * text= [self.record valueForKey:@"switchText"];
    
    if ([text  isEqual: @"YES"])
    {
        [texting setHidden:NO];
        NSLog (@"Email SWITCH IS ON");
    }
    
    else
    {
        texting.hidden = YES;
        NSLog (@"Email SWITCH IS OFF");
    }
    
    //WELCOME SCREEN SWITCH//
    NSString * string8 = [self.record valueForKey:@"switchMessage"];
    
    if ([string8  isEqual: @"YES"])
    {
        NSLog (@"Welcome SWITCH IS ON");
        
    }
    
    else
    {
        NSLog (@"Welcome SWITCH IS OFF");
    }
    
    NSString * string1 = [self.record valueForKey:@"maxSelection1"];
    NSLog(@"PHOTO COUNTDOWN IS %@", string1);
    NSString * video1 = [self.record valueForKey:@"maxSelection2"];
    NSLog(@"VIDEO COUNTDOWN IS %@", video1);
    
    for (UIView *view in lineseparator)
    {
        if ([backgroundcolor length] > 0)
        {
            view.backgroundColor = [UIColor colorWithString:backgroundcolor];
        }
        else
        {
            view.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:1.0];
        }
    }
    
    for (UIImageView *view in screenbackgroundcolor)
    {
        if ([backgroundcolor length] > 0)
        {
            view.backgroundColor = [UIColor colorWithString:backgroundcolor];
        }
        else
        {
            view.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:1.0];
        }
        
    }
    
    for (UIView *view in viewborders)
    {
        if ([fontbordercolor length] > 0)
        {
            view.backgroundColor = [UIColor colorWithString:fontbordercolor];
        }
        else
        {
            view.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0];
        }
    }
    
    for (UILabel *view in labelcolors) {
        if ([fontbordercolor length] > 0) {
            view.textColor = [UIColor colorWithString:fontbordercolor];
        } else {
            view.textColor = [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0];
        }
    }
    
    for (UILabel *view in labelcolors1) {
        if ([backgroundcolor length] > 0) {
            view.textColor = [UIColor colorWithString:backgroundcolor];
        } else {
            view.textColor = [UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:1.0];
        }
    }
    
    
    for (UISlider *view in sliders) {
        if ([fontbordercolor length] > 0) {
            view.minimumTrackTintColor = [UIColor colorWithString:fontbordercolor];
            view.maximumTrackTintColor = [UIColor colorWithString:fontbordercolor];
            view.thumbTintColor = [UIColor colorWithString:fontbordercolor];
        } else {
            view.minimumTrackTintColor = [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0];
            view.maximumTrackTintColor = [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0];
            view.thumbTintColor = [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0];
        }
        
    }
    
    for (UISegmentedControl *view in segmented) {
        
        if ([fontbordercolor length] > 0) {
            
            view.layer.cornerRadius = 0.0;
            view.layer.borderColor = [UIColor colorWithString:fontbordercolor].CGColor;
            view.tintColor =[UIColor colorWithString:fontbordercolor];
            
            view.backgroundColor = [UIColor clearColor];
            view.layer.borderWidth = 1.0f;
            view.layer.masksToBounds = YES;
            
            
        } else {
            
            view.layer.cornerRadius = 0.0;
            view.layer.borderColor = [UIColor whiteColor].CGColor;
            view.tintColor =[UIColor whiteColor];
            
            view.backgroundColor = [UIColor clearColor];
            view.layer.borderWidth = 1.0f;
            view.layer.masksToBounds = YES;
            
            
            
        }
        
        
    }
    
    
    
    
    for (UIButton *button in buttonborders) {
        
        
        if ([fontbordercolor length] > 0  && [backgroundcolor length] > 0) {
            [[button layer] setBorderWidth:1.0f];
            [[button layer] setBorderColor:[UIColor colorWithString:fontbordercolor].CGColor];
            [[button layer] setBackgroundColor:[UIColor colorWithString:backgroundcolor].CGColor];
            [button setTitleColor:[UIColor colorWithString:fontbordercolor] forState:UIControlStateNormal];
            
        }
        
        else {
            [[button layer] setBorderWidth:1.0f];
            [[button layer] setBorderColor:[UIColor whiteColor].CGColor];
            [[button layer] setBackgroundColor:[UIColor blackColor].CGColor];
            [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        }
        
        
        
    }
    
    
    for (UIView *button in lineborder) {
        
        
        if ([fontbordercolor length] > 0) {
            [[button layer] setBorderWidth:1.0f];
            [[button layer] setBorderColor:[UIColor colorWithString:fontbordercolor].CGColor];
            [[button layer] setBackgroundColor:[UIColor clearColor].CGColor];
            
            
        } else {
            [[button layer] setBorderWidth:1.0f];
            [[button layer] setBorderColor:[UIColor whiteColor].CGColor];
            [[button layer] setBackgroundColor:[UIColor clearColor].CGColor];
            
        }
        
    }
    
    
    //EFFECTS BUTTONS//
    
    
    
    
    
    
    //SOCIAL BACKGROUND//
    NSData *emailbackground2 = [self.record valueForKey:@"emailbackground"];
    emailimage = [UIImage imageWithData:emailbackground2];
    NSLog(@"effects IS %@", emailimage);
    
    if(emailimage == nil)
    {
        if ([backgroundcolor length] > 0)
        {
            emailscreen.backgroundColor = [UIColor colorWithString:backgroundcolor];
            [soc setTitleColor:[UIColor colorWithString:backgroundcolor] forState:UIControlStateNormal];
            
            [skippingsocial setTitleColor:[UIColor colorWithString:backgroundcolor] forState:UIControlStateNormal];
            
        } else
        {
            emailscreen.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:1.0];
            
            [skippingsocial setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [soc setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            
        }
        
        
        if ([fontbordercolor length] > 0) {
            [[soc layer] setBackgroundColor:[UIColor colorWithString:fontbordercolor].CGColor];
            
            
            [[skippingsocial layer] setBackgroundColor:[UIColor colorWithString:fontbordercolor].CGColor];
            
        } else {
            [[soc layer] setBackgroundColor:[UIColor whiteColor].CGColor];
            
            [[skippingsocial layer] setBackgroundColor:[UIColor whiteColor].CGColor];
        }
        
        
        
        
        
    }
    else
    {
        email.image = emailimage;
        emailscreen.hidden = YES;
        sociallab1.hidden = YES;
        socialview.layer.borderWidth = 0;
        social5.hidden = YES;
        [mail setBackgroundImage:nil forState:UIControlStateNormal];
        [texting setBackgroundImage:nil forState:UIControlStateNormal];
        [facebook setBackgroundImage:nil forState:UIControlStateNormal];
        [twitter setBackgroundImage:nil forState:UIControlStateNormal];
        
        for (UIButton *button in buttoneffects1)
        {
            [[button layer] setBackgroundColor:[UIColor colorWithWhite:0.0 alpha:0.0].CGColor];
            [button setTitle:@"" forState:UIControlStateNormal];
            [button setTitle:@"" forState:UIControlStateSelected];
        }
    }
    
    for (UIButton *button in buttonborders1) {
        
        if ([backgroundcolor length] > 0) {
            [button setTitleColor:[UIColor colorWithString:backgroundcolor] forState:UIControlStateNormal];
            
        } else {
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        }
        
        
        if ([fontbordercolor length] > 0) {
            [[button layer] setBackgroundColor:[UIColor colorWithString:fontbordercolor].CGColor];
            
        } else {
            [[button layer] setBackgroundColor:[UIColor whiteColor].CGColor];
        }
    }
    
    for (UIView *button in look) {
        if ([backgroundcolor length] > 0) {
            
            [[button layer] setBackgroundColor:[UIColor colorWithString:backgroundcolor].CGColor];
            
        } else {
            [[button layer] setBackgroundColor:[UIColor whiteColor].CGColor];
            
        }
        
        
        if ([fontbordercolor length] > 0) {
            button.backgroundColor = [UIColor colorWithString:fontbordercolor];
            
        } else {
            button.backgroundColor = [UIColor whiteColor];
        }
        
        
    }
    
    
    [TSPAppDelegate sharedAppDelegate].selectedPhotoCount = 0;
    _selections = [[NSMutableArray alloc] init];
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"printip"];
    
    NSString *savedValue1 = [[NSUserDefaults standardUserDefaults]
                             stringForKey:@"printport"];
    printip.text = [NSString stringWithFormat:@"%@", savedValue];
    printport.text = [NSString stringWithFormat:@"%@", savedValue1];
    
    
}

//PRINT SETTINGS//
-(IBAction)connectServer:(id)sender
{
    
    
    [self tapPing];
    NSLog(@"Connecting server...");
    
    
}


- (void)tapPing {
    
    [SimplePingHelper ping: printip.text target:self sel:@selector(pingResult:)];
    // [SimplePingHelper ping: printport.text target:self sel:@selector(pingResult:)];
}


- (void)pingResult:(NSNumber*)success {
    if (success.boolValue) {
        
        NSString *valueToSave = printip.text;
        NSString *valueToSave1 = printport.text;
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *url = [NSString stringWithFormat:@"%@:%@", valueToSave, valueToSave1];
        [defaults setObject:url forKey:@"print"];
        [defaults setObject:valueToSave forKey:@"printip"];
        [defaults setObject:valueToSave1 forKey:@"printport"];
        
        
        NSURLRequest *theRequest=[NSURLRequest requestWithURL:[NSURL URLWithString:url]
                                  
                                                  cachePolicy:NSURLRequestUseProtocolCachePolicy
                                  
                                              timeoutInterval:5.0];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            FCAlertView *alert = [[FCAlertView alloc] init];
            alert.darkTheme = YES;
            [alert makeAlertTypeSuccess];
            alert.detachButtons = YES;
            alert.blurBackground = YES;
            [alert showAlertWithTitle:@"SUCCESSFULLY CONNECTED"
                         withSubtitle:@"You have been successfully connected. "
                      withCustomImage:nil
                  withDoneButtonTitle:nil
                           andButtons:nil];
            
        });
        
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            FCAlertView *alert = [[FCAlertView alloc] init];
            alert.darkTheme = YES;
            [alert makeAlertTypeSuccess];
            alert.detachButtons = YES;
            alert.blurBackground = YES;
            [alert showAlertWithTitle:@"YOU ARE NOT CONNECTED"
                         withSubtitle:@"Please try again. "
                      withCustomImage:nil
                  withDoneButtonTitle:nil
                           andButtons:nil];
            
        });
        
        
    }
}





- (id)initWithCoder:(NSCoder *)aDecoder {
    if ((self = [super initWithCoder:aDecoder])) {
        self.title = @"MWPhotoBrowser";
        
        //        // Clear cache for testing
        //        [[SDImageCache sharedImageCache] clearDisk];
        //        [[SDImageCache sharedImageCache] clearMemory];
        //
        //        _segmentedControl = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:@"Push", @"Modal", nil]];
        //        _segmentedControl.selectedSegmentIndex = 0;
        //        [_segmentedControl addTarget:self action:@selector(segmentChange) forControlEvents:UIControlEventValueChanged];
        //
        //        UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:_segmentedControl];
        //        self.navigationItem.rightBarButtonItem = item;
        //        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStyleBordered target:nil action:nil];
        //
        [self loadAssets];
        
    }
    return self;
}

- (void)segmentChange {
    [self.tableView reloadData];
}




- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [self.data2 count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    
    
    if (cell == nil) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        
    }
    
    
    cell.textLabel.text = [self.data2 objectAtIndex:indexPath.row] ;
    
    //cell.textLabel.font = [UIFont systemFontOfSize:11.0];
    
    
    return cell;
    
    
    
}





- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    _power.text = cell.textLabel.text;
    //  self.tableView.hidden = YES;
    
    NSLog(@"Power level is %@", _power.text);
    
}




//DROPBOX//

-(IBAction)droboxlogin:(id)sender
{
    box=[[SharingNormal alloc]init];
    [box loginWithViewController:self];
    NSNotificationCenter *centerNew = [NSNotificationCenter defaultCenter];
    [centerNew addObserver: self selector: @selector(afterLogin) name:@"Login Success" object: nil];
    
}


-(IBAction)droboxlogout:(id)sender
{
    
    box=[[SharingNormal alloc]init];
    [box logOut];
    outBtn.hidden=YES;
    loginBtn.hidden=NO;
}


-(void)login:(id)sender
{
    box=[[SharingNormal alloc]init];
    [box loginWithViewController:self];
}



-(void)afterLogin
{
    
    loginBtn.hidden=YES;
    outBtn.hidden=NO;
}


-(void)uploadFinish
{
    [MBProgressHUD hideHUDForView:self.view animated:NO];
}

-(void)logout:(id)sender
{
    box=[[SharingNormal alloc]init];
    [box logOut];
    outBtn.hidden=YES;
    loginBtn.hidden=NO;
}




//PASSCODE//

- (void)updateTouchIDState
{
    if (![self.pinController touchIDAvailable:NULL] || self.defaultsUtil.savedPin == nil) {
        
        [self.defaultsUtil setTouchIDActive:NO];
    }
}

- (void)updateVerifyAndChangeState {
    if (self.defaultsUtil.savedPin) {
    }
    else {
    }
}

- (void)updatePin:(NSString *)pin {
    [self.defaultsUtil savePin:pin];
    [self updateTouchIDState];
    [self updateVerifyAndChangeState];
    
}


- (void)resetPin {
    [self.pinController resetPIN];
    [self.defaultsUtil removePin];
    [self updateTouchIDState];
    [self updateVerifyAndChangeState];
}
#pragma mark - Button actions


- (IBAction)didSelectVerify:(id)sender {
    
    [self.pinController presentVerifyControllerFromViewController1:self];
}

- (IBAction)didSelectCreate:(id)sender {
    
    [self.pinController presentCreateControllerFromViewController:self];
}

- (IBAction)didSelectChange:(id)sender {
    
    [self.pinController presentChangeControllerFromViewController:self];
}

- (IBAction)didSelectRemovePIN:(id)sender {
    [self resetPin];
}

- (IBAction)didSelectTouchIDSwitch:(UISwitch *)sender {
    
    [self.defaultsUtil setTouchIDActive:sender.on];
    if (sender.on) {
        [self.pinController storePin:self.defaultsUtil.savedPin];
    }
    else {
        [self.pinController resetPIN];
    }
}

- (IBAction)didSelectShowPinString:(id)sender {
    
    NSLog(@"In pin controller keychain: %@", [self.pinController storedPin]);
    NSLog(@"In user defaults (custom): %@", self.defaultsUtil.savedPin);
}

#pragma mark - Pin controller delegate

- (void)pinChangeController:(UIViewController *)pinChangeController didChangePin:(NSString *)pin
{
    NSLog(@"Did change pin: %@", pin);
    [self updatePin:pin];
}

- (void)pinCreateController:(UIViewController *)pinCreateController didCreatePin:(NSString *)pin
{
    NSLog(@"Did create pin: %@", pin);
    [self updatePin:pin];
}

- (void)pinController:(UIViewController *)pinController1 didVerifyPin1:(NSString *)pin
{
    mainclosed.hidden=YES;
    NSLog(@"Did verify pin1: %@", pin);
}


- (void)pinController:(UIViewController *)pinController didVerifyPin:(NSString *)pin
{
    pinclosed.hidden=NO;

}


- (void)pinControllerDidEnterWrongPin:(UIViewController *)pinController lastRetry:(BOOL)lastRetry
{
    NSLog(@"Did enter wrong pin - last retry? -> %@", lastRetry ? @"YES" : @"NO");
    if (lastRetry) {
        // Maybe show an alert view controller that indicates that the user has just one retry left?
    }
}

- (void)pinControllerCouldNotVerifyPin:(UIViewController *)pinController
{
    NSLog(@"Could not verify pin - no more retries!");
    
}

- (void)pinControllerDidSelectCancel:(UIViewController *)pinController
{
    NSLog(@"Did select cancel!");
    [pinController dismissViewControllerAnimated:YES completion:nil];
}

- (void)handleLongPressGestures:(UILongPressGestureRecognizer *)sender2
{
    if ([sender2 isEqual:self.lpgr]) {
        [self.popTip hide];
        if (sender2.state == UIGestureRecognizerStateBegan)
        {
            [self.pinController presentVerifyControllerFromViewController:self];
            
        }
    }
}


- (IBAction)closebooth:(id)sender {

    pinclosed.hidden=YES;
    mainclosed.hidden=NO;
    closed.hidden=NO;
    
}

- (IBAction)openbooth:(id)sender {
    
    pinclosed.hidden=YES;
    mainclosed.hidden=YES;
    closed.hidden=YES;
    
}

- (IBAction)searchDevices:(id)sender {
    [[_appDelegate mcManager] setupMCBrowser];
    [[[_appDelegate mcManager] browser] setDelegate:self];
    [self presentViewController:[[_appDelegate mcManager] browser] animated:YES completion:nil];
    
}

#pragma mark - MCBrowserViewControllerDelegate method implementation
-(void)browserViewControllerDidFinish:(MCBrowserViewController *)browserViewController{
    [_appDelegate.mcManager.browser dismissViewControllerAnimated:YES completion:nil];
}


-(void)browserViewControllerWasCancelled:(MCBrowserViewController *)browserViewController{
    [_appDelegate.mcManager.browser dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UITextField Delegate method implementation
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [_nameTxt resignFirstResponder];
    
    _appDelegate.mcManager.peerID = nil;
    _appDelegate.mcManager.session = nil;
    _appDelegate.mcManager.browser = nil;
    [_appDelegate.mcManager.advertiser stop];
    _appDelegate.mcManager.advertiser = nil;
    
    NSString *name = _nameTxt.text;
    if ([name isEqualToString:@""]) {
        name = [UIDevice currentDevice].name;
    }
    
    [_appDelegate.mcManager setupPeerAndSessionWithDisplayName:name];
    [_appDelegate.mcManager setupMCBrowser];
    [_appDelegate.mcManager advertiseSelf:YES];
    
    return YES;
}


#pragma mark - Connection state
-(void)peerDidChangeStateWithNotification:(NSNotification *)notification{
    MCPeerID *peerID = [[notification userInfo] objectForKey:@"peerID"];
    NSString *peerDisplayName = peerID.displayName;
    MCSessionState state = [[[notification userInfo] objectForKey:@"state"] intValue];
    //NSLog(@"state %d",state);
    
    if (state != MCSessionStateConnecting) {
        if (state == MCSessionStateConnected) {
            [_appDelegate.arrConnectedDevices addObject:peerDisplayName];
        
          
            
                   dispatch_async(dispatch_get_main_queue(), ^{
            //                FCAlertView *alert = [[FCAlertView alloc] init];
            //                alert.darkTheme = YES;
            //                [alert makeAlertTypeSuccess];
            //                alert.detachButtons = YES;
            //                alert.blurBackground = YES;
            //                [alert showAlertWithTitle:@"SUCCESSFULLY CONNECTED"
            //                             withSubtitle:@"You have been successfully connected. "
            //                          withCustomImage:nil
            //                      withDoneButtonTitle:nil
            //                               andButtons:nil];
            //
                       
                         pinclosed.hidden=YES;
                   });
            
        }
        else if (state == MCSessionStateNotConnected){
            if ([_appDelegate.arrConnectedDevices count] > 0) {
                int indexOfPeer = (int)[_appDelegate.arrConnectedDevices indexOfObject:peerDisplayName];
                [_appDelegate.arrConnectedDevices removeObjectAtIndex:indexOfPeer];
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                FCAlertView *alert = [[FCAlertView alloc] init];
                alert.darkTheme = YES;
                [alert makeAlertTypeWarning];
                alert.detachButtons = YES;
                alert.blurBackground = YES;
                [alert showAlertWithTitle:@"DISCONNECTED"
                             withSubtitle:@"This device has been disconnected. Please let the attendant know."
                          withCustomImage:nil
                      withDoneButtonTitle:nil
                               andButtons:nil];
                
            });
            
            
        }
        
        
        [_connections reloadData];
        
        BOOL peersExist = ([[_appDelegate.mcManager.session connectedPeers] count] == 0);
        [_btnDisconnect setEnabled:!peersExist];
        [_nameTxt setEnabled:peersExist];
    }
}




- (void) viewDidLayoutSubviews {
    [super viewDidLayoutSubviews]; //if you want superclass's behaviour...
    
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (UIInterfaceOrientationIsPortrait(interfaceOrientation) || UIInterfaceOrientationIsLandscape(interfaceOrientation));
    
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    if(UIInterfaceOrientationIsPortrait(toInterfaceOrientation)){
        
        
        
    }
    else if(UIInterfaceOrientationIsLandscape(toInterfaceOrientation)){
    }
    
    
    
    
}



#pragma mark Capturing Photos


- (IBAction)StartBooth {
    [self startboothing];
    mainadmin.hidden=YES;
    
    NSMutableArray *photos = [[NSMutableArray alloc] init];
    NSMutableArray *thumbs = [[NSMutableArray alloc] init];
    BOOL displayActionButton = YES;
    BOOL displaySelectionButtons = YES;
    BOOL displayNavArrows = YES;
    BOOL enableGrid = YES;
    BOOL startOnGrid = YES;
    BOOL autoPlayOnAppear = NO;
    
    NSMutableArray *copy = [_assets copy];
    UIScreen *screen = [UIScreen mainScreen];
    CGFloat scale = screen.scale;
    // Sizing is very rough... more thought required in a real implementation
    CGFloat imageSize = MAX(screen.bounds.size.width, screen.bounds.size.height) * 1.5;
    CGSize imageTargetSize = CGSizeMake(imageSize * scale, imageSize * scale);
    CGSize thumbTargetSize = CGSizeMake(imageSize / 3.0 * scale, imageSize / 3.0 * scale);
    
     NSArray *array = [copy sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:NO]]];
    
    for (PHAsset *asset in array)
    {
        [photos addObject:[MWPhoto photoWithAsset:asset targetSize:imageTargetSize]];
        [thumbs addObject:[MWPhoto photoWithAsset:asset targetSize:thumbTargetSize]];
        displayActionButton = YES;
        displaySelectionButtons = YES;
        startOnGrid = YES;
        enableGrid = YES;
    }
    
    self.photos = photos;
    self.thumbs = thumbs;
    
    // Create browser
    _browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    _browser.displayActionButton = displayActionButton;
    _browser.displayNavArrows = displayNavArrows;
    _browser.displaySelectionButtons = displaySelectionButtons;
    _browser.alwaysShowControls = displaySelectionButtons;
    _browser.zoomPhotosToFill = YES;
    _browser.enableGrid = enableGrid;
    _browser.startOnGrid = startOnGrid;
    _browser.enableSwipeToDismiss = NO;
    _browser.autoPlayOnAppear = autoPlayOnAppear;
    [_browser setCurrentPhotoIndex:1];
    
    
    [press addSubview:[_browser view]];
    
    //    [self addChildViewController:_browser];
    //    [[self view] addSubview:[_browser view]];
    //    [_browser didMoveToParentViewController:self];
    //
    
    
    
    // Modal
    
    
    
    //   [browser willMoveToParentViewController:mainvideo];
    //    [mainvideo addSubview:browser.view];
    //    [mainvideo bringSubviewToFront:browser.view];
    //
    //
    // [self presentViewController:browser animated:YES completion:nil];
    
    
    // Release
    
    // Deselect
    
    // Test reloading of data after delay
    
    
}

-(void)startboothing
{
    NSString * string8 = [self.record valueForKey:@"switchMessage"];
    NSLog(@"STRING 8 IS %@", string8);
    press1.hidden=NO;
    [TSPAppDelegate sharedAppDelegate].selectedPhotoCount = 0;
    
    if ([TSPAppDelegate sharedAppDelegate].isSendingEmail)
    {
        [_browser reloadAllDataForGrid];
        [TSPAppDelegate sharedAppDelegate].isSendingEmail = NO;
    }
    
    _selections = [[NSMutableArray alloc] init];
    
    _photoTakens = [[NSMutableArray alloc] init];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SHOW_GRID" object:self];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RELOAD_GRID" object:self];
    
    _browser.startOnGrid = YES;
    
    if ([string8  isEqual: @"NO"])
    {
        
        mainwelcome.hidden=YES;
        welcomescreen.hidden=YES;
        welcomevideohide.hidden=YES;
        welcomevideowshow.hidden=YES;
        welcome.hidden=YES;
        touchtostart.hidden=NO;
        social.hidden=YES;
        email.hidden=YES;
        mainfinal.hidden=YES;
        final.hidden=YES;
        press.hidden=NO;
        
        
        
    }
    
    else
    {
        NSString *videowelcome = [self.record valueForKey:@"welcomeimagevideo"];
        if (videowelcome == nil)
        {
            
            social.hidden=YES;
            email.hidden=YES;
            mainfinal.hidden=YES;
            final.hidden=YES;
            touchtostart.hidden=NO;
            mainwelcome.hidden=NO;
            welcomescreen.hidden=NO;
            mainadmin.hidden=YES;
            welcome.hidden=NO;
            welcomevideowshow.hidden=YES;
            welcomevideohide.hidden=NO;
            welcomescreen1.hidden=YES;
            welcomescreen.hidden=NO;
            
            
        }
        else
        {
            
            [self addvideowelcome ];
            touchtostart.hidden=NO;
            mainwelcome.hidden=NO;
            welcomevideowshow.hidden=NO;
            welcomevideohide.hidden=YES;
            welcomescreen1.hidden=NO;
            welcomescreen.hidden=YES;
            mainadmin.hidden=YES;
            welcome.hidden=NO;
            social.hidden=YES;
            email.hidden=YES;
            mainfinal.hidden=YES;
            final.hidden=YES;
            [_player play];
            
        }
        
        
    }
    
    
}


- (IBAction)TouchToStart {
    NSLog(@"TOUCH TO START PRESSED");
    [_player pause];
    
    mainwelcome.hidden=YES;
    welcomescreen.hidden=YES;
    welcomevideohide.hidden=YES;
    welcome.hidden=YES;
    touchtostart.hidden=YES;
    _browser.startOnGrid = YES;
    press.hidden=NO;
    [self loadAssets];
    
    NSLog(@"TOUCH PRESSED");
    
}


//THESE ARE THE VIDEOS THAT ARE DISPLAYED IF THE USER SELECTED A VIDEO MESSAGE OPTION FOR THE WELCOME SCREEN AND THE FINAL SCREEN//

-(void)addvideowelcome
{
    
    NSString *videowelcome = [self.record valueForKey:@"welcomeimagevideo"];
    NSLog(@"WELCOME VIDEO IS %@", videowelcome);
    NSURL *videoURL = [NSURL URLWithString:[videowelcome stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    _player = [AVPlayer playerWithURL:videoURL];
    
    // create a player view controller
    _controller = [[AVPlayerViewController alloc]init];
    [_controller setShowsPlaybackControls:NO];
    _controller.videoGravity = AVLayerVideoGravityResizeAspectFill;
    _controller.player = _player;
    [_player play];
    _player.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:[_player currentItem]];
    // [self addChildViewController:controller];
    [welcomescreen1 addSubview:_controller.view];
    _controller.view.frame = welcomescreen1.frame;
}


-(void)addvideofinal
{
    
    NSString *videofinal = [self.record valueForKey:@"finalimagevideo"];
    NSURL *videoURL = [NSURL URLWithString:[videofinal stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    _finalplayer = [AVPlayer playerWithURL:videoURL];
    
    // create a player view controller
    _controller = [[AVPlayerViewController alloc]init];
    [_controller setShowsPlaybackControls:NO];
    _controller.videoGravity = AVLayerVideoGravityResizeAspectFill;
    _controller.player = _finalplayer;
    [_finalplayer play];
    _finalplayer.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd1:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:[_finalplayer currentItem]];
    // [self addChildViewController:controller];
    [finalscreen1 addSubview:_controller.view];
    _controller.view.frame = finalscreen1.frame;
}




- (void)playerItemDidReachEnd:(NSNotification *)notification {
    AVPlayerItem *p = [notification object];
    [p seekToTime:kCMTimeZero];
}

- (void)playerItemDidReachEnd1:(NSNotification *)notification {
    [_finalplayer pause];
    [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(startboothing) userInfo:nil repeats:NO];
    
}



//EMAIL//

-(IBAction)sendemail:(id)sender
{
    
    
    [self sendEmail];
}

- (void)sendEmail
{
    if ([self hasInternetConnection])
    {
        //IF SENDGRID IS ACTIVE//
        NSString * string6 = [self.record valueForKey:@"switchSendgrid"];
        
        
        if ([string6  isEqualToString: @"YES"])
        {
            
            [self sendgridemail:CNPPopupStyleCentered];
            NSLog(@"SENDGRID IS ON");
            
        }
        
        else
        {
            [self nointernetmail:CNPPopupStyleCentered];
        }
    }
    
    else{
        [self nointernetmail:CNPPopupStyleCentered];
    }
    
}



- (void)emailinternetcheck
{
    if ([self hasInternetConnection])
    {
        
        NSLog(@"HAS INTERNET");
        NSString * face = [self.record valueForKey:@"switchFacebook"];
        
        if ([face  isEqual: @"YES"])
        {
            facebook.hidden=NO;
            NSLog (@"Facebook SWITCH IS ON");
        }
        
        else
        {
            facebook.hidden=YES;
            NSLog (@"Facebook SWITCH IS OFF");
        }
        
        NSString *twit= [self.record valueForKey:@"switchTwitter"];
        
        if ([twit  isEqual: @"YES"])
        {
            twitter.hidden=NO;
            NSLog (@"Twitter SWITCH IS ON");
            
        }
        
        else
        {
            twitter.hidden=YES;
            NSLog (@"Twitter SWITCH IS OFF");
        }
        
        
        NSString * email2 = [self.record valueForKey:@"switchEmail"];
        
        if ([email2  isEqual: @"YES"])
        {
            mail.hidden = NO;
            
            NSLog (@"Email SWITCH IS ON");
        }
        
        else
        {
            mail.hidden = YES;
            NSLog (@"Email SWITCH IS OFF");
        }
        
        
        if (![[DBSession sharedSession] isLinked])
        {
            texting.hidden=YES;
        }
        else
        {
            NSString * text= [self.record valueForKey:@"switchText"];
            
            if ([text  isEqual: @"YES"])
            {
                
                NSString *twilioSID = [[NSUserDefaults standardUserDefaults]
                                       stringForKey:@"twiliosid"];
                
                NSString *twilioAuthKey = [[NSUserDefaults standardUserDefaults]
                                           stringForKey:@"twilioauthkey"];
                
                NSString *fromNumber = [[NSUserDefaults standardUserDefaults]
                                        stringForKey:@"twiliofromnumber"];
                
                
                
                
                if ([twilioSID isEqualToString:@""]||[twilioAuthKey isEqualToString:@""]||[fromNumber isEqualToString:@""])
                {
                    texting.hidden = YES;
                }
                else
                {
                    texting.hidden = NO;
                    NSLog (@"Email SWITCH IS ON");
                }
            }
            
            else
            {
                texting.hidden = YES;
                NSLog (@"Email SWITCH IS OFF");
            }
            
        }
        
        
        
        
        
    }
    
    else{
        
        NSLog(@"NO INTERNET");
        texting.hidden=YES;
        facebook.hidden=YES;
        twitter.hidden=YES;
        mail.hidden=NO;
        
    }
    
}

- (void)nointernetmail:(CNPPopupStyle)popupStyle
{
    
    NSLog(@"EMAIL BUTTON PRESSED");
    fontcolor = [UIColor whiteColor];
    backgroundcolor1  = [UIColor darkGrayColor];
    
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 85, 85)];
    imageView.image = [UIImage imageNamed:@"Email.png"];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    
    UIView *customView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 45)];
    customView.backgroundColor = backgroundcolor1;
    UIView *spacerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    UILabel *lineTwoLabel = [[UILabel alloc] init];
    UITextField *textFied = [[UITextField alloc] initWithFrame:CGRectMake(1,1, 318, 43)];
    [textFied setBackgroundColor:fontcolor];
    [textFied setValue: [UIColor colorWithRed:79.0/255.0 green:127.0/255.0 blue:226.0/255.0 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    textFied.placeholder = @"Enter your email address";
    textFied.text = emailaddress1.text;
    [textFied setFont:[UIFont fontWithName:@"Helvetica Neue" size:22]];
    textFied.delegate = self;
    [textFied setLeftViewMode:UITextFieldViewModeAlways];
    [textFied setLeftView:spacerView];
    
    [customView addSubview:lineTwoLabel];
    [customView addSubview:textFied];
    
    UIView *customView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 340, 65)];
    
    CNPPopupButton *button1 = [[CNPPopupButton alloc] initWithFrame:CGRectMake(20, 10, 140, 45)];
    [button1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button1.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button1 setTitle:@"CANCEL" forState:UIControlStateNormal];
    [button1 setTitleColor:[UIColor colorWithRed:79.0/255.0 green:127.0/255.0 blue:226.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    button1.backgroundColor = fontcolor;
    [[button1 layer] setBorderWidth:1.0f];
    [[button1 layer] setBorderColor:[UIColor colorWithRed:79.0/255.0 green:127.0/255.0 blue:226.0/255.0 alpha:1.0].CGColor];
    button1.layer.cornerRadius = 15;
    button1.selectionHandler = ^(CNPPopupButton *button1){
        [self.popupController dismissPopupControllerAnimated:YES];
        
    };
    CNPPopupButton *button = [[CNPPopupButton alloc] initWithFrame:CGRectMake(180, 10, 140, 45)];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button setTitle:@"SEND EMAIL" forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.backgroundColor = [UIColor colorWithRed:79.0/255.0 green:127.0/255.0 blue:226.0/255.0 alpha:1.0];
    button.layer.cornerRadius = 15;
    button.selectionHandler = ^(CNPPopupButton *button){
        [self.popupController dismissPopupControllerAnimated:YES];
        if ([self validateEmail:textFied.text] == 1) {
            
            
            NSString *resultLine=[NSString stringWithFormat:@" %@\n",
                                  
                                  
                                  textFied.text
                                  ];
            
            NSString *eventname = [self.record valueForKey:@"eventname"];
            NSString *docPath =[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0];
            NSString *surveys=[docPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_emails.csv", eventname]];
            
            if (![[NSFileManager defaultManager] fileExistsAtPath:surveys]) {
                [[NSFileManager defaultManager]
                 createFileAtPath:surveys contents:nil attributes:nil];
            }
            
            NSFileHandle *fileHandle = [NSFileHandle fileHandleForUpdatingAtPath:surveys];
            [fileHandle seekToEndOfFile];
            [fileHandle writeData:[resultLine dataUsingEncoding:NSUTF8StringEncoding]];
            [fileHandle closeFile];

            
            NSString *valueToSave = textFied.text;
            [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"emailaddress1"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            NSLog(@"TextField Saved: %@", valueToSave);
            [self nointernetmailcomposer];
            email.hidden=NO;
            emailscreen.hidden=NO;
            social.hidden = NO;
            socialview.hidden=NO;
            social.hidden=NO;
            
            [self.klcPopUp dismiss:YES];
        }
        
        
        else {
            NSLog(@"VALIDATE EMAIL DONE");
            [self tryagain:CNPPopupStyleCentered];
        }
        
    };
    
    
    
    [customView1 addSubview:button1];
    [customView1 addSubview:button];
    self.popupController = [[CNPPopupController alloc] initWithContents:@[imageView, customView, customView1]];
    self.popupController.theme = [CNPPopupTheme defaultTheme];
    self.popupController.theme.popupStyle = popupStyle;
    self.popupController.delegate = self;
    [self.popupController presentPopupControllerAnimated:NO];
    
    
    
}

-(void)nointernetmailcomposer
{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString* strValue =[defaults objectForKey:@"emailsubject"];
    NSLog(@"Email subject %@", strValue);
    
    NSUserDefaults *defaults1 = [NSUserDefaults standardUserDefaults];
    NSString* strValue1 = [defaults1 objectForKey:@"emailmessage"];
    NSLog(@"Email message %@", strValue1);
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"emailaddress1"];
    
    MFMailComposeViewController* composeVC = [[MFMailComposeViewController alloc] init];
    composeVC.mailComposeDelegate = self;
    
    // Configure the fields of the interface.
    
    NSArray *usersTo = [NSArray arrayWithObject: [NSString stringWithFormat:@"%@", savedValue]];
    NSString *subject = [NSString stringWithFormat:@"%@", strValue];
    NSString *message = [NSString stringWithFormat:@"%@", strValue1];
    
    [composeVC setToRecipients:usersTo];
    
    if ([strValue isEqualToString:@""] || strValue == nil)
        [composeVC setSubject:@"Your photobooth pictures"];
    else
        [composeVC setSubject:subject];
    
    if ([strValue1 isEqualToString:@""] || strValue1 == nil)
        [composeVC setMessageBody: @"We hope you enjoyed yourself in the photo booth. Please feel free to use these photos as you wish." isHTML:NO];
    
    else
        [composeVC setMessageBody:message isHTML:YES];
    
    
    for (int i = 0; i < self.photoTakens.count; i++)
    {
        TSPPhotoRepository* photoItem = [self.photoTakens objectAtIndex:i];
        UIImage* image = photoItem.image;
        
        [composeVC addAttachmentData:UIImagePNGRepresentation(image) mimeType:@"image/jpeg" fileName:[NSString stringWithFormat:@"Individual_%d.jpeg", i]];
    }
    
    
    
    
    [self presentViewController:composeVC animated:YES completion:nil];
    
    
    //     NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
    //    [currentDefaults removeObjectForKey:@"yourKeyName"];
    
}

-(void)tryagain:(CNPPopupStyle)popupStyle
{
    fontcolor = [UIColor whiteColor];
    backgroundcolor1  = [UIColor darkGrayColor];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 85, 85)];
    imageView.image = [UIImage imageNamed:@"caution.png"];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    
    UILabel *titleLabel = [[UILabel alloc] init];
    NSAttributedString *lineTwo = [[NSAttributedString alloc] initWithString:@"Looks like you entered an incorrect email address. Would you like to try again?" attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:18], NSForegroundColorAttributeName : [UIColor darkGrayColor], NSParagraphStyleAttributeName : paragraphStyle}];
    titleLabel.numberOfLines = 2;
    titleLabel.attributedText = lineTwo;
    
    UIView *customView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 340, 65)];
    
    CNPPopupButton *button1 = [[CNPPopupButton alloc] initWithFrame:CGRectMake(20, 10, 140, 45)];
    [button1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button1.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button1 setTitle:@"CANCEL" forState:UIControlStateNormal];
    [button1 setTitleColor:[UIColor colorWithRed:79.0/255.0 green:127.0/255.0 blue:226.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    button1.backgroundColor = fontcolor;
    [[button1 layer] setBorderWidth:1.0f];
    [[button1 layer] setBorderColor:[UIColor colorWithRed:79.0/255.0 green:127.0/255.0 blue:226.0/255.0 alpha:1.0].CGColor];
    button1.layer.cornerRadius = 15;
    button1.selectionHandler = ^(CNPPopupButton *button1){
        [self.popupController dismissPopupControllerAnimated:YES];
        
    };
    CNPPopupButton *button = [[CNPPopupButton alloc] initWithFrame:CGRectMake(180, 10, 140, 45)];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button setTitle:@"TRY AGAIN" forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.backgroundColor = [UIColor colorWithRed:79.0/255.0 green:127.0/255.0 blue:226.0/255.0 alpha:1.0];
    button.layer.cornerRadius = 15;
    button.selectionHandler = ^(CNPPopupButton *button){
        [self.popupController dismissPopupControllerAnimated:YES];
        [self nointernetmail:CNPPopupStyleCentered];
    };
    
    
    
    [customView1 addSubview:button1];
    [customView1 addSubview:button];
    self.popupController = [[CNPPopupController alloc] initWithContents:@[imageView, titleLabel, customView1]];
    self.popupController.theme = [CNPPopupTheme defaultTheme];
    self.popupController.theme.popupStyle = popupStyle;
    self.popupController.delegate = self;
    [self.popupController presentPopupControllerAnimated:NO];
    
    
    
}




- (void)sendgridemail:(CNPPopupStyle)popupStyle
{
    
    NSLog(@"EMAIL BUTTON PRESSED");
    fontcolor = [UIColor whiteColor];
    backgroundcolor1  = [UIColor darkGrayColor];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 85, 85)];
    imageView.image = [UIImage imageNamed:@"mailicon.png"];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    UIView *customView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 45)];
    customView.backgroundColor = [UIColor colorWithRed:79.0/255.0 green:127.0/255.0 blue:226.0/255.0 alpha:1.0];
    UIView *spacerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    UILabel *lineTwoLabel = [[UILabel alloc] init];
    UITextField *textFied = [[UITextField alloc] initWithFrame:CGRectMake(1,1, 318, 43)];
    [textFied setBackgroundColor:fontcolor];
    [textFied setValue: [UIColor colorWithRed:79.0/255.0 green:127.0/255.0 blue:226.0/255.0 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    textFied.placeholder = @"Enter your email address";
    textFied.text = emailaddress1.text;
    textFied.delegate = self;
    [textFied setLeftViewMode:UITextFieldViewModeAlways];
    [textFied setLeftView:spacerView];
    
    [customView addSubview:lineTwoLabel];
    [customView addSubview:textFied];
    
    UIView *customView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 340, 65)];
    
    CNPPopupButton *button1 = [[CNPPopupButton alloc] initWithFrame:CGRectMake(20, 10, 140, 45)];
    [button1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button1.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button1 setTitle:@"CANCEL" forState:UIControlStateNormal];
    [button1 setTitleColor:[UIColor colorWithRed:79.0/255.0 green:127.0/255.0 blue:226.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    button1.backgroundColor = fontcolor;
    [[button1 layer] setBorderWidth:1.0f];
    [[button1 layer] setBorderColor:[UIColor colorWithRed:79.0/255.0 green:127.0/255.0 blue:226.0/255.0 alpha:1.0].CGColor];
    button1.layer.cornerRadius = 15;
    button1.selectionHandler = ^(CNPPopupButton *button1){
        [self.popupController dismissPopupControllerAnimated:YES];
        
    };
    CNPPopupButton *button = [[CNPPopupButton alloc] initWithFrame:CGRectMake(180, 10, 140, 45)];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button setTitle:@"SEND EMAIL" forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.backgroundColor = [UIColor colorWithRed:79.0/255.0 green:127.0/255.0 blue:226.0/255.0 alpha:1.0];
    button.layer.cornerRadius = 15;
    button.selectionHandler = ^(CNPPopupButton *button){
        [self.popupController dismissPopupControllerAnimated:YES];
        if ([self validateEmail:textFied.text] == 1) {
            
            NSString *resultLine=[NSString stringWithFormat:@" %@\n",
                                  
                                  
                                  textFied.text
                                  ];
            
            NSString *eventname = [self.record valueForKey:@"eventname"];
            NSString *docPath =[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0];
            NSString *surveys=[docPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_emails.csv", eventname]];
            
            if (![[NSFileManager defaultManager] fileExistsAtPath:surveys]) {
                [[NSFileManager defaultManager]
                 createFileAtPath:surveys contents:nil attributes:nil];
            }
            
            NSFileHandle *fileHandle = [NSFileHandle fileHandleForUpdatingAtPath:surveys];
            [fileHandle seekToEndOfFile];
            [fileHandle writeData:[resultLine dataUsingEncoding:NSUTF8StringEncoding]];
            [fileHandle closeFile];

            
            NSString *valueToSave = textFied.text;
            [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"emailaddress1"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            NSLog(@"TextField Saved: %@", valueToSave);
            [self sendgridmailcomposer];
            email.hidden=NO;
            emailscreen.hidden=NO;
            social.hidden = NO;
            socialview.hidden=NO;
            
            
            
        }
        
        
        else {
            NSLog(@"VALIDATE EMAIL DONE");
            [self tryagain:CNPPopupStyleCentered];
        }
        
    };
    
    
    
    [customView1 addSubview:button1];
    [customView1 addSubview:button];
    self.popupController = [[CNPPopupController alloc] initWithContents:@[imageView, customView, customView1]];
    self.popupController.theme = [CNPPopupTheme defaultTheme];
    self.popupController.theme.popupStyle = popupStyle;
    self.popupController.delegate = self;
    [self.popupController presentPopupControllerAnimated:NO];
    
    
    
}



-(void)sendgridmailcomposer
{
    [KVNProgress showWithStatus:@"Please wait while your photos are being sent."];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString* strValue =[defaults objectForKey:@"emailsubject"];
    NSLog(@"Email subject %@", strValue);
    
    NSUserDefaults *defaults1 = [NSUserDefaults standardUserDefaults];
    NSString* strValue1 = [defaults1 objectForKey:@"emailmessage"];
    NSLog(@"Email message %@", strValue1);
    
    
    NSUserDefaults *defaults2 = [NSUserDefaults standardUserDefaults];
    NSString* strValue2 =[defaults2 objectForKey:@"username"];
    NSLog(@"Username %@", strValue2);
    
    NSUserDefaults *defaults3 = [NSUserDefaults standardUserDefaults];
    NSString* strValue3 = [defaults3 objectForKey:@"password"];
    NSLog(@"Password %@", strValue3);
    
    NSUserDefaults *defaults4 = [NSUserDefaults standardUserDefaults];
    NSString* strValue4 =[defaults4 objectForKey:@"ipixemail"];
    NSLog(@"Email %@", strValue4);
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"emailaddress1"];
    
    SendGrid *sendgrid = [SendGrid apiUser:[NSString stringWithFormat:@"%@", strValue2] apiKey:[NSString stringWithFormat:@"%@", strValue3]];
    SendGridEmail *email4 = [[SendGridEmail alloc] init];
    
    
    
    if ([strValue isEqualToString:@""] || strValue == nil)
        email4.subject = @"Your photobooth pictures";
    else
        email4.subject = [NSString stringWithFormat:@"%@", strValue];
    
    if ([strValue1 isEqualToString:@""] || strValue1 == nil)
        email4.html = @"We hope you enjoyed yourself in the photo booth. Please feel free to use these photos as you wish.";
    else
        email4.html = [NSString stringWithFormat:@"%@", strValue1];
    
    if ([strValue1 isEqualToString:@""] || strValue1 == nil)
        email4.text = @"We hope you enjoyed yourself in the photo booth. Please feel free to use these photos as you wish.";
    else
        email4.text = [NSString stringWithFormat:@"%@", strValue1];
    
    
    email4.to = [NSString stringWithFormat:@"%@", savedValue];
    email4.from = [NSString stringWithFormat:@"%@", strValue4];
    
    for (int i = 0; i < self.photoTakens.count; i++)
    {
        TSPPhotoRepository* photoItem = [self.photoTakens objectAtIndex:i];
        
        SendGridEmailAttachment* someImageAttachment = [[SendGridEmailAttachment alloc] init];
        someImageAttachment.attachmentData = UIImagePNGRepresentation(photoItem.image);
        someImageAttachment.mimeType = @"image/jpeg";
        someImageAttachment.fileName = [NSString stringWithFormat:@"Individual_%d.jpeg", i];
        someImageAttachment.extension = @"jpeg";
        [email4 attachFile:someImageAttachment];
    }
    
    
    
    [sendgrid sendAttachmentWithWeb:email4
                       successBlock:^(id responseObject)
     {
         
         
         NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
         [currentDefaults removeObjectForKey:@"layoutphoto"];
         [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"emailaddress1"];
         [[NSUserDefaults standardUserDefaults] synchronize];
         [KVNProgress dismiss];
         [_browser reloadAllDataForGrid];
         NSLog(@"YES IT WORKED");
         NSString *videofinal = [self.record valueForKey:@"finalimagevideo"];
         if (videofinal == nil)
         {
             social.hidden=YES;
             email.hidden=YES;
             [self.klcPopUp dismiss:YES];
             mainfinal.hidden = NO;
             final.hidden = NO;
             [NSTimer scheduledTimerWithTimeInterval:3.0
                                              target:self
                                            selector:@selector(startboothing)
                                            userInfo:nil
                                             repeats:NO];
         }
         else{
             [self.klcPopUp dismiss:YES];
             social.hidden=YES;
             email.hidden=YES;
             mainfinal.hidden = NO;
             finalscreen2.hidden=YES;
             finalscreen.hidden=YES;
             [self addvideofinal];
         }
         
         
         
         
         
     }
                       failureBlock:^(NSError *error)
     {
         MFMailComposeViewController* composeVC = [[MFMailComposeViewController alloc] init];
         composeVC.mailComposeDelegate = self;
         
         // Configure the fields of the interface.
         NSArray *usersTo = [NSArray arrayWithObject: [NSString stringWithFormat:@"%@", savedValue]];
         NSString *subject = [NSString stringWithFormat:@"%@", strValue];
         NSString *message = [NSString stringWithFormat:@"%@", strValue1];
         
         [composeVC setToRecipients:usersTo];
         
         if ([strValue isEqualToString:@""] || strValue == nil)
             [composeVC setSubject:@"Your photobooth pictures"];
         else
             [composeVC setSubject:subject];
         
         if ([strValue1 isEqualToString:@""] || strValue1 == nil)
             [composeVC setMessageBody: @"We hope you enjoyed yourself in the photo booth. Please feel free to use these photos as you wish." isHTML:NO];
         
         else
             [composeVC setMessageBody:message isHTML:YES];
         
         for (int i = 0; i < self.photoTakens.count; i++)
         {
             TSPPhotoRepository* photoItem = [self.photoTakens objectAtIndex:i];
             UIImage* image = photoItem.image;
             
             [composeVC addAttachmentData:UIImagePNGRepresentation(image) mimeType:@"image/jpeg" fileName:[NSString stringWithFormat:@"Photo_%d.jpeg", i]];
         }
         
         [self presentViewController:composeVC animated:YES completion:nil];
         
     }];
    
    
    
}

-(IBAction)skipsocial:(id)sender
{
    
    
    email.hidden=YES;
    emailscreen.hidden=YES;
    social.hidden = YES;
    socialview.hidden=YES;
    social.hidden=YES;
    
    NSString *videofinal = [self.record valueForKey:@"finalimagevideo"];
    if (videofinal == nil)
    {
        mainfinal.hidden = NO;
        final.hidden = NO;
        [NSTimer scheduledTimerWithTimeInterval:3.0
                                         target:self
                                       selector:@selector(startboothing)
                                       userInfo:nil
                                        repeats:NO];
    }
    else{
        mainfinal.hidden = NO;
        finalscreen2.hidden=YES;
        finalscreen.hidden=YES;
        [self addvideofinal];
    }
    
}

- (BOOL) hasInternetConnection {
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        return false;
    } else {
        return true;
    }
    
    
}

- (BOOL) isWifiConnected {
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == ReachableViaWiFi) {
        return true;
    } else {
        return false;
    }
    
}


-(BOOL)validateEmail:(NSString *)candidate {
    
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    //	return 0;
    return [emailTest evaluateWithObject:candidate];
}


-(void)mailComposeController:(MFMailComposeViewController *)composeVC didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [TSPAppDelegate sharedAppDelegate].isSendingEmail = YES;
    
    if (result == MFMailComposeResultSent) {
        @autoreleasepool {
            
            
            
            [composeVC dismissViewControllerAnimated:YES completion:nil];
            [_browser reloadAllDataForGrid];
            NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
            [currentDefaults removeObjectForKey:@"layoutphoto"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"emailaddress1"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            NSString *videofinal = [self.record valueForKey:@"finalimagevideo"];
            if (videofinal == nil)
            {
                social.hidden=YES;
                email.hidden=YES;
                [self.klcPopUp dismiss:YES];
                mainfinal.hidden = NO;
                final.hidden = NO;
                [NSTimer scheduledTimerWithTimeInterval:3.0
                                                 target:self
                                               selector:@selector(startboothing)
                                               userInfo:nil
                                                repeats:NO];
            }
            else{
                social.hidden=YES;
                email.hidden=YES;
                [self.klcPopUp dismiss:YES];
                mainfinal.hidden = NO;
                finalscreen2.hidden=YES;
                finalscreen.hidden=YES;
                [self addvideofinal];
            }
            
        }
        
    }
    
    else if (result == MFMailComposeResultCancelled) {
        email.hidden=NO;
        emailscreen.hidden=NO;
        social.hidden = NO;
        socialview.hidden=NO;
        
        [txtFldEmail resignFirstResponder];
        [composeVC dismissViewControllerAnimated:YES completion:nil];
        
    }
    
    
    
    
    
    
}



//TWITTER//


-(IBAction)sendTwitter:(id)sender
{
    
    [self sendTwitter];
}

- (void)sendTwitter
{
    UIViewController *loginController = [[FHSTwitterEngine sharedEngine]loginControllerWithCompletionHandler:^(BOOL success) {
        NSLog(success?@"L0L success":@"O noes!!! Loggen faylur!!!");
        [self postTweet];
        
        
    }];
    [self presentViewController:loginController animated:YES completion:nil];
}

- (void)postTweet {
    
    NSUserDefaults *defaults2 = [NSUserDefaults standardUserDefaults];
    NSString* string64 =[defaults2 objectForKey:@"twitterhashtag"];
    
    
    for (int i = 0; i < self.photoTakens.count; i++)
    {
        
        TSPPhotoRepository* photoItem = [self.photoTakens objectAtIndex:i];
        UIImage* image = photoItem.image;
        NSData *data12 = UIImageJPEGRepresentation(image, 0.4);
        
        
        if ([string64 isEqualToString:@""] || string64 == nil)
        {
            [[FHSTwitterEngine sharedEngine]postTweet:[NSString stringWithFormat:@"We had a blast. Here's one of our photos."] withImageData:data12];
        }
        
        else
        {
            [[FHSTwitterEngine sharedEngine]postTweet:[NSString stringWithFormat:@"%@", string64] withImageData:data12];
        }
        
    }
    
    
    
    
    
    [self logout];
    
    
    
}

- (void)logout
{
    [[FHSTwitterEngine sharedEngine] clearAccessToken];
    [_browser reloadAllDataForGrid];
    NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
    [currentDefaults removeObjectForKey:@"layoutphoto"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSString *videofinal = [self.record valueForKey:@"finalimagevideo"];
    if (videofinal == nil)
    {
        [self.klcPopUp dismiss:YES];
        mainfinal.hidden = NO;
        final.hidden = NO;
        [NSTimer scheduledTimerWithTimeInterval:3.0
                                         target:self
                                       selector:@selector(startboothing)
                                       userInfo:nil
                                        repeats:NO];
    }
    else{
        [self.klcPopUp dismiss:YES];
        mainfinal.hidden = NO;
        finalscreen2.hidden=YES;
        finalscreen.hidden=YES;
        [self addvideofinal];
    }
    
    
}


//FACEBOOK//
- (void)sendFacebook
{
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    
    login.loginBehavior = FBSDKLoginBehaviorWeb;
    
    [login logInWithPublishPermissions:@[@"publish_actions"]
     
                    fromViewController:self
     
                               handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                                   
                                   if (error) {
                                       
                                       NSLog(@"Process error");
                                       
                                   } else if (result.isCancelled) {
                                       
                                       NSLog(@"Cancelled");
                                       
                                   } else {
                                       
                                       NSLog(@"Logged in");
                                       
                                       
                                       [self shareSegmentWithFacebookComposer];
                                       
                                       
                                       
                                       
                                   }
                                   
                               }];
}
-(IBAction)sendFacebook:(id)sender
{
    
    [self sendFacebook];
    
    
}


-(void)shareSegmentWithFacebookComposer{
    if ([[FBSDKAccessToken currentAccessToken] hasGranted:@"publish_actions"]) {
        NSLog(@"has publish permissions");
        
        
        [self facebookoption:CNPPopupStyleCentered];; //publish
        
    } else {
        NSLog(@"no publish permissions"); // no publish permissions so get them, then post
        FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
        [loginManager logInWithPublishPermissions:@[@"publish_actions"]
                               fromViewController:self
                                          handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                                              [self facebookoption:CNPPopupStyleCentered];
                                          }];
        
        
        
    }
}

- (void)facebookoption:(CNPPopupStyle)popupStyle
{
    
    
    
    NSLog(@"EMAIL BUTTON PRESSED");
    fontcolor = [UIColor whiteColor];
    backgroundcolor1  = [UIColor darkGrayColor];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 85, 85)];
    imageView.image = [UIImage imageNamed:@"FB.png"];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    
    UILabel *titleLabel = [[UILabel alloc] init];
    NSAttributedString *lineTwo = [[NSAttributedString alloc] initWithString:@"Is this your first time sharing to Facebook from this event?" attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:18], NSForegroundColorAttributeName : [UIColor darkGrayColor], NSParagraphStyleAttributeName : paragraphStyle}];
    titleLabel.numberOfLines = 2;
    titleLabel.attributedText = lineTwo;
    
    UIView *customView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 340, 65)];
    
    CNPPopupButton *button1 = [[CNPPopupButton alloc] initWithFrame:CGRectMake(20, 10, 140, 45)];
    [button1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button1.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button1 setTitle:@"YES" forState:UIControlStateNormal];
    [button1 setTitleColor:[UIColor colorWithRed:58.0/255.0 green:89.0/255.0 blue:153.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    button1.backgroundColor = fontcolor;
    [[button1 layer] setBorderWidth:2.0f];
    [[button1 layer] setBorderColor:[UIColor colorWithRed:58.0/255.0 green:89.0/255.0 blue:153.0/255.0 alpha:1.0].CGColor];
    button1.layer.cornerRadius = 15;
    button1.selectionHandler = ^(CNPPopupButton *button1){
        [self.popupController dismissPopupControllerAnimated:YES];
        [self publishFBPost1];
        
    };
    CNPPopupButton *button = [[CNPPopupButton alloc] initWithFrame:CGRectMake(180, 10, 140, 45)];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button setTitle:@"NO" forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.backgroundColor = [UIColor colorWithRed:58.0/255.0 green:89.0/255.0 blue:153.0/255.0 alpha:1.0];
    button.layer.cornerRadius = 15;
    button.selectionHandler = ^(CNPPopupButton *button){
        [self.popupController dismissPopupControllerAnimated:YES];
        
        [self publishFBPost];
    };
    
    
    
    [customView1 addSubview:button1];
    [customView1 addSubview:button];
    self.popupController = [[CNPPopupController alloc] initWithContents:@[imageView, titleLabel, customView1]];
    self.popupController.theme = [CNPPopupTheme defaultTheme];
    self.popupController.theme.popupStyle = popupStyle;
    self.popupController.delegate = self;
    [self.popupController presentPopupControllerAnimated:NO];
    
    
    
}




-(void) publishFBPost
{
    
    NSMutableArray* photos = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < [self.photoTakens count]; i++)
    {
        TSPPhotoRepository* photoItem = [self.photoTakens objectAtIndex:i];
        UIImage* image = photoItem.image;
        NSData *data12 = UIImageJPEGRepresentation(image, 0.4);
        UIImage *image2 = [UIImage imageWithData:data12];
        FBSDKSharePhoto *photo = [[FBSDKSharePhoto alloc] init];
        photo.image = image2;
        photo.userGenerated = NO;
        [photos addObject:photo];
        
        
        
    }
    
    
    
    
    
    
    
    //   //THIS IS THE PHOTO ONLY CODE
    FBSDKSharePhotoContent *content = [[FBSDKSharePhotoContent alloc] init];
    content.photos = [photos copy];
    [FBSDKShareAPI shareWithContent:content delegate:nil];
    
    
    
    NSString *videofinal = [self.record valueForKey:@"finalimagevideo"];
    if (videofinal == nil)
    {
        [self.klcPopUp dismiss:YES];
        mainfinal.hidden = NO;
        final.hidden = NO;
        [NSTimer scheduledTimerWithTimeInterval:3.0
                                         target:self
                                       selector:@selector(startboothing)
                                       userInfo:nil
                                        repeats:NO];
    }
    else{
        [self.klcPopUp dismiss:YES];
        mainfinal.hidden = NO;
        finalscreen2.hidden=YES;
        finalscreen.hidden=YES;
        [self addvideofinal];
    }
    [_browser reloadAllDataForGrid];
    [[FBSDKLoginManager new] logOut];
}


-(void) publishFBPost1
{
    
    NSMutableArray* photos = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < [self.photoTakens count]; i++)
    {
        TSPPhotoRepository* photoItem = [self.photoTakens objectAtIndex:i];
        UIImage* image = photoItem.image;
        NSData *data12 = UIImageJPEGRepresentation(image, 0.4);
        UIImage *image2 = [UIImage imageWithData:data12];
        FBSDKSharePhoto *photo = [[FBSDKSharePhoto alloc] init];
        photo.image = image2;
        photo.userGenerated = NO;
        [photos addObject:photo];
        
        
        
    }
    
    
    
    
    
    NSUserDefaults *defaults2 = [NSUserDefaults standardUserDefaults];
    NSString* strValue =[defaults2 objectForKey:@"fbwebsite"];
    NSString* strValue1 =[defaults2 objectForKey:@"fbtitle"];
    NSString* strValue2 =[defaults2 objectForKey:@"fbdescription"];
    NSString* strValue3 =[defaults2 objectForKey:@"fblogo"];
    
    //   //THIS IS THE PHOTO ONLY CODE
    FBSDKSharePhotoContent *content = [[FBSDKSharePhotoContent alloc] init];
    content.photos = [photos copy];
    [FBSDKShareAPI shareWithContent:content delegate:nil];
    
    FBSDKShareLinkContent *linkContent = [[FBSDKShareLinkContent alloc] init];
    linkContent.contentURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@", strValue]];
    linkContent.contentTitle=[NSString stringWithFormat:@"%@", strValue1];
    linkContent.contentDescription=[NSString stringWithFormat:@"%@", strValue2];
    linkContent.imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@", strValue3]];
    [FBSDKShareAPI shareWithContent:linkContent delegate:nil];
    
    
    NSString *videofinal = [self.record valueForKey:@"finalimagevideo"];
    if (videofinal == nil)
    {
        [self.klcPopUp dismiss:YES];
        mainfinal.hidden = NO;
        final.hidden = NO;
        [NSTimer scheduledTimerWithTimeInterval:3.0
                                         target:self
                                       selector:@selector(startboothing)
                                       userInfo:nil
                                        repeats:NO];
    }
    else{
        [self.klcPopUp dismiss:YES];
        mainfinal.hidden = NO;
        finalscreen2.hidden=YES;
        finalscreen.hidden=YES;
        [self addvideofinal];
    }
    [_browser reloadAllDataForGrid];
    [[FBSDKLoginManager new] logOut];
}




//TEXTING//

-(IBAction)sendText:(id)sender
{
    [self sendtexting:CNPPopupStyleCentered];
}


-(void)sendtexting:(CNPPopupStyle)popupStyle
{
    
    fontcolor = [UIColor whiteColor];
    backgroundcolor1  = [UIColor darkGrayColor];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];
    imageView.image = [UIImage imageNamed:@"Message.png"];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    UIView *customView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 45)];
    customView.backgroundColor = [UIColor colorWithRed:168.0/255.0 green:217.0/255.0 blue:69.0/255.0 alpha:1.0];
    UIView *spacerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    UILabel *lineTwoLabel = [[UILabel alloc] init];
    UITextField *textFied = [[UITextField alloc] initWithFrame:CGRectMake(1,1, 318, 43)];
    [textFied setBackgroundColor:fontcolor];
    [textFied setValue: [UIColor colorWithRed:168.0/255.0 green:217.0/255.0 blue:69.0/255.0 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    textFied.placeholder = @"Enter your phone number";
    textFied.text = textnumber.text;
    textFied.delegate = self;
    [textFied setLeftViewMode:UITextFieldViewModeAlways];
    [textFied setLeftView:spacerView];
    
    [customView addSubview:lineTwoLabel];
    [customView addSubview:textFied];
    textFied.keyboardType = UIKeyboardTypeNumberPad;
    
    UIView *customView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 340, 65)];
    
    CNPPopupButton *button1 = [[CNPPopupButton alloc] initWithFrame:CGRectMake(20, 10, 140, 45)];
    [button1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button1.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button1 setTitle:@"CANCEL" forState:UIControlStateNormal];
    [button1 setTitleColor:[UIColor colorWithRed:168.0/255.0 green:217.0/255.0 blue:69.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    button1.backgroundColor = fontcolor;
    [[button1 layer] setBorderWidth:1.0f];
    [[button1 layer] setBorderColor:[UIColor colorWithRed:168.0/255.0 green:217.0/255.0 blue:69.0/255.0 alpha:1.0].CGColor];
    button1.layer.cornerRadius = 15;
    button1.selectionHandler = ^(CNPPopupButton *button1){
        [self.popupController dismissPopupControllerAnimated:YES];
        
    };
    CNPPopupButton *button = [[CNPPopupButton alloc] initWithFrame:CGRectMake(180, 10, 140, 45)];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button setTitle:@"SEND TEXT" forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.backgroundColor = [UIColor colorWithRed:168.0/255.0 green:217.0/255.0 blue:69.0/255.0 alpha:1.0];
    button.layer.cornerRadius = 15;
    button.selectionHandler = ^(CNPPopupButton *button){
        [self.popupController dismissPopupControllerAnimated:YES];
        //        MBProgressHUD *nud=[MBProgressHUD showHUDAddedTo:self.view animated:YES];
        //        nud.labelText=@"Uploading..";
        //
        
        
        NSString *valueToSave = textFied.text;
        [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"textnumber"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        NSLog(@"TextField Saved: %@", valueToSave);
        [self uploadphotos];
        
        
        
        
    };
    
    
    
    [customView1 addSubview:button1];
    [customView1 addSubview:button];
    self.popupController = [[CNPPopupController alloc] initWithContents:@[imageView, customView, customView1]];
    self.popupController.theme = [CNPPopupTheme defaultTheme];
    self.popupController.theme.popupStyle = popupStyle;
    self.popupController.delegate = self;
    [self.popupController presentPopupControllerAnimated:NO];
    
}

-(void)uploadphotos
{
    
    
    
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"textnumber"];
    
    
    NSString *twilioSID = [[NSUserDefaults standardUserDefaults]
                           stringForKey:@"twiliosid"];
    
    NSString *twilioAuthKey = [[NSUserDefaults standardUserDefaults]
                               stringForKey:@"twilioauthkey"];
    
    NSString *bodyMessage = [[NSUserDefaults standardUserDefaults]
                             stringForKey:@"twiliomessage"];
    
    NSString *fromNumber = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"twiliofromnumber"];
    
    //   NSString *savedValue1 = [[NSUserDefaults standardUserDefaults]
    //                    stringForKey:@"textphotos"];
    
    
    
    
    
    NSUserDefaults *defaults2 = [NSUserDefaults standardUserDefaults];
    NSString *layphoto = [defaults2 objectForKey:@"textphotos"];
    
    
    for (int i = 0; i < [texttaken count]; i++)
    {
        
        NSString *text = [texttaken objectAtIndex:i];
        
        if ([bodyMessage isEqualToString:@""] || bodyMessage == nil)
        {
            NSString *urlString = [NSString stringWithFormat:@"https://%@:%@@api.twilio.com/2010-04-01/Accounts/%@/Messages", twilioSID, twilioAuthKey, twilioSID];
            
            NSURL *url = [NSURL URLWithString:urlString];
            
            
            
            
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
            [request setURL:url];
            [request setHTTPMethod:@"POST"];
            
            NSString *bodyString = [NSString stringWithFormat:@"From=%@&To=%@&Body=My Photo&MediaUrl=%@", fromNumber,savedValue,text];
            
            NSData *data3 =[bodyString dataUsingEncoding:NSUTF8StringEncoding];
            
            
            
            
            
            [request setHTTPBody:data3];
            NSError *error;
            
            NSURLResponse *response;
            
            NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            
            //Handle the received data
            
            if(error){
                NSLog(@"Error:%@", error);
            }else{
                [_browser reloadAllDataForGrid];
                NSString *receivedString = [[NSString alloc]initWithData:receivedData encoding:NSUTF8StringEncoding];
                NSLog(@"Request sent.%@",receivedString);
            }
            
            
        }
        
        else
        {
            NSString *urlString = [NSString stringWithFormat:@"https://%@:%@@api.twilio.com/2010-04-01/Accounts/%@/Messages", twilioSID, twilioAuthKey, twilioSID];
            
            NSURL *url = [NSURL URLWithString:urlString];
            
            
            
            
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
            [request setURL:url];
            [request setHTTPMethod:@"POST"];
            
            NSString *bodyString = [NSString stringWithFormat:@"From=%@&To=%@&Body=%@&MediaUrl=%@", fromNumber,savedValue,bodyMessage,text];
            
            NSData *data3 =[bodyString dataUsingEncoding:NSUTF8StringEncoding];
            
            
            
            
            [request setHTTPBody:data3];
            
            NSError *error;
            
            NSURLResponse *response;
            
            NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            
            //Handle the received data
            
            if(error){
                NSLog(@"Error:%@", error);
            }else{
                [_browser reloadAllDataForGrid];
                NSString *receivedString = [[NSString alloc]initWithData:receivedData encoding:NSUTF8StringEncoding];
                NSLog(@"Request sent.%@",receivedString);
            }
            
            
            
        }
        
    }
    
    //        if ([bodyMessage isEqualToString:@""] || bodyMessage == nil)
    //        {
    //            NSString *urlString = [NSString stringWithFormat:@"https://%@:%@@api.twilio.com/2010-04-01/Accounts/%@/Messages", twilioSID, twilioAuthKey, twilioSID];
    //
    //            NSURL *url = [NSURL URLWithString:urlString];
    //
    //
    //
    //
    //            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    //            [request setURL:url];
    //            [request setHTTPMethod:@"POST"];
    //
    //            NSString *bodyString = [NSString stringWithFormat:@"From=%@&To=%@&Body=My Photo&MediaUrl=%@", fromNumber,savedValue,layphoto];
    //
    //            NSData *data3 =[bodyString dataUsingEncoding:NSUTF8StringEncoding];
    //
    //            [request setHTTPBody:data3];
    //
    //            NSError *error;
    //
    //            NSURLResponse *response;
    //
    //            NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    //
    //            //Handle the received data
    //
    //            if(error){
    //                NSLog(@"Error:%@", error);
    //            }else{
    //                NSString *receivedString = [[NSString alloc]initWithData:receivedData encoding:NSUTF8StringEncoding];
    //                NSLog(@"Request sent.%@",receivedString);
    //            }
    //
    //
    //
    //        }
    //
    //        else
    //        {
    //            NSString *urlString = [NSString stringWithFormat:@"https://%@:%@@api.twilio.com/2010-04-01/Accounts/%@/Messages", twilioSID, twilioAuthKey, twilioSID];
    //
    //            NSURL *url = [NSURL URLWithString:urlString];
    //
    //
    //
    //
    //            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    //            [request setURL:url];
    //            [request setHTTPMethod:@"POST"];
    //
    //            NSString *bodyString = [NSString stringWithFormat:@"From=%@&To=%@&Body=%@&MediaUrl=%@", fromNumber,savedValue,bodyMessage,layphoto];
    //
    //            NSData *data3 =[bodyString dataUsingEncoding:NSUTF8StringEncoding];
    //            [request setHTTPBody:data3];
    //
    //            NSError *error;
    //
    //            NSURLResponse *response;
    //
    //            NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    //
    //            //Handle the received data
    //
    //            if(error){
    //                NSLog(@"Error:%@", error);
    //            }else{
    //                NSString *receivedString = [[NSString alloc]initWithData:receivedData encoding:NSUTF8StringEncoding];
    //                NSLog(@"Request sent.%@",receivedString);
    //            }
    //
    //
    //
    //
    //        }
    
    
    
    
    
    
    
    
}




//DROPBOX//



- (DBRestClient *)restClient
{
    if (restClient == nil) {
        if ( [[DBSession sharedSession].userIds count] ) {
            restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
            restClient.delegate = self;
        }
    }
    
    return restClient;
}

-(void)getPath
{
    Uppaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    documentsDirectory = [Uppaths objectAtIndex:0];
}

-(void)loginWithViewController:(UIViewController *)selfViewCtrl
{
    [[DBSession sharedSession]linkFromController:selfViewCtrl];
}


-(void)logOut
{
    [[DBSession sharedSession] unlinkAll];
}

#pragma mark - Dropbox uploading Response

- (void)restClient:(DBRestClient*)client uploadedFile:(NSString*)destPath
              from:(NSString*)srcPath metadata:(DBMetadata*)metadata
{
    
    NSLog(@"File uploaded successfully to path: %@", metadata.path);
    
    [[self restClient] loadSharableLinkForFile:metadata.path shortUrl:NO] ;
    
    
    
    value++;
    if ( value==arrayValue)
    {
        
        NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
        [center postNotificationName:@"UploadFinish" object: self];
        value=0;
    }
    
}

- (void)restClient:(DBRestClient*)client uploadFileFailedWithError:(NSError*)error
{
    NSLog(@"File upload failed with error - %@", error);
    value++;
    if ( value==arrayValue)
    {
        
        NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
        [center postNotificationName:@"UploadFinish" object: self];
        value=0;
    }
    
}

#pragma mark -Get Dropbox Uploaded File's Link

- (void)restClient:(DBRestClient*)restClient loadedSharableLink:(NSString*)link
           forFile:(NSString*)path
{
    
    NSLog(@"Old link %@",link);
    NSString *newString = [link substringToIndex:[link length]-4];
    NSString *addString = @"dl=1";
    NSString *complete = [NSString stringWithFormat:@"%@%@", newString,
                          addString];
    
    
    
    NSLog(@"New link %@",newString);
    NSLog(@"Complete Path %@ ",complete);
    //    [[NSUserDefaults standardUserDefaults] setObject:complete forKey:@"textphotos"];
    //    [[NSUserDefaults standardUserDefaults] synchronize];
    //
    
    
    
    [texttaken addObject:complete];
    
    
    
    
    
    
}





- (void)restClient:(DBRestClient*)restClient loadSharableLinkFailedWithError:(NSError*)error
{
    NSLog(@"Error %@",error);
}






//THIS BUTTON RETURNS TO THE ADMIN
- (IBAction)GoBack {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    TSPViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"Home"];
    [vc setManagedObjectContext:self.managedObjectContext];
    
    [vc setModalPresentationStyle:UIModalPresentationFullScreen];
    [self presentViewController:vc animated:NO completion:nil];
}






- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    
    {
        //        if (object == _send_progress) {
        //            // Handle new fractionCompleted value
        //            send_hud.progress=_send_progress.fractionCompleted;
        //            send_hud.labelText=[NSString stringWithFormat:@"Sending... %.f%%",[(NSProgress *)object fractionCompleted] * 100];
        //            return;
        //        }else if (object == _receive_progress){
        //            receive_hud.progress=_receive_progress.fractionCompleted;
        //            receive_hud.labelText=[NSString stringWithFormat:@"Receiving... %.f%%",[(NSProgress *)object fractionCompleted] * 100];
        //            return;
        //        }
        
    }
    
    
}

//DEALLOC//
-(void) dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver: self];
    [[UIDevice currentDevice] endGeneratingDeviceOrientationNotifications];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

- (void)viewDidDisappear:(BOOL)animated
{
    [box logOut];
    NSLog(@"YEP. Disappeared");
}





-(void)disable {
    printport.alpha = 0.5;
    printport.userInteractionEnabled = FALSE;
}

-(void)enable {
    printport.alpha = 1.0;
    printport.userInteractionEnabled = TRUE;
}

-(void)disable1 {
    printip.alpha = 0.5;
    printip.userInteractionEnabled = FALSE;
}

-(void)enable1 {
    printip.alpha = 1.0;
    printip.userInteractionEnabled = TRUE;
}




- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}

- (BOOL)shouldAutorotate
{
    // Disable autorotation of the interface when recording is in progress
    return ! self.movieFileOutput.isRecording;
}




- (BOOL)prefersStatusBarHidden
{
    return YES;
}




//DROPBOX UPLOAD//


//KEYBOARD//


//UNSURE CALLED ACTIONS//

//- (void)updatePreviewLayer
//{
//    CGAffineTransform transform = CGAffineTransformIdentity;
//    switch ( UIDevice.currentDevice.orientation )
//    {
//        case UIDeviceOrientationLandscapeLeft:
//            transform = CGAffineTransformRotate(transform, M_PI);
//            break;
//        case UIDeviceOrientationLandscapeRight:
//            transform = CGAffineTransformRotate(transform, 0);
//            break;
//        case UIDeviceOrientationPortrait:
//            transform = CGAffineTransformRotate(transform,M_PI_2);
//            break;
//        default:
//            break;
//    }
//    videoLayer.affineTransform = transform;
//    [self.previewView.layer addSublayer:videoLayer];
//}



//WATERMARK

- (UIImage*) combineImage:(UIImage*)img {
    
    UIImage *imgWM = [UIImage imageNamed:@"watermark.png"];
    
    CGSize newSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height);
    UIGraphicsBeginImageContext( newSize );
    
    // Use existing opacity as is
    [img drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    
    // Apply supplied opacity if applicable
    [imgWM drawInRect:CGRectMake(0,0,newSize.width,newSize.height) blendMode:kCGBlendModeNormal alpha:0.8];
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}



//SHARING STATION//
- (IBAction)disconnect:(id)sender {
    
    [_appDelegate.mcManager.session disconnect];
    _nameTxt.enabled = YES;
    
    [_appDelegate.arrConnectedDevices removeAllObjects];
    [_connections reloadData];
    
}


- (void)shareYourPhotos
{
    if ([TSPAppDelegate sharedAppDelegate].selectedPhotoCount <= 0)
    {
        FCAlertView *alert = [[FCAlertView alloc] init];
        alert.darkTheme = YES;
        [alert makeAlertTypeCaution];
        alert.detachButtons = YES;
        [alert setAlertSoundWithFileName:@"error.mp3"];
        alert.blurBackground = YES;
        [alert showAlertWithTitle:@"YOU NEED AT LEAST ONE PHOTO"
                     withSubtitle:@"Please check at least one photo to share."
                  withCustomImage:nil
              withDoneButtonTitle:nil
                       andButtons:nil];
        
        return;
    }
    
    [self emailinternetcheck];
    
    email.hidden = NO;
    emailscreen.hidden = NO;
    social.hidden = NO;
    socialview.hidden = NO;
    social.hidden = NO;
    
    //    [social setBackgroundColor:[UIColor redColor]];
    //    [socialview setBackgroundColor:[UIColor yellowColor]];
    //    [emailscreen setBackgroundColor:[UIColor greenColor]];
    //    [email setBackgroundColor:[UIColor whiteColor]];
    //
    //    [social setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height)];
    
    //    UIWindow *window = [UIApplication sharedApplication].windows.lastObject;
    //    [window addSubview:social];
    //    [window bringSubviewToFront:social];
    
    
    
    [self.klcPopUp show];
    
    [_appDelegate.mcManager.browser dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)reloadGallery:(MWPhotoBrowser *)photoBrowser
{
    [self loadAssets];
    
    NSMutableArray *photos = [[NSMutableArray alloc] init];
    NSMutableArray *thumbs = [[NSMutableArray alloc] init];
    
    NSMutableArray *copy = [_assets copy];
    UIScreen *screen = [UIScreen mainScreen];
    CGFloat scale = screen.scale;
    // Sizing is very rough... more thought required in a real implementation
    CGFloat imageSize = MAX(screen.bounds.size.width, screen.bounds.size.height) * 1.5;
    CGSize imageTargetSize = CGSizeMake(imageSize * scale, imageSize * scale);
    CGSize thumbTargetSize = CGSizeMake(imageSize / 3.0 * scale, imageSize / 3.0 * scale);
    
    for (PHAsset *asset in copy)
    {
        [photos addObject:[MWPhoto photoWithAsset:asset targetSize:imageTargetSize]];
        [thumbs addObject:[MWPhoto photoWithAsset:asset targetSize:thumbTargetSize]];
    }
    
    self.photos = photos;
    self.thumbs = thumbs;
    
    return YES;
}


#pragma mark - UIImageView
-(void)setRoundedView:(UIImageView *)roundedView toDiameter:(float)newSize;
{
    CGPoint saveCenter = roundedView.center;
    CGRect newFrame = CGRectMake(roundedView.frame.origin.x, roundedView.frame.origin.y, newSize, newSize);
    roundedView.frame = newFrame;
    roundedView.layer.cornerRadius = newSize / 2.0;
    roundedView.center = saveCenter;
    
    roundedView.clipsToBounds = YES;
}



#pragma mark - Send/Receive
-(void)didStartReceivingResourceWithNotification:(NSNotification *)notification{
    //    NSProgress *progress = [[notification userInfo] objectForKey:@"progress"];
    //    _receive_progress=progress;
    //    dispatch_async(dispatch_get_main_queue(), ^{
    //        NSLog(@"receiving...");
    //        [_receive_progress addObserver:self
    //                            forKeyPath:@"fractionCompleted"
    //                               options:NSKeyValueObservingOptionNew
    //                               context:NULL];
    //        receive_hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //        //  receive_hud.mode = MBProgressHUDModeAnnularDeterminate;
    //        receive_hud.labelText=@"Receiving...";
    //
    //    });
    
}


-(void)updateReceivingProgressWithNotification:(NSNotification *)notification{
    
}


-(void)didFinishReceivingResourceWithNotification:(NSNotification *)notification{
    NSDictionary *dict = [notification userInfo];
    
    NSURL *localURL = [dict objectForKey:@"localURL"];
    //NSString *resourceName = [dict objectForKey:@"resourceName"];
    
    @try{
        [_receive_progress removeObserver:self
                               forKeyPath:@"fractionCompleted"
                                  context:NULL];
    }
    @catch (NSException *e) {
        NSLog(@"error %@",e);
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        //        [MBProgressHUD hideHUDForView:self.view animated:YES];
    });
    
    [self performSelectorOnMainThread:@selector(saveData:) withObject:localURL waitUntilDone:NO];
}


-(void)saveData:(NSURL *)url{
    
    NSData *data2 = [NSData dataWithContentsOfURL:url];
    UIImage *test = [UIImage imageWithData:data2];
    
    if (test)
    {
        NSLog(@"It's an image");
        UIImage *img= [UIImage imageWithData:data2];
        NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                                stringForKey:@"AlbumName"];
        
        
        
        [self getPath];
        NSString *prefixString = @"Image";
        
        NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
        NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
        imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",uniqueFileName]];
        UIImage *image= img;
        data = UIImageJPEGRepresentation(image,0.5);
        [data writeToFile:imagePath atomically:YES];
        NSArray* words = [savedValue componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSString* nospacestring = [words componentsJoinedByString:@""];
        
        NSString *destDir = [NSString stringWithFormat: @"/%@/", nospacestring];
        restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
        restClient.delegate = self;
        [restClient uploadFile:[NSString stringWithFormat:@"%@.jpg",uniqueFileName] toPath:destDir withParentRev:nil fromPath:imagePath];
        
        
        
        [CustomAlbum addNewAssetWithImage:img toAlbum:[CustomAlbum getMyAlbumWithName:savedValue] onSuccess:^(NSString *ImageId) {
            recentImg = ImageId;
            
            [self performLoadAssets];
            
            double delayInSeconds = 2;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                
                [self performLoadAssets];
                
                
            });
            
            
            
        } onError:^(NSError *error) {
            NSLog(@"probelm in saving image");
        }];
    }
    else{
        NSLog(@"It's a movie");
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsPath = [paths objectAtIndex:0]; //Get the docs directory
        NSString *filePath = [documentsPath stringByAppendingPathComponent:@"video.mov"]; //Add the file name
        NSLog(@"video is %@",filePath);
        [data writeToFile:filePath atomically:YES]; //Write the file
        if (UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(filePath)){
            UISaveVideoAtPathToSavedPhotosAlbum(filePath, self, @selector(video:didFinishSavingWithError:contextInfo:), nil);
        }
    }
    
}

-(void)video:(NSString *)videoPath didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo{
    if (error){
        NSLog(@"Finished with error: %@", error);
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: @"Error saving video"
                              message: [NSString stringWithFormat:@"%@",error]
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
}


-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    //    if ([identifier isEqualToString:@"chat"] || [identifier isEqualToString:@"send_files"]) {
    //        if (![[NSUserDefaults standardUserDefaults]boolForKey:@"HyperConnectProf"]) {
    //            UIAlertView *alert = [[UIAlertView alloc]
    //                                  initWithTitle: @"You have to unlock this content."
    //                                  message: @"Go to Settings to unlock all content."
    //                                  delegate: nil
    //                                  cancelButtonTitle:@"OK"
    //                                  otherButtonTitles:nil];
    //            [alert show];
    //
    //            return NO;
    //        }
    //    }
    return YES;
}




#pragma mark - MWPhotoBrowserDelegate

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return _photos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < _photos.count)
        return [_photos objectAtIndex:index];
    return nil;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser thumbPhotoAtIndex:(NSUInteger)index {
    if (index < _thumbs.count)
        return [_thumbs objectAtIndex:index];
    return nil;
}

//- (MWCaptionView *)photoBrowser:(MWPhotoBrowser *)photoBrowser captionViewForPhotoAtIndex:(NSUInteger)index {
//    MWPhoto *photo = [self.photos objectAtIndex:index];
//    MWCaptionView *captionView = [[MWCaptionView alloc] initWithPhoto:photo];
//    return [captionView autorelease];
//}

//- (void)photoBrowser:(MWPhotoBrowser *)photoBrowser actionButtonPressedForPhotoAtIndex:(NSUInteger)index {
//    NSLog(@"ACTION!");
//}

- (void)photoBrowser:(MWPhotoBrowser *)photoBrowser didDisplayPhotoAtIndex:(NSUInteger)index
{
    NSLog(@"Did start viewing photo at index %lu", (unsigned long)index);
}

- (BOOL)photoBrowser:(MWPhotoBrowser *)photoBrowser isPhotoSelectedAtIndex:(NSUInteger)index
{
    NSLog(@"SELECTION COUNT: %lu", (unsigned long)_selections.count);
    
    if (_selections.count <= 0)
    {
        return NO;
    }
    
    return [[_selections objectAtIndex:index] boolValue];
}

//- (NSString *)photoBrowser:(MWPhotoBrowser *)photoBrowser titleForPhotoAtIndex:(NSUInteger)index {
//    return [NSString stringWithFormat:@"Photo %lu", (unsigned long)index+1];
//}

- (void)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index selectedChanged:(BOOL)selected
{
    
    
    
    [_selections replaceObjectAtIndex:index withObject:[NSNumber numberWithBool:selected]];
    
    NSLog(@"Photo at index %lu selected %@", (unsigned long)index, selected ? @"YES" : @"NO");
    
    //    if (_arrSlidshowImg == nil)
    //    {
    //        _arrSlidshowImg = [[NSMutableArray alloc] init];
    //    }
    
    if (self.photoTakens == nil)
    {
        self.photoTakens = [[NSMutableArray alloc] init];
    }
    
    if (selected)
    {
        //Sort
        NSArray *array = [_assets sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:NO]]];
        
        PHAsset* assetPhoto = [array objectAtIndex:index];
        
        PHImageRequestOptions* requestOption = [[PHImageRequestOptions alloc] init];
        requestOption.resizeMode = PHImageRequestOptionsResizeModeExact;
        requestOption.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
        requestOption.synchronous = true;
        
        PHImageManager* imageManager = [[PHImageManager alloc] init];
        [imageManager requestImageDataForAsset:assetPhoto options:requestOption resultHandler:^(NSData * _Nullable imageData, NSString * _Nullable dataUTI, UIImageOrientation orientation, NSDictionary * _Nullable info) {
            
            UIImage* image = [UIImage imageWithData:imageData];
            TSPPhotoRepository* photoItem = [[TSPPhotoRepository alloc] initWithPhoto:image andIndex:index];
            
            [self.photoTakens addObject:photoItem];
            [_arrSlidshowImg addObject:image];
        }];
        
        
        
    }
    else
    {
        PHAsset* assetPhoto = [_assets objectAtIndex:index];
        
        PHImageRequestOptions* requestOption = [[PHImageRequestOptions alloc] init];
        requestOption.resizeMode = PHImageRequestOptionsResizeModeExact;
        requestOption.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
        requestOption.synchronous = true;
        
        PHImageManager* imageManager = [[PHImageManager alloc] init];
        [imageManager requestImageDataForAsset:assetPhoto options:requestOption resultHandler:^(NSData * _Nullable imageData, NSString * _Nullable dataUTI, UIImageOrientation orientation, NSDictionary * _Nullable info) {
            
            UIImage* image = [UIImage imageWithData:imageData];
            
            //     [_arrSlidshowImg removeObject:image];
            for (int i = 0; i < self.photoTakens.count;)
            {
                TSPPhotoRepository* photoSaved = [self.photoTakens objectAtIndex:i];
                if (photoSaved.index == index)
                {
                    [self.photoTakens removeObjectAtIndex:i];
                    break;
                }
                else
                    i++;
            }
            
        }];
    }
}

- (void)photoBrowserDidFinishModalPresentation:(MWPhotoBrowser *)photoBrowser {
    // If we subscribe to this method we must dismiss the view controller ourselves
    NSLog(@"Did finish modal presentation");
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Load Assets

- (void)loadAssets {
    if (NSClassFromString(@"PHAsset")) {
        
        // Check library permissions
        PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
        if (status == PHAuthorizationStatusNotDetermined) {
            [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
                if (status == PHAuthorizationStatusAuthorized) {
                    [self performLoadAssets];
                }
            }];
        } else if (status == PHAuthorizationStatusAuthorized) {
            [self performLoadAssets];
        }
        
    } else {
        
        // Assets library
        [self performLoadAssets];
        
    }
}

- (void)performLoadAssets {
    
    
    // Initialise
    _assets = [NSMutableArray new];
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"AlbumName"];
    
    
    // Load
    if (NSClassFromString(@"PHAsset")) {
        
        // Photos library iOS >= 8
        __block PHAssetCollection *collection;
        // Photos library iOS >= 8
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            PHFetchOptions *options = [PHFetchOptions new];
            //            NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"title = %@", @"test_Layout"];
            //            NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"title = %@", @"test"];
            options.predicate = [NSPredicate predicateWithFormat:@"title = %@", savedValue];
            //            options.predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate1, predicate2]];
            collection = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeAlbum
                                                                  subtype:PHAssetCollectionSubtypeAny
                                                                  options:options].firstObject;
            
            PHFetchResult *fetchResults = [PHAsset fetchAssetsInAssetCollection:collection options:nil];
            [fetchResults enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                NSLog(@"ADD ASSET");
                [_assets addObject:obj];
                
                NSMutableArray *photos = [[NSMutableArray alloc] init];
                NSMutableArray *thumbs = [[NSMutableArray alloc] init];
                
                NSMutableArray *copy = [_assets copy];
                
                NSLog(@"ASSETS COUNT: %lu", (unsigned long)_assets.count);
                
                UIScreen *screen = [UIScreen mainScreen];
                CGFloat scale = screen.scale;
                // Sizing is very rough... more thought required in a real implementation
                CGFloat imageSize = MAX(screen.bounds.size.width, screen.bounds.size.height) * 1.5;
                CGSize imageTargetSize = CGSizeMake(imageSize * scale, imageSize * scale);
                CGSize thumbTargetSize = CGSizeMake(imageSize / 3.0 * scale, imageSize / 3.0 * scale);
                
                NSArray *array = [copy sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:NO]]];
                
                
                for (PHAsset *asset in array)
                {
                    [photos addObject:[MWPhoto photoWithAsset:asset targetSize:imageTargetSize]];
                    [thumbs addObject:[MWPhoto photoWithAsset:asset targetSize:thumbTargetSize]];
                }
                
                
                
                self.photos = photos;
                NSLog(@"PHOTO COUNT: %lu", (unsigned long)photos.count);
                
                self.thumbs = thumbs;
                
                _selections = [NSMutableArray new];
                for (int i = 0; i < photos.count; i++) {
                    [_selections addObject:[NSNumber numberWithBool:NO]];
                }
                
                [_browser reloadAllDataForGrid];
                
            }];
            if (fetchResults.count > 0) {
                [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
            }
        });
        
    } else {
        
        // Assets Library iOS < 8
        _ALAssetsLibrary = [[ALAssetsLibrary alloc] init];
        
        // Run in the background as it takes a while to get all assets from the library
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            NSMutableArray *assetGroups = [[NSMutableArray alloc] init];
            NSMutableArray *assetURLDictionaries = [[NSMutableArray alloc] init];
            
            // Process assets
            void (^assetEnumerator)(ALAsset *, NSUInteger, BOOL *) = ^(ALAsset *result, NSUInteger index, BOOL *stop) {
                if (result != nil) {
                    NSString *assetType = [result valueForProperty:ALAssetPropertyType];
                    if ([assetType isEqualToString:ALAssetTypePhoto] || [assetType isEqualToString:ALAssetTypeVideo]) {
                        [assetURLDictionaries addObject:[result valueForProperty:ALAssetPropertyURLs]];
                        NSURL *url = result.defaultRepresentation.url;
                        [_ALAssetsLibrary assetForURL:url
                                          resultBlock:^(ALAsset *asset) {
                                              if (asset) {
                                                  @synchronized(_assets) {
                                                      [_assets addObject:asset];
                                                      if (_assets.count == 1) {
                                                          // Added first asset so reload data
                                                          [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
                                                      }
                                                  }
                                              }
                                          }
                                         failureBlock:^(NSError *error){
                                             NSLog(@"operation was not successfull!");
                                         }];
                        
                    }
                }
            };
            
            // Process groups
            void (^ assetGroupEnumerator) (ALAssetsGroup *, BOOL *) = ^(ALAssetsGroup *group, BOOL *stop) {
                if (group != nil) {
                    [group enumerateAssetsWithOptions:NSEnumerationReverse usingBlock:assetEnumerator];
                    [assetGroups addObject:group];
                }
            };
            
            // Process!
            [_ALAssetsLibrary enumerateGroupsWithTypes:ALAssetsGroupAll
                                            usingBlock:assetGroupEnumerator
                                          failureBlock:^(NSError *error) {
                                              NSLog(@"There is an error");
                                          }];
            
        });
        
    }
    
}








@end


