//
//  Helper.h
//  Kitchen
//
//  Created by Luong Chau Tuan on 12/31/15.
//  Copyright © 2015 Luong Chau Tuan. All rights reserved.
//

#import <Foundation/Foundation.h>

#define IS_IPHONE_5 ( fabs( ( double )([ [ UIScreen mainScreen ] bounds ].size.width > [ [ UIScreen mainScreen ] bounds ].size.height ? [ [ UIScreen mainScreen ] bounds ].size.width : [ [ UIScreen mainScreen ] bounds ].size.height) - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_4 ( fabs( ( double )([ [ UIScreen mainScreen ] bounds ].size.width > [ [ UIScreen mainScreen ] bounds ].size.height ? [ [ UIScreen mainScreen ] bounds ].size.width : [ [ UIScreen mainScreen ] bounds ].size.height) - ( double )480 ) < DBL_EPSILON )
#define IS_IPHONE_6 ( fabs( ( double )([ [ UIScreen mainScreen ] bounds ].size.width > [ [ UIScreen mainScreen ] bounds ].size.height ? [ [ UIScreen mainScreen ] bounds ].size.width : [ [ UIScreen mainScreen ] bounds ].size.height) - ( double )667 ) < DBL_EPSILON )
#define IS_IPHONE_6_PLUS ( fabs( ( double )([ [ UIScreen mainScreen ] bounds ].size.width > [ [ UIScreen mainScreen ] bounds ].size.height ? [ [ UIScreen mainScreen ] bounds ].size.width : [ [ UIScreen mainScreen ] bounds ].size.height) - ( double )736 ) < DBL_EPSILON )

FOUNDATION_EXPORT NSInteger const ANIMATION_GIF;
FOUNDATION_EXPORT NSInteger const PHOTO_BOOTH;
FOUNDATION_EXPORT NSInteger const SHARE_STATION;
FOUNDATION_EXPORT NSString* FORMAT_VIDEO;
FOUNDATION_EXPORT NSString* FORMAT_GIF;

@interface Helper : NSObject

+ (Helper *) getShareInstance;

+ (NSString *)uuid;

@end
