//
//  TSPAddToDoViewController.m
//  Done
//
//  Created by Bart Jacobs on 24/07/14.
//  Copyright (c) 2014 Tuts+. All rights reserved.
//
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "TSPAddToDoViewController.h"
#import "CNPPopupController.h"
#import "RzColorPicker_UIColor.h"
#import "ISMessages.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <QBImagePicker/QBImagePicker.h>
#import <Photos/Photos.h>
#import "PHPhotoLibrary+CustomPhotoAlbum.h"
#import "TSPViewController.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "SendGrid.h"
#import "SendGridEmail.h"
#import "SendGridEmailAttachment.h"
#import "TSPAppDelegate.h"
#import "MBProgressHUD.h"
#import "TSPEventRepository.h"
#import "Helper.h"


#define RADIANS_TO_DEGREES(radians) ((radians) * (180.0 / M_PI))
#define DEGREES_TO_RADIANS(degrees) ((degrees) * (M_PI / 180.0))
@interface TSPAddToDoViewController ()<UITextFieldDelegate, UITextViewDelegate, CNPPopupControllerDelegate,QBImagePickerControllerDelegate>
{
    
    NSString *albumId;
    NSString *recentImg;
    
    //COLOR SELECTOR//
    RzColorPickerView *colorPicker;
    
    //CUSTOM VIDEOS//
    IBOutlet NSString *welcomeimagevideo;
    IBOutlet NSString *finalimagevideo;
    
    //IMAGEBACKGROUNDS//
    IBOutlet UIImage    *welcomeimage;
    IBOutlet UIImage *custombackgroundimage;
    IBOutlet UIImage*readyimage;
    IBOutlet UIImage*remainingimage;
    IBOutlet UIImage*videoimage;
    IBOutlet UIImage*emailimage;
    IBOutlet UIImage*finalimage;
    IBOutlet UIImage *closedimage;
    IBOutlet UIImage*animatedlayoutimage;
    IBOutlet UIImage*photoboothlayoutimage;
    IBOutlet UIImage *effectsimage;
    IBOutlet UIImage *greenscreenimage;
    IBOutlet UILabel *whatselected;
    IBOutlet UIImageView *selectedimage;
 
    
    //TEXTFIELDS//
    IBOutlet UITextField *txtFldCNames;
    IBOutlet UITextField *txtFldEDate;
    IBOutlet UITextView*   lblEmailMsg;
    IBOutlet UITextField*   lblEmailSub;
    IBOutlet UITextField*   username;
    IBOutlet UITextField*   password;
    IBOutlet UITextField*   emailaddress;
    //VIEWS//
    IBOutlet UIView     *viewCustomScreens;
    IBOutlet UIScrollView    *animcell;
    IBOutlet UIScrollView    *animcell1;
    IBOutlet UIView     *animcellcont;
    IBOutlet UIStackView     *animcellcont1;
    IBOutlet UIView     *bgcol;
    IBOutlet UIView     *fontcol;
     IBOutlet UIView     *layoutselected;
    
    //SWITCHES//
    IBOutlet UISwitch *switchGreen;
    IBOutlet UISwitch *switchEmail;
    IBOutlet UISwitch *switchTwitter;
    IBOutlet UISwitch *switchFacebook;
    IBOutlet UISwitch *switchAnimatedLayout;
    IBOutlet UISwitch *switchGif;
    IBOutlet UISwitch *burstmode;
    IBOutlet UISwitch *switchBlackWhite;
    IBOutlet UISwitch *switchVintage;
    IBOutlet UISwitch *switchSepia;
    IBOutlet UISwitch *switchColor;
    IBOutlet UISwitch *switchIndividual;
    IBOutlet UISwitch *switchLayout;
    IBOutlet UISwitch *switchFacebookIndividual;
    IBOutlet UISwitch *switchFacebookLayout;
    IBOutlet UISwitch *switchTwitterIndividual;
    IBOutlet UISwitch *switchTwitterLayout;
    IBOutlet UISwitch *switchTextIndividual;
    IBOutlet UISwitch *switchTextLayout;
    IBOutlet UISwitch *switchSharingIndividual;
    IBOutlet UISwitch *switchSharingLayout;
    IBOutlet UISwitch *switchOverlay;
    IBOutlet UISwitch *switchOverlayAnimatedGIFBackground;
    IBOutlet UISwitch *switchRear;
    IBOutlet UISwitch *switchVideo;
    IBOutlet UISwitch *switchSharing;
    IBOutlet UISwitch *switchMessage;
    IBOutlet UISwitch *switchCustom;
    IBOutlet UISwitch *switchMessageAnim;
    IBOutlet UISwitch *switchFinalAnim;
    IBOutlet UISwitch *switchPhotoBoothBackground;
    IBOutlet UISwitch *selectimage;
    IBOutlet UISwitch *switchText;
    IBOutlet UISwitch *booth;
    IBOutlet UISwitch *share;
    IBOutlet UISwitch *animated;
    IBOutlet UISwitch *switchsendgrid;
    IBOutlet UISwitch *boomerang;
    //LABELS//
    IBOutlet UILabel *maxSelection2;
    IBOutlet UILabel *animatednumber;
    IBOutlet UILabel *animatedduration;
    IBOutlet UILabel *maxSelection1;
    IBOutlet UILabel *photopreviewlab;
    IBOutlet UILabel *maxSelectionSharingLabel;
    //STEPPERS//
    IBOutlet UIStepper *animatedduration1;
    IBOutlet UIStepper *animatednumber1;
    IBOutlet UIStepper *maxSelectionTimer1;
    IBOutlet UIStepper *maxSelectionTimer2;
    IBOutlet UIStepper *maxSelectionSharingStepper;
    IBOutlet UIStepper *photopreviewstepper;
    
    //BUTTONS//
    IBOutlet UIButton *selectprintbackgroundcolor;
    IBOutlet UIButton *selectprintfontbordercolor;
    IBOutlet UIButton *selectbackgroundcolor;
    IBOutlet UIButton *selectfontbordercolor;
    IBOutlet UIButton *selectbackgroundcoloranim;
    IBOutlet UIButton *selectfontbordercoloranim;
    IBOutlet UIButton *deletebord;
    IBOutlet UIButton *savebord;
    IBOutlet UIButton *vertlay;
    IBOutlet UIButton *horizontallay;
    IBOutlet UIButton *btnSizePrint;
    
    //CUSTOM LAYOUT//
    IBOutlet UIButton *switchOverlayCustom;
    IBOutlet UISwitch *switchOverlay1;
    IBOutlet UIView     *guide;
    IBOutlet UIView     *guide1;
    IBOutlet UIView     *guide2;
    IBOutlet UIView     *guide3;
    IBOutlet UIButton *add;
    IBOutlet UIButton *back;
    IBOutlet UIButton *resetlay;
    IBOutlet UIButton *bringfront;
    IBOutlet UIButton *emaillaycus;
    IBOutlet UIButton *cancelcus;
    IBOutlet UIButton *donecus;
    IBOutlet UILabel *splitlab;
    IBOutlet UILabel *scalelab;
    IBOutlet UILabel *rotlab;
    IBOutlet UILabel *mlrlab;
    IBOutlet UILabel *mtblab;
    IBOutlet UILabel *radlab;
    IBOutlet UILabel *overlab;
    IBOutlet UITextField *txtScale;
    IBOutlet UITextField *txtRotate;
    IBOutlet UIStepper *rotateSlider;
    IBOutlet UIStepper *scaleSlider;
    IBOutlet UIStepper *changeRadius;
    IBOutlet UITextField *txtChangeRadius;
    IBOutlet UISwitch *splitlayout;
    IBOutlet UITextField *txtMoveX;
    IBOutlet UITextField *txtMoveY;
    IBOutlet UIStepper *moveImageX;
    IBOutlet UIStepper *moveImageY;
    
 
    IBOutlet UIStackView     *test;
    
    
    Rectangle *selectedRectangle;
    
    IBOutlet CustomScrollView *fourBySixScrollView;
    IBOutlet UIView *fourbysix;
    IBOutlet CustomScrollView *fiveBySevenScrollView;
    IBOutlet UIView *fivebyseven;
    IBOutlet CustomScrollView *fourBySixScrollViewh;
    IBOutlet UIView *fourbysixh;
    IBOutlet CustomScrollView *fiveBySevenScrollViewh;
    IBOutlet UIView *fivebysevenh;
    UITapGestureRecognizer *doubleTap;
    UITapGestureRecognizer *doubleTap1;
    UITapGestureRecognizer *doubleTap2;
    UITapGestureRecognizer *doubleTap3;
    
    
    NSMutableArray *mArrImages;
    int mIsLoaded;
    int mColorType;
    
    UIImageView *fourBySixBackgroundImageView;
    UIImageView *fiveBySevenBackgroundImageView;
    UIImageView *fourBySixBackgroundImageViewh;
    UIImageView *fiveBySevenBackgroundImageViewh;
    MFMailComposeViewController *mailComposer;
    NSInteger loadedEventIndex;
}

@property (nonatomic, weak) IBOutlet UIView *colorlayout;
@property (nonatomic, weak) IBOutlet UIView *imageslayout;
@property (nonatomic, strong) CNPPopupController *popupController;


//CUSTOM LAYOUT//
@property (nonatomic, strong) UIImage *mImgBack;
@property (nonatomic, strong) UIColor *currentColor;
@property (nonatomic,strong) NSMutableArray *fourBySixRectangleArray;
@property (nonatomic,strong) NSMutableArray *fiveBySevenRectangleArray;
@property (nonatomic,strong) NSMutableArray *fourBySixRectangleArrayh;
@property (nonatomic,strong) NSMutableArray *fiveBySevenRectangleArrayh;
@property (nonatomic,strong) IBOutlet UISegmentedControl *segmentedControl;
- (void)addGestureRecognizersToView:(UIView *)theView;
- (void)adjustAnchorPointForGestureRecognizer:(UIGestureRecognizer *)recognizer;
- (void)handlePan:(UIPanGestureRecognizer *)recognizer;
- (void)handleRotate:(UIRotationGestureRecognizer *)recognizer;
- (void)handleScale:(UIPinchGestureRecognizer *)recognizer;
- (void)handleDoubleTap:(UITapGestureRecognizer *)recognizer;
- (void)handleDoubleTap1:(UITapGestureRecognizer *)recognizer;
//- (void)handleTwoFingerTap:(UITapGestureRecognizer *)recognizer;

- (IBAction)scaleImage:(UIStepper *)sender;
- (IBAction)rotateImage:(UIStepper *)sender;

//PHOTO PICKER//
@property (nonatomic, weak) IBOutlet UISegmentedControl *savinggif;
@property (nonatomic, weak) IBOutlet UISegmentedControl *template;
@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *CustomBackground;
@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *Welcome;
@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *Ready;
@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *Countdown;
@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *Remaining;
@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *Video;
@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *Email;
@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *Final;
@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *Closed;
@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *AnimatedLayout;
@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *PhotoBoothLayout;
@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *Effects;
@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *GreenScreen;
@property (nonatomic, strong) PHPhotoLibrary* library;

@end
int maxSelection2;
int maxSelectionSharing;
int animatednumber;
int animatedduration;
int maxSelection1;

@implementation TSPAddToDoViewController
{
    QBImagePickerController *imageMediaPickerCustomLayoutBackground;
    QBImagePickerController *imageMediaPickerGreenScreen;
    QBImagePickerController *imageMediaPickerPrintLayoutBackground;
    QBImagePickerController *imageMediaPickerWelcome;
    QBImagePickerController *imageMediaPickerCountdown;
    QBImagePickerController *imageMediaPickerEmail;
    QBImagePickerController *imageMediaPickerEffects;
    QBImagePickerController *imageMediaPickerRemaining;
    QBImagePickerController *imageMediaPickerFinal;
    QBImagePickerController *imageMediaPickerReady;
    QBImagePickerController *imageMediaPickerVideo;
    QBImagePickerController *imageMediaPickerClosed;
    QBImagePickerController *imageMediaPickerAnimatedLayout;
}

@synthesize CustomBackground;
@synthesize Welcome;
@synthesize Ready;
@synthesize Countdown;
@synthesize Remaining;
@synthesize Video;
@synthesize Email;
@synthesize Final;
@synthesize Closed;
@synthesize AnimatedLayout;
@synthesize PhotoBoothLayout;
@synthesize Effects;
@synthesize GreenScreen;




@synthesize LayoutBackgroundColor;
@synthesize LayoutFontBorderColor;
@synthesize PrintBackgroundColor;
@synthesize PrintFontBorderColor;
@synthesize scrollviewlayout;
@synthesize buttonborders;
@synthesize buttonborders1;
@synthesize instbut;
@synthesize viewborders;
@synthesize lineseparator;
@synthesize labelcolors;
@synthesize lineseparator1;
@synthesize labelcolors1;
@synthesize lineseparator2;
@synthesize labelcolors2;
@synthesize textfields;
@synthesize switches;
@synthesize stepper;



- (void)viewDidLoad {
    [super viewDidLoad];
 
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *hideview = @"yes";
    [defaults setObject:hideview forKey:@"hideview"];
    [defaults synchronize];
    layoutselected.hidden=YES;
    
    PrintFontBorderColor = [UIColor blackColor];
    PrintBackgroundColor = [UIColor whiteColor];
    LayoutFontBorderColor = [UIColor whiteColor];
    LayoutBackgroundColor = [UIColor blackColor];
    
    
    [photopreviewstepper setMinimumValue:0];
    [photopreviewstepper setContinuous:YES];
    [photopreviewstepper setWraps:NO];
    [photopreviewstepper setStepValue:1];
    [photopreviewstepper setMaximumValue:10];
    photopreviewlab.text=[NSString stringWithFormat:@"%g",photopreviewstepper.value];
    
    [animatednumber1 setMinimumValue:0];
    [animatednumber1 setContinuous:YES];
    [animatednumber1 setWraps:NO];
    [animatednumber1 setStepValue:1];
    [animatednumber1 setMaximumValue:100];
    animatednumber.text=[NSString stringWithFormat:@"%g",animatednumber1.value];
    [animatedduration1 setMinimumValue:0];
    [animatedduration1 setContinuous:YES];
    [animatedduration1 setWraps:NO];
    [animatedduration1 setStepValue:1];
    [animatedduration1 setMaximumValue:10];
    animatedduration.text=[NSString stringWithFormat:@"%g",animatedduration1.value];
    [maxSelectionTimer1 setMinimumValue:0];
    [maxSelectionTimer1 setContinuous:YES];
    [maxSelectionTimer1 setWraps:NO];
    [maxSelectionTimer1 setStepValue:1];
    [maxSelectionTimer1 setMaximumValue:10];
    maxSelection1.text=[NSString stringWithFormat:@"%g",maxSelectionTimer1.value];
    [maxSelectionTimer2 setMinimumValue:0];
    [maxSelectionTimer2 setContinuous:YES];
    [maxSelectionTimer2 setWraps:NO];
    [maxSelectionTimer2 setStepValue:10];
    [maxSelectionTimer2 setMaximumValue:120];
    maxSelection2.text=[NSString stringWithFormat:@"%g",maxSelectionTimer2.value];
    
    [maxSelectionSharingStepper setMinimumValue:0];
    [maxSelectionSharingStepper setContinuous:YES];
    [maxSelectionSharingStepper setWraps:NO];
    [maxSelectionSharingStepper setStepValue:1];
    [maxSelectionSharingStepper setMaximumValue:10];
    maxSelectionSharingLabel.text=[NSString stringWithFormat:@"%g",maxSelectionSharingStepper.value];
   
    
    
    
    
    txtFldEDate.layer.borderColor=[[UIColor whiteColor]CGColor];
    txtFldEDate.layer.borderWidth= 1.0f;
    txtFldCNames.layer.borderColor=[[UIColor whiteColor]CGColor];
    txtFldCNames.layer.borderWidth= 1.0f;
     [txtFldCNames setValue:[UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    [txtFldEDate setValue:[UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    [[deletebord layer] setBorderWidth:1.0f];
    [[deletebord layer] setBorderColor:[UIColor redColor].CGColor];
    [[savebord layer] setBorderWidth:1.0f];
    [[savebord layer] setBorderColor:[UIColor colorWithRed:255/255.0 green:102/255.0 blue:102/255.0 alpha:1.0].CGColor];
    [animcell addSubview:animcellcont];
    [animcell1 addSubview:animcellcont1];
    
   
    for (UIButton *button in buttonborders) {
        [[button layer] setBorderWidth:1.0f];
        [[button layer] setBorderColor:[UIColor darkGrayColor].CGColor];
    }
    
    for (UIButton *button in buttonborders1) {
        [[button layer] setBorderWidth:1.0f];
        [[button layer] setBorderColor:[UIColor whiteColor].CGColor];
    }
    
    
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown) {
        CGRect scrollFrame;
        scrollFrame.origin = animcell1.frame.origin;
        scrollFrame.size = CGSizeMake(700, 2345);
        animcell1.frame = scrollFrame;
        animcell1.contentSize = scrollFrame.size;
        
        CGRect scrollFrame1;
        scrollFrame1.origin = animcell.frame.origin;
        scrollFrame1.size = CGSizeMake(animcellcont1.frame.size.width,animcellcont1.frame.size.height);
        animcell.frame = scrollFrame1;
        animcell.contentSize = scrollFrame1.size;
        
        scrollviewlayout = [[UIScrollView alloc]initWithFrame:CGRectMake(0 ,0 ,320 ,260)];
        [scrollviewlayout setShowsHorizontalScrollIndicator:YES];
        [scrollviewlayout setShowsVerticalScrollIndicator:NO];
        scrollviewlayout.scrollEnabled= YES;
        scrollviewlayout.userInteractionEnabled= YES;
        scrollviewlayout.contentSize= CGSizeMake(2850 ,260);
        
        for (int i=0; i<17; i++) {
            UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(28*i+10, -50, 11, 260) ];
            imgView.contentMode = UIViewContentModeScaleAspectFit;
            imgView.tag = i+20;
            [scrollviewlayout addSubview:imgView];
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
            NSLog(@"Scroll view image: %@", [NSString stringWithFormat:@"vertical%d_thumbnail", i+1]);
            [btn setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"vertical%d_thumbnail", i+2]] forState:UIControlStateNormal];
            btn.frame = CGRectMake(178*i+0, 0, 171, 260);
            btn.tag = i;
            [scrollviewlayout addSubview:btn];
        }
        
        for (UIView *viewborder in viewborders) {
            viewborder.layer.borderWidth = 1;
            viewborder.layer.borderColor = [UIColor whiteColor].CGColor;
            viewborder.translatesAutoresizingMaskIntoConstraints = YES;
            CGRect frame = viewborder.frame;
            frame.size.width = 728.0f;
            viewborder.frame = frame;
            
        }
        
        
        CGRect frame = _segmentedControl.frame;
        frame.origin.y=25;//pass the cordinate which you want
        frame.origin.x= 20;//pass the cordinate which you want
        _segmentedControl.frame= frame;
        
        CGRect frame1 = add.frame;
        frame1.origin.y=18;//pass the cordinate which you want
        frame1.origin.x= 350;//pass the cordinate which you want
        add.frame= frame1;
        
        CGRect frame2 = splitlab.frame;
        frame2.origin.y=27;//pass the cordinate which you want
        frame2.origin.x= 534;//pass the cordinate which you want
        splitlab.frame= frame2;
        
        CGRect frame3 = splitlayout.frame;
        frame3.origin.y=24;//pass the cordinate which you want
        frame3.origin.x= 645;//pass the cordinate which you want
        splitlayout.frame= frame3;
        
        CGRect frame4 = fourBySixScrollView.frame;
        frame4.origin.y=296;//pass the cordinate which you want
        frame4.origin.x= 146;//pass the cordinate which you want
        fourBySixScrollView.frame= frame4;
        
        CGRect frame5 = fourBySixScrollViewh.frame;
        frame5.origin.y=403;//pass the cordinate which you want
        frame5.origin.x= 27;//pass the cordinate which you want
        fourBySixScrollViewh.frame= frame5;
        
        CGRect frame6 = fiveBySevenScrollView.frame;
        frame6.origin.y=296;//pass the cordinate which you want
        frame6.origin.x= 129;//pass the cordinate which you want
        fiveBySevenScrollView.frame= frame6;
        
        CGRect frame7 = fiveBySevenScrollViewh.frame;
        frame7.origin.y=387;//pass the cordinate which you want
        frame7.origin.x= 27;//pass the cordinate which you want
        fiveBySevenScrollViewh.frame= frame7;
        
        CGRect frame8 = scalelab.frame;
        frame8.origin.y=80;//pass the cordinate which you want
        frame8.origin.x= 20;//pass the cordinate which you want
        scalelab.frame= frame8;
        
        CGRect frame9 = scaleSlider.frame;
        frame9.origin.y=105;//pass the cordinate which you want
        frame9.origin.x= 20;//pass the cordinate which you want
        scaleSlider.frame= frame9;
        
        CGRect frame10 = txtScale.frame;
        frame10.origin.y=105;//pass the cordinate which you want
        frame10.origin.x= 120;//pass the cordinate which you want
        txtScale.frame= frame10;
        
        CGRect frame11 = rotlab.frame;
        frame11.origin.y=80;//pass the cordinate which you want
        frame11.origin.x= 220;//pass the cordinate which you want
        rotlab.frame= frame11;
        
        CGRect frame12 = rotateSlider.frame;
        frame12.origin.y=105;//pass the cordinate which you want
        frame12.origin.x= 220;//pass the cordinate which you want
        rotateSlider.frame= frame12;
        
        CGRect frame13 = txtRotate.frame;
        frame13.origin.y=105;//pass the cordinate which you want
        frame13.origin.x= 320;//pass the cordinate which you want
        txtRotate.frame= frame13;
        
        CGRect frame14 = radlab.frame;
        frame14.origin.y=80;//pass the cordinate which you want
        frame14.origin.x= 420;//pass the cordinate which you want
        radlab.frame= frame14;
        
        CGRect frame15 = changeRadius.frame;
        frame15.origin.y=105;//pass the cordinate which you want
        frame15.origin.x= 420;//pass the cordinate which you want
        changeRadius.frame= frame15;
        
        CGRect frame16 = txtChangeRadius.frame;
        frame16.origin.y=105;//pass the cordinate which you want
        frame16.origin.x= 520;//pass the cordinate which you want
        txtChangeRadius.frame= frame16;
        
        CGRect frame17 = mlrlab.frame;
        frame17.origin.y=150;//pass the cordinate which you want
        frame17.origin.x= 20;//pass the cordinate which you want
        mlrlab.frame= frame17;
        
        CGRect frame18 = moveImageX.frame;
        frame18.origin.y=180;//pass the cordinate which you want
        frame18.origin.x= 20;//pass the cordinate which you want
        moveImageX.frame= frame18;
        
        CGRect frame19 = txtMoveX.frame;
        frame19.origin.y=180;//pass the cordinate which you want
        frame19.origin.x= 120;//pass the cordinate which you want
        txtMoveX.frame= frame19;
        
        CGRect frame20 = mtblab.frame;
        frame20.origin.y=150;//pass the cordinate which you want
        frame20.origin.x= 220;//pass the cordinate which you want
        mtblab.frame= frame20;
        
        CGRect frame21 = moveImageY.frame;
        frame21.origin.y=180;//pass the cordinate which you want
        frame21.origin.x= 220;//pass the cordinate which you want
        moveImageY.frame= frame21;
        
        CGRect frame22 = txtMoveY.frame;
        frame22.origin.y=180;//pass the cordinate which you want
        frame22.origin.x= 320;//pass the cordinate which you want
        txtMoveY.frame= frame22;
        
        CGRect frame23 = back.frame;
        frame23.origin.y=160;//pass the cordinate which you want
        frame23.origin.x= 420;//pass the cordinate which you want
        back.frame= frame23;
        
        CGRect frame24 = overlab.frame;
        frame24.origin.y=170;//pass the cordinate which you want
        frame24.origin.x= 595;//pass the cordinate which you want
        overlab.frame= frame24;
        
        CGRect frame25 = switchOverlay1.frame;
        frame25.origin.y=165;//pass the cordinate which you want
        frame25.origin.x= 659;//pass the cordinate which you want
        switchOverlay1.frame= frame25;
        
        CGRect frame26 = resetlay.frame;
        frame26.origin.y=230;//pass the cordinate which you want
        frame26.origin.x= 20;//pass the cordinate which you want
        resetlay.frame= frame26;
        
        CGRect frame27 = bringfront.frame;
        frame27.origin.y=230;//pass the cordinate which you want
        frame27.origin.x= 186;//pass the cordinate which you want
        bringfront.frame= frame27;
        
        CGRect frame28 = emaillaycus.frame;
        frame28.origin.y=230;//pass the cordinate which you want
        frame28.origin.x= 352;//pass the cordinate which you want
        emaillaycus.frame= frame28;
        
        CGRect frame29 = cancelcus.frame;
        frame29.origin.y=960;//pass the cordinate which you want
        frame29.origin.x= 20;//pass the cordinate which you want
        cancelcus.frame= frame29;
        
        CGRect frame30 = donecus.frame;
        frame30.origin.y=230;//pass the cordinate which you want
        frame30.origin.x= 518;//pass the cordinate which you want
        donecus.frame= frame30;
        
        
        
        
    } else {
        CGRect scrollFrame;
        scrollFrame.origin = animcell1.frame.origin;
        scrollFrame.size = CGSizeMake(980, 2345);
        animcell1.frame = scrollFrame;
        animcell1.contentSize = scrollFrame.size;
        CGRect scrollFrame1;
        scrollFrame1.origin = animcell.frame.origin;
        scrollFrame1.size = CGSizeMake(980, 2550);
        animcell.frame = scrollFrame1;
        animcell.contentSize = scrollFrame1.size;
        
        scrollviewlayout = [[UIScrollView alloc]initWithFrame:CGRectMake(0 ,0 ,320 ,260)];
        [scrollviewlayout setShowsHorizontalScrollIndicator:YES];
        [scrollviewlayout setShowsVerticalScrollIndicator:NO];
        scrollviewlayout.scrollEnabled= YES;
        scrollviewlayout.userInteractionEnabled= YES;
        scrollviewlayout.contentSize= CGSizeMake(2850 ,260);
        
        for (int i=0; i<17; i++) {
            UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(28*i+10, -50, 11, 260) ];
            imgView.contentMode = UIViewContentModeScaleAspectFit;
            imgView.tag = i+20;
            [scrollviewlayout addSubview:imgView];
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
            NSLog(@"Scroll view image: %@", [NSString stringWithFormat:@"layout%d_thumbnail", i+1]);
            [btn setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"layout%d_thumbnail", i+2]] forState:UIControlStateNormal];
            btn.frame = CGRectMake(178*i+0, 0, 171, 260);
            btn.tag = i;
            [scrollviewlayout addSubview:btn];
        }
        
        for (UIView *viewborder in viewborders) {
            viewborder.layer.borderWidth = 1;
            viewborder.layer.borderColor = [UIColor whiteColor].CGColor;
            viewborder.translatesAutoresizingMaskIntoConstraints = YES;
            CGRect frame = viewborder.frame;
            frame.size.width = 984.0f;
            viewborder.frame = frame;
            
            
        }
        
        CGRect frame = _segmentedControl.frame;
        frame.origin.y=20;//pass the cordinate which you want
        frame.origin.x= 20;//pass the cordinate which you want
        _segmentedControl.frame= frame;
        
        CGRect frame1 = add.frame;
        frame1.origin.y=77;//pass the cordinate which you want
        frame1.origin.x= 20;//pass the cordinate which you want
        add.frame= frame1;
        
        CGRect frame2 = splitlab.frame;
        frame2.origin.y=74;//pass the cordinate which you want
        frame2.origin.x= 204;//pass the cordinate which you want
        splitlab.frame= frame2;
        
        CGRect frame3 = splitlayout.frame;
        frame3.origin.y=98;//pass the cordinate which you want
        frame3.origin.x= 206;//pass the cordinate which you want
        splitlayout.frame= frame3;
        
        CGRect frame4 = fourBySixScrollView.frame;
        frame4.origin.y=31;//pass the cordinate which you want
        frame4.origin.x= 414;//pass the cordinate which you want
        fourBySixScrollView.frame= frame4;
        
        CGRect frame5 = fourBySixScrollViewh.frame;
        frame5.origin.y=151;//pass the cordinate which you want
        frame5.origin.x= 296;//pass the cordinate which you want
        fourBySixScrollViewh.frame= frame5;
        
        CGRect frame6 = fiveBySevenScrollView.frame;
        frame6.origin.y=31;//pass the cordinate which you want
        frame6.origin.x= 397;//pass the cordinate which you want
        fiveBySevenScrollView.frame= frame6;
        
        CGRect frame7 = fiveBySevenScrollViewh.frame;
        frame7.origin.y=134;//pass the cordinate which you want
        frame7.origin.x= 295;//pass the cordinate which you want
        fiveBySevenScrollViewh.frame= frame7;
        
        CGRect frame8 = scalelab.frame;
        frame8.origin.y=143;//pass the cordinate which you want
        frame8.origin.x= 20;//pass the cordinate which you want
        scalelab.frame= frame8;
        
        CGRect frame9 = scaleSlider.frame;
        frame9.origin.y=164;//pass the cordinate which you want
        frame9.origin.x= 20;//pass the cordinate which you want
        scaleSlider.frame= frame9;
        
        CGRect frame10 = txtScale.frame;
        frame10.origin.y=163;//pass the cordinate which you want
        frame10.origin.x= 127;//pass the cordinate which you want
        txtScale.frame= frame10;
        
        CGRect frame11 = rotlab.frame;
        frame11.origin.y=200;//pass the cordinate which you want
        frame11.origin.x= 20;//pass the cordinate which you want
        rotlab.frame= frame11;
        
        CGRect frame12 = rotateSlider.frame;
        frame12.origin.y=218;//pass the cordinate which you want
        frame12.origin.x= 20;//pass the cordinate which you want
        rotateSlider.frame= frame12;
        
        CGRect frame13 = txtRotate.frame;
        frame13.origin.y=217;//pass the cordinate which you want
        frame13.origin.x= 127;//pass the cordinate which you want
        txtRotate.frame= frame13;
        
        CGRect frame14 = radlab.frame;
        frame14.origin.y=375;//pass the cordinate which you want
        frame14.origin.x= 20;//pass the cordinate which you want
        radlab.frame= frame14;
        
        CGRect frame15 = changeRadius.frame;
        frame15.origin.y=400;//pass the cordinate which you want
        frame15.origin.x= 20;//pass the cordinate which you want
        changeRadius.frame= frame15;
        
        CGRect frame16 = txtChangeRadius.frame;
        frame16.origin.y=400;//pass the cordinate which you want
        frame16.origin.x= 127;//pass the cordinate which you want
        txtChangeRadius.frame= frame16;
        
        CGRect frame17 = mlrlab.frame;
        frame17.origin.y=254;//pass the cordinate which you want
        frame17.origin.x= 20;//pass the cordinate which you want
        mlrlab.frame= frame17;
        
        CGRect frame18 = moveImageX.frame;
        frame18.origin.y=279;//pass the cordinate which you want
        frame18.origin.x= 20;//pass the cordinate which you want
        moveImageX.frame= frame18;
        
        CGRect frame19 = txtMoveX.frame;
        frame19.origin.y=278;//pass the cordinate which you want
        frame19.origin.x= 127;//pass the cordinate which you want
        txtMoveX.frame= frame19;
        
        CGRect frame20 = mtblab.frame;
        frame20.origin.y=315;//pass the cordinate which you want
        frame20.origin.x= 20;//pass the cordinate which you want
        mtblab.frame= frame20;
        
        CGRect frame21 = moveImageY.frame;
        frame21.origin.y=340;//pass the cordinate which you want
        frame21.origin.x= 20;//pass the cordinate which you want
        moveImageY.frame= frame21;
        
        CGRect frame22 = txtMoveY.frame;
        frame22.origin.y=340;//pass the cordinate which you want
        frame22.origin.x= 127;//pass the cordinate which you want
        txtMoveY.frame= frame22;
        
        CGRect frame23 = back.frame;
        frame23.origin.y=454;//pass the cordinate which you want
        frame23.origin.x= 20;//pass the cordinate which you want
        back.frame= frame23;
        
        CGRect frame24 = overlab.frame;
        frame24.origin.y=451;//pass the cordinate which you want
        frame24.origin.x= 203;//pass the cordinate which you want
        overlab.frame= frame24;
        
        CGRect frame25 = switchOverlay1.frame;
        frame25.origin.y=474;//pass the cordinate which you want
        frame25.origin.x= 203;//pass the cordinate which you want
        switchOverlay1.frame= frame25;
        
        CGRect frame26 = resetlay.frame;
        frame26.origin.y=510;//pass the cordinate which you want
        frame26.origin.x= 20;//pass the cordinate which you want
        resetlay.frame= frame26;
        
        CGRect frame27 = bringfront.frame;
        frame27.origin.y=566;//pass the cordinate which you want
        frame27.origin.x= 20;//pass the cordinate which you want
        bringfront.frame= frame27;
        
        CGRect frame28 = emaillaycus.frame;
        frame28.origin.y=621;//pass the cordinate which you want
        frame28.origin.x= 20;//pass the cordinate which you want
        emaillaycus.frame= frame28;
        
        CGRect frame29 = cancelcus.frame;
        frame29.origin.y=20;//pass the cordinate which you want
        frame29.origin.x= 915;//pass the cordinate which you want
        cancelcus.frame= frame29;
        
        CGRect frame30 = donecus.frame;
        frame30.origin.y=698;//pass the cordinate which you want
        frame30.origin.x= 20;//pass the cordinate which you want
        donecus.frame= frame30;
        
        
    }
    
    
    //CUSTOMLAYOUT//
    scaleSlider.minimumValue = 0.0;
    scaleSlider.maximumValue = 5.0;
    scaleSlider.stepValue = 0.01;
    scaleSlider.value = 1.0;
    rotateSlider.minimumValue = -360.0;
    rotateSlider.maximumValue = 360.0;
    rotateSlider.value = 1.0;
    moveImageX.minimumValue = -2100.0;
    moveImageX.maximumValue = 2100.0;
    moveImageY.minimumValue = -2100.0;
    moveImageY.maximumValue = 2100.0;
    
    
    [fourbysix setClipsToBounds:YES];
    [fivebyseven setClipsToBounds:YES];
    [fourbysixh setClipsToBounds:YES];
    [fivebysevenh setClipsToBounds:YES];
    
    //Add background image view for custom layout view
    fourBySixBackgroundImageView = [[UIImageView alloc] initWithFrame:fourbysix.bounds];
    [fourbysix addSubview:fourBySixBackgroundImageView];
    fiveBySevenBackgroundImageView = [[UIImageView alloc] initWithFrame:fivebyseven.bounds];
    [fivebyseven addSubview:fiveBySevenBackgroundImageView];
    fourBySixBackgroundImageViewh = [[UIImageView alloc] initWithFrame:fourbysixh.bounds];
    [fourbysixh addSubview:fourBySixBackgroundImageViewh];
    fiveBySevenBackgroundImageViewh = [[UIImageView alloc] initWithFrame:fivebysevenh.bounds];
    [fivebysevenh addSubview:fiveBySevenBackgroundImageViewh];
    
    
    //Init arrays
    self.fourBySixRectangleArray = [NSMutableArray array];
    self.fiveBySevenRectangleArray = [NSMutableArray array];
    self.fourBySixRectangleArrayh = [NSMutableArray array];
    self.fiveBySevenRectangleArrayh = [NSMutableArray array];
    
    doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)];
    doubleTap.numberOfTapsRequired = 4;
    
    doubleTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap1:)];
    doubleTap1.numberOfTapsRequired = 4;
    
    doubleTap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap2:)];
    doubleTap2.numberOfTapsRequired = 4;
    
    doubleTap3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap3:)];
    doubleTap3.numberOfTapsRequired = 4;
    
    
    //Add double gesture recognizer for view
    [fourbysix addGestureRecognizer:doubleTap];
    [fivebyseven addGestureRecognizer:doubleTap1];
    [fourbysixh addGestureRecognizer:doubleTap2];
    [fivebysevenh addGestureRecognizer:doubleTap3];
    
    
    
    [self addGestureRecognizersToParentView:fourbysix];
    [self addGestureRecognizersToParentView:fivebyseven];
    [self addGestureRecognizersToParentView:fourbysixh];
    [self addGestureRecognizersToParentView:fivebysevenh];
    
    
}

-(IBAction)goback:(id)sender

{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    TSPAddToDoViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"Home"];
    [vc setManagedObjectContext:self.managedObjectContext];
    
    [vc setModalPresentationStyle:UIModalPresentationFullScreen];
    [self presentViewController:vc animated:NO completion:nil];
}



//SOCIAL SHARING FOR BOTH ANIMATED GIF AND PHOTO BOOTH//


-(IBAction)sharingstation:(id)sender

{
    
    if ([switchSharing isOn])
    {
        NSLog(@"Switch Sharing is ON");
        switchEmail.enabled=NO;
        switchTwitter.enabled=NO;
        switchText.enabled=NO;
        switchFacebook.enabled=NO;
        switchEmail.on = NO;
        switchTwitter.on = NO;
        switchText.on = NO;
        switchFacebook.on = NO;
        [self showSharingPopup1:CNPPopupStyleActionSheet];
    }
    else
    {
        
        NSLog(@"Switch Sharing is OFF");
        switchText.enabled=YES;
        switchEmail.enabled=YES;
        switchFacebook.enabled=YES;
        switchTwitter.enabled=YES;
    }
    
    
}



- (IBAction)showEmailPopup:(id)sender;

{
    if ([switchEmail isOn])
    {
        NSLog(@"Switch Email is On");
     [self showEmailPopup1:CNPPopupStyleActionSheet];
    }
    else
    {
       
        NSLog(@"Switch Email is Off");
    }
    
}


-(IBAction)twittertest:(id)sender

{
    if([switchTwitter isOn]){
        [self showTwitterPopup1:CNPPopupStyleActionSheet];
        
        NSLog(@"Twitter switch is ON");
    } else{
        NSLog(@"Twitter switch is OFF");
    }
    
}



-(IBAction)facebooktest:(id)sender

{
    if([switchFacebook isOn]){
      [self showFacebookPopup1:CNPPopupStyleActionSheet];
        NSLog(@"Facebook switch is ON");
    } else{
        NSLog(@"Facebook switch is OFF");
    }
    
}

-(IBAction)texttest:(id)sender

{
    if([switchText isOn]){
        [self showTextPopup1:CNPPopupStyleActionSheet];
       
        NSLog(@"Text switch is ON");
    } else{
        NSLog(@"Text switch is OFF");
    }
    
}


- (IBAction)showEmailPopup2:(id)sender;

{
    if ([switchEmail isOn])
    {
        NSLog(@"Switch Email is On");
        [self showEmailPopup3:CNPPopupStyleActionSheet];
    }
    else
    {
        
        NSLog(@"Switch Email is Off");
    }
    
}


-(IBAction)twittertest1:(id)sender

{
    if([switchTwitter isOn]){
        [self showTwitterPopup2:CNPPopupStyleActionSheet];
        
        NSLog(@"Twitter switch is ON");
    } else{
        NSLog(@"Twitter switch is OFF");
    }
    
}



-(IBAction)facebooktest1:(id)sender

{
    if([switchFacebook isOn]){
        [self showFacebookPopup2:CNPPopupStyleActionSheet];
        NSLog(@"Facebook switch is ON");
    } else{
        NSLog(@"Facebook switch is OFF");
    }
    
}

-(IBAction)texttest1:(id)sender

{
    if([switchText isOn]){
        [self showTextPopup2:CNPPopupStyleActionSheet];
        
        NSLog(@"Text switch is ON");
    } else{
        NSLog(@"Text switch is OFF");
    }
    
}


- (void)showEmailPopup1:(CNPPopupStyle)popupStyle {
   
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString* strValue =[defaults objectForKey:@"emailsubject"];
    
    NSUserDefaults *defaults1 = [NSUserDefaults standardUserDefaults];
    NSString* strValue1 = [defaults1 objectForKey:@"emailmessage"];
   
    NSUserDefaults *defaults2 = [NSUserDefaults standardUserDefaults];
    NSString* strValue2 =[defaults2 objectForKey:@"username"];
    
    NSUserDefaults *defaults3 = [NSUserDefaults standardUserDefaults];
    NSString* strValue3 = [defaults3 objectForKey:@"password"];
    
    NSUserDefaults *defaults4 = [NSUserDefaults standardUserDefaults];
    NSString* strValue4 =[defaults4 objectForKey:@"ipixemail"];

    NSUserDefaults *defaults5 = [NSUserDefaults standardUserDefaults];
    NSString* strValue5 = [defaults5 objectForKey:@"switchSendgrid"];
    
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    NSAttributedString *lineTwo = [[NSAttributedString alloc] initWithString:@"CUSTOMIZE EMAIL" attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:24], NSForegroundColorAttributeName : [UIColor colorWithRed:206.0/255.0 green:188.0/255.0 blue:178.0/255.0 alpha:1.0], NSParagraphStyleAttributeName : paragraphStyle}];
    
    UILabel *lineTwoLabel5 = [[UILabel alloc] init];
    lineTwoLabel5.numberOfLines = 0;
    lineTwoLabel5.attributedText = lineTwo;
    
        UIView *fullView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 620, 325)];
    fullView.layer.borderWidth = 2;
    fullView.layer.borderColor = [[UIColor colorWithRed:206.0/255.0 green:188.0/255.0 blue:178.0/255.0 alpha:1.0] CGColor];
    
    UIView *customView = [[UIView alloc] initWithFrame:CGRectMake(0, 20, 360, 75)];
    UIView *spacerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    UILabel *lineTwoLabel = [[UILabel alloc] init];
    lineTwoLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 100, 15)];
     lineTwoLabel.numberOfLines = 0;
    lineTwoLabel.text = @"SUBJECT";
    lineTwoLabel.textColor = [UIColor darkGrayColor];
    UITextField *textFied = [[UITextField alloc] initWithFrame:CGRectMake(20, 20, 320, 35)];
    textFied.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    textFied.layer.borderWidth= 1.0f;
    textFied.text = strValue;
    textFied.delegate = self;
    [textFied setLeftViewMode:UITextFieldViewModeAlways];
    [textFied setLeftView:spacerView];
    [customView addSubview:lineTwoLabel];
    [customView addSubview:textFied];
    
   UIView *customView7 = [[UIView alloc] initWithFrame:CGRectMake(0, 95, 360, 160)];
    UILabel *lineThreeLabel = [[UILabel alloc] init];
    lineThreeLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 15,100, 15)];
    lineThreeLabel.numberOfLines = 0;
    lineThreeLabel.text = @"MESSAGE";
    lineThreeLabel.textColor = [UIColor darkGrayColor];
    UITextView *message= [[UITextView alloc] initWithFrame:CGRectMake(20, 35, 320, 105)];
  message.contentInset = UIEdgeInsetsMake(10,10,10,10);
    [message setFont:[UIFont systemFontOfSize:17]];
    message.layer.borderWidth = 1.0f;
    message.text = strValue1;
    message.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    [customView7 addSubview:lineThreeLabel];
    [customView7 addSubview:message];
    
     UIView *customView8 = [[UIView alloc] initWithFrame:CGRectMake(0, 255, 360, 95)];
    UILabel *lineThreeLabel1 = [[UILabel alloc] init];
    lineThreeLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 110, 15)];
    lineThreeLabel1.numberOfLines = 0;
    lineThreeLabel1.text = @"INDIVIDUAL";
    lineThreeLabel1.textColor = [UIColor darkGrayColor];
    switchIndividual = [[UISwitch alloc] initWithFrame:CGRectMake(20, 20,40,40)];
    switchIndividual.onTintColor = [UIColor darkGrayColor];
    
    UILabel *lineThreeLabel3 = [[UILabel alloc] init];
    lineThreeLabel3 = [[UILabel alloc] initWithFrame:CGRectMake(190, 0, 110, 15)];
    lineThreeLabel3.numberOfLines = 0;
    lineThreeLabel3.text = @"LAYOUT";
    lineThreeLabel3.textColor = [UIColor darkGrayColor];
    switchLayout = [[UISwitch alloc] initWithFrame:CGRectMake(190, 20 ,40,40)];
    switchLayout.onTintColor = [UIColor darkGrayColor];

   
    
    [customView8 addSubview:lineThreeLabel1];
    [customView8 addSubview:switchIndividual];
    [customView8 addSubview:lineThreeLabel3];
    [customView8 addSubview:switchLayout];
    
    
    UIView *customView3 = [[UIView alloc] initWithFrame:CGRectMake(380, 0, 180, 155)];
    UILabel *lineTwoLabel4 = [[UILabel alloc] init];
    lineTwoLabel4 = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, 100, 20)];
    lineTwoLabel4.numberOfLines = 0;
    lineTwoLabel4.text = @"SENDGRID";
    lineTwoLabel4.textColor = [UIColor darkGrayColor];
    switchsendgrid = [[UISwitch alloc] initWithFrame:CGRectMake(120, 15 ,40,40)];
    switchsendgrid.onTintColor = [UIColor darkGrayColor];
   
    NSLog (@"STR IS %@", strValue5);
   // [switchsendgrid addTarget:self action:@selector(changeSwitch:) forControlEvents:UIControlEventValueChanged];
 
   


    
   UITextField *textFied5 = [[UITextField alloc] initWithFrame:CGRectMake(0, 60, 220, 35)];
    UIView *spacerView3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    textFied5.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    textFied5.layer.borderWidth= 1.0f;
    textFied5.text = strValue4;
    textFied5.delegate = self;
    [textFied5 setLeftViewMode:UITextFieldViewModeAlways];
    [textFied5 setLeftView:spacerView3];
    textFied5.placeholder = @"YOUR EMAIL ADDRESS";

    
    CNPPopupButton *button3 = [[CNPPopupButton alloc] initWithFrame:CGRectMake(0, 115, 220, 40)];
    [button3 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button3.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button3 setTitle:@"TEST SENDGRID" forState:UIControlStateNormal];
    button3.backgroundColor = [UIColor colorWithRed:206.0/255.0 green:188.0/255.0 blue:178.0/255.0 alpha:1.0];
    button3.layer.cornerRadius = 0;
    button3.selectionHandler = ^(CNPPopupButton *button3){
        
    
        
        if (switchsendgrid.on ==1)
        {
            
            
            if (textFied5.text.length == 0)
            {
                FCAlertView *alert = [[FCAlertView alloc] init];
                alert.darkTheme = YES;
                [alert makeAlertTypeCaution];
                alert.detachButtons = YES;
                [alert setAlertSoundWithFileName:@"error.mp3"];
                alert.blurBackground = YES;
                [alert showAlertWithTitle:@"ENTER YOUR EMAIL"
                             withSubtitle:@"It seems that you haven't entered your email."
                          withCustomImage:nil
                      withDoneButtonTitle:nil
                               andButtons:nil];
                
            }
            
            else {
                SendGrid *sendgrid = [SendGrid apiUser:strValue2 apiKey:strValue3];
                SendGridEmail *email4 = [[SendGridEmail alloc] init];
                
                
                
                email4.to = textFied5.text;
                email4.from = textFied5.text;
                email4.subject = @"Test email subject";
                email4.text = @"Test email message.";
                
                [sendgrid sendWithWeb:email4
                         successBlock:^(id responseObject)
                 {
                     
                     NSLog(@"YES IT WORKED");
                     FCAlertView *alert = [[FCAlertView alloc] init];
                     alert.darkTheme = YES;
                     [alert makeAlertTypeSuccess];
                     alert.detachButtons = YES;
                     alert.blurBackground = YES;
                     [alert showAlertWithTitle:@"SUCCESS"
                                  withSubtitle:@"Congratulations. Your SendGrid account is valid and connected successfully. You should receive a test message soon."
                               withCustomImage:nil
                           withDoneButtonTitle:nil
                                    andButtons:nil];
                     [defaults setObject:@"On" forKey:@"switchSendgrid"];
                     
                     
                     
                 }
                         failureBlock:^(NSError *error)
                 
                 
                 {
                     
                     
                     FCAlertView *alert = [[FCAlertView alloc] init];
                     alert.darkTheme = YES;
                     [alert makeAlertTypeCaution];
                     alert.detachButtons = YES;
                     [alert setAlertSoundWithFileName:@"error.mp3"];
                     alert.blurBackground = YES;
                     [alert showAlertWithTitle:@"SOMETHING WENT WRONG"
                                  withSubtitle:@"We tried to make sure your SendGrid account was correct. Please make sure you have internet connected for this test and that your email is the same used when you created your account."
                               withCustomImage:nil
                           withDoneButtonTitle:nil
                                    andButtons:nil];
                     
                 }];
                
                
            }
        }
        
        
        
        
        
        
        
        
        
    };

    
   
   
     [customView3 addSubview:lineTwoLabel4];
     [customView3 addSubview:textFied5];
     [customView3 addSubview:button3];
    
    [customView3 addSubview:switchsendgrid];
    
    
    CNPPopupButton *button = [[CNPPopupButton alloc] initWithFrame:CGRectMake(495, 255, 105, 50)];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button setTitle:@"SAVE" forState:UIControlStateNormal];
    button.backgroundColor = [UIColor colorWithRed:206.0/255.0 green:188.0/255.0 blue:178.0/255.0 alpha:1.0];
    button.layer.cornerRadius = 0;
    button.selectionHandler = ^(CNPPopupButton *button){
        if ((switchIndividual.on == 0) & (switchLayout.on == 0))
        {
            FCAlertView *alert = [[FCAlertView alloc] init];
            alert.darkTheme = YES;
            [alert makeAlertTypeCaution];
            alert.detachButtons = YES;
            [alert setAlertSoundWithFileName:@"error.mp3"];
            alert.blurBackground = YES;
            [alert showAlertWithTitle:@"Missing Information"
                         withSubtitle:@"You need to share at least the individual photos or the layout photo. Please choose one or both."
                      withCustomImage:nil
                  withDoneButtonTitle:nil
                           andButtons:nil];
            
            
        }
        
        else {
        NSString *emailsu = textFied.text;
        NSString *emailme  = message.text;
         NSString *emailaddress1  = textFied5.text;
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        [defaults setObject:emailaddress1 forKey:@"ipixemail"];
        [defaults setObject:emailsu forKey:@"emailsubject"];
        [defaults setObject:emailme forKey:@"emailmessage"];
        
            
            
            
            
            
            
            
        if (switchsendgrid.on == 1)
        {
            
            
      
                
             [self.popupController dismissPopupControllerAnimated:YES];
                
             
                   NSString *switchss = @"YES";
                    [defaults setValue:switchss forKey:@"switchSendgrid"];
                
          
        }
        
        if (switchsendgrid.on ==0)
        {
           
         [self.popupController dismissPopupControllerAnimated:YES];
            NSString *switchss = @"NO";
            [defaults setValue:switchss forKey:@"switchSendgrid"];
            NSLog(@"GRID OFF");

        }
        
        
        if (switchLayout.on == 0) {
            NSString *switchss = @"NO";
            [defaults setValue:switchss forKey:@"switchLayout"];
        } else if (switchLayout.on == 1) {
            NSString *switchss = @"YES";
            [defaults setValue:switchss forKey:@"switchLayout"];
          
            
        }
        if (switchIndividual.on == 0) {
            NSString *switchss = @"NO";
            [defaults setValue:switchss forKey:@"switchIndividual"];
        } else if (switchIndividual.on == 1) {
            NSString *switchss = @"YES";
            [defaults setValue:switchss forKey:@"switchIndividual"];
        
            
        }
        
      
        [defaults synchronize];
        }
      
    };
    
   
    
    CNPPopupButton *button2 = [[CNPPopupButton alloc] initWithFrame:CGRectMake(380, 255, 105, 50)];
    [button2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button2.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button2 setTitle:@"CANCEL" forState:UIControlStateNormal];
    button2.backgroundColor = [UIColor darkGrayColor];
    button2.layer.cornerRadius = 0;
    button2.selectionHandler = ^(CNPPopupButton *button){
        [switchEmail setOn:NO animated:YES];
        [self.popupController dismissPopupControllerAnimated:YES];
    };
    
    
    
     UIView *customView4 = [[UIView alloc] initWithFrame:CGRectMake(360, 20, 1, 285)];
    customView4.backgroundColor = [UIColor colorWithRed:206.0/255.0 green:188.0/255.0 blue:178.0/255.0 alpha:1.0];
     [fullView addSubview:button];
     [fullView addSubview:button2];
    [fullView addSubview:customView];
    [fullView addSubview:customView7];
     [fullView addSubview:customView8];
    [fullView addSubview:customView3];
 [fullView addSubview:customView4];
    
    
    self.popupController = [[CNPPopupController alloc] initWithContents:@[lineTwoLabel5, fullView]];
    self.popupController.theme = [CNPPopupTheme defaultTheme];
    self.popupController.theme.popupStyle = popupStyle;
    self.popupController.theme.presentationStyle = CNPPopupPresentationStyleSlideInFromTop;
    self.popupController.delegate = self;
    [self.popupController presentPopupControllerAnimated:YES];
    
    
    

    
}



- (void)showTwitterPopup1:(CNPPopupStyle)popupStyle {
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString* strValue =[defaults objectForKey:@"twitterhashtag"];
//    NSString* strValue1 =[defaults objectForKey:@"consumerkey"];
//   NSString* strValue2 =[defaults objectForKey:@"consumersecret"];
    
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    NSAttributedString *lineTwo = [[NSAttributedString alloc] initWithString:@"CUSTOM MESSAGE AND/OR HASHTAG" attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:24], NSForegroundColorAttributeName : [UIColor darkGrayColor], NSParagraphStyleAttributeName : paragraphStyle}];
    
    UILabel *lineTwoLabel = [[UILabel alloc] init];
    lineTwoLabel.numberOfLines = 0;
    lineTwoLabel.attributedText = lineTwo;
    UIView *spacerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    UITextField *textFied = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 340, 45)];
    textFied.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    textFied.layer.borderWidth= 1.0f;
    textFied.text = strValue;
    textFied.delegate = self;
    [textFied setLeftViewMode:UITextFieldViewModeAlways];
    [textFied setLeftView:spacerView];

    
    
    
    UIView *customView8 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 360, 140)];
    UILabel *lineThreeLabel1 = [[UILabel alloc] init];
    lineThreeLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 110, 15)];
    lineThreeLabel1.numberOfLines = 0;
    lineThreeLabel1.text = @"INDIVIDUAL";
    lineThreeLabel1.textColor = [UIColor darkGrayColor];
    switchTwitterIndividual = [[UISwitch alloc] initWithFrame:CGRectMake(10, 30,40,40)];
    switchTwitterIndividual.onTintColor = [UIColor darkGrayColor];
    
    UILabel *lineThreeLabel3 = [[UILabel alloc] init];
    lineThreeLabel3 = [[UILabel alloc] initWithFrame:CGRectMake(130, 10, 110, 15)];
    lineThreeLabel3.numberOfLines = 0;
    lineThreeLabel3.text = @"LAYOUT";
    lineThreeLabel3.textColor = [UIColor darkGrayColor];
    switchTwitterLayout = [[UISwitch alloc] initWithFrame:CGRectMake(130, 30 ,40,40)];
    switchTwitterLayout.onTintColor = [UIColor darkGrayColor];
    
    
    CNPPopupButton *button2 = [[CNPPopupButton alloc] initWithFrame:CGRectMake(10, 80, 140, 60)];
    [button2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button2.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button2 setTitle:@"CANCEL" forState:UIControlStateNormal];
    button2.backgroundColor = [UIColor darkGrayColor];
    button2.layer.cornerRadius = 0;
    button2.selectionHandler = ^(CNPPopupButton *button2){
        [self.popupController dismissPopupControllerAnimated:YES];
[switchTwitter setOn:NO animated:YES];
    };
    
    CNPPopupButton *button = [[CNPPopupButton alloc] initWithFrame:CGRectMake(160, 80, 190, 60)];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button setTitle:@"SAVE" forState:UIControlStateNormal];
    button.backgroundColor = [UIColor colorWithRed:206.0/255.0 green:188.0/255.0 blue:178.0/255.0 alpha:1.0];
    button.layer.cornerRadius = 0;
    button.selectionHandler = ^(CNPPopupButton *button){
        
        
        if ((switchTwitterIndividual.on == 0) & (switchTwitterLayout.on == 0))
        {
            FCAlertView *alert = [[FCAlertView alloc] init];
            alert.darkTheme = YES;
            [alert makeAlertTypeCaution];
            alert.detachButtons = YES;
            [alert setAlertSoundWithFileName:@"error.mp3"];
            alert.blurBackground = YES;
            [alert showAlertWithTitle:@"Missing Information"
                         withSubtitle:@"You need to share at least the individual photos or the layout photo. Please choose one or both."
                      withCustomImage:nil
                  withDoneButtonTitle:nil
                           andButtons:nil];
            
            
        }
        else
        {
        
        NSString *hashtag  = textFied.text;
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        if (switchTwitterLayout.on == 0) {
            NSString *switchss = @"NO";
            [defaults setValue:switchss forKey:@"switchTwitterLayout"];
            NSLog(@"Switch Twitter Layout is %@", switchss);
        } else if (switchTwitterLayout.on == 1) {
            NSString *switchss = @"YES";
            [defaults setValue:switchss forKey:@"switchTwitterLayout"];
            NSLog(@"Switch Twitter Layout is %@", switchss);
            
            
        }
        if (switchTwitterIndividual.on == 0) {
            NSString *switchss = @"NO";
            [defaults setValue:switchss forKey:@"switchTwitterIndividual"];
            NSLog(@"Switch Twitter Individual is %@", switchss);
        } else if (switchTwitterIndividual.on == 1) {
            NSString *switchss = @"YES";
            [defaults setValue:switchss forKey:@"switchTwitterIndividual"];
            NSLog(@"Switch Twitter Individual is %@", switchss);
            
            
        }
        [defaults setObject:hashtag forKey:@"twitterhashtag"];
        [defaults synchronize];
        [self.popupController dismissPopupControllerAnimated:YES];
        
        }
    };
    
    
    
    
  [customView8 addSubview:lineThreeLabel1];
    [customView8 addSubview:switchTwitterIndividual];
    [customView8 addSubview:lineThreeLabel3];
     [customView8 addSubview:switchTwitterLayout];
      [customView8 addSubview:button2];
  [customView8 addSubview:button];
  



    
    self.popupController = [[CNPPopupController alloc] initWithContents:@[lineTwoLabel, textFied, customView8]];
    self.popupController.theme = [CNPPopupTheme defaultTheme];
    self.popupController.theme.popupStyle = popupStyle;
    self.popupController.delegate = self;
    [self.popupController presentPopupControllerAnimated:YES];
    
    
   
    
    
    
}


- (void)showFacebookPopup1:(CNPPopupStyle)popupStyle {
    
     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString* strValue =[defaults objectForKey:@"fbwebsite"];
    NSString* strValue1 =[defaults objectForKey:@"fbtitle"];
    NSString* strValue2 =[defaults objectForKey:@"fbdescription"];
    NSString* strValue3 =[defaults objectForKey:@"fblogo"];
    //    NSString* strValue1 =[defaults objectForKey:@"consumerkey"];
    //   NSString* strValue2 =[defaults objectForKey:@"consumersecret"];

    UIView *spacerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    UITextField *textFied = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 340, 35)];
    textFied.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    textFied.layer.borderWidth= 1.0f;
    textFied.text = strValue;
    textFied.placeholder = @"YOUR WEBSITE";
    textFied.delegate = self;
    [textFied setLeftViewMode:UITextFieldViewModeAlways];
    [textFied setLeftView:spacerView];
    
       UIView *spacerView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    UITextField *textFied1 = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 340, 35)];
    textFied1.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    textFied1.layer.borderWidth= 1.0f;
    textFied1.text = strValue1;
    textFied1.placeholder = @"SUBJECT";
    textFied1.delegate = self;
    [textFied1 setLeftViewMode:UITextFieldViewModeAlways];
    [textFied1 setLeftView:spacerView1];
    
    UIView *spacerView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    UITextField *textFied2 = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 340, 35)];
    textFied2.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    textFied2.layer.borderWidth= 1.0f;
    textFied2.text = strValue2;
    textFied2.placeholder = @"DESCRIPTION";
    textFied2.delegate = self;
    [textFied2 setLeftViewMode:UITextFieldViewModeAlways];
    [textFied2 setLeftView:spacerView2];
    
    UIView *spacerView3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    UITextField *textFied3 = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 340, 45)];
    textFied3.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    textFied3.layer.borderWidth= 1.0f;
    textFied3.placeholder = @"YOUR LOGO (FROM WEBSITE ADDRESS)";
    textFied3.text = strValue3;
    textFied3.delegate = self;
    [textFied3 setLeftViewMode:UITextFieldViewModeAlways];
    [textFied3 setLeftView:spacerView3];

    
    
    
    UIView *customView8 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 360, 140)];
    UILabel *lineThreeLabel1 = [[UILabel alloc] init];
    lineThreeLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 110, 15)];
    lineThreeLabel1.numberOfLines = 0;
    lineThreeLabel1.text = @"INDIVIDUAL";
    lineThreeLabel1.textColor = [UIColor darkGrayColor];
    switchFacebookIndividual = [[UISwitch alloc] initWithFrame:CGRectMake(10, 30,40,40)];
    switchFacebookIndividual.onTintColor = [UIColor darkGrayColor];
    
    UILabel *lineThreeLabel3 = [[UILabel alloc] init];
    lineThreeLabel3 = [[UILabel alloc] initWithFrame:CGRectMake(130, 10, 110, 15)];
    lineThreeLabel3.numberOfLines = 0;
    lineThreeLabel3.text = @"LAYOUT";
    lineThreeLabel3.textColor = [UIColor darkGrayColor];
    switchFacebookLayout = [[UISwitch alloc] initWithFrame:CGRectMake(130, 30 ,40,40)];
    switchFacebookLayout.onTintColor = [UIColor darkGrayColor];
    
    
    CNPPopupButton *button2 = [[CNPPopupButton alloc] initWithFrame:CGRectMake(10, 80, 140, 60)];
    [button2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button2.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button2 setTitle:@"CANCEL" forState:UIControlStateNormal];
    button2.backgroundColor = [UIColor darkGrayColor];
    button2.layer.cornerRadius = 0;
    button2.selectionHandler = ^(CNPPopupButton *button2){
        [self.popupController dismissPopupControllerAnimated:YES];
         [switchFacebook setOn:NO animated:YES];
    };
    
    CNPPopupButton *button = [[CNPPopupButton alloc] initWithFrame:CGRectMake(160, 80, 190, 60)];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button setTitle:@"SAVE" forState:UIControlStateNormal];
    button.backgroundColor = [UIColor colorWithRed:206.0/255.0 green:188.0/255.0 blue:178.0/255.0 alpha:1.0];
    button.layer.cornerRadius = 0;
    button.selectionHandler = ^(CNPPopupButton *button){
        
        
        
        if ((switchFacebookIndividual.on == 0) & (switchFacebookLayout.on == 0))
        {
            FCAlertView *alert = [[FCAlertView alloc] init];
            alert.darkTheme = YES;
            [alert makeAlertTypeCaution];
            alert.detachButtons = YES;
            [alert setAlertSoundWithFileName:@"error.mp3"];
            alert.blurBackground = YES;
            [alert showAlertWithTitle:@"Missing Information"
                         withSubtitle:@"You need to share at least the individual photos or the layout photo. Please choose one or both."
                      withCustomImage:nil
                  withDoneButtonTitle:nil
                           andButtons:nil];
           
            
        }
        
        else{
            NSString *fbwebsite  = textFied.text;
            NSString *fbtitle = textFied1.text;
            NSString *fbdescription  = textFied2.text;
            NSString *fblogo  = textFied3.text;
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            
            if (switchFacebookLayout.on == 0) {
                NSString *switchss = @"NO";
                [defaults setValue:switchss forKey:@"switchFacebookLayout"];
                NSLog(@"Switch Facebook Layout is %@", switchss);
            } else if (switchFacebookLayout.on == 1) {
                NSString *switchss = @"YES";
                [defaults setValue:switchss forKey:@"switchFacebookLayout"];
                NSLog(@"Switch Facebook Layout is %@", switchss);
                
                
            }
            if (switchFacebookIndividual.on == 0) {
                NSString *switchss = @"NO";
                [defaults setValue:switchss forKey:@"switchFacebookIndividual"];
                NSLog(@"Switch Facebook Individual is %@", switchss);
            } else if (switchFacebookIndividual.on == 1) {
                NSString *switchss = @"YES";
                [defaults setValue:switchss forKey:@"switchFacebookIndividual"];
                NSLog(@"Switch Facebook Individual is %@", switchss);
                
                
            }
            [defaults setObject:fbwebsite forKey:@"fbwebsite"];
            [defaults setObject:fbtitle forKey:@"fbtitle"];
            [defaults setObject:fbdescription forKey:@"fbdescription"];
            [defaults setObject:fblogo forKey:@"fblogo"];
            [defaults synchronize];
            [self.popupController dismissPopupControllerAnimated:YES];

        }
    };
    
    
    
    
    [customView8 addSubview:lineThreeLabel1];
    [customView8 addSubview:switchFacebookIndividual];
    [customView8 addSubview:lineThreeLabel3];
    [customView8 addSubview:switchFacebookLayout];
    [customView8 addSubview:button2];
    [customView8 addSubview:button];
    
    self.popupController = [[CNPPopupController alloc] initWithContents:@[textFied,textFied1, textFied2,  textFied3, customView8]];
    self.popupController.theme = [CNPPopupTheme defaultTheme];
    self.popupController.theme.popupStyle = popupStyle;
    self.popupController.delegate = self;
    [self.popupController presentPopupControllerAnimated:YES];
    
    
    
    
    
    
}



- (void)showTextPopup1:(CNPPopupStyle)popupStyle {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString* strValue =[defaults objectForKey:@"twiliomessage"];
    //    NSString* strValue1 =[defaults objectForKey:@"consumerkey"];
    //   NSString* strValue2 =[defaults objectForKey:@"consumersecret"];
    
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    NSAttributedString *lineTwo = [[NSAttributedString alloc] initWithString:@"TEXT MESSAGE BODY" attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:24], NSForegroundColorAttributeName : [UIColor darkGrayColor], NSParagraphStyleAttributeName : paragraphStyle}];
    
    UILabel *lineTwoLabel = [[UILabel alloc] init];
    lineTwoLabel.numberOfLines = 0;
    lineTwoLabel.attributedText = lineTwo;
    UIView *spacerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    UITextField *textFied = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 340, 45)];
    textFied.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    textFied.layer.borderWidth= 1.0f;
    textFied.text = strValue;
    textFied.delegate = self;
    [textFied setLeftViewMode:UITextFieldViewModeAlways];
    [textFied setLeftView:spacerView];
    
    
    
    
    UIView *customView8 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 360, 140)];
    UILabel *lineThreeLabel1 = [[UILabel alloc] init];
    lineThreeLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 110, 15)];
    lineThreeLabel1.numberOfLines = 0;
    lineThreeLabel1.text = @"INDIVIDUAL";
    lineThreeLabel1.textColor = [UIColor darkGrayColor];
    switchTextIndividual = [[UISwitch alloc] initWithFrame:CGRectMake(10, 30,40,40)];
    switchTextIndividual.onTintColor = [UIColor darkGrayColor];
    
    UILabel *lineThreeLabel3 = [[UILabel alloc] init];
    lineThreeLabel3 = [[UILabel alloc] initWithFrame:CGRectMake(130, 10, 110, 15)];
    lineThreeLabel3.numberOfLines = 0;
    lineThreeLabel3.text = @"LAYOUT";
    lineThreeLabel3.textColor = [UIColor darkGrayColor];
    switchTextLayout = [[UISwitch alloc] initWithFrame:CGRectMake(130, 30 ,40,40)];
    switchTextLayout.onTintColor = [UIColor darkGrayColor];
    
    
    CNPPopupButton *button2 = [[CNPPopupButton alloc] initWithFrame:CGRectMake(10, 80, 140, 60)];
    [button2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button2.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button2 setTitle:@"CANCEL" forState:UIControlStateNormal];
    button2.backgroundColor = [UIColor darkGrayColor];
    button2.layer.cornerRadius = 0;
    button2.selectionHandler = ^(CNPPopupButton *button2){
        [self.popupController dismissPopupControllerAnimated:YES];
         [switchText setOn:NO animated:YES];
    };
    
    CNPPopupButton *button = [[CNPPopupButton alloc] initWithFrame:CGRectMake(160, 80, 190, 60)];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button setTitle:@"SAVE" forState:UIControlStateNormal];
    button.backgroundColor = [UIColor colorWithRed:206.0/255.0 green:188.0/255.0 blue:178.0/255.0 alpha:1.0];
    button.layer.cornerRadius = 0;
    button.selectionHandler = ^(CNPPopupButton *button){
        
        
        if ((switchTextIndividual.on == 0) & (switchTextLayout.on == 0))
        {
            FCAlertView *alert = [[FCAlertView alloc] init];
            alert.darkTheme = YES;
            [alert makeAlertTypeCaution];
            alert.detachButtons = YES;
            [alert setAlertSoundWithFileName:@"error.mp3"];
            alert.blurBackground = YES;
            [alert showAlertWithTitle:@"Missing Information"
                         withSubtitle:@"You need to share at least the individual photos or the layout photo. Please choose one or both."
                      withCustomImage:nil
                  withDoneButtonTitle:nil
                           andButtons:nil];
            
            
        }
        
        else{
        
        
        NSString *hashtag  = textFied.text;
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        if (switchTextLayout.on == 0) {
            NSString *switchss = @"NO";
            [defaults setValue:switchss forKey:@"switchTextLayout"];
            NSLog(@"Switch Text Layout is %@", switchss);
        } else if (switchTextLayout.on == 1) {
            NSString *switchss = @"YES";
            [defaults setValue:switchss forKey:@"switchTextLayout"];
            NSLog(@"Switch Text Layout is %@", switchss);
            
            
        }
        if (switchTextIndividual.on == 0) {
            NSString *switchss = @"NO";
            [defaults setValue:switchss forKey:@"switchTextIndividual"];
            NSLog(@"Switch Text Individual is %@", switchss);
        } else if (switchTextIndividual.on == 1) {
            NSString *switchss = @"YES";
            [defaults setValue:switchss forKey:@"switchTextIndividual"];
            NSLog(@"Switch Text Individual is %@", switchss);
            
            
        }
        [defaults setObject:hashtag forKey:@"twiliomessage"];
        [defaults synchronize];
        [self.popupController dismissPopupControllerAnimated:YES];
        
        }
    };
    
    
    
    
    [customView8 addSubview:lineThreeLabel1];
    [customView8 addSubview:switchTextIndividual];
    [customView8 addSubview:lineThreeLabel3];
    [customView8 addSubview:switchTextLayout];
    [customView8 addSubview:button2];
    [customView8 addSubview:button];
    
    self.popupController = [[CNPPopupController alloc] initWithContents:@[lineTwoLabel, textFied, customView8]];
    self.popupController.theme = [CNPPopupTheme defaultTheme];
    self.popupController.theme.popupStyle = popupStyle;
    self.popupController.delegate = self;
    [self.popupController presentPopupControllerAnimated:YES];
    
    
}




- (void)showEmailPopup3:(CNPPopupStyle)popupStyle {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString* strValue =[defaults objectForKey:@"emailsubject"];
    
    NSUserDefaults *defaults1 = [NSUserDefaults standardUserDefaults];
    NSString* strValue1 = [defaults1 objectForKey:@"emailmessage"];
    
    NSUserDefaults *defaults2 = [NSUserDefaults standardUserDefaults];
    NSString* strValue2 =[defaults2 objectForKey:@"username"];
    
    NSUserDefaults *defaults3 = [NSUserDefaults standardUserDefaults];
    NSString* strValue3 = [defaults3 objectForKey:@"password"];
    
    NSUserDefaults *defaults4 = [NSUserDefaults standardUserDefaults];
    NSString* strValue4 =[defaults4 objectForKey:@"ipixemail"];
    
    NSUserDefaults *defaults5 = [NSUserDefaults standardUserDefaults];
    NSString* strValue5 = [defaults5 objectForKey:@"switchSendgrid"];
    
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    NSAttributedString *lineTwo = [[NSAttributedString alloc] initWithString:@"CUSTOMIZE EMAIL" attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:24], NSForegroundColorAttributeName : [UIColor colorWithRed:206.0/255.0 green:188.0/255.0 blue:178.0/255.0 alpha:1.0], NSParagraphStyleAttributeName : paragraphStyle}];
    
    UILabel *lineTwoLabel5 = [[UILabel alloc] init];
    lineTwoLabel5.numberOfLines = 0;
    lineTwoLabel5.attributedText = lineTwo;
    
    UIView *fullView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 620, 325)];
    fullView.layer.borderWidth = 2;
    fullView.layer.borderColor = [[UIColor colorWithRed:206.0/255.0 green:188.0/255.0 blue:178.0/255.0 alpha:1.0] CGColor];
    
    UIView *customView = [[UIView alloc] initWithFrame:CGRectMake(0, 20, 360, 75)];
    UIView *spacerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    UILabel *lineTwoLabel = [[UILabel alloc] init];
    lineTwoLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 100, 15)];
    lineTwoLabel.numberOfLines = 0;
    lineTwoLabel.text = @"SUBJECT";
    lineTwoLabel.textColor = [UIColor darkGrayColor];
    UITextField *textFied = [[UITextField alloc] initWithFrame:CGRectMake(20, 20, 320, 35)];
    textFied.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    textFied.layer.borderWidth= 1.0f;
    textFied.text = strValue;
    textFied.delegate = self;
    [textFied setLeftViewMode:UITextFieldViewModeAlways];
    [textFied setLeftView:spacerView];
    [customView addSubview:lineTwoLabel];
    [customView addSubview:textFied];
    
    UIView *customView7 = [[UIView alloc] initWithFrame:CGRectMake(0, 95, 360, 160)];
    UILabel *lineThreeLabel = [[UILabel alloc] init];
    lineThreeLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 15,100, 15)];
    lineThreeLabel.numberOfLines = 0;
    lineThreeLabel.text = @"MESSAGE";
    lineThreeLabel.textColor = [UIColor darkGrayColor];
    UITextView *message= [[UITextView alloc] initWithFrame:CGRectMake(20, 35, 320, 105)];
    message.contentInset = UIEdgeInsetsMake(10,10,10,10);
    [message setFont:[UIFont systemFontOfSize:17]];
    message.layer.borderWidth = 1.0f;
    message.text = strValue1;
    message.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    [customView7 addSubview:lineThreeLabel];
    [customView7 addSubview:message];
    
   
    
    
    UIView *customView3 = [[UIView alloc] initWithFrame:CGRectMake(380, 0, 180, 155)];
    UILabel *lineTwoLabel4 = [[UILabel alloc] init];
    lineTwoLabel4 = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, 100, 20)];
    lineTwoLabel4.numberOfLines = 0;
    lineTwoLabel4.text = @"SENDGRID";
    lineTwoLabel4.textColor = [UIColor darkGrayColor];
    switchsendgrid = [[UISwitch alloc] initWithFrame:CGRectMake(120, 15 ,40,40)];
    switchsendgrid.onTintColor = [UIColor darkGrayColor];
    
    NSLog (@"STR IS %@", strValue5);
    // [switchsendgrid addTarget:self action:@selector(changeSwitch:) forControlEvents:UIControlEventValueChanged];
    
    
    
    
    
    UITextField *textFied5 = [[UITextField alloc] initWithFrame:CGRectMake(0, 60, 220, 35)];
    UIView *spacerView3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    textFied5.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    textFied5.layer.borderWidth= 1.0f;
    textFied5.text = strValue4;
    textFied5.delegate = self;
    [textFied5 setLeftViewMode:UITextFieldViewModeAlways];
    [textFied5 setLeftView:spacerView3];
    textFied5.placeholder = @"YOUR EMAIL ADDRESS";
    
    
    CNPPopupButton *button3 = [[CNPPopupButton alloc] initWithFrame:CGRectMake(0, 115, 220, 40)];
    [button3 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button3.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button3 setTitle:@"TEST SENDGRID" forState:UIControlStateNormal];
    button3.backgroundColor = [UIColor colorWithRed:206.0/255.0 green:188.0/255.0 blue:178.0/255.0 alpha:1.0];
    button3.layer.cornerRadius = 0;
    button3.selectionHandler = ^(CNPPopupButton *button3){
        
        
        
        if (switchsendgrid.on ==1)
        {
            
            
            if (textFied5.text.length == 0)
            {
                FCAlertView *alert = [[FCAlertView alloc] init];
                alert.darkTheme = YES;
                [alert makeAlertTypeCaution];
                alert.detachButtons = YES;
                [alert setAlertSoundWithFileName:@"error.mp3"];
                alert.blurBackground = YES;
                [alert showAlertWithTitle:@"ENTER YOUR EMAIL"
                             withSubtitle:@"It seems that you haven't entered your email."
                          withCustomImage:nil
                      withDoneButtonTitle:nil
                               andButtons:nil];
                
            }
            
            else {
            SendGrid *sendgrid = [SendGrid apiUser:strValue2 apiKey:strValue3];
            SendGridEmail *email4 = [[SendGridEmail alloc] init];
            
            
            
            email4.to = textFied5.text;
            email4.from = textFied5.text;
            email4.subject = @"Test email subject";
            email4.text = @"Test email message.";
            
            [sendgrid sendWithWeb:email4
                     successBlock:^(id responseObject)
             {
                 
                 NSLog(@"YES IT WORKED");
                 FCAlertView *alert = [[FCAlertView alloc] init];
                 alert.darkTheme = YES;
                 [alert makeAlertTypeSuccess];
                 alert.detachButtons = YES;
                 alert.blurBackground = YES;
                 [alert showAlertWithTitle:@"SUCCESS"
                              withSubtitle:@"Congratulations. Your SendGrid account is valid and connected successfully. You should receive a test message soon."
                           withCustomImage:nil
                       withDoneButtonTitle:nil
                                andButtons:nil];
                 [defaults setObject:@"On" forKey:@"switchSendgrid"];
                 
                 
                 
             }
                     failureBlock:^(NSError *error)
             
             
             {
                 
                 
                 FCAlertView *alert = [[FCAlertView alloc] init];
                 alert.darkTheme = YES;
                 [alert makeAlertTypeCaution];
                 alert.detachButtons = YES;
                 [alert setAlertSoundWithFileName:@"error.mp3"];
                 alert.blurBackground = YES;
                 [alert showAlertWithTitle:@"SOMETHING WENT WRONG"
                              withSubtitle:@"We tried to make sure your SendGrid account was correct. Please make sure you have internet connected for this test and that your email is the same used when you created your account."
                           withCustomImage:nil
                       withDoneButtonTitle:nil
                                andButtons:nil];
                 
             }];
            
            
            }
        }
        
        
    };
    
    
    
    
    [customView3 addSubview:lineTwoLabel4];
    [customView3 addSubview:textFied5];
    [customView3 addSubview:button3];
    
    [customView3 addSubview:switchsendgrid];
    
    
    CNPPopupButton *button = [[CNPPopupButton alloc] initWithFrame:CGRectMake(495, 255, 105, 50)];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button setTitle:@"SAVE" forState:UIControlStateNormal];
    button.backgroundColor = [UIColor colorWithRed:206.0/255.0 green:188.0/255.0 blue:178.0/255.0 alpha:1.0];
    button.layer.cornerRadius = 0;
    button.selectionHandler = ^(CNPPopupButton *button){
      
            NSString *emailsu = textFied.text;
            NSString *emailme  = message.text;
            NSString *emailaddress1  = textFied5.text;
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            
            [defaults setObject:emailaddress1 forKey:@"ipixemail"];
            [defaults setObject:emailsu forKey:@"emailsubject"];
            [defaults setObject:emailme forKey:@"emailmessage"];
            
            if (switchsendgrid.on == 1)
            {
                
                
                
                
                [self.popupController dismissPopupControllerAnimated:YES];
                
                
                NSString *switchss = @"YES";
                [defaults setValue:switchss forKey:@"switchSendgrid"];
                
                
            }
            
            if (switchsendgrid.on ==0)
            {
                
                [self.popupController dismissPopupControllerAnimated:YES];
                NSString *switchss = @"NO";
                [defaults setValue:switchss forKey:@"switchSendgrid"];
                NSLog(@"GRID OFF");
                
            }
            
            
            if (switchLayout.on == 0) {
                NSString *switchss = @"NO";
                [defaults setValue:switchss forKey:@"switchLayout"];
            } else if (switchLayout.on == 1) {
                NSString *switchss = @"YES";
                [defaults setValue:switchss forKey:@"switchLayout"];
                
                
            }
            if (switchIndividual.on == 0) {
                NSString *switchss = @"NO";
                [defaults setValue:switchss forKey:@"switchIndividual"];
            } else if (switchIndividual.on == 1) {
                NSString *switchss = @"YES";
                [defaults setValue:switchss forKey:@"switchIndividual"];
                
                
            }
            
            
            [defaults synchronize];
     
        
    };
    
    
    
    CNPPopupButton *button2 = [[CNPPopupButton alloc] initWithFrame:CGRectMake(380, 255, 105, 50)];
    [button2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button2.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button2 setTitle:@"CANCEL" forState:UIControlStateNormal];
    button2.backgroundColor = [UIColor darkGrayColor];
    button2.layer.cornerRadius = 0;
    button2.selectionHandler = ^(CNPPopupButton *button){
        [switchEmail setOn:NO animated:YES];
        [self.popupController dismissPopupControllerAnimated:YES];
    };
    
    
    
    UIView *customView4 = [[UIView alloc] initWithFrame:CGRectMake(360, 20, 1, 285)];
    customView4.backgroundColor = [UIColor colorWithRed:206.0/255.0 green:188.0/255.0 blue:178.0/255.0 alpha:1.0];
    [fullView addSubview:button];
    [fullView addSubview:button2];
    [fullView addSubview:customView];
    [fullView addSubview:customView7];
    [fullView addSubview:customView3];
    [fullView addSubview:customView4];
    
    
    self.popupController = [[CNPPopupController alloc] initWithContents:@[lineTwoLabel5, fullView]];
    self.popupController.theme = [CNPPopupTheme defaultTheme];
    self.popupController.theme.popupStyle = popupStyle;
    self.popupController.theme.presentationStyle = CNPPopupPresentationStyleSlideInFromTop;
    self.popupController.delegate = self;
    [self.popupController presentPopupControllerAnimated:YES];
    
    
    
    
    
}



- (void)showTwitterPopup2:(CNPPopupStyle)popupStyle {
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString* strValue =[defaults objectForKey:@"twitterhashtag"];
    //    NSString* strValue1 =[defaults objectForKey:@"consumerkey"];
    //   NSString* strValue2 =[defaults objectForKey:@"consumersecret"];
    
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    NSAttributedString *lineTwo = [[NSAttributedString alloc] initWithString:@"CUSTOM MESSAGE AND/OR HASHTAG" attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:24], NSForegroundColorAttributeName : [UIColor darkGrayColor], NSParagraphStyleAttributeName : paragraphStyle}];
    
    UILabel *lineTwoLabel = [[UILabel alloc] init];
    lineTwoLabel.numberOfLines = 0;
    lineTwoLabel.attributedText = lineTwo;
    UIView *spacerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    UITextField *textFied = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 340, 45)];
    textFied.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    textFied.layer.borderWidth= 1.0f;
    textFied.text = strValue;
    textFied.delegate = self;
    [textFied setLeftViewMode:UITextFieldViewModeAlways];
    [textFied setLeftView:spacerView];
    
    
    
    
    UIView *customView8 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 360, 80)];

    CNPPopupButton *button2 = [[CNPPopupButton alloc] initWithFrame:CGRectMake(10, 0, 140, 60)];
    [button2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button2.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button2 setTitle:@"CANCEL" forState:UIControlStateNormal];
    button2.backgroundColor = [UIColor darkGrayColor];
    button2.layer.cornerRadius = 0;
    button2.selectionHandler = ^(CNPPopupButton *button2){
        [self.popupController dismissPopupControllerAnimated:YES];
        [switchTwitter setOn:NO animated:YES];
    };
    
    CNPPopupButton *button = [[CNPPopupButton alloc] initWithFrame:CGRectMake(160, 0, 190, 60)];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button setTitle:@"SAVE" forState:UIControlStateNormal];
    button.backgroundColor = [UIColor colorWithRed:206.0/255.0 green:188.0/255.0 blue:178.0/255.0 alpha:1.0];
    button.layer.cornerRadius = 0;
    button.selectionHandler = ^(CNPPopupButton *button){
        
        
        
            
            NSString *hashtag  = textFied.text;
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            
            if (switchTwitterLayout.on == 0) {
                NSString *switchss = @"NO";
                [defaults setValue:switchss forKey:@"switchTwitterLayout"];
                NSLog(@"Switch Twitter Layout is %@", switchss);
            } else if (switchTwitterLayout.on == 1) {
                NSString *switchss = @"YES";
                [defaults setValue:switchss forKey:@"switchTwitterLayout"];
                NSLog(@"Switch Twitter Layout is %@", switchss);
                
                
            }
            if (switchTwitterIndividual.on == 0) {
                NSString *switchss = @"NO";
                [defaults setValue:switchss forKey:@"switchTwitterIndividual"];
                NSLog(@"Switch Twitter Individual is %@", switchss);
            } else if (switchTwitterIndividual.on == 1) {
                NSString *switchss = @"YES";
                [defaults setValue:switchss forKey:@"switchTwitterIndividual"];
                NSLog(@"Switch Twitter Individual is %@", switchss);
                
                
            }
            [defaults setObject:hashtag forKey:@"twitterhashtag"];
            [defaults synchronize];
            [self.popupController dismissPopupControllerAnimated:YES];
     
    };
    
    

    [customView8 addSubview:button2];
    [customView8 addSubview:button];
    
    
    
    
    
    self.popupController = [[CNPPopupController alloc] initWithContents:@[lineTwoLabel, textFied, customView8]];
    self.popupController.theme = [CNPPopupTheme defaultTheme];
    self.popupController.theme.popupStyle = popupStyle;
    self.popupController.delegate = self;
    [self.popupController presentPopupControllerAnimated:YES];
    
    
    
    
    
    
}


- (void)showFacebookPopup2:(CNPPopupStyle)popupStyle {
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString* strValue =[defaults objectForKey:@"fbwebsite"];
    NSString* strValue1 =[defaults objectForKey:@"fbtitle"];
    NSString* strValue2 =[defaults objectForKey:@"fbdescription"];
    NSString* strValue3 =[defaults objectForKey:@"fblogo"];
    //    NSString* strValue1 =[defaults objectForKey:@"consumerkey"];
    //   NSString* strValue2 =[defaults objectForKey:@"consumersecret"];
    
    UIView *spacerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    UITextField *textFied = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 340, 35)];
    textFied.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    textFied.layer.borderWidth= 1.0f;
    textFied.text = strValue;
    textFied.placeholder = @"YOUR WEBSITE";
    textFied.delegate = self;
    [textFied setLeftViewMode:UITextFieldViewModeAlways];
    [textFied setLeftView:spacerView];
    
    UIView *spacerView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    UITextField *textFied1 = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 340, 35)];
    textFied1.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    textFied1.layer.borderWidth= 1.0f;
    textFied1.text = strValue1;
    textFied1.placeholder = @"SUBJECT";
    textFied1.delegate = self;
    [textFied1 setLeftViewMode:UITextFieldViewModeAlways];
    [textFied1 setLeftView:spacerView1];
    
    UIView *spacerView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    UITextField *textFied2 = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 340, 35)];
    textFied2.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    textFied2.layer.borderWidth= 1.0f;
    textFied2.text = strValue2;
    textFied2.placeholder = @"DESCRIPTION";
    textFied2.delegate = self;
    [textFied2 setLeftViewMode:UITextFieldViewModeAlways];
    [textFied2 setLeftView:spacerView2];
    
    UIView *spacerView3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    UITextField *textFied3 = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 340, 45)];
    textFied3.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    textFied3.layer.borderWidth= 1.0f;
    textFied3.placeholder = @"YOUR LOGO (FROM WEBSITE ADDRESS)";
    textFied3.text = strValue3;
    textFied3.delegate = self;
    [textFied3 setLeftViewMode:UITextFieldViewModeAlways];
    [textFied3 setLeftView:spacerView3];
    
    
    UIView *customView8 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 360, 80)];

    
    CNPPopupButton *button2 = [[CNPPopupButton alloc] initWithFrame:CGRectMake(10, 0, 140, 60)];
    [button2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button2.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button2 setTitle:@"CANCEL" forState:UIControlStateNormal];
    button2.backgroundColor = [UIColor darkGrayColor];
    button2.layer.cornerRadius = 0;
    button2.selectionHandler = ^(CNPPopupButton *button2){
        [self.popupController dismissPopupControllerAnimated:YES];
        [switchFacebook setOn:NO animated:YES];
    };
    
    CNPPopupButton *button = [[CNPPopupButton alloc] initWithFrame:CGRectMake(160, 0, 190, 60)];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button setTitle:@"SAVE" forState:UIControlStateNormal];
    button.backgroundColor = [UIColor colorWithRed:206.0/255.0 green:188.0/255.0 blue:178.0/255.0 alpha:1.0];
    button.layer.cornerRadius = 0;
    button.selectionHandler = ^(CNPPopupButton *button){
        
        
   
            NSString *fbwebsite  = textFied.text;
            NSString *fbtitle = textFied1.text;
        NSString *fbdescription  = textFied2.text;
        NSString *fblogo  = textFied3.text;
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            
            if (switchFacebookLayout.on == 0) {
                NSString *switchss = @"NO";
                [defaults setValue:switchss forKey:@"switchFacebookLayout"];
                NSLog(@"Switch Facebook Layout is %@", switchss);
            } else if (switchFacebookLayout.on == 1) {
                NSString *switchss = @"YES";
                [defaults setValue:switchss forKey:@"switchFacebookLayout"];
                NSLog(@"Switch Facebook Layout is %@", switchss);
                
                
            }
            if (switchFacebookIndividual.on == 0) {
                NSString *switchss = @"NO";
                [defaults setValue:switchss forKey:@"switchFacebookIndividual"];
                NSLog(@"Switch Facebook Individual is %@", switchss);
            } else if (switchFacebookIndividual.on == 1) {
                NSString *switchss = @"YES";
                [defaults setValue:switchss forKey:@"switchFacebookIndividual"];
                NSLog(@"Switch Facebook Individual is %@", switchss);
                
                
            }
            [defaults setObject:fbwebsite forKey:@"fbwebsite"];
        [defaults setObject:fbtitle forKey:@"fbtitle"];
         [defaults setObject:fbdescription forKey:@"fbdescription"];
         [defaults setObject:fblogo forKey:@"fblogo"];
            [defaults synchronize];
            [self.popupController dismissPopupControllerAnimated:YES];
            
      
    };
    
    

    [customView8 addSubview:button2];
    [customView8 addSubview:button];
    
    self.popupController = [[CNPPopupController alloc] initWithContents:@[textFied, textFied1, textFied2,  textFied3, customView8]];
    self.popupController.theme = [CNPPopupTheme defaultTheme];
    self.popupController.theme.popupStyle = popupStyle;
    self.popupController.delegate = self;
    [self.popupController presentPopupControllerAnimated:YES];
    
    
    
    
    
    
}



- (void)showTextPopup2:(CNPPopupStyle)popupStyle {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString* strValue =[defaults objectForKey:@"twiliomessage"];
    //    NSString* strValue1 =[defaults objectForKey:@"consumerkey"];
    //   NSString* strValue2 =[defaults objectForKey:@"consumersecret"];
    
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    NSAttributedString *lineTwo = [[NSAttributedString alloc] initWithString:@"TEXT MESSAGE BODY" attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:24], NSForegroundColorAttributeName : [UIColor darkGrayColor], NSParagraphStyleAttributeName : paragraphStyle}];
    
    UILabel *lineTwoLabel = [[UILabel alloc] init];
    lineTwoLabel.numberOfLines = 0;
    lineTwoLabel.attributedText = lineTwo;
    UIView *spacerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    UITextField *textFied = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 340, 45)];
    textFied.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    textFied.layer.borderWidth= 1.0f;
    textFied.text = strValue;
    textFied.delegate = self;
    [textFied setLeftViewMode:UITextFieldViewModeAlways];
    [textFied setLeftView:spacerView];
    
    
    
    
    UIView *customView8 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 360, 80)];

    
    
    CNPPopupButton *button2 = [[CNPPopupButton alloc] initWithFrame:CGRectMake(10, 0, 140, 60)];
    [button2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button2.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button2 setTitle:@"CANCEL" forState:UIControlStateNormal];
    button2.backgroundColor = [UIColor darkGrayColor];
    button2.layer.cornerRadius = 0;
    button2.selectionHandler = ^(CNPPopupButton *button2){
        [self.popupController dismissPopupControllerAnimated:YES];
        [switchText setOn:NO animated:YES];
    };
    
    CNPPopupButton *button = [[CNPPopupButton alloc] initWithFrame:CGRectMake(160, 0, 190, 60)];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button setTitle:@"SAVE" forState:UIControlStateNormal];
    button.backgroundColor = [UIColor colorWithRed:206.0/255.0 green:188.0/255.0 blue:178.0/255.0 alpha:1.0];
    button.layer.cornerRadius = 0;
    button.selectionHandler = ^(CNPPopupButton *button){
        
    
            
            NSString *hashtag  = textFied.text;
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            
            if (switchTextLayout.on == 0) {
                NSString *switchss = @"NO";
                [defaults setValue:switchss forKey:@"switchTextLayout"];
                NSLog(@"Switch Text Layout is %@", switchss);
            } else if (switchTextLayout.on == 1) {
                NSString *switchss = @"YES";
                [defaults setValue:switchss forKey:@"switchTextLayout"];
                NSLog(@"Switch Text Layout is %@", switchss);
                
                
            }
            if (switchTextIndividual.on == 0) {
                NSString *switchss = @"NO";
                [defaults setValue:switchss forKey:@"switchTextIndividual"];
                NSLog(@"Switch Text Individual is %@", switchss);
            } else if (switchTextIndividual.on == 1) {
                NSString *switchss = @"YES";
                [defaults setValue:switchss forKey:@"switchTextIndividual"];
                NSLog(@"Switch Text Individual is %@", switchss);
                
                
            }
            [defaults setObject:hashtag forKey:@"twiliomessage"];
            [defaults synchronize];
            [self.popupController dismissPopupControllerAnimated:YES];
            
       
    };
    
    
    

    [customView8 addSubview:button2];
    [customView8 addSubview:button];
    
    self.popupController = [[CNPPopupController alloc] initWithContents:@[lineTwoLabel, textFied, customView8]];
    self.popupController.theme = [CNPPopupTheme defaultTheme];
    self.popupController.theme.popupStyle = popupStyle;
    self.popupController.delegate = self;
    [self.popupController presentPopupControllerAnimated:YES];
    
    

}

- (void)showSharingPopup1:(CNPPopupStyle)popupStyle {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString* strValue =[defaults objectForKey:@"twitterhashtag"];
    //    NSString* strValue1 =[defaults objectForKey:@"consumerkey"];
    //   NSString* strValue2 =[defaults objectForKey:@"consumersecret"];
    
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    NSAttributedString *lineTwo = [[NSAttributedString alloc] initWithString:@"CHOOSE WHAT PHOTOS WILL BE TRANSFERRED" attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:24], NSForegroundColorAttributeName : [UIColor darkGrayColor], NSParagraphStyleAttributeName : paragraphStyle}];
    
    UILabel *lineTwoLabel = [[UILabel alloc] init];
    lineTwoLabel.numberOfLines = 0;
    lineTwoLabel.attributedText = lineTwo;
    UIView *spacerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    UITextField *textFied = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 340, 45)];
    textFied.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    textFied.layer.borderWidth= 1.0f;
    textFied.text = strValue;
    textFied.delegate = self;
    [textFied setLeftViewMode:UITextFieldViewModeAlways];
    [textFied setLeftView:spacerView];
    
    
    
    
    UIView *customView8 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 360, 140)];
    UILabel *lineThreeLabel1 = [[UILabel alloc] init];
    lineThreeLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 110, 15)];
    lineThreeLabel1.numberOfLines = 0;
    lineThreeLabel1.text = @"INDIVIDUAL";
    lineThreeLabel1.textColor = [UIColor darkGrayColor];
    switchSharingIndividual = [[UISwitch alloc] initWithFrame:CGRectMake(10, 30,40,40)];
    switchSharingIndividual.onTintColor = [UIColor darkGrayColor];
    
    UILabel *lineThreeLabel3 = [[UILabel alloc] init];
    lineThreeLabel3 = [[UILabel alloc] initWithFrame:CGRectMake(130, 10, 110, 15)];
    lineThreeLabel3.numberOfLines = 0;
    lineThreeLabel3.text = @"LAYOUT";
    lineThreeLabel3.textColor = [UIColor darkGrayColor];
    switchSharingLayout = [[UISwitch alloc] initWithFrame:CGRectMake(130, 30 ,40,40)];
    switchSharingLayout.onTintColor = [UIColor darkGrayColor];
    
    
    CNPPopupButton *button2 = [[CNPPopupButton alloc] initWithFrame:CGRectMake(10, 80, 140, 60)];
    [button2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button2.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button2 setTitle:@"CANCEL" forState:UIControlStateNormal];
    button2.backgroundColor = [UIColor darkGrayColor];
    button2.layer.cornerRadius = 0;
    button2.selectionHandler = ^(CNPPopupButton *button2){
        [self.popupController dismissPopupControllerAnimated:YES];
        [switchTwitter setOn:NO animated:YES];
        [switchText setOn:NO animated:YES];
        [switchFacebook setOn:NO animated:YES];
        [switchEmail setOn:NO animated:YES];
        [switchSharing setOn:NO animated:YES];
        switchEmail.enabled = YES;
        switchFacebook.enabled = YES;
        switchTwitter.enabled = YES;
        switchText.enabled = YES;
    };
    
    CNPPopupButton *button = [[CNPPopupButton alloc] initWithFrame:CGRectMake(160, 80, 190, 60)];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button setTitle:@"SAVE" forState:UIControlStateNormal];
    button.backgroundColor = [UIColor colorWithRed:206.0/255.0 green:188.0/255.0 blue:178.0/255.0 alpha:1.0];
    button.layer.cornerRadius = 0;
    button.selectionHandler = ^(CNPPopupButton *button){
        
        
        if ((switchSharingIndividual.on == 0) & (switchSharingLayout.on == 0))
        {
            FCAlertView *alert = [[FCAlertView alloc] init];
            alert.darkTheme = YES;
            [alert makeAlertTypeCaution];
            alert.detachButtons = YES;
            [alert setAlertSoundWithFileName:@"error.mp3"];
            alert.blurBackground = YES;
            [alert showAlertWithTitle:@"Missing Information"
                         withSubtitle:@"You need to share at least the individual photos or the layout photo. Please choose one or both."
                      withCustomImage:nil
                  withDoneButtonTitle:nil
                           andButtons:nil];
            
            
        }
        else
        {
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            
            if (switchSharingLayout.on == 0) {
                NSString *switchss = @"NO";
                [defaults setValue:switchss forKey:@"sswitchSharingLayout"];
                NSLog(@"Switch Sharing Layout is %@", switchss);
            } else if (switchSharingLayout.on == 1) {
                NSString *switchss = @"YES";
                [defaults setValue:switchss forKey:@"switchSharingLayout"];
                NSLog(@"Switch Sharing Layout is %@", switchss);
                
                
            }
            if (switchSharingIndividual.on == 0) {
                NSString *switchss = @"NO";
                [defaults setValue:switchss forKey:@"switchSharingIndividual"];
                NSLog(@"Switch Twitter Individual is %@", switchss);
            } else if (switchSharingIndividual.on == 1) {
                NSString *switchss = @"YES";
                [defaults setValue:switchss forKey:@"switchSharingIndividual"];
                NSLog(@"Switch Sharing Individual is %@", switchss);
                
                
            }
         
            [self.popupController dismissPopupControllerAnimated:YES];
            
        }
    };
    
    
    
    
    [customView8 addSubview:lineThreeLabel1];
    [customView8 addSubview:switchSharingIndividual];
    [customView8 addSubview:lineThreeLabel3];
    [customView8 addSubview:switchSharingLayout];
    [customView8 addSubview:button2];
    [customView8 addSubview:button];
    
    
    
    
    
    self.popupController = [[CNPPopupController alloc] initWithContents:@[lineTwoLabel, customView8]];
    self.popupController.theme = [CNPPopupTheme defaultTheme];
    self.popupController.theme.popupStyle = popupStyle;
    self.popupController.delegate = self;
    [self.popupController presentPopupControllerAnimated:YES];
    
}



-(void)tryagain:(CNPPopupStyle)popupStyle
{
     UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];
    imageView.image = [UIImage imageNamed:@"caution.png"];
    imageView.image = [imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [imageView setTintColor:[UIColor redColor]];
    
    
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    
    UILabel *titleLabel = [[UILabel alloc] init];
    NSAttributedString *lineTwo = [[NSAttributedString alloc] initWithString:@"You need to make sure you have entered a username, password, and your email address. Would you like to try again?" attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:18], NSForegroundColorAttributeName :[UIColor blackColor], NSParagraphStyleAttributeName : paragraphStyle}];
    titleLabel.numberOfLines = 0;
    titleLabel.attributedText = lineTwo;
    
    UIView *customView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 340, 65)];
   
    CNPPopupButton *button = [[CNPPopupButton alloc] initWithFrame:CGRectMake(100, 10, 140, 45)];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button setTitle:@"OK" forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.backgroundColor = [UIColor redColor];
    button.layer.cornerRadius = 0;
    button.selectionHandler = ^(CNPPopupButton *button){
        [self.popupController dismissPopupControllerAnimated:YES];
    };
    
    [customView1 addSubview:button];
    self.popupController = [[CNPPopupController alloc] initWithContents:@[imageView, titleLabel, customView1]];
    self.popupController.theme = [CNPPopupTheme defaultTheme];
    self.popupController.theme.popupStyle = popupStyle;
    self.popupController.theme.presentationStyle = CNPPopupPresentationStyleSlideInFromTop;
    self.popupController.delegate = self;
    [self.popupController presentPopupControllerAnimated:NO];
    
    
    
}


//ANIMATED GIF DURATION AND NUMBER OF PHOTOS//
-(IBAction)stepperPressed1:(id)sender{
    animatednumber.text=[NSString stringWithFormat:@"%g",animatednumber1.value];
    NSLog(@"Animated GIF Number = %@", animatednumber.text);
    
}

-(IBAction)stepperPressed2:(id)sender{
    animatedduration.text=[NSString stringWithFormat:@"%g",animatedduration1.value];
    NSLog(@"Animated GIF Duration = %@", animatedduration.text);
    
}


//PHOTO COUNTDOWN FOR BOTH ANIMATED GIF AND PHOTO BOOTH//
-(IBAction)maxselection1:(id)sender{
    
    maxSelection1.text=[NSString stringWithFormat:@"%g",maxSelectionTimer1.value];
    NSLog(@"HEY %@", maxSelection1.text);
    
    
}

-(IBAction)photopreview:(id)sender{
    photopreviewlab.text=[NSString stringWithFormat:@"%g",photopreviewstepper.value];
    NSLog(@"Photo Preview Number = %@", photopreviewlab.text);
    
}



//VIDEO BUTTON FOR BOTH ANIMATED GIF AND PHOTO BOOTH//
-(IBAction)maxselection2:(id)sender{
    maxSelection2.text=[NSString stringWithFormat:@"%g",maxSelectionTimer2.value];
    NSLog(@"Video Countdown = %@", maxSelection2.text);
    
}

-(IBAction)maxsharing:(id)sender{
    maxSelectionSharingLabel.text=[NSString stringWithFormat:@"%g",maxSelectionSharingStepper.value];
    NSLog(@"Max Number = %@", maxSelectionSharingLabel.text);
    
}


//GREEN SCREEN FOR BOTH ANIMATED GIF AND PHOTO BOOTH//

- (IBAction)showPopupGreen:(id)sender;

{
    if ([switchGreen isOn])
    {
        NSLog(@"Green Screen switch is ON");
        (switchBlackWhite.enabled=NO);
        (switchSepia.enabled=NO);
        (switchVintage.enabled=NO);
        (switchBlackWhite.on=NO);
        (switchSepia.on=NO);
        (switchVintage.on=NO);
        [self selectgreenscreenphotos:CNPPopupStyleActionSheet];
    }
    else
    {
        NSLog(@"Green Screen switch is OFF");
        (switchBlackWhite.enabled=YES);
        (switchSepia.enabled=YES);
        (switchVintage.enabled=YES);
    }
    
    
}


- (void)selectgreenscreenphotos:(CNPPopupStyle)popupStyle {
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSAttributedString *title = [[NSAttributedString alloc] initWithString:@"GREEN SCREEN" attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:24], NSParagraphStyleAttributeName : paragraphStyle}];
    //  NSAttributedString *lineOne = [[NSAttributedString alloc] initWithString:@"You can add text and images" attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:18], NSParagraphStyleAttributeName : paragraphStyle}];
    NSAttributedString *lineTwo = [[NSAttributedString alloc] initWithString:@"You can select up to 5 photos." attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:18], NSParagraphStyleAttributeName : paragraphStyle}];
    
    CNPPopupButton *button = [[CNPPopupButton alloc] initWithFrame:CGRectMake(0, 0, 200, 60)];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button setTitle:@"Exit" forState:UIControlStateNormal];
    button.backgroundColor = [UIColor colorWithRed:0.46 green:0.8 blue:1.0 alpha:1.0];
    button.layer.cornerRadius = 0;
    button.selectionHandler = ^(CNPPopupButton *button){
        (switchBlackWhite.enabled=YES);
        (switchSepia.enabled=YES);
        (switchVintage.enabled=YES);
        [switchGreen setOn:NO animated:YES];
        [self.popupController dismissPopupControllerAnimated:YES];
        NSLog(@"Block for button: %@", button.titleLabel.text);
    };
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.numberOfLines = 0;
    titleLabel.attributedText = title;
    
    UILabel *lineTwoLabel = [[UILabel alloc] init];
    lineTwoLabel.numberOfLines = 0;
    lineTwoLabel.attributedText = lineTwo;
    
    
    CNPPopupButton *button1 = [[CNPPopupButton alloc] initWithFrame:CGRectMake(0, 0, 250, 55)];
    [button1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button1.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button1 setTitle:@"Choose Your Photo(s)" forState:UIControlStateNormal];
    button1.backgroundColor = [UIColor colorWithRed:62/255.0 green:90/255.0 blue:122/255.0 alpha:1.0];
    button1.layer.cornerRadius = 0;
    button1.selectionHandler = ^(CNPPopupButton *button1){
        if (!imageMediaPickerGreenScreen) {
            imageMediaPickerGreenScreen = [QBImagePickerController new];
            imageMediaPickerGreenScreen.mediaType = QBImagePickerMediaTypeImage;
            imageMediaPickerGreenScreen.allowsMultipleSelection = YES;
            imageMediaPickerGreenScreen.showsNumberOfSelectedAssets = YES;
            imageMediaPickerGreenScreen.maximumNumberOfSelection = 5;
            imageMediaPickerGreenScreen.delegate = self;
        }
        [self presentViewController:imageMediaPickerGreenScreen animated:YES completion:nil];
        
        [self.popupController dismissPopupControllerAnimated:YES];
        
        
        
        
        
        
        NSLog(@"Block for button1: %@", button.titleLabel.text);
    };
    
    
    
    
    
    self.popupController = [[CNPPopupController alloc] initWithContents:@[titleLabel, lineTwoLabel,  button1, button]];
    self.popupController.theme = [CNPPopupTheme defaultTheme];
    self.popupController.theme.popupStyle = popupStyle;
    self.popupController.delegate = self;
    [self.popupController presentPopupControllerAnimated:YES];
    
}








-(IBAction)vidtest:(id)sender

{
    if([switchVideo isOn]){
        NSLog(@"Video switch is ON");
    } else{
        NSLog(@"Video switch is OFF");
    }
    
}


//SCREEN BACKGROUNDS FOR BOTH ANIMATED GIF AND PHOTO BOOTH //



-(IBAction)welcomeanimated:(id)sender

{
    
    if([switchMessageAnim isOn]){
        NSLog(@"Welcome screen animated switch is ON");
    } else{
        NSLog(@"Welcome screen animated switch is OFF");
    }
    
}

-(IBAction)layoutbackgroundcolor:(id)sender

{
    colorPicker = [[RzColorPickerView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(self.view.frame), CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame))];
    [self.view addSubview:colorPicker];
    //  [colorPicker setCurrentColor:LayoutBackgroundColor];
    [UIView animateWithDuration:1 animations:^{
        [colorPicker setFrame:self.view.bounds];
    }];
    [colorPicker doneWithSelectedColor:^(UIColor * _Nonnull newColor)
     {
         switchPhotoBoothBackground.enabled = YES;
         LayoutBackgroundColor = newColor;
         NSData *theData = [NSKeyedArchiver archivedDataWithRootObject:LayoutBackgroundColor];
         [[NSUserDefaults standardUserDefaults] setObject:theData forKey:@"layoutbackgroundcolor"];
         NSData *theData1 = [[NSUserDefaults standardUserDefaults] objectForKey:@"layoutbackgroundcolor"];
         UIColor *theColor = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData:theData1];
         for (UIView *view in lineseparator1) {
             view.backgroundColor = theColor;
         }
         LayoutBackgroundColor = theColor;
         
         NSLog(@"Layout Background Color : %@", LayoutBackgroundColor);
         
     }];
    
    NSLog(@"I AM PRESSING LBC");
//    
//    [selectbackgroundcoloranim addTarget:self action:@selector(buttonPressed:)
//                        forControlEvents:UIControlEventTouchUpInside];
    
    
    
}


-(IBAction)layoutfontbordercolor:(id)sender

{
    NSLog(@"I AM PRESSING LFBC");
    colorPicker = [[RzColorPickerView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(self.view.frame), CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame))];
    [self.view addSubview:colorPicker];
    // [colorPicker setCurrentColor:[UIColor whiteColor]];
    [UIView animateWithDuration:1 animations:^{
        [colorPicker setFrame:self.view.bounds];
    }];
    [colorPicker doneWithSelectedColor:^(UIColor * _Nonnull newColor)
     {
         LayoutFontBorderColor = newColor;
         NSData *theData = [NSKeyedArchiver archivedDataWithRootObject:LayoutFontBorderColor];
         [[NSUserDefaults standardUserDefaults] setObject:theData forKey:@"layoutborderfontcolor"];
         NSData *theData1 = [[NSUserDefaults standardUserDefaults] objectForKey:@"layoutborderfontcolor"];
         UIColor *theColor = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData:theData1];
         
         for (UIView *view in labelcolors1) {
             view.backgroundColor = theColor;
         }
         
         
         LayoutFontBorderColor = theColor;
         
         
         NSLog(@"Layout Font/Border Color : %@", LayoutFontBorderColor);
         
         
     }];

    

    
    
    
}



- (IBAction) onClickWelcome:(id)sender {
    NSLog(@"Welcome Background");
    
    if (!imageMediaPickerWelcome) {
        imageMediaPickerWelcome = [QBImagePickerController new];
        imageMediaPickerWelcome.mediaType = QBImagePickerMediaTypeAny;
        imageMediaPickerWelcome.allowsMultipleSelection = YES;
        imageMediaPickerWelcome.showsNumberOfSelectedAssets = YES;
        imageMediaPickerWelcome.maximumNumberOfSelection = 1;
        imageMediaPickerWelcome.delegate = self;
    }
    [self presentViewController:imageMediaPickerWelcome animated:YES completion:nil];
    
    
    
    
}


- (IBAction) onClickEmail:(id)sender {
    NSLog(@"Email Background");
    
    if (!imageMediaPickerEmail) {
        imageMediaPickerEmail = [QBImagePickerController new];
        imageMediaPickerEmail.mediaType = QBImagePickerMediaTypeImage;
        imageMediaPickerEmail.allowsMultipleSelection = YES;
        imageMediaPickerEmail.showsNumberOfSelectedAssets = YES;
        imageMediaPickerEmail.maximumNumberOfSelection = 1;
        imageMediaPickerEmail.delegate = self;
    }
    [self presentViewController:imageMediaPickerEmail animated:YES completion:nil];;
    
    
    
}

- (IBAction) onClickEffects:(id)sender {
    NSLog(@"Effects Background");
    
    if (!imageMediaPickerEffects) {
        imageMediaPickerEffects = [QBImagePickerController new];
        imageMediaPickerEffects.mediaType = QBImagePickerMediaTypeImage;
        imageMediaPickerEffects.allowsMultipleSelection = YES;
        imageMediaPickerEffects.showsNumberOfSelectedAssets = YES;
        imageMediaPickerEffects.maximumNumberOfSelection = 1;
        imageMediaPickerEffects.delegate = self;
    }
    [self presentViewController:imageMediaPickerEffects animated:YES completion:nil];;
    

    
    
}

- (IBAction) onClickRemaining:(id)sender {
    NSLog(@"Remaining Background");
    if (!imageMediaPickerRemaining) {
        imageMediaPickerRemaining = [QBImagePickerController new];
        imageMediaPickerRemaining.mediaType = QBImagePickerMediaTypeImage;
        imageMediaPickerRemaining.allowsMultipleSelection = YES;
       imageMediaPickerRemaining.showsNumberOfSelectedAssets = YES;
        imageMediaPickerRemaining.maximumNumberOfSelection = 1;
        imageMediaPickerRemaining.delegate = self;
    }
    [self presentViewController:imageMediaPickerRemaining animated:YES completion:nil];;
    
    
    
}

- (IBAction) onClickFinal:(id)sender {
    NSLog(@"Final Background");
    
    if (!imageMediaPickerFinal) {
        imageMediaPickerFinal = [QBImagePickerController new];
        imageMediaPickerFinal.mediaType = QBImagePickerMediaTypeAny;
        imageMediaPickerFinal.allowsMultipleSelection = YES;
        imageMediaPickerFinal.showsNumberOfSelectedAssets = YES;
        imageMediaPickerFinal.maximumNumberOfSelection = 1;
        imageMediaPickerFinal.delegate = self;
    }
    [self presentViewController:imageMediaPickerFinal animated:YES completion:nil];;
    
    
    
    
}

- (IBAction) onClickReady:(id)sender {
    NSLog(@"Ready Background");
    
    if (!imageMediaPickerReady) {
        imageMediaPickerReady = [QBImagePickerController new];
        imageMediaPickerReady.mediaType = QBImagePickerMediaTypeImage;
        imageMediaPickerReady.allowsMultipleSelection = YES;
        imageMediaPickerReady.showsNumberOfSelectedAssets = YES;
        imageMediaPickerReady.maximumNumberOfSelection = 1;
        imageMediaPickerReady.delegate = self;
    }
    [self presentViewController:imageMediaPickerReady animated:YES completion:nil];;
    
    
    
    
}

- (IBAction) onClickVideo:(id)sender {
    NSLog(@"Video Background");
    
    if (!imageMediaPickerVideo) {
        imageMediaPickerVideo = [QBImagePickerController new];
        imageMediaPickerVideo.mediaType = QBImagePickerMediaTypeImage;
        imageMediaPickerVideo.allowsMultipleSelection = YES;
        imageMediaPickerVideo.showsNumberOfSelectedAssets = YES;
        imageMediaPickerVideo.maximumNumberOfSelection = 1;
        imageMediaPickerVideo.delegate = self;
    }
    [self presentViewController:imageMediaPickerVideo animated:YES completion:nil];;
    
    
    
}

- (IBAction) onClickClosed:(id)sender {
    NSLog(@"Closed Background");
    
    if (!imageMediaPickerClosed) {
        imageMediaPickerClosed = [QBImagePickerController new];
        imageMediaPickerClosed.mediaType = QBImagePickerMediaTypeImage;
        imageMediaPickerClosed.allowsMultipleSelection = YES;
        imageMediaPickerClosed.showsNumberOfSelectedAssets = YES;
        imageMediaPickerClosed.maximumNumberOfSelection = 1;
        imageMediaPickerClosed.delegate = self;
    }
    [self presentViewController:imageMediaPickerClosed animated:YES completion:nil];;
    
    
    
    
    
}




- (void)qb_imagePickerController:(QBImagePickerController *)imagePickerController didFinishPickingAssets:(NSArray *)assets
{
    
    if(imagePickerController == imageMediaPickerWelcome)
    {
        
        for (PHAsset *asset in assets)
        {
            if (asset.mediaType == PHAssetMediaTypeImage)
            {
                [[PHImageManager defaultManager] requestImageDataForAsset:asset options:nil resultHandler:^(NSData *imageData, NSString *dataUTI, UIImageOrientation orientation, NSDictionary *info) {
                    UIImage *image = [UIImage imageWithData:imageData];
                    welcomeimage  = image;
                    NSLog(@"WHAT THE HELL %@", welcomeimage);
                }];
            }
            else if (asset.mediaType == PHAssetMediaTypeVideo)
            {
                [[PHImageManager defaultManager] requestAVAssetForVideo:asset options:nil resultHandler:^(AVAsset *asset, AVAudioMix *audioMix, NSDictionary *info)
                 {
                     if ([asset isKindOfClass:[AVURLAsset class]])
                     {
                         NSURL *url = [(AVURLAsset*)asset URL];
                         welcomeimagevideo = [url absoluteString];
                         
                         NSLog(@"WHAT THE HELL VIDEO %@", welcomeimagevideo);
                     }
                 }];
            }
        }
        
        
        
    }
    

    if(imagePickerController == imageMediaPickerEmail)
    {

        PHImageManager *imageManager = [PHImageManager new];
        
        for (PHAsset *asset in assets) {
            [imageManager requestImageDataForAsset:asset
                                           options:0
                                     resultHandler:^(NSData *imageData, NSString *dataUTI, UIImageOrientation orientation, NSDictionary *info) {
                                         UIImage *image = [UIImage imageWithData:imageData];
                                         emailimage  = image;
                                     }];
            
            NSLog(@"EMAIL");
            
            
        }
        

    }
    
    if(imagePickerController == imageMediaPickerEffects)
    {
        
        PHImageManager *imageManager = [PHImageManager new];
        
        for (PHAsset *asset in assets) {
            [imageManager requestImageDataForAsset:asset
                                           options:0
                                     resultHandler:^(NSData *imageData, NSString *dataUTI, UIImageOrientation orientation, NSDictionary *info) {
                                         UIImage *image = [UIImage imageWithData:imageData];
                                         effectsimage  = image;
                                     }];
            
            NSLog(@"EFFECTS");
            
            
        }

        
        
    }
    
    
    
    if(imagePickerController == imageMediaPickerRemaining)
    {
        
        
        PHImageManager *imageManager = [PHImageManager new];
        
        for (PHAsset *asset in assets) {
            [imageManager requestImageDataForAsset:asset
                                           options:0
                                     resultHandler:^(NSData *imageData, NSString *dataUTI, UIImageOrientation orientation, NSDictionary *info) {
                                         UIImage *image = [UIImage imageWithData:imageData];
                                         remainingimage  = image;
                                     }];
            
            NSLog(@"REMAINING");
            
            
        }
        
        
    }

    
    if(imagePickerController == imageMediaPickerFinal)
    {
        
        
        for (PHAsset *asset in assets)
        {
            if (asset.mediaType == PHAssetMediaTypeImage)
            {
                [[PHImageManager defaultManager] requestImageDataForAsset:asset options:nil resultHandler:^(NSData *imageData, NSString *dataUTI, UIImageOrientation orientation, NSDictionary *info) {
                    UIImage *image = [UIImage imageWithData:imageData];
                    finalimage  = image;
                    NSLog(@"WHAT THE HELL %@", finalimage);
                }];
            }
            else if (asset.mediaType == PHAssetMediaTypeVideo)
            {
                [[PHImageManager defaultManager] requestAVAssetForVideo:asset options:nil resultHandler:^(AVAsset *asset, AVAudioMix *audioMix, NSDictionary *info)
                 {
                     if ([asset isKindOfClass:[AVURLAsset class]])
                     {
                         NSURL *url = [(AVURLAsset*)asset URL];
                         finalimagevideo = [url absoluteString];
                         
                         NSLog(@"WHAT THE HELL VIDEO %@", finalimagevideo);
                     }
                 }];
            }
            
        }
        
    }

    if(imagePickerController == imageMediaPickerReady)
    {
        
        
        PHImageManager *imageManager = [PHImageManager new];
        
        for (PHAsset *asset in assets) {
            [imageManager requestImageDataForAsset:asset
                                           options:0
                                     resultHandler:^(NSData *imageData, NSString *dataUTI, UIImageOrientation orientation, NSDictionary *info) {
                                         UIImage *image = [UIImage imageWithData:imageData];
                                         readyimage  = image;
                                     }];
            
            NSLog(@"READY");
            
            
        }
        
        
    }

    if(imagePickerController == imageMediaPickerVideo)
    {
        
        
        PHImageManager *imageManager = [PHImageManager new];
        
        for (PHAsset *asset in assets) {
            [imageManager requestImageDataForAsset:asset
                                           options:0
                                     resultHandler:^(NSData *imageData, NSString *dataUTI, UIImageOrientation orientation, NSDictionary *info) {
                                         UIImage *image = [UIImage imageWithData:imageData];
                                         videoimage  = image;
                                     }];
            
            NSLog(@"VIDEO");
            
            
        }
        
        
    }

    if(imagePickerController == imageMediaPickerClosed)
    {
        
        
        
        PHImageManager *imageManager = [PHImageManager new];
        
        for (PHAsset *asset in assets) {
            [imageManager requestImageDataForAsset:asset
                                           options:0
                                     resultHandler:^(NSData *imageData, NSString *dataUTI, UIImageOrientation orientation, NSDictionary *info) {
                                         UIImage *image = [UIImage imageWithData:imageData];
                                         closedimage  = image;
                                     }];
            
            NSLog(@"CLOSED");
            
          
            
        }
        
        
    }

    if(imagePickerController == imageMediaPickerCustomLayoutBackground)
    {
        
        
        
        PHImageManager *imageManager = [PHImageManager new];
        
        for (PHAsset *asset in assets) {
            [imageManager requestImageDataForAsset:asset
                                           options:0
                                     resultHandler:^(NSData *imageData, NSString *dataUTI, UIImageOrientation orientation, NSDictionary *info) {
                                         UIImage *image = [UIImage imageWithData:imageData];
                                         [TSPAppDelegate sharedAppDelegate].imgCustomBackground  = image;
                                         
                                         if (self.segmentedControl.selectedSegmentIndex == 0)
                                         {
                                             fourBySixBackgroundImageView.image = [TSPAppDelegate sharedAppDelegate].imgCustomBackground;
                                             // [record setValue:image forKey:@"customlayoutstore"];
                                             
                                         }
                                         if (self.segmentedControl.selectedSegmentIndex == 1)
                                         {
                                             fiveBySevenBackgroundImageView.image = [TSPAppDelegate sharedAppDelegate].imgCustomBackground;
                                             //      [record setValue:image forKey:@"customlayoutstore"];
                                         }
                                         
                                         if (self.segmentedControl.selectedSegmentIndex == 2)
                                         {
                                             fourBySixBackgroundImageViewh.image = [TSPAppDelegate sharedAppDelegate].imgCustomBackground;
                                             //       [record setValue:image forKey:@"customlayoutstore"];
                                         }
                                         if (self.segmentedControl.selectedSegmentIndex == 3)
                                         {
                                             fiveBySevenBackgroundImageViewh.image = [TSPAppDelegate sharedAppDelegate].imgCustomBackground;
                                             //       [record setValue:image forKey:@"customlayoutstore"];
                                         }
                                         NSLog (@"Layout Image = %@", fourBySixBackgroundImageView.image);
                                         
                                         
                                         
                                     }];
            
            NSLog(@"CUSTOMBACKGROUND");
            
        
        }
        
        
    }

    if(imagePickerController == imageMediaPickerGreenScreen)
    {
        
        
        
        PHImageManager *imageManager = [PHImageManager new];
        
        for (PHAsset *asset in assets) {
            [imageManager requestImageDataForAsset:asset
                                           options:0
                                     resultHandler:^(NSData *imageData, NSString *dataUTI, UIImageOrientation orientation, NSDictionary *info) {
                                         UIImage *image = [UIImage imageWithData:imageData];
                                         greenscreenimage  = image;
                                     }];
            
            NSLog(@"GREENSCREEN");
            
            
        }
        
    }
    
    if(imagePickerController == imageMediaPickerPrintLayoutBackground)
    {
        
        
        
        PHImageManager *imageManager = [PHImageManager new];
        
        for (PHAsset *asset in assets) {
            [imageManager requestImageDataForAsset:asset
                                           options:0
                                     resultHandler:^(NSData *imageData, NSString *dataUTI, UIImageOrientation orientation, NSDictionary *info) {
                                      
                                         UIImage *image = [UIImage imageWithData:imageData];
                                        [TSPAppDelegate sharedAppDelegate].photoboothlayoutimage = image;
                                     }];
            
            NSLog(@"PRINTBACKGROUND");
            
            
        }
        
        
    }
    
      [self dismissViewControllerAnimated:YES completion:NULL];

}


- (void)qb_imagePickerControllerDidCancel:(QBImagePickerController *)imagePickerController
{
    NSLog(@"Canceled.");
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}






//LAYOUT BACKGROUND FOR BOTH ANIMATED GIF AND PHOTO BOOTH//

-(IBAction)switchOverlay:(id)sender

{
    
    if ([switchOverlay1 isOn])
    {
        NSLog(@"Photo Booth Switch Overlay switch is ON");
        selectprintbackgroundcolor.enabled = NO;
        selectprintfontbordercolor.enabled = NO;
        selectprintbackgroundcolor.enabled = NO;
        selectprintfontbordercolor.enabled = NO;
        
        [TSPAppDelegate sharedAppDelegate].isSwitchOverlay = YES;
    }
    
    else
    {
        NSLog(@"Photo Booth Switch Overlay switch is OFF");
        selectprintbackgroundcolor.enabled = YES;
        selectprintfontbordercolor.enabled = YES;
        
        [TSPAppDelegate sharedAppDelegate].isSwitchOverlay = NO;
    }
    
    
}


-(IBAction)switchOverlayAnimatedGIF:(id)sender

{
    
    if ([switchOverlayAnimatedGIFBackground isOn] || [switchAnimatedLayout isOn]  )
    {
        NSLog(@"Animated GIF Switch Overlay switch is ON");
        
    }
    
    else
    {
        NSLog(@"Animated GIF Switch Overlay switch is OFF");
        
    }
    
}


-(IBAction)switchPhotoBoothBackground:(id)sender

{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *hideview = @"no";
    [defaults setObject:hideview forKey:@"hideview"];
    [defaults synchronize];
   
    {
        if ([switchPhotoBoothBackground isOn] )
        {
            NSLog(@"Switch Photo Booth Background switch is ON");
            selectbackgroundcolor.enabled = NO;
            selectfontbordercolor.enabled = NO;
            selectbackgroundcolor.enabled = NO;
            selectfontbordercolor.enabled = NO;
            switchOverlay.on = NO;
            switchOverlay.enabled = YES;
            [TSPAppDelegate sharedAppDelegate].isSwitchOverlay = NO;
            [self animandphotoboothlayoutbackground:CNPPopupStyleActionSheet];
        }
        
        else
        {
            NSLog(@"Switch Photo Booth Background switch is OFF");
            selectbackgroundcolor.enabled = YES;
            selectfontbordercolor.enabled = YES;
            selectbackgroundcolor.enabled = YES;
            selectfontbordercolor.enabled = YES;
            switchOverlay.on = NO;
            switchOverlay.enabled = NO;
            [TSPAppDelegate sharedAppDelegate].isSwitchOverlay = NO;
        }
    }
    
    
    
    
}

- (IBAction)showPopupAnimatedLayout:(id)sender;

{
    
    if (switchAnimatedLayout.isOn)
    {
        NSLog(@"Animated Layout is ON");
        [self animandphotoboothlayoutbackground:CNPPopupStyleActionSheet];
    }
    NSLog(@"Animated Layout is OFF");
    
}

- (void)AnimatedOverlaySwitch:(id)sender{
    
    if([sender isOn]){
        NSLog(@"Animated GIF Background Overlay Switch is ON");
    } else{
        NSLog(@"Animated GIF Background Overlay Switch is OFF");
    }
    
    
}


- (void)animandphotoboothlayoutbackground:(CNPPopupStyle)popupStyle {
    
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSAttributedString *title = [[NSAttributedString alloc] initWithString:@"SELECT LAYOUT" attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:24], NSParagraphStyleAttributeName : paragraphStyle}];
    NSAttributedString *lineOne = [[NSAttributedString alloc] initWithString:@"Choose the design you created which matches the layout you selected." attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:18], NSParagraphStyleAttributeName : paragraphStyle}];
    
    CNPPopupButton *button = [[CNPPopupButton alloc] initWithFrame:CGRectMake(0, 0, 200, 60)];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button setTitle:@"Exit" forState:UIControlStateNormal];
    button.backgroundColor = [UIColor colorWithRed:0.46 green:0.8 blue:1.0 alpha:1.0];
    button.layer.cornerRadius = 0;
    button.selectionHandler = ^(CNPPopupButton *button){
        [switchAnimatedLayout setOn:NO animated:YES];
        [switchPhotoBoothBackground setOn:NO animated:YES];
        [self.popupController dismissPopupControllerAnimated:YES];
        
        NSLog(@"Block for button: %@", button.titleLabel.text);
    };
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.numberOfLines = 0;
    titleLabel.attributedText = title;
    
    
    UIView *customView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 250, 55)];
    customView.backgroundColor = [UIColor lightGrayColor];
    
    
    
    UILabel *lineOneLabel = [[UILabel alloc] init];
    lineOneLabel.numberOfLines = 0;
    lineOneLabel.attributedText = lineOne;
    
    
    CNPPopupButton *button1 = [[CNPPopupButton alloc] initWithFrame:CGRectMake(0, 0, 250, 55)];
    [button1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button1.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button1 setTitle:@"Choose Your Photo" forState:UIControlStateNormal];
    button1.backgroundColor = [UIColor colorWithRed:62/255.0 green:90/255.0 blue:122/255.0 alpha:1.0];
    button1.layer.cornerRadius = 0;
    button1.selectionHandler = ^(CNPPopupButton *button1){
        if (!imageMediaPickerPrintLayoutBackground) {
            imageMediaPickerPrintLayoutBackground = [QBImagePickerController new];
            imageMediaPickerPrintLayoutBackground.mediaType = QBImagePickerMediaTypeImage;
            imageMediaPickerPrintLayoutBackground.allowsMultipleSelection = YES;
            imageMediaPickerPrintLayoutBackground.showsNumberOfSelectedAssets = YES;
            imageMediaPickerPrintLayoutBackground.maximumNumberOfSelection = 1;
            imageMediaPickerPrintLayoutBackground.delegate = self;
        }
        [self presentViewController:imageMediaPickerPrintLayoutBackground animated:YES completion:nil];
        

        
        if (switchOverlay.isOn)
        {
            NSLog(@"Switch Overlay Custom On");
            
        }
        else{
            NSLog(@"Switch Overlay Custom Off");
        }
        
        [self.popupController dismissPopupControllerAnimated:YES];
        
        NSLog(@"Block for button1: %@", button.titleLabel.text);
    };
    
    [customView addSubview:button1];
    
    self.popupController = [[CNPPopupController alloc] initWithContents:@[titleLabel, lineOneLabel, customView, button]];
    self.popupController.theme = [CNPPopupTheme defaultTheme];
    self.popupController.theme.popupStyle = popupStyle;
    self.popupController.delegate = self;
    [self.popupController presentPopupControllerAnimated:YES];
    
    
}




-(IBAction)burst:(id)sender{
    
    
    if (burstmode.isOn)
    {
        NSLog(@"Burst switch is ON");
        maxSelectionTimer1.enabled = NO;
        maxSelection1.text=[NSString stringWithFormat:@"0"];
    }
    else
    {
        NSLog(@"Burst switch is ON");
        maxSelectionTimer1.enabled = YES;
    }
    
    
}





//COLOR EFFECTS BUTTONS//

-(IBAction)bwtest:(id)sender

{
    if([switchBlackWhite isOn]){
        NSLog(@"BW switch is ON");
    } else{
        NSLog(@"BW switch is OFF");
    }
    
}



-(IBAction)septest:(id)sender

{
    if([switchSepia isOn]){
        NSLog(@"Sepia switch is ON");
    } else{
        NSLog(@"Sepia switch is OFF");
    }
    
}



-(IBAction)vintest:(id)sender

{
    if([switchVintage isOn]){
        NSLog(@"Vintage switch is ON");
    } else{
        NSLog(@"Vintage switch is OFF");
    }
    
}



-(IBAction)switchwel:(id)sender

{
    if([switchMessage isOn]){
        NSLog(@"Welcome screen switch is ON");
    } else{
        NSLog(@"Welcome screen switch is OFF");
    }
    
}

-(IBAction)switchboomerang:(id)sender

{
    if([boomerang isOn]){
        NSLog(@"boomerang switch is ON");
    } else{
        NSLog(@"boomerang switch is OFF");
    }
    
}

//PHOTO BOOTH//

-(IBAction)rear:(id)sender

{
    if (switchRear.isOn)
    {
        NSLog(@"Switch Rear is ON");
        switchGreen.enabled=NO;
        switchGif.enabled=NO;
        switchSepia.enabled=NO;
        switchVideo.enabled=NO;
        switchVintage.enabled=NO;
        switchBlackWhite.enabled=NO;
        switchGreen.on=NO;
        switchGif.on=NO;
        switchSepia.on=NO;
        switchVideo.on=NO;
        switchVintage.on=NO;
        switchBlackWhite.on=NO;
    }
    
    else
    {
        NSLog(@"Switch Rear is OFF");
        switchGreen.enabled=YES;
        switchGif.enabled=YES;
        switchSepia.enabled=YES;
        switchVideo.enabled=YES;
        switchVintage.enabled=YES;
        switchBlackWhite.enabled=YES;
    }
    
    
}











- (IBAction)showPopupPhotoBoothLayout:(id)sender;

{
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"hideview"];
    
  NSLog(@"Standard Photo Booth Layout Popup");
    [self standardphotoboothlayout:CNPPopupStyleActionSheet];
    
    
    
}



- (void) standardphotoboothlayout:(CNPPopupStyle)popupStyle {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *hideview = @"yes";
    [defaults setObject:hideview forKey:@"hideview"];
    [defaults synchronize];
    _colorlayout.hidden=YES;
   
    
    
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSAttributedString *title = [[NSAttributedString alloc] initWithString:@"SELECT STANDARD LAYOUT" attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:24], NSParagraphStyleAttributeName : paragraphStyle}];
    NSAttributedString *lineOne = [[NSAttributedString alloc] initWithString:@"Choose the layout you want to use. You may also select the design you created that matches the layout you are selecting. If you did not design a layout you may select a color for the font, border, and background." attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:18], NSParagraphStyleAttributeName : paragraphStyle}];
    
    

    
    if (UIInterfaceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation])) // Landscap
    {   scrollviewlayout = [[UIScrollView alloc]initWithFrame:CGRectMake(0 ,0 ,320 ,280)];
        [scrollviewlayout setShowsHorizontalScrollIndicator:YES];
        [scrollviewlayout setShowsVerticalScrollIndicator:NO];
        scrollviewlayout.scrollEnabled= YES;
        scrollviewlayout.userInteractionEnabled= YES;
        scrollviewlayout.contentSize= CGSizeMake(2800 ,280);
        
        for (int i=0; i<17; i++) {
            UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(175*i+10, 0, 170, 261) ];
            
            
            imgView.tag = i+20;
            [scrollviewlayout addSubview:imgView];
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
            NSLog(@"Scroll view image: %@", [NSString stringWithFormat:@"vertical%d_thumbnail", i+2]);
            [btn setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"vertical%d_thumbnail", i+2]] forState:UIControlStateNormal];
            
            
            btn.frame = CGRectMake(175*i+20, 10, 150, 241);
            
            
            
            btn.tag = i;
            [btn addTarget:self action:@selector(onClickTheme:) forControlEvents:UIControlEventTouchUpInside];
            
            
            [scrollviewlayout addSubview:btn];
        }
        
        
      
    }
    
    else{
        scrollviewlayout = [[UIScrollView alloc]initWithFrame:CGRectMake(0 ,0 ,320 ,280)];
        [scrollviewlayout setShowsHorizontalScrollIndicator:YES];
        [scrollviewlayout setShowsVerticalScrollIndicator:NO];
        scrollviewlayout.scrollEnabled= YES;
        scrollviewlayout.userInteractionEnabled= YES;
        scrollviewlayout.contentSize= CGSizeMake(2800 ,280);
        
        for (int i=0; i<19; i++) {
            UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(175*i+10, 0, 170, 261) ];
            
            
            imgView.tag = i+20;
            [scrollviewlayout addSubview:imgView];
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
            NSLog(@"Scroll view image: %@", [NSString stringWithFormat:@"layout%d_thumbnail", i+2]);
            [btn setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"layout%d_thumbnail", i+2]] forState:UIControlStateNormal];
            
            
            btn.frame = CGRectMake(175*i+20, 10, 150, 241);
            
            
            
            btn.tag = i;
            [btn addTarget:self action:@selector(onClickTheme:) forControlEvents:UIControlEventTouchUpInside];
            
            
            [scrollviewlayout addSubview:btn];
        }
        
        
           }
    
    
    CNPPopupButton *button = [[CNPPopupButton alloc] initWithFrame:CGRectMake(0, 0, 200, 60)];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button setTitle:@"Exit" forState:UIControlStateNormal];
    button.backgroundColor = [UIColor colorWithRed:0.46 green:0.8 blue:1.0 alpha:1.0];
    button.layer.cornerRadius = 0;
    button.selectionHandler = ^(CNPPopupButton *button){
      
//        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//        TSPAddToDoViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"PhotoBoothAdmin"];
//        [vc setManagedObjectContext:self.managedObjectContext];
//        
//        [vc setModalPresentationStyle:UIModalPresentationFullScreen];
//        [self presentViewController:vc animated:NO completion:nil];
        
        
        if (UIInterfaceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation])) // Landscap
        {
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSString* layout = [defaults objectForKey:@"hideview"];
            if ([layout isEqualToString: @"yes"])
            {
                layoutselected.hidden=NO;
                NSUserDefaults *defaults1 = [NSUserDefaults standardUserDefaults];
                NSString* layout = [defaults1 objectForKey:@"shrTheme"];
                if ([layout isEqualToString: @"0"])
                {
                    whatselected.text = @"LAYOUT";
                    selectedimage.image = [UIImage imageNamed:@"vertical2_thumbnail.png"];
                    NSLog(@"LAYOUT HIDDEN");
                }
                
                if ([layout isEqualToString: @"1"])
                {
                    whatselected.text = @"LAYOUT";
                    selectedimage.image = [UIImage imageNamed:@"vertical3_thumbnail.png"];
                    NSLog(@"LAYOUT HIDDEN");
                }
                
                if ([layout isEqualToString: @"2"])
                {
                    whatselected.text = @"LAYOUT";
                    selectedimage.image = [UIImage imageNamed:@"vertical4_thumbnail.png"];
                    NSLog(@"LAYOUT HIDDEN");
                }
                
                if ([layout isEqualToString: @"3"])
                {
                    whatselected.text = @"LAYOUT";
                    selectedimage.image = [UIImage imageNamed:@"vertical5_thumbnail.png"];
                    NSLog(@"LAYOUT HIDDEN");
                }
                
                if ([layout isEqualToString: @"4"])
                {
                    whatselected.text = @"LAYOUT";
                    selectedimage.image = [UIImage imageNamed:@"vertical6_thumbnail.png"];
                    NSLog(@"LAYOUT HIDDEN");
                }
                
                if ([layout isEqualToString: @"5"])
                {
                    whatselected.text = @"LAYOUT";
                    selectedimage.image = [UIImage imageNamed:@"vertical7_thumbnail.png"];
                    NSLog(@"LAYOUT HIDDEN");
                }
                
                if ([layout isEqualToString: @"6"])
                {
                    whatselected.text = @"LAYOUT";
                    selectedimage.image = [UIImage imageNamed:@"vertical8_thumbnail.png"];
                    NSLog(@"LAYOUT HIDDEN");
                }
                
                if ([layout isEqualToString: @"7"])
                {
                    whatselected.text = @"LAYOUT";
                    selectedimage.image = [UIImage imageNamed:@"vertical9_thumbnail.png"];
                    NSLog(@"LAYOUT HIDDEN");
                }
                
                if ([layout isEqualToString: @"8"])
                {
                    whatselected.text = @"LAYOUT";
                    selectedimage.image = [UIImage imageNamed:@"vertical10_thumbnail.png"];
                    NSLog(@"LAYOUT HIDDEN");
                }
                
                if ([layout isEqualToString: @"9"])
                {
                    whatselected.text = @"LAYOUT";
                    selectedimage.image = [UIImage imageNamed:@"vertical11_thumbnail.png"];
                    NSLog(@"LAYOUT HIDDEN");
                }
                
                if ([layout isEqualToString: @"10"])
                {
                    whatselected.text = @"LAYOUT";
                    selectedimage.image = [UIImage imageNamed:@"vertical12_thumbnail.png"];
                    NSLog(@"LAYOUT HIDDEN");
                }
                
                if ([layout isEqualToString: @"11"])
                {
                    whatselected.text = @"LAYOUT";
                    selectedimage.image = [UIImage imageNamed:@"vertical13_thumbnail.png"];
                    NSLog(@"LAYOUT HIDDEN");
                }
                
                if ([layout isEqualToString: @"12"])
                {
                    whatselected.text = @"LAYOUT";
                    selectedimage.image = [UIImage imageNamed:@"vertical14_thumbnail.png"];
                    NSLog(@"LAYOUT HIDDEN");
                }
                
                if ([layout isEqualToString: @"13"])
                {
                    whatselected.text = @"LAYOUT";
                    selectedimage.image = [UIImage imageNamed:@"vertical15_thumbnail.png"];
                    NSLog(@"LAYOUT HIDDEN");
                }
                
                if ([layout isEqualToString: @"14"])
                {
                    whatselected.text = @"LAYOUT";
                    selectedimage.image = [UIImage imageNamed:@"vertical16_thumbnail.png"];
                    NSLog(@"LAYOUT HIDDEN");
                }
                
                if ([layout isEqualToString: @"15"])
                {
                    whatselected.text = @"LAYOUT";
                    selectedimage.image = [UIImage imageNamed:@"vertical17_thumbnail.png"];
                    NSLog(@"LAYOUT HIDDEN");
                }
                
                NSLog(@"LAYOUT HIDDEN");
            }
        }
        
        else
        {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString* layout = [defaults objectForKey:@"hideview"];
        if ([layout isEqualToString: @"yes"])
        {
            layoutselected.hidden=NO;
            NSUserDefaults *defaults1 = [NSUserDefaults standardUserDefaults];
            NSString* layout = [defaults1 objectForKey:@"shrTheme"];
            if ([layout isEqualToString: @"0"])
            {
                whatselected.text = @"LAYOUT";
                selectedimage.image = [UIImage imageNamed:@"layout2_thumbnail.png"];
                NSLog(@"LAYOUT HIDDEN");
            }
        
            if ([layout isEqualToString: @"1"])
            {
                whatselected.text = @"LAYOUT";
                selectedimage.image = [UIImage imageNamed:@"layout3_thumbnail.png"];
                NSLog(@"LAYOUT HIDDEN");
            }
            
            if ([layout isEqualToString: @"2"])
            {
                whatselected.text = @"LAYOUT";
                selectedimage.image = [UIImage imageNamed:@"layout4_thumbnail.png"];
                NSLog(@"LAYOUT HIDDEN");
            }
            
            if ([layout isEqualToString: @"3"])
            {
                whatselected.text = @"LAYOUT";
                selectedimage.image = [UIImage imageNamed:@"layout5_thumbnail.png"];
                NSLog(@"LAYOUT HIDDEN");
            }
            
            if ([layout isEqualToString: @"4"])
            {
                whatselected.text = @"LAYOUT";
                selectedimage.image = [UIImage imageNamed:@"layout6_thumbnail.png"];
                NSLog(@"LAYOUT HIDDEN");
            }
            
            if ([layout isEqualToString: @"5"])
            {
                whatselected.text = @"LAYOUT";
                selectedimage.image = [UIImage imageNamed:@"layout7_thumbnail.png"];
                NSLog(@"LAYOUT HIDDEN");
            }
            
            if ([layout isEqualToString: @"6"])
            {
                whatselected.text = @"LAYOUT";
                selectedimage.image = [UIImage imageNamed:@"layout8_thumbnail.png"];
                NSLog(@"LAYOUT HIDDEN");
            }
            
            if ([layout isEqualToString: @"7"])
            {
                whatselected.text = @"LAYOUT";
                selectedimage.image = [UIImage imageNamed:@"layout9_thumbnail.png"];
                NSLog(@"LAYOUT HIDDEN");
            }
            
            if ([layout isEqualToString: @"8"])
            {
                whatselected.text = @"LAYOUT";
                selectedimage.image = [UIImage imageNamed:@"layout10_thumbnail.png"];
                NSLog(@"LAYOUT HIDDEN");
            }
            
            if ([layout isEqualToString: @"9"])
            {
                whatselected.text = @"LAYOUT";
                selectedimage.image = [UIImage imageNamed:@"layout11_thumbnail.png"];
                NSLog(@"LAYOUT HIDDEN");
            }
            
            if ([layout isEqualToString: @"10"])
            {
                whatselected.text = @"LAYOUT";
                selectedimage.image = [UIImage imageNamed:@"layout12_thumbnail.png"];
                NSLog(@"LAYOUT HIDDEN");
            }
            
            if ([layout isEqualToString: @"11"])
            {
                whatselected.text = @"LAYOUT";
                selectedimage.image = [UIImage imageNamed:@"layout13_thumbnail.png"];
                NSLog(@"LAYOUT HIDDEN");
            }
            
            if ([layout isEqualToString: @"12"])
            {
                whatselected.text = @"LAYOUT";
                selectedimage.image = [UIImage imageNamed:@"layout14_thumbnail.png"];
                NSLog(@"LAYOUT HIDDEN");
            }
            
            if ([layout isEqualToString: @"13"])
            {
                whatselected.text = @"LAYOUT";
                selectedimage.image = [UIImage imageNamed:@"layout15_thumbnail.png"];
                NSLog(@"LAYOUT HIDDEN");
            }
            
            if ([layout isEqualToString: @"14"])
            {
                whatselected.text = @"LAYOUT";
                selectedimage.image = [UIImage imageNamed:@"layout16_thumbnail.png"];
                NSLog(@"LAYOUT HIDDEN");
            }
            
            if ([layout isEqualToString: @"15"])
            {
                whatselected.text = @"LAYOUT";
                selectedimage.image = [UIImage imageNamed:@"layout17_thumbnail.png"];
                NSLog(@"LAYOUT HIDDEN");
            }
            
            NSLog(@"LAYOUT HIDDEN");
        }
        
        }
        
        
        [self.popupController dismissPopupControllerAnimated:YES];
        NSLog(@"Block for button: %@", button.titleLabel.text);
    };
    
    
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.numberOfLines = 0;
    titleLabel.attributedText = title;
    
    
    
    
    
    
    
    UILabel *lineOneLabel = [[UILabel alloc] init];
    lineOneLabel.numberOfLines = 0;
    lineOneLabel.attributedText = lineOne;
    self.popupController = [[CNPPopupController alloc] initWithContents:@[titleLabel, lineOneLabel, scrollviewlayout, button]];
    self.popupController.theme = [CNPPopupTheme defaultTheme];
    self.popupController.theme.popupStyle = popupStyle;
    self.popupController.delegate = self;
    [self.popupController presentPopupControllerAnimated:YES];
    
}



- (IBAction)toggleSave:(id)sender
{
    
    
    UISegmentedControl *control = sender;
    if (control.selectedSegmentIndex == 0)
    {
        _saveFormat = FORMAT_VIDEO;
        NSLog (@"SAVE AS VIDEO");
    }
    
    if (control.selectedSegmentIndex == 1)
    {
         _saveFormat = FORMAT_GIF;
        NSLog (@"SAVE AS GIF");
    }
    
}


- (IBAction)toggleHUD:(id)sender
{
    
    
    UISegmentedControl *control = sender;
    self.imageslayout.hidden = (control.selectedSegmentIndex == 0) ? NO : YES;
    self.colorlayout.hidden = (control.selectedSegmentIndex == 1) ? NO : YES;
    
    if (control.selectedSegmentIndex == 1)
    {
            NSString *eventdate = txtFldEDate.text;
        NSLog (@"SPBB IS OFF");
    }
    
    if (control.selectedSegmentIndex == 0)
    {
        switchPhotoBoothBackground.on = YES;
        NSLog (@"SPBB IS ON");
    }
    
}



- (IBAction) onClickTheme:(id)sender
{
   [self refreshTheme:[sender tag]];
}

- (void) refreshTheme:(int)index
{
    for (int i=0; i<17; i++)
    {
        UIImageView *imgView = (UIImageView*)[scrollviewlayout viewWithTag:i+20];
    
        if (i == index)
            imgView.image = [UIImage imageNamed:@"theme_frame"];
        else
            imgView.image = nil;
    }
    
    if (index == 0)
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *numberofphotos = @"3";
        _numberOfPhotosLayout = numberofphotos;
        [defaults setObject:_numberOfPhotosLayout forKey:@"numberofphotos"];
        
    }
    
    if (index == 1)
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *numberofphotos = @"4";
        _numberOfPhotosLayout = numberofphotos;
        [defaults setObject:_numberOfPhotosLayout forKey:@"numberofphotos"];
        
    }
    if (index == 2)
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *numberofphotos = @"4";
        _numberOfPhotosLayout = numberofphotos;
        [defaults setObject:_numberOfPhotosLayout forKey:@"numberofphotos"];
        
    }
    
    if (index == 3)
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *numberofphotos = @"3";
        _numberOfPhotosLayout = numberofphotos;
        [defaults setObject:_numberOfPhotosLayout forKey:@"numberofphotos"];
        
    }
    
    if (index == 4)
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *numberofphotos = @"3";
        _numberOfPhotosLayout = numberofphotos;
        [defaults setObject:_numberOfPhotosLayout forKey:@"numberofphotos"];
        
    }
    
    if (index == 5)
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *numberofphotos = @"4";
        _numberOfPhotosLayout = numberofphotos;
        [defaults setObject:_numberOfPhotosLayout forKey:@"numberofphotos"];
        
    }
    
    if (index == 6)
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *numberofphotos = @"3";
        _numberOfPhotosLayout = numberofphotos;
        [defaults setObject:_numberOfPhotosLayout forKey:@"numberofphotos"];
        
    }
    
    if (index == 7)
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *numberofphotos = @"4";
        _numberOfPhotosLayout = numberofphotos;
        [defaults setObject:_numberOfPhotosLayout forKey:@"numberofphotos"];
        
    }
    
    
    if (index == 8)
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *numberofphotos = @"3";
        _numberOfPhotosLayout = numberofphotos;
        [defaults setObject:_numberOfPhotosLayout forKey:@"numberofphotos"];
        
    }
    
    if (index == 9)
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *numberofphotos = @"4";
        _numberOfPhotosLayout = numberofphotos;
        [defaults setObject:_numberOfPhotosLayout forKey:@"numberofphotos"];
        
    }
    if (index == 10)
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *numberofphotos = @"4";
        _numberOfPhotosLayout = numberofphotos;
        [defaults setObject:_numberOfPhotosLayout forKey:@"numberofphotos"];
        
    }
    
    if (index == 11)
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *numberofphotos = @"3";
        _numberOfPhotosLayout = numberofphotos;
        [defaults setObject:_numberOfPhotosLayout forKey:@"numberofphotos"];
        
    }
    
    if (index == 12)
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *numberofphotos = @"3";
        _numberOfPhotosLayout = numberofphotos;
        [defaults setObject:_numberOfPhotosLayout forKey:@"numberofphotos"];
        NSLog(@"Number of photos %@", numberofphotos);
    }
    
    if (index == 13)
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *numberofphotos = @"4";
        _numberOfPhotosLayout = numberofphotos;
        [defaults setObject:_numberOfPhotosLayout forKey:@"numberofphotos"];
    }
    
    if (index == 14)
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *numberofphotos = @"3";
        _numberOfPhotosLayout = numberofphotos;
        [defaults setObject:numberofphotos forKey:@"numberofphotos"];
        
    }
    
    if (index == 15)
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *numberofphotos = @"4";
        _numberOfPhotosLayout = numberofphotos;
        [defaults setObject:numberofphotos forKey:@"numberofphotos"];
        
    }
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *theme = [NSString stringWithFormat:@"%d", index];
        [defaults setObject:theme forKey:@"shrTheme"];
    
  
    
    
        [defaults synchronize];
        NSLog(@"Layout is %@", theme);

    
}


-(IBAction)printbackgroundcolor:(id)sender

{
    NSLog(@"Print Background Color switch is ON");
    colorPicker = [[RzColorPickerView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(self.view.frame), CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame))];
    [self.view addSubview:colorPicker];
    //   [colorPicker setCurrentColor:PrintBackgroundColor];
    [UIView animateWithDuration:1 animations:^{
        [colorPicker setFrame:self.view.bounds];
    }];
    [colorPicker doneWithSelectedColor:^(UIColor * _Nonnull newColor)
     {
         PrintBackgroundColor = newColor;
         for (UIView *view in lineseparator2) {
             view.backgroundColor = PrintBackgroundColor;
         }

         
         
     }];
    
    
    
}

-(IBAction)printfontbordercolor:(id)sender

{
  
    NSLog(@"Print Background Color switch is ON");
    colorPicker = [[RzColorPickerView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(self.view.frame), CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame))];
    [self.view addSubview:colorPicker];
    // [colorPicker setCurrentColor:PrintFontBorderColor];
    [UIView animateWithDuration:1 animations:^{
        [colorPicker setFrame:self.view.bounds];
    }];
    [colorPicker doneWithSelectedColor:^(UIColor * _Nonnull newColor)
     {
         PrintFontBorderColor = newColor;
         for (UIView *view in labelcolors2) {
             view.backgroundColor = PrintFontBorderColor;
         }
         
     }];
    
    
}



-(IBAction)savesharing:(id)sender
{
//    NSString *username1 = username.text;
//    NSString *password1 = password.text;
//    NSString *emailaddress1 = emailaddress.text;
    NSString *eventname = txtFldCNames.text;
    NSString *eventdate = txtFldEDate.text;
NSString *maxsharing = maxSelectionSharingLabel.text;
    
    if ([txtFldCNames.text isEqualToString:@""] || [txtFldEDate.text isEqualToString:@""])
    {
        FCAlertView *alert = [[FCAlertView alloc] init];
        alert.darkTheme = YES;
        [alert makeAlertTypeCaution];
        alert.detachButtons = YES;
        [alert setAlertSoundWithFileName:@"error.mp3"];
        alert.blurBackground = YES;
    [alert showAlertWithTitle:@"MISSING INFORMATION"
                 withSubtitle:@"Make sure you've entered an event name and an event date."
              withCustomImage:nil
          withDoneButtonTitle:nil
                   andButtons:nil];
        
        
    }
    
 
    
    
    else
    {
        
       
            if (eventname && eventname.length)
            {
                // Create Entity
                NSEntityDescription *entity = [NSEntityDescription entityForName:@"TSPItem" inManagedObjectContext:self.managedObjectContext];
                
                // Initialize Record
                NSManagedObject *record = [[NSManagedObject alloc] initWithEntity:entity insertIntoManagedObjectContext:self.managedObjectContext];
                
                [record setValue:eventname forKey:@"eventname"];
                [record setValue:eventdate forKey:@"eventdate"];
                
                NSString *theColorStr = [LayoutBackgroundColor stringFromColor];
                [record setValue:theColorStr forKey:@"layoutbackgroundcolor"];
                NSString *theColorStr1 = [LayoutFontBorderColor stringFromColor];
                [record setValue:theColorStr1 forKey:@"layoutfontbordercolor"];
                
                NSData *imageData = UIImageJPEGRepresentation (welcomeimage, 0.7);
                [record setValue:imageData forKey:@"welcomebackground"];
                
                [record setValue:welcomeimagevideo forKey:@"welcomeimagevideo"];
                [record setValue:finalimagevideo forKey:@"finalimagevideo"];
                
                NSData *imageData6 =  UIImageJPEGRepresentation (emailimage, 0.7);
                [record setValue:imageData6 forKey:@"emailbackground"];
                
                NSData *imageData7 =  UIImageJPEGRepresentation (finalimage, 0.7);
                [record setValue:imageData7 forKey:@"finalbackground"];
                
                NSData *imageData8 =  UIImageJPEGRepresentation (closedimage, 0.7);
                [record setValue:imageData8 forKey:@"closedbackground"];
                
                
                [record setValue:maxsharing forKey:@"maxSelectionSharing"];
                
                NSUserDefaults *standardDefaults = [NSUserDefaults standardUserDefaults];
                NSString* strValue5 = [standardDefaults objectForKey:@"switchSendgrid"];
                
                
                
                if (share.isOn)
                {
                    NSString *switchss = @"YES";
                    [record setValue:switchss forKey:@"share"];
                    
                }
                
                else
                {
                    NSString *switchss = @"NO";
                    [record setValue:switchss forKey:@"share"];
                    
                }
                
                
                
                if ([strValue5 isEqualToString:@"YES"])
                {
                    NSString *switchss = @"YES";
                    [record setValue:switchss forKey:@"switchSendgrid"];
                }
                else
                {
                    NSString *switchss = @"NO";
                    [record setValue:switchss forKey:@"switchSendgrid"];
                }
                
                
                
                
                if (switchTwitter.isOn)
                {
                    NSString *switchss = @"YES";
                    [record setValue:switchss forKey:@"switchTwitter"];
                    
                }
                
                else
                {
                    NSString *switchss = @"NO";
                    [record setValue:switchss forKey:@"switchTwitter"];
                    
                }
                if (switchLayout.isOn)
                {
                    NSString *switchss = @"YES";
                    [record setValue:switchss forKey:@"switchLayout"];
                    
                }
                
                else
                {
                    NSString *switchss = @"NO";
                    [record setValue:switchss forKey:@"switchLayout"];
                    
                }
                
                if (switchMessage.isOn)
                {
                    NSString *switchss = @"YES";
                    [record setValue:switchss forKey:@"switchMessage"];
                    NSLog(@"Switch Welcome %@", switchss);
                }
                
                else
                {
                    NSString *switchss = @"NO";
                    [record setValue:switchss forKey:@"switchMessage"];
                    NSLog(@"Switch Welcome %@", switchss);
                }
                
                
                if (switchFacebook.isOn)
                {
                    NSString *switchss = @"YES";
                    [record setValue:switchss forKey:@"switchFacebook"];
                    
                }
                
                else
                {
                    NSString *switchss = @"NO";
                    [record setValue:switchss forKey:@"switchFacebook"];
                    
                }
                
                
                if (switchEmail.isOn)
                {
                    NSString *switchss = @"YES";
                    [record setValue:switchss forKey:@"switchEmail"];
                    
                }
                
                else
                {
                    NSString *switchss = @"NO";
                    [record setValue:switchss forKey:@"switchEmail"];
                    
                }
                
                if (switchText.isOn)
                {
                    NSString *switchss = @"YES";
                    [record setValue:switchss forKey:@"switchText"];
                    
                }
                
                else
                {
                    NSString *switchss = @"NO";
                    [record setValue:switchss forKey:@"switchText"];
                    
                }
                
                
                
                // Save Record
                NSError *error = nil;
                
                if ([self.managedObjectContext save:&error]) {
                    // Dismiss View Controller
                    [self dismissViewControllerAnimated:YES completion:nil];
                    
                } else {
                    if (error) {
                        NSLog(@"Unable to save record.");
                        NSLog(@"%@, %@", error, error.localizedDescription);
                    }
                    FCAlertView *alert = [[FCAlertView alloc] init];
                    
                    [alert showAlertWithTitle:@"Warning"
                                 withSubtitle:@"Your event could not be saved"
                              withCustomImage:nil
                          withDoneButtonTitle:nil
                                   andButtons:nil];
                    
                    
                }
                
            } else {
                FCAlertView *alert = [[FCAlertView alloc] init];
                
                [alert showAlertWithTitle:@"Warning"
                             withSubtitle:@"Your event could not be saved"
                          withCustomImage:nil
                      withDoneButtonTitle:nil
                               andButtons:nil];
            }
            
            
            [self createAlbum];
            FCAlertView *alert = [[FCAlertView alloc] init];
            alert.darkTheme = YES;
            [alert makeAlertTypeSuccess];
            alert.detachButtons = YES;
            [alert setAlertSoundWithFileName:@"ElevatorDing.mp3"];
            alert.blurBackground = YES;
            [alert showAlertWithTitle:@"SUCCESSFULLY SAVED"
                         withSubtitle:@"Your event has been created and is ready to use at your event."
                      withCustomImage:nil
                  withDoneButtonTitle:nil
                           andButtons:nil];
            
            
            
       

                NSLog(@"its on!");
            }
            
    
//                FCAlertView *alert = [[FCAlertView alloc] init];
//                alert.darkTheme = YES;
//                [alert makeAlertTypeCaution];
//                alert.detachButtons = YES;
//                [alert setAlertSoundWithFileName:@"error.mp3"];
//                alert.blurBackground = YES;
//                [alert showAlertWithTitle:@"YOU HAVE TO SHARE SOMETHING"
//                             withSubtitle:@"You need to turn on at least one sharing option (i.e. Email)"
//                          withCustomImage:nil
//                      withDoneButtonTitle:nil
//                               andButtons:nil];
//                
//
//                NSLog(@"its off!");
//    

    
    
    
    
}




-(IBAction)save:(id)sender
{
    //    NSString *username1 = username.text;
    //    NSString *password1 = password.text;
    //    NSString *emailaddress1 = emailaddress.text;
    NSString *eventname = txtFldCNames.text;
    NSString *eventdate = txtFldEDate.text;
    NSString *max = maxSelection1.text;
    NSString *max1 = maxSelection2.text;
    NSString *animdur = animatedduration.text;
    NSString *animnumber = animatednumber.text;
    NSString *photodur = photopreviewlab.text;
    NSString *savegif = _saveFormat;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString* layout = [defaults objectForKey:@"shrTheme"];
     [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"hideview"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"printbackgroundcolor"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"layoutbackgroundcolor"];
    NSUserDefaults *defaults2 = [NSUserDefaults standardUserDefaults];
    _numberOfPhotosLayout = [defaults2 valueForKey:@"numberofphotos"];
    //    [defaults2 setObject:_numberOfPhotosLayout forKey:@"numberofphotos"];
    
    if ([txtFldCNames.text isEqualToString:@""] || [txtFldEDate.text isEqualToString:@""])
    {
        FCAlertView *alert = [[FCAlertView alloc] init];
        alert.darkTheme = YES;
        [alert makeAlertTypeCaution];
        alert.detachButtons = YES;
        [alert setAlertSoundWithFileName:@"error.mp3"];
        alert.blurBackground = YES;
        [alert showAlertWithTitle:@"MISSING INFORMATION"
                     withSubtitle:@"Make sure you've entered an event name and an event date."
                  withCustomImage:nil
              withDoneButtonTitle:nil
                       andButtons:nil];
        
        
    }
    
    
    else if (_numberOfPhotosLayout == NULL && booth.isOn)
    {
        FCAlertView *alert = [[FCAlertView alloc] init];
        alert.darkTheme = YES;
        [alert makeAlertTypeCaution];
        alert.detachButtons = YES;
        [alert setAlertSoundWithFileName:@"error.mp3"];
        alert.blurBackground = YES;
        [alert showAlertWithTitle:@"MISSING LAYOUT"
                     withSubtitle:@"You need to select a layout for the photo booth."
                  withCustomImage:nil
              withDoneButtonTitle:nil
                       andButtons:nil];
        
        
    }
    
    else if (_saveFormat == nil && booth.on == NO)
    {
        FCAlertView *alert = [[FCAlertView alloc] init];
        alert.darkTheme = YES;
        [alert makeAlertTypeCaution];
        alert.detachButtons = YES;
        [alert setAlertSoundWithFileName:@"error.mp3"];
        alert.blurBackground = YES;
        [alert showAlertWithTitle:@"SELECT A SAVE FORMAT"
                     withSubtitle:@"You must choose to save the animation as an MP4 or GIF."
                  withCustomImage:nil
              withDoneButtonTitle:nil
                       andButtons:nil];
        
        
    }
    
    
    else
    {
        if (switchRear.isOn)
        {
            switchTwitter.enabled=NO;
            switchTwitter.on=NO;
            switchFacebook.enabled=NO;
            switchFacebook.on=NO;
            burstmode.enabled=NO;
            burstmode.on=NO;
            switchGreen.enabled=NO;
            switchGif.enabled=NO;
            switchSepia.enabled=NO;
            switchVideo.enabled=NO;
            switchVintage.enabled=NO;
            switchBlackWhite.enabled=NO;
            switchGreen.on=NO;
            switchGif.on=NO;
            switchSepia.on=NO;
            switchVideo.on=NO;
            switchVintage.on=NO;
            switchBlackWhite.on=NO;
            switchMessage.on=NO;
        }
        
        
        if (eventname && eventname.length)
        {
            // Create Entity
            NSEntityDescription *entity = [NSEntityDescription entityForName:@"TSPItem" inManagedObjectContext:self.managedObjectContext];
            
            // Initialize Record
            NSManagedObject *record = [[NSManagedObject alloc] initWithEntity:entity insertIntoManagedObjectContext:self.managedObjectContext];
            
            [record setValue:layout forKey:@"shrTheme"];
            
            [record setValue:_numberOfPhotosLayout forKey:@"numberofphotos"];
           [record setValue:_saveFormat forKey:@"savinggif"];
            [record setValue:eventname forKey:@"eventname"];
            [record setValue:eventdate forKey:@"eventdate"];
            [record setValue:max forKey:@"maxSelection1"];
            [record setValue:max1 forKey:@"maxSelection2"];
            [record setValue:animdur forKey:@"animatedduration"];
            [record setValue:animnumber forKey:@"animatednumber"];
            [record setValue:photodur forKey:@"photopreview"];
            NSString *theColorStr = [LayoutBackgroundColor stringFromColor];
            [record setValue:theColorStr forKey:@"layoutbackgroundcolor"];
            NSString *theColorStr1 = [LayoutFontBorderColor stringFromColor];
            [record setValue:theColorStr1 forKey:@"layoutfontbordercolor"];
            
            NSString *theColorStr2 = [PrintBackgroundColor stringFromColor];
            [record setValue:theColorStr2 forKey:@"printbackgroundcolor"];
            
            NSString *theColorStr3 = [PrintFontBorderColor stringFromColor];
            [record setValue:theColorStr3 forKey:@"printfontbordercolor"];
            
            NSData *imageData = UIImageJPEGRepresentation (welcomeimage, 0.7);
            [record setValue:imageData forKey:@"welcomebackground"];
            
            [record setValue:welcomeimagevideo forKey:@"welcomeimagevideo"];
            [record setValue:finalimagevideo forKey:@"finalimagevideo"];
            NSData *imageData1 =  UIImagePNGRepresentation([TSPAppDelegate sharedAppDelegate].imgCustomBackground);
            [record setValue:imageData1 forKey:@"custombackground"];
            
            NSData *imageData2 =  UIImageJPEGRepresentation (readyimage, 0.7);
            [record setValue:imageData2 forKey:@"readybackground"];
            
            NSData *imageData4 =  UIImageJPEGRepresentation (remainingimage, 0.7);
            [record setValue:imageData4 forKey:@"remainingbackground"];
            
            NSData *imageData5 =  UIImageJPEGRepresentation (videoimage, 0.7);
            [record setValue:imageData5 forKey:@"videobackground"];
            
            NSData *imageData6 =  UIImageJPEGRepresentation (emailimage, 0.7);
            [record setValue:imageData6 forKey:@"emailbackground"];
            
            NSData *imageData7 =  UIImageJPEGRepresentation (finalimage, 0.7);
            [record setValue:imageData7 forKey:@"finalbackground"];
            
            NSData *imageData8 =  UIImageJPEGRepresentation (closedimage, 0.7);
            [record setValue:imageData8 forKey:@"closedbackground"];
            
            NSData *imageData9 =  UIImagePNGRepresentation (animatedlayoutimage);
            [record setValue:imageData9 forKey:@"animatedlayoutbackground"];
            
            NSData *imageData10 =  UIImagePNGRepresentation ([TSPAppDelegate sharedAppDelegate].photoboothlayoutimage);
            [record setValue:imageData10 forKey:@"photoboothlayoutbackground"];
            
            NSData *imageData11 =  UIImageJPEGRepresentation (effectsimage, 0.7);
            [record setValue:imageData11 forKey:@"effectsbackground"];
            
            NSData *imageData12 =  UIImageJPEGRepresentation (greenscreenimage, 0.7);
            [record setValue:imageData12 forKey:@"greenscreenbackground"];
            
            
            
            NSUserDefaults *standardDefaults = [NSUserDefaults standardUserDefaults];
            NSString* strValue5 = [standardDefaults objectForKey:@"switchSendgrid"];
            NSString* strValue11 = [standardDefaults objectForKey:@"switchIndividual"];
            NSString* strValue12 = [standardDefaults objectForKey:@"switchLayout"];
            
            NSString* strValueface = [standardDefaults objectForKey:@"switchFacebookIndividual"];
            NSString* strValueface1 = [standardDefaults objectForKey:@"switchFacebookLayout"];
            
            NSString* strValuetwit = [standardDefaults objectForKey:@"switchTwitterIndividual"];
            NSString* strValuetwit1 = [standardDefaults objectForKey:@"switchTwitterLayout"];
            
            NSString* strValuetext = [standardDefaults objectForKey:@"switchTextIndividual"];
            NSString* strValuetext1 = [standardDefaults objectForKey:@"switchTextLayout"];
            
            
            NSString* strValueSharing = [standardDefaults objectForKey:@"switchSharingIndividual"];
            NSString* strValueSharing1 = [standardDefaults objectForKey:@"switchSharingLayout"];
            
            
            if (switchSharing.isOn) {
                NSString *switchss = @"YES";
                [record setValue:switchss forKey:@"switchSharing"];
          
            }
            
            else {
                NSString *switchss = @"NO";
                [record setValue:switchss forKey:@"switchSharing"];
           
            }
            
            
            
            if ([strValueSharing isEqualToString:@"YES"])
            {
                NSString *switchss = @"YES";
                [record setValue:switchss forKey:@"switchSharingIndividual"];
            }
            else
            {
                NSString *switchss = @"NO";
                [record setValue:switchss forKey:@"switchSharingIndividual"];
            }
            
            if ([strValueSharing1 isEqualToString:@"YES"])
            {
                NSString *switchss = @"YES";
                [record setValue:switchss forKey:@"switchSharingLayout"];
            }
            else
            {
                NSString *switchss = @"NO";
                [record setValue:switchss forKey:@"switchSharingLayout"];
            }

            
            if ([strValue5 isEqualToString:@"YES"])
            {
                NSString *switchss = @"YES";
                [record setValue:switchss forKey:@"switchSendgrid"];
            }
            else
            {
                NSString *switchss = @"NO";
                [record setValue:switchss forKey:@"switchSendgrid"];
            }
            
            if ([strValueface isEqualToString:@"YES"])
            {
                NSString *switchss = @"YES";
                [record setValue:switchss forKey:@"switchFacebookIndividual"];
            }
            else
            {
                NSString *switchss = @"NO";
                [record setValue:switchss forKey:@"switchFacebookIndividual"];
            }
            
            if ([strValueface1 isEqualToString:@"YES"])
            {
                NSString *switchss = @"YES";
                [record setValue:switchss forKey:@"switchFacebookLayout"];
            }
            else
            {
                NSString *switchss = @"NO";
                [record setValue:switchss forKey:@"switchFacebookLayout"];
            }
            
            if ([strValuetwit isEqualToString:@"YES"])
            {
                NSString *switchss = @"YES";
                [record setValue:switchss forKey:@"switchTwitterIndividual"];
            }
            else
            {
                NSString *switchss = @"NO";
                [record setValue:switchss forKey:@"switchTwitterIndividual"];
            }
            
            if ([strValuetwit1 isEqualToString:@"YES"])
            {
                NSString *switchss = @"YES";
                [record setValue:switchss forKey:@"switchTwitterLayout"];
            }
            else
            {
                NSString *switchss = @"NO";
                [record setValue:switchss forKey:@"switchTwitterLayout"];
            }
            
            if ([strValuetext isEqualToString:@"YES"])
            {
                NSString *switchss = @"YES";
                [record setValue:switchss forKey:@"switchTextIndividual"];
            }
            else
            {
                NSString *switchss = @"NO";
                [record setValue:switchss forKey:@"switchTextIndividual"];
            }
            
            if ([strValuetext1 isEqualToString:@"YES"]) {
                NSString *switchss = @"YES";
                [record setValue:switchss forKey:@"switchTextLayout"];
            }
            
            else {
                NSString *switchss = @"NO";
                [record setValue:switchss forKey:@"switchTextLayout"];
            }
            
            
            
            
            
            if ([strValue11 isEqualToString:@"YES"]) {
                NSString *switchss = @"YES";
                [record setValue:switchss forKey:@"switchIndividual"];
            }
            
            else {
                NSString *switchss = @"NO";
                [record setValue:switchss forKey:@"switchIndividual"];
            }
            
            if ([strValue12 isEqualToString:@"YES"]) {
                NSString *switchss = @"YES";
                [record setValue:switchss forKey:@"switchLayout"];
            }
            
            else {
                NSString *switchss = @"NO";
                [record setValue:switchss forKey:@"switchLayout"];
            }
            
            
            
            if (booth.isOn)
            {
                NSString *switchss = @"YES";
                [record setValue:switchss forKey:@"booth"];
                
            }
            
            else
            {
                NSString *switchss = @"NO";
                [record setValue:switchss forKey:@"booth"];
                
            }
            
            
            
            if (burstmode.isOn)
            {
                NSString *switchss = @"YES";
                [record setValue:switchss forKey:@"burstmode"];
                
            }
            
            else
            {
                NSString *switchss = @"NO";
                [record setValue:switchss forKey:@"burstmode"];
                
            }
            
            if (switchOverlayAnimatedGIFBackground.isOn)
            {
                NSString *switchss = @"YES";
                [record setValue:switchss forKey:@"switchOverlayAnimatedGIFBackground"];
                
            }
            
            else
            {
                NSString *switchss = @"NO";
                [record setValue:switchss forKey:@"switchOverlayAnimatedGIFBackground"];
                
            }
            if (switchPhotoBoothBackground.isOn)
            {
                NSString *switchss = @"YES";
                [record setValue:switchss forKey:@"switchPhotoBoothBackground"];
                
            }
            
            else
            {
                NSString *switchss = @"NO";
                [record setValue:switchss forKey:@"switchPhotoBoothBackground"];
                
            }
            
            
            if (boomerang.isOn)
            {
                NSString *switchss = @"YES";
                [record setValue:switchss forKey:@"boomerang"];
                
            }
            
            else
            {
                NSString *switchss = @"NO";
                [record setValue:switchss forKey:@"boomerang"];
                
            }
            
            
            if (switchRear.isOn)
            {
                NSString *switchss = @"YES";
                [record setValue:switchss forKey:@"switchRear"];
              
                
            }
            
            else
            {
                NSString *switchss = @"NO";
                [record setValue:switchss forKey:@"switchRear"];
            
                
            }
            if (switchSepia.isOn)
            {
                NSString *switchss = @"YES";
                [record setValue:switchss forKey:@"switchSepia"];
                
            }
            
            else
            {
                NSString *switchss = @"NO";
                [record setValue:switchss forKey:@"switchSepia"];
                
            }
            if (switchTwitter.isOn)
            {
                NSString *switchss = @"YES";
                [record setValue:switchss forKey:@"switchTwitter"];
                
            }
            
            else
            {
                NSString *switchss = @"NO";
                [record setValue:switchss forKey:@"switchTwitter"];
                
            }
            if (switchVideo.isOn)
            {
                NSString *switchss = @"YES";
                [record setValue:switchss forKey:@"switchVideo"];
                
            }
            
            else
            {
                NSString *switchss = @"NO";
                [record setValue:switchss forKey:@"switchVideo"];
                
            }
            if (switchVintage.isOn)
            {
                NSString *switchss = @"YES";
                [record setValue:switchss forKey:@"switchVintage"];
                
            }
            
            else
            {
                NSString *switchss = @"NO";
                [record setValue:switchss forKey:@"switchVintage"];
                
            }
            
            
            
            if (switchFinalAnim.isOn)
            {
                NSString *switchss = @"YES";
                [record setValue:switchss forKey:@"switchFinalAnim"];
                
            }
            
            else
            {
                NSString *switchss = @"NO";
                [record setValue:switchss forKey:@"switchFinalAnim"];
                
            }
            
            if (switchGif.isOn)
            {
                NSString *switchss = @"YES";
                [record setValue:switchss forKey:@"switchGif"];
                
            }
            
            else
            {
                NSString *switchss = @"NO";
                [record setValue:switchss forKey:@"switchGif"];
                
            }
            
            if (switchIndividual.isOn)
            {
                NSString *switchss = @"YES";
                [record setValue:switchss forKey:@"switchIndividual"];
                
            }
            
            else
            {
                NSString *switchss = @"NO";
                [record setValue:switchss forKey:@"switchIndividual"];
                
            }
            
            if (switchLayout.isOn)
            {
                NSString *switchss = @"YES";
                [record setValue:switchss forKey:@"switchLayout"];
                
            }
            
            else
            {
                NSString *switchss = @"NO";
                [record setValue:switchss forKey:@"switchLayout"];
                
            }
            
            if (switchMessage.isOn)
            {
                NSString *switchss = @"YES";
                [record setValue:switchss forKey:@"switchMessage"];
                
            }
            
            else
            {
                NSString *switchss = @"NO";
                [record setValue:switchss forKey:@"switchMessage"];
                
            }
            
            if (switchMessageAnim.isOn)
            {
                NSString *switchss = @"YES";
                [record setValue:switchss forKey:@"switchMessageAnim"];
                
            }
            
            else
            {
                NSString *switchss = @"NO";
                [record setValue:switchss forKey:@"switchMessageAnim"];
                
            }
            
            if ([TSPAppDelegate sharedAppDelegate].isSwitchOverlay)
            {
                NSString *switchss = @"YES";
                [record setValue:switchss forKey:@"switchOverlay"];
                
            }
            
            else
            {
                NSString *switchss = @"NO";
                [record setValue:switchss forKey:@"switchOverlay"];
                
            }
            
            
            if (switchFacebook.isOn)
            {
                NSString *switchss = @"YES";
                [record setValue:switchss forKey:@"switchFacebook"];
                
            }
            
            else
            {
                NSString *switchss = @"NO";
                [record setValue:switchss forKey:@"switchFacebook"];
                
            }
            
            
            if (switchEmail.isOn)
            {
                NSString *switchss = @"YES";
                [record setValue:switchss forKey:@"switchEmail"];
                
            }
            
            else
            {
                NSString *switchss = @"NO";
                [record setValue:switchss forKey:@"switchEmail"];
                
            }
            
            if (switchText.isOn)
            {
                NSString *switchss = @"YES";
                [record setValue:switchss forKey:@"switchText"];
                
            }
            
            else
            {
                NSString *switchss = @"NO";
                [record setValue:switchss forKey:@"switchText"];
                
            }
            
            if (switchCustom.isOn)
            {
                NSString *switchss = @"YES";
                [record setValue:switchss forKey:@"switchCustom"];
                
            }
            
            else
            {
                NSString *switchss = @"NO";
                [record setValue:switchss forKey:@"switchCustom"];
                
            }
            
            if (switchBlackWhite.isOn)
            {
                NSString *switchss = @"YES";
                [record setValue:switchss forKey:@"switchBlackWhite"];
                
            }
            
            else
            {
                NSString *switchss = @"NO";
                [record setValue:switchss forKey:@"switchBlackWhite"];
                
            }
            
            
            if (switchGreen.isOn)
            {
                NSString *switchss = @"YES";
                [record setValue:switchss forKey:@"switchGreen"];
                
            }
            
            else
            {
                NSString *switchss = @"NO";
                [record setValue:switchss forKey:@"switchGreen"];
                
            }
            
            if ( switchAnimatedLayout.isOn)
            {
                NSString *switchss = @"YES";
                [record setValue:switchss forKey:@"switchAnimatedLayout"];
                
            }
            
            else
            {
                NSString *switchss = @"NO";
                [record setValue:switchss forKey:@"switchAnimatedLayout"];
                
            }
            
             NSLog(@"WELL REAR IS %@",record);
            //Save Properties for Custom Layout
            if ([layout isEqualToString:@"CustomTheme"])
            {
                TSPEventRepository* eventObject = [[TSPEventRepository alloc] init];
                
                NSMutableArray *archiveArray = [NSMutableArray arrayWithCapacity:[TSPAppDelegate sharedAppDelegate].customLayoutArray.count];
                for (Rectangle *rectangle in [TSPAppDelegate sharedAppDelegate].customLayoutArray)
                {
                    NSData *rectangleData = [NSKeyedArchiver archivedDataWithRootObject:rectangle];
                    [archiveArray addObject:rectangleData];
                }
                
                [eventObject setCustomLayoutArray:archiveArray];
                [eventObject setCustomLayoutCount:[TSPAppDelegate sharedAppDelegate].customLayoutCount];
                [eventObject setIsLargeCustomLayout:[TSPAppDelegate sharedAppDelegate].isLargeCustomLayout];
                [eventObject setIsLargeCustomLayoutH:[TSPAppDelegate sharedAppDelegate].isLargeCustomLayouth];
                [eventObject setIsSmallCustomLayout:[TSPAppDelegate sharedAppDelegate].isSmallCustomLayout];
                [eventObject setIsSmallCustomLayoutH:[TSPAppDelegate sharedAppDelegate].isSmallCustomLayouth];
                [eventObject setEventID:[Helper uuid]];
                
                [record setValue:eventObject.eventID forKey:@"eventid"];
                
                if ([TSPAppDelegate sharedAppDelegate].customEvents == nil)
                {
                    [TSPAppDelegate sharedAppDelegate].customEvents = [[NSMutableArray alloc] init];
                }
                
                [[TSPAppDelegate sharedAppDelegate].customEvents addObject:eventObject];
                [[TSPAppDelegate sharedAppDelegate] saveCustomLayoutToDefaults];
            }
            
            
            // Save Record
            NSError *error = nil;
            
            if ([self.managedObjectContext save:&error])
            {
                // Dismiss View Controller
                
                NSLog(@"!!COLORS = %@",[record valueForKey:@"layoutbackgroundcolor"]);
                [TSPAppDelegate sharedAppDelegate].isSwitchOverlay = NO;
                [defaults removeObjectForKey:@"numberofphotos"];
                
                [self dismissViewControllerAnimated:YES completion:nil];
                
            }
            else
            {
                if (error)
                {
                    NSLog(@"Unable to save record.");
                    NSLog(@"%@, %@", error, error.localizedDescription);
                }
                
                FCAlertView *alert = [[FCAlertView alloc] init];
                
                [alert showAlertWithTitle:@"Warning"
                             withSubtitle:@"Your event could not be saved"
                          withCustomImage:nil
                      withDoneButtonTitle:nil
                               andButtons:nil];
                
                
            }
            
        }
        else
        {
            FCAlertView *alert = [[FCAlertView alloc] init];
            
            [alert showAlertWithTitle:@"Warning"
                         withSubtitle:@"Your event could not be saved"
                      withCustomImage:nil
                  withDoneButtonTitle:nil
                           andButtons:nil];
        }
        
        
        [self createAlbum];
        
        FCAlertView *alert = [[FCAlertView alloc] init];
        alert.darkTheme = YES;
        [alert makeAlertTypeSuccess];
        alert.detachButtons = YES;
        [alert setAlertSoundWithFileName:@"ElevatorDing.mp3"];
        alert.blurBackground = YES;
        [alert showAlertWithTitle:@"SUCCESSFULLY SAVED"
                     withSubtitle:@"Your event has been created and is ready to use at your event."
                  withCustomImage:nil
              withDoneButtonTitle:nil
                       andButtons:nil];
        
        
         [defaults removeObjectForKey:@"numberofphotos"];
        
        
    }
    
    
    
}





- (void)createAlbum
{
    [[PHPhotoLibrary sharedPhotoLibrary] createNewAlbumCalled:txtFldCNames.text];
     [[PHPhotoLibrary sharedPhotoLibrary] createNewAlbumCalled:[NSString stringWithFormat:@"%@_Layout", txtFldCNames.text]];
}







- (IBAction)LoadPhotoBooth {
    
    [txtFldCNames resignFirstResponder];
    [txtFldEDate resignFirstResponder];
    
    
    
    if (switchRear.isOn)
    {
        switchTwitter.enabled=NO;
        switchTwitter.on=NO;
        switchFacebook.enabled=NO;
        switchFacebook.on=NO;
        burstmode.enabled=NO;
        burstmode.on=NO;
        switchGreen.enabled=NO;
        switchGif.enabled=NO;
        switchSepia.enabled=NO;
        switchVideo.enabled=NO;
        switchVintage.enabled=NO;
        switchBlackWhite.enabled=NO;
        switchGreen.on=NO;
        switchGif.on=NO;
        switchSepia.on=NO;
        switchVideo.on=NO;
        switchVintage.on=NO;
        switchBlackWhite.on=NO;
        switchMessage.on=NO;
    }
    
    
    
    //Redraw custom layout view
    //    if ([AppDelegate sharedAppDelegate].customLayoutArray.count > 0)
    //    {
    //        if ([AppDelegate sharedAppDelegate].isSmallCustomLayout)
    //        {
    //            [AppDelegate sharedAppDelegate].customLayoutView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 476, 715)];
    //
    //            //Add background imageView
    //            UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 476, 715)];
    //            [backgroundImageView setTag:1];
    //            [[AppDelegate sharedAppDelegate].customLayoutView addSubview:backgroundImageView];
    //
    //            for (int i = 0; i < [AppDelegate sharedAppDelegate].customLayoutArray.count; i++)
    //            {
    //                Rectangle *rectangle = [[AppDelegate sharedAppDelegate].customLayoutArray objectAtIndex:i];
    //                rectangle.frame = rectangle.frame;
    //
    //                CGPoint centerPoint = rectangle.center;
    //
    //                CGAffineTransform rotate = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(rectangle.rotateValue));
    //                CGAffineTransform scale = CGAffineTransformMakeScale(rectangle.scaleValue, rectangle.scaleValue);
    //                CGAffineTransform scaleAndRotate = CGAffineTransformConcat(rotate, scale);
    //                rectangle.transform = scaleAndRotate;
    //
    //                rectangle.center = centerPoint;
    //
    //                [[AppDelegate sharedAppDelegate].customLayoutView addSubview:rectangle];
    //            }
    //        }
    //        else if ([AppDelegate sharedAppDelegate].isLargeCustomLayout)
    //        {
    //            [AppDelegate sharedAppDelegate].customLayoutView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 510, 715)];
    //
    //            //Add background imageView
    //            UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 510, 715)];
    //            [backgroundImageView setTag:1];
    //            [[AppDelegate sharedAppDelegate].customLayoutView addSubview:backgroundImageView];
    //
    //            for (int i = 0; i < [AppDelegate sharedAppDelegate].customLayoutArray.count; i++)
    //            {
    //                Rectangle *rectangle = [[AppDelegate sharedAppDelegate].customLayoutArray objectAtIndex:i];
    //                rectangle.frame = rectangle.frame;
    //
    //                CGPoint centerPoint = rectangle.center;
    //
    //                CGAffineTransform rotate = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(rectangle.rotateValue));
    //                CGAffineTransform scale = CGAffineTransformMakeScale(rectangle.scaleValue, rectangle.scaleValue);
    //                CGAffineTransform scaleAndRotate = CGAffineTransformConcat(rotate, scale);
    //                rectangle.transform = scaleAndRotate;
    //
    //                rectangle.center = centerPoint;
    //
    //                [[AppDelegate sharedAppDelegate].customLayoutView addSubview:rectangle];
    //            }
    //        }
    //
    //        else if ([AppDelegate sharedAppDelegate].isSmallCustomLayouth)
    //        {
    //            [AppDelegate sharedAppDelegate].customLayoutView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 715, 476)];
    //
    //            //Add background imageView
    //            UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 715, 476)];
    //            [backgroundImageView setTag:1];
    //            [[AppDelegate sharedAppDelegate].customLayoutView addSubview:backgroundImageView];
    //
    //            for (int i = 0; i < [AppDelegate sharedAppDelegate].customLayoutArray.count; i++)
    //            {
    //                Rectangle *rectangle = [[AppDelegate sharedAppDelegate].customLayoutArray objectAtIndex:i];
    //                rectangle.frame = rectangle.frame;
    //
    //                CGPoint centerPoint = rectangle.center;
    //
    //                CGAffineTransform rotate = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(rectangle.rotateValue));
    //                CGAffineTransform scale = CGAffineTransformMakeScale(rectangle.scaleValue, rectangle.scaleValue);
    //                CGAffineTransform scaleAndRotate = CGAffineTransformConcat(rotate, scale);
    //                rectangle.transform = scaleAndRotate;
    //
    //                rectangle.center = centerPoint;
    //
    //                [[AppDelegate sharedAppDelegate].customLayoutView addSubview:rectangle];
    //            }
    //        }
    //        else
    //        {
    //            [AppDelegate sharedAppDelegate].customLayoutView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 715, 510)];
    //
    //            //Add background imageView
    //            UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 715, 510)];
    //            [backgroundImageView setTag:1];
    //            [[AppDelegate sharedAppDelegate].customLayoutView addSubview:backgroundImageView];
    //
    //            for (int i = 0; i < [AppDelegate sharedAppDelegate].customLayoutArray.count; i++)
    //            {
    //                Rectangle *rectangle = [[AppDelegate sharedAppDelegate].customLayoutArray objectAtIndex:i];
    //                rectangle.frame = rectangle.frame;
    //
    //                CGPoint centerPoint = rectangle.center;
    //
    //                CGAffineTransform rotate = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(rectangle.rotateValue));
    //                CGAffineTransform scale = CGAffineTransformMakeScale(rectangle.scaleValue, rectangle.scaleValue);
    //                CGAffineTransform scaleAndRotate = CGAffineTransformConcat(rotate, scale);
    //                rectangle.transform = scaleAndRotate;
    //
    //                rectangle.center = centerPoint;
    //
    //                [[AppDelegate sharedAppDelegate].customLayoutView addSubview:rectangle];
    //            }
    //        }
    //
    //    }
    
    //    [self refreshTheme:[AppDelegate sharedAppDelegate].shrLayout];
    //    [pkView reloadAllComponents];
    
    
    
    
}




#pragma mark - CNPPopupController Delegate
-(IBAction)cancelcustom:(id)sender

{
    
    NSUserDefaults *defaults1 = [NSUserDefaults standardUserDefaults];
    NSString* layout = [defaults1 objectForKey:@"hideview"];
    if ([layout isEqualToString: @"yes"])
    {
        layoutselected.hidden=YES;
        NSLog(@"LAYOUT HIDDEN");
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
    
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    TSPAddToDoViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"PhotoBoothAdmin"];
//    [vc setManagedObjectContext:self.managedObjectContext];
//    
//    [vc setModalPresentationStyle:UIModalPresentationFullScreen];
//    [self presentViewController:vc animated:NO completion:nil];
}


-(IBAction)cancelcustom1:(id)sender

{
    
    NSUserDefaults *defaults1 = [NSUserDefaults standardUserDefaults];
    NSString* layout = [defaults1 objectForKey:@"hideview"];
    if ([layout isEqualToString: @"yes"])
    {
        layoutselected.hidden=YES;
        NSLog(@"LAYOUT HIDDEN");
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
    
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    TSPAddToDoViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"AnimAdmin"];
//    [vc setManagedObjectContext:self.managedObjectContext];
//    
//    [vc setModalPresentationStyle:UIModalPresentationFullScreen];
//    [self presentViewController:vc animated:NO completion:nil];
}


- (void)popupController:(CNPPopupController *)controller didDismissWithButtonTitle:(NSString *)title {
    NSLog(@"Dismissed with button title: %@", title);
    
    
}



- (void)popupControllerDidPresent:(CNPPopupController *)controller {
    NSLog(@"Popup controller presented.");
    
}



//INSTRUCTIONAL ALERTS//

- (IBAction) onClickCustomLayout1:(id)sender {
     [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"hideview"];
    booth.on = NO;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    TSPAddToDoViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"CustomDesign1"];
    [vc setManagedObjectContext:self.managedObjectContext];
    
    [vc setModalPresentationStyle:UIModalPresentationFullScreen];
    [self presentViewController:vc animated:NO completion:nil];
    
    
    if (mIsLoaded != 0)
    {
        
        if (self.fourBySixRectangleArrayh)
        {
            for (Rectangle *rectangle in self.fourBySixRectangleArray)
            {
                [rectangle removeFromSuperview];
            }
            [self.fourBySixRectangleArray removeAllObjects];
            self.fourBySixRectangleArray = [NSMutableArray array];
        }
        else if (self.fiveBySevenRectangleArray)
        {
            for (Rectangle *rectangle in self.fiveBySevenRectangleArray)
            {
                [rectangle removeFromSuperview];
            }
            [self.fiveBySevenRectangleArray removeAllObjects];
            self.fiveBySevenRectangleArray = [NSMutableArray array];
        }
        
        else if ( self.fourBySixRectangleArrayh)
        {
            for (Rectangle *rectangle in self.fourBySixRectangleArrayh)
            {
                [rectangle removeFromSuperview];
            }
            [self.fourBySixRectangleArrayh removeAllObjects];
            self.fourBySixRectangleArrayh = [NSMutableArray array];
        }
        
        else
        {
            for (Rectangle *rectangle in self.fiveBySevenRectangleArrayh)
            {
                [rectangle removeFromSuperview];
            }
            [self.fiveBySevenRectangleArrayh removeAllObjects];
            self.fiveBySevenRectangleArrayh = [NSMutableArray array];
        }
        
        
        //  for (int i = 0; i < [AppDelegate sharedAppDelegate].customLayoutArray.count; i++)
        //        {
        //            Rectangle *rectangle = [[AppDelegate sharedAppDelegate].customLayoutArray objectAtIndex:i];
        //            [self addGestureRecognizersToView:rectangle];
        //
        //
        //            if (self.fourBySixRectangleArray)
        //            {
        //                //Set selected segment control
        //                self.segmentedControl.selectedSegmentIndex = 0;
        //                [self changeSegmentedControl];
        //
        //                [self.fourBySixRectangleArray addObject:rectangle];
        //                [fourbysix addSubview:rectangle];
        //                [fourBySixBackgroundImageView setImage:[UIImage imageWithData:[AppDelegate sharedAppDelegate].customLayoutBackground]];
        //            }
        //            else if (self.fiveBySevenRectangleArray )
        //            {
        //                //Set selected segment control
        //                self.segmentedControl.selectedSegmentIndex = 1;
        //                [self changeSegmentedControl];
        //
        //                [self.fiveBySevenRectangleArray addObject:rectangle];
        //                [fivebyseven addSubview:rectangle];
        //                [fiveBySevenBackgroundImageView setImage:[UIImage imageWithData:[AppDelegate sharedAppDelegate].customLayoutBackground]];
        //            }
        //
        //            else if (self.fourBySixRectangleArrayh )
        //            {
        //                //Set selected segment control
        //                self.segmentedControl.selectedSegmentIndex = 2;
        //                [self changeSegmentedControl];
        //
        //                [self.fourBySixRectangleArrayh addObject:rectangle];
        //                [fourbysixh addSubview:rectangle];
        //                [fourBySixBackgroundImageViewh setImage:[UIImage imageWithData:[AppDelegate sharedAppDelegate].customLayoutBackground]];
        //            }
        //            else
        //            {
        //                //Set selected segment control
        //                self.segmentedControl.selectedSegmentIndex = 3;
        //                [self changeSegmentedControl];
        //
        //                [self.fiveBySevenRectangleArrayh addObject:rectangle];
        //                [fivebysevenh addSubview:rectangle];
        //                [fiveBySevenBackgroundImageViewh setImage:[UIImage imageWithData:[AppDelegate sharedAppDelegate].customLayoutBackground]];
        //            }
        //        }
        //    }
        
        
        //    if (mIsLoaded != 0)
        //    {
        //        EventDB *event = [[AppDelegate sharedAppDelegate].arrEventsPb objectAtIndex:loadedEventIndex];
        //
        //        NSMutableArray *archiveArray = [NSMutableArray arrayWithCapacity:[AppDelegate sharedAppDelegate].customLayoutArray.count];
        //        for (Rectangle *rectangle in [AppDelegate sharedAppDelegate].customLayoutArray) {
        //            NSData *rectangleData = [NSKeyedArchiver archivedDataWithRootObject:rectangle];
        //            [archiveArray addObject:rectangleData];
        //        }
        //        event.arrCustomLayout = archiveArray;
        //        event.nCustomLayoutCount = [NSNumber numberWithInteger:[AppDelegate sharedAppDelegate].customLayoutCount];
        //        event.nIsSmallLayout = [NSNumber numberWithBool:[AppDelegate sharedAppDelegate].isSmallCustomLayout];
        //        event.customLayoutBackground = [AppDelegate sharedAppDelegate].customLayoutBackground;
        //
        //        [[AppDelegate sharedAppDelegate].arrEventsPb replaceObjectAtIndex:loadedEventIndex withObject:event];
        //        [[AppDelegate sharedAppDelegate] saveDatabase];
        //    }
        
        
        
    }
    
}



-(void)viewWillAppear:(BOOL)animated
{
    NSUserDefaults *defaults1 = [NSUserDefaults standardUserDefaults];
    NSString* layout = [defaults1 objectForKey:@"hideview"];
    if ([layout isEqualToString: @"yes"])
    {
        layoutselected.hidden=YES;
        NSLog(@"LAYOUT HIDDEN");
    }
}


- (IBAction) onClickCustomLayout:(id)sender {
    
     [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"hideview"];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *hideview = @"yes";
    [defaults setObject:hideview forKey:@"hideview"];
    [defaults synchronize];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    TSPAddToDoViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"CustomDesign"];
    [vc setManagedObjectContext:self.managedObjectContext];
    
    [vc setModalPresentationStyle:UIModalPresentationFullScreen];
    [self presentViewController:vc animated:NO completion:nil];
    
   
    if (mIsLoaded != 0)
    {
        if (share.isOn)
        {
            if (splitlayout.isOn) {
                
                [splitlayout setOn:NO];
                
                guide.hidden = YES;
                guide1.hidden = YES;
                guide2.hidden = YES;
                guide3.hidden = YES;
            }
        }
       
        else
        {
            splitlayout.hidden=YES;
        }
        
        if (self.fourBySixRectangleArrayh)
        {
            for (Rectangle *rectangle in self.fourBySixRectangleArray)
            {
                [rectangle removeFromSuperview];
            }
            [self.fourBySixRectangleArray removeAllObjects];
            self.fourBySixRectangleArray = [NSMutableArray array];
        }
        else if (self.fiveBySevenRectangleArray)
        {
            for (Rectangle *rectangle in self.fiveBySevenRectangleArray)
            {
                [rectangle removeFromSuperview];
            }
            [self.fiveBySevenRectangleArray removeAllObjects];
            self.fiveBySevenRectangleArray = [NSMutableArray array];
        }
        
        else if ( self.fourBySixRectangleArrayh)
        {
            for (Rectangle *rectangle in self.fourBySixRectangleArrayh)
            {
                [rectangle removeFromSuperview];
            }
            [self.fourBySixRectangleArrayh removeAllObjects];
            self.fourBySixRectangleArrayh = [NSMutableArray array];
        }
        
        else
        {
            for (Rectangle *rectangle in self.fiveBySevenRectangleArrayh)
            {
                [rectangle removeFromSuperview];
            }
            [self.fiveBySevenRectangleArrayh removeAllObjects];
            self.fiveBySevenRectangleArrayh = [NSMutableArray array];
        }
        
        

    
    
    }
    
}

-(IBAction) segmentedControlIndexChanged
{
    [self changeSegmentedControl];
}

- (void)changeSegmentedControl
{
    switch (self.segmentedControl.selectedSegmentIndex) {
        case 0:
            [fourbysix addGestureRecognizer:doubleTap];
            [self.view bringSubviewToFront:fourbysix];
            fourBySixScrollView.hidden=NO;
            fiveBySevenScrollView.hidden=YES;
            fourBySixScrollViewh.hidden=YES;
            fiveBySevenScrollViewh.hidden=YES;
            break;
        case 1:
            [fivebyseven addGestureRecognizer:doubleTap1];
            [self.view bringSubviewToFront:fivebyseven];
            fourBySixScrollView.hidden=YES;
            fiveBySevenScrollView.hidden=NO;
            fourBySixScrollViewh.hidden=YES;
            fiveBySevenScrollViewh.hidden=YES;
            break;
        case 2:
            [fourbysixh addGestureRecognizer:doubleTap2];
            [self.view bringSubviewToFront:fourbysixh];
            fourBySixScrollViewh.hidden=NO;
            fiveBySevenScrollView.hidden=YES;
            fourBySixScrollView.hidden=YES;
            fiveBySevenScrollViewh.hidden=YES;
            break;
        case 3:
            [fivebysevenh addGestureRecognizer:doubleTap3];
            [self.view bringSubviewToFront:fivebysevenh];
            fourBySixScrollViewh.hidden=YES;
            fiveBySevenScrollViewh.hidden=NO;
            fiveBySevenScrollView.hidden=YES;
            fourBySixScrollView.hidden=YES;
            break;
        default:
            break;
    }
}


- (IBAction) onClickLayoutDone:(id)sender
{

    
    if ([TSPAppDelegate sharedAppDelegate].imgCustomBackground == nil)
    {
        [ISMessages showCardAlertWithTitle:@"SELECT A BACKGROUND"
                                   message:@"You need to choose a background for the custom layout you created. Press on the Layout Background button and choose one. Make sure the overlay slider is on if you created an overlay design."
                                 iconImage:nil
                                  duration:3.f
                               hideOnSwipe:YES
                                 hideOnTap:YES
                                 alertType:ISAlertTypeError
                             alertPosition:0];

    }
    else
    {
        if (self.segmentedControl.selectedSegmentIndex == 0)
        {

            if (self.fourBySixRectangleArray.count <= 0)
            {
                [ISMessages showCardAlertWithTitle:@"NO PHOTOS ADDED"
                                           message:@"You need to add a photo"
                                         iconImage:nil
                                          duration:3.f
                                       hideOnSwipe:YES
                                         hideOnTap:YES
                                         alertType:ISAlertTypeError
                                     alertPosition:0];
            }
            
            [TSPAppDelegate sharedAppDelegate].isSmallCustomLayout = YES;
            [TSPAppDelegate sharedAppDelegate].isSmallCustomLayouth = NO;
            [TSPAppDelegate sharedAppDelegate].isLargeCustomLayouth = NO;
            [TSPAppDelegate sharedAppDelegate].isLargeCustomLayout = NO;
            [TSPAppDelegate sharedAppDelegate].customLayoutView = fourbysix;

            if (splitlayout.isOn)
            {
                [TSPAppDelegate sharedAppDelegate].customLayoutCount = self.fourBySixRectangleArray.count / 2;
            }
            else
            {
                [TSPAppDelegate sharedAppDelegate].customLayoutCount = self.fourBySixRectangleArray.count;
            }

            [TSPAppDelegate sharedAppDelegate].customLayoutArray = self.fourBySixRectangleArray;
        }
        else if (self.segmentedControl.selectedSegmentIndex == 1)
        {
            if (self.fiveBySevenRectangleArray.count <= 0)
            {
                [ISMessages showCardAlertWithTitle:@"NO PHOTOS ADDED"
                                           message:@"You need to add a photo"
                                         iconImage:nil
                                          duration:3.f
                                       hideOnSwipe:YES
                                         hideOnTap:YES
                                         alertType:ISAlertTypeError
                                     alertPosition:0];
            }

            [TSPAppDelegate sharedAppDelegate].isSmallCustomLayout = NO;
            [TSPAppDelegate sharedAppDelegate].isSmallCustomLayouth = NO;
            [TSPAppDelegate sharedAppDelegate].isLargeCustomLayouth = NO;
            [TSPAppDelegate sharedAppDelegate].isLargeCustomLayout = YES;
            [TSPAppDelegate sharedAppDelegate].customLayoutView = fivebyseven;
            
            if (splitlayout.isOn)
            {
                [TSPAppDelegate sharedAppDelegate].customLayoutCount = self.fiveBySevenRectangleArray.count / 2;
            }
            else
            {
                [TSPAppDelegate sharedAppDelegate].customLayoutCount = self.fiveBySevenRectangleArray.count;
            }

            [TSPAppDelegate sharedAppDelegate].customLayoutArray = self.fiveBySevenRectangleArray;
        }
        else if (self.segmentedControl.selectedSegmentIndex == 2)
        {
            if (self.fourBySixRectangleArrayh.count <= 0)
            {
                [ISMessages showCardAlertWithTitle:@"NO PHOTOS ADDED"
                                           message:@"You need to add a photo"
                                         iconImage:nil
                                          duration:3.f
                                       hideOnSwipe:YES
                                         hideOnTap:YES
                                         alertType:ISAlertTypeError
                                     alertPosition:0];
            }
            
            [TSPAppDelegate sharedAppDelegate].isSmallCustomLayout = NO;
            [TSPAppDelegate sharedAppDelegate].isSmallCustomLayouth = YES;
            [TSPAppDelegate sharedAppDelegate].isLargeCustomLayouth = NO;
            [TSPAppDelegate sharedAppDelegate].isLargeCustomLayout = NO;
            [TSPAppDelegate sharedAppDelegate].customLayoutView = fourbysixh;
         
            if (splitlayout.isOn)
            {
                [TSPAppDelegate sharedAppDelegate].customLayoutCount = self.fourBySixRectangleArrayh.count / 2;
            }
            else
            {
                [TSPAppDelegate sharedAppDelegate].customLayoutCount = self.fourBySixRectangleArrayh.count;
            }

            [TSPAppDelegate sharedAppDelegate].customLayoutArray = self.fourBySixRectangleArrayh;
        }
        else if (self.segmentedControl.selectedSegmentIndex == 3)
        {
            if (self.fiveBySevenRectangleArrayh.count <= 0)
            {
                [ISMessages showCardAlertWithTitle:@"NO PHOTOS ADDED"
                                           message:@"You need to add a photo"
                                         iconImage:nil
                                          duration:3.f
                                       hideOnSwipe:YES
                                         hideOnTap:YES
                                         alertType:ISAlertTypeError
                                     alertPosition:0];
            }

            [TSPAppDelegate sharedAppDelegate].isSmallCustomLayout = NO;
            [TSPAppDelegate sharedAppDelegate].isSmallCustomLayouth = NO;
            [TSPAppDelegate sharedAppDelegate].isLargeCustomLayouth = YES;
            [TSPAppDelegate sharedAppDelegate].isLargeCustomLayout = NO;
            [TSPAppDelegate sharedAppDelegate].customLayoutView = fivebysevenh;

            if (splitlayout.isOn)
            {
                [TSPAppDelegate sharedAppDelegate].customLayoutCount = self.fiveBySevenRectangleArrayh.count / 2;
            }
            else
            {
                [TSPAppDelegate sharedAppDelegate].customLayoutCount = self.fiveBySevenRectangleArrayh.count;
            }


            [TSPAppDelegate sharedAppDelegate].customLayoutArray = self.fiveBySevenRectangleArrayh;
        }
        
        if (splitlayout.isOn)
        {
            [TSPAppDelegate sharedAppDelegate].isSplipLayout = YES;
        }
        else
        {
            [TSPAppDelegate sharedAppDelegate].isSplipLayout = NO;
        }

        [[TSPAppDelegate sharedAppDelegate] saveCustomLayout];

        //Save Custom Layout With NUmber Photo and Theme Code
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *numberofphotos = [NSString stringWithFormat:@"%ld", (long)[TSPAppDelegate sharedAppDelegate].customLayoutCount];
        _numberOfPhotosLayout = numberofphotos;
        [defaults setObject:_numberOfPhotosLayout forKey:@"numberofphotos"];
        NSString *customTheme = @"CustomTheme";
       [defaults setObject:customTheme forKey:@"shrTheme"];
        [defaults synchronize];
       
        NSUserDefaults *defaults1 = [NSUserDefaults standardUserDefaults];
        NSString* layout = [defaults1 objectForKey:@"hideview"];
        if ([layout isEqualToString: @"yes"])
        {
            layoutselected.hidden=YES;
            NSLog(@"LAYOUT HIDDEN");
        }
        
        [self dismissViewControllerAnimated:YES completion:NULL];
    }




}


- (NSString*) saveImageToBundle:(UIImage*)image {
    
    NSData* imageData = UIImagePNGRepresentation(image);
    NSFileManager* fileManager = [NSFileManager defaultManager];
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString* directory = [paths objectAtIndex:0];
    BOOL isCreated = [fileManager createDirectoryAtPath:[NSString stringWithFormat:@"%@/%@", directory, @"PhotoboothAlbum"] withIntermediateDirectories:NO attributes:nil error:NULL];
    if (isCreated) {
        NSLog(@"AlbumDirectory Created!");
    } else {
        NSLog(@"AlbumDirectory Not Created!");
    }
    NSString* dir = [NSString stringWithFormat:@"%@/PhotoboothAlbum/", directory];
    static int id = 0;
    NSString* imgName = [NSString stringWithFormat:@"photolab_%d", ++id];
    NSString* fullPath = [dir stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", imgName]];
    NSLog(@"%@", fullPath);
    while ([fileManager fileExistsAtPath:fullPath]) {
        NSLog(@"exist");
        imgName = [NSString stringWithFormat:@"photolab_%d", ++id];
        fullPath = [dir stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", imgName]];
    }
    
    [fileManager createFileAtPath:fullPath contents:imageData attributes:nil];
    if ([fileManager fileExistsAtPath:fullPath]) {
        NSLog(@"exist");
    }
    return fullPath;
}



- (IBAction) onClickScreensDone:(id)sender {
    
    
    
}







- (BOOL)prefersStatusBarHidden
{
    return YES;
}





- (IBAction) onClickChooseCustomLayoutBackground:(id)sender {
    NSLog(@"Custom Layout Button Pressed");
    
    if (!imageMediaPickerCustomLayoutBackground) {
        imageMediaPickerCustomLayoutBackground = [QBImagePickerController new];
        imageMediaPickerCustomLayoutBackground.mediaType = QBImagePickerMediaTypeImage;
        imageMediaPickerCustomLayoutBackground.allowsMultipleSelection = NO;
        imageMediaPickerCustomLayoutBackground.showsNumberOfSelectedAssets = YES;
        imageMediaPickerCustomLayoutBackground.maximumNumberOfSelection = 1;
        imageMediaPickerCustomLayoutBackground.delegate = self;
    }
    [self presentViewController:imageMediaPickerCustomLayoutBackground animated:YES completion:nil];
    
    if (switchOverlay1.isOn)
    {
        NSLog(@"Switch Overlay Custom On");
        
    }
    else{
        NSLog(@"Switch Overlay Custom Off");
    }
    
}




- (IBAction)addPhoto:(UIButton *)sender {
    
    if (self.modeWorking == ANIMATION_GIF)
    {
        _numberAddPhotoAnimateGif ++;
        if (_numberAddPhotoAnimateGif > 1)
        {
            _numberAddPhotoAnimateGif = 1;
            
            FCAlertView *alert = [[FCAlertView alloc] init];
            alert.darkTheme = YES;
            [alert makeAlertTypeCaution];
            alert.detachButtons = YES;
            [alert setAlertSoundWithFileName:@"error.mp3"];
            alert.blurBackground = YES;
            [alert showAlertWithTitle:@"ONLY ONE PHOTO IS PERMITTED"
                         withSubtitle:@"You may only add one photo to an animated gif layout design."
                      withCustomImage:nil
                  withDoneButtonTitle:nil
                           andButtons:nil];
            
            return;
        }
        
    }
    
    if (splitlayout.isOn)
    {
        if (self.segmentedControl.selectedSegmentIndex==0)
        {
            if (UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation])) // Landscap
            {
                Rectangle *rectangle = [[Rectangle alloc]initWithFrame:CGRectMake(0, 0, 200, 150)];
                rectangle.scaleValue = 1;
                rectangle.rotateValue = 0;
                
                Rectangle *rectangle1 = [[Rectangle alloc]initWithFrame:CGRectMake(250, 0, 200, 150)];
                rectangle1.scaleValue = 1;
                rectangle1.rotateValue = 0;
                CGFloat hue = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
                CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
                CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
                [rectangle setBackgroundColor:[UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1]];
                [rectangle1 setBackgroundColor:[UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1]];
                [self addGestureRecognizersToView:rectangle];
                [self addGestureRecognizersToView:rectangle1];
                [fourbysix addSubview:rectangle1];
                [fourbysix addSubview:rectangle];
                guide.hidden=NO;
                guide1.hidden=YES;
                guide2.hidden=YES;
                guide3.hidden=YES;
                [self.fourBySixRectangleArray addObject:rectangle];
                [self.fourBySixRectangleArray addObject:rectangle1];
            }
            
            else
            {
                Rectangle *rectangle = [[Rectangle alloc]initWithFrame:CGRectMake(0, 0, 150, 200)];
                rectangle.scaleValue = 1;
                rectangle.rotateValue = 0;
                
                Rectangle *rectangle1 = [[Rectangle alloc]initWithFrame:CGRectMake(250, 0, 150, 200)];
                rectangle1.scaleValue = 1;
                rectangle1.rotateValue = 0;
                CGFloat hue = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
                CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
                CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
                [rectangle setBackgroundColor:[UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1]];
                [rectangle1 setBackgroundColor:[UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1]];
                [self addGestureRecognizersToView:rectangle];
                [self addGestureRecognizersToView:rectangle1];
                [fourbysix addSubview:rectangle1];
                [fourbysix addSubview:rectangle];
                guide.hidden=NO;
                guide1.hidden=YES;
                guide2.hidden=YES;
                guide3.hidden=YES;
                [self.fourBySixRectangleArray addObject:rectangle];
                [self.fourBySixRectangleArray addObject:rectangle1];
            }
            
        }
        if (self.segmentedControl.selectedSegmentIndex==1)
        {
            
            if (UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation])) // Landscap
            {
                Rectangle *rectangle = [[Rectangle alloc]initWithFrame:CGRectMake(0, 0, 200, 150)];
                rectangle.scaleValue = 1;
                rectangle.rotateValue = 0;
                
                Rectangle *rectangle1 = [[Rectangle alloc]initWithFrame:CGRectMake(250, 0, 200, 150)];
                rectangle1.scaleValue = 1;
                rectangle1.rotateValue = 0;
                
                CGFloat hue = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
                CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
                CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
                [rectangle setBackgroundColor:[UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1]];
                [rectangle1 setBackgroundColor:[UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1]];
                [self addGestureRecognizersToView:rectangle];
                [self addGestureRecognizersToView:rectangle1];
                [fivebyseven addSubview:rectangle1];
                [fivebyseven addSubview:rectangle];
                guide.hidden=YES;
                guide1.hidden=NO;
                guide2.hidden=YES;
                guide3.hidden=YES;
                [self.fiveBySevenRectangleArray addObject:rectangle];
                [self.fiveBySevenRectangleArray addObject:rectangle1];
            }
            
            else{
                Rectangle *rectangle = [[Rectangle alloc]initWithFrame:CGRectMake(0, 0, 150, 200)];
                rectangle.scaleValue = 1;
                rectangle.rotateValue = 0;
                
                Rectangle *rectangle1 = [[Rectangle alloc]initWithFrame:CGRectMake(250, 0, 150, 200)];
                rectangle1.scaleValue = 1;
                rectangle1.rotateValue = 0;
                
                CGFloat hue = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
                CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
                CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
                [rectangle setBackgroundColor:[UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1]];
                [rectangle1 setBackgroundColor:[UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1]];
                [self addGestureRecognizersToView:rectangle];
                [self addGestureRecognizersToView:rectangle1];
                [fivebyseven addSubview:rectangle1];
                [fivebyseven addSubview:rectangle];
                guide.hidden=YES;
                guide1.hidden=NO;
                guide2.hidden=YES;
                guide3.hidden=YES;
                [self.fiveBySevenRectangleArray addObject:rectangle];
                [self.fiveBySevenRectangleArray addObject:rectangle1];
            }
            
        }
        if (self.segmentedControl.selectedSegmentIndex==2)
        {
            if (UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation])) // Landscap
            {
                Rectangle *rectangle = [[Rectangle alloc]initWithFrame:CGRectMake(0, 0, 200, 150)];
                rectangle.scaleValue = 1;
                rectangle.rotateValue = 0;
                
                Rectangle *rectangle1 = [[Rectangle alloc]initWithFrame:CGRectMake(0, 250, 200, 150)];
                rectangle1.scaleValue = 1;
                rectangle1.rotateValue = 0;
                
                CGFloat hue = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
                CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
                CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
                [rectangle setBackgroundColor:[UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1]];
                [rectangle1 setBackgroundColor:[UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1]];
                [self addGestureRecognizersToView:rectangle];
                [self addGestureRecognizersToView:rectangle1];
                [fourbysixh addSubview:rectangle1];
                [fourbysixh addSubview:rectangle];
                guide2.hidden=NO;
                guide3.hidden=YES;
                guide1.hidden=YES;
                guide.hidden=YES;
                [self.fourBySixRectangleArrayh addObject:rectangle];
                [self.fourBySixRectangleArrayh addObject:rectangle1];
            }
            
            else
            {
                Rectangle *rectangle = [[Rectangle alloc]initWithFrame:CGRectMake(0, 0, 150, 200)];
                rectangle.scaleValue = 1;
                rectangle.rotateValue = 0;
                
                Rectangle *rectangle1 = [[Rectangle alloc]initWithFrame:CGRectMake(0, 250, 150, 200)];
                rectangle1.scaleValue = 1;
                rectangle1.rotateValue = 0;
                
                CGFloat hue = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
                CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
                CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
                [rectangle setBackgroundColor:[UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1]];
                [rectangle1 setBackgroundColor:[UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1]];
                [self addGestureRecognizersToView:rectangle];
                [self addGestureRecognizersToView:rectangle1];
                [fourbysixh addSubview:rectangle1];
                [fourbysixh addSubview:rectangle];
                guide2.hidden=NO;
                guide3.hidden=YES;
                guide1.hidden=YES;
                guide.hidden=YES;
                [self.fourBySixRectangleArrayh addObject:rectangle];
                [self.fourBySixRectangleArrayh addObject:rectangle1];
            }
            
        }
        if (self.segmentedControl.selectedSegmentIndex==3)
        {
            if (UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation])) // Landscap
            {
                Rectangle *rectangle = [[Rectangle alloc]initWithFrame:CGRectMake(0, 0, 200, 150)];
                rectangle.scaleValue = 1;
                rectangle.rotateValue = 0;
                
                Rectangle *rectangle1 = [[Rectangle alloc]initWithFrame:CGRectMake(0, 250, 200, 150)];
                rectangle1.scaleValue = 1;
                rectangle1.rotateValue = 0;
                
                CGFloat hue = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
                CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
                CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
                [rectangle setBackgroundColor:[UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1]];
                [rectangle1 setBackgroundColor:[UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1]];
                [self addGestureRecognizersToView:rectangle];
                [self addGestureRecognizersToView:rectangle1];
                [fivebysevenh addSubview:rectangle1];
                [fivebysevenh addSubview:rectangle];
                guide2.hidden=YES;
                guide3.hidden=NO;
                guide1.hidden=YES;
                guide.hidden=YES;
                [self.fiveBySevenRectangleArrayh addObject:rectangle];
                [self.fiveBySevenRectangleArrayh addObject:rectangle1];
            }
            
            else{
                Rectangle *rectangle = [[Rectangle alloc]initWithFrame:CGRectMake(0, 0, 150, 200)];
                rectangle.scaleValue = 1;
                rectangle.rotateValue = 0;
                
                Rectangle *rectangle1 = [[Rectangle alloc]initWithFrame:CGRectMake(0, 250, 150, 200)];
                rectangle1.scaleValue = 1;
                rectangle1.rotateValue = 0;
                
                CGFloat hue = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
                CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
                CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
                [rectangle setBackgroundColor:[UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1]];
                [rectangle1 setBackgroundColor:[UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1]];
                [self addGestureRecognizersToView:rectangle];
                [self addGestureRecognizersToView:rectangle1];
                [fivebysevenh addSubview:rectangle1];
                [fivebysevenh addSubview:rectangle];
                guide2.hidden=YES;
                guide3.hidden=NO;
                guide1.hidden=YES;
                guide.hidden=YES;
                [self.fiveBySevenRectangleArrayh addObject:rectangle];
                [self.fiveBySevenRectangleArrayh addObject:rectangle1];
            }
        }
    }
    
    else
    {
        
        if (self.segmentedControl.selectedSegmentIndex==0)
        {
            if (UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation])) // Landscap
            {
                Rectangle *rectangle = [[Rectangle alloc]initWithFrame:CGRectMake(0, 0, 200, 150)];
                rectangle.scaleValue = 1;
                rectangle.rotateValue = 0;
                guide.hidden=YES;
                guide1.hidden=YES;
                CGFloat hue = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
                CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
                CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
                [rectangle setBackgroundColor:[UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1]];
                [self addGestureRecognizersToView:rectangle];
                [fourbysix addSubview:rectangle];
                [self.fourBySixRectangleArray addObject:rectangle];
                guide2.hidden=YES;
                guide3.hidden=YES;
                guide1.hidden=YES;
                guide.hidden=YES;
            }
            else{
                Rectangle *rectangle = [[Rectangle alloc]initWithFrame:CGRectMake(0, 0, 150, 200)];
                rectangle.scaleValue = 1;
                rectangle.rotateValue = 0;
                guide.hidden=YES;
                guide1.hidden=YES;
                CGFloat hue = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
                CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
                CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
                [rectangle setBackgroundColor:[UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1]];
                [self addGestureRecognizersToView:rectangle];
                [fourbysix addSubview:rectangle];
                [self.fourBySixRectangleArray addObject:rectangle];
                guide2.hidden=YES;
                guide3.hidden=YES;
                guide1.hidden=YES;
                guide.hidden=YES;
            }
        }
        if (self.segmentedControl.selectedSegmentIndex==1)
        {
            if (UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation])) // Landscap
            {
                Rectangle *rectangle = [[Rectangle alloc]initWithFrame:CGRectMake(0, 0, 200, 150)];
                rectangle.scaleValue = 1;
                rectangle.rotateValue = 0;
                guide.hidden=YES;
                guide1.hidden=YES;
                CGFloat hue = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
                CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
                CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
                [rectangle setBackgroundColor:[UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1]];
                [self addGestureRecognizersToView:rectangle];
                [fivebyseven addSubview:rectangle];
                [self.fiveBySevenRectangleArray addObject:rectangle];
                guide2.hidden=YES;
                guide3.hidden=YES;
                guide1.hidden=YES;
                guide.hidden=YES;
            }
            else
            {
                Rectangle *rectangle = [[Rectangle alloc]initWithFrame:CGRectMake(0, 0, 150, 200)];
                rectangle.scaleValue = 1;
                rectangle.rotateValue = 0;
                guide.hidden=YES;
                guide1.hidden=YES;
                CGFloat hue = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
                CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
                CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
                [rectangle setBackgroundColor:[UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1]];
                [self addGestureRecognizersToView:rectangle];
                [fivebyseven addSubview:rectangle];
                [self.fiveBySevenRectangleArray addObject:rectangle];
                guide2.hidden=YES;
                guide3.hidden=YES;
                guide1.hidden=YES;
                guide.hidden=YES;
            }
        }
        
        if (self.segmentedControl.selectedSegmentIndex==2)
        {
            if (UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation])) // Landscap
            {
                Rectangle *rectangle = [[Rectangle alloc]initWithFrame:CGRectMake(0, 0, 200, 150)];
                rectangle.scaleValue = 1;
                rectangle.rotateValue = 0;
                guide.hidden=YES;
                guide1.hidden=YES;
                CGFloat hue = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
                CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
                CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
                [rectangle setBackgroundColor:[UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1]];
                [self addGestureRecognizersToView:rectangle];
                [fourbysixh addSubview:rectangle];
                [self.fourBySixRectangleArrayh addObject:rectangle];
                guide2.hidden=YES;
                guide3.hidden=YES;
                guide1.hidden=YES;
                guide.hidden=YES;
            }
            else
            {
                Rectangle *rectangle = [[Rectangle alloc]initWithFrame:CGRectMake(0, 0, 150, 200)];
                rectangle.scaleValue = 1;
                rectangle.rotateValue = 0;
                guide.hidden=YES;
                guide1.hidden=YES;
                CGFloat hue = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
                CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
                CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
                [rectangle setBackgroundColor:[UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1]];
                [self addGestureRecognizersToView:rectangle];
                [fourbysixh addSubview:rectangle];
                [self.fourBySixRectangleArrayh addObject:rectangle];
                guide2.hidden=YES;
                guide3.hidden=YES;
                guide1.hidden=YES;
                guide.hidden=YES;
            }
        }
        
        if (self.segmentedControl.selectedSegmentIndex==3)
        {
            if (UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation])) // Landscap
            {
                Rectangle *rectangle = [[Rectangle alloc]initWithFrame:CGRectMake(0, 0, 200, 150)];
                rectangle.scaleValue = 1;
                rectangle.rotateValue = 0;
                guide.hidden=YES;
                guide1.hidden=YES;
                CGFloat hue = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
                CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
                CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
                [rectangle setBackgroundColor:[UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1]];
                [self addGestureRecognizersToView:rectangle];
                [fivebysevenh addSubview:rectangle];
                [self.fiveBySevenRectangleArrayh addObject:rectangle];
                guide2.hidden=YES;
                guide3.hidden=YES;
                guide1.hidden=YES;
                guide.hidden=YES;
            }
            else
            {
                Rectangle *rectangle = [[Rectangle alloc]initWithFrame:CGRectMake(0, 0, 150, 200)];
                rectangle.scaleValue = 1;
                rectangle.rotateValue = 0;
                guide.hidden=YES;
                guide1.hidden=YES;
                CGFloat hue = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
                CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
                CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
                [rectangle setBackgroundColor:[UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1]];
                [self addGestureRecognizersToView:rectangle];
                [fivebysevenh addSubview:rectangle];
                [self.fiveBySevenRectangleArrayh addObject:rectangle];
                guide2.hidden=YES;
                guide3.hidden=YES;
                guide1.hidden=YES;
                guide.hidden=YES;
            }
        }
    }
}



- (IBAction)addPhoto1:(UIButton *)sender {
  
        
        if (self.segmentedControl.selectedSegmentIndex==0)
        {
            if (UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation])) // Landscap
            {
                Rectangle *rectangle = [[Rectangle alloc]initWithFrame:CGRectMake(0, 0, 200, 150)];
                rectangle.scaleValue = 1;
                rectangle.rotateValue = 0;
                guide.hidden=YES;
                guide1.hidden=YES;
                CGFloat hue = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
                CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
                CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
                [rectangle setBackgroundColor:[UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1]];
                [self addGestureRecognizersToView:rectangle];
                [fourbysix addSubview:rectangle];
                [self.fourBySixRectangleArray addObject:rectangle];
                guide2.hidden=YES;
                guide3.hidden=YES;
                guide1.hidden=YES;
                guide.hidden=YES;
            }
            else{
                Rectangle *rectangle = [[Rectangle alloc]initWithFrame:CGRectMake(0, 0, 150, 200)];
                rectangle.scaleValue = 1;
                rectangle.rotateValue = 0;
                guide.hidden=YES;
                guide1.hidden=YES;
                CGFloat hue = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
                CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
                CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
                [rectangle setBackgroundColor:[UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1]];
                [self addGestureRecognizersToView:rectangle];
                [fourbysix addSubview:rectangle];
                [self.fourBySixRectangleArray addObject:rectangle];
                guide2.hidden=YES;
                guide3.hidden=YES;
                guide1.hidden=YES;
                guide.hidden=YES;
            }
        }
        if (self.segmentedControl.selectedSegmentIndex==1)
        {
            if (UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation])) // Landscap
            {
                Rectangle *rectangle = [[Rectangle alloc]initWithFrame:CGRectMake(0, 0, 200, 150)];
                rectangle.scaleValue = 1;
                rectangle.rotateValue = 0;
                guide.hidden=YES;
                guide1.hidden=YES;
                CGFloat hue = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
                CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
                CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
                [rectangle setBackgroundColor:[UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1]];
                [self addGestureRecognizersToView:rectangle];
                [fivebyseven addSubview:rectangle];
                [self.fiveBySevenRectangleArray addObject:rectangle];
                guide2.hidden=YES;
                guide3.hidden=YES;
                guide1.hidden=YES;
                guide.hidden=YES;
            }
            else
            {
                Rectangle *rectangle = [[Rectangle alloc]initWithFrame:CGRectMake(0, 0, 150, 200)];
                rectangle.scaleValue = 1;
                rectangle.rotateValue = 0;
                guide.hidden=YES;
                guide1.hidden=YES;
                CGFloat hue = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
                CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
                CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
                [rectangle setBackgroundColor:[UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1]];
                [self addGestureRecognizersToView:rectangle];
                [fivebyseven addSubview:rectangle];
                [self.fiveBySevenRectangleArray addObject:rectangle];
                guide2.hidden=YES;
                guide3.hidden=YES;
                guide1.hidden=YES;
                guide.hidden=YES;
            }
        }
        
        if (self.segmentedControl.selectedSegmentIndex==2)
        {
            if (UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation])) // Landscap
            {
                Rectangle *rectangle = [[Rectangle alloc]initWithFrame:CGRectMake(0, 0, 200, 150)];
                rectangle.scaleValue = 1;
                rectangle.rotateValue = 0;
                guide.hidden=YES;
                guide1.hidden=YES;
                CGFloat hue = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
                CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
                CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
                [rectangle setBackgroundColor:[UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1]];
                [self addGestureRecognizersToView:rectangle];
                [fourbysixh addSubview:rectangle];
                [self.fourBySixRectangleArrayh addObject:rectangle];
                guide2.hidden=YES;
                guide3.hidden=YES;
                guide1.hidden=YES;
                guide.hidden=YES;
            }
            else
            {
                Rectangle *rectangle = [[Rectangle alloc]initWithFrame:CGRectMake(0, 0, 150, 200)];
                rectangle.scaleValue = 1;
                rectangle.rotateValue = 0;
                guide.hidden=YES;
                guide1.hidden=YES;
                CGFloat hue = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
                CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
                CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
                [rectangle setBackgroundColor:[UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1]];
                [self addGestureRecognizersToView:rectangle];
                [fourbysixh addSubview:rectangle];
                [self.fourBySixRectangleArrayh addObject:rectangle];
                guide2.hidden=YES;
                guide3.hidden=YES;
                guide1.hidden=YES;
                guide.hidden=YES;
            }
        }
        
        if (self.segmentedControl.selectedSegmentIndex==3)
        {
            if (UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation])) // Landscap
            {
                Rectangle *rectangle = [[Rectangle alloc]initWithFrame:CGRectMake(0, 0, 200, 150)];
                rectangle.scaleValue = 1;
                rectangle.rotateValue = 0;
                guide.hidden=YES;
                guide1.hidden=YES;
                CGFloat hue = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
                CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
                CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
                [rectangle setBackgroundColor:[UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1]];
                [self addGestureRecognizersToView:rectangle];
                [fivebysevenh addSubview:rectangle];
                [self.fiveBySevenRectangleArrayh addObject:rectangle];
                guide2.hidden=YES;
                guide3.hidden=YES;
                guide1.hidden=YES;
                guide.hidden=YES;
            }
            else
            {
                Rectangle *rectangle = [[Rectangle alloc]initWithFrame:CGRectMake(0, 0, 150, 200)];
                rectangle.scaleValue = 1;
                rectangle.rotateValue = 0;
                guide.hidden=YES;
                guide1.hidden=YES;
                CGFloat hue = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
                CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
                CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
                [rectangle setBackgroundColor:[UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1]];
                [self addGestureRecognizersToView:rectangle];
                [fivebysevenh addSubview:rectangle];
                [self.fiveBySevenRectangleArrayh addObject:rectangle];
                guide2.hidden=YES;
                guide3.hidden=YES;
                guide1.hidden=YES;
                guide.hidden=YES;
            }
        }
   
}


- (IBAction)bringfront: (id)sender{
    //    [rectangle bringSubviewToFront:rectangle];
}



- (IBAction)scaleImage:(UIStepper *)sender {
    CGFloat value = sender.value;
    selectedRectangle.scaleValue = value;
    
    CGAffineTransform rotate = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(selectedRectangle.rotateValue));
    CGAffineTransform scale = CGAffineTransformMakeScale(value, value);
    CGAffineTransform scaleAndRotate = CGAffineTransformConcat(rotate, scale);
    
    selectedRectangle.width = selectedRectangle.width * value;
    selectedRectangle.height = selectedRectangle.height * value;
    
    selectedRectangle.transform = scaleAndRotate;
    
    //    selectedRectangle.center = CGPointMake(selectedRectangle.xValue + selectedRectangle.frame.size.width/2, selectedRectangle.yValue + selectedRectangle.frame.size.height/2);
    
    txtScale.text = [NSString stringWithFormat:@"%.2f", value];
    
}

- (IBAction)rotateImage:(UIStepper *)sender {
    CGFloat value = sender.value;
    selectedRectangle.rotateValue = value;
    
    CGAffineTransform rotate = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(value));
    CGAffineTransform scale = CGAffineTransformMakeScale(selectedRectangle.scaleValue, selectedRectangle.scaleValue);
    CGAffineTransform scaleAndRotate = CGAffineTransformConcat(rotate, scale);
    
    selectedRectangle.transform = scaleAndRotate;
    
    //    selectedRectangle.center = CGPointMake(selectedRectangle.xValue + selectedRectangle.frame.size.width/2, selectedRectangle.yValue + selectedRectangle.frame.size.height/2);
    
    txtRotate.text = [NSString stringWithFormat:@"%.0f", value];
}

- (IBAction)moveImageX:(UIStepper *)sender {
    int value = (int)sender.value;
    
    CGAffineTransform rotate = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(selectedRectangle.rotateValue));
    CGAffineTransform scale = CGAffineTransformMakeScale(selectedRectangle.scaleValue, selectedRectangle.scaleValue);
    CGAffineTransform scaleAndRotate = CGAffineTransformConcat(rotate, scale);
    
    selectedRectangle.transform = scaleAndRotate;
    
    if (value > selectedRectangle.frame.origin.x)
    {
        selectedRectangle.center = CGPointMake(selectedRectangle.center.x + 1,
                                               selectedRectangle.center.y);
    }
    else
    {
        selectedRectangle.center = CGPointMake(selectedRectangle.center.x - 1,
                                               selectedRectangle.center.y);
    }
    
    //    selectedRectangle.xValue = value;
    txtMoveX.text = [NSString stringWithFormat:@"%.0f", selectedRectangle.frame.origin.x];
    
}

- (IBAction)moveImageY:(UIStepper *)sender {
    int value = (int)sender.value;
    
    CGAffineTransform rotate = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(selectedRectangle.rotateValue));
    CGAffineTransform scale = CGAffineTransformMakeScale(selectedRectangle.scaleValue, selectedRectangle.scaleValue);
    CGAffineTransform scaleAndRotate = CGAffineTransformConcat(rotate, scale);
    
    selectedRectangle.transform = scaleAndRotate;
    
    if (value > selectedRectangle.frame.origin.y)
    {
        selectedRectangle.center = CGPointMake(selectedRectangle.center.x,
                                               selectedRectangle.center.y + 1);
    }
    else
    {
        selectedRectangle.center = CGPointMake(selectedRectangle.center.x,
                                               selectedRectangle.center.y - 1);
    }
    
    //    selectedRectangle.yValue = value;
    txtMoveY.text = [NSString stringWithFormat:@"%.0f", selectedRectangle.frame.origin.y];
}

- (IBAction)changeRadius:(UIStepper*)sender {
    int radius = (int)sender.value;
    
    selectedRectangle.radius = radius;
    [selectedRectangle.layer setCornerRadius:radius];
    
    txtChangeRadius.text = [NSString stringWithFormat:@"%d", radius];
}


- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
}

- (void)resetExample {
    CGPoint defaultAnchorPoint = CGPointMake(.5f, .5f);
    
    if (self.segmentedControl.selectedSegmentIndex==0)
    {
        NSArray *views = [NSArray arrayWithObjects:fourbysix, nil];
        
        fourbysix.transform = CGAffineTransformIdentity;
        [views enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            UIView *theView = (UIView *)obj;
            theView.transform = CGAffineTransformIdentity;
            theView.layer.anchorPoint = defaultAnchorPoint;
            NSValue *originalCenter = [theView.layer valueForKey:@"originalCenter"];
            if(originalCenter) {
                theView.center = [originalCenter CGPointValue];
            }
        }];
        
        fourBySixScrollView.contentSize = CGSizeMake(476, 715);
    }
    
    if (self.segmentedControl.selectedSegmentIndex==1)
    {
        NSArray *views = [NSArray arrayWithObjects:fivebyseven, nil];
        
        fivebyseven.transform = CGAffineTransformIdentity;
        [views enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            UIView *theView = (UIView *)obj;
            theView.transform = CGAffineTransformIdentity;
            theView.layer.anchorPoint = defaultAnchorPoint;
            NSValue *originalCenter = [theView.layer valueForKey:@"originalCenter"];
            if(originalCenter) {
                theView.center = [originalCenter CGPointValue];
            }
        }];
        
        fiveBySevenScrollView.contentSize = CGSizeMake(510, 715);
    }
    
    if (self.segmentedControl.selectedSegmentIndex==2)
    {
        NSArray *views = [NSArray arrayWithObjects:fourbysixh, nil];
        
        fourbysixh.transform = CGAffineTransformIdentity;
        [views enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            UIView *theView = (UIView *)obj;
            theView.transform = CGAffineTransformIdentity;
            theView.layer.anchorPoint = defaultAnchorPoint;
            NSValue *originalCenter = [theView.layer valueForKey:@"originalCenter"];
            if(originalCenter) {
                theView.center = [originalCenter CGPointValue];
            }
        }];
        
        fourBySixScrollViewh.contentSize = CGSizeMake(715, 476);
    }
    
    if (self.segmentedControl.selectedSegmentIndex==3)
    {
        NSArray *views = [NSArray arrayWithObjects:fivebysevenh, nil];
        
        fivebysevenh.transform = CGAffineTransformIdentity;
        [views enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            UIView *theView = (UIView *)obj;
            theView.transform = CGAffineTransformIdentity;
            theView.layer.anchorPoint = defaultAnchorPoint;
            NSValue *originalCenter = [theView.layer valueForKey:@"originalCenter"];
            if(originalCenter) {
                theView.center = [originalCenter CGPointValue];
            }
        }];
        
        fiveBySevenScrollViewh.contentSize = CGSizeMake(715, 510);
    }
    
    
    ;
}

- (void)adjustAnchorPointForGestureRecognizer:(UIGestureRecognizer *)recognizer {
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        CGPoint locationInView = [recognizer locationInView:recognizer.view];
        CGPoint locationInSuperview = [recognizer locationInView:recognizer.view.superview];
        CGSize viewSize = recognizer.view.bounds.size;
        
        recognizer.view.layer.anchorPoint = CGPointMake(locationInView.x / viewSize.width, locationInView.y / viewSize.height);
        recognizer.view.layer.position = locationInSuperview;
    }
}


- (void)addGestureRecognizersToView:(UIView *)theView {
    
    UIRotationGestureRecognizer *rotationGesture = [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(handleRotate:)];
    rotationGesture.delegate = self;
    [theView addGestureRecognizer:rotationGesture];
    //[rotationGesture release];
    
    UIPinchGestureRecognizer *pinchGesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handleScale:)];
    pinchGesture.delegate = self;
    [theView addGestureRecognizer:pinchGesture];
    
    
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
    [panGesture setMaximumNumberOfTouches:2];
    panGesture.delegate = self;
    [theView addGestureRecognizer:panGesture];
    
    
    UIPanGestureRecognizer *panSingleGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    [panSingleGesture setMaximumNumberOfTouches:1];
    panSingleGesture.delegate = self;
    [theView addGestureRecognizer:panSingleGesture];
    
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    singleTap.cancelsTouchesInView = NO; //Default value for cancelsTouchesInView is YES, which will prevent buttons to be clicked
    [theView addGestureRecognizer:singleTap];
    
    UILongPressGestureRecognizer *longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
    [theView addGestureRecognizer:longPressGesture];
    
    [theView.layer setValue:[NSValue valueWithCGPoint:theView.center] forKey:@"originalCenter"];
}

- (void)addGestureRecognizersToParentView:(UIView*)parentView
{
    UIPanGestureRecognizer *panSingleGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    [panSingleGesture setMaximumNumberOfTouches:1];
    panSingleGesture.delegate = self;
    [parentView addGestureRecognizer:panSingleGesture];
    
}

- (void)handlePan:(UIPanGestureRecognizer *)recognizer {
    if(recognizer.state == UIGestureRecognizerStateBegan ||
       recognizer.state == UIGestureRecognizerStateChanged) {
        if (self.segmentedControl.selectedSegmentIndex == 0)
        {
            if (recognizer.view == fourbysix)
            {
                [fourBySixScrollView setScrollEnabled:YES];
            }
            else
            {
                [fourBySixScrollView setScrollEnabled:NO];
                
                CGPoint translation = [recognizer translationInView:recognizer.view.superview];
                
                recognizer.view.center =
                CGPointMake(recognizer.view.center.x + translation.x,
                            recognizer.view.center.y + translation.y);
                
                selectedRectangle = (Rectangle*)recognizer.view;
                
                moveImageX.value = selectedRectangle.frame.origin.x;
                moveImageY.value = selectedRectangle.frame.origin.y;
                
                txtMoveX.text = [NSString stringWithFormat:@"%.0f", selectedRectangle.frame.origin.x];
                txtMoveY.text = [NSString stringWithFormat:@"%.0f", selectedRectangle.frame.origin.y];
                
                [recognizer setTranslation:CGPointZero inView:recognizer.view.superview];
            }
        }
        if (self.segmentedControl.selectedSegmentIndex == 1)
        {
            if (recognizer.view == fivebyseven)
            {
                [fiveBySevenScrollView setScrollEnabled:YES];
            }
            else
            {
                [fiveBySevenScrollView setScrollEnabled:NO];
                
                CGPoint translation = [recognizer translationInView:recognizer.view.superview];
                
                recognizer.view.center =
                CGPointMake(recognizer.view.center.x + translation.x,
                            recognizer.view.center.y + translation.y);
                
                selectedRectangle = (Rectangle*)recognizer.view;
                
                moveImageX.value = selectedRectangle.frame.origin.x;
                moveImageY.value = selectedRectangle.frame.origin.y;
                
                txtMoveX.text = [NSString stringWithFormat:@"%.0f", selectedRectangle.frame.origin.x];
                txtMoveY.text = [NSString stringWithFormat:@"%.0f", selectedRectangle.frame.origin.y];
                
                [recognizer setTranslation:CGPointZero inView:recognizer.view.superview];
            }
        }
        if (self.segmentedControl.selectedSegmentIndex == 2)
        {
            if (recognizer.view == fourbysixh)
            {
                [fourBySixScrollViewh setScrollEnabled:YES];
            }
            else
            {
                [fourBySixScrollViewh setScrollEnabled:NO];
                
                CGPoint translation = [recognizer translationInView:recognizer.view.superview];
                
                recognizer.view.center =
                CGPointMake(recognizer.view.center.x + translation.x,
                            recognizer.view.center.y + translation.y);
                
                selectedRectangle = (Rectangle*)recognizer.view;
                
                moveImageX.value = selectedRectangle.frame.origin.x;
                moveImageY.value = selectedRectangle.frame.origin.y;
                
                txtMoveX.text = [NSString stringWithFormat:@"%.0f", selectedRectangle.frame.origin.x];
                txtMoveY.text = [NSString stringWithFormat:@"%.0f", selectedRectangle.frame.origin.y];
                
                [recognizer setTranslation:CGPointZero inView:recognizer.view.superview];
            }
        }
        
        if (self.segmentedControl.selectedSegmentIndex == 3)
        {
            if (recognizer.view == fivebysevenh)
            {
                [fiveBySevenScrollViewh setScrollEnabled:YES];
            }
            else
            {
                [fiveBySevenScrollViewh setScrollEnabled:NO];
                
                CGPoint translation = [recognizer translationInView:recognizer.view.superview];
                
                recognizer.view.center =
                CGPointMake(recognizer.view.center.x + translation.x,
                            recognizer.view.center.y + translation.y);
                
                selectedRectangle = (Rectangle*)recognizer.view;
                
                moveImageX.value = selectedRectangle.frame.origin.x;
                moveImageY.value = selectedRectangle.frame.origin.y;
                
                txtMoveX.text = [NSString stringWithFormat:@"%.0f", selectedRectangle.frame.origin.x];
                txtMoveY.text = [NSString stringWithFormat:@"%.0f", selectedRectangle.frame.origin.y];
                
                [recognizer setTranslation:CGPointZero inView:recognizer.view.superview];
            }
            
            
        }
    }
}

- (void)handleRotate:(UIRotationGestureRecognizer *)recognizer {
    //    [self adjustAnchorPointForGestureRecognizer:recognizer];
    if(recognizer.state == UIGestureRecognizerStateBegan ||
       recognizer.state == UIGestureRecognizerStateChanged) {
        if (self.segmentedControl.selectedSegmentIndex == 0)
        {
            if (recognizer.view == fourbysix)
            {
                [fourBySixScrollView setScrollEnabled:YES];
            }
            else
            {
                [fourBySixScrollView setScrollEnabled:NO];
                
                //                recognizer.view.transform = CGAffineTransformRotate(recognizer.view.transform, recognizer.rotation);
                //                recognizer.view.center = selectedRectangle.center;
                
                selectedRectangle = (Rectangle*)recognizer.view;
                
                //Update rotate property
                selectedRectangle.rotateValue += RADIANS_TO_DEGREES(recognizer.rotation);
                
                CGAffineTransform rotate = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(selectedRectangle.rotateValue));
                CGAffineTransform scale = CGAffineTransformMakeScale(selectedRectangle.scaleValue, selectedRectangle.scaleValue);
                CGAffineTransform scaleAndRotate = CGAffineTransformConcat(rotate, scale);
                selectedRectangle.transform = scaleAndRotate;
                
                rotateSlider.value = selectedRectangle.rotateValue;
                txtRotate.text = [NSString stringWithFormat:@"%.0f", selectedRectangle.rotateValue];
                
                moveImageX.value = selectedRectangle.frame.origin.x;
                moveImageY.value = selectedRectangle.frame.origin.y;
                
                txtMoveX.text = [NSString stringWithFormat:@"%.0f", selectedRectangle.frame.origin.x];
                txtMoveY.text = [NSString stringWithFormat:@"%.0f", selectedRectangle.frame.origin.y];
                
                [recognizer setRotation:0];
            }
        }
        if (self.segmentedControl.selectedSegmentIndex == 1)
        {
            if (recognizer.view == fivebyseven)
            {
                [fiveBySevenScrollView setScrollEnabled:YES];
            }
            else
            {
                [fiveBySevenScrollView setScrollEnabled:NO];
                
                //                recognizer.view.transform = CGAffineTransformRotate(recognizer.view.transform, recognizer.rotation);
                //                recognizer.view.center = selectedRectangle.center;
                
                selectedRectangle = (Rectangle*)recognizer.view;
                
                //Update rotate property
                selectedRectangle.rotateValue += RADIANS_TO_DEGREES(recognizer.rotation);
                
                CGAffineTransform rotate = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(selectedRectangle.rotateValue));
                CGAffineTransform scale = CGAffineTransformMakeScale(selectedRectangle.scaleValue, selectedRectangle.scaleValue);
                CGAffineTransform scaleAndRotate = CGAffineTransformConcat(rotate, scale);
                selectedRectangle.transform = scaleAndRotate;
                
                rotateSlider.value = selectedRectangle.rotateValue;
                txtRotate.text = [NSString stringWithFormat:@"%.0f", selectedRectangle.rotateValue];
                
                moveImageX.value = selectedRectangle.frame.origin.x;
                moveImageY.value = selectedRectangle.frame.origin.y;
                
                txtMoveX.text = [NSString stringWithFormat:@"%.0f", selectedRectangle.frame.origin.x];
                txtMoveY.text = [NSString stringWithFormat:@"%.0f", selectedRectangle.frame.origin.y];
                
                [recognizer setRotation:0];
            }
        }
        
        if (self.segmentedControl.selectedSegmentIndex == 2)
        {
            if (recognizer.view == fourbysixh)
            {
                [fourBySixScrollViewh setScrollEnabled:YES];
            }
            else
            {
                [fourBySixScrollViewh setScrollEnabled:NO];
                
                //                recognizer.view.transform = CGAffineTransformRotate(recognizer.view.transform, recognizer.rotation);
                //                recognizer.view.center = selectedRectangle.center;
                
                selectedRectangle = (Rectangle*)recognizer.view;
                
                //Update rotate property
                selectedRectangle.rotateValue += RADIANS_TO_DEGREES(recognizer.rotation);
                
                CGAffineTransform rotate = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(selectedRectangle.rotateValue));
                CGAffineTransform scale = CGAffineTransformMakeScale(selectedRectangle.scaleValue, selectedRectangle.scaleValue);
                CGAffineTransform scaleAndRotate = CGAffineTransformConcat(rotate, scale);
                selectedRectangle.transform = scaleAndRotate;
                
                rotateSlider.value = selectedRectangle.rotateValue;
                txtRotate.text = [NSString stringWithFormat:@"%.0f", selectedRectangle.rotateValue];
                
                moveImageX.value = selectedRectangle.frame.origin.x;
                moveImageY.value = selectedRectangle.frame.origin.y;
                
                txtMoveX.text = [NSString stringWithFormat:@"%.0f", selectedRectangle.frame.origin.x];
                txtMoveY.text = [NSString stringWithFormat:@"%.0f", selectedRectangle.frame.origin.y];
                
                [recognizer setRotation:0];
            }
        }
        if (self.segmentedControl.selectedSegmentIndex == 3)
        {
            if (recognizer.view == fivebysevenh)
            {
                [fiveBySevenScrollViewh setScrollEnabled:YES];
            }
            else
            {
                [fiveBySevenScrollViewh setScrollEnabled:NO];
                
                //                recognizer.view.transform = CGAffineTransformRotate(recognizer.view.transform, recognizer.rotation);
                //                recognizer.view.center = selectedRectangle.center;
                
                selectedRectangle = (Rectangle*)recognizer.view;
                
                //Update rotate property
                selectedRectangle.rotateValue += RADIANS_TO_DEGREES(recognizer.rotation);
                
                CGAffineTransform rotate = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(selectedRectangle.rotateValue));
                CGAffineTransform scale = CGAffineTransformMakeScale(selectedRectangle.scaleValue, selectedRectangle.scaleValue);
                CGAffineTransform scaleAndRotate = CGAffineTransformConcat(rotate, scale);
                selectedRectangle.transform = scaleAndRotate;
                
                rotateSlider.value = selectedRectangle.rotateValue;
                txtRotate.text = [NSString stringWithFormat:@"%.0f", selectedRectangle.rotateValue];
                
                moveImageX.value = selectedRectangle.frame.origin.x;
                moveImageY.value = selectedRectangle.frame.origin.y;
                
                txtMoveX.text = [NSString stringWithFormat:@"%.0f", selectedRectangle.frame.origin.x];
                txtMoveY.text = [NSString stringWithFormat:@"%.0f", selectedRectangle.frame.origin.y];
                
                [recognizer setRotation:0];
            }
        }
        
    }
}

- (void)handleScale:(UIPinchGestureRecognizer *)recognizer {
    //    [self adjustAnchorPointForGestureRecognizer:recognizer];
    if ([recognizer state] == UIGestureRecognizerStateBegan ||
        [recognizer state] == UIGestureRecognizerStateChanged) {
        if (self.segmentedControl.selectedSegmentIndex == 0)
        {
            if (recognizer.view == fourbysix)
            {
                [fourBySixScrollView setScrollEnabled:YES];
            }
            else
            {
                [fourBySixScrollView setScrollEnabled:NO];
                
                //                recognizer.view.transform = CGAffineTransformScale(recognizer.view.transform, recognizer.scale, recognizer.scale);
                //                recognizer.view.center = selectedRectangle.center;
                
                selectedRectangle = (Rectangle*)recognizer.view;
                
                
                //Update scale property
                selectedRectangle.scaleValue *= recognizer.scale;
                
                CGAffineTransform rotate = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(selectedRectangle.rotateValue));
                CGAffineTransform scale = CGAffineTransformMakeScale(selectedRectangle.scaleValue, selectedRectangle.scaleValue);
                CGAffineTransform scaleAndRotate = CGAffineTransformConcat(rotate, scale);
                selectedRectangle.transform = scaleAndRotate;
                
                scaleSlider.value = selectedRectangle.scaleValue;
                txtScale.text = [NSString stringWithFormat:@"%.2f", selectedRectangle.scaleValue];
                
                moveImageX.value = selectedRectangle.frame.origin.x;
                moveImageY.value = selectedRectangle.frame.origin.y;
                
                txtMoveX.text = [NSString stringWithFormat:@"%.0f", selectedRectangle.frame.origin.x];
                txtMoveY.text = [NSString stringWithFormat:@"%.0f", selectedRectangle.frame.origin.y];
                
                [recognizer setScale:1];
            }
        }
        if (self.segmentedControl.selectedSegmentIndex == 1)
        {
            if (recognizer.view == fivebyseven)
            {
                [fiveBySevenScrollView setScrollEnabled:YES];
                //                return;
            }
            else
            {
                [fiveBySevenScrollView setScrollEnabled:NO];
                
                //                recognizer.view.transform = CGAffineTransformScale(recognizer.view.transform, recognizer.scale, recognizer.scale);
                //                recognizer.view.center = selectedRectangle.center;
                
                selectedRectangle = (Rectangle*)recognizer.view;
                
                //Update scale property
                selectedRectangle.scaleValue *= recognizer.scale;
                
                CGAffineTransform rotate = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(selectedRectangle.rotateValue));
                CGAffineTransform scale = CGAffineTransformMakeScale(selectedRectangle.scaleValue, selectedRectangle.scaleValue);
                CGAffineTransform scaleAndRotate = CGAffineTransformConcat(rotate, scale);
                selectedRectangle.transform = scaleAndRotate;
                
                scaleSlider.value = selectedRectangle.scaleValue;
                txtScale.text = [NSString stringWithFormat:@"%.2f", selectedRectangle.scaleValue];
                
                moveImageX.value = selectedRectangle.frame.origin.x;
                moveImageY.value = selectedRectangle.frame.origin.y;
                
                txtMoveX.text = [NSString stringWithFormat:@"%.0f", selectedRectangle.frame.origin.x];
                txtMoveY.text = [NSString stringWithFormat:@"%.0f", selectedRectangle.frame.origin.y];
                
                [recognizer setScale:1];
            }
        }
        if (self.segmentedControl.selectedSegmentIndex == 2)
        {
            if (recognizer.view == fourbysixh)
            {
                [fourBySixScrollViewh setScrollEnabled:YES];
            }
            else
            {
                [fourBySixScrollViewh setScrollEnabled:NO];
                
                //                recognizer.view.transform = CGAffineTransformScale(recognizer.view.transform, recognizer.scale, recognizer.scale);
                //                recognizer.view.center = selectedRectangle.center;
                
                selectedRectangle = (Rectangle*)recognizer.view;
                
                
                //Update scale property
                selectedRectangle.scaleValue *= recognizer.scale;
                
                CGAffineTransform rotate = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(selectedRectangle.rotateValue));
                CGAffineTransform scale = CGAffineTransformMakeScale(selectedRectangle.scaleValue, selectedRectangle.scaleValue);
                CGAffineTransform scaleAndRotate = CGAffineTransformConcat(rotate, scale);
                selectedRectangle.transform = scaleAndRotate;
                
                scaleSlider.value = selectedRectangle.scaleValue;
                txtScale.text = [NSString stringWithFormat:@"%.2f", selectedRectangle.scaleValue];
                
                moveImageX.value = selectedRectangle.frame.origin.x;
                moveImageY.value = selectedRectangle.frame.origin.y;
                
                txtMoveX.text = [NSString stringWithFormat:@"%.0f", selectedRectangle.frame.origin.x];
                txtMoveY.text = [NSString stringWithFormat:@"%.0f", selectedRectangle.frame.origin.y];
                
                [recognizer setScale:1];
            }
        }
        if (self.segmentedControl.selectedSegmentIndex == 3)
        {
            if (recognizer.view == fivebysevenh)
            {
                [fiveBySevenScrollViewh setScrollEnabled:YES];
                //                return;
            }
            else
            {
                [fiveBySevenScrollViewh setScrollEnabled:NO];
                
                //                recognizer.view.transform = CGAffineTransformScale(recognizer.view.transform, recognizer.scale, recognizer.scale);
                //                recognizer.view.center = selectedRectangle.center;
                
                selectedRectangle = (Rectangle*)recognizer.view;
                
                //Update scale property
                selectedRectangle.scaleValue *= recognizer.scale;
                
                CGAffineTransform rotate = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(selectedRectangle.rotateValue));
                CGAffineTransform scale = CGAffineTransformMakeScale(selectedRectangle.scaleValue, selectedRectangle.scaleValue);
                CGAffineTransform scaleAndRotate = CGAffineTransformConcat(rotate, scale);
                selectedRectangle.transform = scaleAndRotate;
                
                scaleSlider.value = selectedRectangle.scaleValue;
                txtScale.text = [NSString stringWithFormat:@"%.2f", selectedRectangle.scaleValue];
                
                moveImageX.value = selectedRectangle.frame.origin.x;
                moveImageY.value = selectedRectangle.frame.origin.y;
                
                txtMoveX.text = [NSString stringWithFormat:@"%.0f", selectedRectangle.frame.origin.x];
                txtMoveY.text = [NSString stringWithFormat:@"%.0f", selectedRectangle.frame.origin.y];
                
                [recognizer setScale:1];
            }
        }
    }
    
}

- (void)handleDoubleTap:(UITapGestureRecognizer *)recognizer {
    [self adjustAnchorPointForGestureRecognizer:recognizer];
    
    recognizer.view.transform = CGAffineTransformScale(fourbysix.transform, 1.5f, 1.5f);
    
    [fourBySixScrollView setContentSize:fourbysix.frame.size];
}

- (void)handleDoubleTap1:(UITapGestureRecognizer *)recognizer {
    [self adjustAnchorPointForGestureRecognizer:recognizer];
    
    recognizer.view.transform = CGAffineTransformScale(fivebyseven.transform, 1.5f, 1.5f);
    
    [fiveBySevenScrollView setContentSize:fivebyseven.frame.size];
}

- (void)handleDoubleTap2:(UITapGestureRecognizer *)recognizer {
    [self adjustAnchorPointForGestureRecognizer:recognizer];
    
    recognizer.view.transform = CGAffineTransformScale(fourbysixh.transform, 1.5f, 1.5f);
    
    [fourBySixScrollViewh setContentSize:fourbysixh.frame.size];
}

- (void)handleDoubleTap3:(UITapGestureRecognizer *)recognizer {
    [self adjustAnchorPointForGestureRecognizer:recognizer];
    
    recognizer.view.transform = CGAffineTransformScale(fivebysevenh.transform, 1.5f, 1.5f);
    
    [fiveBySevenScrollViewh setContentSize:fivebysevenh.frame.size];
}



- (BOOL)gestureRecognizer:(UIGestureRecognizer *)recognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)other {
    return YES;
}

- (void)handleLongPress:(UILongPressGestureRecognizer *)recognizer
{
    NSLog(@"Long press");
    if ([recognizer state] == UIGestureRecognizerStateBegan) {
        UIPasteboard *pasteBoard = [UIPasteboard generalPasteboard];
        if ([pasteBoard dataForPasteboardType:@"Rectangle"])
        {
            UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                                     delegate:self
                                                            cancelButtonTitle:nil
                                                       destructiveButtonTitle:nil
                                                            otherButtonTitles:@"Copy", @"Paste", nil];
            [actionSheet showFromRect:selectedRectangle.frame inView:selectedRectangle.superview animated:YES];
        }
        else
        {
            UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                                     delegate:self
                                                            cancelButtonTitle:nil
                                                       destructiveButtonTitle:nil
                                                            otherButtonTitles:@"Copy", nil];
            [actionSheet showFromRect:selectedRectangle.frame inView:selectedRectangle.superview animated:YES];
        }
    }
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    //    NSLog(@"Index = %d - Title = %@", buttonIndex, [actionSheet buttonTitleAtIndex:buttonIndex]);
    if (buttonIndex == 0)
    {
        NSLog(@"Copy");
        UIPasteboard *pasteBoard = [UIPasteboard generalPasteboard];
        NSDictionary *copiedDictionary = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithFloat:selectedRectangle.scaleValue], @"scale", [NSNumber numberWithFloat:selectedRectangle.rotateValue], @"rotate", [NSNumber numberWithFloat:selectedRectangle.radius], @"radius", nil];
        NSData *copiedData = [NSKeyedArchiver archivedDataWithRootObject:copiedDictionary];
        if (copiedData) [pasteBoard setData:copiedData forPasteboardType:@"Rectangle"];
    }
    else if (buttonIndex == 1)
    {
        NSLog(@"Paste");
        UIPasteboard *pasteBoard = [UIPasteboard generalPasteboard];
        NSData *copiedData = [pasteBoard dataForPasteboardType:@"Rectangle"];
        if (copiedData)
        {
            NSDictionary *copiedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:copiedData];
            
            CGFloat scaleValue = [copiedDictionary[@"scale"] floatValue];
            CGFloat rotateValue = [copiedDictionary[@"rotate"] floatValue];
            CGFloat radiusValue = [copiedDictionary[@"radius"] floatValue];
            
            selectedRectangle.scaleValue = scaleValue;
            selectedRectangle.rotateValue = rotateValue;
            selectedRectangle.radius = radiusValue;
            
            //Set corner radius
            [selectedRectangle.layer setCornerRadius:selectedRectangle.radius];
            
            //Make rotate and scale
            CGAffineTransform rotate = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(selectedRectangle.rotateValue));
            CGAffineTransform scale = CGAffineTransformMakeScale(selectedRectangle.scaleValue, selectedRectangle.scaleValue);
            CGAffineTransform scaleAndRotate = CGAffineTransformConcat(rotate, scale);
            selectedRectangle.transform = scaleAndRotate;
            
            //Set again coords value
            moveImageX.value = selectedRectangle.frame.origin.x;
            moveImageY.value = selectedRectangle.frame.origin.y;
            txtMoveX.text = [NSString stringWithFormat:@"%.0f", selectedRectangle.frame.origin.x];
            txtMoveY.text = [NSString stringWithFormat:@"%.0f", selectedRectangle.frame.origin.y];
            
            //Set scale value again
            scaleSlider.value = selectedRectangle.scaleValue;
            txtScale.text = [NSString stringWithFormat:@"%.2f", selectedRectangle.scaleValue];
            
            //Set rotate value again
            rotateSlider.value = selectedRectangle.rotateValue;
            txtRotate.text = [NSString stringWithFormat:@"%.0f", selectedRectangle.rotateValue];
            
            //Set radius value again
            changeRadius.value = selectedRectangle.radius;
            txtChangeRadius.text = [NSString stringWithFormat:@"%d", selectedRectangle.radius];
        }
    }
}

- (IBAction)resetLayout:(id)sender {
    if (self.segmentedControl.selectedSegmentIndex==0)
    {
        if (self.fourBySixRectangleArray.count > 0)
        {
            Rectangle *lastRectangle = [self.fourBySixRectangleArray objectAtIndex:self.fourBySixRectangleArray.count - 1];
            
            if (lastRectangle == selectedRectangle)
            {
                selectedRectangle = nil;
                
                scaleSlider.value = 1;
                rotateSlider.value = 0;
                moveImageX.value = 0;
                moveImageY.value = 0;
                
                txtScale.text = @"1";
                txtRotate.text = @"0";
                txtMoveX.text = @"0";
                txtMoveY.text = @"0";
            }
            
            [lastRectangle removeFromSuperview];
            [self.fourBySixRectangleArray removeLastObject];
        }
    }
    if (self.segmentedControl.selectedSegmentIndex==1)
    {
        if (self.fiveBySevenRectangleArray.count > 0)
        {
            Rectangle *lastRectangle = [self.fiveBySevenRectangleArray objectAtIndex:self.fiveBySevenRectangleArray.count - 1];
            if (lastRectangle == selectedRectangle)
            {
                selectedRectangle = nil;
                
                scaleSlider.value = 1;
                rotateSlider.value = 0;
                moveImageX.value = 0;
                moveImageY.value = 0;
                
                txtScale.text = @"1";
                txtRotate.text = @"0";
                txtMoveX.text = @"0";
                txtMoveY.text = @"0";
            }
            
            [lastRectangle removeFromSuperview];
            
            [self.fiveBySevenRectangleArray removeLastObject];
        }
    }
    if (self.segmentedControl.selectedSegmentIndex==2)
    {
        if (self.fourBySixRectangleArrayh.count > 0)
        {
            Rectangle *lastRectangle = [self.fourBySixRectangleArrayh objectAtIndex:self.fourBySixRectangleArrayh.count - 1];
            
            if (lastRectangle == selectedRectangle)
            {
                selectedRectangle = nil;
                
                scaleSlider.value = 1;
                rotateSlider.value = 0;
                moveImageX.value = 0;
                moveImageY.value = 0;
                
                txtScale.text = @"1";
                txtRotate.text = @"0";
                txtMoveX.text = @"0";
                txtMoveY.text = @"0";
            }
            
            [lastRectangle removeFromSuperview];
            [self.fourBySixRectangleArrayh removeLastObject];
        }
    }
    if (self.segmentedControl.selectedSegmentIndex==3)
    {
        if (self.fiveBySevenRectangleArrayh.count > 0)
        {
            Rectangle *lastRectangle = [self.fiveBySevenRectangleArrayh objectAtIndex:self.fiveBySevenRectangleArrayh.count - 1];
            if (lastRectangle == selectedRectangle)
            {
                selectedRectangle = nil;
                
                scaleSlider.value = 1;
                rotateSlider.value = 0;
                moveImageX.value = 0;
                moveImageY.value = 0;
                
                txtScale.text = @"1";
                txtRotate.text = @"0";
                txtMoveX.text = @"0";
                txtMoveY.text = @"0";
            }
            
            [lastRectangle removeFromSuperview];
            
            [self.fiveBySevenRectangleArrayh removeLastObject];
        }
    }
    
}

- (IBAction)emailLayout:(id)sender {
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    picker.mailComposeDelegate = self;
    
    //Set subject
    //    [picker setSubject:@"Email subject"];
    
    // Set up recipients
    //    NSArray *toRecipients = [NSArray arrayWithObject:@""];
    //    [picker setToRecipients:toRecipients];
    
    //Create image from view
    UIImage *capturedImage;
    if (self.segmentedControl.selectedSegmentIndex == 0)
    {
        capturedImage = [self captureView:fourbysix];
        capturedImage = [self imageWithImage:capturedImage scaledToSize:CGSizeMake(1200, 1800)];
    }
    if (self.segmentedControl.selectedSegmentIndex == 1)
    {
        capturedImage = [self captureView:fivebyseven];
        capturedImage = [self imageWithImage:capturedImage scaledToSize:CGSizeMake(1500, 2100)];
    }
    
    if (self.segmentedControl.selectedSegmentIndex == 2)
    {
        capturedImage = [self captureView:fourbysixh];
        capturedImage = [self imageWithImage:capturedImage scaledToSize:CGSizeMake(1800, 1200)];
    }
    if (self.segmentedControl.selectedSegmentIndex == 3)
    {
        capturedImage = [self captureView:fivebysevenh];
        capturedImage = [self imageWithImage:capturedImage scaledToSize:CGSizeMake(2100, 1500)];
    }
    
    // Attach image to the email
    NSData *imageData = UIImagePNGRepresentation (capturedImage);
    NSString *tempFilePath = [[self getDocumentPath] stringByAppendingPathComponent:@"iSIT-customview.png"];
    [imageData writeToFile:tempFilePath atomically:YES];
    
    if (imageData != nil)
    {
        [picker addAttachmentData:imageData mimeType:@"image/png" fileName:tempFilePath];
    }
    
    // Fill out the email body text
    [picker setMessageBody:[NSString stringWithFormat:@""] isHTML:YES];
    
    
    [self presentViewController:picker animated:NO completion:nil];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
        {
            NSLog(@"Mail cancelled");
            break;
        }
        case MFMailComposeResultSaved:
        {
            NSLog(@"Email saved");
            break;
        }
        case MFMailComposeResultSent:
        {
            NSLog(@"Email sent");
            break;
        }
        case MFMailComposeResultFailed:
        {
            NSLog(@"Email failed");
            break;
        }
        default:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email" message:@"Email Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
            break;
    }
    
    [self becomeFirstResponder];
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (UIImage *)captureView:(UIView *)view
{
    CGRect screenRect = view.frame;
    UIGraphicsBeginImageContextWithOptions(screenRect.size, view.opaque, 0.0);
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextFillRect(ctx, screenRect);
    [view.layer renderInContext:ctx];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (NSString*)getDocumentPath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return documentsDirectory;
}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}



//Tap gesture
-(void)handleSingleTap:(UITapGestureRecognizer *)gesture
{
    
    if (self.segmentedControl.selectedSegmentIndex == 0)
    {
        if (gesture.view == fourbysix)
        {
            fourBySixScrollView.scrollEnabled = YES;
            //            return;
        }
        else
        {
            fourBySixScrollView.scrollEnabled = NO;
            selectedRectangle = (Rectangle*)gesture.view;
            
            NSLog(@"Rectangle: %@", selectedRectangle);
            NSLog(@"Rectangle center point: %.0f %.0f", selectedRectangle.center.x, selectedRectangle.center.y);
            
            rotateSlider.value = [selectedRectangle rotateValue];
            scaleSlider.value = [selectedRectangle scaleValue];
            moveImageX.value = selectedRectangle.frame.origin.x;
            moveImageY.value = selectedRectangle.frame.origin.y;
            changeRadius.value = selectedRectangle.radius;
            
            txtScale.text = [NSString stringWithFormat:@"%.2f", selectedRectangle.scaleValue];
            txtRotate.text = [NSString stringWithFormat:@"%.0f", selectedRectangle.rotateValue];
            txtMoveX.text = [NSString stringWithFormat:@"%.0f", selectedRectangle.frame.origin.x];
            txtMoveY.text = [NSString stringWithFormat:@"%.0f", selectedRectangle.frame.origin.y];
            txtChangeRadius.text = [NSString stringWithFormat:@"%d", selectedRectangle.radius];
        }
    }
    if (self.segmentedControl.selectedSegmentIndex == 1)
    {
        if (gesture.view == fivebyseven)
        {
            fiveBySevenScrollView.scrollEnabled = YES;
            //            return;
        }
        else
        {
            fiveBySevenScrollView.scrollEnabled = NO;
            selectedRectangle = (Rectangle*)gesture.view;
            
            rotateSlider.value = [selectedRectangle rotateValue];
            scaleSlider.value = [selectedRectangle scaleValue];
            moveImageX.value = selectedRectangle.frame.origin.x;
            moveImageY.value = selectedRectangle.frame.origin.y;
            changeRadius.value = selectedRectangle.radius;
            
            txtScale.text = [NSString stringWithFormat:@"%.2f", selectedRectangle.scaleValue];
            txtRotate.text = [NSString stringWithFormat:@"%.0f", selectedRectangle.rotateValue];
            txtMoveX.text = [NSString stringWithFormat:@"%.0f", selectedRectangle.frame.origin.x];
            txtMoveY.text = [NSString stringWithFormat:@"%.0f", selectedRectangle.frame.origin.y];
            txtChangeRadius.text = [NSString stringWithFormat:@"%d", selectedRectangle.radius];
        }
    }
    
    if (self.segmentedControl.selectedSegmentIndex == 2)
    {
        if (gesture.view == fourbysixh)
        {
            fourBySixScrollViewh.scrollEnabled = YES;
            //            return;
        }
        else
        {
            fourBySixScrollViewh.scrollEnabled = NO;
            selectedRectangle = (Rectangle*)gesture.view;
            
            NSLog(@"Rectangle: %@", selectedRectangle);
            NSLog(@"Rectangle center point: %.0f %.0f", selectedRectangle.center.x, selectedRectangle.center.y);
            
            rotateSlider.value = [selectedRectangle rotateValue];
            scaleSlider.value = [selectedRectangle scaleValue];
            moveImageX.value = selectedRectangle.frame.origin.x;
            moveImageY.value = selectedRectangle.frame.origin.y;
            changeRadius.value = selectedRectangle.radius;
            
            txtScale.text = [NSString stringWithFormat:@"%.2f", selectedRectangle.scaleValue];
            txtRotate.text = [NSString stringWithFormat:@"%.0f", selectedRectangle.rotateValue];
            txtMoveX.text = [NSString stringWithFormat:@"%.0f", selectedRectangle.frame.origin.x];
            txtMoveY.text = [NSString stringWithFormat:@"%.0f", selectedRectangle.frame.origin.y];
            txtChangeRadius.text = [NSString stringWithFormat:@"%d", selectedRectangle.radius];
        }
    }
    if (self.segmentedControl.selectedSegmentIndex == 3)
    {
        if (gesture.view == fivebysevenh)
        {
            fiveBySevenScrollViewh.scrollEnabled = YES;
            //            return;
        }
        else
        {
            fiveBySevenScrollViewh.scrollEnabled = NO;
            selectedRectangle = (Rectangle*)gesture.view;
            
            rotateSlider.value = [selectedRectangle rotateValue];
            scaleSlider.value = [selectedRectangle scaleValue];
            moveImageX.value = selectedRectangle.frame.origin.x;
            moveImageY.value = selectedRectangle.frame.origin.y;
            changeRadius.value = selectedRectangle.radius;
            
            txtScale.text = [NSString stringWithFormat:@"%.2f", selectedRectangle.scaleValue];
            txtRotate.text = [NSString stringWithFormat:@"%.0f", selectedRectangle.rotateValue];
            txtMoveX.text = [NSString stringWithFormat:@"%.0f", selectedRectangle.frame.origin.x];
            txtMoveY.text = [NSString stringWithFormat:@"%.0f", selectedRectangle.frame.origin.y];
            txtChangeRadius.text = [NSString stringWithFormat:@"%d", selectedRectangle.radius];
        }
    }
}

#pragma UITextField delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    //    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    //
    //    NSString *expression = @"^([0-9]+)?(\\.([0-9]{1,2})?)?$";
    //
    //    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression
    //                                                                           options:NSRegularExpressionCaseInsensitive
    //                                                                             error:nil];
    //    NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString
    //                                                        options:0
    //                                                          range:NSMakeRange(0, [newString length])];
    //    if (numberOfMatches == 0)
    //        return NO;
    //    return YES;
    
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    CGFloat value = [newString floatValue];
    
    switch (textField.tag) {
        case 1:
        {
            NSUInteger lengthOfString = string.length;
            for (NSInteger index = 0; index < lengthOfString; index++) {
                unichar character = [string characterAtIndex:index];
                if (character < 48 && character != 46) return NO; // 48 unichar for 0
                if (character > 57) return NO; // 57 unichar for 9
            }
            
            if (value > 5) return NO;
            
            break;
        }
        case 2:
        {
            NSUInteger lengthOfString = string.length;
            for (NSInteger index = 0; index < lengthOfString; index++) {
                unichar character = [string characterAtIndex:index];
                if (character < 48 && character != 45) return NO; // 48 unichar for 0
                if (character > 57) return NO; // 57 unichar for 9
            }
            
            if (value < -360 || value > 360) return NO;
            
            break;
        }
        case 3:
        case 4:
        {
            NSUInteger lengthOfString = string.length;
            for (NSInteger index = 0; index < lengthOfString; index++) {
                unichar character = [string characterAtIndex:index];
                if (character < 48 && character != 45) return NO; // 48 unichar for 0
                if (character > 57) return NO; // 57 unichar for 9
            }
            
            if (value < -2100 || value > 2100) return NO;
            
            break;
        }
        case 5:
        {
            NSUInteger lengthOfString = string.length;
            for (NSInteger index = 0; index < lengthOfString; index++) {
                unichar character = [string characterAtIndex:index];
                if (character < 48) return NO; // 48 unichar for 0
                if (character > 57) return NO; // 57 unichar for 9
            }
            
            break;
        }
        default:
            break;
    }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
      [textField resignFirstResponder];
    switch (textField.tag) {
        case 1:
        {
            CGFloat value = [textField.text floatValue];
            
            selectedRectangle.scaleValue = value;
            scaleSlider.value = value;
            
            CGAffineTransform rotate = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(selectedRectangle.rotateValue));
            CGAffineTransform scale = CGAffineTransformMakeScale(value, value);
            CGAffineTransform scaleAndRotate = CGAffineTransformConcat(rotate, scale);
            
            selectedRectangle.transform = scaleAndRotate;
            
            txtMoveX.text = [NSString stringWithFormat:@"%.0f", selectedRectangle.frame.origin.x];
            txtMoveY.text = [NSString stringWithFormat:@"%.0f", selectedRectangle.frame.origin.y];
            
            moveImageX.value = selectedRectangle.frame.origin.x;
            moveImageY.value = selectedRectangle.frame.origin.y;
            
            //            selectedRectangle.center = CGPointMake(selectedRectangle.xValue + selectedRectangle.frame.size.width/2, selectedRectangle.yValue + selectedRectangle.frame.size.height/2);
            break;
        }
            
        case 2:
        {
            CGFloat value = [textField.text floatValue];
            
            selectedRectangle.rotateValue = value;
            rotateSlider.value = value;
            
            CGAffineTransform rotate = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(value));
            CGAffineTransform scale = CGAffineTransformMakeScale(selectedRectangle.scaleValue, selectedRectangle.scaleValue);
            CGAffineTransform scaleAndRotate = CGAffineTransformConcat(rotate, scale);
            
            selectedRectangle.transform = scaleAndRotate;
            
            txtMoveX.text = [NSString stringWithFormat:@"%.0f", selectedRectangle.frame.origin.x];
            txtMoveY.text = [NSString stringWithFormat:@"%.0f", selectedRectangle.frame.origin.y];
            
            moveImageX.value = selectedRectangle.frame.origin.x;
            moveImageY.value = selectedRectangle.frame.origin.y;
            
            //            selectedRectangle.center = CGPointMake(selectedRectangle.xValue + selectedRectangle.frame.size.width/2, selectedRectangle.yValue + selectedRectangle.frame.size.height/2);
            break;
        }
            
        case 3:
        {
            CGFloat value = [textField.text floatValue];
            
            selectedRectangle.center = CGPointMake(selectedRectangle.center.x + (value - selectedRectangle.frame.origin.x),
                                                   selectedRectangle.center.y);
            
            CGAffineTransform rotate = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(selectedRectangle.rotateValue));
            CGAffineTransform scale = CGAffineTransformMakeScale(selectedRectangle.scaleValue, selectedRectangle.scaleValue);
            CGAffineTransform scaleAndRotate = CGAffineTransformConcat(rotate, scale);
            
            selectedRectangle.transform = scaleAndRotate;
            
            //            selectedRectangle.xValue = value;
            moveImageX.value = value;
            break;
        }
            
        case 4:
        {
            CGFloat value = [textField.text floatValue];
            
            selectedRectangle.center = CGPointMake(selectedRectangle.center.x,
                                                   selectedRectangle.center.y + (value - selectedRectangle.frame.origin.y));
            
            CGAffineTransform rotate = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(selectedRectangle.rotateValue));
            CGAffineTransform scale = CGAffineTransformMakeScale(selectedRectangle.scaleValue, selectedRectangle.scaleValue);
            CGAffineTransform scaleAndRotate = CGAffineTransformConcat(rotate, scale);
            
            selectedRectangle.transform = scaleAndRotate;
            
            //            selectedRectangle.yValue = value;
            moveImageY.value = value;
            break;
        }
            
        case 5:
        {
            NSInteger radius = [textField.text integerValue];
            
            selectedRectangle.radius = radius;
            [selectedRectangle.layer setCornerRadius:radius];
            
            changeRadius.value = radius;
            break;
        }
            
        default:
            break;
    }
}


- (IBAction)bringToFront:(id)sender {
    if (self.segmentedControl.selectedSegmentIndex == 0)
    {
        [fourbysix bringSubviewToFront:selectedRectangle];
    }
    if (self.segmentedControl.selectedSegmentIndex == 1)
    {
        [fivebyseven bringSubviewToFront:selectedRectangle];
    }
    if (self.segmentedControl.selectedSegmentIndex == 2)
    {
        [fourbysixh bringSubviewToFront:selectedRectangle];
    }
    if (self.segmentedControl.selectedSegmentIndex == 3)
    {
        [fivebysevenh bringSubviewToFront:selectedRectangle];
    }
}



//ROTATE ORIENTATIONS//
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (UIInterfaceOrientationIsPortrait(interfaceOrientation) || UIInterfaceOrientationIsLandscape(interfaceOrientation));
    
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    if(UIInterfaceOrientationIsPortrait(toInterfaceOrientation)){
        //self.view = portraitView;
        CGRect scrollFrame;
        scrollFrame.origin = animcell1.frame.origin;
        scrollFrame.size = CGSizeMake(700, 2345);
        animcell1.frame = scrollFrame;
        animcell1.contentSize = scrollFrame.size;
        CGRect scrollFrame1;
        scrollFrame1.origin = animcell.frame.origin;
        scrollFrame1.size = CGSizeMake(700, 2550);
        animcell.frame = scrollFrame1;
        animcell.contentSize = scrollFrame1.size;
        
        
        for (UIView *viewborder in viewborders) {
            viewborder.layer.borderWidth = 1;
            viewborder.layer.borderColor = [UIColor whiteColor].CGColor;
            viewborder.translatesAutoresizingMaskIntoConstraints = YES;
            CGRect frame = viewborder.frame;
            frame.size.width = 728.0f;
            viewborder.frame = frame;
            
        }
        
        
        CGRect frame = _segmentedControl.frame;
        frame.origin.y=25;//pass the cordinate which you want
        frame.origin.x= 20;//pass the cordinate which you want
        _segmentedControl.frame= frame;
        
        CGRect frame1 = add.frame;
        frame1.origin.y=18;//pass the cordinate which you want
        frame1.origin.x= 350;//pass the cordinate which you want
        add.frame= frame1;
        
        CGRect frame2 = splitlab.frame;
        frame2.origin.y=27;//pass the cordinate which you want
        frame2.origin.x= 534;//pass the cordinate which you want
        splitlab.frame= frame2;
        
        CGRect frame3 = splitlayout.frame;
        frame3.origin.y=24;//pass the cordinate which you want
        frame3.origin.x= 645;//pass the cordinate which you want
        splitlayout.frame= frame3;
        
        CGRect frame4 = fourBySixScrollView.frame;
        frame4.origin.y=296;//pass the cordinate which you want
        frame4.origin.x= 146;//pass the cordinate which you want
        fourBySixScrollView.frame= frame4;
        
        CGRect frame5 = fourBySixScrollViewh.frame;
        frame5.origin.y=403;//pass the cordinate which you want
        frame5.origin.x= 27;//pass the cordinate which you want
        fourBySixScrollViewh.frame= frame5;
        
        CGRect frame6 = fiveBySevenScrollView.frame;
        frame6.origin.y=296;//pass the cordinate which you want
        frame6.origin.x= 129;//pass the cordinate which you want
        fiveBySevenScrollView.frame= frame6;
        
        CGRect frame7 = fiveBySevenScrollViewh.frame;
        frame7.origin.y=387;//pass the cordinate which you want
        frame7.origin.x= 27;//pass the cordinate which you want
        fiveBySevenScrollViewh.frame= frame7;
        
        CGRect frame8 = scalelab.frame;
        frame8.origin.y=80;//pass the cordinate which you want
        frame8.origin.x= 20;//pass the cordinate which you want
        scalelab.frame= frame8;
        
        CGRect frame9 = scaleSlider.frame;
        frame9.origin.y=105;//pass the cordinate which you want
        frame9.origin.x= 20;//pass the cordinate which you want
        scaleSlider.frame= frame9;
        
        CGRect frame10 = txtScale.frame;
        frame10.origin.y=105;//pass the cordinate which you want
        frame10.origin.x= 120;//pass the cordinate which you want
        txtScale.frame= frame10;
        
        CGRect frame11 = rotlab.frame;
        frame11.origin.y=80;//pass the cordinate which you want
        frame11.origin.x= 220;//pass the cordinate which you want
        rotlab.frame= frame11;
        
        CGRect frame12 = rotateSlider.frame;
        frame12.origin.y=105;//pass the cordinate which you want
        frame12.origin.x= 220;//pass the cordinate which you want
        rotateSlider.frame= frame12;
        
        CGRect frame13 = txtRotate.frame;
        frame13.origin.y=105;//pass the cordinate which you want
        frame13.origin.x= 320;//pass the cordinate which you want
        txtRotate.frame= frame13;
        
        CGRect frame14 = radlab.frame;
        frame14.origin.y=80;//pass the cordinate which you want
        frame14.origin.x= 420;//pass the cordinate which you want
        radlab.frame= frame14;
        
        CGRect frame15 = changeRadius.frame;
        frame15.origin.y=105;//pass the cordinate which you want
        frame15.origin.x= 420;//pass the cordinate which you want
        changeRadius.frame= frame15;
        
        CGRect frame16 = txtChangeRadius.frame;
        frame16.origin.y=105;//pass the cordinate which you want
        frame16.origin.x= 520;//pass the cordinate which you want
        txtChangeRadius.frame= frame16;
        
        CGRect frame17 = mlrlab.frame;
        frame17.origin.y=150;//pass the cordinate which you want
        frame17.origin.x= 20;//pass the cordinate which you want
        mlrlab.frame= frame17;
        
        CGRect frame18 = moveImageX.frame;
        frame18.origin.y=180;//pass the cordinate which you want
        frame18.origin.x= 20;//pass the cordinate which you want
        moveImageX.frame= frame18;
        
        CGRect frame19 = txtMoveX.frame;
        frame19.origin.y=180;//pass the cordinate which you want
        frame19.origin.x= 120;//pass the cordinate which you want
        txtMoveX.frame= frame19;
        
        CGRect frame20 = mtblab.frame;
        frame20.origin.y=150;//pass the cordinate which you want
        frame20.origin.x= 220;//pass the cordinate which you want
        mtblab.frame= frame20;
        
        CGRect frame21 = moveImageY.frame;
        frame21.origin.y=180;//pass the cordinate which you want
        frame21.origin.x= 220;//pass the cordinate which you want
        moveImageY.frame= frame21;
        
        CGRect frame22 = txtMoveY.frame;
        frame22.origin.y=180;//pass the cordinate which you want
        frame22.origin.x= 320;//pass the cordinate which you want
        txtMoveY.frame= frame22;
        
        CGRect frame23 = back.frame;
        frame23.origin.y=160;//pass the cordinate which you want
        frame23.origin.x= 420;//pass the cordinate which you want
        back.frame= frame23;
        
        CGRect frame24 = overlab.frame;
        frame24.origin.y=170;//pass the cordinate which you want
        frame24.origin.x= 595;//pass the cordinate which you want
        overlab.frame= frame24;
        
        CGRect frame25 = switchOverlay1.frame;
        frame25.origin.y=165;//pass the cordinate which you want
        frame25.origin.x= 659;//pass the cordinate which you want
        switchOverlay1.frame= frame25;
        
        CGRect frame26 = resetlay.frame;
        frame26.origin.y=230;//pass the cordinate which you want
        frame26.origin.x= 20;//pass the cordinate which you want
        resetlay.frame= frame26;
        
        CGRect frame27 = bringfront.frame;
        frame27.origin.y=230;//pass the cordinate which you want
        frame27.origin.x= 186;//pass the cordinate which you want
        bringfront.frame= frame27;
        
        CGRect frame28 = emaillaycus.frame;
        frame28.origin.y=230;//pass the cordinate which you want
        frame28.origin.x= 352;//pass the cordinate which you want
        emaillaycus.frame= frame28;
        
        CGRect frame29 = cancelcus.frame;
        frame29.origin.y=960;//pass the cordinate which you want
        frame29.origin.x= 20;//pass the cordinate which you want
        cancelcus.frame= frame29;
        
        CGRect frame30 = donecus.frame;
        frame30.origin.y=230;//pass the cordinate which you want
        frame30.origin.x= 518;//pass the cordinate which you want
        donecus.frame= frame30;
        
        
    }
    else if(UIInterfaceOrientationIsLandscape(toInterfaceOrientation)){
        CGRect scrollFrame;
        scrollFrame.origin = animcell1.frame.origin;
        scrollFrame.size = CGSizeMake(960, 2345);
        animcell1.frame = scrollFrame;
        animcell1.contentSize = scrollFrame.size;
        CGRect scrollFrame1;
        scrollFrame1.origin = animcell.frame.origin;
        scrollFrame1.size = CGSizeMake(960, 2550);
        animcell.frame = scrollFrame1;
        animcell.contentSize = scrollFrame1.size;
        
        for (UIView *viewborder in viewborders) {
            viewborder.layer.borderWidth = 1;
            viewborder.layer.borderColor = [UIColor whiteColor].CGColor;
            viewborder.translatesAutoresizingMaskIntoConstraints = YES;
            CGRect frame = viewborder.frame;
            frame.size.width = 984.0f;
            viewborder.frame = frame;
            
        }
        
        CGRect frame = _segmentedControl.frame;
        frame.origin.y=20;//pass the cordinate which you want
        frame.origin.x= 20;//pass the cordinate which you want
        _segmentedControl.frame= frame;
        
        CGRect frame1 = add.frame;
        frame1.origin.y=77;//pass the cordinate which you want
        frame1.origin.x= 20;//pass the cordinate which you want
        add.frame= frame1;
        
        CGRect frame2 = splitlab.frame;
        frame2.origin.y=74;//pass the cordinate which you want
        frame2.origin.x= 204;//pass the cordinate which you want
        splitlab.frame= frame2;
        
        CGRect frame3 = splitlayout.frame;
        frame3.origin.y=98;//pass the cordinate which you want
        frame3.origin.x= 206;//pass the cordinate which you want
        splitlayout.frame= frame3;
        
        CGRect frame4 = fourBySixScrollView.frame;
        frame4.origin.y=31;//pass the cordinate which you want
        frame4.origin.x= 414;//pass the cordinate which you want
        fourBySixScrollView.frame= frame4;
        
        CGRect frame5 = fourBySixScrollViewh.frame;
        frame5.origin.y=151;//pass the cordinate which you want
        frame5.origin.x= 296;//pass the cordinate which you want
        fourBySixScrollViewh.frame= frame5;
        
        CGRect frame6 = fiveBySevenScrollView.frame;
        frame6.origin.y=31;//pass the cordinate which you want
        frame6.origin.x= 397;//pass the cordinate which you want
        fiveBySevenScrollView.frame= frame6;
        
        CGRect frame7 = fiveBySevenScrollViewh.frame;
        frame7.origin.y=134;//pass the cordinate which you want
        frame7.origin.x= 295;//pass the cordinate which you want
        fiveBySevenScrollViewh.frame= frame7;
        
        CGRect frame8 = scalelab.frame;
        frame8.origin.y=143;//pass the cordinate which you want
        frame8.origin.x= 20;//pass the cordinate which you want
        scalelab.frame= frame8;
        
        CGRect frame9 = scaleSlider.frame;
        frame9.origin.y=164;//pass the cordinate which you want
        frame9.origin.x= 20;//pass the cordinate which you want
        scaleSlider.frame= frame9;
        
        CGRect frame10 = txtScale.frame;
        frame10.origin.y=163;//pass the cordinate which you want
        frame10.origin.x= 127;//pass the cordinate which you want
        txtScale.frame= frame10;
        
        CGRect frame11 = rotlab.frame;
        frame11.origin.y=200;//pass the cordinate which you want
        frame11.origin.x= 20;//pass the cordinate which you want
        rotlab.frame= frame11;
        
        CGRect frame12 = rotateSlider.frame;
        frame12.origin.y=218;//pass the cordinate which you want
        frame12.origin.x= 20;//pass the cordinate which you want
        rotateSlider.frame= frame12;
        
        CGRect frame13 = txtRotate.frame;
        frame13.origin.y=217;//pass the cordinate which you want
        frame13.origin.x= 127;//pass the cordinate which you want
        txtRotate.frame= frame13;
        
        CGRect frame14 = radlab.frame;
        frame14.origin.y=375;//pass the cordinate which you want
        frame14.origin.x= 20;//pass the cordinate which you want
        radlab.frame= frame14;
        
        CGRect frame15 = changeRadius.frame;
        frame15.origin.y=400;//pass the cordinate which you want
        frame15.origin.x= 20;//pass the cordinate which you want
        changeRadius.frame= frame15;
        
        CGRect frame16 = txtChangeRadius.frame;
        frame16.origin.y=400;//pass the cordinate which you want
        frame16.origin.x= 127;//pass the cordinate which you want
        txtChangeRadius.frame= frame16;
        
        CGRect frame17 = mlrlab.frame;
        frame17.origin.y=254;//pass the cordinate which you want
        frame17.origin.x= 20;//pass the cordinate which you want
        mlrlab.frame= frame17;
        
        CGRect frame18 = moveImageX.frame;
        frame18.origin.y=279;//pass the cordinate which you want
        frame18.origin.x= 20;//pass the cordinate which you want
        moveImageX.frame= frame18;
        
        CGRect frame19 = txtMoveX.frame;
        frame19.origin.y=278;//pass the cordinate which you want
        frame19.origin.x= 127;//pass the cordinate which you want
        txtMoveX.frame= frame19;
        
        CGRect frame20 = mtblab.frame;
        frame20.origin.y=315;//pass the cordinate which you want
        frame20.origin.x= 20;//pass the cordinate which you want
        mtblab.frame= frame20;
        
        CGRect frame21 = moveImageY.frame;
        frame21.origin.y=340;//pass the cordinate which you want
        frame21.origin.x= 20;//pass the cordinate which you want
        moveImageY.frame= frame21;
        
        CGRect frame22 = txtMoveY.frame;
        frame22.origin.y=340;//pass the cordinate which you want
        frame22.origin.x= 127;//pass the cordinate which you want
        txtMoveY.frame= frame22;
        
        CGRect frame23 = back.frame;
        frame23.origin.y=454;//pass the cordinate which you want
        frame23.origin.x= 20;//pass the cordinate which you want
        back.frame= frame23;
        
        CGRect frame24 = overlab.frame;
        frame24.origin.y=451;//pass the cordinate which you want
        frame24.origin.x= 203;//pass the cordinate which you want
        overlab.frame= frame24;
        
        CGRect frame25 = switchOverlay1.frame;
        frame25.origin.y=474;//pass the cordinate which you want
        frame25.origin.x= 203;//pass the cordinate which you want
        switchOverlay1.frame= frame25;
        
        CGRect frame26 = resetlay.frame;
        frame26.origin.y=510;//pass the cordinate which you want
        frame26.origin.x= 20;//pass the cordinate which you want
        resetlay.frame= frame26;
        
        CGRect frame27 = bringfront.frame;
        frame27.origin.y=566;//pass the cordinate which you want
        frame27.origin.x= 20;//pass the cordinate which you want
        bringfront.frame= frame27;
        
        CGRect frame28 = emaillaycus.frame;
        frame28.origin.y=621;//pass the cordinate which you want
        frame28.origin.x= 20;//pass the cordinate which you want
        emaillaycus.frame= frame28;
        
        CGRect frame29 = cancelcus.frame;
        frame29.origin.y=20;//pass the cordinate which you want
        frame29.origin.x= 915;//pass the cordinate which you want
        cancelcus.frame= frame29;
        
        CGRect frame30 = donecus.frame;
        frame30.origin.y=698;//pass the cordinate which you want
        frame30.origin.x= 20;//pass the cordinate which you want
        donecus.frame= frame30;
        
        
    }
    
    
    
    
}



- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

- (IBAction)eventname:(id)sender {
    [txtFldCNames resignFirstResponder];
    [txtFldEDate resignFirstResponder];
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}
@end

