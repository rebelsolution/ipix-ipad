//
//  TSPTestViewController.m
//  iPIX Social
//
//  Created by Luong Chau Tuan on 1/22/17.
//  Copyright © 2017 Tuts+. All rights reserved.
//

#import "TSPTestViewController.h"

@interface TSPTestViewController ()

@end

@implementation TSPTestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    CGFloat borderWidth = 2.0f;
    
    self.viewSocial.frame = CGRectInset(self.viewSocial.frame, -borderWidth, -borderWidth);
    self.viewSocial.layer.borderColor = [UIColor yellowColor].CGColor;
    self.viewSocial.layer.borderWidth = borderWidth;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIButton Sender
- (IBAction)btnShareWithEmail_Clicked:(id)sender
{
    [self.delegate shareWithEmail];
}

- (IBAction)btnShareWithNormalText_Clicked:(id)sender
{
    [self.delegate shareWithNormalText];
}

- (IBAction)btnShareWithFacebook_Clicked:(id)sender
{
    [self.delegate shareWithFacebook];
}

- (IBAction)btnShareWithTwitter_Clicked:(id)sender
{
    [self.delegate shareWithTwitter];
}

- (IBAction)btnCancelSharing_Clicked:(id)sender
{
    [self.delegate cancelSharing];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
