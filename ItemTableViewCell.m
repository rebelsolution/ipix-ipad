//
//  ItemTableViewCell.m
//  PhotoManagerTest
//
//  Created by App Developer on 10/5/16.
//  Copyright © 2016 My Company. All rights reserved.
//

#import "ItemTableViewCell.h"

@implementation ItemTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.btnRetry.hidden = YES;
    self.btnDelete.hidden = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
