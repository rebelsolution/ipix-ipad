//
//  TSPTestViewController.h
//  iPIX Social
//
//  Created by Luong Chau Tuan on 1/22/17.
//  Copyright © 2017 Tuts+. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TSPTestViewControllerDelegate <NSObject>

- (void)shareWithFacebook;
- (void)shareWithNormalText;
- (void)shareWithEmail;
- (void)shareWithTwitter;
- (void)cancelSharing;

@end

@interface TSPTestViewController : UIViewController

@property (nonatomic) id <TSPTestViewControllerDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIButton *btnShareEmail;
@property (weak, nonatomic) IBOutlet UIView *emailScreen;
@property (weak, nonatomic) IBOutlet UIButton *btnShareText;
@property (weak, nonatomic) IBOutlet UIButton *btnShareFacebook;
@property (weak, nonatomic) IBOutlet UIButton *btnShareTwitter;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleShare;
@property (weak, nonatomic) IBOutlet UIView *viewSocial;
@property (weak, nonatomic) IBOutlet UIButton *btnCancelSharing;

@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *lineborder;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *buttoneffects1;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *labelcolors;

@end
