//
//  AMUploadItem.h
//  PhotoManagerTest
//
//  Created by App Developer on 10/5/16.
//  Copyright © 2016 My Company. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface AMUploadItem : NSObject <NSCoding>
@property (nonatomic, strong) NSData * imageData; //The image to be uploaded in NSData format
@property (nonatomic, strong) NSString * itemName;  //The name of the item being uploaded.
@property (nonatomic, strong) NSDictionary * dictionary;  //The image to be uploaded.
@property bool  isUploading; //Whether the uploaded is ongoing or paused.
@property float fileProgress; //The fractional progress of the uploaded; a float between 0.0 and 1.0.
@property bool  didError; //Flag to show if the uploaded errored
@property (nonatomic, strong) NSURLSessionUploadTask *uploadTask; //A NSURLSessionUploadTask object that will be used to keep a strong reference to the upload task of a file.
@property (nonatomic, strong) NSData *taskResumeData; //
@property (nonatomic) BOOL uploadComplete; //Indicates whether a file download has been completed.
@property (nonatomic) unsigned long taskIdentifier; // Unique identifer assigned the the session manager



- (id) initWithItem:(NSString *)itemName andDictionary:(NSMutableDictionary*)dictionary;

@end
