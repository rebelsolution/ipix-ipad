

#import "AMPhotoManager.h"
#import "Reachability.h"
#import "AMUploadItem.h"


//Also be sure to update the info.plist App Transport Security Settings to your domain!
//CHANGE THIS URL TO THE CORRECT URL WHERE YOUR UPLOADS SHOULD GO
//static NSString * const UPLOAD_URL          = @"http://10.0.1.3:9696";


static NSString * const IMAGE_FILE_EXT      = @".jpg";
static NSString * const FILE_QUEUE_NAME     = @"photoQueue.plist";
static NSString * const FILE_LOG_NAME       = @"LOG_PHOTO_ERRORS.txt";


@interface AMPhotoManager () <NSURLSessionDelegate, NSURLSessionDataDelegate, NSURLSessionTaskDelegate>
@property (nonatomic, strong) NSTimer * timer;
@property (nonatomic, strong) NSMutableArray * arrQueue;
@property (nonatomic, strong) NSURLSessionConfiguration * config;
@property (nonatomic, strong) NSURLSession * session;
@end


@implementation AMPhotoManager

static AMPhotoManager* _sharedManager;

#pragma mark - SINGLETON SETUP

+ (AMPhotoManager *) sharedManager {
    
    static AMPhotoManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
    });
    return sharedManager;
    
}

- (id) init {
    if (self =  [super init]){
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:[self getPathToPhotoQueueFile]] ) {
            //Previous Queue Exists
            [self getData:[self getPathToPhotoQueueFile]];
            
        } else {
            //CREATE A NEW QUEUE
            _arrQueue = [[NSMutableArray alloc] init];
        }
        
        _config = [NSURLSessionConfiguration defaultSessionConfiguration];
        _config.allowsCellularAccess = self.useCellularData;
        _config.HTTPMaximumConnectionsPerHost = 5;//ehh... you can change this. I woudn't go over 5 though.
        
        _session = [NSURLSession sessionWithConfiguration:_config delegate:self delegateQueue:[NSOperationQueue mainQueue]];
        
    }
    
    return self;
}

#pragma mark - Public methods

- (void) start {
    if (!_timer) {
        if(![self isTimerRunning]){
            _timer = [NSTimer scheduledTimerWithTimeInterval:self.timerInterval target:self selector:@selector(checkForPhotosToUpload) userInfo:nil repeats:YES];
        }
    }
}

- (void) stop {
    [_timer invalidate];
    _timer = nil;
    [self saveData:[self getPathToPhotoQueueFile]];
}

- (NSUInteger) getQueueCount {
    return [_arrQueue count];
}

- (NSMutableArray*) listImagesInQueue {
    return _arrQueue;
}

//DELETES ALL ITEMS IN QUEUE
- (void) deleteQueue {
    [_arrQueue removeAllObjects];
    [self saveData:[self getPathToPhotoQueueFile]];
    [self notifiyDelegatesOfChanges];
    
}

- (void) retryItemWithIdentifier:(NSUInteger)identfier {
    
    AMUploadItem * item = _arrQueue[[self getItemArrayIndexFromTaskIndentifier:identfier]];
    
    item.isUploading = false;
    item.didError = false;
    item.uploadComplete = false;
    item.fileProgress = 0.0f;
    
    NSData * uploadData = item.imageData;
    
    item.uploadTask =  [_session uploadTaskWithRequest:[self getPostRequest] fromData:uploadData completionHandler:^(NSData *data,NSURLResponse *response,NSError *error) {
        
        //MAIN THREAD ACCESS UPDATE TO THE GUI OR DISPLAY ERRORS
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (error) {
                if (self.logErrorReports) {
                    [self addLogToReport:error.localizedDescription];
                }
            }
            
            [self finishedUploadTaskWith:data Response:response TaskIdentifier:item.uploadTask.taskIdentifier];
        });
        
    }];
    
    item.taskIdentifier = item.uploadTask.taskIdentifier;
    
    
    //NOTIFY ANY DELEGATES
    [self notifiyDelegatesOfChanges];
    
    
}

- (void) deleteItemWithIdentifier:(NSUInteger)identfier {
    
    [_arrQueue removeObjectAtIndex:[self getItemArrayIndexFromTaskIndentifier:identfier]];
    
    [self saveData:[self getPathToPhotoQueueFile]];
    
    [self notifiyDelegatesOfChanges];
    
}

- (BOOL) isTimerRunning {
    return _timer.isValid;
}

- (void) addImageToQueue:(NSData *)image  {
    
    NSData * uploadData = image;
    
    //Create the upload item
    AMUploadItem * upload = [[AMUploadItem alloc] init];
    upload.uploadTask =  [_session uploadTaskWithRequest:[self getPostRequest] fromData:uploadData completionHandler:^(NSData *data,NSURLResponse *response,NSError *error) {
        
        //MAIN THREAD ACCESS UPDATE TO THE GUI OR DISPLAY ERRORS
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (error) {
                if (self.logErrorReports) {
                    [self addLogToReport:error.localizedDescription];
                }
            }
            
            [self finishedUploadTaskWith:data Response:response TaskIdentifier:upload.uploadTask.taskIdentifier];
        });
        
    }];
    
    upload.taskIdentifier = upload.uploadTask.taskIdentifier;
    upload.imageData = image;
    
    //Add to queue
    [_arrQueue addObject:upload];
    
    //Save the state
    [self saveData:[self getPathToPhotoQueueFile]];
    
    //Notifiy any delegates
    [self notifiyDelegatesOfChanges];
    
    
}

#pragma mark - Post Methods


- (NSURL*) uploadURL {
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"print"];
    
    NSLog (@"YO MY URL IS %@", savedValue);
    return [NSURL URLWithString:[NSString stringWithFormat:@"http://%@", savedValue]];
}

- (NSString*) getBoundary {
    return @"---------------------------14737809831466499882746641449";
}

- (NSMutableURLRequest*) getPostRequest {
    NSMutableURLRequest * postRequest = [NSMutableURLRequest requestWithURL:[self uploadURL] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];
    [postRequest setHTTPMethod:@"POST"];
    [postRequest addValue:[NSString stringWithFormat:@"multipart/form-data; boundary=%@", [self getBoundary]] forHTTPHeaderField:@"Content-Type"];
    
    return postRequest;
}

- (NSMutableData *)  image:(NSData *)image {
    
    NSMutableData *postBody = [NSMutableData data];
    
    // JSON part for posting varibles
    [postBody appendData:[[NSString stringWithFormat:@"--%@\r\n", [self getBoundary]] dataUsingEncoding:NSUTF8StringEncoding]];
    [postBody appendData:[@"Content-Disposition: form-data; name=\"params\"\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [postBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    // media part usually a form field that accepts the image
    [postBody appendData:[[NSString stringWithFormat:@"--%@\r\n", [self getBoundary]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [postBody appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [postBody appendData:image];
    [postBody appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // final boundary
    [postBody appendData:[[NSString stringWithFormat:@"--%@--\r\n", [self getBoundary]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    return postBody;
}



#pragma mark - Class Delegate methods

- (void) notifiyDelegatesOfChanges {
    if (self.delegate && [self.delegate respondsToSelector:@selector(didUpdatePhotoCount) ] ){
        [self.delegate didUpdatePhotoCount];
    }
}

- (void) notiftyDelegatesofProgressViewChange:(NSUInteger)identifier {
    if (self.delegate && [self.delegate respondsToSelector:@selector(updateProgressViewInIndex:) ] ){
        [self.delegate updateProgressViewInIndex:identifier];
    }
}

- (void) notifyCellReloadData:(NSUInteger)identifer {
    if (self.delegate && [self.delegate respondsToSelector:@selector(reloadCellContainingIdentifer:) ] ){
        [self.delegate reloadCellContainingIdentifer:identifer];
    }
}

- (void) notifiyPhotoWasAdded:(NSString*)name {
    if (self.delegate && [self.delegate respondsToSelector:@selector(photoWasAddedToQueue:) ] ){
        [self.delegate photoWasAddedToQueue:name];
    }
}


#pragma mark - Queue Save / Restore

- (NSString*) getPathToPhotoQueueFile {
    NSURL * docsDir = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
    NSURL * plist = [docsDir URLByAppendingPathComponent:FILE_QUEUE_NAME];
    return plist.path;
}

- (NSString*) getPathToPhotoReportFile {
    NSURL * docsDir = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
    NSURL * plist = [docsDir URLByAppendingPathComponent:FILE_LOG_NAME];
    return plist.path;
}

- (void) saveData:(NSString*)filePath {
    //SAVES THE QUEUE LATER RETRIEVAL
    [NSKeyedArchiver archiveRootObject:_arrQueue toFile:filePath];
}

- (void) getData:(NSString*)filePath {
    //RESTORES THE QUEUE
    _arrQueue = [NSKeyedUnarchiver unarchiveObjectWithFile:filePath];
}



#pragma mark - Error Reporting

- (void) addLogToReport:(NSString*)content{
    content = [NSString stringWithFormat:@"%@: %@\n",[self currentDateInSystemTimezone],content];
    [content writeToFile:[self getPathToPhotoReportFile] atomically:NO encoding:NSStringEncodingConversionAllowLossy error:nil];
}

- (NSString*) getReportContents {
    
    NSError *error = nil;
    NSString *content = @"";
    if ([[NSFileManager defaultManager] fileExistsAtPath:[self getPathToPhotoReportFile]]){
        content = [[NSString alloc] initWithContentsOfFile:[self getPathToPhotoReportFile] usedEncoding:nil error:&error];
    } else {
        content = @"NOTHING TO REPORT\n";
    }
    
    if (error) {
        content = error.localizedDescription;
    }
    
    return content;
}

- (void) clearReportLog {
    if ([[NSFileManager defaultManager] fileExistsAtPath:[self getPathToPhotoReportFile]]){
        [[NSFileManager defaultManager] removeItemAtPath:[self getPathToPhotoReportFile] error:nil];
    }
}

#pragma mark - Private methods

- (void) checkForPhotosToUpload {
    // NSLog(@" Check for Photos to uplaod Arr PhotoQueue Count: %lu", [_arrQueue count]);
    if ([_arrQueue count]) {
        if ([self hasInternetConnection]) {
            if (!self.useCellularData) {
                if ([self isWifiConnected]) {
                    //GO AHEAD AND UPLOAD ON WIFI ONLY
                    [self processQueue];
                    // NSLog(@"Process Queue On Wifi");
                    
                } else {
                    //PEND THE PICTURE FOR LATER
                    // NSLog(@"Has Internet, but needs wifi to upload. Will upload later.");
                }
                
            } else {
                //GO AHEAD AND UPLOAD ON ANY CONNECTION
                [self processQueue];
                // NSLog(@"Process Queue On Cellular");
            }
            
        } else {
            //PEND THE PICTURE FOR LATER
            //NSLog(@"No internet. Will upload later.");
        }
    } else {
        // NSLog(@"No photos in queue");
    }
    
    
}

- (void) processQueue {
    
    for (AMUploadItem * item in _arrQueue){
        
        //The Item maybe have been resumed from a hard close of the app, just rebuild the session task.
        if (item.uploadTask == nil) {
            [self retryItemWithIdentifier:item.taskIdentifier];
        }
        
        //Process new queue item
        if (!item.isUploading && item.didError == NO) {
            item.isUploading = true;
            [item.uploadTask resume];
        }
        
        //notify cell that the task failed.
        if (item.uploadTask.state == NSURLSessionTaskStateCompleted && item.didError == YES) {
            [self notifyCellReloadData:item.taskIdentifier];
        }
        
    }
}

- (int) getItemArrayIndexFromTaskIndentifier:(NSUInteger)taskIdentifier{
    int index = 0;
    
    for (int i=0; i< [self.arrQueue count]; i++) {
        AMUploadItem *upload = [self.arrQueue objectAtIndex:i];
        if (upload.taskIdentifier == taskIdentifier) {
            index = i;
            break;
        }
    }
    return index;
}

#pragma mark - URLSession delegates

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didSendBodyData:(int64_t)bytesSent totalBytesSent:(int64_t)totalBytesSent totalBytesExpectedToSend:(int64_t)totalBytesExpectedToSend {
    
    NSInteger taskID = task.taskIdentifier;
    
    if (totalBytesExpectedToSend == NSURLSessionTransferSizeUnknown) {
        NSLog(@"Unknown transfer size");
    }
    else{
        
        // Locate the FileDownloadInfo object among all based on the taskIdentifier property of the task.
        int index = [self getItemArrayIndexFromTaskIndentifier:taskID];
        
        AMUploadItem *item = [self.arrQueue objectAtIndex:index];
        
        // Calculate the progress.
        item.fileProgress = (float)totalBytesSent/(float)totalBytesExpectedToSend;
        
        //Since this thread is on the backgournd, access the main thread.
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [self notiftyDelegatesofProgressViewChange:taskID];
        }];
        
    }
    
}

- (void) finishedUploadTaskWith:(NSData*)data Response:(NSURLResponse *)response TaskIdentifier:(NSUInteger)taskIdentifer {
    //THIS METHOD IS BACK ON THE MAIN THREAD
    NSError * e = nil;
    
    if (e) {
        if (self.logErrorReports) {
            [self addLogToReport:e.localizedDescription];
        }
        
    }
    
    int index = -1;
    AMUploadItem * item = nil;
    for (int i = 0; i < [_arrQueue count]; i++) {
        item = _arrQueue[i];
        if(item.taskIdentifier == taskIdentifer){
            index = i;
            item.isUploading = false;
            break;
        }
    }
    
    
    if(data != nil) {
        
        NSDictionary * resultsDict = [NSJSONSerialization JSONObjectWithData: data options: NSJSONReadingMutableContainers error: &e];
        
        if (item.fileProgress == 1.000000) {
            
            if (index >= 0) {
                item.uploadComplete = YES;
                item.didError = NO;
                
                [_arrQueue removeObjectAtIndex:index];
                
                [self notifiyDelegatesOfChanges];
                
                [self saveData:[self getPathToPhotoQueueFile]];
                
            } else {
                NSLog(@"index was -1 on success");
            }
            
            
        } else {
            item.uploadComplete = NO;
            item.didError = YES;
            item.fileProgress = 0.0f;
            [self notiftyDelegatesofProgressViewChange:item.taskIdentifier];
            if (self.logErrorReports) {
                [self addLogToReport:[NSString stringWithFormat:@"FAILED to upload: Response: %@", [resultsDict description]]];
            }
            
        }
    }
    else{
        item.uploadComplete = NO;
        item.didError = YES;
        item.fileProgress = 0.0f;
        [self notiftyDelegatesofProgressViewChange:item.taskIdentifier];
        
    }
    
}

#pragma mark - REACHABILITY METHODS

- (BOOL) hasInternetConnection {
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        return false;
    } else {
        return true;
    }
    
}

- (BOOL) isWifiConnected {
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == ReachableViaWiFi) {
        return true;
    } else {
        return false;
    }
    
}

#pragma mark - Helpers

- (NSString *) currentDateInSystemTimezone {
    
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    [dateFormatter setTimeZone:timeZone];
    NSString* stringFromDate = [dateFormatter stringFromDate:[NSDate date]];
    
    return stringFromDate;
}

- (NSString*) returnState:(NSURLSessionTaskState)state{
    NSString *result = nil;
    
    switch(state) {
        case NSURLSessionTaskStateRunning:
            result = @"NSURLSessionTaskStateRunning";
            break;
        case NSURLSessionTaskStateSuspended:
            result = @"NSURLSessionTaskStateSuspended";
            break;
        case NSURLSessionTaskStateCanceling:
            result = @"NSURLSessionTaskStateCanceling";
            break;
        case NSURLSessionTaskStateCompleted:
            result = @"NSURLSessionTaskStateCompleted";
            break;
            
        default:
            result = @"unknown";
    }
    
    return result;
}









@end
